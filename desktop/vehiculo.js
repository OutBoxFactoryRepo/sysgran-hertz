                    



/*!
 * Ext JS Library 3.1.0
 * Copyright(c) 2006-2009 Ext JS, LLC
 * licensing@extjs.com
 * http://www.extjs.com/license
 */


var fm = Ext.form;
var vehiculo = null;

Ext.grid.dataVehiculo = new Ext.data.JsonStore(
    {
            // store configs
            autoDestroy: false,
            autoLoad:true,
            url: 'http://local.hertz/web/app_dev.php/vehiculo/search/',
            storeId: 'myStore',
            api:
            {
                create : 'http://local.hertz/web/app_dev.php/vehiculo/crear/',
                read : 'http://local.hertz/web/app_dev.php/vehiculo/search/',
                update: 'http://local.hertz/web/app_dev.php/vehiculo/editar/'
                //,destroy: '../web/app_dev.php/vehiculo/delete/'
            },
            writer: 
                {
                    type: 'post',
                    writeAllFields : true,  //just send changed fields
                    allowSingle :false,     //always wrap in an array
                    encode:false,
                },
                reader: {
                    root: "Violations",
                    type: "json",
                    messageProperty : 'message' //without this, it doesn't work
                },
            // reader configs
             sortInfo: {field: 'id', direction: 'DESC'},
                                idProperty: 'id',
                                fields: [{ name: 'id',type:'integer' },
                                       { name: 'descripcion', type: 'string' },
                                       { name: 'modelo', type: 'integer' },
                                       { name: 'agencia', type: 'string' ,mapping: 'agencia.id'},
                                       { name: 'estado', type: 'string' ,mapping: 'estado.id'},
                                       { name: 'kilometros', type: 'integer'},
                                       { name: 'modelo_cal', type: 'string'},
                                       { name: 'fecha_hora_devolucion', type: 'date'},
                                       { name: 'fecha_hora_salida', type: 'date'},
                                       { name: 'fecha_hora_retorno', type: 'date'},
                                       { name: 'dominio', type: 'string'},
                                       { name: 'debaja', type: 'boolean'}]
    });



var storeAgencia = null;
var storeEstadoVehiculo = null;
var comboEstadoV = null;
var comboAgencias = null;



var comboVehiculo = null;
var arraySiNo = null;
var comboDeBaja = null;
var editorVehiculo = null;
var grillaVehiculo = null;
function cargarVehiculoObj()
{

     storeAgencia = new Ext.data.JsonStore({
        url: 'http://local.hertz/web/app_dev.php/agencia/search/',
        root: '',
        fields: ['id', 'descripcion']
    });
    storeAgencia.load();


    storeEstadoVehiculo = new Ext.data.JsonStore({
        url: 'http://local.hertz/web/app_dev.php/estadovehiculo/search/',
        root: '',
        fields: ['id', 'descripcion']
    });
    storeEstadoVehiculo.load();



    comboEstadoV = new Ext.form.ComboBox({
        typeAhead: true,
        triggerAction: 'all',
        lazyRender:true,
        mode: 'local',
        store: storeEstadoVehiculo,
        valueField: 'id',
        displayField: 'descripcion'
    });


    comboAgencias = new Ext.form.ComboBox({
        typeAhead: true,
        triggerAction: 'all',
        lazyRender:true,
        mode: 'local',
        store: storeAgencia,
        valueField: 'id',
        displayField: 'descripcion'
    });


    vehiculo = Ext.data.Record.create(
        [
                    { name: 'descripcion', type: 'string' },
                    { name: 'modelo', type: 'integer' },
                    { name: 'agencia', type: 'string' ,mapping: 'agencia.id'},
                    { name: 'estado', type: 'string' ,mapping: 'estado.id'},
                    { name: 'kilometros', type: 'integer'},
                    { name: 'modelo_cal', type: 'string'},
                    { name: 'fecha_hora_devolucion', type: 'date', dateFormat:'d/m/Y H:i'},
                    { name: 'fecha_hora_salida', type: 'date', dateFormat:'d/m/Y H:i'},
                    { name: 'fecha_hora_retorno', type: 'date', dateFormat:'d/m/Y H:i'},
                    { name: 'dominio', type: 'string'},
                    { name: 'debaja', type: 'boolean'}

        ]);

   

    arraySiNo = new Ext.data.ArrayStore({
        id: 0,
        fields: [
            'myId',
            'displayText'
        ],
        data: [[false, 'Alta'], [true, 'De baja']]
    });


    comboDeBaja = new Ext.form.ComboBox({
        typeAhead: true,
        triggerAction: 'all',
        lazyRender:true,
        mode: 'local',
        store: arraySiNo,
        valueField: 'myId',
        displayField: 'displayText'
    });

    editorVehiculo = new Ext.ux.grid.RowEditor(
    {
        id:"editorVehiculo",
        saveText  : "Guardar",
        cancelText: "Cancelar",
        //clicksToEdit: 1, //this changes from the default double-click activation to single click activation
        errorSummary: false //disables display of validation messages if the row is invalid
    });

    editorVehiculo.on(
    {
        canceledit: function(roweditor, changes)
        {
            Ext.grid.dataVehiculo.reload();
        },
        validateedit: function(roweditor, changes, record, rowIndex)
        {
            console.log(this.isValid());
            if (this.editing && !this.isValid()) 
            {
                return false;
            }
            else
            {
                record.commit();
            }
        },
        afteredit: function(roweditor, changes, record, rowIndex) 
        {
            Ext.grid.dataVehiculo.reload();
        }
    });

    Ext.util.Format.comboRenderer = function(combo)
    {
        return function(value)
        {
            var record = combo.findRecord(combo.valueField, value);
            return record ? record.get(combo.displayField) : combo.valueNotFoundText;
        }
    };
    grillaVehiculo = null;
    grillaVehiculo = new Ext.grid.GridPanel({
                        border:false,
                        id:'grillaVehiculo',
                        plugins: editorVehiculo,
                        ds: Ext.grid.dataVehiculo,
                        cm: new Ext.grid.ColumnModel([
                                            //new Ext.grid.RowNumberer(),
                                            {
                                                header: "ID",
                                                sortable: true,
                                                width: 50,
                                                dataIndex: "id"
                                            }, {
                                                header: "Descripci&oacute;n",
                                                width: 200,
                                                sortable: true,
                                                dataIndex: "descripcion"
                                                ,// use shorthand alias defined above
                                                editor: {allowBlank: false}
                                            }, {
                                                header: "Modelo",
                                                width: 150,
                                                sortable: true,
                                                //renderer: Ext.util.Format.usMoney,
                                                //renderer: Ext.util.Format.dateRenderer('Y-m-d')
                                                dataIndex: "modelo"
                                                ,editor: {allowBlank: false}
                                            }, {
                                                header: "Agencia",
                                                width: 150,
                                                sortable: true,
                                                dataIndex: "agencia",
                                                editor: comboAgencias, // specify reference to combo instance
                                                renderer: Ext.util.Format.comboRenderer(comboAgencias) // pass combo instance to reusable renderer
                                                
                                            }, {
                                                header: "estado",
                                                width: 150,
                                                sortable: true,
                                                dataIndex: "estado",
                                                editor: comboEstadoV,
                                                renderer: Ext.util.Format.comboRenderer(comboEstadoV) // pass combo instance to reusable renderer
                                            
                                            }, {
                                                header: "KM",
                                                width: 70,
                                                sortable: true,
                                                dataIndex: "kilometros"
                                                ,editor: {allowBlank: false}
                                            }
                                            , {
                                                header: "CAT",
                                                width: 70,
                                                sortable: true,
                                                dataIndex: "modelo_cal"
                                                ,editor: {allowBlank: false}
                                            }
                                            , {
                                                header: "Fec. Devoluci&oacute;n",
                                                width: 150,
                                                sortable: true,
                                                dataIndex: "fecha_hora_devolucion",
                                                renderer: Ext.util.Format.dateRenderer('d/m/Y H:i')
                                                ,editor: new fm.DateField({
                                                    format: 'd/m/Y H:i',
                                                    minValue: '01/01/2014 15:30',
                                                    disabledDays: [0, 6],
                                                    disabledDaysText: 'Plants are not available on the weekends'
                                                })
                                            }, {
                                                header: "Fec. Salida",
                                                width: 150,
                                                sortable: true,
                                                dataIndex: "fecha_hora_salida",
                                                renderer: Ext.util.Format.dateRenderer('d/m/Y H:i')
                                                ,editor: new fm.DateField({
                                                    format: 'd/m/Y H:i',
                                                    minValue: '01/01/2014 15:30',
                                                    disabledDays: [0, 6],
                                                    disabledDaysText: 'Plants are not available on the weekends'
                                                })                                            
                                            }, {
                                                header: "Fec. Retorno",
                                                width: 150,
                                                sortable: true,
                                                dataIndex: "fecha_hora_retorno",
                                                renderer: Ext.util.Format.dateRenderer('d/m/Y H:i')
                                                ,editor: new fm.DateField({
                                                    format: 'd/m/Y H:i',
                                                    minValue: '01/01/2014 15:30',
                                                    disabledDays: [0, 6],
                                                    disabledDaysText: 'Plants are not available on the weekends'
                                                })
                                            }, {
                                                header: "Dominio",
                                                width: 70,
                                                sortable: true,
                                                dataIndex: "dominio"
                                                ,editor: {minLength : 6, maxLength : 6}
                                            }, {
                                                header: "De Baja",
                                                width: 70,
                                                sortable: true,
                                                dataIndex: "debaja",
                                                editor: comboDeBaja,
                                                renderer: Ext.util.Format.comboRenderer(comboDeBaja) // pass combo instance to reusable renderer
                                            }
                            ]),

                        viewConfig: {
                            forceFit:true
                        },
                        //autoExpandColumn:'company',

                        tbar:[{
                                text:'Insertar un registro',
                                
                                handler: function(){
                                    var e = new vehiculo();
                                    editorVehiculo.stopEditing();
                                    Ext.grid.dataVehiculo.insert(0, e);
                                    //grillaVehiculo.getView().refresh();
                                    //grillaVehiculo.getSelectionModel().selectRow(0);
                                    //editorVehiculo.startEditing(0);
                                }
                                ,
                                tooltip:'Nuevo',
                                iconCls:'add'
                            }, '-',
                            {
                                ref: '../removeBtn',
                                text:'Eliminar un registro',
                                tooltip:'Elimina el registro seleccionado',
                                iconCls:'remove',
                                disabled:true,
                                handler: function(){
                                                /*
                                                editor.stopEditing();
                                                var s = miGrilla.getSelectionModel().getSelections();
                                                for(var i = 0, r; r = s[i]; i++){
                                                    datar.remove(r);
                                                }
                                                */
                                                Ext.Msg.alert('Eliminar un registro', 'En esta vista para eliminar un registro debe darlo de baja');
                                            }
                            }]
                    });

    grillaVehiculo.getSelectionModel().on('selectionchange', function(sm){
        grillaVehiculo.removeBtn.setDisabled(sm.getCount() < 1);
    });


}


cargarVehiculoObj();
//cargargrillaVehiculo();
grillaVehiculo.removeBtn.setDisabled(false);

function formatDate(value)
{
    return value ? value.dateFormat('d/m/Y H:i') : '';
}







// Sample desktop configuration
MyDesktop = new Ext.app.App({
    init :function(){
        Ext.QuickTips.init();
    },

    getModules : function(){
        return [
            new MyDesktop.GridWindow(),

            
            new MyDesktop.BogusMenuModule(),
            new MyDesktop.BogusModule()
        ];
    },

    // config for the start menu
    getStartConfig : function(){
        return {
            title: 'Jack Slocum',
            iconCls: 'user',
            toolItems: [{
                text:'Settings',
                iconCls:'settings',
                scope:this
            },'-',{
                text:'Logout',
                iconCls:'logout',
                scope:this
            }]
        };
    }
});


MyDesktop.GridWindow = Ext.extend(Ext.app.Module, {
    id:'grid-win',
    init : function(){
        this.launcher = {
            text: 'Grid Window',
            iconCls:'icon-grid',
            handler : this.createWindow,
            scope: this
        }
    },

    createWindow : function(){
        var desktop = this.app.getDesktop();
        var win = desktop.getWindow('grid-win');
        if(!win){
            win = desktop.createWindow({
                id: 'grid-win',
                title:'Grid Window',
                width:740,
                height:480,
                iconCls: 'icon-grid',
                shim:false,
                animCollapse:false,
                constrainHeader:true,
                layout: 'fit',
                items: [grillaVehiculo],
                listeners: {
                            beforeshow: function( window ) 
                            {          
                                //vuelvo a cargar los valores porque al cerrarse la ventana
                                //causa que todos los objetos en cascada desaparezcan.
                                cargarVehiculoObj();
                                
                                Ext.grid.dataVehiculo.reload();
                            }
                        }
                    
            });
        }
        win.show();
    }
});





// for example purposes
var windowIndex = 0;


MyDesktop.BogusModule = Ext.extend(Ext.app.Module, {
    init : function(){
        this.launcher = {
            text: 'Window '+(++windowIndex),
            iconCls:'bogus',
            handler : this.createWindow,
            scope: this,
            windowId:windowIndex
        }
    },

    createWindow : function(src){
        var desktop = this.app.getDesktop();
        var win = desktop.getWindow('bogus'+src.windowId);
        if(!win){
            win = desktop.createWindow({
                id: 'bogus'+src.windowId,
                title:src.text,
                width:640,
                height:480,
                html : '<p>Something useful would be in here.</p>',
                iconCls: 'bogus',
                shim:false,
                animCollapse:false,
                constrainHeader:true
            });
        }
        win.show();
    }
});


MyDesktop.BogusMenuModule = Ext.extend(MyDesktop.BogusModule, {
    init : function(){
        this.launcher = {
            text: 'Bogus Submenu',
            iconCls: 'bogus',
            handler: function() {
                return false;
            },
            menu: {
                items:[{
                    text: 'Bogus Window '+(++windowIndex),
                    iconCls:'bogus',
                    handler : this.createWindow,
                    scope: this,
                    windowId: windowIndex
                    },{
                    text: 'Bogus Window '+(++windowIndex),
                    iconCls:'bogus',
                    handler : this.createWindow,
                    scope: this,
                    windowId: windowIndex
                    },{
                    text: 'Bogus Window '+(++windowIndex),
                    iconCls:'bogus',
                    handler : this.createWindow,
                    scope: this,
                    windowId: windowIndex
                    },{
                    text: 'Bogus Window '+(++windowIndex),
                    iconCls:'bogus',
                    handler : this.createWindow,
                    scope: this,
                    windowId: windowIndex
                    },{
                    text: 'Bogus Window '+(++windowIndex),
                    iconCls:'bogus',
                    handler : this.createWindow,
                    scope: this,
                    windowId: windowIndex
                }]
            }
        }
    }
});




