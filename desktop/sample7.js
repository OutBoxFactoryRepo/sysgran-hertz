                    



/*!
 * Ext JS Library 3.1.0
 * Copyright(c) 2006-2009 Ext JS, LLC
 * licensing@extjs.com
 * http://www.extjs.com/license
 */

var storeVehiculo = null;
var comboVehiculo = null;
var arraySiNo = null;
var comboDeBaja = null;
//var editorOt = null;
//var grillaOt = null;
var Ot = null;
var fm = Ext.form;

function cargarDataOT()
{
    Ext.grid.dataOt = new Ext.data.JsonStore(
    {
            // store configs
            autoDestroy: false,
            autoLoad:true,
            url: 'http://local.hertz/web/app_dev.php/ordentrabajo/search/',
            storeId: 'myStore',
            api:
            {
                create : 'http://local.hertz/web/app_dev.php/ordentrabajo/crear/',
                read : 'http://local.hertz/web/app_dev.php/ordentrabajo/search/',
                update: 'http://local.hertz/web/app_dev.php/ordentrabajo/editar/'
                //,destroy: '../web/app_dev.php/vehiculo/delete/'
            },
            writer: 
                {
                    type: 'post',
                    writeAllFields : true,  //just send changed fields
                    allowSingle :false,     //always wrap in an array
                    encode:false,
                },
                reader: {
                    root: "Violations",
                    type: "json",
                    messageProperty : 'message' //without this, it doesn't work
                },
            // reader configs
            sortInfo: {field: 'id', direction: 'DESC'},
            idProperty: 'id',
            fields: [
                   { name: 'id',type:'integer' },
                   { name: 'descripcion', type: 'string' },
                   { name: 'dominio', type: 'string' ,mapping: 'vehiculo.id'},
                   { name: 'fecha_carga', type: 'date'},
                   { name: 'debaja', type: 'boolean'}]
    });

}

    cargarDataOT();

  var editorOt = new Ext.ux.grid.RowEditor({
    id:"editorOt",
    saveText  : "Guardar",
    cancelText: "Cancelar",
    //clicksToEdit: 1, //this changes from the default double-click activation to single click activation
    errorSummary: false //disables display of validation messages if the row is invalid
    });
  

 



function cargarOtObj()
{


    editorOt.on(
    {
        canceledit: function(roweditor, changes)
        {
            Ext.grid.dataOt.reload();
        },
        validateedit: function(roweditor, changes, record, rowIndex)
        {
            console.log(this.isValid());
            if (this.editing && !this.isValid()) 
            {
                return false;
            }
            else
            {
                record.commit();
            }
        },
        afteredit: function(roweditor, changes, record, rowIndex) 
        {
            Ext.grid.dataOt.reload();
        }
    });

  Ot = Ext.data.Record.create(
        [
                       { name: 'id',type:'integer' },
                       { name: 'descripcion', type: 'string' },
                       { name: 'dominio', type: 'string', mapping:"vehiculo"},
                       { name: 'fecha_carga', type: 'date', dateFormat:'d/m/Y'},
                       { name: 'debaja', type: 'boolean'}
        ]);

    storeVehiculo = new Ext.data.JsonStore({
        url: 'http://local.hertz/web/app_dev.php/vehiculo/search/',
        root: '',
        fields: ['id', 'dominio']
    });
    storeVehiculo.load();

    comboVehiculo = new Ext.form.ComboBox({
        typeAhead: true,
        triggerAction: 'all',
        lazyRender:true,
        mode: 'local',
        store: storeVehiculo,
        valueField: 'id',
        displayField: 'dominio'
    });

    arraySiNo = new Ext.data.ArrayStore({
        id: 0,
        fields: [
            'myId',
            'displayText'
        ],
        data: [[false, 'Alta'], [true, 'De baja']]
    });


    comboDeBaja = new Ext.form.ComboBox({
        typeAhead: true,
        triggerAction: 'all',
        lazyRender:true,
        mode: 'local',
        store: arraySiNo,
        valueField: 'myId',
        displayField: 'displayText'
    });


    

    Ext.util.Format.comboRenderer = function(combo)
    {
        return function(value)
        {
            var record = combo.findRecord(combo.valueField, value);
            return record ? record.get(combo.displayField) : combo.valueNotFoundText;
        }
    };

  

 
}


cargarOtObj();
//cargarGrillaOt();

var   grillaOt = new Ext.grid.GridPanel({
                        border:false,
                        id:'grillaOt',
                        plugins: editorOt,
                        ds: Ext.grid.dataOt,
                        cm: new Ext.grid.ColumnModel([
                                   {
                                                header: "ID",
                                                sortable: true,
                                                width: 50,
                                                dataIndex: "id"
                                            }, {
                                                header: "Descripci&oacute;n",
                                                width: 200,
                                                sortable: true,
                                                dataIndex: "descripcion"
                                                ,// use shorthand alias defined above
                                                editor: {allowBlank: false}
                                            }, {
                                                header: "Dominio",
                                                width: 150,
                                                sortable: true,
                                                dataIndex: "dominio",
                                                editor: comboVehiculo, // specify reference to combo instance
                                                renderer: Ext.util.Format.comboRenderer(comboVehiculo) // pass combo instance to reusable renderer
                                            }, {
                                                header: "Fec. de Carga",
                                                width: 150,
                                                sortable: true,
                                                dataIndex: "fecha_carga",
                                                renderer: Ext.util.Format.dateRenderer('d/m/Y')
                                                ,editor: new fm.DateField({
                                                    format: 'd/m/Y',
                                                    minValue: '01/01/2014'
                                                })
                                            }, {
                                                header: "De Baja",
                                                width: 70,
                                                sortable: true,
                                                dataIndex: "debaja",
                                                editor: comboDeBaja,
                                                renderer: Ext.util.Format.comboRenderer(comboDeBaja) // pass combo instance to reusable renderer
                                            }
                        ]),
                        viewConfig: 
                        {
                            forceFit:true
                        },
                        tbar:
                        [
                            {
                                text:'Insertar un registro',
                                
                                handler: function(){
                                    var e = new Ot();
                                    editorOt.stopEditing();
                                    Ext.grid.dataOt.insert(0, e);
                                    grillaOt.getView().refresh();
                                    grillaOt.getSelectionModel().selectRow(0);
                                    editorOt.startEditing(0);
                                }
                                ,
                                tooltip:'Nuevo',
                                iconCls:'add'
                            }, 
                            '-'
                            , 
                            {
                                ref: '../removeBtn',
                                text:'Eliminar un registro',
                                tooltip:'Elimina el registro seleccionado',
                                iconCls:'remove',
                                disabled:true,
                                handler: function(){
                                                /*
                                                editor.stopEditing();
                                                var s = miGrilla.getSelectionModel().getSelections();
                                                for(var i = 0, r; r = s[i]; i++){
                                                    datar.remove(r);
                                                }
                                                */
                                                Ext.Msg.alert('Eliminar un registro', 'En esta vista para eliminar un registro debe darlo de baja');
                                            }
                            }
                        ]
                    });

function formatDate(value)
{
    return value ? value.dateFormat('d/m/Y H:i') : '';
}







// Sample desktop configuration
MyDesktop = new Ext.app.App({
    init :function(){
        Ext.QuickTips.init();
    },

    getModules : function(){
        return [
            new MyDesktop.GridWindow(),

            
            new MyDesktop.BogusMenuModule(),
            new MyDesktop.BogusModule()
        ];
    },

    // config for the start menu
    getStartConfig : function(){
        return {
            title: 'Jack Slocum',
            iconCls: 'user',
            toolItems: [{
                text:'Settings',
                iconCls:'settings',
                scope:this
            },'-',{
                text:'Logout',
                iconCls:'logout',
                scope:this
            }]
        };
    }
});


MyDesktop.GridWindow = Ext.extend(Ext.app.Module, {
    id:'grid-win',
    init : function(){
        this.launcher = {
            text: 'Grid Window',
            iconCls:'icon-grid',
            handler : this.createWindow,
            scope: this
        }
    },

    createWindow : function(){
        var desktop = this.app.getDesktop();
        var win = desktop.getWindow('grid-win');
        if(!win){
            win = desktop.createWindow({
                id: 'grid-win',
                title:'Grid Window',
                width:740,
                height:480,
                iconCls: 'icon-grid',
                shim:false,
                animCollapse:false,
                constrainHeader:true,
                layout: 'fit',
                items: [grillaOt],
                listeners: {
                            beforeshow: function( window ) 
                            {          
                                //vuelvo a cargar los valores porque al cerrarse la ventana
                                //causa que todos los objetos en cascada desaparezcan.
                                //Ext.grid.dataOt = null;
                                //cargarGrillaOT();
                                cargarOtObj();
                            }
                        }
                    
            });
        }
        win.show();
    }
});





// for example purposes
var windowIndex = 0;


MyDesktop.BogusModule = Ext.extend(Ext.app.Module, {
    init : function(){
        this.launcher = {
            text: 'Window '+(++windowIndex),
            iconCls:'bogus',
            handler : this.createWindow,
            scope: this,
            windowId:windowIndex
        }
    },

    createWindow : function(src){
        var desktop = this.app.getDesktop();
        var win = desktop.getWindow('bogus'+src.windowId);
        if(!win){
            win = desktop.createWindow({
                id: 'bogus'+src.windowId,
                title:src.text,
                width:640,
                height:480,
                html : '<p>Something useful would be in here.</p>',
                iconCls: 'bogus',
                shim:false,
                animCollapse:false,
                constrainHeader:true
            });
        }
        win.show();
    }
});


MyDesktop.BogusMenuModule = Ext.extend(MyDesktop.BogusModule, {
    init : function(){
        this.launcher = {
            text: 'Bogus Submenu',
            iconCls: 'bogus',
            handler: function() {
                return false;
            },
            menu: {
                items:[{
                    text: 'Bogus Window '+(++windowIndex),
                    iconCls:'bogus',
                    handler : this.createWindow,
                    scope: this,
                    windowId: windowIndex
                    },{
                    text: 'Bogus Window '+(++windowIndex),
                    iconCls:'bogus',
                    handler : this.createWindow,
                    scope: this,
                    windowId: windowIndex
                    },{
                    text: 'Bogus Window '+(++windowIndex),
                    iconCls:'bogus',
                    handler : this.createWindow,
                    scope: this,
                    windowId: windowIndex
                    },{
                    text: 'Bogus Window '+(++windowIndex),
                    iconCls:'bogus',
                    handler : this.createWindow,
                    scope: this,
                    windowId: windowIndex
                    },{
                    text: 'Bogus Window '+(++windowIndex),
                    iconCls:'bogus',
                    handler : this.createWindow,
                    scope: this,
                    windowId: windowIndex
                }]
            }
        }
    }
});



    grillaOt.getSelectionModel().on('selectionchange', function(sm){
        grillaOt.removeBtn.setDisabled(sm.getCount() < 1);
    });
