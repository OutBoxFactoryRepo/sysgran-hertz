<?php

namespace Hertz\ReservaBundle\Controller;

/**
 * This code has been auto-generated by the JMSDiExtraBundle.
 *
 * Manual changes to it will be lost.
 */
class EstadoVehiculoController__JMSInjector
{
    public static function inject($container) {
        $instance = new \Hertz\ReservaBundle\Controller\EstadoVehiculoController();
        $refProperty = new \ReflectionProperty('Hertz\\ReservaBundle\\Controller\\EstadoVehiculoController', 'searchEstadoVehiculoSvc');
        $refProperty->setAccessible(true);
        $refProperty->setValue($instance, $container->get('estadovehiculosearchservice.search', 1));
        $refProperty = new \ReflectionProperty('Hertz\\ReservaBundle\\Controller\\EstadoVehiculoController', 'EstadoVehiculoGetOneSvc');
        $refProperty->setAccessible(true);
        $refProperty->setValue($instance, $container->get('estadovehiculogetoneservice.getone', 1));
        $refProperty = new \ReflectionProperty('Hertz\\ReservaBundle\\Controller\\EstadoVehiculoController', 'EstadoVehiculoCrearSvc');
        $refProperty->setAccessible(true);
        $refProperty->setValue($instance, $container->get('estadovehiculocrearservice.crear', 1));
        return $instance;
    }
}
