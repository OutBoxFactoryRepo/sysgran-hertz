<?php

namespace Hertz\ReservaBundle\Controller;

/**
 * This code has been auto-generated by the JMSDiExtraBundle.
 *
 * Manual changes to it will be lost.
 */
class AdicionalController__JMSInjector
{
    public static function inject($container) {
        $instance = new \Hertz\ReservaBundle\Controller\AdicionalController();
        $refProperty = new \ReflectionProperty('Hertz\\ReservaBundle\\Controller\\AdicionalController', 'userSvc');
        $refProperty->setAccessible(true);
        $refProperty->setValue($instance, $container->get('user.manager', 1));
        $refProperty = new \ReflectionProperty('Hertz\\ReservaBundle\\Controller\\AdicionalController', 'searchAdicionalSvc');
        $refProperty->setAccessible(true);
        $refProperty->setValue($instance, $container->get('adicionalsearchservice.search', 1));
        $refProperty = new \ReflectionProperty('Hertz\\ReservaBundle\\Controller\\AdicionalController', 'AdicionalGetOneSvc');
        $refProperty->setAccessible(true);
        $refProperty->setValue($instance, $container->get('adicionalgetoneservice.getone', 1));
        $refProperty = new \ReflectionProperty('Hertz\\ReservaBundle\\Controller\\AdicionalController', 'AdicionalCrearSvc');
        $refProperty->setAccessible(true);
        $refProperty->setValue($instance, $container->get('adicionalcrearservice.crear', 1));
        return $instance;
    }
}
