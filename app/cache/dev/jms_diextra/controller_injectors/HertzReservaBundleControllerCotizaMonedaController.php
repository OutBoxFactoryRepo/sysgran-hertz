<?php

namespace Hertz\ReservaBundle\Controller;

/**
 * This code has been auto-generated by the JMSDiExtraBundle.
 *
 * Manual changes to it will be lost.
 */
class CotizaMonedaController__JMSInjector
{
    public static function inject($container) {
        $instance = new \Hertz\ReservaBundle\Controller\CotizaMonedaController();
        $refProperty = new \ReflectionProperty('Hertz\\ReservaBundle\\Controller\\CotizaMonedaController', 'crearCotizaMonedaSvc');
        $refProperty->setAccessible(true);
        $refProperty->setValue($instance, $container->get('cotizamonedacrearservice.crear', 1));
        $refProperty = new \ReflectionProperty('Hertz\\ReservaBundle\\Controller\\CotizaMonedaController', 'searchCotizaMonedaSvc');
        $refProperty->setAccessible(true);
        $refProperty->setValue($instance, $container->get('cotizamonedasearchservice.search', 1));
        $refProperty = new \ReflectionProperty('Hertz\\ReservaBundle\\Controller\\CotizaMonedaController', 'CotizaMonedaGetOneSvc');
        $refProperty->setAccessible(true);
        $refProperty->setValue($instance, $container->get('cotizamonedagetoneservice.getone', 1));
        return $instance;
    }
}
