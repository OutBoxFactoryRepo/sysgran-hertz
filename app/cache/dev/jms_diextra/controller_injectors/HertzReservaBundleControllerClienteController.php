<?php

namespace Hertz\ReservaBundle\Controller;

/**
 * This code has been auto-generated by the JMSDiExtraBundle.
 *
 * Manual changes to it will be lost.
 */
class ClienteController__JMSInjector
{
    public static function inject($container) {
        $instance = new \Hertz\ReservaBundle\Controller\ClienteController();
        $refProperty = new \ReflectionProperty('Hertz\\ReservaBundle\\Controller\\ClienteController', 'searchClienteSvc');
        $refProperty->setAccessible(true);
        $refProperty->setValue($instance, $container->get('clientesearchservice.search', 1));
        $refProperty = new \ReflectionProperty('Hertz\\ReservaBundle\\Controller\\ClienteController', 'searchNombresSvc');
        $refProperty->setAccessible(true);
        $refProperty->setValue($instance, $container->get('clientesearchservice.getbynombres', 1));
        $refProperty = new \ReflectionProperty('Hertz\\ReservaBundle\\Controller\\ClienteController', 'searchClienteNombreSvc');
        $refProperty->setAccessible(true);
        $refProperty->setValue($instance, $container->get('clientesearchservice.getclientebynombre', 1));
        $refProperty = new \ReflectionProperty('Hertz\\ReservaBundle\\Controller\\ClienteController', 'ClienteGetOneSvc');
        $refProperty->setAccessible(true);
        $refProperty->setValue($instance, $container->get('clientegetoneservice.getone', 1));
        $refProperty = new \ReflectionProperty('Hertz\\ReservaBundle\\Controller\\ClienteController', 'ClienteCrearSvc');
        $refProperty->setAccessible(true);
        $refProperty->setValue($instance, $container->get('clientecrearservice.crear', 1));
        return $instance;
    }
}
