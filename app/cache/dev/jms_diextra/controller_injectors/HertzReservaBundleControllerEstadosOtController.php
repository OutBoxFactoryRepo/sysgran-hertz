<?php

namespace Hertz\ReservaBundle\Controller;

/**
 * This code has been auto-generated by the JMSDiExtraBundle.
 *
 * Manual changes to it will be lost.
 */
class EstadosOtController__JMSInjector
{
    public static function inject($container) {
        $instance = new \Hertz\ReservaBundle\Controller\EstadosOtController();
        $refProperty = new \ReflectionProperty('Hertz\\ReservaBundle\\Controller\\EstadosOtController', 'userSvc');
        $refProperty->setAccessible(true);
        $refProperty->setValue($instance, $container->get('user.manager', 1));
        $refProperty = new \ReflectionProperty('Hertz\\ReservaBundle\\Controller\\EstadosOtController', 'searchEstadosOtSvc');
        $refProperty->setAccessible(true);
        $refProperty->setValue($instance, $container->get('estadosotsearchservice.search', 1));
        $refProperty = new \ReflectionProperty('Hertz\\ReservaBundle\\Controller\\EstadosOtController', 'EstadosOtGetOneSvc');
        $refProperty->setAccessible(true);
        $refProperty->setValue($instance, $container->get('estadosotgetoneservice.getone', 1));
        return $instance;
    }
}
