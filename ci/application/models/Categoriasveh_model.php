<?php

class Categoriasveh_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();

        $this->load->database();
    }

    public function getCategoria($id)
    {
        $this->db->where("id", $id);

        $query = $this->db->get('categoriasveh');

        return $query->row_array();
    }
}
