<?php

class Vehiculos_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();

        $this->load->database();
    }

    public function getAll()
    {
        $query = $this->db->query("
            select distinct vehiculos.descripcion, modelo,
                vehiculos.id as id,
                colores.descripcion as colornombre
                ,colores.color as colorhexadecimal
                ,vehiculos.color
                ,categoria
                ,catv.descripcion as catvehdescrip
                from vehiculos
                left join colores
                on colores.id = vehiculos.color
                left join categoriasveh as catv
                on catv.id = vehiculos.categoria
                where vehiculos.debaja = false
                order by vehiculos.descripcion;
        ");

        return $query->result_array();
    }

    public function getDisponibilidad($agenciaorigen, $fechahorasalida, $fechahoradevolucion)
    {
        $agencia = '';
        $agencia2 = "";
        if ($agenciaorigen != 1)
        {
            $agencia = " and reservas.agenciaorigen = $agenciaorigen ";
            $agencia2 = "and vehiculos.agencia = ".$agenciaorigen;
        }
        
        
     
        
        $query = $this->db->query("
            select distinct vehiculos.descripcion, modelo,
                vehiculos.id as id,
                colores.descripcion as colornombre
                ,colores.color as colorhexadecimal
                ,vehiculos.color
                ,categoria
                ,catv.descripcion as catvehdescrip
                from vehiculos
                left join colores
                on colores.id = vehiculos.color
                left join categoriasveh as catv
                on catv.id = vehiculos.categoria
                where vehiculos.debaja = false AND 
                vehiculos.id NOT IN (
                     select distinct vehiculos.id as id from vehiculos join reservas on reservas.vehiculo = vehiculos.id
                        where (( reservas.fechahorasalida <= '$fechahorasalida' and '$fechahorasalida' <= reservas.fechahoradevolucion ) OR 
                              ( reservas.fechahorasalida <= '$fechahoradevolucion' and '$fechahoradevolucion' <= reservas.fechahoradevolucion ) OR 
                              ( '$fechahorasalida' <= reservas.fechahorasalida and reservas.fechahoradevolucion  <= '$fechahoradevolucion' ))  
                              $agencia
                )
                $agencia2
                order by vehiculos.descripcion;
        ");

        return $query->result_array();
    }
}

?>