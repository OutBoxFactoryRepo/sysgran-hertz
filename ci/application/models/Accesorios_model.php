<?php

class Accesorios_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();

        $this->load->database();
    }

    public function getAccesoriosByIdVehiculo($id)
    {
        $this->db->select('id, debaja, descripcion');
        $this->db->where("vehiculo_id", $id);

        $this->db->join("vehiculo_accesorios", 'vehiculo_accesorios.accesorios_id = accesorios.id');

        $query = $this->db->get('accesorios');

        return $query->result_array();
    }
}
