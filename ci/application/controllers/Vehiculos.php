<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vehiculos extends CI_Controller {
	
	public function index()
	{
		$this->load->model("vehiculos_model");
		$this->load->model("accesorios_model");

		$vehiculos = $this->vehiculos_model->getAll();

		foreach ($vehiculos as &$vehiculo) {
			$vehiculo['accesorios'] = $this->accesorios_model->getAccesoriosByIdVehiculo($vehiculo['id']);
		}

		echo json_encode($vehiculos);
	}
}

