<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class vehiculosdisp extends CI_Controller
{

    public function index()
    {
        $this->load->model("vehiculos_model");
        $this->load->model("accesorios_model");

//        $Id_sucursal_origen = 1;
//        $fecha_ini = '22/03/2017';
//        $fecha_fin = '25/06/2017';
//        $hora_ini = "01:00";
//        $hora_fin = "23:00";

        $Id_sucursal_origen = $this->input->post_get('agenciaOrigen');
        $fecha_ini = $this->input->post_get('fecha_ini');
        $fecha_fin = $this->input->post_get('fecha_fin');
        $hora_ini = $this->input->post_get('hora_ini');
        $hora_fin = $this->input->post_get('hora_fin');

        $vehiculos = $this->vehiculos_model->getDisponibilidad($Id_sucursal_origen, $this->convertirFechaHora($fecha_ini, $hora_ini), $this->convertirFechaHora($fecha_fin, $hora_fin));

        foreach ($vehiculos as &$vehiculo) {
            $vehiculo['accesorios'] = $this->accesorios_model->getAccesoriosByIdVehiculo($vehiculo['id']);
        }

        echo json_encode($vehiculos);
    }

    /**
     * @param $fecha campo con formato dia/mes/año ejemplo 25/02/2017
     * @param $hora campo con formato 24 horas ejemplo 21:25
     * @return string retorna hora completa ejemplo 2017-02-25 21:25
     */
    private function convertirFechaHora($fecha, $hora)
    {
        $arrFecha = explode('/', $fecha);

        return $arrFecha[2] . '-' . $arrFecha[1] . '-' . $arrFecha[0] . ' ' . $hora;
    }
}
