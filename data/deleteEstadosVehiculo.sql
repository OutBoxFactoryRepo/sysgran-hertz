-- Elimina los estados En mantenimiento (4) y En viaje (5) de las tablas vehiculoestadoshistorico y estadosvehiculos

delete from vehiculoestadoshistorico where estadovehiculo = 4;
delete from vehiculoestadoshistorico where estadovehiculo = 5;
delete from estadosvehiculos where id = 4;
delete from estadosvehiculos where id = 5;