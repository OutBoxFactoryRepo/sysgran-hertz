-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tempo de Geração: 14/09/2015 às 00:16
-- Versão do servidor: 5.5.44-0ubuntu0.14.04.1
-- Versão do PHP: 5.5.9-1ubuntu4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de dados: `hertz`
--
CREATE DATABASE IF NOT EXISTS `hertz` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `hertz`;

-- --------------------------------------------------------

--
-- Estrutura para tabela `adicionales`
--

CREATE TABLE IF NOT EXISTS `adicionales` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fechaCarga` datetime NOT NULL,
  `usuario` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cantidad` int(11) NOT NULL,
  `debaja` tinyint(1) NOT NULL,
  `tipoAdicional` int(11) NOT NULL,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `valor` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_5E2AAC6A9EC6A516` (`tipoAdicional`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Tabela truncada antes do insert `adicionales`
--

TRUNCATE TABLE `adicionales`;
--
-- Fazendo dump de dados para tabela `adicionales`
--

INSERT INTO `adicionales` (`id`, `fechaCarga`, `usuario`, `cantidad`, `debaja`, `tipoAdicional`, `descripcion`, `valor`) VALUES
(1, '2015-08-25 00:00:00', 'admin', 0, 0, 1, 'Diaria', 14.37),
(2, '2015-08-19 00:00:00', 'admin', 0, 0, 1, 'Horas Extras', 14.37),
(3, '2015-08-20 00:00:00', 'admin', 0, 0, 1, 'Km Rodado', 17.68),
(4, '2015-08-20 00:00:00', 'admin', 0, 0, 2, 'Seguro Modificado', 200.5),
(5, '2015-08-20 16:09:10', 'admin', 0, 0, 2, 'Patente', 58.3),
(6, '2015-08-19 00:00:00', 'admin', 0, 0, 2, 'Poderes', 115.3),
(7, '2015-08-27 00:00:00', 'admin', 10, 0, 3, 'GPS', 2500),
(8, '2015-08-12 00:00:00', 'admin', 0, 0, 2, 'Nuevo registro de Prueba', 25.3),
(9, '2015-09-07 00:00:00', 'admin', 1, 0, 1, 'Nuevo registro de paginado', 0);

-- --------------------------------------------------------

--
-- Estrutura para tabela `agencias`
--

CREATE TABLE IF NOT EXISTS `agencias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `debaja` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Tabela truncada antes do insert `agencias`
--

TRUNCATE TABLE `agencias`;
--
-- Fazendo dump de dados para tabela `agencias`
--

INSERT INTO `agencias` (`id`, `descripcion`, `debaja`) VALUES
(1, 'Casa Matriz', 0),
(2, 'Sucursal Misiones', 0),
(3, 'Sucursal Mendoza', 0);

-- --------------------------------------------------------

--
-- Estrutura para tabela `cargos`
--

CREATE TABLE IF NOT EXISTS `cargos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `valor` double NOT NULL,
  `debaja` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

--
-- Tabela truncada antes do insert `cargos`
--

TRUNCATE TABLE `cargos`;
-- --------------------------------------------------------

--
-- Estrutura para tabela `clientes`
--

CREATE TABLE IF NOT EXISTS `clientes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipodoc` int(11) NOT NULL,
  `idpais` int(11) NOT NULL,
  `provincia` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `apellido` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fechaNac` datetime NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `numeroDoc` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `debaja` tinyint(1) NOT NULL,
  `domicilio` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `localidad` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `codigoPostal` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_50FE07D749275638` (`tipodoc`),
  KEY `IDX_50FE07D7AD92DF5F` (`idpais`),
  KEY `IDX_50FE07D7D39AF213` (`provincia`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=26 ;

--
-- Tabela truncada antes do insert `clientes`
--

TRUNCATE TABLE `clientes`;
--
-- Fazendo dump de dados para tabela `clientes`
--

INSERT INTO `clientes` (`id`, `tipodoc`, `idpais`, `provincia`, `nombre`, `apellido`, `fechaNac`, `email`, `numeroDoc`, `debaja`, `domicilio`, `localidad`, `codigoPostal`) VALUES
(1, 2, 1, 2, 'Osvaldo', 'Sevilla', '1974-09-07 00:00:00', 'a@abc.com', '222222222', 0, 'sdfasdfasdf', 'asdfasdfasdf', '1544'),
(2, 2, 1, 2, 'Osvaldo', 'Sevilla', '1974-09-07 00:00:00', 'a@abc.com', '222222222', 0, 'sdfasdfasdf', 'asdfasdfasdf', '1544'),
(3, 2, 1, 2, 'Osvaldo', 'Sevilla', '1974-09-07 00:00:00', 'a@abc.com', '222222222', 0, 'sdfasdfasdf', 'asdfasdfasdf', '1544'),
(4, 2, 1, 2, 'Osvaldo', 'Sevilla', '1974-09-07 00:00:00', 'a@abc.com', '222222222', 0, 'sdfasdfasdf', 'asdfasdfasdf', '1544'),
(5, 2, 1, 2, 'Osvaldo', 'Sevilla', '1974-09-07 00:00:00', 'a@abc.com', '222222222', 0, 'sdfasdfasdf', 'asdfasdfasdf', '1544'),
(6, 2, 1, 2, 'Osvaldo', 'Sevilla', '1974-09-07 00:00:00', 'a@abc.com', '222222222', 0, 'sdfasdfasdf', 'asdfasdfasdf', '1544'),
(7, 2, 1, 2, 'Osvaldo', 'Sevilla', '1974-09-07 00:00:00', 'a@abc.com', '222222222', 0, 'sdfasdfasdf', 'asdfasdfasdf', '1544'),
(8, 2, 1, 2, 'Osvaldo', 'Sevilla', '1974-09-07 00:00:00', 'a@abc.com', '222222222', 0, 'sdfasdfasdf', 'asdfasdfasdf', '1544'),
(9, 2, 1, 2, 'Osvaldo', 'Sevilla', '1974-09-07 00:00:00', 'a@abc.com', '222222222', 0, 'sdfasdfasdf', 'asdfasdfasdf', '1544'),
(10, 2, 1, 2, 'Osvaldo', 'Sevilla', '1974-09-07 00:00:00', 'a@abc.com', '222222222', 0, 'sdfasdfasdf', 'asdfasdfasdf', '1544'),
(11, 2, 1, 2, 'Osvaldo', 'Sevilla', '1974-09-07 00:00:00', 'a@abc.com', '222222222', 0, 'sdfasdfasdf', 'asdfasdfasdf', '1544'),
(12, 2, 1, 2, 'Osvaldo', 'Sevilla', '1974-09-07 00:00:00', 'a@abc.com', '222222222', 0, 'sdfasdfasdf', 'asdfasdfasdf', '1544'),
(13, 2, 1, 2, 'Osvaldo', 'Sevilla', '1974-09-07 00:00:00', 'a@abc.com', '222222222', 0, 'sdfasdfasdf', 'asdfasdfasdf', '1544'),
(14, 2, 1, 2, 'Osvaldo', 'Sevilla', '1974-09-07 00:00:00', 'a@abc.com', '222222222', 0, 'sdfasdfasdf', 'asdfasdfasdf', '1544'),
(15, 2, 1, 2, 'Osvaldo', 'Sevilla', '1974-09-07 00:00:00', 'a@abc.com', '222222222', 0, 'sdfasdfasdf', 'asdfasdfasdf', '1544'),
(16, 2, 1, 2, 'Osvaldo', 'Sevilla', '1974-09-07 00:00:00', 'a@abc.com', '222222222', 0, 'sdfasdfasdf', 'asdfasdfasdf', '1544'),
(17, 2, 1, 2, 'Osvaldo', 'Sevilla', '1974-09-07 00:00:00', 'a@abc.com', '222222222', 0, 'sdfasdfasdf', 'asdfasdfasdf', '1544'),
(18, 2, 1, 2, 'Osvaldo', 'Sevilla', '1974-09-07 00:00:00', 'a@abc.com', '222222222', 0, 'sdfasdfasdf', 'asdfasdfasdf', '1544'),
(19, 2, 1, 2, 'Osvaldo', 'Sevilla', '1974-09-07 00:00:00', 'a@abc.com', '222222222', 0, 'sdfasdfasdf', 'asdfasdfasdf', '1544'),
(20, 2, 1, 2, 'Osvaldo', 'Sevilla', '1974-09-07 00:00:00', 'a@abc.com', '222222222', 0, 'sdfasdfasdf', 'asdfasdfasdf', '1544'),
(21, 2, 1, 2, 'Osvaldo', 'Sevilla', '1974-09-07 00:00:00', 'a@abc.com', '222222222', 0, 'sdfasdfasdf', 'asdfasdfasdf', '1544'),
(22, 2, 1, 2, 'Osvaldo', 'Sevilla', '1974-09-07 00:00:00', 'a@abc.com', '222222222', 0, 'sdfasdfasdf', 'asdfasdfasdf', '1544'),
(23, 2, 1, 2, 'Osvaldo', 'Sevilla', '1974-09-07 00:00:00', 'a@abc.com', '222222222', 0, 'sdfasdfasdf', 'asdfasdfasdf', '1544'),
(24, 2, 1, 2, 'Osvaldo', 'Sevilla', '1974-09-07 00:00:00', 'a@abc.com', '222222222', 0, 'sdfasdfasdf', 'asdfasdfasdf', '1544'),
(25, 2, 1, 2, 'Osvaldo', 'Sevilla', '1974-09-07 00:00:00', 'a@abc.com', '222222222', 0, 'sdfasdfasdf', 'asdfasdfasdf', '1544');

-- --------------------------------------------------------

--
-- Estrutura para tabela `clientesOtrosDatos`
--

CREATE TABLE IF NOT EXISTS `clientesOtrosDatos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idcliente` int(11) NOT NULL,
  `licencia` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `licFecVto` datetime NOT NULL,
  `nroTarjetaCre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tarjFecVto` datetime NOT NULL,
  `cAut` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cs` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cuit` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `nroIIBB` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `condicionIva` int(11) NOT NULL,
  `idIIBB` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_79A99CB78F951425` (`condicionIva`),
  KEY `IDX_79A99CB73141827F` (`idIIBB`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Tabela truncada antes do insert `clientesOtrosDatos`
--

TRUNCATE TABLE `clientesOtrosDatos`;
--
-- Fazendo dump de dados para tabela `clientesOtrosDatos`
--

INSERT INTO `clientesOtrosDatos` (`id`, `idcliente`, `licencia`, `licFecVto`, `nroTarjetaCre`, `tarjFecVto`, `cAut`, `cs`, `cuit`, `nroIIBB`, `condicionIva`, `idIIBB`) VALUES
(1, 25, '2222222222', '2016-01-01 00:00:00', '2222222222', '2018-01-01 00:00:00', '222222222222', '22222222', '23234382489', '222222222', 2, 1);

-- --------------------------------------------------------

--
-- Estrutura para tabela `cliente_tiposcliente`
--

CREATE TABLE IF NOT EXISTS `cliente_tiposcliente` (
  `tipocliente_id` int(11) NOT NULL,
  `cliente_id` int(11) NOT NULL,
  PRIMARY KEY (`tipocliente_id`,`cliente_id`),
  KEY `IDX_1D300B2E71815002` (`tipocliente_id`),
  KEY `IDX_1D300B2EDE734E51` (`cliente_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Tabela truncada antes do insert `cliente_tiposcliente`
--

TRUNCATE TABLE `cliente_tiposcliente`;
--
-- Fazendo dump de dados para tabela `cliente_tiposcliente`
--

INSERT INTO `cliente_tiposcliente` (`tipocliente_id`, `cliente_id`) VALUES
(1, 22),
(1, 23),
(1, 24),
(1, 25),
(2, 23),
(2, 24),
(2, 25),
(3, 23),
(3, 24),
(3, 25);

-- --------------------------------------------------------

--
-- Estrutura para tabela `condicionIva`
--

CREATE TABLE IF NOT EXISTS `condicionIva` (
  `id` int(11) NOT NULL,
  `tipo` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  `porcentaje` double NOT NULL,
  `descripcion` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `detalle` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `idCondIvaAfip` int(11) DEFAULT NULL,
  `idTipoAfip` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `debaja` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Tabela truncada antes do insert `condicionIva`
--

TRUNCATE TABLE `condicionIva`;
--
-- Fazendo dump de dados para tabela `condicionIva`
--

INSERT INTO `condicionIva` (`id`, `tipo`, `porcentaje`, `descripcion`, `detalle`, `idCondIvaAfip`, `idTipoAfip`, `debaja`) VALUES
(1, 'C', 0, 'Factura C', 'Cons. Final, Monotributist y Exento', 5, '011', 0),
(2, 'A', 0.21, 'Factura A', 'IVA discriminado', 5, '001', 0),
(3, 'B', 0.21, 'Factura B', 'Cons. Final, Monotributist y Exento', 5, '006', 0),
(4, 'R', 0, 'Recibos', 'Recibos "no suman"', NULL, NULL, 0),
(5, 'A', 0.27, 'Factura A (Servicios)', 'Iva discriminado de Servicios', 1, '001', 0),
(6, 'A', 0.105, 'Factura A (Bienes de Uso)', 'Iva discriminado para bienes de uso', 4, '001', 0),
(7, 'E', 0, 'Factura E (Exportacion)', 'Consumidor final exterior', 5, '019', 0),
(8, 'C', 0, 'Factura C2', 'Cons. Final, Monotributist y Exento', 5, '011', 0),
(9, 'N', 0, 'No Aplica', 'No Aplica', NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Estrutura para tabela `cotizamoneda`
--

CREATE TABLE IF NOT EXISTS `cotizamoneda` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idmoneda` int(11) NOT NULL,
  `fechaAlta` datetime DEFAULT NULL,
  `cotizacion` double NOT NULL,
  `cotizacionVenta` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_F9CF48F3D4B68681` (`idmoneda`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=49 ;

--
-- Tabela truncada antes do insert `cotizamoneda`
--

TRUNCATE TABLE `cotizamoneda`;
--
-- Fazendo dump de dados para tabela `cotizamoneda`
--

INSERT INTO `cotizamoneda` (`id`, `idmoneda`, `fechaAlta`, `cotizacion`, `cotizacionVenta`) VALUES
(37, 2, '2015-07-13 00:20:22', 1.01, 0),
(38, 2, '2015-07-13 00:20:31', 0.5, 0),
(39, 2, '2015-07-13 00:20:35', 1, 0),
(40, 7, '2015-07-13 00:21:33', 8.5, 0),
(41, 7, '2015-07-13 00:21:37', 9.5, 0),
(42, 7, '2015-07-13 00:21:39', 10.5, 0),
(43, 8, '2015-07-13 00:21:48', 10.5, 0),
(44, 8, '2015-07-13 00:21:57', 15, 0),
(45, 4, '2015-07-13 00:22:19', 120000.55, 0),
(46, 5, '2015-07-13 00:22:27', 1200000.55, 0),
(47, 4, '2015-07-15 15:14:47', 50000, 0),
(48, 5, '2015-07-15 15:15:22', 10000, 10021);

-- --------------------------------------------------------

--
-- Estrutura para tabela `estadosVehiculos`
--

CREATE TABLE IF NOT EXISTS `estadosVehiculos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `debaja` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Tabela truncada antes do insert `estadosVehiculos`
--

TRUNCATE TABLE `estadosVehiculos`;
--
-- Fazendo dump de dados para tabela `estadosVehiculos`
--

INSERT INTO `estadosVehiculos` (`id`, `descripcion`, `debaja`) VALUES
(1, 'Disponible', 0),
(2, 'En espera de trámites varios', 0),
(3, 'En Taller', 0),
(4, 'En mantenimiento', 0),
(5, 'En viaje', 0);

-- --------------------------------------------------------

--
-- Estrutura para tabela `metodosPago`
--

CREATE TABLE IF NOT EXISTS `metodosPago` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `debaja` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Tabela truncada antes do insert `metodosPago`
--

TRUNCATE TABLE `metodosPago`;
--
-- Fazendo dump de dados para tabela `metodosPago`
--

INSERT INTO `metodosPago` (`id`, `descripcion`, `debaja`) VALUES
(1, 'Efectivo', 0),
(2, 'Tarjeta de Débito', 0),
(3, 'Tarjeta de Crédito', 0);

-- --------------------------------------------------------

--
-- Estrutura para tabela `migration_versions`
--

CREATE TABLE IF NOT EXISTS `migration_versions` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Tabela truncada antes do insert `migration_versions`
--

TRUNCATE TABLE `migration_versions`;
--
-- Fazendo dump de dados para tabela `migration_versions`
--

INSERT INTO `migration_versions` (`version`) VALUES
('20150706223110'),
('20150711195755'),
('20150712182855'),
('20150712190945'),
('20150712204255'),
('20150712204802'),
('20150712212441'),
('20150712220126'),
('20150712235717'),
('20150713000316'),
('20150713000603'),
('20150713173921'),
('20150713175010'),
('20150714105750'),
('20150714125519'),
('20150714130155'),
('20150714140729'),
('20150714141410'),
('20150714142832'),
('20150714151452'),
('20150715132143'),
('20150715132609'),
('20150715150416'),
('20150715154144'),
('20150715163807'),
('20150715173300'),
('20150715173308'),
('20150715173638'),
('20150715180330'),
('20150715191719'),
('20150715202742'),
('20150715213013'),
('20150715213330'),
('20150715224133'),
('20150723175122'),
('20150723200902'),
('20150723202430'),
('20150723214002'),
('20150723214639'),
('20150723222023'),
('20150723222620'),
('20150724122007'),
('20150724123147'),
('20150724125632'),
('20150724145641'),
('20150724165641'),
('20150724193340'),
('20150725162736'),
('20150725203408'),
('20150725221241'),
('20150725221446'),
('20150726143652'),
('20150729142944'),
('20150810154816'),
('20150811155530'),
('20150811155802'),
('20150811160923'),
('20150811163613'),
('20150811164455'),
('20150819170959'),
('20150819173417'),
('20150819180828'),
('20150819182151'),
('20150819182244'),
('20150820205132'),
('20150902163423');

-- --------------------------------------------------------

--
-- Estrutura para tabela `ordenesTrabajo`
--

CREATE TABLE IF NOT EXISTS `ordenesTrabajo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vehiculo` int(11) NOT NULL,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fechaCarga` datetime NOT NULL,
  `usuario` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `debaja` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_8F8FDE69C9FA1603` (`vehiculo`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Tabela truncada antes do insert `ordenesTrabajo`
--

TRUNCATE TABLE `ordenesTrabajo`;
--
-- Fazendo dump de dados para tabela `ordenesTrabajo`
--

INSERT INTO `ordenesTrabajo` (`id`, `vehiculo`, `descripcion`, `fechaCarga`, `usuario`, `debaja`) VALUES
(1, 44, 'Orden de trabajo x b c', '2015-08-20 00:00:00', 'admin', 1),
(2, 47, 'orden 3', '2015-08-04 00:00:00', 'admin', 1),
(3, 46, 'Orden 4 b c', '2015-08-20 00:00:00', 'admin', 0),
(4, 45, 'Prueba de orden de trabajo 3 b 4', '2015-08-20 00:00:00', 'admin', 1),
(5, 45, 'abcd', '2015-08-27 00:00:00', 'admin', 0),
(6, 47, 'Nuevo registro b', '2015-09-14 00:00:00', 'admin', 0);

-- --------------------------------------------------------

--
-- Estrutura para tabela `ordenesTrabajoAuto`
--

CREATE TABLE IF NOT EXISTS `ordenesTrabajoAuto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vehiculo` int(11) NOT NULL,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `kilometros` int(11) NOT NULL,
  `ultimaActualizacion` int(11) NOT NULL,
  `fechaCarga` datetime NOT NULL,
  `usuario` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `debaja` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_C6AD3042C9FA1603` (`vehiculo`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Tabela truncada antes do insert `ordenesTrabajoAuto`
--

TRUNCATE TABLE `ordenesTrabajoAuto`;
--
-- Fazendo dump de dados para tabela `ordenesTrabajoAuto`
--

INSERT INTO `ordenesTrabajoAuto` (`id`, `vehiculo`, `descripcion`, `kilometros`, `ultimaActualizacion`, `fechaCarga`, `usuario`, `debaja`) VALUES
(1, 46, 'Cambio de aceite', 1000, 0, '2015-09-02 17:57:27', 'admin', 0);

-- --------------------------------------------------------

--
-- Estrutura para tabela `paises`
--

CREATE TABLE IF NOT EXISTS `paises` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `debaja` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Tabela truncada antes do insert `paises`
--

TRUNCATE TABLE `paises`;
--
-- Fazendo dump de dados para tabela `paises`
--

INSERT INTO `paises` (`id`, `descripcion`, `debaja`) VALUES
(1, 'Argentina', 0),
(2, 'Brasil', 0),
(3, 'Paraguay', 0),
(4, 'Uruguay', 0),
(5, 'Bolivia', 0),
(6, 'Chile', 0),
(7, 'Perú', 0),
(8, 'Otro', 0);

-- --------------------------------------------------------

--
-- Estrutura para tabela `permission`
--

CREATE TABLE IF NOT EXISTS `permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permission_idx` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=43 ;

--
-- Tabela truncada antes do insert `permission`
--

TRUNCATE TABLE `permission`;
--
-- Fazendo dump de dados para tabela `permission`
--

INSERT INTO `permission` (`id`, `name`, `description`) VALUES
(29, 'ROLE_PERMISSION_ADD', 'Permiso para crear permisos'),
(30, 'ROLE_PERMISSION_DELETE', 'Permiso para eliminar permisos'),
(31, 'ROLE_PERMISSION_SHOW', 'Permiso para ver el detalle de un permiso'),
(32, 'ROLE_PERMISSION_UPDATE', 'Permiso para actualizar permisos'),
(33, 'ROLE_ROLE_ADD', 'Permiso para crear roles'),
(34, 'ROLE_ROLE_DELETE', 'Permiso para eliminar un rol'),
(35, 'ROLE_ROLE_SHOW', 'Permiso para el detalle de un rol'),
(36, 'ROLE_ROLE_UPDATE', 'Permiso para actualizar roles'),
(37, 'ROLE_USER', 'Permiso básico'),
(38, 'ROLE_USER_ADD', 'Permiso para crear usuarios'),
(39, 'ROLE_USER_DELETE', 'Permiso para eliminar usuarios'),
(40, 'ROLE_USER_MANAGER', 'Permiso básico para la administración de usuarios'),
(41, 'ROLE_USER_SHOW', 'Permiso para detalle de usuario'),
(42, 'ROLE_USER_UPDATE', 'Permiso para modificar usuarios');

-- --------------------------------------------------------

--
-- Estrutura para tabela `provincias`
--

CREATE TABLE IF NOT EXISTS `provincias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `codigoIIBB` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `debaja` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=26 ;

--
-- Tabela truncada antes do insert `provincias`
--

TRUNCATE TABLE `provincias`;
--
-- Fazendo dump de dados para tabela `provincias`
--

INSERT INTO `provincias` (`id`, `descripcion`, `codigoIIBB`, `debaja`) VALUES
(1, 'Capital Federal', '901', 0),
(2, 'Buenos Aires', '902', 0),
(3, 'Catamarca', '903', 0),
(4, 'Córdoba', '904', 0),
(5, 'Corrientes', '905', 0),
(6, 'Chaco', '906', 0),
(7, 'Chubut', '907', 0),
(8, 'Entre Ríos', '908', 0),
(9, 'Formosa', '909', 0),
(10, 'Jujuy', '910', 0),
(11, 'La Pampa', '911', 0),
(12, 'La Rioja', '912', 0),
(13, 'Mendoza', '913', 0),
(14, 'Misiones', '914', 0),
(15, 'Neuquén', '915', 0),
(16, 'Río Negro', '916', 0),
(17, 'Salta', '917', 0),
(18, 'San Juan', '918', 0),
(19, 'San Luis', '919', 0),
(20, 'Santa Cruz', '920', 0),
(21, 'Santa Fe', '921', 0),
(22, 'Santiago del Estero', '922', 0),
(23, 'Tierra del Fuego', '923', 0),
(24, 'Tucumán', '924', 0),
(25, 'Otro', '', 0);

-- --------------------------------------------------------

--
-- Estrutura para tabela `reservas`
--

CREATE TABLE IF NOT EXISTS `reservas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(501) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

--
-- Tabela truncada antes do insert `reservas`
--

TRUNCATE TABLE `reservas`;
-- --------------------------------------------------------

--
-- Estrutura para tabela `role`
--

CREATE TABLE IF NOT EXISTS `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_57698A6A5E237E06` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Tabela truncada antes do insert `role`
--

TRUNCATE TABLE `role`;
--
-- Fazendo dump de dados para tabela `role`
--

INSERT INTO `role` (`id`, `name`, `description`, `created`, `modified`) VALUES
(5, 'Usuario', 'Permiso básico.', '2015-08-16 23:10:39', '2015-08-16 23:10:39');

-- --------------------------------------------------------

--
-- Estrutura para tabela `role_permission`
--

CREATE TABLE IF NOT EXISTS `role_permission` (
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`role_id`,`permission_id`),
  KEY `IDX_6F7DF886D60322AC` (`role_id`),
  KEY `IDX_6F7DF886FED90CCA` (`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Tabela truncada antes do insert `role_permission`
--

TRUNCATE TABLE `role_permission`;
--
-- Fazendo dump de dados para tabela `role_permission`
--

INSERT INTO `role_permission` (`role_id`, `permission_id`) VALUES
(5, 37),
(5, 40);

-- --------------------------------------------------------

--
-- Estrutura para tabela `tipoAdicional`
--

CREATE TABLE IF NOT EXISTS `tipoAdicional` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `usuario` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `debaja` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Tabela truncada antes do insert `tipoAdicional`
--

TRUNCATE TABLE `tipoAdicional`;
--
-- Fazendo dump de dados para tabela `tipoAdicional`
--

INSERT INTO `tipoAdicional` (`id`, `descripcion`, `usuario`, `debaja`) VALUES
(1, 'Gravado', 'admin', 0),
(2, 'No Gravado', 'admin', 0),
(3, 'Registra Stock', 'admin', 0);

-- --------------------------------------------------------

--
-- Estrutura para tabela `tipocliente`
--

CREATE TABLE IF NOT EXISTS `tipocliente` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `debaja` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Tabela truncada antes do insert `tipocliente`
--

TRUNCATE TABLE `tipocliente`;
--
-- Fazendo dump de dados para tabela `tipocliente`
--

INSERT INTO `tipocliente` (`id`, `descripcion`, `debaja`) VALUES
(1, 'Responsable', 0),
(2, 'Pagador', 0),
(3, 'Conductor Principal', 0),
(4, 'Conductor Adicional', 0),
(5, 'Pasajero', 0);

-- --------------------------------------------------------

--
-- Estrutura para tabela `tipodoc`
--

CREATE TABLE IF NOT EXISTS `tipodoc` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `debaja` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Tabela truncada antes do insert `tipodoc`
--

TRUNCATE TABLE `tipodoc`;
--
-- Fazendo dump de dados para tabela `tipodoc`
--

INSERT INTO `tipodoc` (`id`, `descripcion`, `debaja`) VALUES
(1, 'D.U.I', 0),
(2, 'Pasaporte', 0),
(3, 'C.I. Argentina', 0),
(4, 'Cédula Mercosur', 0);

-- --------------------------------------------------------

--
-- Estrutura para tabela `tipomoneda`
--

CREATE TABLE IF NOT EXISTS `tipomoneda` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `debaja` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Tabela truncada antes do insert `tipomoneda`
--

TRUNCATE TABLE `tipomoneda`;
--
-- Fazendo dump de dados para tabela `tipomoneda`
--

INSERT INTO `tipomoneda` (`id`, `descripcion`, `debaja`) VALUES
(1, 'No Aplica', 0),
(2, 'Peso Argentino', 0),
(3, 'Real Brasilero', 0),
(4, 'Peso Chileno', 0),
(5, 'Guaraní', 0),
(6, 'Sol Peruano', 0),
(7, 'Dolar EE.UU.', 0),
(8, 'Euro', 0);

-- --------------------------------------------------------

--
-- Estrutura para tabela `tiposIIBB`
--

CREATE TABLE IF NOT EXISTS `tiposIIBB` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `debaja` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Tabela truncada antes do insert `tiposIIBB`
--

TRUNCATE TABLE `tiposIIBB`;
--
-- Fazendo dump de dados para tabela `tiposIIBB`
--

INSERT INTO `tiposIIBB` (`id`, `descripcion`, `debaja`) VALUES
(1, 'No Aplica', 0),
(2, 'Convenio', 0),
(3, 'Local', 0),
(4, 'Exento', 0);

-- --------------------------------------------------------

--
-- Estrutura para tabela `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `deleted` datetime DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_super_admin` tinyint(1) DEFAULT NULL,
  `description` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_idx` (`user`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Tabela truncada antes do insert `user`
--

TRUNCATE TABLE `user`;
--
-- Fazendo dump de dados para tabela `user`
--

INSERT INTO `user` (`id`, `user`, `password`, `created`, `modified`, `deleted`, `status`, `email`, `name`, `last_name`, `is_super_admin`, `description`) VALUES
(7, 'admin', '$2y$12$MP64Q3U1Unn2RpUWg5FdMOZyBiZPuM6pxQpyk5VqZwMsOu6GHNqUq', '2015-08-16 23:10:39', '2015-08-16 23:10:39', NULL, 1, NULL, NULL, NULL, 1, NULL),
(8, 'user', '$2y$12$ag2PEfyLH86rS/dU88HR9OhdT2Xgf04S6KsXk7bmjJNgXklvWJW1i', '2015-08-16 23:10:40', '2015-08-16 23:10:40', NULL, 1, NULL, NULL, NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `user_role`
--

CREATE TABLE IF NOT EXISTS `user_role` (
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `IDX_2DE8C6A3A76ED395` (`user_id`),
  KEY `IDX_2DE8C6A3D60322AC` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Tabela truncada antes do insert `user_role`
--

TRUNCATE TABLE `user_role`;
--
-- Fazendo dump de dados para tabela `user_role`
--

INSERT INTO `user_role` (`user_id`, `role_id`) VALUES
(7, 5),
(8, 5);

-- --------------------------------------------------------

--
-- Estrutura para tabela `vehiculos`
--

CREATE TABLE IF NOT EXISTS `vehiculos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `modelo` int(11) NOT NULL,
  `kilometros` int(11) NOT NULL,
  `agencia` int(11) NOT NULL,
  `modeloCal` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `fechaHoraDevolucion` datetime DEFAULT NULL,
  `fechaHoraSalida` datetime NOT NULL,
  `fechaHoraRetorno` datetime NOT NULL,
  `debaja` tinyint(1) NOT NULL,
  `estado` int(11) NOT NULL,
  `dominio` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_82CE64A7EB6C2B99` (`agencia`),
  KEY `IDX_82CE64A7265DE1E3` (`estado`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=48 ;

--
-- Tabela truncada antes do insert `vehiculos`
--

TRUNCATE TABLE `vehiculos`;
--
-- Fazendo dump de dados para tabela `vehiculos`
--

INSERT INTO `vehiculos` (`id`, `descripcion`, `modelo`, `kilometros`, `agencia`, `modeloCal`, `fechaHoraDevolucion`, `fechaHoraSalida`, `fechaHoraRetorno`, `debaja`, `estado`, `dominio`) VALUES
(44, 'Vehículo de Prueba', 2000, 1000, 1, 'abcd', '2015-08-12 00:00:00', '2015-08-12 00:00:00', '2015-08-12 00:00:00', 0, 1, '789456'),
(45, 'Otro vehículo', 2015, 100, 1, 'abcd', '2015-08-21 00:00:00', '2015-08-21 00:00:00', '2015-08-21 00:00:00', 0, 3, 'CFK999'),
(46, 'Nuevo Vehículo de Prueba Mod', 1999, 1000, 2, '100', '2015-09-15 00:00:00', '2015-09-14 00:00:00', '2015-09-15 00:00:00', 0, 1, '123456'),
(47, 'Registro de alta de prueba modificado', 1955, 1000, 1, 'abcd', '2015-09-15 16:30:00', '2015-08-19 17:30:00', '2015-08-10 18:30:00', 0, 4, '456789');

--
-- Restrições para dumps de tabelas
--

--
-- Restrições para tabelas `adicionales`
--
ALTER TABLE `adicionales`
  ADD CONSTRAINT `FK_5E2AAC6A9EC6A516` FOREIGN KEY (`tipoAdicional`) REFERENCES `tipoAdicional` (`id`);

--
-- Restrições para tabelas `clientes`
--
ALTER TABLE `clientes`
  ADD CONSTRAINT `FK_50FE07D749275638` FOREIGN KEY (`tipodoc`) REFERENCES `tipodoc` (`id`),
  ADD CONSTRAINT `FK_50FE07D7AD92DF5F` FOREIGN KEY (`idpais`) REFERENCES `paises` (`id`),
  ADD CONSTRAINT `FK_50FE07D7D39AF213` FOREIGN KEY (`provincia`) REFERENCES `provincias` (`id`);

--
-- Restrições para tabelas `clientesOtrosDatos`
--
ALTER TABLE `clientesOtrosDatos`
  ADD CONSTRAINT `FK_79A99CB73141827F` FOREIGN KEY (`idIIBB`) REFERENCES `tiposIIBB` (`id`),
  ADD CONSTRAINT `FK_79A99CB78F951425` FOREIGN KEY (`condicionIva`) REFERENCES `condicionIva` (`id`);

--
-- Restrições para tabelas `cliente_tiposcliente`
--
ALTER TABLE `cliente_tiposcliente`
  ADD CONSTRAINT `FK_1D300B2E71815002` FOREIGN KEY (`tipocliente_id`) REFERENCES `tipocliente` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_1D300B2EDE734E51` FOREIGN KEY (`cliente_id`) REFERENCES `clientes` (`id`) ON DELETE CASCADE;

--
-- Restrições para tabelas `cotizamoneda`
--
ALTER TABLE `cotizamoneda`
  ADD CONSTRAINT `FK_F9CF48F3D4B68681` FOREIGN KEY (`idmoneda`) REFERENCES `tipomoneda` (`id`);

--
-- Restrições para tabelas `ordenesTrabajo`
--
ALTER TABLE `ordenesTrabajo`
  ADD CONSTRAINT `FK_8F8FDE69C9FA1603` FOREIGN KEY (`vehiculo`) REFERENCES `vehiculos` (`id`);

--
-- Restrições para tabelas `ordenesTrabajoAuto`
--
ALTER TABLE `ordenesTrabajoAuto`
  ADD CONSTRAINT `FK_C6AD3042C9FA1603` FOREIGN KEY (`vehiculo`) REFERENCES `vehiculos` (`id`);

--
-- Restrições para tabelas `role_permission`
--
ALTER TABLE `role_permission`
  ADD CONSTRAINT `FK_6F7DF886D60322AC` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_6F7DF886FED90CCA` FOREIGN KEY (`permission_id`) REFERENCES `permission` (`id`) ON DELETE CASCADE;

--
-- Restrições para tabelas `user_role`
--
ALTER TABLE `user_role`
  ADD CONSTRAINT `FK_2DE8C6A3A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_2DE8C6A3D60322AC` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE CASCADE;

--
-- Restrições para tabelas `vehiculos`
--
ALTER TABLE `vehiculos`
  ADD CONSTRAINT `FK_82CE64A7265DE1E3` FOREIGN KEY (`estado`) REFERENCES `estadosVehiculos` (`id`),
  ADD CONSTRAINT `FK_82CE64A7EB6C2B99` FOREIGN KEY (`agencia`) REFERENCES `agencias` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
