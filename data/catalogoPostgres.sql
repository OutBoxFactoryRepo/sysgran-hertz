﻿delete from paises;

INSERT INTO paises (id, descripcion, debaja) VALUES
(1, 'Argentina',false),
(2, 'Brasil',false),
(3, 'Paraguay',false),
(4, 'Uruguay',false),
(5, 'Bolivia',false),
(6, 'Chile',false),
(7, 'Perú',false),
(8, 'Otro',false);

delete from provincias;

INSERT INTO provincias (id, descripcion, codigoIIBB, debaja) VALUES
(1, 'Capital Federal', '901',false),
(2, 'Buenos Aires', '902',false),
(3, 'Catamarca', '903',false),
(4, 'Córdoba', '904',false),
(5, 'Corrientes', '905',false),
(6, 'Chaco', '906',false),
(7, 'Chubut', '907',false),
(8, 'Entre Ríos', '908',false),
(9, 'Formosa', '909',false),
(10, 'Jujuy', '910',false),
(11, 'La Pampa', '911',false),
(12, 'La Rioja', '912',false),
(13, 'Mendoza', '913',false),
(14, 'Misiones', '914',false),
(15, 'Neuquén', '915',false),
(16, 'Río Negro', '916',false),
(17, 'Salta', '917',false),
(18, 'San Juan', '918',false),
(19, 'San Luis', '919',false),
(20, 'Santa Cruz', '920',false),
(21, 'Santa Fe', '921',false),
(22, 'Santiago del Estero', '922',false),
(23, 'Tierra del Fuego', '923',false),
(24, 'Tucumán', '924',false),
(25, 'Otro', '',false);


delete from tiposiibb;


INSERT INTO tiposIIBB (id, descripcion, debaja) VALUES
(1, 'No Aplica', false),
(2, 'Convenio', false),
(3, 'Local', false),
(4, 'Exento', false);


delete from tipoDoc;

INSERT INTO tipodoc (id, descripcion, debaja) VALUES
(1, 'D.U.I',false),
(2, 'Pasaporte',false),
(3, 'C.I. Argentina',false),
(4, 'Cédula Mercosur',false);

delete from tipocliente;

INSERT INTO tipocliente (id, descripcion, debaja) VALUES
(1, 'Responsable', false),
(2, 'Pagador', false),
(3, 'Conductor Principal', false),
(4, 'Conductor Adicional', false),
(5, 'Pasajero', false);

delete from condicioniva;

INSERT INTO condicionIva (id, tipo, porcentaje, descripcion, detalle, idCondIvaAfip, idTipoAfip, debaja) VALUES
(1, 'C', 0, 'Factura C', 'Cons. Final, Monotributist y Exento', 5, '011', false),
(2, 'A', 0.21, 'Factura A', 'IVA discriminado', 5, '001', false),
(3, 'B', 0.21, 'Factura B', 'Cons. Final, Monotributist y Exento', 5, '006', false),
(4, 'R', 0, 'Recibos', 'Recibos "no suman"', NULL, NULL, false),
(5, 'A', 0.27, 'Factura A (Servicios)', 'Iva discriminado de Servicios', 1, '001', false),
(6, 'A', 0.105, 'Factura A (Bienes de Uso)', 'Iva discriminado para bienes de uso', 4, '001', false),
(7, 'E', 0, 'Factura E (Exportacion)', 'Consumidor final exterior', 5, '019', false),
(8, 'C', 0, 'Factura C2', 'Cons. Final, Monotributist y Exento', 5, '011', false),
(9, 'N', 0, 'No Aplica', 'No Aplica', NULL, NULL, false);

