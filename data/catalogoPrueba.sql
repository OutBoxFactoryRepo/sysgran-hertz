INSERT INTO `tipodoc` (`id`, `descripcion`, `debaja`) VALUES
(1, 'D.U.I', 0),
(2, 'Pasaporte', 0),
(3, 'C.I. Argentina', 0),
(4, 'Cédula Mercosur', 0);


INSERT INTO `tipomoneda` (`id`, `descripcion`, `debaja`) VALUES
(1, 'No Aplica', 0),
(2, 'Peso Argentino', 0),
(3, 'Real Brasilero', 0),
(4, 'Peso Chileno', 0),
(5, 'Guaraní', 0),
(6, 'Sol Peruano', 0),
(7, 'Dolar EE.UU.', 0),
(8, 'Euro', 0);

INSERT INTO `tiposIIBB` (`id`, `descripcion`, `debaja`) VALUES
(1, 'No Aplica', 0),
(2, 'Convenio', 0),
(3, 'Local', 0),
(4, 'Exento', 0);


INSERT INTO `tipocliente` (`id`, `descripcion`, `debaja`) VALUES
(1, 'Responsable', 0),
(2, 'Pagador', 0),
(3, 'Conductor Principal', 0),
(4, 'Conductor Adicional', 0),
(5, 'Pasajero', 0);



INSERT INTO `provincias` (`id`, `descripcion`, `codigoIIBB`, `debaja`) VALUES
(1, 'Capital Federal', '901', 0),
(2, 'Buenos Aires', '902', 0),
(3, 'Catamarca', '903', 0),
(4, 'Córdoba', '904', 0),
(5, 'Corrientes', '905', 0),
(6, 'Chaco', '906', 0),
(7, 'Chubut', '907', 0),
(8, 'Entre Ríos', '908', 0),
(9, 'Formosa', '909', 0),
(10, 'Jujuy', '910', 0),
(11, 'La Pampa', '911', 0),
(12, 'La Rioja', '912', 0),
(13, 'Mendoza', '913', 0),
(14, 'Misiones', '914', 0),
(15, 'Neuquén', '915', 0),
(16, 'Río Negro', '916', 0),
(17, 'Salta', '917', 0),
(18, 'San Juan', '918', 0),
(19, 'San Luis', '919', 0),
(20, 'Santa Cruz', '920', 0),
(21, 'Santa Fe', '921', 0),
(22, 'Santiago del Estero', '922', 0),
(23, 'Tierra del Fuego', '923', 0),
(24, 'Tucumán', '924', 0),
(25, 'Otro', '', 0);

INSERT INTO `paises` (`id`, `descripcion`, `debaja`) VALUES
(1, 'Argentina', 0),
(2, 'Brasil', 0),
(3, 'Paraguay', 0),
(4, 'Uruguay', 0),
(5, 'Bolivia', 0),
(6, 'Chile', 0),
(7, 'Perú', 0),
(8, 'Otro', 0);


INSERT INTO `metodosPago` (`id`, `descripcion`, `debaja`) VALUES
(1, 'Efectivo', 0),
(2, 'Tarjeta de Débito', 0),
(3, 'Tarjeta de Crédito', 0);


INSERT INTO `estadosVehiculos` (`id`, `descripcion`, `debaja`) VALUES
(1, 'Disponible', '0'),
(2, 'En espera de trámites varios', '0'),
(3, 'En Taller', '0'),
(4, 'En mantenimiento', '0'),
(5, 'En viaje', '0');


INSERT INTO `cotizamoneda` (`id`, `idmoneda`, `fechaAlta`, `cotizacion`, `cotizacionVenta`) VALUES
(37, 2, '2015-07-13 00:20:22', 1.01, 0),
(38, 2, '2015-07-13 00:20:31', 0.5, 0),
(39, 2, '2015-07-13 00:20:35', 1, 0),
(40, 7, '2015-07-13 00:21:33', 8.5, 0),
(41, 7, '2015-07-13 00:21:37', 9.5, 0),
(42, 7, '2015-07-13 00:21:39', 10.5, 0),
(43, 8, '2015-07-13 00:21:48', 10.5, 0),
(44, 8, '2015-07-13 00:21:57', 15, 0),
(45, 4, '2015-07-13 00:22:19', 120000.55, 0),
(46, 5, '2015-07-13 00:22:27', 1200000.55, 0),
(47, 4, '2015-07-15 15:14:47', 50000, 0),
(48, 5, '2015-07-15 15:15:22', 10000, 10021);


INSERT INTO `condicionIva` (`id`, `tipo`, `porcentaje`, `descripcion`, `detalle`, `idCondIvaAfip`, `idTipoAfip`, `debaja`) VALUES
(1, 'C', 0, 'Factura C', 'Cons. Final, Monotributist y Exento', 5, '011', 0),
(2, 'A', 0.21, 'Factura A', 'IVA discriminado', 5, '001', 0),
(3, 'B', 0.21, 'Factura B', 'Cons. Final, Monotributist y Exento', 5, '006', 0),
(4, 'R', 0, 'Recibos', 'Recibos "no suman"', NULL, NULL, 0),
(5, 'A', 0.27, 'Factura A (Servicios)', 'Iva discriminado de Servicios', 1, '001', 0),
(6, 'A', 0.105, 'Factura A (Bienes de Uso)', 'Iva discriminado para bienes de uso', 4, '001', 0),
(7, 'E', 0, 'Factura E (Exportacion)', 'Consumidor final exterior', 5, '019', 0),
(8, 'C', 0, 'Factura C2', 'Cons. Final, Monotributist y Exento', 5, '011', 0),
(9, 'N', 0, 'No Aplica', 'No Aplica', NULL, NULL, 0);


INSERT INTO `clientes` (`id`, `tipodoc`, `idpais`, `provincia`, `nombre`, `apellido`, `fechaNac`, `email`, `numeroDoc`, `debaja`, `domicilio`, `localidad`, `codigoPostal`) VALUES
(1, 2, 1, 2, 'Osvaldo', 'Sevilla', '1974-09-07 00:00:00', 'a@abc.com', '222222222', 0, 'sdfasdfasdf', 'asdfasdfasdf', '1544'),
(2, 2, 1, 2, 'Osvaldo', 'Sevilla', '1974-09-07 00:00:00', 'a@abc.com', '222222222', 0, 'sdfasdfasdf', 'asdfasdfasdf', '1544'),
(3, 2, 1, 2, 'Osvaldo', 'Sevilla', '1974-09-07 00:00:00', 'a@abc.com', '222222222', 0, 'sdfasdfasdf', 'asdfasdfasdf', '1544'),
(4, 2, 1, 2, 'Osvaldo', 'Sevilla', '1974-09-07 00:00:00', 'a@abc.com', '222222222', 0, 'sdfasdfasdf', 'asdfasdfasdf', '1544'),
(5, 2, 1, 2, 'Osvaldo', 'Sevilla', '1974-09-07 00:00:00', 'a@abc.com', '222222222', 0, 'sdfasdfasdf', 'asdfasdfasdf', '1544'),
(6, 2, 1, 2, 'Osvaldo', 'Sevilla', '1974-09-07 00:00:00', 'a@abc.com', '222222222', 0, 'sdfasdfasdf', 'asdfasdfasdf', '1544'),
(7, 2, 1, 2, 'Osvaldo', 'Sevilla', '1974-09-07 00:00:00', 'a@abc.com', '222222222', 0, 'sdfasdfasdf', 'asdfasdfasdf', '1544'),
(8, 2, 1, 2, 'Osvaldo', 'Sevilla', '1974-09-07 00:00:00', 'a@abc.com', '222222222', 0, 'sdfasdfasdf', 'asdfasdfasdf', '1544'),
(9, 2, 1, 2, 'Osvaldo', 'Sevilla', '1974-09-07 00:00:00', 'a@abc.com', '222222222', 0, 'sdfasdfasdf', 'asdfasdfasdf', '1544'),
(10, 2, 1, 2, 'Osvaldo', 'Sevilla', '1974-09-07 00:00:00', 'a@abc.com', '222222222', 0, 'sdfasdfasdf', 'asdfasdfasdf', '1544'),
(11, 2, 1, 2, 'Osvaldo', 'Sevilla', '1974-09-07 00:00:00', 'a@abc.com', '222222222', 0, 'sdfasdfasdf', 'asdfasdfasdf', '1544'),
(12, 2, 1, 2, 'Osvaldo', 'Sevilla', '1974-09-07 00:00:00', 'a@abc.com', '222222222', 0, 'sdfasdfasdf', 'asdfasdfasdf', '1544'),
(13, 2, 1, 2, 'Osvaldo', 'Sevilla', '1974-09-07 00:00:00', 'a@abc.com', '222222222', 0, 'sdfasdfasdf', 'asdfasdfasdf', '1544'),
(14, 2, 1, 2, 'Osvaldo', 'Sevilla', '1974-09-07 00:00:00', 'a@abc.com', '222222222', 0, 'sdfasdfasdf', 'asdfasdfasdf', '1544'),
(15, 2, 1, 2, 'Osvaldo', 'Sevilla', '1974-09-07 00:00:00', 'a@abc.com', '222222222', 0, 'sdfasdfasdf', 'asdfasdfasdf', '1544'),
(16, 2, 1, 2, 'Osvaldo', 'Sevilla', '1974-09-07 00:00:00', 'a@abc.com', '222222222', 0, 'sdfasdfasdf', 'asdfasdfasdf', '1544'),
(17, 2, 1, 2, 'Osvaldo', 'Sevilla', '1974-09-07 00:00:00', 'a@abc.com', '222222222', 0, 'sdfasdfasdf', 'asdfasdfasdf', '1544'),
(18, 2, 1, 2, 'Osvaldo', 'Sevilla', '1974-09-07 00:00:00', 'a@abc.com', '222222222', 0, 'sdfasdfasdf', 'asdfasdfasdf', '1544'),
(19, 2, 1, 2, 'Osvaldo', 'Sevilla', '1974-09-07 00:00:00', 'a@abc.com', '222222222', 0, 'sdfasdfasdf', 'asdfasdfasdf', '1544'),
(20, 2, 1, 2, 'Osvaldo', 'Sevilla', '1974-09-07 00:00:00', 'a@abc.com', '222222222', 0, 'sdfasdfasdf', 'asdfasdfasdf', '1544'),
(21, 2, 1, 2, 'Osvaldo', 'Sevilla', '1974-09-07 00:00:00', 'a@abc.com', '222222222', 0, 'sdfasdfasdf', 'asdfasdfasdf', '1544'),
(22, 2, 1, 2, 'Osvaldo', 'Sevilla', '1974-09-07 00:00:00', 'a@abc.com', '222222222', 0, 'sdfasdfasdf', 'asdfasdfasdf', '1544'),
(23, 2, 1, 2, 'Osvaldo', 'Sevilla', '1974-09-07 00:00:00', 'a@abc.com', '222222222', 0, 'sdfasdfasdf', 'asdfasdfasdf', '1544'),
(24, 2, 1, 2, 'Osvaldo', 'Sevilla', '1974-09-07 00:00:00', 'a@abc.com', '222222222', 0, 'sdfasdfasdf', 'asdfasdfasdf', '1544'),
(25, 2, 1, 2, 'Osvaldo', 'Sevilla', '1974-09-07 00:00:00', 'a@abc.com', '222222222', 0, 'sdfasdfasdf', 'asdfasdfasdf', '1544');


INSERT INTO `clientesOtrosDatos` (`id`, `idcliente`, `licencia`, `licFecVto`, `nroTarjetaCre`, `tarjFecVto`, `cAut`, `cs`, `cuit`, `nroIIBB`, `condicionIva`, `idIIBB`) VALUES
(1, 25, '2222222222', '2016-01-01 00:00:00', '2222222222', '2018-01-01 00:00:00', '222222222222', '22222222', '23234382489', '222222222', 2, 1);


INSERT INTO `cliente_tiposcliente` (`tipocliente_id`, `cliente_id`) VALUES
(1, 22),
(1, 23),
(1, 24),
(1, 25),
(2, 23),
(2, 24),
(2, 25),
(3, 23),
(3, 24),
(3, 25);


INSERT INTO `agencias` (`id`, `descripcion`, `debaja`) VALUES
(1, 'Casa Matriz', '0'),
(2, 'Sucursal Misiones', '0'),
(3, 'Sucursal Mendoza', '0');