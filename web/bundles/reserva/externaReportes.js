


	//Desktop

        MyDesktop.ExternaReportesTWindow = Ext.extend(Ext.app.Module, {
	    id:'externaReportes-win',
	    autoDestroy:false,
	    init : function(){
	
	        this.launcher = {
	            text: 'Reportes',
	            iconCls:'icon-grid',
	            handler : this.createWindow,
	            scope: this
	        }
	    },
	
	    createWindow : function(){
	        var desktop = this.app.getDesktop();
	        var win = desktop.getWindow('externaReportes-win');
	        if(!win){         
	            win = desktop.createWindow({
	                id: 'externaReportes-win',
	                title:'Reportes',
	                width:400,
	                height:400,
	                iconCls: 'icon-grid',
	                shim:false,
	                animCollapse:false,
	                constrainHeader:true,
                        html:"<iframe src='../../externo/reportes.php' style='width:100%;height:100%;border:none'></iframe>",
                        autoScroll:true,
	                layout: 'fit'	                
	            });
	        }
	        win.show();
	    
	    }
	});
