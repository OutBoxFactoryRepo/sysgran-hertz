var fm = Ext.form;
var Usuario = null;

Ext.grid.dataUsuario = new Ext.data.JsonStore(
                        {
                                // store configs
                        		autoDestroy: false,
                            	autoLoad: {params:{start: 0, limit: 5}},
                                url: '../usuarios/',
                                storeId: 'myStoreUsuario',
                                totalProperty: 'results',
                                root:'users',
                                remoteSort:false,
                                api:
                                {
                                    create : '../usuarios/crear/',
                                    read : '../usuarios/',
                                    update: '../usuarios/crear/'
                                    //,destroy: '../web/app_dev.php/vehiculo/delete/'
                                },
                                writer: 
                                    {
                                        type: 'post',
                                        writeAllFields : true,  //just send changed fields
                                        allowSingle :false,     //always wrap in an array
                                        encode:false,
                                    },
                                    reader: {
                                        root: "Violations",
                                        type: "json",
                                        messageProperty : 'message' //without this, it doesn't work
                                    },
                                // reader configs
                                sortInfo: {field: 'id', direction: 'DESC'},
                                idProperty: 'id',
                                fields: [
                                       { name: 'id',type:'integer' },
                                       { name: 'usuario', type: 'string',mapping: 'user' },
                                       { name: 'creado', type: 'date' ,mapping: 'created'},
                                       { name: 'modificado', type: 'date' ,mapping: 'modified'},
                                       { name: 'superadmin', type: 'boolean' ,mapping: 'is_super_admin'},
                                       { name: 'role', type: 'string' ,mapping: 'assigned_roles[0].id'},
                                       { name: 'agencia', type: 'string' ,mapping: 'sucursal'},
                                       { name: 'clave', type: 'string'},
                                       { name: 'debaja', type: 'boolean',mapping: 'status'}
                                       ]
                        });



var comboTipoUsuario = null;


var arraySiNo = null;
var comboDeBaja = null;
var editorUsuario = null;
var grillaUsuario = null;
var storeRoles = null;
var storeAgenciaUsr = null;
function cargarUsuarioObj()
{
    
	
    storeAgenciaUsr = new Ext.data.JsonStore({
        url: '../agencia/search/',
        root: '',
        fields: ['id', 'descripcion']
    });
    storeAgenciaUsr.load();
	
    comboAgencias = new Ext.form.ComboBox({
        typeAhead: true,
        triggerAction: 'all',
        lazyRender:true,
        mode: 'local',
        store: storeAgenciaUsr,
        valueField: 'id',
        displayField: 'descripcion',
        listeners: {
            // delete the previous query in the beforequery event or set
            // combo.lastQuery = null (this will reload the store the next time it expands)
            beforequery: function(qe){
                delete qe.combo.lastQuery;
            }
        }
    });


    Usuario = Ext.data.Record.create(
        [

           { name: 'id',type:'integer' }, 
           { name: 'usuario', type: 'string',mapping: 'user' },
           { name: 'creado', type: 'date' ,mapping: 'created'},
           { name: 'modificado', type: 'date' ,mapping: 'modified'},
           { name: 'superadmin', type: 'boolean' ,mapping: 'is_super_admin'},
           { name: 'role', type: 'string' ,mapping: 'assigned_roles[0].id'},
           { name: 'agencia', type: 'string' ,mapping: 'sucursal'},
           { name: 'clave', type: 'string'},
           { name: 'debaja', type: 'boolean',mapping: 'status'}

        ]);

   

    arraySiNo = new Ext.data.ArrayStore({
        id: 0,
        fields: [
            'myId',
            'displayText'
        ],
        data: [[true, 'Alta'], [false, 'De baja']]
    });

   arrayYesNo = new Ext.data.ArrayStore({
        id: 0,
        fields: [
            'myId',
            'displayText'
        ],
        data: [[true, 'Si'], [false, 'No']]
    });

    comboSuperAdmin = new Ext.form.ComboBox({
        typeAhead: true,
        triggerAction: 'all',
        lazyRender:true,
        mode: 'local',
        store: arrayYesNo,
        valueField: 'myId',
        displayField: 'displayText'
    });

    comboDeBaja = new Ext.form.ComboBox({
        typeAhead: true,
        triggerAction: 'all',
        lazyRender:true,
        mode: 'local',
        store: arraySiNo,
        valueField: 'myId',
        displayField: 'displayText'
    });


    storeRoles = null;
    storeRoles = new Ext.data.JsonStore({
        url: '../usuarios/role',
        root: 'roles',
        fields: ['id', 'name']
    });
    storeRoles.load();


    comboRoles = new Ext.form.ComboBox({
        typeAhead: true,
        triggerAction: 'all',
        lazyRender:false,
        mode: 'remote',
        store: storeRoles,
        valueField: 'id',
        displayField: 'name',
        listeners: {
            // delete the previous query in the beforequery event or set
            // combo.lastQuery = null (this will reload the store the next time it expands)
            beforequery: function(qe){
                delete qe.combo.lastQuery;
            }
        }
    });


    editorUsuario = new Ext.ux.grid.RowEditor(
    {
        id:"editorUsuario",
        saveText  : "Guardar",
        cancelText: "Cancelar",
        clicksToEdit: 1, //this changes from the default double-click activation to single click activation
        errorSummary: false //disables display of validation messages if the row is invalid
    });

    editorUsuario.on(
    {
        canceledit: function(roweditor, changes)
        {
            Ext.grid.dataUsuario.reload();
        },
        validateedit: function(roweditor, changes, record, rowIndex)
        {
            console.log(this.isValid());
            if (this.editing && !this.isValid()) 
            {
                return false;
            }
            else
            {
                record.commit();
            }
        },
        afteredit: function(roweditor, changes, record, rowIndex) 
        {
            Ext.grid.dataUsuario.reload();
        }
    });

    Ext.util.Format.comboRenderer = function(combo)
    {
        return function(value)
        {
            var record = combo.findRecord(combo.valueField, value);
            return record ? record.get(combo.displayField) : combo.valueNotFoundText;
        }
    };
    grillaUsuario = null;
    grillaUsuario = new Ext.grid.GridPanel({
                        border:false,
                        id:'grillaUsuario',
                        plugins: editorUsuario,
                        ds: Ext.grid.dataUsuario,
                     // paging bar on the bottom
                        bbar: new Ext.PagingToolbar({
                            pageSize: 5,
                            store: Ext.grid.dataUsuario,
                            displayInfo: true,
                            displayMsg: 'Mostrando Registros: {0} - {1} of {2}',
                            emptyMsg: "No hay registros"
                            
                        }),
                        cm: new Ext.grid.ColumnModel( [
                                            //new Ext.grid.RowNumberer(),
                                            {
                                                header: "ID",
                                                sortable: true,
                                                width: 50,
                                                dataIndex: "id"
                                            }, {
                                                header: "Usuario",
                                                width: 200,
                                                sortable: true,
                                                dataIndex: "usuario"
                                                ,// use shorthand alias defined above
                                                editor: {allowBlank: false}
                                            }, {
                                                header: "Super Admin",
                                                width: 70,
                                                sortable: true,
                                                dataIndex: "superadmin",
                                                editor: comboSuperAdmin,
                                                renderer: Ext.util.Format.comboRenderer(comboSuperAdmin) // pass combo instance to reusable renderer
                                            }, {
                                                header: "Rol",
                                                width: 150,
                                                sortable: true,
                                                dataIndex: "role",
                                                editor: comboRoles, // specify reference to combo instance
                                                renderer: Ext.util.Format.comboRenderer(comboRoles) // pass combo instance to reusable renderer
                                                
                                            }, {
                                                header: "Creado",
                                                width: 70,
                                                sortable: true,
                                                dataIndex: "creado",
                                                renderer: Ext.util.Format.dateRenderer('d/m/Y')
                                                ,editor: new fm.DateField({
                                                    format: 'd/m/Y',
                                                    minValue: '01/01/2014'
                                                })
                                            }, {
                                                header: "Modificado",
                                                width: 70,
                                                sortable: true,
                                                dataIndex: "modificado",
                                                renderer: Ext.util.Format.dateRenderer('d/m/Y')
                                                ,editor: new fm.DateField({
                                                    format: 'd/m/Y',
                                                    minValue: '01/01/2014'
                                                })
                                            }, {
                                                header: "Agencia",
                                                width: 150,
                                                sortable: true,
                                                dataIndex: "agencia",
                                                editor: comboAgencias, // specify reference to combo instance
                                                renderer: Ext.util.Format.comboRenderer(comboAgencias) // pass combo instance to reusable renderer
                                                
                                            }, {
                                                header: "Clave",
                                                width: 50,
                                                sortable: true,
                                                dataIndex: "clave"
                                                ,// use shorthand alias defined above
                                                editor: {allowBlank: true}
                                            }, {
                                                header: "De Baja",
                                                width: 70,
                                                sortable: true,
                                                dataIndex: "debaja",
                                                editor: comboDeBaja,
                                                renderer: Ext.util.Format.comboRenderer(comboDeBaja) // pass combo instance to reusable renderer
                                            }
                            ]),

                        viewConfig: {
                            forceFit:true,
                            getRowClass : function(record, rowIndex, p, store){
                                if(this.showPreview){
                                    p.body = '<p>'+record.data.excerpt+'</p>';
                                    return 'x-grid3-row-expanded';
                                }
                                return 'x-grid3-row-collapsed';
                            }
                        },
                        //autoExpandColumn:'company',

                        tbar:[{
                                text:'Insertar un registro',
                                
                                handler: function(){
                                    var e = new Usuario();
                                    editorUsuario.stopEditing();
                                    Ext.grid.dataUsuario.insert(0, e);
                                    //grillaUsuario.getView().refresh();
                                    //grillaUsuario.getSelectionModel().selectRow(0);
                                    //editorUsuario.startEditing(0);
                                }
                                ,
                                tooltip:'Nuevo',
                                iconCls:'add'
                            }, '-',
                            {
                                ref: '../removeBtn',
                                text:'Eliminar un registro',
                                tooltip:'Elimina el registro seleccionado',
                                iconCls:'remove',
                                disabled:true,
                                handler: function()
                                			{
                                                Ext.Msg.alert('Eliminar un registro', 'En esta vista para eliminar un registro debe darlo de baja');
                                            }
                            }
                            
                            ]
                    });

    grillaUsuario.getSelectionModel().on('selectionchange', function(sm){
        grillaUsuario.removeBtn.setDisabled(sm.getCount() < 1);
    });


}



cargarUsuarioObj();
//cargargrillaUsuario();
grillaUsuario.removeBtn.setDisabled(false);

function formatDate(value)
{
    return value ? value.dateFormat('d/m/Y H:i') : '';
}


function crearVentanaUsuarioes()
{
	//Desktop
		MyDesktop.UsuarioTWindow = Ext.extend(Ext.app.Module, {
	    id:'usuarios-win',
	    autoDestroy:false,
	    init : function(){
	
	        this.launcher = {
	            text: 'Usuarios',
	            iconCls:'user-suit',
	            handler : this.createWindow,
	            scope: this
	        }
	    },
	
	    createWindow : function(){
	        var desktop = this.app.getDesktop();
	        var win = desktop.getWindow('usuarios-win');
	        if(!win){         
	            win = desktop.createWindow({
	                id: 'usuarios-win',
	                title:'Usuarios',
	                width:900,
	                height:400,
	                iconCls: 'user-suit',
	                shim:false,
	                animCollapse:false,
	                constrainHeader:true,
	                layout: 'fit',
	                items: [grillaUsuario],
	                listeners: {
	                            beforeshow: function( window ) 
	                            {
	                                cargarUsuarioObj();
	                                Ext.grid.dataUsuario.reload();
	                            }
	                        }
	            });
	        }
	        win.show();
	    
	    }
	});
}
crearVentanaUsuarioes();

    grillaUsuario.getSelectionModel().on('selectionchange', function(sm){
        grillaUsuario.removeBtn.setDisabled(sm.getCount() < 1);
    });