


	//Desktop

        MyDesktop.ExternaOrdenesComTWindow = Ext.extend(Ext.app.Module, {
	    id:'externaOrdenesCom-win',
	    autoDestroy:false,
	    init : function(){
	
	        this.launcher = {
	            text: '&Oacute;rdenes de Compra',
	            iconCls:'icon-ordenesc',
	            handler : this.createWindow,
	            scope: this
	        }
	    },
	
	    createWindow : function(){
	        var desktop = this.app.getDesktop();
	        var win = desktop.getWindow('externaOrdenesCom-win');
	        if(!win){         
	            win = desktop.createWindow({
	                id: 'externaOrdenesCom-win',
	                title:'&Oacute;rdenes de Compra',
	                width:1000,
	                height:500,
	                iconCls: 'icon-ordenesc',
	                shim:false,
	                animCollapse:false,
	                constrainHeader:true,
                        html:"<iframe src='../../externo/ordenesCompra.php' style='width:100%;height:100%;border:none'></iframe>",
                        autoScroll:true,
	                layout: 'fit'	                
	            });
	        }
	        win.show();
	    
	    }
	});
