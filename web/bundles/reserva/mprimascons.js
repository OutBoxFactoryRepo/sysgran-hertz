var fm = Ext.form;
var MprimasCons = null;

Ext.grid.dataMprimasCons = new Ext.data.JsonStore(
    {
            // store configs
            autoDestroy: false,
            autoLoad: {params:{start: 0, limit: 15}},
            url: '../materiaprimacons/search/',
            storeId: 'myStoreMprimasCons',
            totalProperty: 'results',
            root:'rows',
            remoteSort:false,
            api:
            {
                create : '../materiaprimacons/crear/',
                read : '../materiaprimacons/search/',
                update: '../materiaprimacons/editar/'
                //,destroy: '../web/app_dev.php/MprimasCons/delete/'
            },
            writer: 
                {
                    type: 'post',
                    writeAllFields : true,  //just send changed fields
                    allowSingle :false,     //always wrap in an array
                    encode:false,
                },
                reader: {
                    root: "Violations",
                    type: "json",
                    messageProperty : 'message' //without this, it doesn't work
                },
            // reader configs
             sortInfo: {field: 'id', direction: 'DESC'},
                                idProperty: 'id',
                                fields: [
                                       { name: 'id',type:'integer' },
                                       { name: 'descripcion', type: 'string' },
                                       { name: 'materiaprima', type: 'string' ,mapping: 'materiaprima.id'},
                                       { name: 'cantidad', type: 'integer' ,mapping: 'cantidad'},
                                       { name: 'debaja', type: 'boolean'}
                                       ]
                });



var comboTipoMprimasCons = null;


var arraySiNo = null;
var comboDeBaja = null;
var editorMprimasCons = null;
var grillaMprimasCons = null;
var storeAgenciaMprimasCons = null;
var storeMprimasConsMP = null;


function cargarMprimasConsObj()
{

  
    
    storeMprimasConsMP = new Ext.data.JsonStore({
        url: '../materiaprima/search/',
        root: 'rows',
        fields: ['id', 'descripcion']
        ,listeners:
            {
                beforeload: function(store)
                {
                    console.log("taller: "+filterTaller);
                    store.setBaseParam('taller', filterTaller);
                }
            }
        //,autoLoad: {params:{taller:filterTaller}}
    });
    //con el taller tengo la sucursal a filtrar de materias primas

    //alert(filterTaller);
    storeMprimasConsMP.load({params:{taller:filterTaller}});

    comboMprimasConsMP = new Ext.form.ComboBox({
        typeAhead: true,
        triggerAction: 'all',
        lazyRender:false,
        mode: 'remote',
        store: storeMprimasConsMP,
        valueField: 'id',
        lastQuery: '',
        displayField: 'descripcion',
        listeners: {
            // delete the previous query in the beforequery event or set
            // combo.lastQuery = null (this will reload the store the next time it expands)
            beforequery: function(qe){
                
                delete qe.combo.lastQuery;
            }
        }
    });

    Ext.grid.dataMprimasCons.setBaseParam('ot', filterOt);
    
    MprimasCons = Ext.data.Record.create(
        [
                                       { name: 'id',type:'integer' },
                                       { name: 'descripcion', type: 'string' },
                                       { name: 'materiaprima', type: 'string' ,mapping: 'materiaprima.id'},
                                       { name: 'cantidad', type: 'integer' ,mapping: 'cantidad'},
                                       { name: 'debaja', type: 'boolean'}
        ]);

   

    arraySiNo = new Ext.data.ArrayStore({
        id: 0,
        fields: [
            'myId',
            'displayText'
        ],
        data: [[false, 'Alta'], [true, 'De baja']]
    });


    comboDeBaja = new Ext.form.ComboBox({
        typeAhead: true,
        triggerAction: 'all',
        lazyRender:true,
        mode: 'local',
        store: arraySiNo,
        valueField: 'myId',
        displayField: 'displayText'
    });

    editorMprimasCons = new Ext.ux.grid.RowEditor(
    {
        id:"editorMprimasCons",
        saveText  : "Guardar",
        cancelText: "Cancelar",
        clicksToEdit: 2, //this changes from the default double-click activation to single click activation
        errorSummary: false //disables display of validation messages if the row is invalid
    });

    editorMprimasCons.on(
    {
        canceledit: function(roweditor, changes)
        {
            Ext.grid.dataMprimasCons.reload();
        },
        validateedit: function(roweditor, changes, record, rowIndex)
        {
            console.log(this.isValid());
            if (this.editing && !this.isValid()) 
            {
                return false;
            }
            else
            {
                record.commit();
            }
        },
        afteredit: function(roweditor, changes, record, rowIndex) 
        {
            Ext.grid.dataMprimasCons.reload();
        }
    });

    Ext.util.Format.comboRenderer = function(combo)
    {
        return function(value)
        {
            var record = combo.findRecord(combo.valueField, value);
            return record ? record.get(combo.displayField) : combo.valueNotFoundText;
        }
    };


    grillaMprimasCons = null;

    grillaMprimasCons = new Ext.grid.GridPanel({
                            sm: new Ext.grid.RowSelectionModel({singleSelect:true}),  
                            listeners: {
                                        rowclick: function(searchgrid, rowIndex, e) {
                                            storeMprimasConsMP.load({params:{taller:filterTaller}});
                                  
                                        }
                                    }

                            ,
                        border:false,
                        width:"100%",
                        height:"200",
                        id:'grillaMprimasCons',
                        plugins: editorMprimasCons,
                        ds: Ext.grid.dataMprimasCons,
                        // paging bar on the bottom
                        bbar: new Ext.PagingToolbar({
                            pageSize: 15,
                            store: Ext.grid.dataMprimasCons,
                            displayInfo: true,
                            displayMsg: 'Mostrando Registros: {0} - {1} of {2}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
                            emptyMsg: "No hay registros"
                            
                        }),
                        cm: new Ext.grid.ColumnModel([
                                            //new Ext.grid.RowNumberer(),
                                            {
                                                header: "ID",
                                                sortable: true,
                                                width: 50,
                                                dataIndex: "id"
                                            }, {
                                                header: "Descripci&oacute;n",
                                                width: 200,
                                                sortable: true,
                                                dataIndex: "descripcion"
                                                ,// use shorthand alias defined above
                                                editor: {allowBlank: false}
                                            },
                                            {
                                                header: "Materia Prima",
                                                width: 150,
                                                sortable: true,
                                                dataIndex: "materiaprima",
                                                editor: comboMprimasConsMP, // specify reference to combo instance
                                                renderer: Ext.util.Format.comboRenderer(comboMprimasConsMP) // pass combo instance to reusable renderer
                                            },
                                             {
                                                header: "Cantidad",
                                                width: 70,
                                                sortable: true,
                                                dataIndex: "cantidad"
                                                ,editor: {allowBlank: false}
                                            }
                                            , {
                                                header: "De Baja",
                                                width: 70,
                                                sortable: true,
                                                dataIndex: "debaja",
                                                editor: comboDeBaja,
                                                renderer: Ext.util.Format.comboRenderer(comboDeBaja) // pass combo instance to reusable renderer
                                            }
                            ])
                            ,
                        viewConfig: {
                        	forceFit:true,
                            getRowClass : function(record, rowIndex, p, store){
                                if(this.showPreview){
                                    p.body = '<p>'+record.data.excerpt+'</p>';
                                    return 'x-grid3-row-expanded';
                                }
                                return 'x-grid3-row-collapsed';
                            }
                        },
                        //autoExpandColumn:'company',

                        tbar:[{
                                text:'Insertar un registro',
                                
                                handler: function(){
                                    var e = new MprimasCons();
                                    editorMprimasCons.stopEditing();
                                    Ext.grid.dataMprimasCons.insert(0, e);
                                    //grillaMprimasCons.getView().refresh();
                                    //grillaMprimasCons.getSelectionModel().selectRow(0);
                                    //editorMprimasCons.startEditing(0);
                                }
                                ,
                                tooltip:'Nuevo',
                                iconCls:'add'
                            }, '-',
                            {
                                ref: '../removeBtn',
                                text:'Eliminar un registro',
                                tooltip:'Elimina el registro seleccionado',
                                iconCls:'remove',
                                disabled:true,
                                handler: function(){
                                                /*
                                                editor.stopEditing();
                                                var s = miGrilla.getSelectionModel().getSelections();
                                                for(var i = 0, r; r = s[i]; i++){
                                                    datar.remove(r);
                                                }
                                                */
                                                Ext.Msg.alert('Eliminar un registro', 'En esta vista para eliminar un registro debe darlo de baja');
                                            }
                            }]
              
    				}
    				
    			);
    //Ext.grid.dataMprimasCons.load();
    grillaMprimasCons.getSelectionModel().on('selectionchange', function(sm){
        grillaMprimasCons.removeBtn.setDisabled(sm.getCount() < 1);
    });


}


cargarMprimasConsObj();
//cargargrillaMprimasCons();
grillaMprimasCons.removeBtn.setDisabled(false);

function formatDate(value)
{
    return value ? value.dateFormat('d/m/Y H:i') : '';
}

function crearVentanaMprimasCons()
{
	//Desktop
	MyDesktop.MprimasConsWindow = Ext.extend(Ext.app.Module, {
	    id:'mprimascons-win',
	    init : function(){
	
	        this.launcher = {
	            text: 'M.P. Consumos',
	            iconCls:'icon-adicionales',
	            handler : this.createWindow,
	            scope: this
	        }
	    },
	
	    createWindow : function(){
	        var desktop = this.app.getDesktop();
	        var win = desktop.getWindow('mprimascons-win');
	        if(!win){
	   
	            
	            win = desktop.createWindow({
	                id: 'mprimascons-win',
	                title:'M.P. Consumos',
	                width:900,
	                height:400,
	                iconCls: 'icon-adicionales',
	                shim:false,
	                animCollapse:false,
	                constrainHeader:true,
	                layout: 'fit',
	                items: [grillaMprimasCons],
	                listeners: {
	                            beforeshow: function( window ) 
	                            {
	                                cargarMprimasConsObj();
	                                Ext.grid.dataMprimasCons.reload();
	                            }
	                        }
	            });
	        }
	        win.show();
	    }
	});

}
crearVentanaMprimasCons();

    grillaMprimasCons.getSelectionModel().on('selectionchange', function(sm){
        grillaMprimasCons.removeBtn.setDisabled(sm.getCount() < 1);
    });