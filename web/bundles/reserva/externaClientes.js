


	//Desktop

        MyDesktop.ExternaClienteTWindow = Ext.extend(Ext.app.Module, {
	    id:'externaCliente-win',
	    autoDestroy:false,
	    init : function(){
	
	        this.launcher = {
	            text: 'Clientes',
	            iconCls:'icon-cliente',
	            handler : this.createWindow,
	            scope: this
	        }
	    },
	
	    createWindow : function(){
	        var desktop = this.app.getDesktop();
	        var win = desktop.getWindow('externaCliente-win');
	        if(!win){         
	            win = desktop.createWindow({
	                id: 'externaCliente-win',
	                title:'Clientes',
	                width:1200,
	                height:800,
	                iconCls: 'icon-cliente',
	                shim:false,
	                animCollapse:false,
	                constrainHeader:true,
                        html:"<iframe src='../../externo/cliente.php' style='width:100%;height:100%;border:none'></iframe>",
                        autoScroll:true,
	                layout: 'fit'	                
	            });
	        }
	        win.show();
	    
	    }
	});
