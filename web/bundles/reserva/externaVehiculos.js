


	//Desktop

        MyDesktop.ExternaVehiculoTWindow = Ext.extend(Ext.app.Module, {
	    id:'externaVehiculo-win',
	    autoDestroy:false,
	    init : function(){
	
	        this.launcher = {
	            text: 'Veh&iacute;culos',
	            iconCls:'icon-vehiculos',
	            handler : this.createWindow,
	            scope: this
	        }
	    },
	
	    createWindow : function(){
	        var desktop = this.app.getDesktop();
	        var win = desktop.getWindow('externaVehiculo-win');
	        if(!win){         
	            win = desktop.createWindow({
	                id: 'externaVehiculo-win',
	                title:'Vehículos',
	                width:1000,
	                height:500,
	                iconCls: 'icon-vehiculos',
	                shim:false,
	                animCollapse:false,
	                constrainHeader:true,
                        html:"<iframe src='../../externo/vehiculos.php' style='width:100%;height:100%;border:none'></iframe>",
                        autoScroll:true,
	                layout: 'fit'	                
	            });
	        }
	        win.show();
	    
	    }
	});
