


	//Desktop

        MyDesktop.ExternaTWindow = Ext.extend(Ext.app.Module, {
	    id:'externa-win',
	    autoDestroy:false,
	    init : function(){
	
	        this.launcher = {
	            text: '&Oacute;rdenes de Trabajo',
	            iconCls:'icon-ordenest',
	            handler : this.createWindow,
	            scope: this
	        }
	    },
	
	    createWindow : function(){
	        var desktop = this.app.getDesktop();
	        var win = desktop.getWindow('externa-win');
	        if(!win){         
	            win = desktop.createWindow({
	                id: 'externa-win',
	                title:'Ventana Externa',
	                width:900,
	                height:400,
	                iconCls: 'icon-ordenest',
	                shim:false,
	                animCollapse:false,
	                constrainHeader:true,
                        html:"<iframe src='http://local.hertz/externo/prueba.html' style='width:100%;height:100%;border:none'></iframe>",
                        autoScroll:true
	                //layout: 'fit',
	                
	                
	            });
	        }
	        win.show();
	    
	    }
	});
