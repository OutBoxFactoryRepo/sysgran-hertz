var fm = Ext.form;
var Mprimas = null;

Ext.grid.dataMprimas = new Ext.data.JsonStore(
    {
            // store configs
            autoDestroy: false,
            autoLoad: {params:{start: 0, limit: 15}},
            url: '../materiaprima/search/',
            storeId: 'myStoreMprimas',
            totalProperty: 'results',
            root:'rows',
            remoteSort:false,
            api:
            {
                create : '../materiaprima/crear/',
                read : '../materiaprima/search/',
                update: '../materiaprima/editar/'
                //,destroy: '../web/app_dev.php/Mprimas/delete/'
            },
            writer: 
                {
                    type: 'post',
                    writeAllFields : true,  //just send changed fields
                    allowSingle :false,     //always wrap in an array
                    encode:false,
                },
                reader: {
                    root: "Violations",
                    type: "json",
                    messageProperty : 'message' //without this, it doesn't work
                },
            // reader configs
             sortInfo: {field: 'id', direction: 'DESC'},
                                idProperty: 'id',
                                fields: [
                                       { name: 'id',type:'integer' },
                                       { name: 'descripcion', type: 'string' },
                                       { name: 'agencia', type: 'string' ,mapping: 'agencia.id'},
                                       { name: 'cantidad', type: 'integer' ,mapping: 'cantidad'},
                                       { name: 'stockcritico', type: 'integer', mapping:'stock_critico'},
                                       { name: 'valor', type: 'float'},
                                       { name: 'debaja', type: 'boolean'}
                                       ]
                });



var comboTipoMprimas = null;


var arraySiNo = null;
var comboDeBaja = null;
var editorMprimas = null;
var grillaMprimas = null;
var storeAgenciaMprimas = null;
function cargarMprimasObj()
{

    storeAgenciaMprimas = new Ext.data.JsonStore({
        url: '../agencia/searchadictodas/',
        root: '',
        fields: ['id', 'descripcion']
    });
    storeAgenciaMprimas.load();

    comboAgenciasMprimas = new Ext.form.ComboBox({
        typeAhead: true,
        triggerAction: 'all',
        lazyRender:true,
        mode: 'local',
        store: storeAgenciaMprimas,
        valueField: 'id',
        displayField: 'descripcion',
        listeners: {
            // delete the previous query in the beforequery event or set
            // combo.lastQuery = null (this will reload the store the next time it expands)
            beforequery: function(qe){
                delete qe.combo.lastQuery;
            }
        }
    });


    Mprimas = Ext.data.Record.create(
        [
                                       { name: 'id',type:'integer' },
                                       { name: 'descripcion', type: 'string' },
                                       { name: 'agencia', type: 'string' ,mapping: 'agencia.id'},
                                       { name: 'cantidad', type: 'integer' ,mapping: 'cantidad'},
                                       { name: 'stockcritico', type: 'integer' ,mapping: 'stock_critico'},
                                       { name: 'valor', type: 'float'},
                                       { name: 'debaja', type: 'boolean'}
        ]);

   

    arraySiNo = new Ext.data.ArrayStore({
        id: 0,
        fields: [
            'myId',
            'displayText'
        ],
        data: [[false, 'Alta'], [true, 'De baja']]
    });


    comboDeBaja = new Ext.form.ComboBox({
        typeAhead: true,
        triggerAction: 'all',
        lazyRender:true,
        mode: 'local',
        store: arraySiNo,
        valueField: 'myId',
        displayField: 'displayText'
    });

    editorMprimas = new Ext.ux.grid.RowEditor(
    {
        id:"editorMprimas",
        saveText  : "Guardar",
        cancelText: "Cancelar",
        //clicksToEdit: 1, //this changes from the default double-click activation to single click activation
        errorSummary: false //disables display of validation messages if the row is invalid
    });

    editorMprimas.on(
    {
        canceledit: function(roweditor, changes)
        {
            Ext.grid.dataMprimas.reload();
        },
        validateedit: function(roweditor, changes, record, rowIndex)
        {
            console.log(this.isValid());
            if (this.editing && !this.isValid()) 
            {
                return false;
            }
            else
            {
                record.commit();
            }
        },
        afteredit: function(roweditor, changes, record, rowIndex) 
        {
            Ext.grid.dataMprimas.reload();
        }
    });

    Ext.util.Format.comboRenderer = function(combo)
    {
        return function(value)
        {
            var record = combo.findRecord(combo.valueField, value);
            return record ? record.get(combo.displayField) : combo.valueNotFoundText;
        }
    };
    grillaMprimas = null;

    grillaMprimas = new Ext.grid.GridPanel({
                        border:false,
                        id:'grillaMprimas',
                        plugins: editorMprimas,
                        ds: Ext.grid.dataMprimas,
                        // paging bar on the bottom
                        bbar: new Ext.PagingToolbar({
                            pageSize: 15,
                            store: Ext.grid.dataMprimas,
                            displayInfo: true,
                            displayMsg: 'Mostrando Registros: {0} - {1} of {2}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
                            emptyMsg: "No hay registros"
                            
                        }),
                        cm: new Ext.grid.ColumnModel([
                                            //new Ext.grid.RowNumberer(),
                                            {
                                                header: "ID",
                                                sortable: true,
                                                width: 50,
                                                dataIndex: "id"
                                            }, {
                                                header: "Descripci&oacute;n",
                                                width: 200,
                                                sortable: true,
                                                dataIndex: "descripcion"
                                                ,// use shorthand alias defined above
                                                editor: {allowBlank: false}
                                            },
                                            {
                                                header: "Agencia",
                                                width: 150,
                                                sortable: true,
                                                dataIndex: "agencia",
                                                editor: comboAgenciasMprimas, // specify reference to combo instance
                                                renderer: Ext.util.Format.comboRenderer(comboAgenciasMprimas) // pass combo instance to reusable renderer
                                                
                                            },
                                             {
                                                header: "Cantidad",
                                                width: 70,
                                                sortable: true,
                                                dataIndex: "cantidad"
                                                ,editor: {allowBlank: false}
                                            },
                                             {
                                                header: "Stock Cr&iacute;tico",
                                                width: 70,
                                                sortable: true,
                                                dataIndex: "stockcritico"
                                                ,editor: {allowBlank: false}
                                            }
                                            ,{
                                                header: "Valor",
                                                width: 70,
                                                sortable: true,
                                                dataIndex: "valor"
                                                ,renderer: Ext.util.Format.usMoney
                                                ,editor: {allowBlank: false}
                                            }
                                            , {
                                                header: "De Baja",
                                                width: 70,
                                                sortable: true,
                                                dataIndex: "debaja",
                                                editor: comboDeBaja,
                                                renderer: Ext.util.Format.comboRenderer(comboDeBaja) // pass combo instance to reusable renderer
                                            }
                            ])
                            ,
                        viewConfig: {
                        	forceFit:true,
                            getRowClass : function(record, rowIndex, p, store){
                                if(this.showPreview){
                                    p.body = '<p>'+record.data.excerpt+'</p>';
                                    return 'x-grid3-row-expanded';
                                }
                                return 'x-grid3-row-collapsed';
                            }
                        },
                        //autoExpandColumn:'company',

                        tbar:[{
                                text:'Insertar un registro',
                                
                                handler: function(){
                                    var e = new Mprimas();
                                    editorMprimas.stopEditing();
                                    Ext.grid.dataMprimas.insert(0, e);
                                    //grillaMprimas.getView().refresh();
                                    //grillaMprimas.getSelectionModel().selectRow(0);
                                    //editorMprimas.startEditing(0);
                                }
                                ,
                                tooltip:'Nuevo',
                                iconCls:'add'
                            }, '-',
                            {
                                ref: '../removeBtn',
                                text:'Eliminar un registro',
                                tooltip:'Elimina el registro seleccionado',
                                iconCls:'remove',
                                disabled:true,
                                handler: function(){
                                                /*
                                                editor.stopEditing();
                                                var s = miGrilla.getSelectionModel().getSelections();
                                                for(var i = 0, r; r = s[i]; i++){
                                                    datar.remove(r);
                                                }
                                                */
                                                Ext.Msg.alert('Eliminar un registro', 'En esta vista para eliminar un registro debe darlo de baja');
                                            }
                            }]
              
    				}
    				
    			);
    //Ext.grid.dataMprimas.load();
    grillaMprimas.getSelectionModel().on('selectionchange', function(sm){
        grillaMprimas.removeBtn.setDisabled(sm.getCount() < 1);
    });


}


cargarMprimasObj();
//cargargrillaMprimas();
grillaMprimas.removeBtn.setDisabled(false);

function formatDate(value)
{
    return value ? value.dateFormat('d/m/Y H:i') : '';
}

function crearVentanaMprimas()
{
	//Desktop
	MyDesktop.MprimasesWindow = Ext.extend(Ext.app.Module, {
	    id:'mprimas-win',
	    init : function(){
	
	        this.launcher = {
	            text: 'Materias Primas',
	            iconCls:'icon-adicionales',
	            handler : this.createWindow,
	            scope: this
	        }
	    },
	
	    createWindow : function(){
	        var desktop = this.app.getDesktop();
	        var win = desktop.getWindow('mprimas-win');
	        if(!win){
	   
	            
	            win = desktop.createWindow({
	                id: 'mprimas-win',
	                title:'Materias Primas',
	                width:900,
	                height:400,
	                iconCls: 'icon-adicionales',
	                shim:false,
	                animCollapse:false,
	                constrainHeader:true,
	                layout: 'fit',
	                items: [grillaMprimas],
	                listeners: {
	                            beforeshow: function( window ) 
	                            {
	                                cargarMprimasObj();
	                                Ext.grid.dataMprimas.reload();
	                            }
	                        }
	            });
	        }
	        win.show();
	    }
	});

}
crearVentanaMprimas();

    grillaMprimas.getSelectionModel().on('selectionchange', function(sm){
        grillaMprimas.removeBtn.setDisabled(sm.getCount() < 1);
    });