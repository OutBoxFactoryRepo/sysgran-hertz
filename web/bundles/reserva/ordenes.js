var fm = Ext.form;
var Orden = null;
var filterOt = 0;
var filterTaller = 0;
Ext.grid.dataOrden = new Ext.data.JsonStore(
                        {
                                // store configs
                        		autoDestroy: false,
                            	autoLoad: {params:{start: 0, limit: 5}},
                                url: '../ordentrabajo/search/',
                                storeId: 'myStoreOrden',
                                totalProperty: 'results',
                                root:'rows',
                                remoteSort:false,
                                api:
                                {
                                    create : '../ordentrabajo/crear/',
                                    read : '../ordentrabajo/search/',
                                    update: '../ordentrabajo/editar/'
                                    //,destroy: '../web/app_dev.php/vehiculo/delete/'
                                },
                                writer: 
                                    {
                                        type: 'post',
                                        writeAllFields : true,  //just send changed fields
                                        allowSingle :false,     //always wrap in an array
                                        encode:false,
                                    },
                                    reader: {
                                        root: "Violations",
                                        type: "json",
                                        messageProperty : 'message' //without this, it doesn't work
                                    },
                                // reader configs
                                sortInfo: {field: 'id', direction: 'DESC'},
                                idProperty: 'id',
                                fields: [
                                       { name: 'id',type:'integer' },
                                       { name: 'descripcion', type: 'string' },
                                       { name: 'dominio', type: 'string' ,mapping: 'vehiculo.id'},
                                       { name: 'estado', type: 'string' ,mapping: 'estado.id'},
                                       { name: 'taller', type: 'string' ,mapping: 'taller.id'},
                                       { name: 'fecha_carga', type: 'date'},
                                       { name: 'debaja', type: 'boolean'}]
                        });



var comboTipoOrden = null;


var arraySiNo = null;
var comboDeBaja = null;
var editorOrden = null;
var grillaOrden = null;
var storeVehiculoOrd = null;
var comboVehiculoOrd = null;
var storeEstadosOrd = null;
var comboEstadosOrd = null;
var storeTallerOrd = null;
var comboTallerOrd = null;

function cargarOrdenObj()
{
    
    storeTallerOrd = null;
    storeTallerOrd = new Ext.data.JsonStore({
        url: '../taller/search/',
        root: 'rows',
        fields: ['id', 'descripcion']
    });
    storeTallerOrd.load();

    comboTallerOrd = new Ext.form.ComboBox({
        typeAhead: true,
        triggerAction: 'all',
        lazyRender:false,
        mode: 'remote',
        store: storeTallerOrd,
        valueField: 'id',
        displayField: 'descripcion',
        listeners: {
            // delete the previous query in the beforequery event or set
            // combo.lastQuery = null (this will reload the store the next time it expands)
            beforequery: function(qe){
                delete qe.combo.lastQuery;
            }
        }
    });
	
	
	storeVehiculoOrd = null;
    storeVehiculoOrd = new Ext.data.JsonStore({
        url: '../vehiculo/search/?debaja=false',
        root: 'rows',
        fields: ['id', 'dominio']
    });
    storeVehiculoOrd.load();

    comboVehiculoOrd = new Ext.form.ComboBox({
        typeAhead: true,
        triggerAction: 'all',
        lazyRender:false,
        mode: 'remote',
        store: storeVehiculoOrd,
        valueField: 'id',
        displayField: 'dominio',
        listeners: {
            // delete the previous query in the beforequery event or set
            // combo.lastQuery = null (this will reload the store the next time it expands)
            beforequery: function(qe){
                delete qe.combo.lastQuery;
            }
        }
    });

    storeEstadosOrd = new Ext.data.JsonStore({
        url: '../estadosot/search/',
        root: 'rows',
        fields: ['id', 'descripcion']
    });
    storeEstadosOrd.load();

    comboEstadosOrd = new Ext.form.ComboBox({
        typeAhead: true,
        triggerAction: 'all',
        lazyRender:false,
        mode: 'remote',
        store: storeEstadosOrd,
        valueField: 'id',
        displayField: 'descripcion',
        listeners: {
            // delete the previous query in the beforequery event or set
            // combo.lastQuery = null (this will reload the store the next time it expands)
            beforequery: function(qe){
                delete qe.combo.lastQuery;
            }
        }
    });


    Orden = Ext.data.Record.create(
        [
                       { name: 'id',type:'integer' },
                       { name: 'descripcion', type: 'string' },
                       { name: 'dominio', type: 'string' ,mapping: 'vehiculo.id'},
                       { name: 'estado', type: 'string' ,mapping: 'estado.id'},
                       { name: 'taller', type: 'string' ,mapping: 'taller.id'},
                       { name: 'fecha_carga', type: 'date'},
                       { name: 'debaja', type: 'boolean'}
        ]);

   

    arraySiNo = new Ext.data.ArrayStore({
        id: 0,
        fields: [
            'myId',
            'displayText'
        ],
        data: [[false, 'Alta'], [true, 'De baja']]
    });


    comboDeBaja = new Ext.form.ComboBox({
        typeAhead: true,
        triggerAction: 'all',
        lazyRender:true,
        mode: 'local',
        store: arraySiNo,
        valueField: 'myId',
        displayField: 'displayText'
    });

    editorOrden = new Ext.ux.grid.RowEditor(
    {
        id:"editorOrden",
        saveText  : "Guardar",
        cancelText: "Cancelar",
        clicksToEdit: 2, //this changes from the default double-click activation to single click activation
        errorSummary: false //disables display of validation messages if the row is invalid
    });

    editorOrden.on(
    {
        canceledit: function(roweditor, changes)
        {
            Ext.grid.dataOrden.reload();
        },
        validateedit: function(roweditor, changes, record, rowIndex)
        {
            console.log(this.isValid());
            if (this.editing && !this.isValid()) 
            {
                return false;
            }
            else
            {
                record.commit();
            }
        },
        afteredit: function(roweditor, changes, record, rowIndex) 
        {
            Ext.grid.dataOrden.reload();
        }
    });

    Ext.util.Format.comboRenderer = function(combo)
    {
        return function(value)
        {
            var record = combo.findRecord(combo.valueField, value);
            return record ? record.get(combo.displayField) : combo.valueNotFoundText;
        }
    };
    grillaOrden = null;
    grillaOrden = new Ext.grid.GridPanel({
                        border:false,
                        id:'grillaOrden',
                        plugins: editorOrden,          
                        width:"100%",
                        height:"200",
                            sm: new Ext.grid.RowSelectionModel({singleSelect:true}),
               
                            listeners: {
                                        rowclick: function(searchgrid, rowIndex, e) {
                                            var record = Ext.grid.dataOrden.getAt(rowIndex);
                                            //paso como parámetro los valores de la fila seleccionada
                                            filterOt = record.data.id;
                                            filterTaller = record.data.taller;
                                            //alert(filterTaller);
                                            cargarMprimasConsObj();
                                            Ext.grid.dataMprimasCons.load({params:{start: 0, limit: 15, ot:filterOt,taller:filterTaller}});
                                            storeMprimasConsMP.load({params:{taller:filterTaller}});
                                        }
                                    }

                            ,
                        ds: Ext.grid.dataOrden,
                     // paging bar on the bottom
                        bbar: new Ext.PagingToolbar({
                            pageSize: 5,
                            store: Ext.grid.dataOrden,
                            displayInfo: true,
                            displayMsg: 'Mostrando Registros: {0} - {1} of {2}',
                            emptyMsg: "No hay registros"
                            
                        }),
                        cm: new Ext.grid.ColumnModel( [
                                            //new Ext.grid.RowNumberer(),
                                            {
                                                header: "ID",
                                                sortable: true,
                                                width: 50,
                                                dataIndex: "id"
                                            }, {
                                                header: "Descripci&oacute;n",
                                                width: 200,
                                                sortable: true,
                                                dataIndex: "descripcion"
                                                ,// use shorthand alias defined above
                                                editor: {allowBlank: false}
                                            }, {
                                                header: "Dominio",
                                                width: 150,
                                                sortable: true,
                                                dataIndex: "dominio",
                                                editor: comboVehiculoOrd, // specify reference to combo instance
                                                renderer: Ext.util.Format.comboRenderer(comboVehiculoOrd) // pass combo instance to reusable renderer
                                                
                                            }, {
                                                header: "Estado",
                                                width: 150,
                                                sortable: true,
                                                dataIndex: "estado",
                                                editor: comboEstadosOrd, // specify reference to combo instance
                                                renderer: Ext.util.Format.comboRenderer(comboEstadosOrd) // pass combo instance to reusable renderer
                                                
                                            }, {
                                                header: "Taller",
                                                width: 150,
                                                sortable: true,
                                                dataIndex: "taller",
                                                editor: comboTallerOrd, // specify reference to combo instance
                                                renderer: Ext.util.Format.comboRenderer(comboTallerOrd) // pass combo instance to reusable renderer
                                                
                                            }, {
                                                header: "De Baja",
                                                width: 70,
                                                sortable: true,
                                                dataIndex: "debaja",
                                                editor: comboDeBaja,
                                                renderer: Ext.util.Format.comboRenderer(comboDeBaja) // pass combo instance to reusable renderer
                                            }
                            ]),

                        viewConfig: {
                            forceFit:true,
                            getRowClass : function(record, rowIndex, p, store){
                                if(this.showPreview){
                                    p.body = '<p>'+record.data.excerpt+'</p>';
                                    return 'x-grid3-row-expanded';
                                }
                                return 'x-grid3-row-collapsed';
                            }
                        },
                        //autoExpandColumn:'company',

                        tbar:[{
                                text:'Insertar un registro',
                                
                                handler: function(){
                                    var e = new Orden();
                                    editorOrden.stopEditing();
                                    Ext.grid.dataOrden.insert(0, e);
                                    //grillaOrden.getView().refresh();
                                    //grillaOrden.getSelectionModel().selectRow(0);
                                    //editorOrden.startEditing(0);
                                }
                                ,
                                tooltip:'Nuevo',
                                iconCls:'add'
                            }, '-',
                            {
                                ref: '../removeBtn',
                                text:'Eliminar un registro',
                                tooltip:'Elimina el registro seleccionado',
                                iconCls:'remove',
                                disabled:true,
                                handler: function()
                                			{
                                                Ext.Msg.alert('Eliminar un registro', 'En esta vista para eliminar un registro debe darlo de baja');
                                            }
                            }
                            
                            ]
                    });

    grillaOrden.getSelectionModel().on('selectionchange', function(sm)
    {

        grillaOrden.removeBtn.setDisabled(sm.getCount() < 1);
    });

}



cargarOrdenObj();


//cargargrillaOrden();
grillaOrden.removeBtn.setDisabled(false);

function formatDate(value)
{
    return value ? value.dateFormat('d/m/Y H:i') : '';
}


function crearVentanaOrdenes()
{
	//Desktop

        MyDesktop.OrdenesTWindow = Ext.extend(Ext.app.Module, {
	    id:'ordenest-win',
	    autoDestroy:false,
	    init : function(){
	
	        this.launcher = {
	            text: '&Oacute;rdenes de Trabajo',
	            iconCls:'icon-ordenest',
	            handler : this.createWindow,
	            scope: this
	        }
	    },
	
	    createWindow : function(){
	        var desktop = this.app.getDesktop();
	        var win = desktop.getWindow('ordenest-win');
	        if(!win){         
	            win = desktop.createWindow({
	                id: 'ordenest-win',
	                title:'&Oacute;rdenes de Trabajo',
	                width:900,
	                height:400,
	                iconCls: 'icon-ordenest',
	                shim:false,
	                animCollapse:false,
	                constrainHeader:true,
                    autoScroll:true,
	                //layout: 'fit',
	                items: [grillaOrden,grillaMprimasCons],
	                listeners: {
	                            beforeshow: function( window ) 
	                            {
	                                cargarOrdenObj();
                                    cargarMprimasConsObj();
	                                Ext.grid.dataOrden.reload();
	                            }
	                        }
	            });
	        }
	        win.show();
	    
	    }
	});
}
crearVentanaOrdenes();

    grillaOrden.getSelectionModel().on('selectionchange', function(sm){
        grillaOrden.removeBtn.setDisabled(sm.getCount() < 1);
    });