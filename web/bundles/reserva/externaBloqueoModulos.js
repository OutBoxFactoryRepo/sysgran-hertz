




	//Desktop


function externaBloqueoModulowin(desktop,idVentana)
{
    win = desktop.createWindow({
        id: idVentana,
        title:'Solo Lectura Módulos',
        width:1200,
        height:500,
        iconCls: 'icon-grid',
        shim:false,
        animCollapse:false,
        constrainHeader:true,
        html:"<iframe src='../../externo/bloqueomodulo.php' style='width:100%;height:100%;border:none'></iframe>",
        autoScroll:true,
        layout: 'fit'	                
        });

    return win;
}

/*
        MyDesktop.ExternaModuloTWindow = Ext.extend(Ext.app.Module, {
	    id:'externaModulo-win',
	    autoDestroy:false,
	    init : function(){
	
	        this.launcher = {
	            text: 'Módulos',
	            iconCls:'icon-modulo',
	            handler : this.createWindow,
	            scope: this
	        };
	    },
	
	    createWindow : function(){
	        var desktop = this.app.getDesktop();
	        var win = desktop.getWindow('externaModulo-win');
	        if(!win){         
	            win = desktop.createWindow({
	                id: 'externaModulo-win',
	                title:'Módulos',
	                width:1800,
	                height:800,
	                iconCls: 'icon-modulo',
	                shim:false,
	                animCollapse:false,
	                constrainHeader:true,
                        html:"<iframe src='../../../externo/modulo.php' style='width:100%;height:100%;border:none'></iframe>",
                        autoScroll:true,
	                layout: 'fit'	                
	            });
	        }
	        win.show();
	    
	    }
	});
*/

