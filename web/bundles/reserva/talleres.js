var fm = Ext.form;
var Taller = null;

Ext.grid.dataTaller = new Ext.data.JsonStore(
    {
            // store configs
            autoDestroy: false,
            autoLoad: {params:{start: 0, limit: 15}},
            url: '../taller/search/',
            storeId: 'myStoreTaller',
            totalProperty: 'results',
            root:'rows',
            remoteSort:false,
            api:
            {
                create : '../taller/crear/',
                read : '../taller/search/',
                update: '../taller/editar/'
                //,destroy: '../web/app_dev.php/Taller/delete/'
            },
            writer: 
                {
                    type: 'post',
                    writeAllFields : true,  //just send changed fields
                    allowSingle :false,     //always wrap in an array
                    encode:false,
                },
                reader: {
                    root: "Violations",
                    type: "json",
                    messageProperty : 'message' //without this, it doesn't work
                },
            // reader configs
             sortInfo: {field: 'id', direction: 'DESC'},
                                idProperty: 'id',
                                fields: [
                                       { name: 'id',type:'integer' },
                                       { name: 'descripcion', type: 'string' },
                                       { name: 'agencia', type: 'string' ,mapping: 'agencia.id'},
                                       { name: 'debaja', type: 'boolean'}]
                });



var comboTipoTaller = null;


var arraySiNo = null;
var comboDeBaja = null;
var editorTaller = null;
var grillaTaller = null;
var storeAgenciaTalleres = null;
function cargarTallerObj()
{

    storeAgenciaTalleres = new Ext.data.JsonStore({
        url: '../agencia/searchadictodas/',
        root: '',
        fields: ['id', 'descripcion']
    });
    storeAgenciaTalleres.load();

    comboAgenciasTaller = new Ext.form.ComboBox({
        typeAhead: true,
        triggerAction: 'all',
        lazyRender:true,
        mode: 'local',
        store: storeAgenciaTalleres,
        valueField: 'id',
        displayField: 'descripcion',
        listeners: {
            // delete the previous query in the beforequery event or set
            // combo.lastQuery = null (this will reload the store the next time it expands)
            beforequery: function(qe){
                delete qe.combo.lastQuery;
            }
        }
    });

    storeTipoTaller = new Ext.data.JsonStore({
        url: '../tipoadicional/search/',
        root: '',
        fields: ['id', 'descripcion']
    });
    storeTipoTaller.load();

    comboTipoTaller = new Ext.form.ComboBox({
        typeAhead: true,
        triggerAction: 'all',
        lazyRender:true,
        mode: 'local',
        store: storeTipoTaller,
        valueField: 'id',
        displayField: 'descripcion',
        listeners: {
            // delete the previous query in the beforequery event or set
            // combo.lastQuery = null (this will reload the store the next time it expands)
            beforequery: function(qe){
                delete qe.combo.lastQuery;
            }
        }
    });


    Taller = Ext.data.Record.create(
        [
                                       { name: 'id',type:'integer' },
                                       { name: 'descripcion', type: 'string' },
                                       { name: 'agencia', type: 'string' ,mapping: 'agencia.id'},
                                       { name: 'debaja', type: 'boolean'}
        ]);

   

    arraySiNo = new Ext.data.ArrayStore({
        id: 0,
        fields: [
            'myId',
            'displayText'
        ],
        data: [[false, 'Alta'], [true, 'De baja']]
    });


    comboDeBaja = new Ext.form.ComboBox({
        typeAhead: true,
        triggerAction: 'all',
        lazyRender:true,
        mode: 'local',
        store: arraySiNo,
        valueField: 'myId',
        displayField: 'displayText'
    });

    editorTaller = new Ext.ux.grid.RowEditor(
    {
        id:"editorTaller",
        saveText  : "Guardar",
        cancelText: "Cancelar",
        //clicksToEdit: 1, //this changes from the default double-click activation to single click activation
        errorSummary: false //disables display of validation messages if the row is invalid
    });

    editorTaller.on(
    {
        canceledit: function(roweditor, changes)
        {
            Ext.grid.dataTaller.reload();
        },
        validateedit: function(roweditor, changes, record, rowIndex)
        {
            console.log(this.isValid());
            if (this.editing && !this.isValid()) 
            {
                return false;
            }
            else
            {
                record.commit();
            }
        },
        afteredit: function(roweditor, changes, record, rowIndex) 
        {
            Ext.grid.dataTaller.reload();
        }
    });

    Ext.util.Format.comboRenderer = function(combo)
    {
        return function(value)
        {
            var record = combo.findRecord(combo.valueField, value);
            return record ? record.get(combo.displayField) : combo.valueNotFoundText;
        }
    };
    grillaTaller = null;

    grillaTaller = new Ext.grid.GridPanel({
                        border:false,
                        
                        //bodyStyle:"padding:100px",
                        width:"100%",
                        height:"200",
                        id:'grillaTaller',
                        plugins: editorTaller,
                        ds: Ext.grid.dataTaller,
                        // paging bar on the bottom
                        bbar: new Ext.PagingToolbar({
                            pageSize: 15,
                            store: Ext.grid.dataTaller,
                            displayInfo: true,
                            displayMsg: 'Mostrando Registros: {0} - {1} of {2}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
                            emptyMsg: "No hay registros"
                            
                        }),
                        cm: new Ext.grid.ColumnModel([
                                            //new Ext.grid.RowNumberer(),
                                            {
                                                header: "ID",
                                                sortable: true,
                                                width: 50,
                                                dataIndex: "id"
                                            }, {
                                                header: "Descripci&oacute;n",
                                                width: 200,
                                                sortable: true,
                                                dataIndex: "descripcion"
                                                ,// use shorthand alias defined above
                                                editor: {allowBlank: false}
                                            },
                                            {
                                                header: "Agencia",
                                                width: 150,
                                                sortable: true,
                                                dataIndex: "agencia",
                                                editor: comboAgenciasTaller, // specify reference to combo instance
                                                renderer: Ext.util.Format.comboRenderer(comboAgenciasTaller) // pass combo instance to reusable renderer
                                                
                                            }
                                            , {
                                                header: "De Baja",
                                                width: 70,
                                                sortable: true,
                                                dataIndex: "debaja",
                                                editor: comboDeBaja,
                                                renderer: Ext.util.Format.comboRenderer(comboDeBaja) // pass combo instance to reusable renderer
                                            }
                            ])
                            ,
                        viewConfig: {
                        	forceFit:true,
                            getRowClass : function(record, rowIndex, p, store){
                                if(this.showPreview){
                                    p.body = '<p>'+record.data.excerpt+'</p>';
                                    return 'x-grid3-row-expanded';
                                }
                                return 'x-grid3-row-collapsed';
                            }
                        },
                        //autoExpandColumn:'company',

                        tbar:[{
                                text:'Insertar un registro',
                                
                                handler: function(){
                                    var e = new Taller();
                                    editorTaller.stopEditing();
                                    Ext.grid.dataTaller.insert(0, e);
                                    //grillaTaller.getView().refresh();
                                    //grillaTaller.getSelectionModel().selectRow(0);
                                    //editorTaller.startEditing(0);
                                }
                                ,
                                tooltip:'Nuevo',
                                iconCls:'add'
                            }, '-',
                            {
                                ref: '../removeBtn',
                                text:'Eliminar un registro',
                                tooltip:'Elimina el registro seleccionado',
                                iconCls:'remove',
                                disabled:true,
                                handler: function(){
                                                /*
                                                editor.stopEditing();
                                                var s = miGrilla.getSelectionModel().getSelections();
                                                for(var i = 0, r; r = s[i]; i++){
                                                    datar.remove(r);
                                                }
                                                */
                                                Ext.Msg.alert('Eliminar un registro', 'En esta vista para eliminar un registro debe darlo de baja');
                                            }
                            }]
              
    				}
    				
    			);
    //Ext.grid.dataTaller.load();
    grillaTaller.getSelectionModel().on('selectionchange', function(sm){
        grillaTaller.removeBtn.setDisabled(sm.getCount() < 1);
    });


}


cargarTallerObj();
//cargargrillaTaller();
grillaTaller.removeBtn.setDisabled(false);

function formatDate(value)
{
    return value ? value.dateFormat('d/m/Y H:i') : '';
}

function crearVentanaTaller()
{
	//Desktop
	MyDesktop.TalleresWindow = Ext.extend(Ext.app.Module, {
	    id:'talleres-win',
	    init : function(){
	
	        this.launcher = {
	            text: 'Talleres',
	            iconCls:'icon-adicionales',
	            handler : this.createWindow,
	            scope: this
	        }
	    },
	
	    createWindow : function(){
	        var desktop = this.app.getDesktop();
	        var win = desktop.getWindow('talleres-win');
	        if(!win){
	   
	            
	            win = desktop.createWindow({
	                id: 'talleres-win',
	                title:'Talleres',
	                width:900,
	                height:400,
                    autoScroll: true,
	                iconCls: 'icon-adicionales',
	                shim:false,
	                animCollapse:false,
	                constrainHeader:true,
	                layout: 'fit',
	                items: [grillaTaller],
	                listeners: {
	                            beforeshow: function( window ) 
	                            {
	                                cargarTallerObj();
	                                Ext.grid.dataTaller.reload();
	                            }
	                        }
	            });
	        }
	        win.show();
	    }
	});

}
crearVentanaTaller();

    grillaTaller.getSelectionModel().on('selectionchange', function(sm){
        grillaTaller.removeBtn.setDisabled(sm.getCount() < 1);
    });