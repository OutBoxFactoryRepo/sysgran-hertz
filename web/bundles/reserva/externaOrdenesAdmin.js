//Desktop

MyDesktop.ExternaOrdenesAdminTWindow = Ext.extend(Ext.app.Module, {
    id:'externaOrdenesAdmin-win',
    autoDestroy:false,
    init : function(){

        this.launcher = {
            text: 'OT Administración',
            iconCls:'icon-ordenesadmin',
            handler : this.createWindow,
            scope: this
        }
    },

    createWindow : function(){
        var desktop = this.app.getDesktop();
        var win = desktop.getWindow('externaOrdenesAdmin-win');
        if(!win){         
            win = desktop.createWindow({
                id: 'externaOrdenesAdmin-win',
                title:'OT Administración',
                width:1000,
                height:500,
                iconCls: 'icon-ordenesadmin',
                shim:false,
                animCollapse:false,
                constrainHeader:true,
                    html:"<iframe src='../../externo/ordenesTrabajoAdmin.php' style='width:100%;height:100%;border:none'></iframe>",
                    autoScroll:true,
                layout: 'fit'	                
            });
        }
        win.show();
    
    }
});
