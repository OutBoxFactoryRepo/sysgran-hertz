


	//Desktop

        MyDesktop.ExternaTalleresTWindow = Ext.extend(Ext.app.Module, {
	    id:'externaTalleres-win',
	    autoDestroy:false,
	    init : function(){
	
	        this.launcher = {
	            text: 'Talleres',
	            iconCls:'icon-adicionales',
	            handler : this.createWindow,
	            scope: this
	        }
	    },
	
	    createWindow : function(){
	        var desktop = this.app.getDesktop();
	        var win = desktop.getWindow('externaTalleres-win');
	        if(!win){         
	            win = desktop.createWindow({
	                id: 'externaTalleres-win',
	                title:'Talleres',
	                width:1000,
	                height:500,
	                iconCls: 'icon-adicionales',
	                shim:false,
	                animCollapse:false,
	                constrainHeader:true,
                        html:"<iframe src='../../externo/talleres.php' style='width:100%;height:100%;border:none'></iframe>",
                        autoScroll:true,
	                layout: 'fit'	                
	            });
	        }
	        win.show();
	    
	    }
	});
