var fm = Ext.form;
var OrdenAuto = null;

Ext.grid.dataOrdenAuto = new Ext.data.JsonStore(
                        {
                                // store configs
                                autoDestroy: false,
                                autoLoad:true,
                                url: '../ordentrabajoauto/search/',
                                storeId: 'myStoreOrdenAuto',
                                api:
                                {
                                    create : '../ordentrabajoauto/crear/',
                                    read : '../ordentrabajoauto/search/',
                                    update: '../ordentrabajoauto/editar/'
                                    //,destroy: '../web/app_dev.php/vehiculo/delete/'
                                },
                                writer: 
                                    {
                                        type: 'post',
                                        writeAllFields : true,  //just send changed fields
                                        allowSingle :false,     //always wrap in an array
                                        encode:false,
                                    },
                                    reader: {
                                        root: "Violations",
                                        type: "json",
                                        messageProperty : 'message' //without this, it doesn't work
                                    },
                                // reader configs
                                sortInfo: {field: 'id', direction: 'DESC'},
                                idProperty: 'id',
                                fields: [
                                       { name: 'id',type:'integer' },
                                       { name: 'descripcion', type: 'string' },
                                       { name: 'dominio', type: 'string' ,mapping: 'vehiculo.id'},
                                       { name: 'kilometros', type: 'integer' ,mapping: 'kilometros'},
                                       { name: 'fecha_carga', type: 'date'},
                                       { name: 'debaja', type: 'boolean'}]
                        });



var comboTipoOrdenAuto = null;


var arraySiNo = null;
var comboDeBaja = null;
var editorOrdenAuto = null;
var grillaOrdenAuto = null;
var storeVehiculo = null;
var comboVehiculo = null;
function cargarOrdenAutoObj()
{
    
	
	
	storeVehiculo = null;
    storeVehiculo = new Ext.data.JsonStore({
        url: '../vehiculo/search/?debaja=false',
        root: '',
        fields: ['id', 'dominio']
    });
    storeVehiculo.load();

    comboVehiculo = new Ext.form.ComboBox({
        typeAhead: true,
        triggerAction: 'all',
        lazyRender:false,
        mode: 'remote',
        store: storeVehiculo,
        valueField: 'id',
        displayField: 'dominio',
        listeners: {
            // delete the previous query in the beforequery event or set
            // combo.lastQuery = null (this will reload the store the next time it expands)
            beforequery: function(qe){
                delete qe.combo.lastQuery;
            }
        }
    });

    OrdenAuto = Ext.data.Record.create(
        [
                       { name: 'id',type:'integer' },
                       { name: 'descripcion', type: 'string' },
                       { name: 'dominio', type: 'string' ,mapping: 'dominio.id'},
                       { name: 'kilometros', type: 'integer' ,mapping: 'kilometros'},
                       { name: 'debaja', type: 'boolean'}
        ]);

   

    arraySiNo = new Ext.data.ArrayStore({
        id: 0,
        fields: [
            'myId',
            'displayText'
        ],
        data: [[false, 'Alta'], [true, 'De baja']]
    });


    comboDeBaja = new Ext.form.ComboBox({
        typeAhead: true,
        triggerAction: 'all',
        lazyRender:true,
        mode: 'local',
        store: arraySiNo,
        valueField: 'myId',
        displayField: 'displayText'
    });

    editorOrdenAuto = new Ext.ux.grid.RowEditor(
    {
        id:"editorOrdenAuto",
        saveText  : "Guardar",
        cancelText: "Cancelar",
        clicksToEdit: 1, //this changes from the default double-click activation to single click activation
        errorSummary: false //disables display of validation messages if the row is invalid
    });

    editorOrdenAuto.on(
    {
        canceledit: function(roweditor, changes)
        {
            Ext.grid.dataOrdenAuto.reload();
        },
        validateedit: function(roweditor, changes, record, rowIndex)
        {
            console.log(this.isValid());
            if (this.editing && !this.isValid()) 
            {
                return false;
            }
            else
            {
                record.commit();
            }
        },
        afteredit: function(roweditor, changes, record, rowIndex) 
        {
            Ext.grid.dataOrdenAuto.reload();
        }
    });

    Ext.util.Format.comboRenderer = function(combo)
    {
        return function(value)
        {
            var record = combo.findRecord(combo.valueField, value);
            return record ? record.get(combo.displayField) : combo.valueNotFoundText;
        }
    };
    grillaOrdenAuto = null;
    grillaOrdenAuto = new Ext.grid.GridPanel({
                        border:false,
                        id:'grillaOrdenAuto',
                        plugins: editorOrdenAuto,
                        ds: Ext.grid.dataOrdenAuto,
                        cm: new Ext.grid.ColumnModel( [
                                            //new Ext.grid.RowNumberer(),
                                            {
                                                header: "ID",
                                                sortable: true,
                                                width: 50,
                                                dataIndex: "id"
                                            }, {
                                                header: "Descripci&oacute;n",
                                                width: 200,
                                                sortable: true,
                                                dataIndex: "descripcion"
                                                ,// use shorthand alias defined above
                                                editor: {allowBlank: false}
                                            }, {
                                                header: "Dominio",
                                                width: 150,
                                                sortable: true,
                                                dataIndex: "dominio",
                                                editor: comboVehiculo, // specify reference to combo instance
                                                renderer: Ext.util.Format.comboRenderer(comboVehiculo) // pass combo instance to reusable renderer
                                                
                                            },
                                            {
                                                header: "Kilometros",
                                                sortable: true,
                                                width: 50,
                                                dataIndex: "kilometros",
                                                editor: {allowBlank: false}
                                            }, {
                                                header: "Fec. de Carga",
                                                width: 150,
                                                sortable: true,
                                                dataIndex: "fecha_carga",
                                                renderer: Ext.util.Format.dateRenderer('d/m/Y')
                                                ,editor: new fm.DateField({
                                                    format: 'd/m/Y',
                                                    minValue: '01/01/2014'
                                                })
                                            }, {
                                                header: "De Baja",
                                                width: 70,
                                                sortable: true,
                                                dataIndex: "debaja",
                                                editor: comboDeBaja,
                                                renderer: Ext.util.Format.comboRenderer(comboDeBaja) // pass combo instance to reusable renderer
                                            }
                            ]),

                        viewConfig: {
                            forceFit:true
                        },
                        //autoExpandColumn:'company',

                        tbar:[{
                                text:'Insertar un registro',
                                
                                handler: function(){
                                    var e = new OrdenAuto();
                                    editorOrdenAuto.stopEditing();
                                    Ext.grid.dataOrdenAuto.insert(0, e);
                                    //grillaOrdenAuto.getView().refresh();
                                    //grillaOrdenAuto.getSelectionModel().selectRow(0);
                                    //editorOrdenAuto.startEditing(0);
                                }
                                ,
                                tooltip:'Nuevo',
                                iconCls:'add'
                            }, '-',
                            {
                                ref: '../removeBtn',
                                text:'Eliminar un registro',
                                tooltip:'Elimina el registro seleccionado',
                                iconCls:'remove',
                                disabled:true,
                                handler: function()
                                			{
                                                Ext.Msg.alert('Eliminar un registro', 'En esta vista para eliminar un registro debe darlo de baja');
                                            }
                            }
                            
                            ]
                    });

    grillaOrdenAuto.getSelectionModel().on('selectionchange', function(sm){
        grillaOrdenAuto.removeBtn.setDisabled(sm.getCount() < 1);
    });


}



cargarOrdenAutoObj();
//cargargrillaOrdenAuto();
grillaOrdenAuto.removeBtn.setDisabled(false);

function formatDate(value)
{
    return value ? value.dateFormat('d/m/Y H:i') : '';
}

//Desktop
MyDesktop.OrdenesAutoTWindow = Ext.extend(Ext.app.Module, {
    id:'ordenestauto-win',
    autoDestroy:false,
    init : function(){

        this.launcher = {
            text: 'OT Autom&aacute;ticas',
            iconCls:'icon-ordenesauto',
            handler : this.createWindow,
            scope: this
        }
    },

    createWindow : function(){
        var desktop = this.app.getDesktop();
        var win = desktop.getWindow('ordenestauto-win');
        if(!win){         
            win = desktop.createWindow({
                id: 'ordenestauto-win',
                title:'&Oacute;rdenes de Trabajo Autom&aacute;ticas',
                width:900,
                height:400,
                iconCls: 'icon-ordenesauto',
                shim:false,
                animCollapse:false,
                constrainHeader:true,
                layout: 'fit',
                items: [grillaOrdenAuto],
                listeners: {
                            beforeshow: function( window ) 
                            {
                                cargarOrdenAutoObj();
                                Ext.grid.dataOrdenAuto.reload();
                            }
                        }
            });
        }
        win.show();
    
    }
});


    grillaOrdenAuto.getSelectionModel().on('selectionchange', function(sm){
        grillaOrdenAuto.removeBtn.setDisabled(sm.getCount() < 1);
    });