
//var arrayModulos = ['ExternaOrdenesTWindow','ExternaOrdenesAutoTWindow','ExternaMprimasTWindow','BogusMenuModule','ExternaVehiculoTWindow','ExternaAdicionalTWindow','ExternaTalleresTWindow','ExternaReservaTWindow'];

MyDesktop = new Ext.app.App({
    init :function(){
        Ext.QuickTips.init();
    },

    getModules : function(){
        
        var modulos = [];
        arrayModulos.forEach(function(entry) 
        {
            
            modulos.push(eval("new MyDesktop."+entry+"()"));
        });
        
        return modulos;
    },

    // config for the start menu
    getStartConfig : function(){
        return {
            title: 'Usuario del Sistema',
            iconCls: 'user',
            toolItems: [{
                text:'Settings',
                iconCls:'settings',
                scope:this
            },'-',{
                text:'Logout',
                iconCls:'logout',
                handler:function()
                { 
                    window.location = "logout"; 
                },
                scope:this
            }            
            
            ]
                        
        };
    }
});


// for example purposes
var windowIndex = 0;

MyDesktop.BogusModule = Ext.extend(Ext.app.Module, {
   crearVentana : function(src){
            
            var desktop = this.app.getDesktop();
            var win = desktop.getWindow(src.ventana);
            if(!win)
            {         
	            win = eval(src.ventana+"(desktop,src.ventana)");

            }
            win.show();
    }
    
});


MyDesktop.BogusMenuModule = Ext.extend(MyDesktop.BogusModule, {
    init : function(){
        this.launcher = {
            text: 'Administración',
            iconCls: 'admin',
            handler: function() {
                return false;
            },
            menu: {
                items:[
                    {
                        text: "Usuarios",
                        iconCls:'user-suit',
                        handler : this.crearVentana,
                        ventana: 'externaUsuariowin',
                        scope: this
                    },
                    {
                        text: "Módulos",
                        iconCls:'icon-modulo',
                        handler : this.crearVentana,
                        ventana: 'externaModulowin',
                        scope: this
                    },
                    {
                        text: "Roles",
                        iconCls:'role',
                        handler : this.crearVentana,
                        ventana: 'externaRolwin',
                        scope: this
                    },
                    {
                        text: "Acceso Modulos",
                        iconCls:'icon-grid',
                        handler : this.crearVentana,
                        ventana: 'externaAccesoModulowin',
                        scope: this
                    },
                    {
                        text: "Solo Lectura Modulos",
                        iconCls:'icon-grid',
                        handler : this.crearVentana,
                        ventana: 'externaBloqueoModulowin',
                        scope: this
                    },
                    {
                        text: "Categorías Vehículos",
                        iconCls:'icon-grid',
                        handler : this.crearVentana,
                        ventana: 'externaCategoriasVeh',
                        scope: this
                    },
                    {
                        text: "Categorías Materias Primas",
                        iconCls:'icon-grid',
                        handler : this.crearVentana,
                        ventana: 'externaMateriaPrimaCategoriawin',
                        scope: this
                    },
                    {
                        text: "Marcas",
                        iconCls:'icon-grid',
                        handler : this.crearVentana,
                        ventana: 'externaMarcawin',
                        scope: this
                    },
                    {
                        text: "Accesorios",
                        iconCls:'icon-grid',
                        handler : this.crearVentana,
                        ventana: 'externaAccesorioswin',
                        scope: this
                    },
                    {
                        text: "Colores",
                        iconCls:'icon-grid',
                        handler : this.crearVentana,
                        ventana: 'externaColoreswin',
                        scope: this
                    },
                    {
                        text: "Reparaciones",
                        iconCls:'icon-grid',
                        handler : this.crearVentana,
                        ventana: 'externaReparacioneswin',
                        scope: this
                    },
                    {
                        text: "Proveedores",
                        iconCls:'icon-grid',
                        handler : this.crearVentana,
                        ventana: 'externaProveedoreswin',
                        scope: this
                    }  ,
                    {
                        text: "Franquiciados",
                        iconCls:'icon-grid',
                        handler : this.crearVentana,
                        ventana: 'externaFranquiciadoswin',
                        scope: this
                    }                 
                ]
            }
        };
    }
});



/*
MyDesktop.BogusMenuModule = Ext.extend(MyDesktop.BogusModule, {
    init : function(){
        this.launcher = {
            text: 'Administración',
            iconCls: 'bogus',
            handler: function() {
                return false;
            },
            menu: {
                items:[{
                    text: "Usuarios",
                    iconCls:'bogus',
                    handler : this.windowUsuarios,
                    scope: this,
                    windowId: windowIndex
                    },{
                    text: 'Externo '+(++windowIndex),
                    iconCls:'bogus',
                    handler : this.createWindow,
                    scope: this,
                    windowId: windowIndex
                    ,htmlx:"<iframe src='../../../web/app_dev.php/user/role' style='width:100%;height:100%;border:none'></iframe>"
                    },{
                    text: 'Bogus Window '+(++windowIndex),
                    iconCls:'bogus',
                    handler : this.createWindow,
                    scope: this,
                    windowId: windowIndex
                    },{
                    text: 'Bogus Window '+(++windowIndex),
                    iconCls:'bogus',
                    handler : this.createWindow,
                    scope: this,
                    windowId: windowIndex
                    },{
                    text: 'Bogus Window '+(++windowIndex),
                    iconCls:'bogus',
                    handler : this.createWindow,
                    scope: this,
                    windowId: windowIndex
                }]
            }
        }
    }
});*/

function abrirMPrimas()
{
    MyDesktop.getModule('externaMprimas-win').createWindow();
}
function abrirVehiculos()
{
 MyDesktop.getModule('externaVehiculo-win').createWindow();   
}