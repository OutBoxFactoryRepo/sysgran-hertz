


	//Desktop

        MyDesktop.ExternaReservaTWindow = Ext.extend(Ext.app.Module, {
	    id:'externaReserva-win',
	    autoDestroy:false,
	    init : function(){
	
	        this.launcher = {
	            text: 'Reservas',
	            iconCls:'icon-reserva',
	            handler : this.createWindow,
	            scope: this
	        }
	    },
	
	    createWindow : function(){
	        var desktop = this.app.getDesktop();
	        var win = desktop.getWindow('externaReserva-win');
	        if(!win){         
	            win = desktop.createWindow({
	                id: 'externaReserva-win',
	                title:'Reserva',
	                width:1200,
	                height:800,
	                iconCls: 'icon-reserva',
	                shim:false,
	                animCollapse:false,
	                constrainHeader:true,
                        html:"<iframe src='../../externo/reserva.php' style='width:100%;height:100%;border:none'></iframe>",
                        autoScroll:true,
	                layout: 'fit'	                
	            });
	        }
	        win.show();
	    
	    }
	});
