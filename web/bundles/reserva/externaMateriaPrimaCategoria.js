//Desktop
function externaMateriaPrimaCategoriawin(desktop,idVentana)
{
    win = desktop.createWindow({
        id: idVentana,
        title:'Categor&iacute;a de Materia Prima',
        width:1200,
        height:500,
        iconCls: 'incon-grid',
        shim:false,
        animCollapse:false,
        constrainHeader:true,
        html:"<iframe src='../../externo/materiaprimacategoria.php' style='width:100%;height:100%;border:none'></iframe>",
        autoScroll:true,
        layout: 'fit'
    });

    return win;
}