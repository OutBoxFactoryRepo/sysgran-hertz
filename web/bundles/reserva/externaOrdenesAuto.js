


	//Desktop

        MyDesktop.ExternaOrdenesAutoTWindow = Ext.extend(Ext.app.Module, {
	    id:'externaOrdenesAuto-win',
	    autoDestroy:false,
	    init : function(){
	
	        this.launcher = {
	            text: 'OT Automáticas',
	            iconCls:'icon-ordenesauto',
	            handler : this.createWindow,
	            scope: this
	        }
	    },
	
	    createWindow : function(){
	        var desktop = this.app.getDesktop();
	        var win = desktop.getWindow('externaOrdenesAuto-win');
	        if(!win){         
	            win = desktop.createWindow({
	                id: 'externaOrdenesAuto-win',
	                title:'OT Automáticas',
	                width:1000,
	                height:500,
	                iconCls: 'icon-ordenesauto',
	                shim:false,
	                animCollapse:false,
	                constrainHeader:true,
                        html:"<iframe src='../../externo/ordenesTrabajoAuto.php' style='width:100%;height:100%;border:none'></iframe>",
                        autoScroll:true,
	                layout: 'fit'	                
	            });
	        }
	        win.show();
	    
	    }
	});
