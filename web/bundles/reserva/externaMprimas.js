


	//Desktop

        MyDesktop.ExternaMprimasTWindow = Ext.extend(Ext.app.Module, {
	    id:'externaMprimas-win',
	    autoDestroy:false,
	    init : function(){
	
	        this.launcher = {
	            text: 'Materias Primas',
	            iconCls:'icon-adicionales',
	            handler : this.createWindow,
	            scope: this
	        }
	    },
	
	    createWindow : function(){
	        var desktop = this.app.getDesktop();
	        var win = desktop.getWindow('externaMprimas-win');
	        if(!win){         
	            win = desktop.createWindow({
	                id: 'externaMprimas-win',
	                title:'Materias Primas',
	                width:1000,
	                height:500,
	                iconCls: 'icon-adicionales',
	                shim:false,
	                animCollapse:false,
	                constrainHeader:true,
                        html:"<iframe src='../../externo/mprimas.php' style='width:100%;height:100%;border:none'></iframe>",
                        autoScroll:true,
	                layout: 'fit'	                
	            });
	        }
	        win.show();
	    
	    }
	});
