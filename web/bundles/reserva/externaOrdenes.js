


	//Desktop

        MyDesktop.ExternaOrdenesTWindow = Ext.extend(Ext.app.Module, {
	    id:'externaOrdenes-win',
	    autoDestroy:false,
	    init : function(){
	
	        this.launcher = {
	            text: '&Oacute;rdenes de Trabajo',
	            iconCls:'icon-ordenest',
	            handler : this.createWindow,
	            scope: this
	        }
	    },
	
	    createWindow : function(){
	        var desktop = this.app.getDesktop();
	        var win = desktop.getWindow('externaOrdenes-win');
	        if(!win){         
	            win = desktop.createWindow({
	                id: 'externaOrdenes-win',
	                title:'&Oacute;rdenes de Trabajo',
	                width:1000,
	                height:500,
	                iconCls: 'icon-ordenest',
	                shim:false,
	                animCollapse:false,
	                constrainHeader:true,
                        html:"<iframe src='../../externo/ordenesTrabajo.php' style='width:100%;height:100%;border:none'></iframe>",
                        autoScroll:true,
	                layout: 'fit'	                
	            });
	        }
	        win.show();
	    
	    }
	});
