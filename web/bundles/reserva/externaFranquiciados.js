//Desktop
function externaFranquiciadoswin(desktop,idVentana)
{
    win = desktop.createWindow({
        id: idVentana,
        title:'Franquiciados',
        width:1200,
        height:500,
        iconCls: 'incon-grid',
        shim:false,
        animCollapse:false,
        constrainHeader:true,
        html:"<iframe src='../../externo/franquiciados.php' style='width:100%;height:100%;border:none'></iframe>",
        autoScroll:true,
        layout: 'fit'                   
        });

    return win;
}