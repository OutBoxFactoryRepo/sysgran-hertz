


	//Desktop
/*
        MyDesktop.ExternaUsuarioTWindow = Ext.extend(Ext.app.Module, {
	    id:'externaUsuario-win',
	    autoDestroy:false,
	    init : function(){
                //return false;
	        this.launcher = {
	            text: 'Usuarios',
	            iconCls:'user-suit',
	            handler : this.createWindow,
	            scope: this
	        };
	    },
	
	    createWindow : function(){
	        var desktop = this.app.getDesktop();
	        var win = desktop.getWindow('externaUsuario-win');
	        if(!win){         
	            win = desktop.createWindow({
	                id: 'externaUsuario-win',
	                title:'Usuarios',
	                width:1200,
	                height:500,
	                iconCls: 'user-suit',
	                shim:false,
	                animCollapse:false,
	                constrainHeader:true,
                        html:"<iframe src='../../../externo/usuarios.php' style='width:100%;height:100%;border:none'></iframe>",
                        autoScroll:true,
	                layout: 'fit'	                
	            });
	        }
	        win.show();
	    
	    }
	});
*/

function externaUsuariowin(desktop,idVentana)
{
    win = desktop.createWindow({
        id: idVentana,
        title:'Usuarios',
        width:1200,
        height:800,
        iconCls: 'user-suit',
        shim:false,
        animCollapse:false,
        constrainHeader:true,
        html:"<iframe src='../../externo/usuarios.php' style='width:100%;height:100%;border:none'></iframe>",
        autoScroll:true,
        layout: 'fit'	                
    });

    return win;
}
