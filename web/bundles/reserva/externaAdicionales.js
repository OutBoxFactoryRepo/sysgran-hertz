


	//Desktop
/*
        MyDesktop.ExternaAdicionalTWindow = Ext.extend(Ext.app.Module, {
	    id:'externaAdicionales-win',
	    autoDestroy:false,
	    init : function(){
	
	        this.launcher = {
	            text: 'Adicionales',
	            iconCls:'icon-adicionales',
	            handler : this.createWindow,
	            scope: this
	        }
	    },
	
	    createWindow : function(){
	        var desktop = this.app.getDesktop();
	        var win = desktop.getWindow('externaAdicionales-win');
	        if(!win){         
	            win = desktop.createWindow({
	                id: 'externaAdicional-win',
	                title:'Adicionales',
	                width:1000,
	                height:500,
	                iconCls: 'icon-adicionales',
	                shim:false,
	                animCollapse:false,
	                constrainHeader:true,
                        html:"<iframe src='../../externo/adicionales.php' style='width:100%;height:100%;border:none'></iframe>",
                        autoScroll:true,
	                layout: 'fit'	                
	            });
	        }
	        win.show();
	    
	    }
	});
*/




	//Desktop

        MyDesktop.ExternaAdicionalTWindow = Ext.extend(Ext.app.Module, {
	    id:'externaAdicional-win',
	    autoDestroy:false,
	    init : function(){
	
	        this.launcher = {
	            text: 'Adicionales',
	            iconCls:'icon-adicionales',
	            handler : this.createWindow,
	            scope: this
	        }
	    },
	
	    createWindow : function(){
	        var desktop = this.app.getDesktop();
	        var win = desktop.getWindow('externaAdicional-win');
	        if(!win){         
	            win = desktop.createWindow({
	                id: 'externaAdicional-win',
	                title:'Adicionales',
	                width:1200,
	                height:800,
	                iconCls: 'icon-adicionales',
	                shim:false,
	                animCollapse:false,
	                constrainHeader:true,
                        html:"<iframe src='../../externo/adicionales.php' style='width:100%;height:100%;border:none'></iframe>",
                        autoScroll:true,
	                layout: 'fit'	                
	            });
	        }
	        win.show();
	    
	    }
	});
