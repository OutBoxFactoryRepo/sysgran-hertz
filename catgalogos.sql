﻿SELECT * FROM tiposIIBB


INSERT INTO tiposIIBB (id, descripcion, debaja) VALUES
(1, 'No Aplica', false),
(2, 'Convenio',false),
(3, 'Local', false),
(4, 'Exento', false);


INSERT INTO condicionIva (id, tipo, porcentaje, descripcion, idCondIvaAfip, idTipoAfip, debaja, detalle) VALUES
(1, 'C', 0, 'Factura C', 5, '011', false, 'Cons. Final, Monotributist y Exento'),
(2, 'A', 0.21, 'Factura A', 5, '001',false, 'IVA discriminado'),
(3, 'B', 0.21, 'Factura B', 5, '006',false, 'Cons. Final, Monotributist y Exento'),
(4, 'R', 0, 'Recibos', NULL, NULL,false, 'Recibos "no suman"'),
(5, 'A', 0.27, 'Factura A (Servicios)', 1, '001',false, 'Iva discriminado de Servicios'),
(6, 'A', 0.105, 'Factura A (Bienes de Uso)', 4, '001',false, 'Iva discriminado para bienes de uso'),
(7, 'E', 0, 'Factura E (Exportacion)', 5, '019',false, 'Consumidor final exterior'),
(8, 'C', 0, 'Factura C2', 5, '011',false, 'Cons. Final, Monotributist y Exento'),
(9, 'N', 0, 'No Aplica', NULL, NULL,false, 'No Aplica');


INSERT INTO tipomoneda (id, descripcion, debaja) VALUES
(1, 'No Aplica',false),
(2, 'Peso Argentino',false),
(3, 'Real Brasilero',false),
(4, 'Peso Chileno',false),
(5, 'Guaraní',false),
(6, 'Sol Peruano',false),
(7, 'Dolar EE.UU.',false),
(8, 'Euro',false);

INSERT INTO cotizamoneda (id, fechaAlta, cotizacion, idmoneda, cotizacionVenta) VALUES
(37, '2015-07-13 00:20:22', 1.01, 2, 0),
(38, '2015-07-13 00:20:31', 0.5, 2, 0),
(39, '2015-07-13 00:20:35', 1, 2, 0),
(40, '2015-07-13 00:21:33', 8.5, 7, 0),
(41, '2015-07-13 00:21:37', 9.5, 7, 0),
(42, '2015-07-13 00:21:39', 10.5, 7, 0),
(43, '2015-07-13 00:21:48', 10.5, 8, 0),
(44, '2015-07-13 00:21:57', 15, 8, 0),
(45, '2015-07-13 00:22:19', 120000.55, 4, 0),
(46, '2015-07-13 00:22:27', 1200000.55, 5, 0),
(47, '2015-07-15 15:14:47', 50000, 4, 0),
(48, '2015-07-15 15:15:22', 10000, 5, 10021);

INSERT INTO tipodoc (id, descripcion, debaja) VALUES
(1, 'D.U.I',false),
(2, 'Pasaporte',false),
(3, 'C.I. Argentina',false),
(4, 'Cédula Mercosur',false);

INSERT INTO tipocliente (id, descripcion, debaja) VALUES
(1, 'Responsable',false),
(2, 'Pagador', false),
(3, 'Conductor Principal',false),
(4, 'Conductor Adicional',false),
(5, 'Pasajero',false);

INSERT INTO metodosPago (id, descripcion, debaja) VALUES
(1, 'Efectivo',false),
(2, 'Tarjeta de Débito',false),
(3, 'Tarjeta de Crédito', false);

SELECT * FROM condicionIva