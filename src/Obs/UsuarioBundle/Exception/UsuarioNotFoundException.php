<?php
namespace Obs\UsuarioBundle\Exception;

class UsuarioNotFoundException extends \Exception{

	private $globalError = array();

	public function setError($message)
	{
		$this->globalError[] = $message;		
	}

	public function getError()
	{
		return $this->globalError;
	}

	public function isException()
	{
		return (count($this->globalError) > 0 ? true : false);
	}

	public function getAllError()
	{
		return array(
				'error' => $this->globalError
		);
	}

	public function __toString()
	{
		return json_encode($this->getError());
	}
}
