<?php
namespace Obs\UsuarioBundle\Exception;

class InvalidParametersException extends \Exception{

	private $globalError = array();

	private $parameterError = array();

	public function setGlobalError($message)
	{
		$this->globalError[] = $message;		
	}

	public function getGlobalError()
	{
		return $this->globalError;
	}

	public function setParameterError($key, $value)
	{
		$this->parameterError[$key][] = $value;
	}

	public function setParametersError($error)
	{
		$this->parameterError = array_merge($this->parameterError, $error);
	}

	public function getParameterError()
	{
		return $this->parameterError;
	}

	public function isException()
	{
		return (count($this->globalError) + count($this->parameterError) > 0 ? true : false);
	}

	public function getAllError()
	{
		return array(
				'globalError' => $this->globalError,
				'parameterError'=> $this->parameterError
		);
	}

	public function __toString()
	{
		return json_encode($this->getAllError());
	}
}
