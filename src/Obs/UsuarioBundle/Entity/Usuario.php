<?php
namespace Obs\UsuarioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\MaxDepth;

/**
 * Usuario
 *
 * @ORM\Table(name="usuarios")
 * @ORM\Entity(repositoryClass="UsuarioRepository")
 */
class Usuario{
	
	const ORM_ENTITY = "ObsUsuarioBundle:Usuario";

	/**
	 *
	 * @var integer
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @Assert\Type(type="integer", message="Debe ser entero mayor a 0|id") 
	 */
	private $id;

	/**
	 *
	 * @var string
	 *
	 * @ORM\Column(name="nombre", type="string", length=100, nullable=false)
	 * @Assert\NotNull(message="El campo nombre es obligatorio.|nombre")
	 */
	private $nombre;

	/**
	 *
	 * @var integer
	 *
	 * @ORM\Column(name="ministerio_id", type="integer", nullable=false)
	 * @Assert\NotNull(message="El campo nombre es obligatorio.|ministerioId")
	 * @Assert\Regex(pattern= "/[0-9]/", message = "Debe ser entero mayor a 0|ministerioId")
	 */
	private $ministerioId;

	/**
	 *
	 * @var boolean
	 *
	 * @ORM\Column(name="escribe_chip", type="boolean", nullable=false)
	 * @Assert\NotNull(message="El campo escribeChip es obligatorio.|escribeChip")
	 * @Assert\Regex(pattern= "/(0|1)/", message = "Debe ser boolean 0 o 1|escribeChip")  
	 */
	private $escribeChip;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Usuario
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    
        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set ministerioId
     *
     * @param integer $ministerioId
     * @return Usuario
     */
    public function setMinisterioId($ministerioId)
    {
        $this->ministerioId = $ministerioId;
    
        return $this;
    }

    /**
     * Get ministerioId
     *
     * @return integer 
     */
    public function getMinisterioId()
    {
        return $this->ministerioId;
    }

    /**
     * Set escribeChip
     *
     * @param boolean $escribeChip
     * @return Usuario
     */
    public function setEscribeChip($escribeChip)
    {
        $this->escribeChip = $escribeChip;
    
        return $this;
    }

    /**
     * Get escribeChip
     *
     * @return boolean 
     */
    public function getEscribeChip()
    {
        return $this->escribeChip;
    }
}