<?php
namespace Obs\UsuarioBundle\Service;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Pagination\Paginator;

use Obs\UsuarioBundle\Entity\Usuario;

class SearchService{
	/** 
	 * 
	 * @var EntityManager
	 * 
	 */
	private $em;

	public function __construct(EntityManager $em){
		$this->em = $em;
		$config = $em->getConfiguration();
	}

	public function getAll()
	{
		return $this->em->getRepository(Usuario::ORM_ENTITY)->getAll();
	}
}
