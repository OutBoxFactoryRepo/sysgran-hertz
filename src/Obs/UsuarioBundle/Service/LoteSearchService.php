<?php
namespace Tarjeta\TarjetaBundle\Service;

use Symfony\Component\Validator\Constraints as Assert;

use Doctrine\ORM\EntityManager;

use Tarjeta\TarjetaBundle\Exception\LoteNotFoundException;
use Tarjeta\TarjetaBundle\Exception\InvalidParametersException;

use Tarjeta\TarjetaBundle\Entity\Lote;

class LoteSearchService{
	/** 
	 * 
	 * @var EntityManager
	 */
	private $em;

	public function __construct(EntityManager $em){
		$this->em = $em;
	}

	public function getAll()
	{
		return $this->em->getRepository(Lote::ORM_ENTITY)->getAll();
	}
}