<?php

namespace Obs\UsuarioBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\Serializer\SerializerBuilder;
use JMS\DiExtraBundle\Annotation as DI;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use Obs\UsuarioBundle\Exception\InvalidParametersException;
use Obs\UsuarioBundle\Exception\UsuarioNotFoundException;
use Obs\UsuarioBundle\Service\SearchService;

class UsuarioController extends Controller
{
	/**
	 *
	 * @var SearchService
	 *
	 * @DI\Inject("SearchService.search")
	 */
	private $searchSvc;

    /**
     * @Route("/usuario/")
     * @Method({"GET"})
     * 
     * @ApiDoc(
     * 		description = "Devuelve una lista de sedes",
     * 		requirements = {},
	 *      parameters={}
     * )
     */
    public function getAllAction()
    {
    	
    		$result = $this->searchSvc->getAll();
			$serializer = SerializerBuilder::create()->build();
    		$result = $serializer->serialize($result, 'json');
    		$httpError = 200;
    	
    	

    	$response = new Response($result, $httpError);
    	$response->headers->set('Content-Type', 'application/json');
    	return $response;
    }

}
