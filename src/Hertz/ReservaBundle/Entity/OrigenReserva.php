<?php

namespace Hertz\ReservaBundle\Entity;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\DiExtraBundle\Annotation as DI;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Doctrine\ORM\EntityManager;
use Doctrine\Common\Collections\ArrayCollection;

use Doctrine\ORM\Mapping as ORM;

/**
 * OrigenReserva
 * @ORM\Table(name="origenReserva")
 * @ORM\Entity(repositoryClass="OrigenReservaRepository")
 */
class OrigenReserva
{
    const ORM_ENTITY = "HertzReservaBundle:OrigenReserva";
    
    /**
     *
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Assert\Type(type="integer", message="Debe ser entero mayor a 0") 
     */
    private $id;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=255, nullable=false)
     * @Assert\NotBlank(message="El campo es obligatorio.")
     * @Assert\Length(
     *      min = "2",
     *      max = "255",
     *      minMessage = "Debe tener al menos {{ limit }} caracteres de largo",
     *      maxMessage = "Debe tener un máximo de {{ limit }} caracteres"
     * )
     */
    private $descripcion;

    /**
     *
     * @var boolean
     *
     * @ORM\Column(name="debaja", type="boolean", nullable=false)
     * @Assert\Choice(choices = {true,false}, message = "Elección incorrecta (Y , S , N).")
     */
    private $debaja;

    
    /**
     *
     * @var boolean
     *
     * @ORM\Column(name="tieneNombre", type="boolean", nullable=false,options={"default":false})
     * @Assert\Choice(choices = {true,false}, message = "Elección incorrecta (Y , S , N).")
     */
    private $tieneNombre;
    
    /**
     *
     * @var integer
     * 
     * @ORM\Column(name="idPadre", type="integer", nullable=true,options={"default":"0"})
     * @Assert\Type(type="integer", message="Debe ser entero mayor a 0") 
     */
    private $idPadre;
    

    


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return OrigenReserva
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set debaja
     *
     * @param boolean $debaja
     * @return OrigenReserva
     */
    public function setDebaja($debaja)
    {
        $this->debaja = $debaja;

        return $this;
    }

    /**
     * Get debaja
     *
     * @return boolean 
     */
    public function getDebaja()
    {
        return $this->debaja;
    }

    /**
     * Set idPadre
     *
     * @param integer $idPadre
     * @return OrigenReserva
     */
    public function setIdPadre($idPadre)
    {
        $this->idPadre = $idPadre;

        return $this;
    }

    /**
     * Get idPadre
     *
     * @return integer 
     */
    public function getIdPadre()
    {
        return $this->idPadre;
    }

    /**
     * Set tieneNombre
     *
     * @param boolean $tieneNombre
     * @return OrigenReserva
     */
    public function setTieneNombre($tieneNombre)
    {
        $this->tieneNombre = $tieneNombre;

        return $this;
    }

    /**
     * Get tieneNombre
     *
     * @return boolean 
     */
    public function getTieneNombre()
    {
        return $this->tieneNombre;
    }
}
