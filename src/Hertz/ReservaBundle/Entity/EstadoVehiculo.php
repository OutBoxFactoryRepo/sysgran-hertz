<?php
namespace Hertz\ReservaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\DiExtraBundle\Annotation as DI;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Doctrine\ORM\EntityManager;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * EstadoVehiculo
 *
 * @ORM\Table(name="estadosVehiculos")
 * @ORM\Entity(repositoryClass="EstadoVehiculoRepository")
 */
class EstadoVehiculo{
	
	const ORM_ENTITY = "HertzReservaBundle:EstadoVehiculo";

	/**
	 *
	 * @var integer
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @Assert\Type(type="integer", message="Debe ser entero mayor a 0") 
	 */
	private $id;

	/**
	 *
	 * @var string
	 *
	 * @ORM\Column(name="descripcion", type="string", length=255, nullable=false)
	 * @Assert\NotBlank(message="El campo es obligatorio.")
	 * @Assert\Length(
     *      min = "2",
     *      max = "255",
     *      minMessage = "Debe tener al menos {{ limit }} caracteres de largo",
     *      maxMessage = "Debe tener un máximo de {{ limit }} caracteres"
     * )
	 */
	private $descripcion;
	
		
	/**
	 *
	 * @var boolean
	 *
	 * @ORM\Column(name="debaja", type="boolean", nullable=false)
	 * @Assert\NotNull(message="El campo es obligatorio.")
	 */
	private $debaja;


   
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return EstadoVehiculo
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set debaja
     *
     * @param string $debaja
     * @return EstadoVehiculo
     */
    public function setDebaja($debaja)
    {
        $this->debaja = $debaja;

        return $this;
    }

    /**
     * Get debaja
     *
     * @return string 
     */
    public function getDebaja()
    {
        return $this->debaja;
    }
}
