<?php
namespace Hertz\ReservaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\DiExtraBundle\Annotation as DI;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Doctrine\ORM\EntityManager;
use Hertz\ReservaBundle\Entity\TipoDoc;
use Doctrine\Common\Collections\ArrayCollection;
use Hertz\ReservaBundle\Entity\ClienteOtrosDatos;

/**
 * Cliente
 *
 * @ORM\Table(name="clientes")
 * @ORM\Entity(repositoryClass="ClienteRepository")
 */
class Cliente{
	
	const ORM_ENTITY = "HertzReservaBundle:Cliente";
	
	/**
	 *
	 * @var integer
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @Assert\Type(type="integer", message="Debe ser entero mayor a 0|id") 
	 */
	private $id;

	/**
	 *
	 * @var string
	 *
	 * @ORM\Column(name="nombre", type="string", length=255, nullable=false)
	 * @Assert\NotNull(message="El campo Nombre es Obligatorio.|nombre")
	 */
	private $nombre;

	/**
	 *
	 * @var string
	 *
	 * @ORM\Column(name="apellido", type="string", length=255, nullable=false)
	 * @Assert\NotNull(message="El campo Apellido es Obligatorio.|apellido")
	 */
	private $apellido;
	
	/**
	 *
	 * @var datetime
	 *
	 * @ORM\Column(name="fechaNac", type="datetime", nullable=true)
	 * @Assert\NotNull(message="El campo Fecha de Nacimiento es Obligatorio.|fechaNac")
	 */
	private $fechaNac;
	
	/**
	 *
	 * @var string
	 *
	 * @ORM\Column(name="email", type="string", length=255, nullable=true)
	 * @Assert\NotNull(message="El campo Email es Obligatorio.|email")
	 */
	private $email;
	
        /**
	 *
	 * @var string
	 *
	 * @ORM\Column(name="telefono", type="string", length=255, nullable=true)
	 * @Assert\NotNull(message="El campo Email es Obligatorio.|email")
	 */
	private $telefono;
        
		
	/**
	 *
	 * @var string
	 *
	 * @ORM\Column(name="numeroDoc", type="string", length=255 ,  nullable=false)
	 * @Assert\NotNull(message="El campo Número de documento es obligatorio.|cs")
	 */
	private $numeroDoc;
	
	
	/**
	 *
	 * @var boolean
	 *
	 * @ORM\Column(name="debaja", type="boolean")
	 */
	private $debaja;
	
	/**
	 * 
	 *
	 *@ORM\ManyToOne(targetEntity="Hertz\ReservaBundle\Entity\TipoDoc",  cascade={"remove"})
     *@ORM\JoinColumn(name="tipodoc", referencedColumnName="id", nullable=false )
	 * 
	 */
	private $tipodoc;
	
	/**
	 *
	 *
	 *@ORM\ManyToOne(targetEntity="Hertz\ReservaBundle\Entity\Pais",  cascade={"remove"})
	 *@ORM\JoinColumn(name="idpais", referencedColumnName="id", nullable=true )
	 *
	 */
	private $idpais;
	

	/**
	 * @ORM\ManyToMany(targetEntity="Hertz\ReservaBundle\Entity\TipoCliente", mappedBy="tipos", cascade={"persist"})
	 */
	private $tipoCliente;
	

	
	/**
	 *
	 * @var string
	 *
	 * @ORM\Column(name="domicilio", type="string", length=250, nullable=true)
	 */
	private $domicilio;
	
	/**
	 *
	 * @var string
	 *
	 * @ORM\Column(name="localidad", type="string", length=250, nullable=true)
	 */
	private $localidad;
	
	/**
	 *
	 * @var string
	 *
	 * @ORM\Column(name="codigoPostal", type="string", length=250, nullable=true)
	 */
	private $codigoPostal;
	
	/**
	 *
	 *
	 *@ORM\ManyToOne(targetEntity="Hertz\ReservaBundle\Entity\Provincias",  cascade={"remove"})
	 *@ORM\JoinColumn(name="provincia", referencedColumnName="id", nullable=true )
	 *
	 */
	private $provincia;
        
        /**
	 * @ORM\ManyToMany(targetEntity="Hertz\ReservaBundle\Entity\Reserva", mappedBy="clientesAdicionales", cascade={"persist"})
	 */
	private $idReserva;
        
        /**
	 * @ORM\ManyToMany(targetEntity="Hertz\ReservaBundle\Entity\ClienteOtrosDatos", mappedBy="cliente", cascade={"persist"})
	 */
	private $otrosDatos;
	

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->tipoCliente = new \Doctrine\Common\Collections\ArrayCollection();
        $this->idReserva = new \Doctrine\Common\Collections\ArrayCollection();
        $this->otrosDatos = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Cliente
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set apellido
     *
     * @param string $apellido
     * @return Cliente
     */
    public function setApellido($apellido)
    {
        $this->apellido = $apellido;

        return $this;
    }

    /**
     * Get apellido
     *
     * @return string 
     */
    public function getApellido()
    {
        return $this->apellido;
    }

    /**
     * Set fechaNac
     *
     * @param \DateTime $fechaNac
     * @return Cliente
     */
    public function setFechaNac($fechaNac)
    {
        $this->fechaNac = $fechaNac;

        return $this;
    }

    /**
     * Get fechaNac
     *
     * @return \DateTime 
     */
    public function getFechaNac()
    {
        return $this->fechaNac;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Cliente
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set numeroDoc
     *
     * @param string $numeroDoc
     * @return Cliente
     */
    public function setNumeroDoc($numeroDoc)
    {
        $this->numeroDoc = $numeroDoc;

        return $this;
    }

    /**
     * Get numeroDoc
     *
     * @return string 
     */
    public function getNumeroDoc()
    {
        return $this->numeroDoc;
    }

    /**
     * Set debaja
     *
     * @param boolean $debaja
     * @return Cliente
     */
    public function setDebaja($debaja)
    {
        $this->debaja = $debaja;

        return $this;
    }

    /**
     * Get debaja
     *
     * @return boolean 
     */
    public function getDebaja()
    {
        return $this->debaja;
    }

    /**
     * Set domicilio
     *
     * @param string $domicilio
     * @return Cliente
     */
    public function setDomicilio($domicilio)
    {
        $this->domicilio = $domicilio;

        return $this;
    }

    /**
     * Get domicilio
     *
     * @return string 
     */
    public function getDomicilio()
    {
        return $this->domicilio;
    }

    /**
     * Set localidad
     *
     * @param string $localidad
     * @return Cliente
     */
    public function setLocalidad($localidad)
    {
        $this->localidad = $localidad;

        return $this;
    }

    /**
     * Get localidad
     *
     * @return string 
     */
    public function getLocalidad()
    {
        return $this->localidad;
    }

    /**
     * Set codigoPostal
     *
     * @param string $codigoPostal
     * @return Cliente
     */
    public function setCodigoPostal($codigoPostal)
    {
        $this->codigoPostal = $codigoPostal;

        return $this;
    }

    /**
     * Get codigoPostal
     *
     * @return string 
     */
    public function getCodigoPostal()
    {
        return $this->codigoPostal;
    }

    /**
     * Set tipodoc
     *
     * @param \Hertz\ReservaBundle\Entity\TipoDoc $tipodoc
     * @return Cliente
     */
    public function setTipodoc(\Hertz\ReservaBundle\Entity\TipoDoc $tipodoc)
    {
        $this->tipodoc = $tipodoc;

        return $this;
    }

    /**
     * Get tipodoc
     *
     * @return \Hertz\ReservaBundle\Entity\TipoDoc 
     */
    public function getTipodoc()
    {
        return $this->tipodoc;
    }

    /**
     * Set idpais
     *
     * @param \Hertz\ReservaBundle\Entity\Pais $idpais
     * @return Cliente
     */
    public function setIdpais(\Hertz\ReservaBundle\Entity\Pais $idpais)
    {
        $this->idpais = $idpais;

        return $this;
    }

    /**
     * Get idpais
     *
     * @return \Hertz\ReservaBundle\Entity\Pais 
     */
    public function getIdpais()
    {
        return $this->idpais;
    }

    /**
     * Add tipoCliente
     *
     * @param \Hertz\ReservaBundle\Entity\TipoCliente $tipoCliente
     * @return Cliente
     */
    public function addTipoCliente(\Hertz\ReservaBundle\Entity\TipoCliente $tipoCliente)
    {
    	$tipoCliente->addTipo($this); // Call Post's setter here
        $this->tipoCliente[] = $tipoCliente;

        return $this;
        
        
    }



    /**
     * Remove tipoCliente
     *
     * @param \Hertz\ReservaBundle\Entity\TipoCliente $tipoCliente
     */
    public function removeTipoCliente(\Hertz\ReservaBundle\Entity\TipoCliente $tipoCliente)
    {
        $this->tipoCliente->removeElement($tipoCliente);
    }

    /**
     * Get tipoCliente
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTipoCliente()
    {
        return $this->tipoCliente;
    }

    /**
     * Set provincia
     *
     * @param \Hertz\ReservaBundle\Entity\Provincias $provincia
     * @return Cliente
     */
    public function setProvincia(\Hertz\ReservaBundle\Entity\Provincias $provincia)
    {
        $this->provincia = $provincia;

        return $this;
    }

    /**
     * Get provincia
     *
     * @return \Hertz\ReservaBundle\Entity\Provincias 
     */
    public function getProvincia()
    {
        return $this->provincia;
    }

    /**
     * Add idReserva
     *
     * @param \Hertz\ReservaBundle\Entity\Reserva $idReserva
     * @return Cliente
     */
    public function addIdReserva(\Hertz\ReservaBundle\Entity\Reserva $idReserva)
    {

        $this->idReserva[] = $idReserva;

        return $this;
    }

    /**
     * Remove idReserva
     *
     * @param \Hertz\ReservaBundle\Entity\Reserva $idReserva
     */
    public function removeIdReserva(\Hertz\ReservaBundle\Entity\Reserva $idReserva)
    {
        $this->idReserva->removeElement($idReserva);
    }

    /**
     * Get idReserva
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getIdReserva()
    {
        return $this->idReserva;
    }

    /**
     * Add otrosDatos
     *
     * @param \Hertz\ReservaBundle\Entity\ClienteOtrosDatos $otrosDatos
     * @return Cliente
     */
    public function addOtrosDatos(\Hertz\ReservaBundle\Entity\ClienteOtrosDatos $otrosDatos)
    {
        $otrosDatos->addCliente($this); // Call Post's setter here
        $this->otrosDatos[] = $otrosDatos;

        return $this;
    }

    /**
     * Remove otrosDatos
     *
     * @param \Hertz\ReservaBundle\Entity\ClienteOtrosDatos $otrosDatos
     */
    public function removeOtrosDato(\Hertz\ReservaBundle\Entity\ClienteOtrosDatos $otrosDatos)
    {
        $otrosDatos->removeCliente($this); // Call Post's setter here
        $this->otrosDatos->removeElement($otrosDatos);
    }

    /**
     * Get otrosDatos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getOtrosDatos()
    {
        return $this->otrosDatos;
    }

    /**
     * Add otrosDatos
     *
     * @param \Hertz\ReservaBundle\Entity\ClienteOtrosDatos $otrosDatos
     * @return Cliente
     */
    public function addOtrosDato(\Hertz\ReservaBundle\Entity\ClienteOtrosDatos $otrosDatos)
    {
        $this->otrosDatos[] = $otrosDatos;

        return $this;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     * @return Cliente
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get telefono
     *
     * @return string 
     */
    public function getTelefono()
    {
        return $this->telefono;
    }
}
