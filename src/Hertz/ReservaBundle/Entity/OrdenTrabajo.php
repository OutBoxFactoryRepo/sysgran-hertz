<?php
namespace Hertz\ReservaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\DiExtraBundle\Annotation as DI;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Doctrine\ORM\EntityManager;

/**
 * OrdenTrabajo
 *
 * @ORM\Table(name="ordenesTrabajo")
 * @ORM\Entity(repositoryClass="OrdenTrabajoRepository")
 */
class OrdenTrabajo
{

    const ORM_ENTITY = "HertzReservaBundle:OrdenTrabajo";

    /**
     *
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Assert\Type(type="integer", message="Debe ser entero mayor a 0")
     */
    private $id;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=255, nullable=false)
     * @Assert\NotBlank(message="El campo Descripción es obligatorio.")
     * @Assert\Length(
     *      min = "2",
     *      max = "255",
     *      minMessage = "Debe tener al menos {{ limit }} caracteres de largo",
     *      maxMessage = "Debe tener un máximo de {{ limit }} caracteres"
     * )
     */
    private $descripcion;


    /**
     *
     *
     * @ORM\ManyToOne(targetEntity="Hertz\ReservaBundle\Entity\Vehiculo",  cascade={"persist"})
     * @ORM\JoinColumn(name="vehiculo", referencedColumnName="id", nullable=false )
     *
     */
    private $vehiculo;

    /**
     *
     * @var datetime
     *
     * @ORM\Column(name="fechaCarga", type="datetime", nullable=false)
     * @Assert\NotBlank(message="El campo fecha es obligatorio.")
     */
    private $fechaCarga;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="usuario", type="string", length=255, nullable=false)
     * @Assert\NotBlank(message="El campo usuario es obligatorio.")
     * @Assert\Length(
     *      min = "2",
     *      max = "255",
     *      minMessage = "Debe tener al menos {{ limit }} caracteres de largo",
     *      maxMessage = "Debe tener un máximo de {{ limit }} caracteres"
     * )
     */
    private $usuario;

    /**
     *
     *
     * @ORM\ManyToOne(targetEntity="Hertz\ReservaBundle\Entity\Taller",  cascade={"persist"})
     * @ORM\JoinColumn(name="taller", referencedColumnName="id", nullable=false )
     *
     */
    private $taller;

    /**
     *
     *
     * @ORM\ManyToOne(targetEntity="Hertz\ReservaBundle\Entity\TipoComprobante",  cascade={"persist"})
     * @ORM\JoinColumn(name="tipoComprobante", referencedColumnName="id", nullable=true )
     *
     */
    private $tipoComprobante;


    /**
     *
     *
     * @ORM\ManyToOne(targetEntity="Hertz\ReservaBundle\Entity\EstadosOt",  cascade={"persist"})
     * @ORM\JoinColumn(name="estado", referencedColumnName="id", nullable=false )
     *
     */
    private $estado;

    /**
     *
     *
     * @ORM\ManyToOne(targetEntity="Hertz\ReservaBundle\Entity\EstadosOtAdmin", cascade={"persist"})
     * @ORM\JoinColumn(name="estadoadmin", referencedColumnName="id", nullable=false )
     *
     */
    private $estadoadmin;


    /**
     *
     * @ORM\ManyToOne(targetEntity="Hertz\ReservaBundle\Entity\Agencia",  cascade={"persist"})
     * @ORM\JoinColumn(name="agencia", referencedColumnName="id", nullable=false )
     * @Assert\NotBlank(message="El campo Agencia es obligatorio.")
     * @Assert\Type(type="object", message="El valor {{ value }} no es válido debe ser {{ type }}.")
     *
     */
    private $agencia;

    /**
     *
     * @var boolean
     *
     * @ORM\Column(name="debaja", type="boolean", nullable=false)
     * @Assert\Choice(choices = {true,false}, message = "Elección incorrecta (Y , S , N).")
     */
    private $debaja;

    /**
     *
     * @var boolean
     *
     * @ORM\Column(name="pagado", type="boolean", nullable=true)
     */
    private $pagado;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="nrofactura", type="string", length=100, nullable=true)
     * @Assert\Length(
     *      max = "100",
     *      maxMessage = "Debe tener un máximo de {{ limit }} caracteres"
     * )
     */
    private $nrofactura;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="tareas", type="text", nullable=true)
     */
    private $tareas;

    /**
     * @ORM\OneToMany(targetEntity="OrdenTrabajoReparacionesTareas", mappedBy="ot")
     */
    private $otReparacionesTareas;

    /**
     * @return bool
     */
    public function isPagado()
    {
        return $this->pagado ? true : false;
    }

    /**
     * @param bool $pagado
     */
    public function setPagado($pagado)
    {
        $this->pagado = $pagado;
    }



    /**
     * @return mixed
     */
    public function getOtReparacionesTareas()
    {
        return $this->otReparacionesTareas[0];
    }

    /**
     * @param mixed $otReparacionesTareas
     */
    public function setOtReparacionesTareas($otReparacionesTareas)
    {
        $this->otReparacionesTareas = $otReparacionesTareas;
    }

    /**
     *
     * @var float
     *
     * @ORM\Column(name="total", type="float", nullable=true)
     */
    private $total;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return OrdenTrabajo
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set fechaCarga
     *
     * @param \DateTime $fechaCarga
     * @return OrdenTrabajo
     */
    public function setFechaCarga($fechaCarga)
    {
        $this->fechaCarga = $fechaCarga;

        return $this;
    }

    /**
     * Get fechaCarga
     *
     * @return \DateTime
     */
    public function getFechaCarga()
    {
        return $this->fechaCarga;
    }

    /**
     * Set usuario
     *
     * @param string $usuario
     * @return OrdenTrabajo
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return string
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set debaja
     *
     * @param boolean $debaja
     * @return OrdenTrabajo
     */
    public function setDebaja($debaja)
    {
        $this->debaja = $debaja;

        return $this;
    }

    /**
     * Get debaja
     *
     * @return boolean
     */
    public function getDebaja()
    {
        return $this->debaja;
    }

    /**
     * Set vehiculo
     *
     * @param \Hertz\ReservaBundle\Entity\Vehiculo $vehiculo
     * @return OrdenTrabajo
     */
    public function setVehiculo(\Hertz\ReservaBundle\Entity\Vehiculo $vehiculo)
    {
        $this->vehiculo = $vehiculo;

        return $this;
    }

    /**
     * Get vehiculo
     *
     * @return \Hertz\ReservaBundle\Entity\Vehiculo
     */
    public function getVehiculo()
    {
        return $this->vehiculo;
    }

    /**
     * Set taller
     *
     * @param \Hertz\ReservaBundle\Entity\Taller $taller
     * @return OrdenTrabajo
     */
    public function setTaller(\Hertz\ReservaBundle\Entity\Taller $taller)
    {
        $this->taller = $taller;

        return $this;
    }

    /**
     * Get taller
     *
     * @return \Hertz\ReservaBundle\Entity\Taller
     */
    public function getTaller()
    {
        return $this->taller;
    }

    /**
     * Set estado
     *
     * @param \Hertz\ReservaBundle\Entity\EstadosOt $estado
     * @return OrdenTrabajo
     */
    public function setEstado(\Hertz\ReservaBundle\Entity\EstadosOt $estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return \Hertz\ReservaBundle\Entity\EstadosOt
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set agencia
     *
     * @param \Hertz\ReservaBundle\Entity\Agencia $agencia
     * @return OrdenTrabajo
     */
    public function setAgencia(\Hertz\ReservaBundle\Entity\Agencia $agencia)
    {
        $this->agencia = $agencia;

        return $this;
    }

    /**
     * Get agencia
     *
     * @return \Hertz\ReservaBundle\Entity\Agencia
     */
    public function getAgencia()
    {
        return $this->agencia;
    }

    /**
     * Set estadoadmin
     *
     * @param \Hertz\ReservaBundle\Entity\EstadosOtAdmin $estadoadmin
     * @return OrdenTrabajo
     */
    public function setEstadoAdmin(\Hertz\ReservaBundle\Entity\EstadosOtAdmin $estadoadmin)
    {
        $this->estadoadmin = $estadoadmin;

        return $this;
    }

    /**
     * Get estadoadmin
     *
     * @return \Hertz\ReservaBundle\Entity\EstadosOtAdmin
     */
    public function getEstadoAdmin()
    {
        return $this->estadoadmin;
    }

    /**
     * Set nrofactura
     *
     * @param string $nrofactura
     * @return OrdenTrabajo
     */
    public function setNroFactura($nrofactura)
    {
        $this->nrofactura = $nrofactura;

        return $this;
    }

    /**
     * Get nrofactura
     *
     * @return string
     */
    public function getNroFactura()
    {
        return $this->nrofactura;
    }

    /**
     * Set tipoComprobante
     *
     * @param \Hertz\ReservaBundle\Entity\TipoComprobante $tipoComprobante
     * @return OrdenTrabajo
     */
    public function setTipoComprobante(\Hertz\ReservaBundle\Entity\TipoComprobante $tipoComprobante = null)
    {
        $this->tipoComprobante = $tipoComprobante;

        return $this;
    }

    /**
     * Get tipoComprobante
     *
     * @return \Hertz\ReservaBundle\Entity\TipoComprobante
     */
    public function getTipoComprobante()
    {
        return $this->tipoComprobante;
    }

    /**
     * Set tareas
     *
     * @param string $tareas
     * @return OrdenTrabajo
     */
    public function setTareas($tareas)
    {
        $this->tareas = $tareas;

        return $this;
    }

    /**
     * Get tareas
     *
     * @return string
     */
    public function getTareas()
    {
        return $this->tareas;
    }

    /**
     * Set total
     *
     * @param float $total
     * @return OrdenTrabajo
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get total
     *
     * @return float
     */
    public function getTotal()
    {
        return $this->total;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->otReparacionesTareas = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get pagado
     *
     * @return boolean 
     */
    public function getPagado()
    {
        return $this->pagado;
    }

    /**
     * Add otReparacionesTareas
     *
     * @param \Hertz\ReservaBundle\Entity\OrdenTrabajoReparacionesTareas $otReparacionesTareas
     * @return OrdenTrabajo
     */
    public function addOtReparacionesTarea(\Hertz\ReservaBundle\Entity\OrdenTrabajoReparacionesTareas $otReparacionesTareas)
    {
        $this->otReparacionesTareas[] = $otReparacionesTareas;

        return $this;
    }

    /**
     * Remove otReparacionesTareas
     *
     * @param \Hertz\ReservaBundle\Entity\OrdenTrabajoReparacionesTareas $otReparacionesTareas
     */
    public function removeOtReparacionesTarea(\Hertz\ReservaBundle\Entity\OrdenTrabajoReparacionesTareas $otReparacionesTareas)
    {
        $this->otReparacionesTareas->removeElement($otReparacionesTareas);
    }
}
