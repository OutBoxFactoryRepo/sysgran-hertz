<?php
namespace Hertz\ReservaBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Hertz\UserBundle\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraint;
use Doctrine\Common\Collections\ArrayCollection;
use Hertz\ReservaBundle\Entity\TipoCliente;
use Hertz\ReservaBundle\Entity\Cliente;

/**
 * ReservaRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ReservaRepository extends EntityRepository
{

    public function getAll(Request $request, $user, $sucursal)
    {


        $sucursal = $sucursal->getSucursal()->getId();
        $pagina = $request->request->get("page");
        $registros = (int)$request->request->get("rows");
        $orden = strtoupper($request->request->get("sord"));
        $campo = $request->request->get("sidx");

        $auxCampo = explode(",", $campo);

        if (count($auxCampo) > 1) {
            $campo = trim($auxCampo[1]);
        }

        $fin = $registros;//(int)($registros * $pagina);//($pagina * $registros);
        $inicio = (int)($registros * $pagina) - $registros;
        //echo "inicio/fin: ".$inicio."/".$fin;
        $campoBusqueda = $request->request->get("searchField");
        $valorCampoBusqueda = $request->request->get("searchString");
        $searchOper = $request->request->get("searchOper");

        $fDesde = $request->request->get("fDesde");
        $fHasta = $request->request->get("fHasta");

        $buscaFecha = false;
        if ($fDesde != "" && $fHasta != "") {
            $buscaFecha = true;
            $fDesde = $this->convertirFecha($fDesde);
            $fHasta = $this->convertirFecha($fHasta);
        }

        // print_r($request->request);die();

        if ($searchOper == "eq") {
            $searchOper = "=";
        } elseif ($searchOper == "ne") {
            $searchOper = "!=";
        } elseif ($searchOper == "cn") {
            $searchOper = "like";
        }

        if ($campo == "") {
            $campo = "id";
        } else {
            $campo = "id";
        }

        if ($campoBusqueda == "nombre" || $campoBusqueda == "apellido") {
            $nombreSearch = $this->getClienteByNombre($valorCampoBusqueda);
            if ($nombreSearch == null) {
                $valorCampoBusqueda = 0;
            } else {
                $arrayAlgo = new ArrayCollection($nombreSearch);
                $auxArray = "";
                foreach ($arrayAlgo as $veh) {
                    //echo "count: ".$veh["vehiculoId"];
                    $auxArray[] = $veh["clienteId"];
                }
                //print_r($auxArray);die();
                //$auxArray = substr ($auxArray, 0, strlen($auxArray) - 1);
                $valorCampoBusqueda = $auxArray;
            }
            $searchOper = "IN";

            $campoBusqueda = "cliente";
        } elseif ($campoBusqueda == "dominio") {

            $vehiculoSearch = $this->getVehiculoByDominio($valorCampoBusqueda);
            if ($vehiculoSearch == null) {
                $valorCampoBusqueda = 0;
            } else {
                $arrayAlgo = new ArrayCollection($vehiculoSearch);
                $auxArray = "";
                foreach ($arrayAlgo as $veh) {
                    //echo "count: ".$veh["vehiculoId"];
                    $auxArray[] = $veh["vehiculoId"];
                }
                //print_r($auxArray);die();
                //$auxArray = substr ($auxArray, 0, strlen($auxArray) - 1);
                $valorCampoBusqueda = $auxArray;
            }
            $searchOper = "IN";

            $campoBusqueda = "vehiculo";
        }

        $qb = $this->getEntityManager()->createQueryBuilder()
            ->select('u')
            ->from(Reserva::ORM_ENTITY, 'u')
            ->leftJoin("u.contrato", "c")
            ->setFirstResult($inicio)
            ->setMaxResults($fin);

        $qb = $qb->orderBy('u.' . $campo, $orden);

        if (!$user->getEsAdmin()) {

            if ($sucursal == 1) { //todas las sucursales
                $parameters = "";

                if ($campoBusqueda != "") {
                    if ($searchOper == "like") {
                        $valorCampoBusqueda = "%" . $valorCampoBusqueda . "%";
                    }
                    $parameters = array("b_" . $campoBusqueda => $valorCampoBusqueda);
                    if ($campoBusqueda == "cliente") {
                        $qb = $qb->where('u.' . $campoBusqueda . " " . $searchOper . " (:b_" . $campoBusqueda . ")");
                    } else if ($campoBusqueda == "vehiculo") {
                        $qb = $qb->where('u.' . $campoBusqueda . " " . $searchOper . " (:b_" . $campoBusqueda . ")");
                    } else {
                        $qb = $qb->where('u.' . $campoBusqueda . " " . $searchOper . " :b_" . $campoBusqueda . "");
                    }


                }
                $qb = $qb->where('u.debaja = false');
                $qb = $qb->where('u.contrato IS NULL or u.contrato = 0 or c.estado = 0');
                if ($parameters != "") {
                    $qb = $qb->setParameters($parameters);
                }

            } else {
                /*
                por defecto un usuario que no es admin y pertenece a una sucursal en particular solo
                puede ver sus reservas y de su sucursal, tanto de origen como destino.
                Pero si hace una búsqueda esas condiciones ya no son válidas y filtra lo que devuelve
                 */
                
                if ($campoBusqueda != "") {
                    if ($searchOper == "like") {
                        $valorCampoBusqueda = "%" . $valorCampoBusqueda . "%";
                    }
                    $parameters = array("b_" . $campoBusqueda => $valorCampoBusqueda);
                    if ($campoBusqueda == "cliente") {
                        $qb = $qb->where('u.' . $campoBusqueda . " " . $searchOper . " (:b_" . $campoBusqueda . ") and  (u.contrato IS NULL or u.contrato = 0  or c.estado = 0)");
                    } else if ($campoBusqueda == "vehiculo") {
                        $qb = $qb->where('u.' . $campoBusqueda . " " . $searchOper . " (:b_" . $campoBusqueda . ")  and  (u.contrato IS NULL or u.contrato = 0  or c.estado = 0)");
                    } else {
                        $qb = $qb->where('u.' . $campoBusqueda . " " . $searchOper . " :b_" . $campoBusqueda . "  and  (u.contrato IS NULL or u.contrato = 0  or c.estado = 0) and  (c.agenciaOrigen IS NULL or c.agenciaDestino IS NULL or c.agenciaOrigen = " . $sucursal . " or c.agenciaDestino = " . $sucursal . ")");
                    }

                    //$qb = $qb->where(' (u.contrato IS NULL or u.contrato = 0)');
                    $qb = $qb->setParameters($parameters);
                } else {
                    //$parameters = array('agenciax' => $this->getAgencia($sucursal),'baja'=>false);
                    //$qb = $qb->where('u.agencia =:agenciax and u.debaja =:baja  and (u.contrato IS NULL or u.contrato = 0)');
                    $parameters = array('baja' => false);
                    $qb = $qb->where("u.debaja =:baja  and (u.contrato IS NULL or u.contrato = 0  or c.estado = 0) and  (c.agenciaOrigen IS NULL or c.agenciaDestino IS NULL or c.agenciaOrigen = " . $sucursal . " or c.agenciaDestino = " . $sucursal . ")");
                }

                //$qb = $qb->where('u.contrato IS NULL or u.contrato = 0');
                //echo "entra3";die();
                $qb = $qb->setParameters($parameters);
            }


        } else {

            if ($campoBusqueda != "") {
                if ($searchOper == "like") {
                    $valorCampoBusqueda = "%" . $valorCampoBusqueda . "%";
                }
                $parameters = array("b_" . $campoBusqueda => $valorCampoBusqueda);
                if ($campoBusqueda == "cliente") {
                    $qb = $qb->where('u.' . $campoBusqueda . " " . $searchOper . " (:b_" . $campoBusqueda . ")");
                } else if ($campoBusqueda == "vehiculo") {
                    $qb = $qb->where('u.' . $campoBusqueda . " " . $searchOper . " (:b_" . $campoBusqueda . ")");
                } else {
                    $qb = $qb->where('u.' . $campoBusqueda . " " . $searchOper . " :b_" . $campoBusqueda . "");
                }


                $qb = $qb->setParameters($parameters);
            }
        }
        $qb = $qb->getQuery();
        
        $arrayReservas = array();
        
        foreach($qb->getResult() as $reservax)
        {
             $auxArrayReserva["id"] = $reservax->getId();
             $auxArrayReserva["cliente"] = $reservax->getCliente();
             $auxArrayReserva["vehiculo"] = $reservax->getVehiculo();
             $auxArrayReserva["usuario"] = $reservax->getUsuario();
             $auxArrayReserva["idUsuario"] = $reservax->getIdUsuario()->getId();
             $auxArrayReserva["debaja"] = $reservax->getDebaja();
             $auxContrato = $reservax->getContrato();
             $arrayContrato = array();
             if($auxContrato)
             {
                $arrayContrato["id"] =  $auxContrato->getId();
                $arrayContrato["observaciones"] =  $auxContrato->getObservaciones();
                $arrayContrato["total_dias"] =  $auxContrato->getTotalDias();
                $arrayContrato["total_horas"] =  $auxContrato->getTotalHoras();
                    $arrayAdicionales = array();
                    foreach($auxContrato->getAdicionales() as $adicionalx)
                    {
                        $auxArrayAdicional["id"]=$adicionalx->getId();
                        $auxArrayAdicional["cantidad"]=$adicionalx->getCantidad();
                        $auxArrayAdicional["valor"]=$adicionalx->getValor();
                        $auxArrayAdicional["adicional"]=$adicionalx->getAdicional();
                        $arrayAdicionales[] = $auxArrayAdicional;
                    }
                $arrayContrato["adicionales"] =  $arrayAdicionales;
                $arrayContrato["tasa_aeropuerto"] =  $auxContrato->getTasaAeropuerto();
                $arrayContrato["tasa_apt"] =  $auxContrato->getTasaApt();
                $arrayContrato["iva_porcent"] =  $auxContrato->getIvaPorcent();
                $arrayContrato["anticipo1"] =  $auxContrato->getAnticipo1();
                $arrayContrato["anticipo2"] =  $auxContrato->getAnticipo2();
                $arrayContrato["voucher"] =  $auxContrato->getVoucher();
                $arrayContrato["reintegros"] =  $auxContrato->getReintegros();
                $arrayContrato["estado"] =  $auxContrato->getEstado();
                $arrayContrato["contado"] =  $auxContrato->getContado();
                $arrayContrato["cheque"] =  $auxContrato->getCheque();
                $arrayContrato["tarjeta"] =  $auxContrato->getTarjeta();
                $arrayContrato["ctacte"] =  $auxContrato->getCtacte();
                $arrayContrato["prepagos"] =  $auxContrato->getPrepagos();
                $arrayContrato["tarjeta_emisor"] =  $auxContrato->getTarjetaEmisor();
                $arrayContrato["diaria"] =  $auxContrato->getDiaria();
                $arrayContrato["diaria_cantidad"] =  $auxContrato->getDiariaCantidad();
                $arrayContrato["usuario"] =  $auxContrato->getUsuario();
                $arrayContrato["lock_time"] =  $auxContrato->getLockTime();
                $arrayContrato["kilometros"] =  $auxContrato->getKilometros();
                $arrayContrato["estado_tanque"] =  $auxContrato->getEstadoTanque();
                $arrayContrato["agencia_origen"] =  $auxContrato->getAgenciaOrigen();
                $arrayContrato["agencia_destino"] =  $auxContrato->getAgenciaDestino();
                //return $arrayContrato;
             }
             $auxArrayReserva["contrato"] = $arrayContrato;
             $auxArrayReserva["fecha_hora_devolucion"] = $reservax->getFechaHoraDevolucion();
             $auxArrayReserva["fecha_hora_salida"] = $reservax->getFechaHoraSalida();
             $auxArrayReserva["fecha_hora_retorno"] = $reservax->getFechaHoraRetorno();
             $auxArrayReserva["hora_salida"] = $reservax->getHoraSalida();
             $auxArrayReserva["hora_devolucion"] = $reservax->getHoraDevolucion();
             $auxArrayReserva["origen_padre"] = $reservax->getOrigenPadre();
             $auxArrayReserva["origen_hijo"] = $reservax->getOrigenHijo();
             $auxArrayReserva["nombre_via"] = $reservax->getNombreVia();
             $auxArrayReserva["agencia"] = $reservax->getAgencia();
             $auxArrayReserva["nro_vuelo"] = $reservax->getNroVuelo();
             $auxArrayReserva["nro_telefono"] = $reservax->getNroTelefono();
             
             $arrayReservas[] = $auxArrayReserva;
        }
        
        return $arrayReservas;
    }


    public function getResXVehiculo(Request $request, $user)
    {
        $qb = $this->getEntityManager()->createQueryBuilder()
            ->select('u')
            ->from(Reserva::ORM_ENTITY, 'u');

        $idVehiculo = $request->query->get("id");

        //return "idvehiculo: ".$idVehiculo;
        $qb = $qb->where('u.vehiculo=' . $idVehiculo);

        $qb = $qb->orderBy('u.id', 'DESC');
        $qb = $qb->getQuery();
        return $qb->getResult();
    }


    public function getOne($id)
    {
        $qb = $this->getEntityManager()->createQueryBuilder()
            ->select('u')
            ->from(Reserva::ORM_ENTITY, 'u')
            ->where('u.id =:identifier')
            ->setParameter('identifier', $id)
            ->getQuery();
        
        $arrayReservas = array();
        
        foreach($qb->getResult() as $reservax)
        {
             $auxArrayReserva["id"] = $reservax->getId();
             $auxArrayReserva["cliente"] = $reservax->getCliente();
             $auxArrayReserva["vehiculo"] = $reservax->getVehiculo();
             $auxArrayReserva["usuario"] = $reservax->getUsuario();
             $auxArrayReserva["idUsuario"] = $reservax->getIdUsuario()->getId();
             $auxArrayReserva["debaja"] = $reservax->getDebaja();
             $auxContrato = $reservax->getContrato();
             $arrayContrato = array();
             if($auxContrato)
             {
                $arrayContrato["id"] =  $auxContrato->getId();
                $arrayContrato["observaciones"] =  $auxContrato->getObservaciones();
                $arrayContrato["total_dias"] =  $auxContrato->getTotalDias();
                $arrayContrato["total_horas"] =  $auxContrato->getTotalHoras();
                    $arrayAdicionales = array();
                    foreach($auxContrato->getAdicionales() as $adicionalx)
                    {
                        $auxArrayAdicional["id"]=$adicionalx->getId();
                        $auxArrayAdicional["cantidad"]=$adicionalx->getCantidad();
                        $auxArrayAdicional["valor"]=$adicionalx->getValor();
                        $auxArrayAdicional["adicional"]=$adicionalx->getAdicional();
                        $arrayAdicionales[] = $auxArrayAdicional;
                    }
                $arrayContrato["adicionales"] =  $arrayAdicionales;
                $arrayContrato["tasa_aeropuerto"] =  $auxContrato->getTasaAeropuerto();
                $arrayContrato["tasa_apt"] =  $auxContrato->getTasaApt();
                $arrayContrato["iva_porcent"] =  $auxContrato->getIvaPorcent();
                $arrayContrato["anticipo1"] =  $auxContrato->getAnticipo1();
                $arrayContrato["anticipo2"] =  $auxContrato->getAnticipo2();
                $arrayContrato["voucher"] =  $auxContrato->getVoucher();
                $arrayContrato["reintegros"] =  $auxContrato->getReintegros();
                $arrayContrato["estado"] =  $auxContrato->getEstado();
                $arrayContrato["contado"] =  $auxContrato->getContado();
                $arrayContrato["cheque"] =  $auxContrato->getCheque();
                $arrayContrato["tarjeta"] =  $auxContrato->getTarjeta();
                $arrayContrato["ctacte"] =  $auxContrato->getCtacte();
                $arrayContrato["prepagos"] =  $auxContrato->getPrepagos();
                $arrayContrato["tarjeta_emisor"] =  $auxContrato->getTarjetaEmisor();
                $arrayContrato["diaria"] =  $auxContrato->getDiaria();
                $arrayContrato["diaria_cantidad"] =  $auxContrato->getDiariaCantidad();
                $arrayContrato["usuario"] =  $auxContrato->getUsuario();
                $arrayContrato["lock_time"] =  $auxContrato->getLockTime();
                $arrayContrato["kilometros"] =  $auxContrato->getKilometros();
                $arrayContrato["estado_tanque"] =  $auxContrato->getEstadoTanque();
                $arrayContrato["agencia_origen"] =  $auxContrato->getAgenciaOrigen();
                $arrayContrato["agencia_destino"] =  $auxContrato->getAgenciaDestino();
                //return $arrayContrato;
             }
             $auxArrayReserva["contrato"] = $arrayContrato;
             $auxArrayReserva["fecha_hora_devolucion"] = $reservax->getFechaHoraDevolucion();
             $auxArrayReserva["fecha_hora_salida"] = $reservax->getFechaHoraSalida();
             $auxArrayReserva["fecha_hora_retorno"] = $reservax->getFechaHoraRetorno();
             $auxArrayReserva["hora_salida"] = $reservax->getHoraSalida();
             $auxArrayReserva["hora_devolucion"] = $reservax->getHoraDevolucion();
             $auxArrayReserva["origen_padre"] = $reservax->getOrigenPadre();
             $auxArrayReserva["origen_hijo"] = $reservax->getOrigenHijo();
             $auxArrayReserva["nombre_via"] = $reservax->getNombreVia();
             $auxArrayReserva["agencia"] = $reservax->getAgencia();
             $auxArrayReserva["nro_vuelo"] = $reservax->getNroVuelo();
             $auxArrayReserva["nro_telefono"] = $reservax->getNroTelefono();
             
             $arrayReservas[] = $auxArrayReserva;
        }
        
        
        return $arrayReservas;
    }
    
    public function getOneObj($id)
    {
        $qb = $this->getEntityManager()->createQueryBuilder()
            ->select('u')
            ->from(Reserva::ORM_ENTITY, 'u')
            ->where('u.id =:identifier')
            ->setParameter('identifier', $id)
            ->getQuery();
        
        
        
        return $qb->getResult();
    }


    public function getOneArray($id)
    {
        $qb = $this->getEntityManager()->createQueryBuilder()
            ->select('u')
            ->from(Reserva::ORM_ENTITY, 'u')
            ->where('u.id =:identifier')
            ->setParameter('identifier', $id)
            ->getQuery();

        $qb2 = $qb->getResult();
        if (count($qb2) > 0) {
            $objeto = $qb2[0];
            $arrayObjeto = $objeto->getClientesAdicionales();
            return $arrayObjeto;
        } else {
            return null;
        }

        return;
    }


    public function addPasAdicional(Request $request)
    {
        $id = $request->query->get("idReserva");
        $idCliente = $request->request->get("id");
        if ($idCliente == "_empty") {
            $idCliente = $request->request->get("idx");
        }
        $qb = $this->getEntityManager()->createQueryBuilder()
            ->select('u')
            ->from(Reserva::ORM_ENTITY, 'u')
            ->where('u.id =:identifier')
            ->setParameter('identifier', $id)
            ->getQuery();
        $Reserva = $qb->getResult();
        //Otengo el cliente para agregarlo al objeto
        $cliAdicional = $this->getCliente($idCliente);

        $oper = $request->request->get("oper");
        $tipoPasajero = $request->request->get("tipo_pasajero");
        $tipoCliente = $this->getTipoCliente($tipoPasajero);


        if (count($Reserva) > 0) {
            $objetoReserva = $Reserva[0];
            $objetoReserva2 = $Reserva[0];

            //voy al cliente para ver si tiene cargada una reserva
            $reservaCli = $cliAdicional->getIdReserva();

            //$arrayObjeto = $objetoReserva->getClientesAdicionales($cliAdicional);
            if ($reservaCli->isEmpty()) {

                //$tipoCliente = $this->getTipoCliente(3);
                $cliAdicional->addTipoCliente($tipoCliente);

                //$objetoReserva->removeClientesAdicionale($cliAdicional);
                $objetoReserva->addClientesAdicionale($cliAdicional);


                $em = $this->getEntityManager();
                $em->persist($objetoReserva);
                $em->flush();
            } else {


                if ($oper == "edit") {

                    $coleccionTipoCli = $cliAdicional->getTipoCliente();

                    $resultado = $this->getObjectById($coleccionTipoCli, $tipoPasajero);

                    if ($resultado != null) {

                        $this->eliminarColeccion($cliAdicional, $coleccionTipoCli);
                    }

                    //$arrayObjTipoCliente = $this->getArrayObjTipoCliente($tipoPasajero);
                    //echo "idpasajero".$tipoPasajero;die();

                    //$clientex = $this->getCliente($idCliente);
                    //for($countObj = 0;$countObj < count($arrayObjTipoCliente);$countObj++)
                    //{
                    //    $cliAdicional->removeTipoCliente($arrayObjTipoCliente[$countObj]);
                    //}

                    $objetoReserva2->removeClientesAdicionale($cliAdicional);
                    $cliAdicional->addTipoCliente($tipoCliente);
                    $em = $this->getEntityManager();
                    $em->persist($cliAdicional);
                    $em->flush();

                    $em3 = $this->getEntityManager();
                    $em3->persist($objetoReserva2);
                    $em3->flush();

                    $objetoReserva2->addClientesAdicionale($cliAdicional);

                    $em4 = $this->getEntityManager();
                    $em4->persist($objetoReserva2);
                    $em4->flush();

                    return $objetoReserva2;
                }
                if ($oper == "del") {
                    $objetoReserva2->removeClientesAdicionale($cliAdicional);

                    $em = $this->getEntityManager();
                    $em->persist($cliAdicional);
                    $em->flush();
                }
            }


		$usuario = utf8_encode($usuario);
		$objReserva->setDescripcion($descripcion);
                $objReserva->setUsuario($usuario);
                $objUser = $this->getEntityManager()->getRepository(User::ORM_ENTITY)->findOneById($user->getId());
                $objReserva->setidUsuario($objUser);
                
                

                $objReserva->setNroVuelo($nroVuelo);
                $objReserva->setNroTelefono($nroTelefono);

            return $objetoReserva;
        } else {
            return null;
        }
    }

    //obtiene el objeto agencia
    private function getAgencia($id)
    {

        $qb = $this->getEntityManager()->getRepository(Agencia::ORM_ENTITY)->getOne($id);
        if (count($qb) > 0) {
            return $qb[0];
        } else {
            return null;
        }
    }

    public function insertar(Request $request, $validator, $user, $sucursal)
    {

        $agenciax = $sucursal->getSucursal()->getId();
        $usuario = $user->getUsername();
        //echo "agenciaaaaaaaaaaa :".$agenciax;die();

        $content = $request->getContent($request, $validator, $user);

        $jsonContent = json_decode($content, true);

        $idReserva = 0;
        if (json_decode($content, true) == "") {
            $objRequest = $request->request;

            $descripcion = $objRequest->get("descripcion");
            $idcliente = $objRequest->get("idcliente");
            $idmetodopago = $objRequest->get("idmetodopago");
            $idvehiculo = $objRequest->get("idvehiculo");
            $fechaHoraDevolucion = $objRequest->get("vehiculo_fecha_hora_devolucion");
            $fechaHoraSalida = $objRequest->get("vehiculo_fecha_hora_salida");
            $nombre = $objRequest->get("cliente_nombre");
            $apellido = $objRequest->get("cliente_apellido");
            $fechaHoraRetorno = $objRequest->get("vehiculo_fecha_hora_retorno");
            $debaja = $objRequest->get("debaja");
            if ($idmetodopago == "") {
                $idmetodopago = $objRequest->get("metodo_pago_descripcion");
            }

            if ($idcliente == "") {
                $idcliente = $objRequest->get("cliente_id");
            }


            if ($idvehiculo == "") {
                $idvehiculo = $objRequest->get("vehiculo_id");
            }
            $tipoDoc = $objRequest->get("cliente_tipodoc_descripcion");
            $nroDoc = $objRequest->get("cliente_numero_doc");
            $nroTelefono = $objRequest->get("nro_telefono");

        } else {

            $objRequest = new ArrayCollection(json_decode($content, true));
            if ($objRequest->get("rows") != "") {
                $objRequest = new ArrayCollection($objRequest->get("rows"));
            }
            $descripcion = $objRequest->get("descripcion");
            $idcliente = $objRequest->get("idcliente");
            $idmetodopago = $objRequest->get("idmetodopago");
            $idvehiculo = $objRequest->get("idvehiculo");


        }

        $idPadre = $objRequest->get("origen_padre_id");
        $idHijo = $objRequest->get("via");
        $nombreVia = $objRequest->get("nombre_via");
        $nroVuelo = $objRequest->get("nro_vuelo");
        $nroTelefono = $objRequest->get("nro_telefono");

        //return $objRequest;

        $fechaHoraDevolucion = utf8_encode($objRequest->get("fecha_hora_devolucion"));
        $fechaHoraSalida = utf8_encode($objRequest->get("fecha_hora_salida"));
        $fechaHoraRetorno = utf8_encode($objRequest->get("fecha_hora_retorno"));
        $horaDevolucion = utf8_encode($objRequest->get("hora_devolucion"));

        $horaSalida = utf8_encode($objRequest->get("hora_salida"));
        $horaDevolucion = utf8_encode($objRequest->get("hora_devolucion"));

        $fechaHoraDevolucion = new \DateTime($this->convertirFechaHora($fechaHoraDevolucion, $horaDevolucion));


        $fechaHoraSalida = new \DateTime($this->convertirFechaHora($fechaHoraSalida, $horaSalida));


        $fechaHoraRetorno = new \DateTime($this->convertirFechaHora($fechaHoraRetorno, $horaDevolucion));

        //print_r(array($fechaHoraSalida->format('U'),$fechaHoraDevolucion->format('U')));
        //die();
        if ($fechaHoraSalida->format('U') > $fechaHoraDevolucion->format('U')) {
            return array("error" => "La fecha de retorno debe ser mayor a la de salida");
        }

        $objContrato = false;
        if ($objRequest->get("id") != "" && $objRequest->get("id") != "_empty") {
            $idReserva = $objRequest->get("id");
            $objReserva = $this->getReserva($idReserva);
            //print_r($objReserva);die();
            
                $objContrato = $objReserva->getContrato();
            

            if ($objReserva == null) {
                throw new \Exception('No existe la reserva', 401);
                return null;
            }

        } else {
            $objReserva = new Reserva();
            $objReserva->setIdUsuario($this->getUsuario($user->getId()));
        }


        //print_r($objRequest);die();

        if ($idcliente != "") {
            $objCliente = $this->getCliente($idcliente);
            if ($nroTelefono != "") {
                $objCliente->setTelefono($nroTelefono);
            }
        } else {
            $objCliente = $this->newCliente($nombre, $apellido, $tipoDoc, $nroDoc);
            if ($nroTelefono != "") {
                $objCliente->setTelefono($nroTelefono);
            }
            $idcliente = $objCliente->getId();
            
        }


        $objMetodoPago = $this->getMetodoPago($idmetodopago);


        $objVehiculo = $this->getVehiculo($idvehiculo);
        if ($objVehiculo == null) {
            throw new \Exception('No existe el vehiculo', 401);
            return null;
        }

        /******** lOG DE CAMBIO DE ESTADO DE VEHICULO ------------*/
        $estadoVehiculo = $this->getEstadosVehiculo(6);


        $fechaDeHoy = new \DateTime("now");

        $usuariox = $this->getUsuario($user->getId());
        $vehiculoEstadosHistorio = new VehiculoEstadosHistorico();
        $vehiculoEstadosHistorio->setEstadoVehiculo($estadoVehiculo);
        $vehiculoEstadosHistorio->setFechaCambio($fechaDeHoy);
        $vehiculoEstadosHistorio->setUsuario($usuariox);
        $vehiculoEstadosHistorio->setVehiculo($objVehiculo);


        $emx = $this->getEntityManager();
        $emx->persist($vehiculoEstadosHistorio);
        $emx->flush();

        /********FIN DE CAMBIO DE ESTADO DE VECHÍCULO***/

        if ($objVehiculo == null) {
            return array("error" => 'No existe el estado de vehiculo');
        }


        $objVehiculo->setEstado($estadoVehiculo);//queda reservado

        $valor = $objRequest->get("valor");
        
        $usuario = utf8_encode($usuario);
        $objReserva->setDescripcion($descripcion);
        $objReserva->setUsuario($usuario);
        $objReserva->setNroVuelo($nroVuelo);
        $objReserva->setNroTelefono($nroTelefono);

        $objReserva->setAgencia($this->getAgencia($agenciax));

        if ($objContrato) {

            if ($debaja == "true") {
                $objContrato->setEstado(1);
                //echo "id: ".$objContrato->getId(). " " . $debaja;die();
            } else {
                $objContrato->setEstado(0);
            }
            $objReserva->setContrato($objContrato);
        }

        $objReserva->setDebaja($debaja);
        $objReserva->setMetodoPago($objMetodoPago);
        $objReserva->setVehiculo($objVehiculo);
        $objReserva->setCliente($objCliente);

        $objOrigenPadre = $this->getOrigen((int)$idPadre);


        $objReserva->setOrigenPadre($objOrigenPadre);


        $objOrigenHijo = $this->getOrigen((int)$idHijo);


        $objReserva->setOrigenHijo($objOrigenHijo);


        $objReserva->setNombreVia($nombreVia);

        $objReserva->setHoraDevolucion($horaDevolucion);
        $objReserva->setHoraSalida($horaSalida);

        $objReserva->setFechaHoraDevolucion($fechaHoraDevolucion);
        $objReserva->setFechaHoraSalida($fechaHoraSalida);
        $objReserva->setFechaHoraRetorno($fechaHoraRetorno);


        //$em = $this->getEntityManager();


        $errores = $validator->validate($objReserva);

        $contErrores = 0;

        foreach ($errores as $error) {
            $contErrores++;
        }


        if ($contErrores > 0) {
            throw new \Exception($errores, 401);
            return null;
        } else {

            $em = $this->getEntityManager();
            $em->persist($objReserva);
            $em->flush();

            return "";
        }
    }


    private function getMetodoPago($id)
    {

        $qb = $this->getEntityManager()->getRepository(MetodoPago::ORM_ENTITY)->getOne($id);
        if (count($qb) > 0) {
            return $qb[0];
        } else {
            return null;
        }
    }

    private function getOrigen($id)
    {

        $qb = $this->getEntityManager()->getRepository(OrigenReserva::ORM_ENTITY)->getOne($id);
        if (count($qb) > 0) {
            return $qb[0];
        } else {
            return null;
        }
    }

    private function getCliente($id)
    {

        $qb = $this->getEntityManager()->getRepository(Cliente::ORM_ENTITY)->getOne($id);
        if (count($qb) > 0) {
            return $qb[0];
        } else {
            return null;
        }
    }

    private function newCliente($nombre, $apellido, $tipoDoc, $nroDoc)
    {
        $cli = new Cliente();
        $cli->setNombre($nombre);
        $cli->setApellido($apellido);
        $cli->setTipodoc($this->getTipoDoc($tipoDoc));
        $cli->setNumeroDoc($nroDoc);
        $cli->setDebaja(false);
        $em = $this->getEntityManager();
        $em->persist($cli);
        $em->flush();
        return $cli;
    }

    private function getTipoDoc($id)
    {

        $qb = $this->getEntityManager()->getRepository(TipoDoc::ORM_ENTITY)->getOne($id);
        if (count($qb) > 0) {
            return $qb[0];
        } else {
            return null;
        }
    }

    private function getVehiculo($id)
    {

        $qb = $this->getEntityManager()->getRepository(Vehiculo::ORM_ENTITY)->getOne($id);
        if (count($qb) > 0) {
            return $qb[0];
        } else {
            return null;
        }
    }


    private function getTipoCliente($id)
    {

        $qb = $this->getEntityManager()->getRepository(TipoCliente::ORM_ENTITY)->getOne($id);
        if (count($qb) > 0) {
            return $qb[0];
        } else {
            return null;
        }
    }


    public function getReserva($id)
    {

        $qb = $this->getOneObj($id);
        if (count($qb) > 0) {
            return $qb[0];
        } else {
            return null;
        }
    }

    private function getEstadosVehiculo($id)
    {

        $qb = $this->getEntityManager()->getRepository(EstadoVehiculo::ORM_ENTITY)->getOne($id);
        if (count($qb) > 0) {
            return $qb[0];
        } else {
            return null;
        }
    }

    private function convertirFecha($cadena)
    {
        if ($cadena != "") {
            $arrayFecha = explode("/", $cadena);
            if (count($arrayFecha) >= 2) {
                $nuevaFecha = $arrayFecha[2] . "-" . $arrayFecha[1] . "-" . $arrayFecha[0];
                return $nuevaFecha;
            } else {
                return $cadena;
            }
        } else {
            return "";
        }
    }

    private function convertirFechaHora($cadena, $hora)
    {
        if ($cadena != "") {
            $arrayFecha = explode("/", $cadena);
            if (count($arrayFecha) >= 2) {
                $nuevaFecha = $arrayFecha[2] . "-" . $arrayFecha[1] . "-" . $arrayFecha[0] . " " . $hora;
                return $nuevaFecha;
            } else {
                return $cadena;
            }
        } else {
            return "";
        }
    }


    //retorna un array de objetosTipoCliente
    private function getArrayObjTipoCliente($ids)
    {
        $arrayx = explode(",", $ids);
        $arraySalida = null;
        for ($countx = 0; $countx < count($arrayx); $countx++) {
            $objTipoCliente = $this->getTipocliente($arrayx[$countx]);
            if ($objTipoCliente == null) {
                return null;
            } else {
                $arraySalida[] = $objTipoCliente;
            }
        }
        return $arraySalida;
    }


    public function getObjectById($objeto, $id)
    {
        $retorno = null;
        foreach ($objeto as $root) {
            echo $root->getId() . "/";
            if ($id == $root->getId()) {
                $retorno = $root;
            }
        }
        return $retorno;
        throw new InvalidArgumentException(sprintf('Not a valid ID: %d', $id));
    }

    public function eliminarColeccion($objeto, $coleccion)
    {

        foreach ($coleccion as $root) {
            $objeto->removeTipoCliente($root);

        }
        $em = $this->getEntityManager();
        $em->persist($objeto);
        $em->flush();

    }

    private function getUsuario($id)
    {

        $qb = $this->getEntityManager()->getRepository(\Hertz\UserBundle\Entity\User::ORM_ENTITY)->findOneById($id);
        if (count($qb) > 0) {
            return $qb;
        } else {
            return null;
        }
    }

    public function getClienteByNombre($nombre)
    {


        $qb = $this->getEntityManager()->getRepository(Cliente::ORM_ENTITY)->getClienteByNombre($nombre);
        return $qb;

    }

    public function getVehiculoByDominio($dominio)
    {


        $qb = $this->getEntityManager()->getRepository(Vehiculo::ORM_ENTITY)->getBySinFecha($dominio);
        return $qb;

    }
}
