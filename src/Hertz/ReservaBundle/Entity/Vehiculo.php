<?php
namespace Hertz\ReservaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\DiExtraBundle\Annotation as DI;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Doctrine\ORM\EntityManager;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Vehiculo
 *
 * @ORM\Table(name="vehiculos")
 * @ORM\Entity(repositoryClass="VehiculoRepository")
 */
class Vehiculo{
    
    const ORM_ENTITY = "HertzReservaBundle:Vehiculo";

    /**
     *
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Assert\Type(type="integer", message="Debe ser entero mayor a 0") 
     */
    private $id;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=255, nullable=true)
     * @Assert\Length(
     *      min = "2",
     *      max = "255",
     *      minMessage = "Debe tener al menos {{ limit }} caracteres de largo",
     *      maxMessage = "Debe tener un máximo de {{ limit }} caracteres"
     * )
     */
    private $descripcion;
    
    /**
     *
     * @var integer
     *
     * @ORM\Column(name="modelo", type="integer", nullable=false)
     * @Assert\NotBlank(message="El campo Modelo es obligatorio.")
     * @Assert\Type(type="numeric", message="El valor {{ value }} no es válido debe ser {{ type }}.")
     */
    private $modelo;
    
    /**
     *
     * @var integer
     *
     * @ORM\Column(name="kilometros", type="integer", nullable=false)
     * @Assert\NotBlank(message="El campo Kilometros es obligatorio.")
     * @Assert\Type(type="numeric", message="El valor {{ value }} no es válido debe ser {{ type }}.")
     */
    private $kilometros;
    
    /**
     *
     *
     *@ORM\ManyToOne(targetEntity="Hertz\ReservaBundle\Entity\Agencia",  cascade={"remove"})
     *@ORM\JoinColumn(name="agencia", referencedColumnName="id", nullable=false )
     *@Assert\NotBlank(message="El campo Agencia es obligatorio.")
     *@Assert\Type(type="object", message="El valor {{ value }} no es válido debe ser {{ type }}.")
     *
     */
    private $agencia;
    
    
    
    /**
     *
     *
     *@ORM\ManyToOne(targetEntity="Hertz\ReservaBundle\Entity\EstadoVehiculo",  cascade={"remove"})
     *@ORM\JoinColumn(name="estado", referencedColumnName="id", nullable=false )
     *@Assert\NotBlank(message="El campo Estado es obligatorio.")
     *@Assert\Type(type="object", message="El valor {{ value }} no es válido debe ser {{ type }}.")
     *
     */
    private $estado;
    
    
    /**
     *
     * @var string
     *
     * @ORM\Column(name="dominio", type="string", length=10, nullable=false)
     * @Assert\NotBlank(message="El campo es obligatorio.")
     * @Assert\Length(
     *      min = "6",
     *      max = "10",
     *      minMessage = "Debe tener al menos {{ limit }} caracteres de largo",
     *      maxMessage = "Debe tener un máximo de {{ limit }} caracteres"
     * )
     */
    private $dominio;
    
        /**
     *
     * @var string
     *
     * @ORM\Column(name="estadoTanque", type="string", length=10, nullable=true,options={"default":""})
     */
    private $estadoTanque;
        
    /**
     *
     * @var boolean
     *
     * @ORM\Column(name="debaja", type="boolean", nullable=false)
     * @Assert\Choice(choices = {true,false}, message = "Elección incorrecta (Y , S , N).")
     */
    private $debaja;
    
    
    /**
     *
     *
     *@ORM\ManyToOne(targetEntity="Hertz\ReservaBundle\Entity\Agencia",  cascade={"remove"})
     *@ORM\JoinColumn(name="sucursal", referencedColumnName="id", nullable=true )
     *@Assert\Type(type="object", message="El valor {{ value }} no es válido debe ser {{ type }}.")
     *
     */
    private $sucursal;
        
        
        /**
     *
     *
     *@ORM\ManyToOne(targetEntity="Hertz\ReservaBundle\Entity\CategoriasVeh",  cascade={"remove"})
     *@ORM\JoinColumn(name="categoria", referencedColumnName="id", nullable=true )
     *@Assert\Type(type="object", message="El valor {{ value }} no es válido debe ser {{ type }}.")
     *
     */
    private $categoria;
        
        
    /**
     *
     * @var string
     *
     * @ORM\Column(name="nromotor", type="string", length=255, nullable=false, options={"default":""})
     * @Assert\NotBlank(message="El campo es obligatorio.")
     */
    private $nromotor;
    /**
     *
     * @var string
     *
     * @ORM\Column(name="nrochasis", type="string", length=255, nullable=false, options={"default":""})
     * @Assert\NotBlank(message="El campo es obligatorio.")
     */
    private $nrochasis;

    /**
     *
     *
     *@ORM\ManyToOne(targetEntity="Hertz\ReservaBundle\Entity\Marca",  cascade={"remove"})
     *@ORM\JoinColumn(name="marca", referencedColumnName="id", nullable=false )
     *@Assert\NotBlank(message="El campo Agencia es obligatorio.")
     *@Assert\Type(type="object", message="El valor {{ value }} no es válido debe ser {{ type }}.")
     *
     */
    private $marca;
    

    /**
    * @ORM\ManyToMany(targetEntity="Hertz\ReservaBundle\Entity\Accesorios", mappedBy="vehiculo", cascade={"persist"})
    */
    private $accesorios;

    /**
     * @ORM\ManyToOne(targetEntity="Hertz\ReservaBundle\Entity\Colores", cascade={"persist"})
     * @ORM\JoinColumn(name="color", referencedColumnName="id")
     */
    private $color;
    
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Vehiculo
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set modelo
     *
     * @param integer $modelo
     * @return Vehiculo
     */
    public function setModelo($modelo)
    {
        $this->modelo = $modelo;

        return $this;
    }

    /**
     * Get modelo
     *
     * @return integer 
     */
    public function getModelo()
    {
        return $this->modelo;
    }

    /**
     * Set kilometros
     *
     * @param integer $kilometros
     * @return Vehiculo
     */
    public function setKilometros($kilometros)
    {
        $this->kilometros = $kilometros;

        return $this;
    }

    /**
     * Get kilometros
     *
     * @return integer 
     */
    public function getKilometros()
    {
        return $this->kilometros;
    }


    /**
     * Set modeloCal
     *
     * @param string $modeloCal
     * @return Vehiculo
     */
    public function setModeloCal($modeloCal)
    {
        $this->modeloCal = $modeloCal;

        return $this;
    }

    /**
     * Get modeloCal
     *
     * @return string 
     */
    public function getModeloCal()
    {
        return $this->modeloCal;
    }

    /**
     * Set fechaHoraDevolucion
     *
     * @param \DateTime $fechaHoraDevolucion
     * @return Vehiculo
     */
    public function setFechaHoraDevolucion($fechaHoraDevolucion)
    {
        $this->fechaHoraDevolucion = $fechaHoraDevolucion;

        return $this;
    }

    /**
     * Get fechaHoraDevolucion
     *
     * @return \DateTime 
     */
    public function getFechaHoraDevolucion()
    {
        return $this->fechaHoraDevolucion;
    }

    /**
     * Set fechaHoraSalida
     *
     * @param \DateTime $fechaHoraSalida
     * @return Vehiculo
     */
    public function setFechaHoraSalida($fechaHoraSalida)
    {
        $this->fechaHoraSalida = $fechaHoraSalida;

        return $this;
    }

    /**
     * Get fechaHoraSalida
     *
     * @return \DateTime 
     */
    public function getFechaHoraSalida()
    {
        return $this->fechaHoraSalida;
    }

    /**
     * Set estado
     *
     * @param \DateTime $estado
     * @return Vehiculo
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return \DateTime 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set debaja
     *
     * @param boolean $debaja
     * @return Vehiculo
     */
    public function setDebaja($debaja)
    {
        $this->debaja = $debaja;

        return $this;
    }

    /**
     * Get debaja
     *
     * @return boolean 
     */
    public function getDebaja()
    {
        return $this->debaja;
    }

    /**
     * Set fechaHoraRetorno
     *
     * @param \DateTime $fechaHoraRetorno
     * @return Vehiculo
     */
    public function setFechaHoraRetorno($fechaHoraRetorno)
    {
        $this->fechaHoraRetorno = $fechaHoraRetorno;

        return $this;
    }

    /**
     * Get fechaHoraRetorno
     *
     * @return \DateTime 
     */
    public function getFechaHoraRetorno()
    {
        return $this->fechaHoraRetorno;
    }

    /**
     * Set agencia
     *
     * @param \Hertz\ReservaBundle\Entity\Agencia $agencia
     * @return Vehiculo
     */
    public function setAgencia(\Hertz\ReservaBundle\Entity\Agencia $agencia)
    {
        $this->agencia = $agencia;

        return $this;
    }

    /**
     * Get agencia
     *
     * @return \Hertz\ReservaBundle\Entity\Agencia 
     */
    public function getAgencia()
    {
        return $this->agencia;
    }

    /**
     * Set dominio
     *
     * @param string $dominio
     * @return Vehiculo
     */
    public function setDominio($dominio)
    {
        $this->dominio = $dominio;

        return $this;
    }

    /**
     * Get dominio
     *
     * @return string 
     */
    public function getDominio()
    {
        return $this->dominio;
    }

    /**
     * Set sucursal
     *
     * @param integer $sucursal
     * @return Vehiculo
     */
    public function setSucursal($sucursal)
    {
        $this->sucursal = $sucursal;

        return $this;
    }

    /**
     * Get sucursal
     *
     * @return integer 
     */
    public function getSucursal()
    {
        return $this->sucursal;
    }

    /**
     * Set estadoTanque
     *
     * @param string $estadoTanque
     * @return Vehiculo
     */
    public function setEstadoTanque($estadoTanque)
    {
        $this->estadoTanque = $estadoTanque;

        return $this;
    }

    /**
     * Get estadoTanque
     *
     * @return string 
     */
    public function getEstadoTanque()
    {
        return $this->estadoTanque;
    }

    /**
     * Set nromotor
     *
     * @param string $nromotor
     * @return Vehiculo
     */
    public function setNromotor($nromotor)
    {
        $this->nromotor = $nromotor;

        return $this;
    }

    /**
     * Get nromotor
     *
     * @return string 
     */
    public function getNromotor()
    {
        return $this->nromotor;
    }

    /**
     * Set nrochasis
     *
     * @param string $nrochasis
     * @return Vehiculo
     */
    public function setNrochasis($nrochasis)
    {
        $this->nrochasis = $nrochasis;

        return $this;
    }

    /**
     * Get nrochasis
     *
     * @return string 
     */
    public function getNrochasis()
    {
        return $this->nrochasis;
    }

    /**
     * Set marca
     *
     * @param \Hertz\ReservaBundle\Entity\Marca $marca
     * @return Vehiculo
     */
    public function setMarca(\Hertz\ReservaBundle\Entity\Marca $marca)
    {
        $this->marca = $marca;

        return $this;
    }

    /**
     * Get marca
     *
     * @return \Hertz\ReservaBundle\Entity\Marca 
     */
    public function getMarca()
    {
        return $this->marca;
    }

    /**
     * Set categoria
     *
     * @param \Hertz\ReservaBundle\Entity\CategoriasVeh $categoria
     * @return Vehiculo
     */
    public function setCategoria(\Hertz\ReservaBundle\Entity\CategoriasVeh $categoria = null)
    {
        $this->categoria = $categoria;

        return $this;
    }

    /**
     * Get categoria
     *
     * @return \Hertz\ReservaBundle\Entity\CategoriasVeh 
     */
    public function getCategoria()
    {
        return $this->categoria;
    }

    /**
     * Set horaSalida
     *
     * @param string $horaSalida
     * @return Vehiculo
     */
    public function setHoraSalida($horaSalida)
    {
        $this->horaSalida = $horaSalida;

        return $this;
    }

    /**
     * Get horaSalida
     *
     * @return string 
     */
    public function getHoraSalida()
    {
        return $this->horaSalida;
    }

    /**
     * Set horaDevolucion
     *
     * @param string $horaDevolucion
     * @return Vehiculo
     */
    public function setHoraDevolucion($horaDevolucion)
    {
        $this->horaDevolucion = $horaDevolucion;

        return $this;
    }

    /**
     * Get horaDevolucion
     *
     * @return string 
     */
    public function getHoraDevolucion()
    {
        return $this->horaDevolucion;
    }

    /**
     * Set idReserva
     *
     * @param \Hertz\ReservaBundle\Entity\Reserva $idReserva
     * @return Vehiculo
     */
    public function setIdReserva(\Hertz\ReservaBundle\Entity\Reserva $idReserva = null)
    {
        $this->idReserva = $idReserva;

        return $this;
    }

    /**
     * Get idReserva
     *
     * @return \Hertz\ReservaBundle\Entity\Reserva 
     */
    public function getIdReserva()
    {
        return $this->idReserva;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->accesorios = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add accesorios
     *
     * @param \Hertz\ReservaBundle\Entity\Accesorios $accesorios
     * @return Vehiculo
     */
    public function addAccesorio(\Hertz\ReservaBundle\Entity\Accesorios $accesorios)
    {
        $accesorios->addVehiculo($this); // Call Post's setter here
        $this->accesorios[] = $accesorios;

        return $this;
    }

    /**
     * Remove accesorios
     *
     * @param \Hertz\ReservaBundle\Entity\Accesorios $accesorios
     */
    public function removeAccesorio(\Hertz\ReservaBundle\Entity\Accesorios $accesorios)
    {
        $accesorios->removeVehiculo($this); // Call Post's setter here
        $this->accesorios->removeElement($accesorios);
    }

    /**
     * Get accesorios
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAccesorios()
    {
        return $this->accesorios;
    }

    /**
     * Set color
     *
     * @param \Hertz\ReservaBundle\Entity\Colores $color
     * @return Vehiculo
     */
    public function setColor(\Hertz\ReservaBundle\Entity\Colores $color = null)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color
     *
     * @return \Hertz\ReservaBundle\Entity\Colores 
     */
    public function getColor()
    {
        return $this->color;
    }
}
