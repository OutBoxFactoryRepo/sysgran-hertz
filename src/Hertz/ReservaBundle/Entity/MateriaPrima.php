<?php
namespace Hertz\ReservaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\DiExtraBundle\Annotation as DI;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Doctrine\ORM\EntityManager;

/**
 * MateriaPrima
 *
 * @ORM\Table(name="materiasprimas")
 * @ORM\Entity(repositoryClass="MateriaPrimaRepository")
 */
class MateriaPrima{
	
	const ORM_ENTITY = "HertzReservaBundle:MateriaPrima";

	/**
	 *
	 * @var integer
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @Assert\Type(type="integer", message="Debe ser entero mayor a 0") 
	 */
	private $id;
	
	/**
	 *
	 * @var string
	 *
	 * @ORM\Column(name="descripcion", type="string", length=255, nullable=true)
	 * @Assert\Length(
     *      min = "2",
     *      max = "255",
     *      minMessage = "Debe tener al menos {{ limit }} caracteres de largo",
     *      maxMessage = "Debe tener un máximo de {{ limit }} caracteres"
     * )
	 */
	private $descripcion;
	

	/**
	 *
	 *
	 *@ORM\ManyToOne(targetEntity="Hertz\ReservaBundle\Entity\Agencia",  cascade={"remove"})
	 *@ORM\JoinColumn(name="agencia", referencedColumnName="id", nullable=false )
	 *@Assert\NotBlank(message="El campo Agencia es obligatorio.")
	 *@Assert\Type(type="object", message="El valor {{ value }} no es válido debe ser {{ type }}.")
	 *
	 */
	private $agencia;
	
	
	/**
	 *
	 * @var integer
	 *
	 * @ORM\Column(name="cantidad", type="integer", nullable=false)
	 * @Assert\NotBlank(message="El campo cantidad es obligatorio.")
	 */
	private $cantidad;	
	
	/**
	 *
	 * @var integer
	 *
	 * @ORM\Column(name="stockcritico", type="integer", nullable=false)
	 * @Assert\NotBlank(message="El campo stock crítico es obligatorio.")
	 */
	private $stockCritico;	
	
	/**
	 *
	 * @var float
	 *
	 * @ORM\Column(name="valor", type="float", nullable=false)
	 * @Assert\NotBlank(message="El campo valor es obligatorio.")
	 */
	private $valor;
			
	/**
	 *
	 * @var datetime
	 *
	 * @ORM\Column(name="fechaCarga", type="datetime", nullable=false)
	 * @Assert\NotBlank(message="El campo fecha es obligatorio.")
	 */
	private $fechaCarga;
	
	/**
	 *
	 * @var string
	 *
	 * @ORM\Column(name="usuario", type="string", length=255, nullable=false)
	 * @Assert\NotBlank(message="El campo usuario es obligatorio.")
	 * @Assert\Length(
     *      min = "2",
     *      max = "255",
     *      minMessage = "Debe tener al menos {{ limit }} caracteres de largo",
     *      maxMessage = "Debe tener un máximo de {{ limit }} caracteres"
     * )
	 */
	private $usuario;

	
	/**
	 *
	 * @var boolean
	 *
	 * @ORM\Column(name="debaja", type="boolean", nullable=false)
	 * @Assert\Choice(choices = {true,false}, message = "Elección incorrecta (Y , S , N).")
	 */
	private $debaja;


    /**
     *
     *@ORM\ManyToOne(targetEntity="MateriaPrimaCategoria",  cascade={"remove"})
     *@ORM\JoinColumn(name="categoria", referencedColumnName="id", nullable=true)
     *@Assert\Type(type="object", message="El valor {{ value }} no es válido debe ser {{ type }}.")
     *
     */
    private $categoria;

    /**
     * @return mixed
     */
    public function getCategoria()
    {
        return $this->categoria;
    }

    /**
     * @param mixed $categoria
     */
    public function setCategoria($categoria)
    {
        $this->categoria = $categoria;
    }


	
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return MateriaPrima
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set cantidad
     *
     * @param integer $cantidad
     * @return MateriaPrima
     */
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    /**
     * Get cantidad
     *
     * @return integer 
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * Set stockCritico
     *
     * @param integer $stockCritico
     * @return MateriaPrima
     */
    public function setStockCritico($stockCritico)
    {
        $this->stockCritico = $stockCritico;

        return $this;
    }

    /**
     * Get stockCritico
     *
     * @return integer 
     */
    public function getStockCritico()
    {
        return $this->stockCritico;
    }

    /**
     * Set valor
     *
     * @param float $valor
     * @return MateriaPrima
     */
    public function setValor($valor)
    {
        $this->valor = $valor;

        return $this;
    }

    /**
     * Get valor
     *
     * @return float 
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * Set fechaCarga
     *
     * @param \DateTime $fechaCarga
     * @return MateriaPrima
     */
    public function setFechaCarga($fechaCarga)
    {
        $this->fechaCarga = $fechaCarga;

        return $this;
    }

    /**
     * Get fechaCarga
     *
     * @return \DateTime 
     */
    public function getFechaCarga()
    {
        return $this->fechaCarga;
    }

    /**
     * Set usuario
     *
     * @param string $usuario
     * @return MateriaPrima
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return string 
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set debaja
     *
     * @param boolean $debaja
     * @return MateriaPrima
     */
    public function setDebaja($debaja)
    {
        $this->debaja = $debaja;

        return $this;
    }

    /**
     * Get debaja
     *
     * @return boolean 
     */
    public function getDebaja()
    {
        return $this->debaja;
    }

    /**
     * Set agencia
     *
     * @param \Hertz\ReservaBundle\Entity\Agencia $agencia
     * @return MateriaPrima
     */
    public function setAgencia(\Hertz\ReservaBundle\Entity\Agencia $agencia)
    {
        $this->agencia = $agencia;

        return $this;
    }

    /**
     * Get agencia
     *
     * @return \Hertz\ReservaBundle\Entity\Agencia 
     */
    public function getAgencia()
    {
        return $this->agencia;
    }
}
