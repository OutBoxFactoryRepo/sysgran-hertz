<?php
namespace Hertz\ReservaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\DiExtraBundle\Annotation as DI;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Doctrine\ORM\EntityManager;

/**
 * CondicionIva
 *
 * @ORM\Table(name="condicionIva")
 * @ORM\Entity(repositoryClass="CondicionIvaRepository")
 */
class CondicionIva{
	
	const ORM_ENTITY = "HertzReservaBundle:CondicionIva";

	/**
	 *
	 * @var integer
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @Assert\Type(type="integer", message="Debe ser entero mayor a 0|id") 
	 */
	private $id;

	/**
	 *
	 * @var string
	 *
	 * @ORM\Column(name="tipo", type="string", length=1, nullable=false)
	 * @Assert\NotNull(message="El campo tipo es obligatorio.|tipo")
	 */
	private $tipo;

	/**
	 *
	 * @var float
	 *
	 * @ORM\Column(name="porcentaje", type="float", scale = 2,  nullable=false)
	 * @Assert\NotNull(message="El campo porcentaje es obligatorio.|porcentaje")
	 */
	private $porcentaje;

	/**
	 *
	 * @var string
	 *
	 * @ORM\Column(name="descripcion", type="string", length = 100,  nullable=false)
	 * @Assert\NotNull(message="El campo descripción es obligatorio.|descripcion")
	 */
	private $descripcion;
	
	
	/**
	 *
	 * @var string
	 *
	 * @ORM\Column(name="detalle", type="string", length = 200,  nullable=false)
	 * @Assert\NotNull(message="El campo detalle es obligatorio.|detalle")
	 */
	private $destalle;
	
	/**
	 *
	 * @var integer
	 *
	 * @ORM\Column(name="idCondIvaAfip", type="integer",  nullable=true)
	 */
	private $idCondIvaAfip;
	
	/**
	 *
	 * @var string
	 *
	 * @ORM\Column(name="idTipoAfip", type="string", length = 3,  nullable=true)
	 */
	private $idTipoAfip;
	
	/**
	 *
	 * @var boolean
	 *
	 * @ORM\Column(name="debaja", type="boolean")
	 */
	private $debaja;


    /**
     * Set id
     *
     * @param integer $id
     * @return CondicionIva
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     * @return CondicionIva
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return string 
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set porcentaje
     *
     * @param float $porcentaje
     * @return CondicionIva
     */
    public function setPorcentaje($porcentaje)
    {
        $this->porcentaje = $porcentaje;

        return $this;
    }

    /**
     * Get porcentaje
     *
     * @return float 
     */
    public function getPorcentaje()
    {
        return $this->porcentaje;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return CondicionIva
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set idCondIvaAfip
     *
     * @param integer $idCondIvaAfip
     * @return CondicionIva
     */
    public function setIdCondIvaAfip($idCondIvaAfip)
    {
        $this->idCondIvaAfip = $idCondIvaAfip;

        return $this;
    }

    /**
     * Get idCondIvaAfip
     *
     * @return integer 
     */
    public function getIdCondIvaAfip()
    {
        return $this->idCondIvaAfip;
    }

    /**
     * Set idTipoAfip
     *
     * @param string $idTipoAfip
     * @return CondicionIva
     */
    public function setIdTipoAfip($idTipoAfip)
    {
        $this->idTipoAfip = $idTipoAfip;

        return $this;
    }

    /**
     * Get idTipoAfip
     *
     * @return string 
     */
    public function getIdTipoAfip()
    {
        return $this->idTipoAfip;
    }

    /**
     * Set debaja
     *
     * @param boolean $debaja
     * @return CondicionIva
     */
    public function setDebaja($debaja)
    {
        $this->debaja = $debaja;

        return $this;
    }

    /**
     * Get debaja
     *
     * @return boolean 
     */
    public function getDebaja()
    {
        return $this->debaja;
    }

    /**
     * Set destalle
     *
     * @param string $destalle
     * @return CondicionIva
     */
    public function setDestalle($destalle)
    {
        $this->destalle = $destalle;

        return $this;
    }

    /**
     * Get destalle
     *
     * @return string 
     */
    public function getDestalle()
    {
        return $this->destalle;
    }
}
