<?php
namespace Hertz\ReservaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\DiExtraBundle\Annotation as DI;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Doctrine\ORM\EntityManager;

/**
 * Provincias
 *
 * @ORM\Table(name="provincias")
 * @ORM\Entity(repositoryClass="ProvinciasRepository")
 */
class Provincias{
	
	const ORM_ENTITY = "HertzReservaBundle:Provincias";

	/**
	 *
	 * @var integer
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @Assert\Type(type="integer", message="Debe ser entero mayor a 0|id") 
	 */
	private $id;


	/**
	 *
	 * @var string
	 *
	 * @ORM\Column(name="descripcion", type="string", length = 100,  nullable=false)
	 * @Assert\NotNull(message="El campo descripción es obligatorio.|descripcion")
	 */
	private $descripcion;
	
	/**
	 *
	 * @var string
	 *
	 * @ORM\Column(name="codigoIIBB", type="string", length = 3,  nullable=false)
	 * @Assert\NotNull(message="El campo codigo de IIBB es obligatorio.|codigoIIBB")
	 */
	private $codigoIIBB;
	
	/**
	 *
	 * @var boolean
	 *
	 * @ORM\Column(name="debaja", type="boolean")
	 */
	private $debaja;




    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Provincias
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set codigoIIBB
     *
     * @param string $codigoIIBB
     * @return Provincias
     */
    public function setCodigoIIBB($codigoIIBB)
    {
        $this->codigoIIBB = $codigoIIBB;

        return $this;
    }

    /**
     * Get codigoIIBB
     *
     * @return string 
     */
    public function getCodigoIIBB()
    {
        return $this->codigoIIBB;
    }

    /**
     * Set debaja
     *
     * @param boolean $debaja
     * @return Provincias
     */
    public function setDebaja($debaja)
    {
        $this->debaja = $debaja;

        return $this;
    }

    /**
     * Get debaja
     *
     * @return boolean 
     */
    public function getDebaja()
    {
        return $this->debaja;
    }
}
