<?php

namespace Hertz\ReservaBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\HttpFoundation\Request;

/**
 * MetodoPagoRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class MetodoPagoRepository extends EntityRepository
{
    public function getAll()
	{
		$qb = $this->getEntityManager()->createQueryBuilder()
		->select('u')
		->from(MetodoPago::ORM_ENTITY, 'u')
		->getQuery();
		return $qb->getResult();
	}
	
	
	public function getOne($id)
	{
		
		$qb = $this->getEntityManager()->createQueryBuilder()
			->select('u')
			->from(MetodoPago::ORM_ENTITY, 'u')
			->where('u.id =:identifier and u.debaja =:identifier2')
			->setParameter('identifier', $id)
			->setParameter('identifier2', false)
			->getQuery();
			return $qb->getResult();
		
	}

}