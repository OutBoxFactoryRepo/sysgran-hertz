<?php
namespace Hertz\ReservaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\DiExtraBundle\Annotation as DI;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Doctrine\ORM\EntityManager;

/**
 * Taller
 *
 * @ORM\Table(name="talleres")
 * @ORM\Entity(repositoryClass="TallerRepository")
 */
class Taller{
	
	const ORM_ENTITY = "HertzReservaBundle:Taller";

	/**
	 *
	 * @var integer
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @Assert\Type(type="integer", message="Debe ser entero mayor a 0") 
	 */
	private $id;
	
	/**
	 *
	 * @var string
	 *
	 * @ORM\Column(name="descripcion", type="string", length=255, nullable=true)
	 * @Assert\Length(
     *      min = "2",
     *      max = "255",
     *      minMessage = "Debe tener al menos {{ limit }} caracteres de largo",
     *      maxMessage = "Debe tener un máximo de {{ limit }} caracteres"
     * )
	 */
	private $descripcion;
	

	/**
	 *
	 *
	 *@ORM\ManyToOne(targetEntity="Hertz\ReservaBundle\Entity\Agencia",  cascade={"remove"})
	 *@ORM\JoinColumn(name="agencia", referencedColumnName="id", nullable=false )
	 *@Assert\NotBlank(message="El campo Agencia es obligatorio.")
	 *@Assert\Type(type="object", message="El valor {{ value }} no es válido debe ser {{ type }}.")
	 *
	 */
	private $agencia;
			
	/**
	 *
	 * @var datetime
	 *
	 * @ORM\Column(name="fechaCarga", type="datetime", nullable=false)
	 * @Assert\NotBlank(message="El campo fecha es obligatorio.")
	 */
	private $fechaCarga;
	
	/**
	 *
	 * @var string
	 *
	 * @ORM\Column(name="usuario", type="string", length=255, nullable=false)
	 * @Assert\NotBlank(message="El campo usuario es obligatorio.")
	 * @Assert\Length(
     *      min = "2",
     *      max = "255",
     *      minMessage = "Debe tener al menos {{ limit }} caracteres de largo",
     *      maxMessage = "Debe tener un máximo de {{ limit }} caracteres"
     * )
	 */
	private $usuario;

	
	/**
	 *
	 * @var boolean
	 *
	 * @ORM\Column(name="debaja", type="boolean", nullable=false)
	 * @Assert\Choice(choices = {true,false}, message = "Elección incorrecta (Y , S , N).")
	 */
	private $debaja;
        
        
        /**
	 *
	 * @var boolean
	 *
	 * @ORM\Column(name="externo", type="boolean", nullable=false, options={"default":false})
	 * @Assert\Choice(choices = {true,false}, message = "Elección true or false")
	 */
	private $externo;
        
        
        /**
	 *
	 * @var string
	 *
	 * @ORM\Column(name="nombreTallerExterno", type="string", length=255, nullable=true)
	 * @Assert\Length(
        *      min = "2",
        *      max = "255",
        *      minMessage = "Debe tener al menos {{ limit }} caracteres de largo",
        *      maxMessage = "Debe tener un máximo de {{ limit }} caracteres"
        * )
	 */
	private $nombreTallerExterno;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Taller
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set fechaCarga
     *
     * @param \DateTime $fechaCarga
     * @return Taller
     */
    public function setFechaCarga($fechaCarga)
    {
        $this->fechaCarga = $fechaCarga;

        return $this;
    }

    /**
     * Get fechaCarga
     *
     * @return \DateTime 
     */
    public function getFechaCarga()
    {
        return $this->fechaCarga;
    }

    /**
     * Set usuario
     *
     * @param string $usuario
     * @return Taller
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return string 
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set debaja
     *
     * @param boolean $debaja
     * @return Taller
     */
    public function setDebaja($debaja)
    {
        $this->debaja = $debaja;

        return $this;
    }

    /**
     * Get debaja
     *
     * @return boolean 
     */
    public function getDebaja()
    {
        return $this->debaja;
    }

    /**
     * Set agencia
     *
     * @param \Hertz\ReservaBundle\Entity\Agencia $agencia
     * @return Taller
     */
    public function setAgencia(\Hertz\ReservaBundle\Entity\Agencia $agencia)
    {
        $this->agencia = $agencia;

        return $this;
    }

    /**
     * Get agencia
     *
     * @return \Hertz\ReservaBundle\Entity\Agencia 
     */
    public function getAgencia()
    {
        return $this->agencia;
    }

    /**
     * Set externo
     *
     * @param boolean $externo
     * @return Taller
     */
    public function setExterno($externo)
    {
        $this->externo = $externo;

        return $this;
    }

    /**
     * Get externo
     *
     * @return boolean 
     */
    public function getExterno()
    {
        return $this->externo;
    }

    /**
     * Set nombreTallerExterno
     *
     * @param string $nombreTallerExterno
     * @return Taller
     */
    public function setNombreTallerExterno($nombreTallerExterno)
    {
        $this->nombreTallerExterno = $nombreTallerExterno;

        return $this;
    }

    /**
     * Get nombreTallerExterno
     *
     * @return string 
     */
    public function getNombreTallerExterno()
    {
        return $this->nombreTallerExterno;
    }
}
