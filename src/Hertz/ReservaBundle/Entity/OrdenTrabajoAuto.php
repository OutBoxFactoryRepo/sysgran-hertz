<?php
namespace Hertz\ReservaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\DiExtraBundle\Annotation as DI;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Doctrine\ORM\EntityManager;

/**
 * OrdenTrabajoAuto
 *
 * @ORM\Table(name="ordenesTrabajoAuto")
 * @ORM\Entity(repositoryClass="OrdenTrabajoAutoRepository")
 */
class OrdenTrabajoAuto{
	
	const ORM_ENTITY = "HertzReservaBundle:OrdenTrabajoAuto";

	/**
	 *
	 * @var integer
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @Assert\Type(type="integer", message="Debe ser entero mayor a 0") 
	 */
	private $id;
	
	/**
	 *
	 * @var string
	 *
	 * @ORM\Column(name="descripcion", type="string", length=255, nullable=false)
	 * @Assert\NotBlank(message="El campo Descripción es obligatorio.")
	 * @Assert\Length(
     *      min = "2",
     *      max = "255",
     *      minMessage = "Debe tener al menos {{ limit }} caracteres de largo",
     *      maxMessage = "Debe tener un máximo de {{ limit }} caracteres"
     * )
	 */
	private $descripcion;
	

	/**
	 * 
	 *
	 *@ORM\ManyToOne(targetEntity="Hertz\ReservaBundle\Entity\Vehiculo",  cascade={"remove"})
         *@ORM\JoinColumn(name="vehiculo", referencedColumnName="id", nullable=true )
	 * 
	 */
	private $vehiculo;
		
	
	/**
	 *
	 * @var integer
	 *
	 * @ORM\Column(name="kilometros", type="integer", nullable=false)
	 * @Assert\NotBlank(message="El campo Kilometros es obligatorio.")
	 * @Assert\Type(type="numeric", message="El valor {{ value }} no es válido debe ser {{ type }}.")
	 */
	private $kilometros;
	
	
	/**
	 *
	 * @var datetime
	 *
	 * @ORM\Column(name="fechaCarga", type="datetime", nullable=false)
	 * @Assert\NotBlank(message="El campo fecha es obligatorio.")
	 */
	private $fechaCarga;
	
        /**
         *
         * @var string
         *
         * @ORM\Column(name="usuario", type="string", length=255, nullable=false)
         * @Assert\NotBlank(message="El campo usuario es obligatorio.")
         * @Assert\Length(
         *      min = "2",
         *      max = "255",
         *      minMessage = "Debe tener al menos {{ limit }} caracteres de largo",
         *      maxMessage = "Debe tener un máximo de {{ limit }} caracteres"
         * )
	 */
	private $usuario;	
		

	/**
	 *
	 * @var boolean
	 *
	 * @ORM\Column(name="debaja", type="boolean", nullable=false)
	 * @Assert\Choice(choices = {true,false}, message = "Elección incorrecta (Y , S , N).")
	 */
	private $debaja;

	/**
	 * 
	 *
	 *@ORM\ManyToOne(targetEntity="Hertz\ReservaBundle\Entity\Taller",  cascade={"remove"})
         *@ORM\JoinColumn(name="taller", referencedColumnName="id", nullable=false )
	 * 
	 */
	private $taller;

        /**
	 * 
	 *
	 *@ORM\ManyToOne(targetEntity="Hertz\ReservaBundle\Entity\CategoriasVeh",  cascade={"remove"})
         *@ORM\JoinColumn(name="categoria", referencedColumnName="id", nullable=true )
	 * 
	 */
	private $categoriaVeh;
   


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return OrdenTrabajoAuto
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set kilometros
     *
     * @param integer $kilometros
     * @return OrdenTrabajoAuto
     */
    public function setKilometros($kilometros)
    {
        $this->kilometros = $kilometros;

        return $this;
    }

    /**
     * Get kilometros
     *
     * @return integer 
     */
    public function getKilometros()
    {
        return $this->kilometros;
    }

    /**
     * Set fechaCarga
     *
     * @param \DateTime $fechaCarga
     * @return OrdenTrabajoAuto
     */
    public function setFechaCarga($fechaCarga)
    {
        $this->fechaCarga = $fechaCarga;

        return $this;
    }

    /**
     * Get fechaCarga
     *
     * @return \DateTime 
     */
    public function getFechaCarga()
    {
        return $this->fechaCarga;
    }

    /**
     * Set usuario
     *
     * @param string $usuario
     * @return OrdenTrabajoAuto
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return string 
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set debaja
     *
     * @param boolean $debaja
     * @return OrdenTrabajoAuto
     */
    public function setDebaja($debaja)
    {
        $this->debaja = $debaja;

        return $this;
    }

    /**
     * Get debaja
     *
     * @return boolean 
     */
    public function getDebaja()
    {
        return $this->debaja;
    }

    /**
     * Set vehiculo
     *
     * @param \Hertz\ReservaBundle\Entity\Vehiculo $vehiculo
     * @return OrdenTrabajoAuto
     */
    public function setVehiculo(\Hertz\ReservaBundle\Entity\Vehiculo $vehiculo = null)
    {
        $this->vehiculo = $vehiculo;

        return $this;
    }

    /**
     * Get vehiculo
     *
     * @return \Hertz\ReservaBundle\Entity\Vehiculo 
     */
    public function getVehiculo()
    {
        return $this->vehiculo;
    }

    /**
     * Set taller
     *
     * @param \Hertz\ReservaBundle\Entity\Taller $taller
     * @return OrdenTrabajoAuto
     */
    public function setTaller(\Hertz\ReservaBundle\Entity\Taller $taller)
    {
        $this->taller = $taller;

        return $this;
    }

    /**
     * Get taller
     *
     * @return \Hertz\ReservaBundle\Entity\Taller 
     */
    public function getTaller()
    {
        return $this->taller;
    }

    /**
     * Set categoriaVeh
     *
     * @param \Hertz\ReservaBundle\Entity\CategoriasVeh $categoriaVeh
     * @return OrdenTrabajoAuto
     */
    public function setCategoriaVeh(\Hertz\ReservaBundle\Entity\CategoriasVeh $categoriaVeh = null)
    {
        $this->categoriaVeh = $categoriaVeh;

        return $this;
    }

    /**
     * Get categoriaVeh
     *
     * @return \Hertz\ReservaBundle\Entity\CategoriasVeh 
     */
    public function getCategoriaVeh()
    {
        return $this->categoriaVeh;
    }
}
