<?php
namespace Hertz\ReservaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\DiExtraBundle\Annotation as DI;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Doctrine\ORM\EntityManager;

/**
 * Modulo
 *
 * @ORM\Table(name="modulos")
 * @ORM\Entity(repositoryClass="ModuloRepository")
 */
class Modulo{
	
	const ORM_ENTITY = "HertzReservaBundle:Modulo";

	/**
	 *
	 * @var integer
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @Assert\Type(type="integer", message="Debe ser entero mayor a 0|id") 
	 */
	private $id;

	/**
	 *
	 * @var string
	 *
	 * @ORM\Column(name="descripcion", type="string", length=501, nullable=true)
	 * @Assert\NotNull(message="El campo Descripción es obligatorio.|Descripcion")
	 */
	private $descripcion;

        
        /**
	 *
	 * @var string
	 *
	 * @ORM\Column(name="nombreJs", type="string", length=501, nullable=false)
	 * @Assert\NotNull(message="El campo Nombre JS es obligatorio.|nombreJs")
	 */
	private $nombreJs;

        /**
	 * @ORM\ManyToMany(targetEntity="Hertz\UserBundle\Entity\Role", inversedBy="idReserva", cascade={"persist"})
	 * @ORM\JoinTable(name="modulos_roles")
	 **/
	private $roles;
        
	/**
	 *
	 * @var boolean
	 *
	 * @ORM\Column(name="debaja", type="boolean", nullable=false)
	 * @Assert\Choice(choices = {true,false}, message = "Elección incorrecta (Y , S , N).")
	 */
	private $debaja;
        
    /**
     * @ORM\OneToMany(targetEntity="Hertz\ReservaBundle\Entity\ModuloPerfil", mappedBy="modulo", cascade={"all"})
     */
    private $moduloPerfil;
        
        


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->roles = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Modulo
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set nombreJs
     *
     * @param string $nombreJs
     * @return Modulo
     */
    public function setNombreJs($nombreJs)
    {
        $this->nombreJs = $nombreJs;

        return $this;
    }

    /**
     * Get nombreJs
     *
     * @return string 
     */
    public function getNombreJs()
    {
        return $this->nombreJs;
    }

    /**
     * Set debaja
     *
     * @param boolean $debaja
     * @return Modulo
     */
    public function setDebaja($debaja)
    {
        $this->debaja = $debaja;

        return $this;
    }

    /**
     * Get debaja
     *
     * @return boolean 
     */
    public function getDebaja()
    {
        return $this->debaja;
    }

    /**
     * Add roles
     *
     * @param \Hertz\UserBundle\Entity\Role $roles
     * @return Modulo
     */
    public function addRole(\Hertz\UserBundle\Entity\Role $roles)
    {
        $this->roles[] = $roles;

        return $this;
    }

    /**
     * Remove roles
     *
     * @param \Hertz\UserBundle\Entity\Role $roles
     */
    public function removeRole(\Hertz\UserBundle\Entity\Role $roles)
    {
        $this->roles->removeElement($roles);
    }

    /**
     * Get roles
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRoles()
    {
        return $this->roles;
    }
    
    
    //carga los nombres
    public function getRolesName() 
    {
        $roles = array();

        foreach ($this->getRoles() as $role) 
        {
            $roles[] = $role->getName();
        }

        return array_unique($roles);
    }
    
    //filtra por nombre
    public function hasRoleName($role_name)
    {
        return in_array($role_name, $this->getRolesName());
    }
    
    //carga los id
    public function getRolesId() 
    {
        $roles = array();

        foreach ($this->getRoles() as $role) 
        {
            $roles[] = $role->getId();
        }

        return array_unique($roles);
    }
    
    //filtra por id
    public function hasRoleId($role_id)
    {
        return in_array($role_id, $this->getRolesId());
    }
    

    /**
     * Add moduloPerfil
     *
     * @param \Hertz\ReservaBundle\Entity\ModuloPerfil $moduloPerfil
     * @return Modulo
     */
    public function addModuloPerfil(\Hertz\ReservaBundle\Entity\ModuloPerfil $moduloPerfil)
    {
        $moduloPerfil->setModulo($this);
        $this->moduloPerfil[] = $moduloPerfil;

        return $this;
    }

    /**
     * Remove moduloPerfil
     *
     * @param \Hertz\ReservaBundle\Entity\ModuloPerfil $moduloPerfil
     */
    public function removeModuloPerfil(\Hertz\ReservaBundle\Entity\ModuloPerfil $moduloPerfil)
    {
        $this->moduloPerfil->removeElement($moduloPerfil);
    }

    /**
     * Get moduloPerfil
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getModuloPerfil()
    {
        return $this->moduloPerfil;
    }
}
