<?php

namespace Hertz\ReservaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\DiExtraBundle\Annotation as DI;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Doctrine\ORM\EntityManager;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * Colores
 *
 * @ORM\Table(name="colores")
 * @ORM\Entity(repositoryClass="ColoresRepository")
 */
class Colores
{
    const ORM_ENTITY = "HertzReservaBundle:Colores";

    /**
     *
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Assert\Type(type="integer", message="Debe ser entero mayor a 0") 
     */
    private $id;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="color", type="string", length=7, nullable=false)
     * @Assert\NotBlank(message="El campo es obligatorio.")
     */
    private $color;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=255, nullable=true)
     * @Assert\Length(
     *      min = "2",
     *      max = "255",
     *      minMessage = "Debe tener al menos {{ limit }} caracteres de largo",
     *      maxMessage = "Debe tener un máximo de {{ limit }} caracteres"
     * )
     */
    private $descripcion;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set color
     *
     * @param string $color
     * @return Colores
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color
     *
     * @return string 
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Colores
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }
}
