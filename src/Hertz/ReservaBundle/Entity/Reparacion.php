<?php
namespace Hertz\ReservaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\DiExtraBundle\Annotation as DI;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Doctrine\ORM\EntityManager;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Reparacion
 *
 * @ORM\Table(name="reparaciones")
 * @ORM\Entity(repositoryClass="ReparacionRepository")
 */
class Reparacion{
	
	const ORM_ENTITY = "HertzReservaBundle:Reparacion";

	/**
	 *
	 * @var integer
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @Assert\Type(type="integer", message="Debe ser entero mayor a 0") 
	 */
	private $id;

	/**
	 *
	 * @var string
	 *
	 * @ORM\Column(name="descripcion", type="string", length=255, nullable=false)
	 * @Assert\NotBlank(message="El campo es obligatorio.")
	 * @Assert\Length(
     *      min = "2",
     *      max = "255",
     *      minMessage = "Debe tener al menos {{ limit }} caracteres de largo",
     *      maxMessage = "Debe tener un máximo de {{ limit }} caracteres"
     * )
	 */
	private $descripcion;
	
		
	/**
	 *
	 * @var boolean
	 *
	 * @ORM\Column(name="debaja", type="boolean", nullable=false)
	 * @Assert\NotNull(message="El campo es obligatorio.")
	 */
	private $debaja;

        /**
	 * @ORM\ManyToMany(targetEntity="Hertz\ReservaBundle\Entity\Tareas", inversedBy="idReparacion", cascade={"persist"})
	 * @ORM\JoinTable(name="reparacion_tareas")
         * @ORM\OrderBy({"id" = "ASC"})
	 **/
	private $tareas;
    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Reparacion
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set debaja
     *
     * @param boolean $debaja
     * @return Reparacion
     */
    public function setDebaja($debaja)
    {
        $this->debaja = $debaja;

        return $this;
    }

    /**
     * Get debaja
     *
     * @return boolean 
     */
    public function getDebaja()
    {
        return $this->debaja;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->tareas = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add tareas
     *
     * @param \Hertz\ReservaBundle\Entity\Tareas $tareas
     * @return Reparacion
     */
    public function addTarea(\Hertz\ReservaBundle\Entity\Tareas $tareas)
    {
        $this->tareas[] = $tareas;

        return $this;
    }

    /**
     * Remove tareas
     *
     * @param \Hertz\ReservaBundle\Entity\Tareas $tareas
     */
    public function removeTarea(\Hertz\ReservaBundle\Entity\Tareas $tareas)
    {
        $this->tareas->removeElement($tareas);
    }

    /**
     * Get tareas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTareas()
    {
        return $this->tareas;
    }
}
