<?php

namespace Hertz\ReservaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\DiExtraBundle\Annotation as DI;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Doctrine\ORM\EntityManager;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * Tareas
 *
 * @ORM\Table(name="tareas")
 * @ORM\Entity(repositoryClass="TareasRepository")
 */
class Tareas
{
    const ORM_ENTITY = "HertzReservaBundle:Tareas";

    /**
     *
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Assert\Type(type="integer", message="Debe ser entero mayor a 0") 
     */
    private $id;


    /**
     *
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=255, nullable=false)
     * @Assert\NotBlank(message="El campo es obligatorio.")
     * @Assert\Length(
     *      min = "2",
     *      max = "255",
     *      minMessage = "Debe tener al menos {{ limit }} caracteres de largo",
     *      maxMessage = "Debe tener un máximo de {{ limit }} caracteres"
     * )
     */
    private $descripcion;

    
    /**
	 *
	 * @var boolean
	 *
	 * @ORM\Column(name="debaja", type="boolean")
	 */
	private $debaja;
        
        /**
	 * @ORM\ManyToMany(targetEntity="Hertz\ReservaBundle\Entity\Reparacion", mappedBy="tareas", cascade={"persist"})
	 */
	private $idReparacion;
    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Tareas
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set debaja
     *
     * @param boolean $debaja
     * @return Tareas
     */
    public function setDebaja($debaja)
    {
        $this->debaja = $debaja;

        return $this;
    }

    /**
     * Get debaja
     *
     * @return boolean 
     */
    public function getDebaja()
    {
        return $this->debaja;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->idReparacion = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add idReparacion
     *
     * @param \Hertz\ReservaBundle\Entity\Reparacion $idReparacion
     * @return Tareas
     */
    public function addIdReparacion(\Hertz\ReservaBundle\Entity\Reparacion $idReparacion)
    {
        $this->idReparacion[] = $idReparacion;

        return $this;
    }

    /**
     * Remove idReparacion
     *
     * @param \Hertz\ReservaBundle\Entity\Reparacion $idReparacion
     */
    public function removeIdReparacion(\Hertz\ReservaBundle\Entity\Reparacion $idReparacion)
    {
        $this->idReparacion->removeElement($idReparacion);
    }

    /**
     * Get idReparacion
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getIdReparacion()
    {
        return $this->idReparacion;
    }
}
