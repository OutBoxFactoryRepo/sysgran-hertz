<?php

namespace Hertz\ReservaBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\DiExtraBundle\Annotation as DI;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Doctrine\ORM\EntityManager;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Franquiciados
 *
 * @ORM\Table(name="franquiciados")
 * @ORM\Entity(repositoryClass="FranquiciadosRepository")
 */
class Franquiciados
{
    const ORM_ENTITY = "HertzReservaBundle:Franquiciados";

    /**
     *
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Assert\Type(type="integer", message="Debe ser entero mayor a 0") 
     */
    private $id;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255, nullable=false)
     * @Assert\Length(
     *      max = "255",
     *      maxMessage = "Debe tener un máximo de {{ limit }} caracteres"
     * )
     */
    private $nombre;

    /**
     *
     * @var boolean
     *
     * @ORM\Column(name="debaja", type="boolean", nullable=false)
     * @Assert\Choice(choices = {true,false}, message = "Elección incorrecta (Y , S , N).")
     */
    private $debaja;

    /**
     * @ORM\OneToMany(targetEntity="Hertz\ReservaBundle\Entity\Agencia", mappedBy="franquiciados")
     */
    private $agencia;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Franquiciados
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set debaja
     *
     * @param boolean $debaja
     * @return Franquiciados
     */
    public function setDebaja($debaja)
    {
        $this->debaja = $debaja;

        return $this;
    }

    /**
     * Get debaja
     *
     * @return boolean 
     */
    public function getDebaja()
    {
        return $this->debaja;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->agencia = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add agencia
     *
     * @param \Hertz\ReservaBundle\Entity\Agencia $agencia
     * @return Franquiciados
     */
    public function addAgencium(\Hertz\ReservaBundle\Entity\Agencia $agencia)
    {
        $this->agencia[] = $agencia;

        return $this;
    }

    /**
     * Remove agencia
     *
     * @param \Hertz\ReservaBundle\Entity\Agencia $agencia
     */
    public function removeAgencium(\Hertz\ReservaBundle\Entity\Agencia $agencia)
    {
        $this->agencia->removeElement($agencia);
    }

    /**
     * Get agencia
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAgencia()
    {
        return $this->agencia;
    }
}
