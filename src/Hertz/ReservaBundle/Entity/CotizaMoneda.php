<?php
namespace Hertz\ReservaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\DiExtraBundle\Annotation as DI;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Doctrine\ORM\EntityManager;




/**
 * CotizaMoneda
 *
 * @ORM\Table(name="cotizamoneda")
 * @ORM\Entity(repositoryClass="CotizaMonedaRepository")
 */
class CotizaMoneda{
	
	const ORM_ENTITY = "HertzReservaBundle:CotizaMoneda";
	
	public function __construct()
	{

		$this->preCreate();
	}
	
	/**
	 *
	 * @var integer
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @Assert\Type(type="integer", message="Debe ser entero mayor a 0|id") 
	 */
	private $id;

	/**
	 *
	 * @var datetime
	 * @Gedmo\Timestampable(on="create")
	 * @ORM\Column(name="fechaAlta", type="datetime", nullable=true)
	 */
	private $fechaAlta;
	
	/**
	 *
	 *
	 *@ORM\ManyToOne(targetEntity="Hertz\ReservaBundle\Entity\TipoMoneda",  cascade={"remove"})
	 *@ORM\JoinColumn(name="idmoneda", referencedColumnName="id", nullable=false )
	 *
	 */
	private $idmoneda;
	
	/**
	 *
	 * @var float
	 *
	 * @ORM\Column(name="cotizacion", type="float", scale = 2,  nullable=false)
	 * @Assert\NotNull(message="El campo Cotización de compra Obligatorio.|cotizacion")
	 */
	private $cotizacion;
	
	/**
	 *
	 * @var float
	 *
	 * @ORM\Column(name="cotizacionVenta", type="float", scale = 2,  nullable=false)
	 * @Assert\NotNull(message="El campo Cotización de Venta Obligatorio.|cotizacionVenta")
	 */
	private $cotizacionVenta;

	/**
	 * Now we tell doctrine that before we persist or update we call the updatedTimestamps() function.
	 *
	 */
	private function preCreate()
	{
		$this->setFechaAlta(new \DateTime(date('Y-m-d H:i:s')));
	
		if($this->getFechaAlta() == null)
		{
			$this->setFechaAlta(new \DateTime(date('Y-m-d H:i:s')));
		}
	}
	



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fechaAlta
     *
     * @param \DateTime $fechaAlta
     * @return CotizaMoneda
     */
    public function setFechaAlta($fechaAlta)
    {
        $this->fechaAlta = $fechaAlta;

        return $this;
    }

    /**
     * Get fechaAlta
     *
     * @return \DateTime 
     */
    public function getFechaAlta()
    {
        return $this->fechaAlta;
    }

    /**
     * Set cotizacion
     *
     * @param float $cotizacion
     * @return CotizaMoneda
     */
    public function setCotizacion($cotizacion)
    {
        $this->cotizacion = $cotizacion;

        return $this;
    }

    /**
     * Get cotizacion
     *
     * @return float 
     */
    public function getCotizacion()
    {
        return $this->cotizacion;
    }

    /**
     * Set idmoneda
     *
     * @param \Hertz\ReservaBundle\Entity\TipoMoneda $idmoneda
     * @return CotizaMoneda
     */
    public function setIdmoneda(\Hertz\ReservaBundle\Entity\TipoMoneda $idmoneda)
    {
        $this->idmoneda = $idmoneda;

        return $this;
    }

    /**
     * Get idmoneda
     *
     * @return \Hertz\ReservaBundle\Entity\TipoMoneda 
     */
    public function getIdmoneda()
    {
        return $this->idmoneda;
    }

    /**
     * Set cotizacionVenta
     *
     * @param float $cotizacionVenta
     * @return CotizaMoneda
     */
    public function setCotizacionVenta($cotizacionVenta)
    {
        $this->cotizacionVenta = $cotizacionVenta;

        return $this;
    }

    /**
     * Get cotizacionVenta
     *
     * @return float 
     */
    public function getCotizacionVenta()
    {
        return $this->cotizacionVenta;
    }
}
