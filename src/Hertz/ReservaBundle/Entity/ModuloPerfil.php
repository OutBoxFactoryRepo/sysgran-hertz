<?php
namespace Hertz\ReservaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\DiExtraBundle\Annotation as DI;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Doctrine\ORM\EntityManager;

/**
 * Modulo
 *
 * @ORM\Table(name="moduloPerfil")
 * @UniqueEntity(fields={"modulo", "perfil"}, errorPath="perfil", message="Este valor ya esta registrado")
 * @ORM\Entity(repositoryClass="ModuloPerfilRepository")
 */
class ModuloPerfil {
	
	const ORM_ENTITY = "HertzReservaBundle:ModuloPerfil";

	/**
	 *
	 * @var integer
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @Assert\Type(type="integer", message="Debe ser entero mayor a 0|id") 
	 */
	private $id;

    /**
     *@ORM\ManyToOne(targetEntity="Hertz\ReservaBundle\Entity\Modulo", inversedBy="moduloPerfil")
     *@ORM\JoinColumn(name="modulo_id", referencedColumnName="id", nullable=false )
     *@Assert\NotBlank(message="El campo modulo es obligatorio.")
     */
    private $modulo;

    /**
     *@ORM\ManyToOne(targetEntity="Hertz\UserBundle\Entity\Role", inversedBy="moduloPerfil")
     *@ORM\JoinColumn(name="perfil_id", referencedColumnName="id", nullable=false )
     *@Assert\NotBlank(message="El campo rol es obligatorio.")
     */
    private $perfil;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set modulo
     *
     * @param \Hertz\ReservaBundle\Entity\Modulo $modulo
     * @return ModuloPerfil
     */
    public function setModulo(\Hertz\ReservaBundle\Entity\Modulo $modulo)
    {
        $this->modulo = $modulo;

        return $this;
    }

    /**
     * Get modulo
     *
     * @return \Hertz\ReservaBundle\Entity\Modulo 
     */
    public function getModulo()
    {
        return $this->modulo;
    }

    /**
     * Set perfil
     *
     * @param \Hertz\UserBundle\Entity\Role $perfil
     * @return ModuloPerfil
     */
    public function setPerfil(\Hertz\UserBundle\Entity\Role $perfil)
    {

        $this->perfil = $perfil;

        return $this;
    }

    /**
     * Get perfil
     *
     * @return \Hertz\UserBundle\Entity\Role 
     */
    public function getPerfil()
    {
        return $this->perfil;
    }
}
