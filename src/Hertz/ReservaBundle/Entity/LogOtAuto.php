<?php
namespace Hertz\ReservaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\DiExtraBundle\Annotation as DI;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Doctrine\ORM\EntityManager;

/**
 * LogOtAuto
 *
 * @ORM\Table(name="logOtAuto")
 * @ORM\Entity(repositoryClass="LogOtAutoRepository")
 */
class LogOtAuto{
	
	const ORM_ENTITY = "HertzReservaBundle:LogOtAuto";

	/**
	 *
	 * @var integer
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @Assert\Type(type="integer", message="Debe ser entero mayor a 0") 
	 */
	private $id;
	

	

	/**
	 * 
	 *
	 *@ORM\ManyToOne(targetEntity="Hertz\ReservaBundle\Entity\Vehiculo",  cascade={"remove"})
         *@ORM\JoinColumn(name="vehiculo", referencedColumnName="id", nullable=false )
	 * 
	 */
	private $vehiculo;
		
	/**
	 * 
	 *
	 *@ORM\ManyToOne(targetEntity="Hertz\ReservaBundle\Entity\OrdenTrabajoAuto",  cascade={"remove"})
         *@ORM\JoinColumn(name="ordenAuto", referencedColumnName="id", nullable=false )
	 * 
	 */
	private $otAuto;
        
        
	/**
	 *
	 * @var integer
	 *
	 * @ORM\Column(name="kilometros", type="integer", nullable=false)
	 * @Assert\NotBlank(message="El campo Kilometros es obligatorio.")
	 * @Assert\Type(type="numeric", message="El valor {{ value }} no es válido debe ser {{ type }}.")
	 */
	private $kilometros;
	
	
	/**
	 *
	 * @var datetime
	 *
	 * @ORM\Column(name="fechaCarga", type="datetime", nullable=false)
	 * @Assert\NotBlank(message="El campo fecha proceso es obligatorio.")
	 */
	private $fechaProceso;
	



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set kilometros
     *
     * @param integer $kilometros
     * @return LogOtAuto
     */
    public function setKilometros($kilometros)
    {
        $this->kilometros = $kilometros;

        return $this;
    }

    /**
     * Get kilometros
     *
     * @return integer 
     */
    public function getKilometros()
    {
        return $this->kilometros;
    }

    /**
     * Set fechaProceso
     *
     * @param \DateTime $fechaProceso
     * @return LogOtAuto
     */
    public function setFechaProceso($fechaProceso)
    {
        $this->fechaProceso = $fechaProceso;

        return $this;
    }

    /**
     * Get fechaProceso
     *
     * @return \DateTime 
     */
    public function getFechaProceso()
    {
        return $this->fechaProceso;
    }

    /**
     * Set vehiculo
     *
     * @param \Hertz\ReservaBundle\Entity\Vehiculo $vehiculo
     * @return LogOtAuto
     */
    public function setVehiculo(\Hertz\ReservaBundle\Entity\Vehiculo $vehiculo)
    {
        $this->vehiculo = $vehiculo;

        return $this;
    }

    /**
     * Get vehiculo
     *
     * @return \Hertz\ReservaBundle\Entity\Vehiculo 
     */
    public function getVehiculo()
    {
        return $this->vehiculo;
    }

    /**
     * Set otAuto
     *
     * @param \Hertz\ReservaBundle\Entity\OrdenTrabajoAuto $otAuto
     * @return LogOtAuto
     */
    public function setOtAuto(\Hertz\ReservaBundle\Entity\OrdenTrabajoAuto $otAuto)
    {
        $this->otAuto = $otAuto;

        return $this;
    }

    /**
     * Get otAuto
     *
     * @return \Hertz\ReservaBundle\Entity\OrdenTrabajoAuto 
     */
    public function getOtAuto()
    {
        return $this->otAuto;
    }
}
