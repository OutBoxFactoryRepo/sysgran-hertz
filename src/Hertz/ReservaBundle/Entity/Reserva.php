<?php
namespace Hertz\ReservaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\DiExtraBundle\Annotation as DI;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Doctrine\ORM\EntityManager;

/**
 * Reserva
 *
 * @ORM\Table(name="reservas")
 * @ORM\Entity(repositoryClass="ReservaRepository")
 */
class Reserva{
	
	const ORM_ENTITY = "HertzReservaBundle:Reserva";

	/**
	 *
	 * @var integer
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @Assert\Type(type="integer", message="Debe ser entero mayor a 0|id") 
	 */
	private $id;

	/**
	 *
	 * @var string
	 *
	 * @ORM\Column(name="descripcion", type="string", length=501, nullable=true)
	 */
	private $descripcion;

        /**
	 *
	 *
	 *@ORM\ManyToOne(targetEntity="Hertz\ReservaBundle\Entity\Cliente",  cascade={"remove"})
	 *@ORM\JoinColumn(name="cliente", referencedColumnName="id", nullable=false )
	 *@Assert\NotBlank(message="El campo cliente es obligatorio.")
	 *@Assert\Type(type="object", message="El valor {{ value }} no es válido debe ser {{ type }}.")
	 *
	 */
        private $cliente;

        /**
	 *
	 *
	 *@ORM\ManyToOne(targetEntity="Hertz\ReservaBundle\Entity\MetodoPago",  cascade={"remove"})
	 *@ORM\JoinColumn(name="metodoPago", referencedColumnName="id", nullable=true )
	 *@Assert\Type(type="object", message="El valor {{ value }} no es válido debe ser {{ type }}.")
	 *
	 */
        private $metodoPago;
        
         /**
	 *
	 *
	 *@ORM\ManyToOne(targetEntity="Hertz\ReservaBundle\Entity\Vehiculo",  cascade={"remove"})
	 *@ORM\JoinColumn(name="vehiculo", referencedColumnName="id", nullable=true )
	 *@Assert\Type(type="object", message="El valor {{ value }} no es válido debe ser {{ type }}.")
	 *
	 */
        private $vehiculo;
        
        
        /**
	*
	* @var string
	*
	* @ORM\Column(name="usuario", type="string", length=255, nullable=false)
	* @Assert\NotBlank(message="El campo usuario es obligatorio.")
	* @Assert\Length(
        *      min = "2",
        *      max = "255",
        *      minMessage = "Debe tener al menos {{ limit }} caracteres de largo",
        *      maxMessage = "Debe tener un máximo de {{ limit }} caracteres"
        * )
	 */
	private $usuario;

	
	/**
	 *
	 * @var boolean
	 *
	 * @ORM\Column(name="debaja", type="boolean", nullable=false)
	 * @Assert\Choice(choices = {true,false}, message = "Elección incorrecta (Y , S , N).")
	 */
	private $debaja;
        
        
        /**
	 * @ORM\ManyToMany(targetEntity="Hertz\ReservaBundle\Entity\Cliente", inversedBy="idReserva", cascade={"persist"})
	 * @ORM\JoinTable(name="reserva_clientes_adicionales")
	 **/
	private $clientesAdicionales;

       
        /**
	 *
	 *
	 *@ORM\ManyToOne(targetEntity="Hertz\ReservaBundle\Entity\Contrato",  cascade={"remove"})
	 *@ORM\JoinColumn(name="contrato", referencedColumnName="id", nullable=true )
	 *@Assert\Type(type="object", message="El valor {{ value }} no es válido debe ser {{ type }}.")
	 *
	 */
        private $contrato;
        
        
        /**
	 *
	 * @var datetime
	 *
	 * @ORM\Column(name="fechaHoraDevolucion", type="datetime", nullable=true)
	 * @Assert\DateTime(message="Debe ingresar una fecha válida.")
	 */
	private $fechaHoraDevolucion;

	/**
	 *
	 * @var datetime
	 *
	 * @ORM\Column(name="fechaHoraSalida", type="datetime", nullable=true)
	 * @Assert\DateTime(message="El campo Fecha de Devolución es Obligatorio.")
	 * @Assert\NotBlank(message="El campo es obligatorio.")
	 */
	private $fechaHoraSalida;
	
	/**
	 *
	 * @var datetime
	 *
	 * @ORM\Column(name="fechaHoraRetorno", type="datetime", nullable=true)
	 * @Assert\DateTime(message="El campo Fecha de Retorno es Obligatorio.")
	 * @Assert\NotBlank(message="El campo es obligatorio.")
	 */
	private $fechaHoraRetorno;
	
        /**
        *
        * @var string
        *
        * @ORM\Column(name="horaSalida", type="string", length=5, nullable=true, options={"default":""})
        * @Assert\Length(
        *      min = "5",
        *      max = "5",
        *      minMessage = "Debe tener al menos {{ limit }} caracteres de largo",
        *      maxMessage = "Debe tener un máximo de {{ limit }} caracteres"
        * )
        */
       private $horaSalida;

       /**
        *
        * @var string
        *
        * @ORM\Column(name="horaDevolucion", type="string", length=5, nullable=true, options={"default":""})
        * @Assert\Length(
        *      min = "5",
        *      max = "5",
        *      minMessage = "Debe tener al menos {{ limit }} caracteres de largo",
        *      maxMessage = "Debe tener un máximo de {{ limit }} caracteres"
        * )
        */
       private $horaDevolucion;
       
       
       
       /**
	 *
	 *
	 *@ORM\ManyToOne(targetEntity="Hertz\ReservaBundle\Entity\OrigenReserva",  cascade={"remove"})
	 *@ORM\JoinColumn(name="origenPadre", referencedColumnName="id", nullable=true )
	 *@Assert\Type(type="object", message="El valor {{ value }} no es válido debe ser {{ type }}.")
	 *
	 */
        private $origenPadre;
        
        /**
	 *
	 *
	 *@ORM\ManyToOne(targetEntity="Hertz\ReservaBundle\Entity\OrigenReserva",  cascade={"remove"})
	 *@ORM\JoinColumn(name="origenHijo", referencedColumnName="id", nullable=true )
	 *@Assert\Type(type="object", message="El valor {{ value }} no es válido debe ser {{ type }}.")
	 *
	 */
        private $origenHijo;
        
        
        /**
        *
        * @var string
        *
        * @ORM\Column(name="nombreVia", type="string", length=50, nullable=true, options={"default":""})
        * @Assert\Length(
        *      min = "2",
        *      max = "50",
        *      minMessage = "Debe tener al menos {{ limit }} caracteres de largo",
        *      maxMessage = "Debe tener un máximo de {{ limit }} caracteres"
        * )
        */
       private $nombreVia;
       
       
       /**
        *
        *
        *@ORM\ManyToOne(targetEntity="Hertz\ReservaBundle\Entity\Agencia",  cascade={"remove"})
        *@ORM\JoinColumn(name="agenciaOrigen", referencedColumnName="id", nullable=false )
        *@Assert\NotBlank(message="El campo Agencia es obligatorio.")
        *@Assert\Type(type="object", message="El valor {{ value }} no es válido debe ser {{ type }}.")
        *
        */
       private $agencia;
       
       
       /**
        *
        * @var string
        *
        * @ORM\Column(name="nroVuelo", type="string", length=100, nullable=true, options={"default":""})
        * @Assert\Length(
        *      max = "100",
        *      maxMessage = "Debe tener un máximo de {{ limit }} caracteres"
        * )
        */
       private $nroVuelo;

    /**
         *
         * @var string
         *
         * @ORM\Column(name="nroTelefono", type="string", length=100, nullable=true, options={"default":""})
         * @Assert\Length(
         *      max = "100",
         *      maxMessage = "Debe tener un máximo de {{ limit }} caracteres"
         * )
         */
        private $nroTelefono;

    /**
     *
     *
     *@ORM\ManyToOne(targetEntity="Hertz\UserBundle\Entity\User", inversedBy="reserva")
     *@ORM\JoinColumn(name="idUsuario", referencedColumnName="id", nullable=true )
     *@Assert\Type(type="object", message="El valor {{ value }} no es válido debe ser {{ type }}.")
     *
     */
    private $idUsuario;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Reserva
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set usuario
     *
     * @param string $usuario
     * @return Reserva
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return string 
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set debaja
     *
     * @param boolean $debaja
     * @return Reserva
     */
    public function setDebaja($debaja)
    {
        $this->debaja = $debaja;

        return $this;
    }

    /**
     * Get debaja
     *
     * @return boolean 
     */
    public function getDebaja()
    {
        return $this->debaja;
    }

    /**
     * Set cliente
     *
     * @param \Hertz\ReservaBundle\Entity\Cliente $cliente
     * @return Reserva
     */
    public function setCliente(\Hertz\ReservaBundle\Entity\Cliente $cliente)
    {
        $this->cliente = $cliente;

        return $this;
    }

    /**
     * Get cliente
     *
     * @return \Hertz\ReservaBundle\Entity\Cliente 
     */
    public function getCliente()
    {
        return $this->cliente;
    }

    /**
     * Set metodoPago
     *
     * @param \Hertz\ReservaBundle\Entity\MetodoPago $metodoPago
     * @return Reserva
     */
    public function setMetodoPago(\Hertz\ReservaBundle\Entity\MetodoPago $metodoPago = null)
    {
        $this->metodoPago = $metodoPago;

        return $this;
    }

    /**
     * Get metodoPago
     *
     * @return \Hertz\ReservaBundle\Entity\MetodoPago 
     */
    public function getMetodoPago()
    {
        return $this->metodoPago;
    }

    /**
     * Set vehiculo
     *
     * @param \Hertz\ReservaBundle\Entity\Vehiculo $vehiculo
     * @return Reserva
     */
    public function setVehiculo(\Hertz\ReservaBundle\Entity\Vehiculo $vehiculo = null)
    {
        $this->vehiculo = $vehiculo;

        return $this;
    }

    /**
     * Get vehiculo
     *
     * @return \Hertz\ReservaBundle\Entity\Vehiculo 
     */
    public function getVehiculo()
    {
        return $this->vehiculo;
    }

    /**
     * Set clientesAdicionales
     *
     * @param \Hertz\ReservaBundle\Entity\Cliente $clientesAdicionales
     * @return Reserva
     */
    public function setClientesAdicionales(\Hertz\ReservaBundle\Entity\Cliente $clientesAdicionales = null)
    {
        $this->clientesAdicionales = $clientesAdicionales;

        return $this;
    }

    /**
     * Get clientesAdicionales
     *
     * @return \Hertz\ReservaBundle\Entity\Cliente 
     */
    public function getClientesAdicionales()
    {
        return $this->clientesAdicionales;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->clientesAdicionales = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add clientesAdicionales
     *
     * @param \Hertz\ReservaBundle\Entity\Cliente $clientesAdicionales
     * @return Reserva
     */
    public function addClientesAdicionale(\Hertz\ReservaBundle\Entity\Cliente $clientesAdicionales)
    {
        $this->clientesAdicionales[] = $clientesAdicionales;

        return $this;
    }

    /**
     * Remove clientesAdicionales
     *
     * @param \Hertz\ReservaBundle\Entity\Cliente $clientesAdicionales
     */
    public function removeClientesAdicionale(\Hertz\ReservaBundle\Entity\Cliente $clientesAdicionales)
    {
        $this->clientesAdicionales->removeElement($clientesAdicionales);
    }

    /**
     * Set contrato
     *
     * @param \Hertz\ReservaBundle\Entity\Contrato $contrato
     * @return Reserva
     */
    public function setContrato(\Hertz\ReservaBundle\Entity\Contrato $contrato = null)
    {
        $this->contrato = $contrato;

        return $this;
    }

    /**
     * Get contrato
     *
     * @return \Hertz\ReservaBundle\Entity\Contrato 
     */
    public function getContrato()
    {
        return $this->contrato;
    }

    /**
     * Set fechaHoraDevolucion
     *
     * @param \DateTime $fechaHoraDevolucion
     * @return Reserva
     */
    public function setFechaHoraDevolucion($fechaHoraDevolucion)
    {
        $this->fechaHoraDevolucion = $fechaHoraDevolucion;

        return $this;
    }

    /**
     * Get fechaHoraDevolucion
     *
     * @return \DateTime 
     */
    public function getFechaHoraDevolucion()
    {
        return $this->fechaHoraDevolucion;
    }

    /**
     * Set fechaHoraSalida
     *
     * @param \DateTime $fechaHoraSalida
     * @return Reserva
     */
    public function setFechaHoraSalida($fechaHoraSalida)
    {
        $this->fechaHoraSalida = $fechaHoraSalida;

        return $this;
    }

    /**
     * Get fechaHoraSalida
     *
     * @return \DateTime 
     */
    public function getFechaHoraSalida()
    {
        return $this->fechaHoraSalida;
    }

    /**
     * Set fechaHoraRetorno
     *
     * @param \DateTime $fechaHoraRetorno
     * @return Reserva
     */
    public function setFechaHoraRetorno($fechaHoraRetorno)
    {
        $this->fechaHoraRetorno = $fechaHoraRetorno;

        return $this;
    }

    /**
     * Get fechaHoraRetorno
     *
     * @return \DateTime 
     */
    public function getFechaHoraRetorno()
    {
        return $this->fechaHoraRetorno;
    }

    /**
     * Set horaSalida
     *
     * @param string $horaSalida
     * @return Reserva
     */
    public function setHoraSalida($horaSalida)
    {
        $this->horaSalida = $horaSalida;

        return $this;
    }

    /**
     * Get horaSalida
     *
     * @return string 
     */
    public function getHoraSalida()
    {
        return $this->horaSalida;
    }

    /**
     * Set horaDevolucion
     *
     * @param string $horaDevolucion
     * @return Reserva
     */
    public function setHoraDevolucion($horaDevolucion)
    {
        $this->horaDevolucion = $horaDevolucion;

        return $this;
    }

    /**
     * Get horaDevolucion
     *
     * @return string 
     */
    public function getHoraDevolucion()
    {
        return $this->horaDevolucion;
    }

    /**
     * Set origenPadre
     *
     * @param \Hertz\ReservaBundle\Entity\OrigenReserva $origenPadre
     * @return Reserva
     */
    public function setOrigenPadre(\Hertz\ReservaBundle\Entity\OrigenReserva $origenPadre = null)
    {
        $this->origenPadre = $origenPadre;

        return $this;
    }

    /**
     * Get origenPadre
     *
     * @return \Hertz\ReservaBundle\Entity\OrigenReserva 
     */
    public function getOrigenPadre()
    {
        return $this->origenPadre;
    }

    /**
     * Set origenHijo
     *
     * @param \Hertz\ReservaBundle\Entity\OrigenReserva $origenHijo
     * @return Reserva
     */
    public function setOrigenHijo(\Hertz\ReservaBundle\Entity\OrigenReserva $origenHijo = null)
    {
        $this->origenHijo = $origenHijo;

        return $this;
    }

    /**
     * Get origenHijo
     *
     * @return \Hertz\ReservaBundle\Entity\OrigenReserva 
     */
    public function getOrigenHijo()
    {
        return $this->origenHijo;
    }

    /**
     * Set nombreVia
     *
     * @param string $nombreVia
     * @return Reserva
     */
    public function setNombreVia($nombreVia)
    {
        $this->nombreVia = $nombreVia;

        return $this;
    }

    /**
     * Get nombreVia
     *
     * @return string 
     */
    public function getNombreVia()
    {
        return $this->nombreVia;
    }

    /**
     * Set agencia
     *
     * @param \Hertz\ReservaBundle\Entity\Agencia $agencia
     * @return Reserva
     */
    public function setAgencia(\Hertz\ReservaBundle\Entity\Agencia $agencia)
    {
        $this->agencia = $agencia;

        return $this;
    }

    /**
     * Get agencia
     *
     * @return \Hertz\ReservaBundle\Entity\Agencia 
     */
    public function getAgencia()
    {
        return $this->agencia;
    }

    /**
     * Set nroVuelo
     *
     * @param string $nroVuelo
     * @return Reserva
     */
    public function setNroVuelo($nroVuelo)
    {
        $this->nroVuelo = $nroVuelo;

        return $this;
    }

    /**
     * Get nroVuelo
     *
     * @return string 
     */
    public function getNroVuelo()
    {
        return $this->nroVuelo;
    }

    /**
     * Set nroTelefono
     *
     * @param string $nroTelefono
     * @return Reserva
     */
    public function setNroTelefono($nroTelefono)
    {
        $this->nroTelefono = $nroTelefono;

        return $this;
    }

    /**
     * Get nroTelefono
     *
     * @return string
     */
    public function getNroTelefono()
    {
        return $this->nroTelefono;
    }

    /**
     * Set idUsuario
     *
     * @param \Hertz\UserBundle\Entity\User $idUsuario
     * @return Reserva
     */
    public function setIdUsuario(\Hertz\UserBundle\Entity\User $idUsuario)
    {
        $this->idUsuario = $idUsuario;

        return $this;
    }

    /**
     * Get idUsuario
     *
     * @return \Hertz\UserBundle\Entity\User 
     */
    public function getIdUsuario()
    {
        return $this->idUsuario;
    }
}
