<?php
namespace Hertz\ReservaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\DiExtraBundle\Annotation as DI;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Doctrine\ORM\EntityManager;


/**
 * VehiculoEstadosHistorico
 *
 * @ORM\Table(name="vehiculoEstadosHistorico")
 * @ORM\Entity(repositoryClass="VehiculoEstadosHistoricoRepository")
 */
class VehiculoEstadosHistorico{
	
	const ORM_ENTITY = "HertzReservaBundle:VehiculoEstadosHistorico";

	/**
	*
	* @var integer
        *
        * @ORM\Id
        * @ORM\Column(type="integer")
        * @ORM\GeneratedValue(strategy="AUTO")
	*/
	private $id;
	
	
        /**
	 * 
	 *
	 *@ORM\ManyToOne(targetEntity="Hertz\ReservaBundle\Entity\Vehiculo",  cascade={"remove"})
         *@ORM\JoinColumn(name="vehiculo", referencedColumnName="id", nullable=false )
	 * 
	 */
	private $vehiculo;
        
        /**
	 * 
	 *
	 *@ORM\ManyToOne(targetEntity="Hertz\ReservaBundle\Entity\EstadoVehiculo",  cascade={"remove"})
         *@ORM\JoinColumn(name="estadoVehiculo", referencedColumnName="id", nullable=false )
	 * 
	 */
	private $estadoVehiculo;
	
	

		
	/**
	 *
	 * @var datetime
	 *
	 * @ORM\Column(name="fechaCambio", type="datetime", nullable=false)
	 * @Assert\NotBlank(message="El campo fecha es obligatorio.")
	 */
	private $fechaCambio;
	
	/**
	 * 
	 *
	 *@ORM\ManyToOne(targetEntity="Hertz\UserBundle\Entity\User",  cascade={"remove"})
         *@ORM\JoinColumn(name="usuario", referencedColumnName="id", nullable=false )
	 * 
	 */	
	private $usuario;	
	
	

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fechaCambio
     *
     * @param \DateTime $fechaCambio
     * @return VehiculoEstadosHistorico
     */
    public function setFechaCambio($fechaCambio)
    {
        $this->fechaCambio = $fechaCambio;

        return $this;
    }

    /**
     * Get fechaCambio
     *
     * @return \DateTime 
     */
    public function getFechaCambio()
    {
        return $this->fechaCambio;
    }

    /**
     * Set estadoVehiculo
     *
     * @param \Hertz\ReservaBundle\Entity\EstadoVehiculo $estadoVehiculo
     * @return VehiculoEstadosHistorico
     */
    public function setEstadoVehiculo(\Hertz\ReservaBundle\Entity\EstadoVehiculo $estadoVehiculo)
    {
        $this->estadoVehiculo = $estadoVehiculo;

        return $this;
    }

    /**
     * Get estadoVehiculo
     *
     * @return \Hertz\ReservaBundle\Entity\EstadoVehiculo 
     */
    public function getEstadoVehiculo()
    {
        return $this->estadoVehiculo;
    }

    /**
     * Set usuario
     *
     * @param \Hertz\UserBundle\Entity\User $usuario
     * @return VehiculoEstadosHistorico
     */
    public function setUsuario(\Hertz\UserBundle\Entity\User $usuario)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return \Hertz\UserBundle\Entity\User 
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set vehiculo
     *
     * @param \Hertz\ReservaBundle\Entity\Vehiculo $vehiculo
     * @return VehiculoEstadosHistorico
     */
    public function setVehiculo(\Hertz\ReservaBundle\Entity\Vehiculo $vehiculo)
    {
        $this->vehiculo = $vehiculo;

        return $this;
    }

    /**
     * Get vehiculo
     *
     * @return \Hertz\ReservaBundle\Entity\Vehiculo 
     */
    public function getVehiculo()
    {
        return $this->vehiculo;
    }
}
