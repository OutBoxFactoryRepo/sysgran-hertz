<?php
namespace Hertz\ReservaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\DiExtraBundle\Annotation as DI;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Doctrine\ORM\EntityManager;

/**
 * ClienteOtrosDatos
 *
 * @ORM\Table(name="clientesOtrosDatos")
 * @ORM\Entity(repositoryClass="ClienteOtrosDatosRepository")
 */
class ClienteOtrosDatos{
	
	const ORM_ENTITY = "HertzReservaBundle:ClienteOtrosDatos";

	/**
	 *
	 * @var integer
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @Assert\Type(type="integer", message="Debe ser entero mayor a 0|id") 
	 */
	private $id;

		
	/**
	 *
	 * @var string
	 *
	 * @ORM\Column(name="licencia", type="string", length=255, nullable=true)
	 * @Assert\NotNull(message="El campo Licencia es Obligatorio.|licencia")
	 */
	private $licencia;
	
	/**
	 *
	 * @var datetime
	 *
	 * @ORM\Column(name="licFecVto", type="datetime", nullable=true)
	 * @Assert\NotNull(message="El campo Licencia Fecha de Vencimiento es Obligatorio.|licFecVto")
	 */
	private $licFecVto;

	/**
	 *
	 * @var string
	 *
	 * @ORM\Column(name="nroTarjetaCre", type="string", length=255 ,  nullable=true)
	 * @Assert\NotNull(message="El campo Número de tarjeta de crédito es Obligatorio.|nroTarjetaCre")
	 */
	private $nroTarjetaCre;
	
	/**
	 *
	 * @var string
	 *
	 * @ORM\Column(name="tarjFecVto", type="string" , length=20 ,  nullable=true)
	 * @Assert\NotNull(message="El campo Fecha de Vto. de Tarjeta de Crédito es Obligatorio.|tarjFecVto")
	 */
	private $tarjFecVto;
		
	
	/**
	 *
	 * @var string
	 *
	 * @ORM\Column(name="cAut", type="string", length=255 ,  nullable=true)
	 * @Assert\NotNull(message="El campo C/Aut de licencia del Automovil es Obligatorio.|cAut")
	 */
	private $cAut;
	
	/**
	 *
	 * @var string
	 *
	 * @ORM\Column(name="cs", type="string", length=255 ,  nullable=true)
	 * @Assert\NotNull(message="El campo cs es Obligatorio.|cs")
	 */
	private $cs;
	
	
	/**
	 *
	 * @var string
	 *
	 * @ORM\Column(name="cuit", type="string", length=11, nullable=true)
	 */
	private $cuit;
		
    
	/**
	 *
	 *
	 *@ORM\ManyToOne(targetEntity="Hertz\ReservaBundle\Entity\CondicionIva",  cascade={"remove"})
	 *@ORM\JoinColumn(name="condicionIva", referencedColumnName="id", nullable=false )
	 *
	 */
	private $condicionIva;
	
	/**
	 *
	 *
	 *@ORM\ManyToOne(targetEntity="Hertz\ReservaBundle\Entity\TipoIIBB",  cascade={"remove"})
	 *@ORM\JoinColumn(name="idIIBB", referencedColumnName="id", nullable=false )
	 *
	 */
	private $idIIBB;
	
        
	/**
	 *
	 * @var string
	 *
	 * @ORM\Column(name="tipoTarjeta", type="integer", length=10, nullable=true)
	 */
	private $tipoTarjeta;
        
        
        /**
	 *
	 * @var float
	 *
	 * @ORM\Column(name="monto", type="float", scale = 2,  nullable=true)
	 */
	private $monto;
        
        
	/**
	 *
	 * @var string
	 *
	 * @ORM\Column(name="nroIIBB", type="string", length=20, nullable=true)
	 */
	private $nroIIBB;
	
         /**
	 * @ORM\ManyToMany(targetEntity="Hertz\ReservaBundle\Entity\Cliente", inversedBy="otrosDatos", cascade={"persist"})
	 * @ORM\JoinTable(name="cliente_ClienteOtrosDatos")
	 **/
	private $cliente;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idCliente
     *
     * @param integer $idCliente
     * @return ClienteOtrosDatos
     */
    public function setIdCliente($idCliente)
    {
        $this->idCliente = $idCliente;

        return $this;
    }

    /**
     * Get idCliente
     *
     * @return integer 
     */
    public function getIdCliente()
    {
        return $this->idCliente;
    }

    /**
     * Set licencia
     *
     * @param string $licencia
     * @return ClienteOtrosDatos
     */
    public function setLicencia($licencia)
    {
        $this->licencia = $licencia;

        return $this;
    }

    /**
     * Get licencia
     *
     * @return string 
     */
    public function getLicencia()
    {
        return $this->licencia;
    }

    /**
     * Set licFecVto
     *
     * @param \DateTime $licFecVto
     * @return ClienteOtrosDatos
     */
    public function setLicFecVto($licFecVto)
    {
        $this->licFecVto = $licFecVto;

        return $this;
    }

    /**
     * Get licFecVto
     *
     * @return \DateTime 
     */
    public function getLicFecVto()
    {
        return $this->licFecVto;
    }

    /**
     * Set nroTarjetaCre
     *
     * @param string $nroTarjetaCre
     * @return ClienteOtrosDatos
     */
    public function setNroTarjetaCre($nroTarjetaCre)
    {
        $this->nroTarjetaCre = $nroTarjetaCre;

        return $this;
    }

    /**
     * Get nroTarjetaCre
     *
     * @return string 
     */
    public function getNroTarjetaCre()
    {
        return $this->nroTarjetaCre;
    }

    /**
     * Set tarjFecVto
     *
     * @param \DateTime $tarjFecVto
     * @return ClienteOtrosDatos
     */
    public function setTarjFecVto($tarjFecVto)
    {
        $this->tarjFecVto = $tarjFecVto;

        return $this;
    }

    /**
     * Get tarjFecVto
     *
     * @return \DateTime 
     */
    public function getTarjFecVto()
    {
        return $this->tarjFecVto;
    }

    /**
     * Set cAut
     *
     * @param string $cAut
     * @return ClienteOtrosDatos
     */
    public function setCAut($cAut)
    {
        $this->cAut = $cAut;

        return $this;
    }

    /**
     * Get cAut
     *
     * @return string 
     */
    public function getCAut()
    {
        return $this->cAut;
    }

    /**
     * Set cs
     *
     * @param string $cs
     * @return ClienteOtrosDatos
     */
    public function setCs($cs)
    {
        $this->cs = $cs;

        return $this;
    }

    /**
     * Get cs
     *
     * @return string 
     */
    public function getCs()
    {
        return $this->cs;
    }

    /**
     * Set cuit
     *
     * @param string $cuit
     * @return ClienteOtrosDatos
     */
    public function setCuit($cuit)
    {
        $this->cuit = $cuit;

        return $this;
    }

    /**
     * Get cuit
     *
     * @return string 
     */
    public function getCuit()
    {
        return $this->cuit;
    }

    /**
     * Set nroIIBB
     *
     * @param string $nroIIBB
     * @return ClienteOtrosDatos
     */
    public function setNroIIBB($nroIIBB)
    {
        $this->nroIIBB = $nroIIBB;

        return $this;
    }

    /**
     * Get nroIIBB
     *
     * @return string 
     */
    public function getNroIIBB()
    {
        return $this->nroIIBB;
    }

    /**
     * Set condicionIva
     *
     * @param \Hertz\ReservaBundle\Entity\CondicionIva $condicionIva
     * @return ClienteOtrosDatos
     */
    public function setCondicionIva(\Hertz\ReservaBundle\Entity\CondicionIva $condicionIva)
    {
        $this->condicionIva = $condicionIva;

        return $this;
    }

    /**
     * Get condicionIva
     *
     * @return \Hertz\ReservaBundle\Entity\CondicionIva 
     */
    public function getCondicionIva()
    {
        return $this->condicionIva;
    }

    /**
     * Set idIIBB
     *
     * @param \Hertz\ReservaBundle\Entity\TipoIIBB $idIIBB
     * @return ClienteOtrosDatos
     */
    public function setIdIIBB(\Hertz\ReservaBundle\Entity\TipoIIBB $idIIBB)
    {
        $this->idIIBB = $idIIBB;

        return $this;
    }

    /**
     * Get idIIBB
     *
     * @return \Hertz\ReservaBundle\Entity\TipoIIBB 
     */
    public function getIdIIBB()
    {
        return $this->idIIBB;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->cliente = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add cliente
     *
     * @param \Hertz\ReservaBundle\Entity\Cliente $cliente
     * @return ClienteOtrosDatos
     */
    public function addCliente(\Hertz\ReservaBundle\Entity\Cliente $cliente)
    {
        $this->cliente[] = $cliente;

        return $this;
    }

    /**
     * Remove cliente
     *
     * @param \Hertz\ReservaBundle\Entity\Cliente $cliente
     */
    public function removeCliente(\Hertz\ReservaBundle\Entity\Cliente $cliente)
    {
        $this->cliente->removeElement($cliente);
    }

    /**
     * Get cliente
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCliente()
    {
        return $this->cliente;
    }

    /**
     * Set tipoTarjeta
     *
     * @param integer $tipoTarjeta
     * @return ClienteOtrosDatos
     */
    public function setTipoTarjeta($tipoTarjeta)
    {
        $this->tipoTarjeta = $tipoTarjeta;

        return $this;
    }

    /**
     * Get tipoTarjeta
     *
     * @return integer 
     */
    public function getTipoTarjeta()
    {
        return $this->tipoTarjeta;
    }

    /**
     * Set monto
     *
     * @param float $monto
     * @return ClienteOtrosDatos
     */
    public function setMonto($monto)
    {
        $this->monto = $monto;

        return $this;
    }

    /**
     * Get monto
     *
     * @return float 
     */
    public function getMonto()
    {
        return $this->monto;
    }
}
