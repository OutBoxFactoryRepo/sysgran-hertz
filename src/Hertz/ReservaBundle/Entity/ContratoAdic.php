<?php
namespace Hertz\ReservaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\DiExtraBundle\Annotation as DI;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Doctrine\ORM\EntityManager;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * ContratoAdic
 *
 * @ORM\Table(name="contratosAdic")
 * @ORM\Entity(repositoryClass="ContratoAdicRepository")
 */
class ContratoAdic{
	
	const ORM_ENTITY = "HertzReservaBundle:ContratoAdic";

	/**
	 *
	 * @var integer
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @Assert\Type(type="integer", message="Debe ser entero mayor a 0") 
	 */
	private $id;

        /**
	 *
	 * @var integer
	 *
	 * @ORM\Column(name="cantidad", type="integer", nullable=false)
	 * @Assert\NotBlank(message="El campo cantidad es obligatorio.")
	 */
	private $cantidad;	
	
	/**
	 *
	 * @var float
	 *
	 * @ORM\Column(name="valor", type="float", nullable=false)
	 * @Assert\NotBlank(message="El campo valor es obligatorio.")
	 */
	private $valor;
        
        /**
	 * @ORM\ManyToMany(targetEntity="Hertz\ReservaBundle\Entity\Contrato", mappedBy="adicionales", cascade={"persist"})
	 */
	private $idContrato;
        
        
        /**
	 *
	 *
	 *@ORM\ManyToOne(targetEntity="Hertz\ReservaBundle\Entity\Adicional",  cascade={"remove"})
	 *@ORM\JoinColumn(name="adicional", referencedColumnName="id", nullable=true )
	 *@Assert\NotBlank(message="El campo Adicional es obligatorio.")
	 *@Assert\Type(type="object", message="El valor {{ value }} no es válido debe ser {{ type }}.")
	 *
	 */
	private $adicional;
  
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->idContrato = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cantidad
     *
     * @param integer $cantidad
     * @return ContratoAdic
     */
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    /**
     * Get cantidad
     *
     * @return integer 
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * Set valor
     *
     * @param float $valor
     * @return ContratoAdic
     */
    public function setValor($valor)
    {
        $this->valor = $valor;

        return $this;
    }

    /**
     * Get valor
     *
     * @return float 
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * Add idContrato
     *
     * @param \Hertz\ReservaBundle\Entity\Contrato $idContrato
     * @return ContratoAdic
     */
    public function addIdContrato(\Hertz\ReservaBundle\Entity\Contrato $idContrato)
    {
        $this->idContrato[] = $idContrato;

        return $this;
    }

    /**
     * Remove idContrato
     *
     * @param \Hertz\ReservaBundle\Entity\Contrato $idContrato
     */
    public function removeIdContrato(\Hertz\ReservaBundle\Entity\Contrato $idContrato)
    {
        $this->idContrato->removeElement($idContrato);
    }

    /**
     * Get idContrato
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getIdContrato()
    {
        return $this->idContrato;
    }

    /**
     * Set adicional
     *
     * @param \Hertz\ReservaBundle\Entity\Adicional $adicional
     * @return ContratoAdic
     */
    public function setAdicional(\Hertz\ReservaBundle\Entity\Adicional $adicional = null)
    {
        $this->adicional = $adicional;

        return $this;
    }

    /**
     * Get adicional
     *
     * @return \Hertz\ReservaBundle\Entity\Adicional 
     */
    public function getAdicional()
    {
        return $this->adicional;
    }
}
