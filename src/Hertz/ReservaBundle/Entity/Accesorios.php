<?php

namespace Hertz\ReservaBundle\Entity;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\DiExtraBundle\Annotation as DI;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Doctrine\ORM\EntityManager;
use Doctrine\Common\Collections\ArrayCollection;

use Doctrine\ORM\Mapping as ORM;

/**
 * Accesorios
 * @ORM\Table(name="accesorios")
 * @ORM\Entity(repositoryClass="AccesoriosRepository")
 */
class Accesorios
{
    const ORM_ENTITY = "HertzReservaBundle:Accesorios";
    
    /**
     *
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Assert\Type(type="integer", message="Debe ser entero mayor a 0") 
     */
    private $id;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=255, nullable=true)
     * @Assert\Length(
     *      min = "2",
     *      max = "255",
     *      minMessage = "Debe tener al menos {{ limit }} caracteres de largo",
     *      maxMessage = "Debe tener un máximo de {{ limit }} caracteres"
     * )
     */
    private $descripcion;

    /**
     *
     * @var boolean
     *
     * @ORM\Column(name="debaja", type="boolean", nullable=false)
     * @Assert\Choice(choices = {true,false}, message = "Elección incorrecta (Y , S , N).")
     */
    private $debaja;

    
    /**
    * @ORM\ManyToMany(targetEntity="Hertz\ReservaBundle\Entity\Vehiculo", inversedBy="accesorios", cascade={"persist"})
    * @ORM\JoinTable(name="vehiculo_accesorios")
    **/
   private $vehiculo;
    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Accesorios
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set debaja
     *
     * @param boolean $debaja
     * @return Accesorios
     */
    public function setDebaja($debaja)
    {
        $this->debaja = $debaja;

        return $this;
    }

    /**
     * Get debaja
     *
     * @return boolean 
     */
    public function getDebaja()
    {
        return $this->debaja;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->vehiculo = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add vehiculo
     *
     * @param \Hertz\ReservaBundle\Entity\Vehiculo $vehiculo
     * @return Accesorios
     */
    public function addVehiculo(\Hertz\ReservaBundle\Entity\Vehiculo $vehiculo)
    {
        $this->vehiculo[] = $vehiculo;

        return $this;
    }

    /**
     * Remove vehiculo
     *
     * @param \Hertz\ReservaBundle\Entity\Vehiculo $vehiculo
     */
    public function removeVehiculo(\Hertz\ReservaBundle\Entity\Vehiculo $vehiculo)
    {
        $this->vehiculo->removeElement($vehiculo);
    }

    /**
     * Get vehiculo
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getVehiculo()
    {
        return $this->vehiculo;
    }
}
