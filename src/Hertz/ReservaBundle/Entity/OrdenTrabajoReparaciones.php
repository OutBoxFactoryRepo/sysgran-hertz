<?php
namespace Hertz\ReservaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\DiExtraBundle\Annotation as DI;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Doctrine\ORM\EntityManager;

/**
 * OrdenTrabajoReparaciones
 *
 * @ORM\Table(name="ordenTrabajoReparaciones")
 * @ORM\Entity(repositoryClass="OrdenTrabajoReparacionesRepository")
 */
class OrdenTrabajoReparaciones{
	
	const ORM_ENTITY = "HertzReservaBundle:OrdenTrabajoReparaciones";

	/**
	 *
	 * @var integer
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @Assert\Type(type="integer", message="Debe ser entero mayor a 0") 
	 */
	private $id;
	
	/**
	 *
	 * @var string
	 *
	 * @ORM\Column(name="descripcion", type="string", length=255, nullable=true)
	 */
	private $descripcion;
			
	
	/**
	 *
	 *
	 *@ORM\ManyToOne(targetEntity="Hertz\ReservaBundle\Entity\OrdenTrabajo")
	 *@ORM\JoinColumn(name="ot", referencedColumnName="id", nullable=false )
	 *@Assert\NotBlank(message="El campo Orden Trabajo es obligatorio.")
	 *@Assert\Type(type="object", message="El valor {{ value }} no es válido debe ser {{ type }}.")
	 *
	 */
	private $ot;
	
	
	/**
	 *
	 *
	 *@ORM\ManyToOne(targetEntity="Hertz\ReservaBundle\Entity\Reparacion")
	 *@ORM\JoinColumn(name="reparacion", referencedColumnName="id", nullable=false )
	 *@Assert\NotBlank(message="El campo Reparación es obligatorio.")
	 *@Assert\Type(type="object", message="El valor {{ value }} no es válido debe ser {{ type }}.")
	 *
	 */
	private $reparacion;
	
		
	/**
	 *
	 * @var datetime
	 *
	 * @ORM\Column(name="fechaCarga", type="datetime", nullable=false)
	 * @Assert\NotBlank(message="El campo fecha es obligatorio.")
	 */
	private $fechaCarga;
	
	/**
	 *
	 * @var string
	 *
	 * @ORM\Column(name="usuario", type="string", length=255, nullable=false)
	 * @Assert\NotBlank(message="El campo usuario es obligatorio.")
	 * @Assert\Length(
     *      min = "2",
     *      max = "255",
     *      minMessage = "Debe tener al menos {{ limit }} caracteres de largo",
     *      maxMessage = "Debe tener un máximo de {{ limit }} caracteres"
     * )
	 */
	private $usuario;

	
	/**
	 *
	 * @var boolean
	 *
	 * @ORM\Column(name="debaja", type="boolean", nullable=false)
	 * @Assert\Choice(choices = {true,false}, message = "Elección incorrecta (Y , S , N).")
	 */
	private $debaja;
	

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return OrdenTrabajoReparaciones
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set fechaCarga
     *
     * @param \DateTime $fechaCarga
     * @return OrdenTrabajoReparaciones
     */
    public function setFechaCarga($fechaCarga)
    {
        $this->fechaCarga = $fechaCarga;

        return $this;
    }

    /**
     * Get fechaCarga
     *
     * @return \DateTime 
     */
    public function getFechaCarga()
    {
        return $this->fechaCarga;
    }

    /**
     * Set usuario
     *
     * @param string $usuario
     * @return OrdenTrabajoReparaciones
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return string 
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set debaja
     *
     * @param boolean $debaja
     * @return OrdenTrabajoReparaciones
     */
    public function setDebaja($debaja)
    {
        $this->debaja = $debaja;

        return $this;
    }

    /**
     * Get debaja
     *
     * @return boolean 
     */
    public function getDebaja()
    {
        return $this->debaja;
    }

    /**
     * Set ot
     *
     * @param \Hertz\ReservaBundle\Entity\OrdenTrabajo $ot
     * @return OrdenTrabajoReparaciones
     */
    public function setOt(\Hertz\ReservaBundle\Entity\OrdenTrabajo $ot)
    {
        $this->ot = $ot;

        return $this;
    }

    /**
     * Get ot
     *
     * @return \Hertz\ReservaBundle\Entity\OrdenTrabajo 
     */
    public function getOt()
    {
        return $this->ot;
    }

    /**
     * Set reparacion
     *
     * @param \Hertz\ReservaBundle\Entity\Reparacion $reparacion
     * @return OrdenTrabajoReparaciones
     */
    public function setReparacion(\Hertz\ReservaBundle\Entity\Reparacion $reparacion)
    {
        $this->reparacion = $reparacion;

        return $this;
    }

    /**
     * Get reparacion
     *
     * @return \Hertz\ReservaBundle\Entity\Reparacion 
     */
    public function getReparacion()
    {
        return $this->reparacion;
    }
}
