<?php
namespace Hertz\ReservaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\DiExtraBundle\Annotation as DI;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Doctrine\ORM\EntityManager;

/**
 * OrdenTrabajoReparacionesTareas
 *
 * @ORM\Table(name="ordenTrabajoReparacionesTareas")
 * @ORM\Entity(repositoryClass="OrdenTrabajoReparacionesTareasRepository")
 */
class OrdenTrabajoReparacionesTareas{
	
	const ORM_ENTITY = "HertzReservaBundle:OrdenTrabajoReparacionesTareas";

	/**
	 *
	 * @var integer
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @Assert\Type(type="integer", message="Debe ser entero mayor a 0") 
	 */
	private $id;
	
        /**
	 *
	 *
	 *@ORM\ManyToOne(targetEntity="Hertz\ReservaBundle\Entity\OrdenTrabajo")
	 *@ORM\JoinColumn(name="ot", referencedColumnName="id", nullable=false )
	 *@Assert\NotBlank(message="El campo Orden Trabajo es obligatorio.")
	 *@Assert\Type(type="object", message="El valor {{ value }} no es válido debe ser {{ type }}.")
	 *
	 */
	private $ot;
	

	/**
	 *
	 *
	 *@ORM\ManyToOne(targetEntity="Hertz\ReservaBundle\Entity\OrdenTrabajoReparaciones")
	 *@ORM\JoinColumn(name="idOtReparacion", referencedColumnName="id", nullable=false , onDelete="CASCADE")
	 *
	 */
	private $idOtReparacion;
        
        /**
	 *
	 *
	 *@ORM\ManyToOne(targetEntity="Hertz\ReservaBundle\Entity\Tareas")
	 *@ORM\JoinColumn(name="idTarea", referencedColumnName="id", nullable=false )
	 *
	 */
	private $idTarea;
	


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idOtReparacion
     *
     * @param \Hertz\ReservaBundle\Entity\OrdenTrabajoReparaciones $idOtReparacion
     * @return OrdenTrabajoReparacionesTareas
     */
    public function setIdOtReparacion(\Hertz\ReservaBundle\Entity\OrdenTrabajoReparaciones $idOtReparacion)
    {
        $this->idOtReparacion = $idOtReparacion;

        return $this;
    }

    /**
     * Get idOtReparacion
     *
     * @return \Hertz\ReservaBundle\Entity\OrdenTrabajoReparaciones 
     */
    public function getIdOtReparacion()
    {
        return $this->idOtReparacion;
    }

    /**
     * Set idTarea
     *
     * @param \Hertz\ReservaBundle\Entity\Tareas $idTarea
     * @return OrdenTrabajoReparacionesTareas
     */
    public function setIdTarea(\Hertz\ReservaBundle\Entity\Tareas $idTarea)
    {
        $this->idTarea = $idTarea;

        return $this;
    }

    /**
     * Get idTarea
     *
     * @return \Hertz\ReservaBundle\Entity\Tareas 
     */
    public function getIdTarea()
    {
        return $this->idTarea;
    }

    /**
     * Set ot
     *
     * @param \Hertz\ReservaBundle\Entity\OrdenTrabajo $ot
     * @return OrdenTrabajoReparacionesTareas
     */
    public function setOt(\Hertz\ReservaBundle\Entity\OrdenTrabajo $ot)
    {
        $this->ot = $ot;

        return $this;
    }

    /**
     * Get ot
     *
     * @return \Hertz\ReservaBundle\Entity\OrdenTrabajo 
     */
    public function getOt()
    {
        return $this->ot;
    }
}
