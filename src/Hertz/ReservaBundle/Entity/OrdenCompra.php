<?php
namespace Hertz\ReservaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\DiExtraBundle\Annotation as DI;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Doctrine\ORM\EntityManager;

/**
 * OrdenCompra
 *
 * @ORM\Table(name="ordenesCompra")
 * @ORM\Entity(repositoryClass="OrdenCompraRepository")
 */
class OrdenCompra{
	
	const ORM_ENTITY = "HertzReservaBundle:OrdenCompra";

	/**
	 *
	 * @var integer
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @Assert\Type(type="integer", message="Debe ser entero mayor a 0") 
	 */
	private $id;
	
	/**
	 *
	 * @var string
	 *
	 * @ORM\Column(name="descripcion", type="string", length=255, nullable=false)
	 * @Assert\NotBlank(message="El campo Descripción es obligatorio.")
	 * @Assert\Length(
        *      min = "2",
        *      max = "255",
        *      minMessage = "Debe tener al menos {{ limit }} caracteres de largo",
        *      maxMessage = "Debe tener un máximo de {{ limit }} caracteres"
        * )
	 */
	private $descripcion;
		
	/**
	 *
	 * @var datetime
	 *
	 * @ORM\Column(name="fechaCarga", type="datetime", nullable=false)
	 * @Assert\NotBlank(message="El campo fecha es obligatorio.")
	 */
	private $fechaCarga;
	
	/**
	 *
	 * @var string
	 *
	 * @ORM\Column(name="usuario", type="string", length=255, nullable=false)
	 * @Assert\NotBlank(message="El campo usuario es obligatorio.")
	 * @Assert\Length(
            *      min = "2",
            *      max = "255",
            *      minMessage = "Debe tener al menos {{ limit }} caracteres de largo",
            *      maxMessage = "Debe tener un máximo de {{ limit }} caracteres"
            * )
	 */
	private $usuario;

	/**
	 *
	 *@ORM\ManyToOne(targetEntity="Hertz\ReservaBundle\Entity\Agencia",  cascade={"persist"})
	 *@ORM\JoinColumn(name="agencia", referencedColumnName="id", nullable=false )
	 *@Assert\NotBlank(message="El campo Agencia es obligatorio.")
	 *@Assert\Type(type="object", message="El valor {{ value }} no es válido debe ser {{ type }}.")
	 *
	 */
	private $agencia;
	
	/**
	 *
	 * @var boolean
	 *
	 * @ORM\Column(name="debaja", type="boolean", nullable=false)
	 * @Assert\Choice(choices = {true,false}, message = "Elección incorrecta (Y , S , N).")
	 */
	private $debaja;

        /**
	 *
	 *@ORM\ManyToOne(targetEntity="Hertz\ReservaBundle\Entity\Proveedores",  cascade={"persist"})
	 *@ORM\JoinColumn(name="proveedor", referencedColumnName="id", nullable=false )
	 *@Assert\NotBlank(message="El campo Proveedor es obligatorio.")
	 *@Assert\Type(type="object", message="El valor {{ value }} no es válido debe ser {{ type }}.")
	 *
	 */
	private $proveedor;


        /**
         * 
         *
         *@ORM\ManyToOne(targetEntity="Hertz\ReservaBundle\Entity\EstadosOc",  cascade={"persist"})
         *@ORM\JoinColumn(name="estado", referencedColumnName="id", nullable=true)
         * 
         */
        private $estado;

        
        /**
	 *
	 * @var boolean
	 *
	 * @ORM\Column(name="pagado", type="boolean", nullable=false, options={"default":false})
	 * @Assert\Choice(choices = {true,false}, message = "Elección incorrecta (Y , S , N).")
	 */
	private $pagado;

        /**
        * @ORM\ManyToMany(targetEntity="Hertz\ReservaBundle\Entity\MateriaPrimaCompras", mappedBy="oc", cascade={"persist"})
        */
        private $materiaPrimaCompras;
        
         /**
	 * 
	 *
	 *@ORM\ManyToOne(targetEntity="Hertz\ReservaBundle\Entity\TipoComprobante",  cascade={"persist"})
        *@ORM\JoinColumn(name="tipoComprobante", referencedColumnName="id", nullable=true )
	 * 
	 */
	private $tipoComprobante;
        
        /**
         *
         * @var string
         *
         * @ORM\Column(name="nrofactura", type="string", length=100, nullable=true)
         * @Assert\Length(
         *      max = "100",
         *      maxMessage = "Debe tener un máximo de {{ limit }} caracteres"
         * )
         */
        private $nrofactura;  

        
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return OrdenCompra
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set fechaCarga
     *
     * @param \DateTime $fechaCarga
     * @return OrdenCompra
     */
    public function setFechaCarga($fechaCarga)
    {
        $this->fechaCarga = $fechaCarga;

        return $this;
    }

    /**
     * Get fechaCarga
     *
     * @return \DateTime 
     */
    public function getFechaCarga()
    {
        return $this->fechaCarga;
    }

    /**
     * Set usuario
     *
     * @param string $usuario
     * @return OrdenCompra
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return string 
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set debaja
     *
     * @param boolean $debaja
     * @return OrdenCompra
     */
    public function setDebaja($debaja)
    {
        $this->debaja = $debaja;

        return $this;
    }

    /**
     * Get debaja
     *
     * @return boolean 
     */
    public function getDebaja()
    {
        return $this->debaja;
    }

    /**
     * Set pagado
     *
     * @param boolean $pagado
     * @return OrdenCompra
     */
    public function setPagado($pagado)
    {
        $this->pagado = $pagado;

        return $this;
    }

    /**
     * Get pagado
     *
     * @return boolean 
     */
    public function getPagado()
    {
        return $this->pagado;
    }

    /**
     * Set agencia
     *
     * @param \Hertz\ReservaBundle\Entity\Agencia $agencia
     * @return OrdenCompra
     */
    public function setAgencia(\Hertz\ReservaBundle\Entity\Agencia $agencia)
    {
        $this->agencia = $agencia;

        return $this;
    }

    /**
     * Get agencia
     *
     * @return \Hertz\ReservaBundle\Entity\Agencia 
     */
    public function getAgencia()
    {
        return $this->agencia;
    }

    /**
     * Set proveedor
     *
     * @param \Hertz\ReservaBundle\Entity\Proveedores $proveedor
     * @return OrdenCompra
     */
    public function setProveedor(\Hertz\ReservaBundle\Entity\Proveedores $proveedor)
    {
        $this->proveedor = $proveedor;

        return $this;
    }

    /**
     * Get proveedor
     *
     * @return \Hertz\ReservaBundle\Entity\Proveedores 
     */
    public function getProveedor()
    {
        return $this->proveedor;
    }

    /**
     * Set estado
     *
     * @param \Hertz\ReservaBundle\Entity\EstadosOc $estado
     * @return OrdenCompra
     */
    public function setEstado(\Hertz\ReservaBundle\Entity\EstadosOc $estado = null)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return \Hertz\ReservaBundle\Entity\EstadosOc 
     */
    public function getEstado()
    {
        return $this->estado;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->materiaPrimaCompras = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add materiaPrimaCompras
     *
     * @param \Hertz\ReservaBundle\Entity\MateriaPrimaCompras $materiaPrimaCompras
     * @return OrdenCompra
     */
    public function addMateriaPrimaCompra(\Hertz\ReservaBundle\Entity\MateriaPrimaCompras $materiaPrimaCompras)
    {
        $this->materiaPrimaCompras[] = $materiaPrimaCompras;

        return $this;
    }

    /**
     * Remove materiaPrimaCompras
     *
     * @param \Hertz\ReservaBundle\Entity\MateriaPrimaCompras $materiaPrimaCompras
     */
    public function removeMateriaPrimaCompra(\Hertz\ReservaBundle\Entity\MateriaPrimaCompras $materiaPrimaCompras)
    {
        $this->materiaPrimaCompras->removeElement($materiaPrimaCompras);
    }

    /**
     * Get materiaPrimaCompras
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMateriaPrimaCompras()
    {
        return $this->materiaPrimaCompras;
    }

    /**
     * Set nrofactura
     *
     * @param string $nrofactura
     * @return OrdenCompra
     */
    public function setNrofactura($nrofactura)
    {
        $this->nrofactura = $nrofactura;

        return $this;
    }

    /**
     * Get nrofactura
     *
     * @return string 
     */
    public function getNrofactura()
    {
        return $this->nrofactura;
    }

    /**
     * Set tipoComprobante
     *
     * @param \Hertz\ReservaBundle\Entity\TipoComprobante $tipoComprobante
     * @return OrdenCompra
     */
    public function setTipoComprobante(\Hertz\ReservaBundle\Entity\TipoComprobante $tipoComprobante = null)
    {
        $this->tipoComprobante = $tipoComprobante;

        return $this;
    }

    /**
     * Get tipoComprobante
     *
     * @return \Hertz\ReservaBundle\Entity\TipoComprobante 
     */
    public function getTipoComprobante()
    {
        return $this->tipoComprobante;
    }
}
