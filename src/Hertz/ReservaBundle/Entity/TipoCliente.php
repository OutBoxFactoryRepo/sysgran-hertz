<?php
namespace Hertz\ReservaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\DiExtraBundle\Annotation as DI;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Doctrine\ORM\EntityManager;

/**
 * TipoCliente
 *
 * @ORM\Table(name="tipocliente")
 * @ORM\Entity(repositoryClass="TipoClienteRepository")
 */
class TipoCliente{
	
	const ORM_ENTITY = "HertzReservaBundle:TipoCliente";

	/**
	 *
	 * @var integer
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @Assert\Type(type="integer", message="Debe ser entero mayor a 0|id") 
	 */
	private $id;

	/**
	 *
	 * @var string
	 *
	 * @ORM\Column(name="descripcion", type="string", length=255, nullable=false)
	 * @Assert\NotNull(message="El campo Descripción es obligatorio.|descripcion")
	 */
	private $descripcion;

	/**
	 * @ORM\ManyToMany(targetEntity="Hertz\ReservaBundle\Entity\Cliente", inversedBy="tipoCliente", cascade={"persist"})
	 * @ORM\JoinTable(name="cliente_tiposcliente")
	 **/
	private $tipos;
	
	/**
	 *
	 * @var boolean
	 *
	 * @ORM\Column(name="debaja", type="boolean")
	 */
	private $debaja;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->tipos = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set id
     *
     * @param integer $id
     * @return TipoCliente
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return TipoCliente
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set debaja
     *
     * @param boolean $debaja
     * @return TipoCliente
     */
    public function setDebaja($debaja)
    {
        $this->debaja = $debaja;

        return $this;
    }

    /**
     * Get debaja
     *
     * @return boolean 
     */
    public function getDebaja()
    {
        return $this->debaja;
    }

    /**
     * Add tipos
     *
     * @param \Hertz\ReservaBundle\Entity\Cliente $tipos
     * @return TipoCliente
     */
    public function addTipo(\Hertz\ReservaBundle\Entity\Cliente $tipos)
    {
        $this->tipos[] = $tipos;

        return $this;
    }

    /**
     * Remove tipos
     *
     * @param \Hertz\ReservaBundle\Entity\Cliente $tipos
     */
    public function removeTipo(\Hertz\ReservaBundle\Entity\Cliente $tipos)
    {
        $this->tipos->removeElement($tipos);
    }

    /**
     * Get tipos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTipos()
    {
        return $this->tipos;
    }
}
