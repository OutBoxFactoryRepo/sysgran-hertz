<?php
namespace Hertz\ReservaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\DiExtraBundle\Annotation as DI;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Doctrine\ORM\EntityManager;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Contrato
 *
 * @ORM\Table(name="contratos")
 * @ORM\Entity(repositoryClass="ContratoRepository")
 */
class Contrato{
	
	const ORM_ENTITY = "HertzReservaBundle:Contrato";

	/**
	 *
	 * @var integer
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @Assert\Type(type="integer", message="Debe ser entero mayor a 0") 
	 */
	private $id;

        /**
	 *
	 * @var string
	 *
	 * @ORM\Column(name="observaciones", type="string", length=500, nullable=true)
	 * @Assert\Length(
         *      max = "500",
         *      maxMessage = "Debe tener un máximo de {{ limit }} caracteres"
         * )
	 */
	private $observaciones;
        
	/**
	 *
	 * @var integer
	 *
	 * @ORM\Column(name="totalDias", type="integer", nullable=true)
	 * @Assert\NotBlank(message="El campo Total días es obligatorio.")
	 * @Assert\Type(type="numeric", message="El valor {{ value }} no es válido debe ser {{ type }}.")
	 */
	private $totalDias;
	
        /**
	 *
	 * @var integer
	 *
	 * @ORM\Column(name="totalHoras", type="integer", nullable=true)
	 * @Assert\NotBlank(message="El campo Total horas es obligatorio.")
	 * @Assert\Type(type="numeric", message="El valor {{ value }} no es válido debe ser {{ type }}.")
	 */
	private $totalHoras;
        
	/**
	 *
	 * @var integer
	 *
	 * @ORM\Column(name="totalKm", type="integer", nullable=true)
	 * @Assert\NotBlank(message="El campo Total Kilometros es obligatorio.")
	 * @Assert\Type(type="numeric", message="El valor {{ value }} no es válido debe ser {{ type }}.")
	 */
	private $totalKm;
        
        
 	/**
	 *
	 *
	 *@ORM\ManyToOne(targetEntity="Hertz\ReservaBundle\Entity\Reserva",  cascade={"remove"})
	 *@ORM\JoinColumn(name="reserva", referencedColumnName="id", nullable=true )
	 *@Assert\NotBlank(message="El campo Reserva es obligatorio.")
	 *@Assert\Type(type="object", message="El valor {{ value }} no es válido debe ser {{ type }}.")
	 *
	 */
	private $reserva;

        
        /**
	 * @ORM\ManyToMany(targetEntity="Hertz\ReservaBundle\Entity\ContratoAdic", inversedBy="idContrato", cascade={"persist"})
	 * @ORM\JoinTable(name="contrato_adicionales")
	 **/
	private $adicionales;
        
        /**
	 *
	 * @var float
	 *
	 * @ORM\Column(name="tasaAeropuerto", type="float", nullable=false,options={"default":"0"})
	 * @Assert\NotBlank(message="El campo Tasa Aeropuerto es obligatorio.")
	 * @Assert\Type(type="numeric", message="El valor {{ value }} no es válido debe ser {{ type }}.")
	 */
	private $tasaAeropuerto;

        /**
	 *
	 * @var float
	 *
	 * @ORM\Column(name="tasaApt", type="float", nullable=false,options={"default":"0"})
	 * @Assert\NotBlank(message="El campo Tasa APT es obligatorio.")
	 * @Assert\Type(type="numeric", message="El valor {{ value }} no es válido debe ser {{ type }}.")
	 */
	private $tasaApt;
        
        /**
	 *
	 * @var float
	 *
	 * @ORM\Column(name="ivaPorcent", type="float", nullable=false,options={"default":"0"})
	 * @Assert\NotBlank(message="El campo Iva % es obligatorio.")
	 * @Assert\Type(type="numeric", message="El valor {{ value }} no es válido debe ser {{ type }}.")
	 */
	private $ivaPorcent;
        
        /**
	 *
	 * @var float
	 *
	 * @ORM\Column(name="anticipo1", type="float", nullable=false,options={"default":"0"})
	 * @Assert\NotBlank(message="El campo Anticipo 1 es obligatorio.")
	 * @Assert\Type(type="numeric", message="El valor {{ value }} no es válido debe ser {{ type }}.")
	 */
	private $anticipo1;
        
        /**
	 *
	 * @var float
	 *
	 * @ORM\Column(name="anticipo2", type="float", nullable=false,options={"default":"0"})
	 * @Assert\NotBlank(message="El campo Anticipo 2 es obligatorio.")
	 * @Assert\Type(type="numeric", message="El valor {{ value }} no es válido debe ser {{ type }}.")
	 */
	private $anticipo2;
        
        /**
	 *
	 * @var float
	 *
	 * @ORM\Column(name="voucher", type="float", nullable=false,options={"default":"0"})
	 * @Assert\NotBlank(message="El campo voucher es obligatorio.")
	 * @Assert\Type(type="numeric", message="El valor {{ value }} no es válido debe ser {{ type }}.")
	 */
	private $voucher;
        
        /**
	 *
	 * @var float
	 *
	 * @ORM\Column(name="reintegros", type="float", nullable=false,options={"default":"0"})
	 * @Assert\NotBlank(message="El campo reintegros es obligatorio.")
	 * @Assert\Type(type="numeric", message="El valor {{ value }} no es válido debe ser {{ type }}.")
	 */
	private $reintegros;
        
        /**
	 *
	 * @var float
	 *
	 * @ORM\Column(name="estado", type="integer", nullable=false,options={"default":"0"})
	 * @Assert\NotBlank(message="El campo eestado es obligatorio.")
	 */
	private $estado;
        
        
        /**
	 *
	 * @var float
	 *
	 * @ORM\Column(name="contado", type="float", nullable=false,options={"default":"0"})
	 * @Assert\NotBlank(message="El campo contado es obligatorio.")
	 * @Assert\Type(type="numeric", message="El valor {{ value }} no es válido debe ser {{ type }}.")
	 */
	private $contado;
        
        
        /**
	 *
	 * @var float
	 *
	 * @ORM\Column(name="cheque", type="float", nullable=false,options={"default":"0"})
	 * @Assert\NotBlank(message="El campo cheque es obligatorio.")
	 * @Assert\Type(type="numeric", message="El valor {{ value }} no es válido debe ser {{ type }}.")
	 */
	private $cheque;
        
        /**
	 *
	 * @var float
	 *
	 * @ORM\Column(name="tarjeta", type="float", nullable=false,options={"default":"0"})
	 * @Assert\NotBlank(message="El campo tarjeta es obligatorio.")
	 * @Assert\Type(type="numeric", message="El valor {{ value }} no es válido debe ser {{ type }}.")
	 */
	private $tarjeta;
        
        /**
	 *
	 * @var float
	 *
	 * @ORM\Column(name="ctacte", type="float", nullable=false,options={"default":"0"})
	 * @Assert\NotBlank(message="El campo ctacte es obligatorio.")
	 * @Assert\Type(type="numeric", message="El valor {{ value }} no es válido debe ser {{ type }}.")
	 */
	private $ctacte;
        
        /**
	 *
	 * @var float
	 *
	 * @ORM\Column(name="prepagos", type="float", nullable=false,options={"default":"0"})
	 * @Assert\NotBlank(message="El campo prepagos es obligatorio.")
	 * @Assert\Type(type="numeric", message="El valor {{ value }} no es válido debe ser {{ type }}.")
	 */
	private $prepagos;
        
        /**
	 *
	 * @var float
	 *
	 * @ORM\Column(name="tarjetaEmisor", type="integer", nullable=false,options={"default":"0"})
	 * @Assert\NotBlank(message="El campo emisor es obligatorio.")
	 * @Assert\Type(type="numeric", message="El valor {{ value }} no es válido debe ser {{ type }}.")
	 */
	private $tarjetaEmisor;
        
        /**
	 *
	 * @var float
	 *
	 * @ORM\Column(name="diaria", type="float", nullable=false,options={"default":"0"})
	 * @Assert\NotBlank(message="El campo diaria es obligatorio.")
	 * @Assert\Type(type="numeric", message="El valor {{ value }} no es válido debe ser {{ type }}.")
	 */
	private $diaria;
        
        /**
	 *
	 * @var integer
	 *
	 * @ORM\Column(name="diariaCantidad", type="integer", nullable=false,options={"default":"0"})
	 * @Assert\NotBlank(message="El campo diaria cantidad es obligatorio.")
	 * @Assert\Type(type="numeric", message="El valor {{ value }} no es válido debe ser {{ type }}.")
	 */
	private $diariaCantidad;


	/**
	 *
	 * @var integer
	 *
	 * @ORM\Column(name="usuario", type="integer", nullable=true)
	 */
	private $usuario;

	/**
	 *
	 * @var datetime
	 *
	 * @ORM\Column(name="lockTime", type="datetime", nullable=true)
	 */
	private $lockTime;

    /**
     *
     * @var integer
     *
     * @ORM\Column(name="kilometros", type="integer", nullable=false)
     * @Assert\NotBlank(message="El campo Kilometros es obligatorio.")
     * @Assert\Type(type="numeric", message="El valor {{ value }} no es válido debe ser {{ type }}.")
     */
    private $kilometros;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="estadoTanque", type="string", length=10, nullable=true,options={"default":""})
     */
    private $estadoTanque;
        
        /**
        *
        *
        *@ORM\ManyToOne(targetEntity="Hertz\ReservaBundle\Entity\Agencia",  cascade={"remove"})
        *@ORM\JoinColumn(name="agenciaOrigen", referencedColumnName="id", nullable=true )
        *@Assert\NotBlank(message="El campo Agencia de Origen es obligatorio.")
        *@Assert\Type(type="object", message="El valor {{ value }} no es válido debe ser {{ type }}.")
        *
        */
       private $agenciaOrigen;
       
       /**
        *
        *
        *@ORM\ManyToOne(targetEntity="Hertz\ReservaBundle\Entity\Agencia",  cascade={"remove"})
        *@ORM\JoinColumn(name="agenciaDestino", referencedColumnName="id", nullable=true )
        *@Assert\NotBlank(message="El campo Agencia de Destino es obligatorio.")
        *@Assert\Type(type="object", message="El valor {{ value }} no es válido debe ser {{ type }}.")
        *
        */
       private $agenciaDestino;

	/**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set observaciones
     *
     * @param string $observaciones
     * @return Contrato
     */
    public function setObservaciones($observaciones)
    {
        $this->observaciones = $observaciones;

        return $this;
    }

    /**
     * Get observaciones
     *
     * @return string 
     */
    public function getObservaciones()
    {
        return $this->observaciones;
    }

    /**
     * Set totalDias
     *
     * @param integer $totalDias
     * @return Contrato
     */
    public function setTotalDias($totalDias)
    {
        $this->totalDias = $totalDias;

        return $this;
    }

    /**
     * Get totalDias
     *
     * @return integer 
     */
    public function getTotalDias()
    {
        return $this->totalDias;
    }

    /**
     * Set totalHoras
     *
     * @param integer $totalHoras
     * @return Contrato
     */
    public function setTotalHoras($totalHoras)
    {
        $this->totalHoras = $totalHoras;

        return $this;
    }

    /**
     * Get totalHoras
     *
     * @return integer 
     */
    public function getTotalHoras()
    {
        return $this->totalHoras;
    }

    /**
     * Set totalKm
     *
     * @param integer $totalKm
     * @return Contrato
     */
    public function setTotalKm($totalKm)
    {
        $this->totalKm = $totalKm;

        return $this;
    }

    /**
     * Get totalKm
     *
     * @return integer 
     */
    public function getTotalKm()
    {
        return $this->totalKm;
    }

    /**
     * Set reserva
     *
     * @param \Hertz\ReservaBundle\Entity\Reserva $reserva
     * @return Contrato
     */
    public function setReserva(\Hertz\ReservaBundle\Entity\Reserva $reserva)
    {
        $this->reserva = $reserva;

        return $this;
    }

    /**
     * Get reserva
     *
     * @return \Hertz\ReservaBundle\Entity\Reserva 
     */
    public function getReserva()
    {
        return $this->reserva;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->adicionales = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add adicionales
     *
     * @param \Hertz\ReservaBundle\Entity\ContratoAdic $adicionales
     * @return Contrato
     */
    public function addAdicionale(\Hertz\ReservaBundle\Entity\ContratoAdic $adicionales)
    {
        $this->adicionales[] = $adicionales;

        return $this;
    }

    /**
     * Remove adicionales
     *
     * @param \Hertz\ReservaBundle\Entity\ContratoAdic $adicionales
     * @return Contrato
     */
    public function removeAdicionale(\Hertz\ReservaBundle\Entity\ContratoAdic $adicionales)
    {
        $adicionales->removeIdContrato($this); // Call Post's setter here
        $this->adicionales->removeElement($adicionales);

    }

    /**
     * Get adicionales
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAdicionales()
    {
        return $this->adicionales;
    }
    
    
    /**
     * Clear adicionales
     *
     * @param \Hertz\ReservaBundle\Entity\ContratoAdic $adicionales
     */
    public function clearAdicionales()
    {
        //return $this->adicionales;
        $auxAdic = $this->getAdicionales();
        foreach($auxAdic as $Adic)
        {
            $this->removeAdicionale($Adic);
        }
    }

    /**
     * Set tasaAeropuerto
     *
     * @param float $tasaAeropuerto
     * @return Contrato
     */
    public function setTasaAeropuerto($tasaAeropuerto)
    {
        $this->tasaAeropuerto = $tasaAeropuerto;

        return $this;
    }

    /**
     * Get tasaAeropuerto
     *
     * @return float 
     */
    public function getTasaAeropuerto()
    {
        return $this->tasaAeropuerto;
    }

    /**
     * Set tasaApt
     *
     * @param float $tasaApt
     * @return Contrato
     */
    public function setTasaApt($tasaApt)
    {
        $this->tasaApt = $tasaApt;

        return $this;
    }

    /**
     * Get tasaApt
     *
     * @return float 
     */
    public function getTasaApt()
    {
        return $this->tasaApt;
    }

    /**
     * Set ivaPorcent
     *
     * @param float $ivaPorcent
     * @return Contrato
     */
    public function setIvaPorcent($ivaPorcent)
    {
        $this->ivaPorcent = $ivaPorcent;

        return $this;
    }

    /**
     * Get ivaPorcent
     *
     * @return float 
     */
    public function getIvaPorcent()
    {
        return $this->ivaPorcent;
    }

    /**
     * Set anticipo1
     *
     * @param float $anticipo1
     * @return Contrato
     */
    public function setAnticipo1($anticipo1)
    {
        $this->anticipo1 = $anticipo1;

        return $this;
    }

    /**
     * Get anticipo1
     *
     * @return float 
     */
    public function getAnticipo1()
    {
        return $this->anticipo1;
    }

    /**
     * Set anticipo2
     *
     * @param float $anticipo2
     * @return Contrato
     */
    public function setAnticipo2($anticipo2)
    {
        $this->anticipo2 = $anticipo2;

        return $this;
    }

    /**
     * Get anticipo2
     *
     * @return float 
     */
    public function getAnticipo2()
    {
        return $this->anticipo2;
    }

    /**
     * Set voucher
     *
     * @param float $voucher
     * @return Contrato
     */
    public function setVoucher($voucher)
    {
        $this->voucher = $voucher;

        return $this;
    }

    /**
     * Get voucher
     *
     * @return float 
     */
    public function getVoucher()
    {
        return $this->voucher;
    }

    /**
     * Set reintegros
     *
     * @param float $reintegros
     * @return Contrato
     */
    public function setReintegros($reintegros)
    {
        $this->reintegros = $reintegros;

        return $this;
    }

    /**
     * Get reintegros
     *
     * @return float 
     */
    public function getReintegros()
    {
        return $this->reintegros;
    }

    /**
     * Set estado
     *
     * @param integer $estado
     * @return Contrato
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return integer 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set contado
     *
     * @param float $contado
     * @return Contrato
     */
    public function setContado($contado)
    {
        $this->contado = $contado;

        return $this;
    }

    /**
     * Get contado
     *
     * @return float 
     */
    public function getContado()
    {
        return $this->contado;
    }

    /**
     * Set cheque
     *
     * @param float $cheque
     * @return Contrato
     */
    public function setCheque($cheque)
    {
        $this->cheque = $cheque;

        return $this;
    }

    /**
     * Get cheque
     *
     * @return float 
     */
    public function getCheque()
    {
        return $this->cheque;
    }

    /**
     * Set tarjeta
     *
     * @param float $tarjeta
     * @return Contrato
     */
    public function setTarjeta($tarjeta)
    {
        $this->tarjeta = $tarjeta;

        return $this;
    }

    /**
     * Get tarjeta
     *
     * @return float 
     */
    public function getTarjeta()
    {
        return $this->tarjeta;
    }

    /**
     * Set ctacte
     *
     * @param float $ctacte
     * @return Contrato
     */
    public function setCtacte($ctacte)
    {
        $this->ctacte = $ctacte;

        return $this;
    }

    /**
     * Get ctacte
     *
     * @return float 
     */
    public function getCtacte()
    {
        return $this->ctacte;
    }

    /**
     * Set prepagos
     *
     * @param float $prepagos
     * @return Contrato
     */
    public function setPrepagos($prepagos)
    {
        $this->prepagos = $prepagos;

        return $this;
    }

    /**
     * Get prepagos
     *
     * @return float 
     */
    public function getPrepagos()
    {
        return $this->prepagos;
    }

    /**
     * Set tarjetaEmisor
     *
     * @param integer $tarjetaEmisor
     * @return Contrato
     */
    public function setTarjetaEmisor($tarjetaEmisor)
    {
        $this->tarjetaEmisor = $tarjetaEmisor;

        return $this;
    }

    /**
     * Get tarjetaEmisor
     *
     * @return integer 
     */
    public function getTarjetaEmisor()
    {
        return $this->tarjetaEmisor;
    }

    /**
     * Set diaria
     *
     * @param float $diaria
     * @return Contrato
     */
    public function setDiaria($diaria)
    {
        $this->diaria = $diaria;

        return $this;
    }

    /**
     * Get diaria
     *
     * @return float 
     */
    public function getDiaria()
    {
        return $this->diaria;
    }

    

    /**
     * Set diariaCantidad
     *
     * @param integer $diariaCantidad
     * @return Contrato
     */
    public function setDiariaCantidad($diariaCantidad)
    {
        $this->diariaCantidad = $diariaCantidad;

        return $this;
    }

    /**
     * Get diariaCantidad
     *
     * @return integer 
     */
    public function getDiariaCantidad()
    {
        return $this->diariaCantidad;
    }

	/**
	 * Set lockTime
	 *
	 * @param \DateTime $lockTime
	 * @return Contrato
	 */
	public function setLockTime($lockTime)
	{
		$this->lockTime = $lockTime;

		return $this;
	}

	/**
	 * Get lockTime
	 *
	 * @return \DateTime
	 */
	public function getLockTime()
	{
		return $this->lockTime;
	}

	

    /**
     * Set usuario
     *
     * @param integer $usuario
     * @return Contrato
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return integer 
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set agenciaOrigen
     *
     * @param \Hertz\ReservaBundle\Entity\Agencia $agenciaOrigen
     * @return Contrato
     */
    public function setAgenciaOrigen(\Hertz\ReservaBundle\Entity\Agencia $agenciaOrigen = null)
    {
        $this->agenciaOrigen = $agenciaOrigen;

        return $this;
    }

    /**
     * Get agenciaOrigen
     *
     * @return \Hertz\ReservaBundle\Entity\Agencia 
     */
    public function getAgenciaOrigen()
    {
        return $this->agenciaOrigen;
    }

    /**
     * Set agenciaDestino
     *
     * @param \Hertz\ReservaBundle\Entity\Agencia $agenciaDestino
     * @return Contrato
     */
    public function setAgenciaDestino(\Hertz\ReservaBundle\Entity\Agencia $agenciaDestino = null)
    {
        $this->agenciaDestino = $agenciaDestino;

        return $this;
    }

    /**
     * Get agenciaDestino
     *
     * @return \Hertz\ReservaBundle\Entity\Agencia 
     */
    public function getAgenciaDestino()
    {
        return $this->agenciaDestino;
    }

    /**
     * Set kilometros
     *
     * @param integer $kilometros
     * @return Contrato
     */
    public function setKilometros($kilometros)
    {
        $this->kilometros = $kilometros;

        return $this;
    }

    /**
     * Get kilometros
     *
     * @return integer 
     */
    public function getKilometros()
    {
        return $this->kilometros;
    }

    /**
     * Set estadoTanque
     *
     * @param string $estadoTanque
     * @return Contrato
     */
    public function setEstadoTanque($estadoTanque)
    {
        $this->estadoTanque = $estadoTanque;

        return $this;
    }

    /**
     * Get estadoTanque
     *
     * @return string 
     */
    public function getEstadoTanque()
    {
        return $this->estadoTanque;
    }
}
