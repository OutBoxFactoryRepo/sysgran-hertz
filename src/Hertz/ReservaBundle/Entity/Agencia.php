<?php
namespace Hertz\ReservaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\DiExtraBundle\Annotation as DI;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Doctrine\ORM\EntityManager;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Agencia
 *
 * @ORM\Table(name="agencias")
 * @ORM\Entity(repositoryClass="AgenciaRepository")
 */
class Agencia{
	
	const ORM_ENTITY = "HertzReservaBundle:Agencia";

	/**
	 *
	 * @var integer
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @Assert\Type(type="integer", message="Debe ser entero mayor a 0") 
	 */
	private $id;

	/**
	 *
	 * @var string
	 *
	 * @ORM\Column(name="descripcion", type="string", length=255, nullable=false)
	 * @Assert\NotBlank(message="El campo es obligatorio.")
	 * @Assert\Length(
     *      min = "2",
     *      max = "255",
     *      minMessage = "Debe tener al menos {{ limit }} caracteres de largo",
     *      maxMessage = "Debe tener un máximo de {{ limit }} caracteres"
     * )
	 */
	private $descripcion;
	
		
	/**
	 *
	 * @var boolean
	 *
	 * @ORM\Column(name="debaja", type="boolean", nullable=false)
	 * @Assert\NotNull(message="El campo es obligatorio.")
	 */
	private $debaja;

    /**
     * @ORM\ManyToOne(targetEntity="Hertz\ReservaBundle\Entity\Franquiciados", inversedBy="agencia")
     * @ORM\JoinColumn(name="idfranquiciados", referencedColumnName="id", nullable=true)
     */
    private $franquiciados;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Agencia
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set debaja
     *
     * @param string $debaja
     * @return Agencia
     */
    public function setDebaja($debaja)
    {
        $this->debaja = $debaja;

        return $this;
    }

    /**
     * Get debaja
     *
     * @return string 
     */
    public function getDebaja()
    {
        return $this->debaja;
    }

    /**
     * Set franquiciados
     *
     * @param \Hertz\ReservaBundle\Entity\Franquiciados $franquiciados
     * @return Agencia
     */
    public function setFranquiciados(\Hertz\ReservaBundle\Entity\Franquiciados $franquiciados = null)
    {
        $this->franquiciados = $franquiciados;

        return $this;
    }

    /**
     * Get franquiciados
     *
     * @return \Hertz\ReservaBundle\Entity\Franquiciados 
     */
    public function getFranquiciados()
    {
        return $this->franquiciados;
    }

    //Determinar si tiene ya una franquicia asignada la agencia.
    public function hasFranquiciados()
    {
        return $this->getFranquiciados();
    }
}
