<?php
namespace Hertz\ReservaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\DiExtraBundle\Annotation as DI;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Doctrine\ORM\EntityManager;

/**
 * MateriaPrimaConsumos
 *
 * @ORM\Table(name="materiasprimasconsumos")
 * @ORM\Entity(repositoryClass="MateriaPrimaConsumosRepository")
 */
class MateriaPrimaConsumos{
	
	const ORM_ENTITY = "HertzReservaBundle:MateriaPrimaConsumos";

	/**
	 *
	 * @var integer
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @Assert\Type(type="integer", message="Debe ser entero mayor a 0") 
	 */
	private $id;
	
	/**
	 *
	 * @var string
	 *
	 * @ORM\Column(name="descripcion", type="string", length=255, nullable=false)
	 * @Assert\NotBlank(message="El campo Descripción es obligatorio.")
	 * @Assert\Length(
     *      min = "2",
     *      max = "255",
     *      minMessage = "Debe tener al menos {{ limit }} caracteres de largo",
     *      maxMessage = "Debe tener un máximo de {{ limit }} caracteres"
     * )
	 */
	private $descripcion;
	

	/**
	 *
	 *
	 *@ORM\ManyToOne(targetEntity="Hertz\ReservaBundle\Entity\Agencia")
	 *@ORM\JoinColumn(name="agencia", referencedColumnName="id", nullable=false )
	 *@Assert\NotBlank(message="El campo Agencia es obligatorio.")
	 *@Assert\Type(type="object", message="El valor {{ value }} no es válido debe ser {{ type }}.")
	 *
	 */
	private $agencia;
		
	
	/**
	 *
	 *
	 *@ORM\ManyToOne(targetEntity="Hertz\ReservaBundle\Entity\OrdenTrabajo")
	 *@ORM\JoinColumn(name="ot", referencedColumnName="id", nullable=false )
	 *@Assert\NotBlank(message="El campo Orden Trabajo es obligatorio.")
	 *@Assert\Type(type="object", message="El valor {{ value }} no es válido debe ser {{ type }}.")
	 *
	 */
	private $ot;
	
	
	/**
	 *
	 *
	 *@ORM\ManyToOne(targetEntity="Hertz\ReservaBundle\Entity\MateriaPrima")
	 *@ORM\JoinColumn(name="materiaprima", referencedColumnName="id", nullable=false )
	 *@Assert\NotBlank(message="El campo Materia Prima es obligatorio.")
	 *@Assert\Type(type="object", message="El valor {{ value }} no es válido debe ser {{ type }}.")
	 *
	 */
	private $materiaprima;
	
	
	/**
	 *
	 * @var integer
	 *
	 * @ORM\Column(name="cantidad", type="integer", nullable=false)
	 * @Assert\NotBlank(message="El campo cantidad es obligatorio.")
	 */
	private $cantidad;	
		
	/**
	 *
	 * @var float
	 *
	 * @ORM\Column(name="valor", type="float", nullable=false)
	 * @Assert\NotBlank(message="El campo valor es obligatorio.")
	 */
	private $valor;
			
	/**
	 *
	 * @var datetime
	 *
	 * @ORM\Column(name="fechaCarga", type="datetime", nullable=false)
	 * @Assert\NotBlank(message="El campo fecha es obligatorio.")
	 */
	private $fechaCarga;
	
	/**
	 *
	 * @var string
	 *
	 * @ORM\Column(name="usuario", type="string", length=255, nullable=false)
	 * @Assert\NotBlank(message="El campo usuario es obligatorio.")
	 * @Assert\Length(
     *      min = "2",
     *      max = "255",
     *      minMessage = "Debe tener al menos {{ limit }} caracteres de largo",
     *      maxMessage = "Debe tener un máximo de {{ limit }} caracteres"
     * )
	 */
	private $usuario;

	
	/**
	 *
	 * @var boolean
	 *
	 * @ORM\Column(name="debaja", type="boolean", nullable=false)
	 * @Assert\Choice(choices = {true,false}, message = "Elección incorrecta (Y , S , N).")
	 */
	private $debaja;
	

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return MateriaPrimaConsumos
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set cantidad
     *
     * @param integer $cantidad
     * @return MateriaPrimaConsumos
     */
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    /**
     * Get cantidad
     *
     * @return integer 
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * Set valor
     *
     * @param float $valor
     * @return MateriaPrimaConsumos
     */
    public function setValor($valor)
    {
        $this->valor = $valor;

        return $this;
    }

    /**
     * Get valor
     *
     * @return float 
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * Set fechaCarga
     *
     * @param \DateTime $fechaCarga
     * @return MateriaPrimaConsumos
     */
    public function setFechaCarga($fechaCarga)
    {
        $this->fechaCarga = $fechaCarga;

        return $this;
    }

    /**
     * Get fechaCarga
     *
     * @return \DateTime 
     */
    public function getFechaCarga()
    {
        return $this->fechaCarga;
    }

    /**
     * Set usuario
     *
     * @param string $usuario
     * @return MateriaPrimaConsumos
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return string 
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set debaja
     *
     * @param boolean $debaja
     * @return MateriaPrimaConsumos
     */
    public function setDebaja($debaja)
    {
        $this->debaja = $debaja;

        return $this;
    }

    /**
     * Get debaja
     *
     * @return boolean 
     */
    public function getDebaja()
    {
        return $this->debaja;
    }

    /**
     * Set agencia
     *
     * @param \Hertz\ReservaBundle\Entity\Agencia $agencia
     * @return MateriaPrimaConsumos
     */
    public function setAgencia(\Hertz\ReservaBundle\Entity\Agencia $agencia)
    {
        $this->agencia = $agencia;

        return $this;
    }

    /**
     * Get agencia
     *
     * @return \Hertz\ReservaBundle\Entity\Agencia 
     */
    public function getAgencia()
    {
        return $this->agencia;
    }

    /**
     * Set ot
     *
     * @param \Hertz\ReservaBundle\Entity\OrdenTrabajo $ot
     * @return MateriaPrimaConsumos
     */
    public function setOt(\Hertz\ReservaBundle\Entity\OrdenTrabajo $ot)
    {
        $this->ot = $ot;

        return $this;
    }

    /**
     * Get ot
     *
     * @return \Hertz\ReservaBundle\Entity\OrdenTrabajo 
     */
    public function getOt()
    {
        return $this->ot;
    }

    /**
     * Set materiaprima
     *
     * @param \Hertz\ReservaBundle\Entity\MateriaPrima $materiaprima
     * @return MateriaPrimaConsumos
     */
    public function setMateriaprima(\Hertz\ReservaBundle\Entity\MateriaPrima $materiaprima)
    {
        $this->materiaprima = $materiaprima;

        return $this;
    }

    /**
     * Get materiaprima
     *
     * @return \Hertz\ReservaBundle\Entity\MateriaPrima 
     */
    public function getMateriaprima()
    {
        return $this->materiaprima;
    }
}
