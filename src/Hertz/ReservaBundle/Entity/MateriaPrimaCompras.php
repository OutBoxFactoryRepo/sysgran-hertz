<?php
namespace Hertz\ReservaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\DiExtraBundle\Annotation as DI;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Doctrine\ORM\EntityManager;

/**
 * MateriaPrimaCompras
 *
 * @ORM\Table(name="materiasprimascompras")
 * @ORM\Entity(repositoryClass="MateriaPrimaComprasRepository")
 */
class MateriaPrimaCompras{
	
	const ORM_ENTITY = "HertzReservaBundle:MateriaPrimaCompras";

	/**
	 *
	 * @var integer
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @Assert\Type(type="integer", message="Debe ser entero mayor a 0") 
	 */
	private $id;
	
	/**
	 *
	 * @var string
	 *
	 * @ORM\Column(name="descripcion", type="string", length=255, nullable=false)
	 * @Assert\NotBlank(message="El campo Descripción es obligatorio.")
	 * @Assert\Length(
     *      min = "2",
     *      max = "255",
     *      minMessage = "Debe tener al menos {{ limit }} caracteres de largo",
     *      maxMessage = "Debe tener un máximo de {{ limit }} caracteres"
     * )
	 */
	private $descripcion;
	

	/**
	 *
	 *
	 *@ORM\ManyToOne(targetEntity="Hertz\ReservaBundle\Entity\Agencia")
	 *@ORM\JoinColumn(name="agencia", referencedColumnName="id", nullable=false )
	 *@Assert\NotBlank(message="El campo Agencia es obligatorio.")
	 *@Assert\Type(type="object", message="El valor {{ value }} no es válido debe ser {{ type }}.")
	 *
	 */
	private $agencia;
		
        
        /**
        * @ORM\ManyToMany(targetEntity="Hertz\ReservaBundle\Entity\OrdenCompra", inversedBy="materiaPrimaCompras", cascade={"persist"})
        * @ORM\JoinTable(name="ordencompra_materiaprimacompras")
        **/
       private $oc;
	
	
	/**
	 *
	 *
	 *@ORM\ManyToOne(targetEntity="Hertz\ReservaBundle\Entity\MateriaPrima")
	 *@ORM\JoinColumn(name="materiaprima", referencedColumnName="id", nullable=false )
	 *@Assert\NotBlank(message="El campo Materia Prima es obligatorio.")
	 *@Assert\Type(type="object", message="El valor {{ value }} no es válido debe ser {{ type }}.")
	 *
	 */
	private $materiaprima;
	
	
	/**
	 *
	 * @var integer
	 *
	 * @ORM\Column(name="cantidad", type="integer", nullable=false)
	 * @Assert\NotBlank(message="El campo cantidad es obligatorio.")
	 */
	private $cantidad;	
		
	/**
	 *
	 * @var float
	 *
	 * @ORM\Column(name="valor", type="float", nullable=false)
	 */
	private $valor;
			
	/**
	 *
	 * @var datetime
	 *
	 * @ORM\Column(name="fechaCarga", type="datetime", nullable=false)
	 * @Assert\NotBlank(message="El campo fecha es obligatorio.")
	 */
	private $fechaCarga;
	
	/**
	 *
	 * @var string
	 *
	 * @ORM\Column(name="usuario", type="string", length=255, nullable=false)
	 * @Assert\NotBlank(message="El campo usuario es obligatorio.")
	 * @Assert\Length(
     *      min = "2",
     *      max = "255",
     *      minMessage = "Debe tener al menos {{ limit }} caracteres de largo",
     *      maxMessage = "Debe tener un máximo de {{ limit }} caracteres"
     * )
	 */
	private $usuario;

	
	/**
	 *
	 * @var boolean
	 *
	 * @ORM\Column(name="debaja", type="boolean", nullable=false)
	 * @Assert\Choice(choices = {true,false}, message = "Elección incorrecta (Y , S , N).")
	 */
	private $debaja;
	


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return MateriaPrimaCompras
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set cantidad
     *
     * @param integer $cantidad
     * @return MateriaPrimaCompras
     */
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    /**
     * Get cantidad
     *
     * @return integer 
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * Set valor
     *
     * @param float $valor
     * @return MateriaPrimaCompras
     */
    public function setValor($valor)
    {
        $this->valor = $valor;

        return $this;
    }

    /**
     * Get valor
     *
     * @return float 
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * Set fechaCarga
     *
     * @param \DateTime $fechaCarga
     * @return MateriaPrimaCompras
     */
    public function setFechaCarga($fechaCarga)
    {
        $this->fechaCarga = $fechaCarga;

        return $this;
    }

    /**
     * Get fechaCarga
     *
     * @return \DateTime 
     */
    public function getFechaCarga()
    {
        return $this->fechaCarga;
    }

    /**
     * Set usuario
     *
     * @param string $usuario
     * @return MateriaPrimaCompras
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return string 
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set debaja
     *
     * @param boolean $debaja
     * @return MateriaPrimaCompras
     */
    public function setDebaja($debaja)
    {
        $this->debaja = $debaja;

        return $this;
    }

    /**
     * Get debaja
     *
     * @return boolean 
     */
    public function getDebaja()
    {
        return $this->debaja;
    }

    /**
     * Set agencia
     *
     * @param \Hertz\ReservaBundle\Entity\Agencia $agencia
     * @return MateriaPrimaCompras
     */
    public function setAgencia(\Hertz\ReservaBundle\Entity\Agencia $agencia)
    {
        $this->agencia = $agencia;

        return $this;
    }

    /**
     * Get agencia
     *
     * @return \Hertz\ReservaBundle\Entity\Agencia 
     */
    public function getAgencia()
    {
        return $this->agencia;
    }

    /**
     * Set oc
     *
     * @param \Hertz\ReservaBundle\Entity\OrdenCompra $oc
     * @return MateriaPrimaCompras
     */
    public function setOc(\Hertz\ReservaBundle\Entity\OrdenCompra $oc)
    {
        $this->oc = $oc;

        return $this;
    }

    /**
     * Get oc
     *
     * @return \Hertz\ReservaBundle\Entity\OrdenCompra 
     */
    public function getOc()
    {
        return $this->oc;
    }

    /**
     * Set materiaprima
     *
     * @param \Hertz\ReservaBundle\Entity\MateriaPrima $materiaprima
     * @return MateriaPrimaCompras
     */
    public function setMateriaprima(\Hertz\ReservaBundle\Entity\MateriaPrima $materiaprima)
    {
        $this->materiaprima = $materiaprima;

        return $this;
    }

    /**
     * Get materiaprima
     *
     * @return \Hertz\ReservaBundle\Entity\MateriaPrima 
     */
    public function getMateriaprima()
    {
        return $this->materiaprima;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->oc = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add oc
     *
     * @param \Hertz\ReservaBundle\Entity\OrdenCompra $oc
     * @return MateriaPrimaCompras
     */
    public function addOc(\Hertz\ReservaBundle\Entity\OrdenCompra $oc)
    {
        $this->oc[] = $oc;

        return $this;
    }

    /**
     * Remove oc
     *
     * @param \Hertz\ReservaBundle\Entity\OrdenCompra $oc
     */
    public function removeOc(\Hertz\ReservaBundle\Entity\OrdenCompra $oc)
    {
        $this->oc->removeElement($oc);
    }
}
