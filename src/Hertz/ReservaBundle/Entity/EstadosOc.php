<?php
namespace Hertz\ReservaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\DiExtraBundle\Annotation as DI;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Doctrine\ORM\EntityManager;

/**
 * EstadosOc
 *
 * @ORM\Table(name="estadoOc")
 * @ORM\Entity(repositoryClass="EstadosOcRepository")
 */
class EstadosOc
{
    const ORM_ENTITY = "HertzReservaBundle:EstadosOc";
    /**
     *
     * @var integer
     * 
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     *
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=255, nullable=false)
     * @Assert\NotBlank(message="El campo descripción es obligatorio.")
     * @Assert\Length(
     *      min = "2",
     *      max = "255",
     *      minMessage = "Debe tener al menos {{ limit }} caracteres de largo",
     *      maxMessage = "Debe tener un máximo de {{ limit }} caracteres"
     * )
     */
    private $descripcion;    
        
    /**
     *
     * @var datetime
     *
     * @ORM\Column(name="fechaCarga", type="datetime", nullable=false)
     * @Assert\NotBlank(message="El campo fechaCarga es obligatorio.")
     */
    private $fechaCarga;
    
    /**
     *
     * @var string
     *
     * @ORM\Column(name="usuario", type="string", length=255, nullable=false)
     * @Assert\NotBlank(message="El campo usuario es obligatorio.")
     * @Assert\Length(
     *      min = "2",
     *      max = "255",
     *      minMessage = "Debe tener al menos {{ limit }} caracteres de largo",
     *      maxMessage = "Debe tener un máximo de {{ limit }} caracteres"
     * )
     */
    private $usuario;   
    
    /**
     *
     * @var boolean
     *
     * @ORM\Column(name="debaja", type="boolean", nullable=false)
     * @Assert\Choice(choices = {true,false}, message = "Elección incorrecta (Y , S , N).")
     */
    private $debaja;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return EstadosOc
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set fechaCarga
     *
     * @param \DateTime $fechaCarga
     * @return EstadosOc
     */
    public function setFechaCarga($fechaCarga)
    {
        $this->fechaCarga = $fechaCarga;

        return $this;
    }

    /**
     * Get fechaCarga
     *
     * @return \DateTime 
     */
    public function getFechaCarga()
    {
        return $this->fechaCarga;
    }

    /**
     * Set usuario
     *
     * @param string $usuario
     * @return EstadosOc
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return string 
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set debaja
     *
     * @param boolean $debaja
     * @return EstadosOc
     */
    public function setDebaja($debaja)
    {
        $this->debaja = $debaja;

        return $this;
    }

    /**
     * Get debaja
     *
     * @return boolean 
     */
    public function getDebaja()
    {
        return $this->debaja;
    }
}
