<?php
namespace Hertz\ReservaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
//use Acme\DemoBundle\Form\ContactType;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use JMS\SecurityExtraBundle\Annotation as Security;

// these import the "@Route" and "@Template" annotations
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\Serializer\SerializerBuilder;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use JMS\DiExtraBundle\Annotation as DI;
use Hertz\ReservaBundle\Service\VehiculoService;
use Symfony\Component\Validator;


class VehiculoController extends Controller
{
	
	/**
	 *
	 * @var UserService
	 * @DI\Inject("user.manager")
	 */
	private $userSvc;
	
	/**
	 *
	 * @var VehiculoSearchService
	 *
	 * @DI\Inject("VehiculoSearchService.Search")
	 */
	private $searchVehiculoSvc;

        /**
	 *
	 * @var VehiculoSearchService
	 *
	 * @DI\Inject("VehiculoSearchService.getexcell")
	 */
	private $getExcellSvc;
        
        
	/**
	 *
	 * @var VehiculoGetOneService
	 *
	 * @DI\Inject("VehiculoGetOneService.GetOne")
	 */
	private $VehiculoGetOneSvc;
	
	/**
	 *
	 * @var VehiculoCrearService
	 *
	 * @DI\Inject("VehiculoCrearService.Crear")
	 */
	private $VehiculoCrearSvc;
	
	/**
	 *
	 * @var VehiculoGetByService
	 *
	 * @DI\Inject("VehiculoGetByService.GetBy")
	 */
	private $VehiculoGetBySvc;
        
        
	
	public function indexAction()
	{
		return array();
	}

	/**
	 * @Route("/search/")
     * @Method({"GET","POST"})
     * 
     * @ApiDoc(
     * 		description = "La lista de todos los Vehículos",
     * 		requirements = {},
	 *      parameters={}
     * )
	 */
	public function getAllAction(Request $request)
	{
		
		$userx = $this->get("security.context")->getToken()->getUser();
		$esAdmin = $userx->getEsAdmin();
                $userx = $userx->getUsername();
		
		$sucursal = $this->userSvc->getByUserUser($userx);
		
                
		if(!$sucursal)
		{
                        return array("error"=>"no existe el usuario");
		}
		$sucursalx = $sucursal->getSucursal();
                		
		$result = $this->searchVehiculoSvc->getAll($request,$sucursalx,$esAdmin);
		$serializer = SerializerBuilder::create()->build();
		
		$totalRows = $result["total"];
                $totalPaginas = $result["totalPaginas"];
                $totalPaginasEntero = (int) $totalPaginas;
                
                if($totalPaginas > $totalPaginasEntero)
                {
                    $totalPaginas = (int)($totalPaginas + 1);
                }
                
		$arrayResultado = array("current"=>(int)$request->request->get("current"), "rowCount"=>$totalRows,"rows"=>$result["objeto"],"total"=>$totalPaginas);

		$result = $serializer->serialize($arrayResultado, 'json');
                $httpError = 200;
                $response = new Response($result, $httpError);
                //print_r($result);die();
                $response->headers->set('Content-Type', 'application/json');
                return $response;
	}
	
	/**
	 * @Route("/{id}/search/")
	 * @Method({"GET"})
	 *
	 *
	 * @ApiDoc(
	 * 		description = "obtiene un Vehículo en particular",
	 * 		requirements = {},
	 *      parameters={}
	 * )
	 */
	public function getOneAction($id)
	{
		$result = $this->VehiculoGetOneSvc->getOne($id);
		$serializer = SerializerBuilder::create()->build();
		
		if($result == null)
		{
			$result = "Vehículo no encontrado";
			$httpError = 404;
		}
		else
		{
			$httpError = 200;
		}
		$result = $serializer->serialize($result, 'json');
		$response = new Response($result, $httpError);
		$response->headers->set('Content-Type', 'application/json');
		return $response;
	}
        
        
        /**
	 * @Route("/searchby/")
	 * @Method({"GET"})
	 *
	 * @ApiDoc(
	 * 		description = "obtiene una lista de vehículos por descripcion o patente",
	 * 		requirements = {{
	 *         "name"="term",
	 *         "dataType"="string",
	 *         "requirement"="\d+",
	 *         "description"="variable"
	 *			}},
	 *      parameters={}
	 * )
	 */
	public function getByAction(Request $request)
	{
		$word = $request->query->get("term");
                $fRSalida = $request->query->get("fechaHoraSalida");
                $fRDevolucion = $request->query->get("fechaHoraDevolucion");
                $horaSalida = $request->query->get("HoraSalida");
                $horaDevolucion = $request->query->get("HoraDevolucion");
                $categoria = $request->query->get("categoriaVeh");
                //echo $horaSalida;die();
                //print_r($request->query);die();
		$result = $this->VehiculoGetBySvc->getBy($word,$fRSalida,$fRDevolucion,$horaSalida,$horaDevolucion,$categoria);
                
		$serializer = SerializerBuilder::create()->build();
                $result = $serializer->serialize($result, 'json');
                
                
                $httpError = 200;
                $response = new Response($result, $httpError);
                $response->headers->set('Content-Type', 'application/json');
                return $response;
	}
        
        
        /**
	 * @Route("/searchbysinfecha/")
	 * @Method({"GET"})
	 *
	 * @ApiDoc(
	 * 		description = "obtiene una lista de vehículos por descripcion o patente",
	 * 		requirements = {{
	 *         "name"="term",
	 *         "dataType"="string",
	 *         "requirement"="\d+",
	 *         "description"="variable"
	 *			}},
	 *      parameters={}
	 * )
	 */
	public function getBySinFechaAction(Request $request)
	{
		$word = $request->query->get("term");
                //echo $horaSalida;die();
		$result = $this->VehiculoGetBySvc->getBySinFecha($word);
                
		$serializer = SerializerBuilder::create()->build();
                $result = $serializer->serialize($result, 'json');
                
                
                $httpError = 200;
                $response = new Response($result, $httpError);
                $response->headers->set('Content-Type', 'application/json');
                return $response;
	}
	
        
	/**
	 * @Route("/crear/", name="vehiculo_crear")
	 * 
	 * @ApiDoc(
	 * 		description = "crea un Vehículo nuevo",
	 * 		
	 * 		requirements = {
	 *			{
	 *         		"name"="descripcion",
	 *         		"dataType"="string",
	 *         		"requirement"="string",
	 *         		"description"="descripción del vehículo"
	 *			},
	 *			{
	 *         		"name"="modelo",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="modelo del vehículo"
	 *			},
	 *			{
	 *         		"name"="kilometros",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="kilometros recorridos por el vehículo"
	 *			},
	 *			{
	 *         		"name"="agencia",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="id de la agencia de origen del vehículo"
	 *			},
	 *			{
	 *         		"name"="modeloCal",
	 *         		"dataType"="string",
	 *         		"requirement"="string(4)",
	 *         		"description"="*********Desconocido hasta el momento, aguardando info funcional"
	 *			},
	 *			{
	 *         		"name"="fechaHoraDevolucion",
	 *         		"dataType"="datetime",
	 *         		"requirement"="dd/mm/yy hh:mm:ss",
	 *         		"description"="Fecha de devolución efectiva del vehículo"
	 *			},
	 *			{
	 *         		"name"="fechaHoraSalida",
	 *         		"dataType"="datetime",
	 *         		"requirement"="dd/mm/yy hh:mm:ss",
	 *         		"description"="Fecha y hora de salida programada"
	 *			}
	 *			,
	 *			{
	 *         		"name"="fechaHoraRetorno",
	 *         		"dataType"="datetime",
	 *         		"requirement"="dd/mm/yy hh:mm:ss",
	 *         		"description"="Fecha y hora de retorno programado"
	 *			},
	 *			{
	 *				"name"="dominio",
	 *				"dataType"="string",
	 *				"requirement"="string(10)",
	 *				"description"="Dominio/Patente del vehículo"
	 *			},
	 *			{
	 *				"name"="estado",
	 *				"dataType"="integer",
	 *				"requirement"="\d+",
	 *				"description"="Estado del vehículo"
	 *			}
	 *		},
	 *      parameters={}
	 * )
	 * @Template()
	 */
	public function crearAction(Request $request)
	{
		
                
                $validator = $this->get('validator');
		$result = $this->VehiculoCrearSvc->crear($request,$validator);
		$serializer = SerializerBuilder::create()->build();
		$result = $serializer->serialize($result, 'json');
		$httpError = 200;
		$response = new Response($result, $httpError);
		$response->headers->set('Content-Type', 'application/json');
		return $response;
	}
	
	
	/**
	 * @Route("/editar/", name="vehiculo_editar")
	 *
	 *
	 * @ApiDoc(
	 * 		description = "Edita un Vehículo",
	 *
	 * 		requirements = {
	 *			{
	 *         		"name"="descripcion",
	 *         		"dataType"="string",
	 *         		"requirement"="string",
	 *         		"description"="descripción del vehículo"
	 *			},
	 *			{
	 *         		"name"="modelo",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="modelo del vehículo"
	 *			},
	 *			{
	 *         		"name"="kilometros",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="kilometros recorridos por el vehículo"
	 *			},
	 *			{
	 *         		"name"="agencia",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="id de la agencia de origen del vehículo"
	 *			},
	 *			{
	 *         		"name"="modelo_cal",
	 *         		"dataType"="string",
	 *         		"requirement"="string(4)",
	 *         		"description"="*********Desconocido hasta el momento, aguardando info funcional"
	 *			},
	 *			{
	 *         		"name"="fechaHoraDevolucion",
	 *         		"dataType"="datetime",
	 *         		"requirement"="dd/mm/yy hh:mm:ss",
	 *         		"description"="Fecha de devolución efectiva del vehículo"
	 *			},
	 *			{
	 *         		"name"="fechaHoraSalida",
	 *         		"dataType"="datetime",
	 *         		"requirement"="dd/mm/yy hh:mm:ss",
	 *         		"description"="Fecha y hora de salida programada"
	 *			}
	 *			,
	 *			{
	 *         		"name"="fechaHoraRetorno",
	 *         		"dataType"="datetime",
	 *         		"requirement"="dd/mm/yy hh:mm:ss",
	 *         		"description"="Fecha y hora de retorno programado"
	 *			},
	 *			{
	 *				"name"="estado",
	 *				"dataType"="integer",
	 *				"requirement"="\d+",
	 *				"description"="Estado del vehículo"
	 *			},
	 *			{
	 *				"name"="dominio",
	 *				"dataType"="string",
	 *				"requirement"="string(10)",
	 *				"description"="Dominio/Patente del vehículo"
	 *			},
	 *			{
	 *         		"name"="debaja",
	 *         		"dataType"="boolean",
	 *         		"requirement"="string",
	 *         		"description"="True está dado de baja"
	 *			},
	 *			{
	 *         		"name"="id",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="Id del vehículo a modificar"
	 *			}
	 *		},
	 *      parameters={}
	 * )
	 * @Template()
	 */
	public function editarAction(Request $request)
	{
		$validator = $this->get('validator');
		
                
		$user = $this->get("security.context")->getToken()->getUser();
                
                if($request->request->get("oper") == "add")
                {
                    $validator = $this->get('validator');
                    $result = $this->VehiculoCrearSvc->crear($request,$validator,$user);
                    $serializer = SerializerBuilder::create()->build();
                    $result = $serializer->serialize($result, 'json');
                    $httpError = 200;
                    $response = new Response($result, $httpError);
                    $response->headers->set('Content-Type', 'application/json');
                    return $response;
                }
		else
                {
                    $result = $this->VehiculoCrearSvc->editar($request,$validator,$user);
                    $serializer = SerializerBuilder::create()->build();
                    if($result == null)
                    {
                            $result = "Vehículo no encontrado";
                            $httpError = 404;
                    }
                    else
                    {
                            $httpError = 200;
                    }
                    $result = $serializer->serialize($result, 'json');
                    $response = new Response($result, $httpError);
                    $response->headers->set('Content-Type', 'application/json');
                    return $response;
                }
	}

	/**
            * @Route("/getexcell/")
            * @Method({"GET","POST"})
            * 
            * @ApiDoc(
            * 		description = "La lista de todos los Vehículos",
            * 		requirements = {},
            *      parameters={}
            * )
	 */
	public function getAllExcellAction(Request $request)
	{
		
		$userx = $this->get("security.context")->getToken()->getUser();
		$userx = $userx->getUsername();
		
		$sucursal = $this->userSvc->getByUserUser($userx);
		
		if(!$sucursal)
		{
                        return array("error"=>"no existe el usuario");
		}
		$sucursalx = $sucursal->getSucursal();
                		
		$result = $this->getExcellSvc->getAllExcell($request,$sucursalx);
		$serializer = SerializerBuilder::create()->build();
		

		$result = $serializer->serialize($result, 'json');
                $httpError = 200;
                $response = new Response($result, $httpError);
                
                $response->headers->set('Content-Type', 'application/json');
                return $response;
	}
}