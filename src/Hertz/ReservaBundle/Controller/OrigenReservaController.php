<?php
namespace Hertz\ReservaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use JMS\SecurityExtraBundle\Annotation as Security;
// these import the "@Route" and "@Template" annotations
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\Serializer\SerializerBuilder;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use JMS\DiExtraBundle\Annotation as DI;
use Hertz\ReservaBundle\Service\OrigenReservaService;
use Symfony\Component\Validator;

class OrigenReservaController extends Controller
{
	
	/**
	 *
	 * @var OrigenReservaSearchService
	 *
	 * @DI\Inject("OrigenReservaSearchService.Search")
	 */
	private $searchOrigenReservaSvc;
		
	/**
	 *
	 * @var OrigenReservaGetOneService
	 *
	 * @DI\Inject("OrigenReservaGetOneService.GetOne")
	 */
	private $OrigenReservaGetOneSvc;
	

	
	public function indexAction()
	{
		return array();
	}

	/**
	 * @Route("/search/")
     * @Method({"GET","POST"})
     * 
     * @ApiDoc(
     * 		description = "La lista de todos los tipos de origenReserva",
     * 		requirements = {},
	 *      parameters={}
     * )
	 */
	public function getAllAction(Request $request)
	{
            
            
            $idPadre = $request->query->get("idOrigen");
            $result = $this->searchOrigenReservaSvc->getAll($idPadre);
            $serializer = SerializerBuilder::create()->build();
            $result = $serializer->serialize($result, 'json');
            $httpError = 200;
            $response = new Response($result, $httpError);
            $response->headers->set('Content-Type', 'application/json');
            return $response;
	}
	
        
        
	/**
	 * @Route("/{id}/search/")
	 * @Method({"GET"})
	 *
	 * @ApiDoc(
	 * 		description = "obtiene un accesorio en particular",
	 * 		requirements = {},
	 *      parameters={}
	 * )
	 */
	public function getOneAction($id)
	{
		$result = $this->OrigenReservaGetOneSvc->getOne($id);
		$serializer = SerializerBuilder::create()->build();
		
		if($result == null)
		{
			$result = "Accesorio no encontrado";
			$httpError = 404;
		}
		else
		{
			$httpError = 200;
		}
		$result = $serializer->serialize($result, 'json');
		$response = new Response($result, $httpError);
		$response->headers->set('Content-Type', 'application/json');
		return $response;
	}
	
	
}