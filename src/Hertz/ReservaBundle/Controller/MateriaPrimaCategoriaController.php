<?php
/**
 * Created by PhpStorm.
 * User: hfuentesbx
 * Date: 28/01/2017
 * Time: 02:11 PM
 */

namespace Hertz\ReservaBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
//use Acme\DemoBundle\Form\ContactType;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use JMS\SecurityExtraBundle\Annotation as Security;

// these import the "@Route" and "@Template" annotations
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\Serializer\SerializerBuilder;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use JMS\DiExtraBundle\Annotation as DI;
use Hertz\ReservaBundle\Service\MarcaService;
use Symfony\Component\Validator;

class MateriaPrimaCategoriaController extends Controller
{
    /**
     *
     * @var materiaPrimaCategoriaSearchService
     *
     * @DI\Inject("MateriaPrimaCategoriaSearchService.Search")
     */
    private $searchMateriaPrimaCategoriaSvc;

    /**
     *
     * @var materiaPrimaCategoriaGetOneService
     *
     * @DI\Inject("MateriaPrimaCategoriaGetOneService.GetOne")
     */
    private $materiaPrimaCategoriaGetOneSvc;

    /**
     *
     * @var materiaPrimaCategoriaCrearService
     *
     * @DI\Inject("MateriaPrimaCategoriaCrearService.Crear")
     */
    private $materiaPrimaCategoriaCrearSvc;


    public function indexAction()
    {
        return array();
    }

    /**
     * @Route("/search/")
     * @Method({"GET","POST"})
     *
     * @ApiDoc(
     * 		description = "La lista de todos los tipos de MateriaPrimaCategoia",
     * 		requirements = {},
     *      parameters={}
     * )
     */
    public function getAllAction()
    {
        $result = $this->searchMateriaPrimaCategoriaSvc->getAll();
        $serializer = SerializerBuilder::create()->build();
        $result = $serializer->serialize($result, 'json');
        $httpError = 200;
        $response = new Response($result, $httpError);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @Route("/{id}/search/")
     * @Method({"GET"})
     *
     * @ApiDoc(
     * 		description = "Obtiene una MateriaPrimaCategoia en particular",
     * 		requirements = {},
     *      parameters={}
     * )
     */
    public function getOneAction($id)
    {
        $result = $this->materiaPrimaCategoriaGetOneSvc->getOne($id);
        $serializer = SerializerBuilder::create()->build();

        if($result == null)
        {
            $result = "MateriaPrimaCategoia no encontrada";
            $httpError = 404;
        }
        else
        {
            $httpError = 200;
        }
        $result = $serializer->serialize($result, 'json');
        $response = new Response($result, $httpError);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @Route("/crear/", name="MateriaPrimaCategoia_crear")
     * @Method({"GET","POST"})
     * @ApiDoc(
     * 		description = "Crea una MateriaPrimaCategoia",
     *
     * 		requirements = {
     * 			{
     *         		"name"="nombre",
     *         		"dataType"="string",
     *         		"requirement"="string",
     *         		"description"="Nombre del tipo de MateriaPrimaCategoia"
     *			},
     *		},
     *      parameters={}
     * )
     * @Template()
     */
    public function crearAction(Request $request)
    {
        $validator = $this->get('validator');
        $user = $this->get("security.context")->getToken()->getUser();
        $result = $this->materiaPrimaCategoriaCrearSvc->crear($request,$validator,$user);
        $serializer = SerializerBuilder::create()->build();
        $result = $serializer->serialize($result, 'json');
        $httpError = 200;
        $response = new Response($result, $httpError);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }


    /**
     * @Route("/editar/", name="MateriaPrimaCategoia_editar")
     * @Method({"GET","POST"})
     * @ApiDoc(
     * 		description = "Edita una MateriaPrimaCategoia",
     * 		requirements = {
     *			{
     *         		"name"="id",
     *         		"dataType"="integer",
     *         		"requirement"="\d+",
     *         		"description"="id de la MateriaPrimaCategoia"
     *			},{
     *         		"name"="nombre",
     *         		"dataType"="string",
     *         		"requirement"="string",
     *         		"description"="nombre del tipo de MateriaPrimaCategoia"
     *			},
     *			{
     *         		"name"="debaja",
     *         		"dataType"="boolean",
     *         		"requirement"="string S o N",
     *         		"description"="S está dado de baja"
     *			},	 *
     *		},
     *      parameters={}
     * )
     * @Template()
     */
    public function editarAction(Request $request)
    {
        $validator = $this->get('validator');
        $user = $this->get("security.context")->getToken()->getUser();
        $result = $this->materiaPrimaCategoriaCrearSvc->editar($request,$validator,$user);
        $serializer = SerializerBuilder::create()->build();
        if($result == null)
        {
            $result = "MateriaPrimaCategoia o encontrada";
            $httpError = 404;
        }
        else
        {
            $httpError = 200;
        }
        $result = $serializer->serialize($result, 'json');
        $response = new Response($result, $httpError);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
}