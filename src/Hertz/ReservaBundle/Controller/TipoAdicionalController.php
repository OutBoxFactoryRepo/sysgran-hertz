<?php
namespace Hertz\ReservaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
//use Acme\DemoBundle\Form\ContactType;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

// these import the "@Route" and "@Template" annotations
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\Serializer\SerializerBuilder;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use JMS\DiExtraBundle\Annotation as DI;
use Hertz\ReservaBundle\Service\TipoAdicionalService;
use Symfony\Component\Validator;

class TipoAdicionalController extends Controller
{
	
	/**
	 *
	 * @var TipoAdicionalSearchService
	 *
	 * @DI\Inject("TipoAdicionalSearchService.Search")
	 */
	private $searchTipoAdicionalSvc;
		
	/**
	 *
	 * @var TipoAdicionalGetOneService
	 *
	 * @DI\Inject("TipoAdicionalGetOneService.GetOne")
	 */
	private $TipoAdicionalGetOneSvc;
	
	/**
	 *
	 * @var TipoAdicionalCrearService
	 *
	 * @DI\Inject("TipoAdicionalCrearService.Crear")
	 */
	private $TipoAdicionalCrearSvc;
	
	
	public function indexAction()
	{
		return array();
	}

	/**
	 * @Route("/search/")
     * @Method({"GET","POST"})
     * 
     * @ApiDoc(
     * 		description = "La lista de todos los tipos de adicionales",
     * 		requirements = {},
	 *      parameters={}
     * )
	 */
	public function getAllAction()
	{
		$result = $this->searchTipoAdicionalSvc->getAll();
		$serializer = SerializerBuilder::create()->build();
    	$result = $serializer->serialize($result, 'json');
    	$httpError = 200;
    	$response = new Response($result, $httpError);
    	$response->headers->set('Content-Type', 'application/json');
    	return $response;
	}
	
	/**
	 * @Route("/{id}/search/")
	 * @Method({"GET"})
	 *
	 * @ApiDoc(
	 * 		description = "obtiene un tipo de adicional en particular",
	 * 		requirements = {},
	 *      parameters={}
	 * )
	 */
	public function getOneAction($id)
	{
		$result = $this->TipoAdicionalGetOneSvc->getOne($id);
		$serializer = SerializerBuilder::create()->build();
		
		if($result == null)
		{
			$result = "Tipo de adicional no encontrado";
			$httpError = 404;
		}
		else
		{
			$httpError = 200;
		}
		$result = $serializer->serialize($result, 'json');
		$response = new Response($result, $httpError);
		$response->headers->set('Content-Type', 'application/json');
		return $response;
	}
	
	/**
	 * @Route("/crear/", name="tipo_adicional_crear")
	 *
	 * @ApiDoc(
	 * 		description = "crea un Tipo de Adicional nuevo",
	 * 		
	 * 		requirements = {
	 *			{
	 *         		"name"="descripcion",
	 *         		"dataType"="string",
	 *         		"requirement"="string",
	 *         		"description"="descripción del tipo de adicional"
	 *			},
	 *			{
	 *         		"name"="usuario",
	 *         		"dataType"="string",
	 *         		"requirement"="string",
	 *         		"description"="usuario logueado"
	 *			}
	 *		},
	 *      parameters={}
	 * )
	 * @Template()
	 */
	public function crearAction(Request $request)
	{
		$validator = $this->get('validator');
		$result = $this->TipoAdicionalCrearSvc->crear($request,$validator);
		$serializer = SerializerBuilder::create()->build();
		$result = $serializer->serialize($result, 'json');
		$httpError = 200;
		$response = new Response($result, $httpError);
		$response->headers->set('Content-Type', 'application/json');
		return $response;
	}
	
	
	/**
	 * @Route("/{id}/editar/", name="tipo_adicional_editar")
	 *
	 * @ApiDoc(
	 * 		description = "Edita un tipo de adicional",
	 *
	 * 		requirements = {
	 *			{
	 *         		"name"="descripcion",
	 *         		"dataType"="string",
	 *         		"requirement"="string",
	 *         		"description"="descripción del tipo de adicional"
	 *			},
	 *			{
	 *         		"name"="usuario",
	 *         		"dataType"="string",
	 *         		"requirement"="string",
	 *         		"description"="usuario logueado"
	 *			},
	 *			{
	 *         		"name"="debaja",
	 *         		"dataType"="boolean",
	 *         		"requirement"="string",
	 *         		"description"="True está dado de baja"
	 *			}
	 *		},
	 *      parameters={}
	 * )
	 * @Template()
	 */
	public function editarAction(Request $request,$id)
	{
		$validator = $this->get('validator');
		$result = $this->TipoAdicionalCrearSvc->editar($request,$id,$validator);
		$serializer = SerializerBuilder::create()->build();
		if($result == null)
		{
			$result = "Tipo de adicional no encontrado";
			$httpError = 404;
		}
		else
		{
			$httpError = 200;
		}
		$result = $serializer->serialize($result, 'json');
		$response = new Response($result, $httpError);
		$response->headers->set('Content-Type', 'application/json');
		return $response;
	}
	
}