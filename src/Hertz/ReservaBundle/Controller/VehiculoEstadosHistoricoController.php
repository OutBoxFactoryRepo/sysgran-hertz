<?php
namespace Hertz\ReservaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
//use Acme\DemoBundle\Form\ContactType;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use JMS\SecurityExtraBundle\Annotation as Security;

// these import the "@Route" and "@Template" annotations
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\Serializer\SerializerBuilder;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use JMS\DiExtraBundle\Annotation as DI;
use Hertz\ReservaBundle\Service\VehiculoEstadosHistoricoService;
use Symfony\Component\Validator;

class VehiculoEstadosHistoricoController extends Controller
{
	
	/**
	 *
	 * @var UserService
	 * @DI\Inject("user.manager")
	 */
	private $userSvc;
	
	/**
	 *
	 * @var VehiculoEstadosHistoricoSearchService
	 *
	 * @DI\Inject("VehiculoEstadosHistoricoSearchService.Search")
	 */
	private $searchVehiculoEstadosHistoricoSvc;
		
	/**
	 *
	 * @var VehiculoEstadosHistoricoGetOneService
	 *
	 * @DI\Inject("VehiculoEstadosHistoricoGetOneService.GetOne")
	 */
	private $VehiculoEstadosHistoricoGetOneSvc;
	
	/**
	 *
	 * @var VehiculoEstadosHistoricoCrearService
	 *
	 * @DI\Inject("VehiculoEstadosHistoricoCrearService.Crear")
	 */
	private $VehiculoEstadosHistoricoCrearSvc;
	
	
	public function indexAction()
	{
		return array();
	}

	/**
	 * @Route("/search/")
         * @Method({"GET","POST"})
         * 
         * @ApiDoc(
         * 		description = "La lista de todos los tipos de VehiculoEstadosHistoricoes",
         * 		requirements = {},
	 *      parameters={}
         * )
	 */
	public function getAllAction(Request $request)
	{
                $userx = $this->get("security.context")->getToken()->getUser();
		//$userx = $userx->getId();
                
                
                $result = $this->searchVehiculoEstadosHistoricoSvc->getAll($request,$userx);
                
                /*
                $totalRows = $result["total"];
                $totalPaginas = $result["totalPaginas"];
                $totalPaginasEntero = (int) $totalPaginas;
                
                if($totalPaginas > $totalPaginasEntero)
                {
                    $totalPaginas = (int)($totalPaginas + 1);
                }
                //echo "total: ".$totalPaginas;die();
                $arrayResultado = array("current"=>(int)$request->request->get("current"), "rowCount"=>$totalRows,"rows"=>$result["objeto"],"total"=>$totalPaginas);
                
		*/
                $serializer = SerializerBuilder::create()->build();
		$result = $serializer->serialize($result, 'json');
                $httpError = 200;
                $response = new Response($result, $httpError);
                $response->headers->set('Content-Type', 'application/json');
                return $response;
	}
	
	
	
        
	/**
	 * @Route("/{id}/search/")
	 * @Method({"GET"})
	 *
	 * @ApiDoc(
	 * 		description = "obtiene un VehiculoEstadosHistorico en particular",
	 * 		requirements = {},
	 *      parameters={}
	 * )
	 */
	public function getOneAction($id)
	{
		$result = $this->VehiculoEstadosHistoricoGetOneSvc->getOne($id);
		$serializer = SerializerBuilder::create()->build();
		
		if($result == null)
		{
			$result = "VehiculoEstadosHistorico no encontrado";
			$httpError = 404;
		}
		else
		{
			$httpError = 200;
		}
		$result = $serializer->serialize($result, 'json');
		$response = new Response($result, $httpError);
		$response->headers->set('Content-Type', 'application/json');
		return $response;
	}
	
	/**
	 * @Route("/crear/", name="VehiculoEstadosHistorico_crear")
	 * @Method({"GET","POST"})
	 * @ApiDoc(
	 * 		description = "crea un VehiculoEstadosHistorico",
	 * 		
	 * 		requirements = {
	 * 			{
	 *         		"name"="idVehiculo",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="Id del vehículo"
	 *			},
	 *			{
	 *         		"name"="idEstadoVehiculo",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="Estado del vehículo"
	 *			}
	 *		},
	 *      parameters={}
	 * )
	 * @Template()
	 */
	public function crearAction(Request $request)
	{
		$validator = $this->get('validator');
		$user = $this->get("security.context")->getToken()->getUser();
                
                $usuario = $this->userSvc->getByIdUser($user->getId());
                
                
		$result = $this->VehiculoEstadosHistoricoCrearSvc->crear($request,$validator,$usuario);
		$serializer = SerializerBuilder::create()->build();
		$result = $serializer->serialize($result, 'json');
		$httpError = 200;
		$response = new Response($result, $httpError);
		$response->headers->set('Content-Type', 'application/json');
		return $response;
	}
	
	
	
}