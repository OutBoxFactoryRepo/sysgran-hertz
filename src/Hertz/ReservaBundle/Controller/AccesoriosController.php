<?php
namespace Hertz\ReservaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use JMS\SecurityExtraBundle\Annotation as Security;
// these import the "@Route" and "@Template" annotations
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\Serializer\SerializerBuilder;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use JMS\DiExtraBundle\Annotation as DI;
use Hertz\ReservaBundle\Service\AccesoriosService;
use Symfony\Component\Validator;

class AccesoriosController extends Controller
{
	
	/**
	 *
	 * @var AccesoriosSearchService
	 *
	 * @DI\Inject("AccesoriosSearchService.Search")
	 */
	private $searchAccesoriosSvc;
		
	/**
	 *
	 * @var AccesoriosGetOneService
	 *
	 * @DI\Inject("AccesoriosGetOneService.GetOne")
	 */
	private $AccesoriosGetOneSvc;
	
	/**
	 *
	 * @var AccesoriosCrearService
	 *
	 * @DI\Inject("AccesoriosCrearService.Crear")
	 */
	private $AccesoriosCrearSvc;
	
	
	public function indexAction()
	{
		return array();
	}

	/**
	 * @Route("/search/")
     * @Method({"GET","POST"})
     * 
     * @ApiDoc(
     * 		description = "La lista de todos los tipos de accesorios",
     * 		requirements = {},
	 *      parameters={}
     * )
	 */
	public function getAllAction()
	{
            $result = $this->searchAccesoriosSvc->getAll();
            $serializer = SerializerBuilder::create()->build();
            $result = $serializer->serialize($result, 'json');
            $httpError = 200;
            $response = new Response($result, $httpError);
            $response->headers->set('Content-Type', 'application/json');
            return $response;
	}
	
        
        
        
        /**
            * @Route("/addaccesoriosveh/")
            * @Method({"GET","POST"})
            * 
            * @ApiDoc(
            * 		description = "inserta un accesorio a un vehículo",
            * 		requirements = {},
                *      parameters={}
            * )
	 */
	public function addVehiculoAccAction(Request $request)
	{
		$result = $this->searchAccesoriosSvc->addVehiculoAcc($request);
		$serializer = SerializerBuilder::create()->build();
                $result = $serializer->serialize($result, 'json');
                $httpError = 200;
                $response = new Response($result, $httpError);
                $response->headers->set('Content-Type', 'application/json');
                return $response;
	}
        
        
        /**
            * @Route("/getaccesoriosveh/")
            * @Method({"GET","POST"})
            * 
            * @ApiDoc(
            * 		description = "La lista de todos los tipos de accesorios para un vehículo dado",
            * 		requirements = {},
                *      parameters={}
            * )
	 */
	public function getAllByVehiculoAction(Request $request)
	{
		$result = $this->searchAccesoriosSvc->getAllByVehiculo($request);
		$serializer = SerializerBuilder::create()->build();
                $result = $serializer->serialize($result, 'json');
                $httpError = 200;
                $response = new Response($result, $httpError);
                $response->headers->set('Content-Type', 'application/json');
                return $response;
	}
        
	/**
	 * @Route("/{id}/search/")
	 * @Method({"GET"})
	 *
	 * @ApiDoc(
	 * 		description = "obtiene un accesorio en particular",
	 * 		requirements = {},
	 *      parameters={}
	 * )
	 */
	public function getOneAction($id)
	{
		$result = $this->AccesoriosGetOneSvc->getOne($id);
		$serializer = SerializerBuilder::create()->build();
		
		if($result == null)
		{
			$result = "Accesorio no encontrado";
			$httpError = 404;
		}
		else
		{
			$httpError = 200;
		}
		$result = $serializer->serialize($result, 'json');
		$response = new Response($result, $httpError);
		$response->headers->set('Content-Type', 'application/json');
		return $response;
	}
	
	/**
	 * @Route("/crear/", name="accesorios_crear")
	 * @Method({"GET","POST"})
	 * @ApiDoc(
	 * 		description = "crea un accesorio", 		
	 * 		requirements = {
	 * 			{
	 *         		"name"="descripcion",
	 *         		"dataType"="string",
	 *         		"requirement"="string",
	 *         		"description"="descripción del tipo de accesorio"
	 *			},
	 *		},
	 *      parameters={}
	 * )
	 * @Template()
	 */
	public function crearAction(Request $request)
	{
		$validator = $this->get('validator');
		$user = $this->get("security.context")->getToken()->getUser();
		$result = $this->AccesoriosCrearSvc->crear($request,$validator,$user);
		$serializer = SerializerBuilder::create()->build();
		$result = $serializer->serialize($result, 'json');
		$httpError = 200;
		$response = new Response($result, $httpError);
		$response->headers->set('Content-Type', 'application/json');
		return $response;
	}
	
	
	/**
	 * @Route("/editar/", name="accesorio_editar")
	 * @Method({"GET","POST"})
	 * @ApiDoc(
	 * 		description = "Edita un accesorio",
	 * 		requirements = {
	 *			{
	 *         		"name"="id",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="id del accesorio"
	 *			},
	 * 			{
	 *         		"name"="descripcion",
	 *         		"dataType"="string",
	 *         		"requirement"="string",
	 *         		"description"="descripción del tipo de accesorio"
	 *			},
	 *			{
	 *         		"name"="debaja",
	 *         		"dataType"="boolean",
	 *         		"requirement"="string S o N",
	 *         		"description"="S está dado de baja"
	 *			},	 
	 *		},
	 *      parameters={}
	 * )
	 * @Template()
	 */
	public function editarAction(Request $request)
	{
		$validator = $this->get('validator');
		$user = $this->get("security.context")->getToken()->getUser();

		$result = $this->AccesoriosCrearSvc->editar($request,$validator,$user);
		$serializer = SerializerBuilder::create()->build();
		if($result == null)
		{
			$result = "Accesorio no encontrado";
			$httpError = 404;
		}
		else
		{
			$httpError = 200;
		}
		$result = $serializer->serialize($result, 'json');
		$response = new Response($result, $httpError);
		$response->headers->set('Content-Type', 'application/json');
		return $response;
	}
	
}