<?php
namespace Hertz\ReservaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
//use Acme\DemoBundle\Form\ContactType;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use JMS\SecurityExtraBundle\Annotation as Security;

// these import the "@Route" and "@Template" annotations
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\Serializer\SerializerBuilder;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use JMS\DiExtraBundle\Annotation as DI;
use Hertz\ReservaBundle\Service\OrdenTrabajoReparacionesService;
use Symfony\Component\Validator;

class OrdenTrabajoReparacionesController extends Controller
{
	
	/**
	 *
	 * @var UserService
	 * @DI\Inject("user.manager")
	 */
	private $userSvc;
	
	/**
	 *
	 * @var OrdenTrabajoReparacionesSearchService
	 *
	 * @DI\Inject("OrdenTrabajoReparacionesSearchService.Search")
	 */
	private $searchOrdenTrabajoReparacionesSvc;
		
	/**
	 *
	 * @var OrdenTrabajoReparacionesGetOneService
	 *
	 * @DI\Inject("OrdenTrabajoReparacionesGetOneService.GetOne")
	 */
	private $OrdenTrabajoReparacionesGetOneSvc;
	
	/**
	 *
	 * @var OrdenTrabajoReparacionesCrearService
	 *
	 * @DI\Inject("OrdenTrabajoReparacionesCrearService.Crear")
	 */
	private $OrdenTrabajoReparacionesCrearSvc;
	
	
	public function indexAction()
	{
		return array();
	}

	/**
	 * @Route("/search/")
     * @Method({"GET","POST"})
     * 
     * @ApiDoc(
     * 		description = "La lista de todos los tipos de OrdenTrabajoReparacioneses",
     * 		requirements = {},
	 *      parameters={}
     * )
	 */
	public function getAllAction(Request $request)
	{

		$result = $this->searchOrdenTrabajoReparacionesSvc->getAll($request);
		$serializer = SerializerBuilder::create()->build();
    	$totalRows = $result["total"];

    	$arrayResultado = array("success"=>true,"results"=>$totalRows,"rows"=>$result["objeto"]);
    	
		$result = $serializer->serialize($arrayResultado, 'json');
    	$httpError = 200;
    	$response = new Response($result, $httpError);
    	$response->headers->set('Content-Type', 'application/json');
    	return $response;
	}
	
	
	/**
	 * @Route("/searchcombo/")
     * @Method({"GET","POST"})
     * 
     * @ApiDoc(
     * 		description = "La lista de todos los tipos de OrdenTrabajoReparacioneses por usuario",
     * 		requirements = {},
	 *      parameters={}
     * )
	 */
	public function getAllComboAction(Request $request)
	{
		$userx = $this->get("security.context")->getToken()->getUser();
		$userx = $userx->getUsername();
		
		$sucursal = $this->userSvc->getByUserUser($userx);
		
		if(!$sucursal)
		{
			echo "no existe el usuario";die();
		}
		$sucursalx = $sucursal->getSucursal();
		
		$result = $this->searchOrdenTrabajoReparacionesSvc->getAllCombo($request,$sucursalx);
		$serializer = SerializerBuilder::create()->build();


    	$arrayResultado = $result;
    	
		$result = $serializer->serialize($arrayResultado, 'json');
    	$httpError = 200;
    	$response = new Response($result, $httpError);
    	$response->headers->set('Content-Type', 'application/json');
    	return $response;
	}
	
	
	/**
	 * @Route("/{id}/search/")
	 * @Method({"GET"})
	 *
	 * @ApiDoc(
	 * 		description = "obtiene un OrdenTrabajoReparaciones en particular",
	 * 		requirements = {},
	 *      parameters={}
	 * )
	 */
	public function getOneAction($id)
	{
		$result = $this->OrdenTrabajoReparacionesGetOneSvc->getOne($id);
		$serializer = SerializerBuilder::create()->build();
		
		if($result == null)
		{
			$result = "Reparaciones no encontradadas";
			$httpError = 404;
		}
		else
		{
			$httpError = 200;
		}
		$result = $serializer->serialize($result, 'json');
		$response = new Response($result, $httpError);
		$response->headers->set('Content-Type', 'application/json');
		return $response;
	}
	
	/**
	 * @Route("/crear/", name="OrdenTrabajoReparaciones_crear")
	 * @Method({"GET","POST"})
	 * @ApiDoc(
	 * 		description = "crea un OrdenTrabajoReparaciones",
	 * 		
	 * 		requirements = {
	 * 			{
	 *         		"name"="descripcion",
	 *         		"dataType"="string",
	 *         		"requirement"="string",
	 *         		"description"="descripción del tipo de OrdenTrabajoReparaciones"
	 *			},
	 *			{
	 *         		"name"="fechaCarga",
	 *         		"dataType"="datetime",
	 *         		"requirement"="dd/mm/yyyy",
	 *         		"description"="fecha de carga del OrdenTrabajoReparaciones"
	 *			},
	 *			{
	 *         		"name"="idReparacion",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="id de la reparación"
	 *			},
	 *			{
	 *         		"name"="idOt",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="id de la OT"
	 *			},
	 *			{
	 *         		"name"="debaja",
	 *         		"dataType"="boolean",
	 *         		"requirement"="string S o N",
	 *         		"description"="S está dado de baja"
	 *			}
	 *		},
	 *      parameters={}
	 * )
	 * @Template()
	 */
	public function crearAction(Request $request)
	{

		$validator = $this->get('validator');
		$user = $this->get("security.context")->getToken()->getUser();
		$serializer = SerializerBuilder::create()->build();
                
                $oper = $request->request->get("oper");
                
                //print_r($request->request);die();
                
                if($oper=="edit")
                {
                    
                    $result = $this->OrdenTrabajoReparacionesCrearSvc->editar($request,$validator,$user);
                }
                elseif($oper=="del")
                {
                    $result = $this->OrdenTrabajoReparacionesCrearSvc->delete($request,$validator,$user);
                }
                else
                {
                    $result = $this->OrdenTrabajoReparacionesCrearSvc->crear($request,$validator,$user);
                }

		$result = $serializer->serialize($result, 'json');
                
                /*          
                $result = '{
	"error": "Integer should be a positive"
                }';
                
                */
                $httpError = 200;
                $response = new Response($result, $httpError);
                $response->headers->set('Content-Type', 'application/json');
                return $response;		
	}
	
	
	/**
	 * @Route("/editar/", name="OrdenTrabajoReparaciones_editar")
	 * @Method({"GET","POST"})
	 * @ApiDoc(
	 * 		description = "Edita un OrdenTrabajoReparaciones",
	 * 		requirements = {
	 *			{
	 *         		"name"="id",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="id del OrdenTrabajoReparaciones"
	 *			},{
	 *         		"name"="descripcion",
	 *         		"dataType"="string",
	 *         		"requirement"="string",
	 *         		"description"="descripción del tipo de OrdenTrabajoReparaciones"
	 *			},
	 *			{
	 *         		"name"="fechaCarga",
	 *         		"dataType"="datetime",
	 *         		"requirement"="dd/mm/yyyy",
	 *         		"description"="fecha de carga del OrdenTrabajoReparaciones"
	 *			},
	 *			{
	 *         		"name"="cantidad",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="cantidad de OrdenTrabajoReparacioneses"
	 *			},
	 *			{
	 *         		"name"="valor",
	 *         		"dataType"="float",
	 *         		"requirement"="\d+",
	 *         		"description"="valor, si el tipo de OrdenTrabajoReparaciones es gravado se le suma el % de la tasa de aeropuerto"
	 *			},
	 *			{
	 *         		"name"="tipoOrdenTrabajoReparaciones",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="Tipo de OrdenTrabajoReparaciones"
	 *			},
	 *			{
	 *         		"name"="debaja",
	 *         		"dataType"="boolean",
	 *         		"requirement"="string S o N",
	 *         		"description"="S está dado de baja"
	 *			},
	 *			{
	 *         		"name"="usuario",
	 *         		"dataType"="string",
	 *         		"requirement"="string",
	 *         		"description"="usuario logueado"
	 *			}
	 *
	 *		},
	 *      parameters={}
	 * )
	 * @Template()
	 */
	public function editarAction(Request $request)
	{
		$validator = $this->get('validator');
		$user = $this->get("security.context")->getToken()->getUser();

		
		$result = $this->OrdenTrabajoReparacionesCrearSvc->editar($request,$validator,$user);
		$serializer = SerializerBuilder::create()->build();
		if($result == null)
		{
			$result = "OrdenTrabajoReparaciones no encontrado";
			$httpError = 404;
		}
		else
		{
			$httpError = 200;
		}
		$result = $serializer->serialize($result, 'json');
                
		$response = new Response($result, $httpError);
		$response->headers->set('Content-Type', 'application/json');
		return $response;
	}
	
}