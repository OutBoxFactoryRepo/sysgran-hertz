<?php
namespace Hertz\ReservaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use JMS\SecurityExtraBundle\Annotation as Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\Serializer\SerializerBuilder;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use JMS\DiExtraBundle\Annotation as DI;
use Hertz\ReservaBundle\Service\TareasService;
use Symfony\Component\Validator;

class TareasController extends Controller
{
	
	/**
	 *
	 * @var TareasSearchService
	 *
	 * @DI\Inject("TareasSearchService.Search")
	 */
	private $searchTareasSvc;
		
	/**
	 *
	 * @var TareasGetOneService
	 *
	 * @DI\Inject("TareasGetOneService.GetOne")
	 */
	private $TareasGetOneSvc;
	
	/**
	 *
	 * @var TareasCrearService
	 *
	 * @DI\Inject("TareasCrearService.Crear")
	 */
	private $TareasCrearSvc;
	
	
	public function indexAction()
	{
		return array();
	}

        
        /**
	 * @Route("/agregarreparacion/", name="Tareas_agregar_repararion")
	 * @Method({"GET","POST"})
	 * @ApiDoc(
	 * 		description = "crea un tarea",
	 * 		
	 * 		requirements = {
	 * 			{
	 *         		"name"="idReparacion",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="id del reparacion"
	 *			},
	 *			{
	 *         		"name"="debaja",
	 *         		"dataType"="boolean",
	 *         		"requirement"="string S o N",
	 *         		"description"="S está dado de baja"
	 *			},	 
	 *			{
	 *         		"name"="descripcion",
	 *         		"dataType"="string",
	 *         		"requirement"="string",
	 *         		"description"="descripción del tipo de tarea"
	 *			}
	 *		},
	 *      parameters={}
	 * )
	 * @Template()
	 */
        public function agregarreparacionAction(Request $request)
        {
            $validator = $this->get('validator');
		$user = $this->get("security.context")->getToken()->getUser();
		$result = $this->TareasCrearSvc->agregarreparacion($request,$validator,$user);
		$serializer = SerializerBuilder::create()->build();
		$result = $serializer->serialize($result, 'json');
		$httpError = 200;
		$response = new Response($result, $httpError);
		$response->headers->set('Content-Type', 'application/json');
		return $response;
        }
        
        
	/**
	 * @Route("/search/")
     * @Method({"GET","POST"})
     * 
     * @ApiDoc(
     * 		description = "La lista de todos los tipos de tareas",
     * 		requirements = {},
	 *      parameters={}
     * )
	 */
	public function getAllAction()
	{
		$result = $this->searchTareasSvc->getAll();
		$serializer = SerializerBuilder::create()->build();
    	$result = $serializer->serialize($result, 'json');
    	$httpError = 200;
    	$response = new Response($result, $httpError);
    	$response->headers->set('Content-Type', 'application/json');
    	return $response;
	}
	
	/**
	 * @Route("/{id}/search/")
	 * @Method({"GET"})
	 *
	 * @ApiDoc(
	 * 		description = "obtiene un tarea en particular",
	 * 		requirements = {},
	 *      parameters={}
	 * )
	 */
	public function getOneAction($id)
	{
		$result = $this->TareasGetOneSvc->getOne($id);
		$serializer = SerializerBuilder::create()->build();
		
		if($result == null)
		{
			$result = "Color no encontrado";
			$httpError = 404;
		}
		else
		{
			$httpError = 200;
		}
		$result = $serializer->serialize($result, 'json');
		$response = new Response($result, $httpError);
		$response->headers->set('Content-Type', 'application/json');
		return $response;
	}
	
	/**
	 * @Route("/crear/", name="Tareas_crear")
	 * @Method({"GET","POST"})
	 * @ApiDoc(
	 * 		description = "crea un tarea",
	 * 		
	 * 		requirements = {
	 * 			{
	 *         		"name"="id",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="id del tarea"
	 *			},
	 *			{
	 *         		"name"="tarea",
	 *         		"dataType"="string",
	 *         		"requirement"="string",
	 *         		"description"="nombre del tarea"
	 *			},	 
	 *			{
	 *         		"name"="descripcion",
	 *         		"dataType"="string",
	 *         		"requirement"="string",
	 *         		"description"="descripción del tipo de tarea"
	 *			}
	 *		},
	 *      parameters={}
	 * )
	 * @Template()
	 */
	public function crearAction(Request $request)
	{
		$validator = $this->get('validator');
		$user = $this->get("security.context")->getToken()->getUser();
		$result = $this->TareasCrearSvc->crear($request,$validator,$user);
		$serializer = SerializerBuilder::create()->build();
		$result = $serializer->serialize($result, 'json');
		$httpError = 200;
		$response = new Response($result, $httpError);
		$response->headers->set('Content-Type', 'application/json');
		return $response;
	}
	
	
	/**
	 * @Route("/editar/", name="Tareas_editar")
	 * @Method({"GET","POST"})
	 * @ApiDoc(
	 * 		description = "Edita un tarea",
	 * 		requirements = {
	 *			{
	 *         		"name"="id",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="id del tarea"
	 *			},
	 *			{
	 *         		"name"="tarea",
	 *         		"dataType"="string",
	 *         		"requirement"="string",
	 *         		"description"="nombre del tarea"
	 *			},	 
	 *			{
	 *         		"name"="descripcion",
	 *         		"dataType"="string",
	 *         		"requirement"="string",
	 *         		"description"="descripción del tipo de tarea"
	 *			}
	 *		},
	 *      parameters={}
	 * )
	 * @Template()
	 */
	public function editarAction(Request $request)
	{
		$validator = $this->get('validator');
		$user = $this->get("security.context")->getToken()->getUser();
		
		$result = $this->TareasCrearSvc->editar($request,$validator,$user);
		$serializer = SerializerBuilder::create()->build();
		if($result == null)
		{
			$result = "Color no encontrado";
			$httpError = 404;
		}
		else
		{
			$httpError = 200;
		}
		$result = $serializer->serialize($result, 'json');
		$response = new Response($result, $httpError);
		$response->headers->set('Content-Type', 'application/json');
		return $response;
	}
	
}