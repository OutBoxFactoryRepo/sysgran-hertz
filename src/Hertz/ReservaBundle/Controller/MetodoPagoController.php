<?php
namespace Hertz\ReservaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
//use Acme\DemoBundle\Form\ContactType;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

// these import the "@Route" and "@Template" annotations
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\Serializer\SerializerBuilder;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use JMS\DiExtraBundle\Annotation as DI;
use Hertz\ReservaBundle\Service\MetodoPagoService;

class MetodoPagoController extends Controller
{
	
	/**
	 *
	 * @var MetodoPagoSearchService
	 *
	 * @DI\Inject("MetodoPagoSearchService.Search")
	 */
	private $searchMetodoPagoSvc;
		

	/**
	 *
	 * @var MetodoPagoGetOneSvc
	 *
	 * @DI\Inject("MetodoPagoSearchService.getone")
	 */
	private $getOnex;
	
	
	public function indexAction()
	{
		return array();
	}

	/**
	 * @Route("/search/")
            * @Method({"GET","POST"})
            * 
            * @ApiDoc(
            * 		description = "La lista de todas los metodos de pago",
            * 		requirements = {},
                *      parameters={}
            * )
	 */
	public function getAllAction()
	{
		$result = $this->searchMetodoPagoSvc->getAll();
		$serializer = SerializerBuilder::create()->build();
                $result = $serializer->serialize($result, 'json');
                $httpError = 200;
                $response = new Response($result, $httpError);
                $response->headers->set('Content-Type', 'application/json');
    	return $response;
	}
	
        /**
	 * @Route("/search/{id}")
	 * @Method({"GET","POST"})
	 *
	 * @ApiDoc(
	 * 		description = "obtiene un tipo de documento en particular",
	 * 		requirements = {{
	 *         "name"="id",
	 *         "dataType"="integer",
	 *         "requirement"="",
	 *         "description"="Id del tipo de documento"
	 *			}},
	 *      parameters={}
	 * )
	 */
	public function getOneAction($id)
	{
		$result = $this->getOnex->getOne($id);
		$serializer = SerializerBuilder::create()->build();
		$result = $serializer->serialize($result, 'json');
		$httpError = 200;
		$response = new Response($result, $httpError);
		$response->headers->set('Content-Type', 'application/json');
		return $response;
	}	

}