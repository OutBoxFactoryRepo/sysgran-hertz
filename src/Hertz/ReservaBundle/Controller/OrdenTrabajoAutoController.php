<?php
namespace Hertz\ReservaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
//use Acme\DemoBundle\Form\ContactType;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use JMS\SecurityExtraBundle\Annotation as Security;

// these import the "@Route" and "@Template" annotations
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\Serializer\SerializerBuilder;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use JMS\DiExtraBundle\Annotation as DI;
use Hertz\ReservaBundle\Service\OrdenTrabajoAutoService;
use Symfony\Component\Validator;

class OrdenTrabajoAutoController extends Controller
{
	
	/**
	 *
	 * @var OrdenTrabajoAutoSearchService
	 *
	 * @DI\Inject("OrdenTrabajoAutoSearchService.Search")
	 */
	private $searchOrdenTrabajoAutoSvc;
		
	/**
	 *
	 * @var OrdenTrabajoAutoGetOneService
	 *
	 * @DI\Inject("OrdenTrabajoAutoGetOneService.GetOne")
	 */
	private $OrdenTrabajoAutoGetOneSvc;
	
	/**
	 *
	 * @var OrdenTrabajoAutoCrearService
	 *
	 * @DI\Inject("OrdenTrabajoAutoCrearService.Crear")
	 */
	private $OrdenTrabajoAutoCrearSvc;
	
	
	public function indexAction()
	{
		return array();
	}

	/**
	 * @Route("/search/")
     * @Method({"GET","POST"})
     * 
     * @ApiDoc(
     * 		description = "La lista de ordenes de trabajo",
     * 		requirements = {},
	 *      parameters={}
     * )
	 */
	public function getAllAction(Request $request)
	{
		$user = $this->get("security.context")->getToken()->getUser();
		
		$result = $this->searchOrdenTrabajoAutoSvc->getAll($request, $user);
		$serializer = SerializerBuilder::create()->build();                


                $totalRows = $result["total"];
                $totalPaginas = $result["totalPaginas"];
                $totalPaginasEntero = (int) $totalPaginas;

                if($totalPaginas > $totalPaginasEntero)
                {
                    $totalPaginas = (int)($totalPaginas + 1);
                }

                $arrayResultado = array("current"=>(int)$request->request->get("current"), "rowCount"=>$totalRows,"rows"=>$result["objeto"],"total"=>$totalPaginas);

                $result = $serializer->serialize($arrayResultado, 'json');



                $httpError = 200;
                $response = new Response($result, $httpError);
                $response->headers->set('Content-Type', 'application/json');
                return $response;
	}
	
	/**
	 * @Route("/{id}/search/")
	 * @Method({"GET"})
	 *
	 * @ApiDoc(
	 * 		description = "obtiene una orden de trabajo en particular",
	 * 		requirements = {},
	 *      parameters={}
	 * )
	 */
	public function getOneAction($id)
	{
		$result = $this->OrdenTrabajoAutoGetOneSvc->getOne($id);
		$serializer = SerializerBuilder::create()->build();
		
		if($result == null)
		{
			$result = "Orden de trabajo no encontrada";
			$httpError = 404;
		}
		else
		{
			$httpError = 200;
		}
		$result = $serializer->serialize($result, 'json');
		$response = new Response($result, $httpError);
		$response->headers->set('Content-Type', 'application/json');
		return $response;
	}
	
	/**
	 * @Route("/crear/", name="ordentrabajoautocrear")
	 * @Method({"GET","POST"})
	 * @ApiDoc(
	 * 		description = "crea una orden de trabajo automática",
	 * 		
	 * 		requirements = {
	 *			{
	 *         		"name"="descripcion",
	 *         		"dataType"="string",
	 *         		"requirement"="string",
	 *         		"description"="descripción del tipo de adicional"
	 *			},
	 *			{
	 *         		"name"="fechaCarga",
	 *         		"dataType"="datetime",
	 *         		"requirement"="dd/mm/yyyy",
	 *         		"description"="fecha de carga del adicional"
	 *			},
	 *			{
	 *         		"name"="vehiculo",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="id del vehículo"
	 *			},
	 *			{
	 *         		"name"="kilometros",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="cada cuántos kilometros se disparará la orden de trabajo automática"
	 *			},
	 *			{
	 *         		"name"="ultimaActualizacion",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="cantidad de kilómetros de la última creación de una orden automática"
	 *			},
	 *			{
	 *         		"name"="debaja",
	 *         		"dataType"="boolean",
	 *         		"requirement"="string S o N",
	 *         		"description"="S está dado de baja"
	 *			},
	 *			{
	 *         		"name"="usuario",
	 *         		"dataType"="string",
	 *         		"requirement"="string",
	 *         		"description"="usuario logueado"
	 *			}
	 *		},
	 *      parameters={}
	 * )
	 * @Template()
	 */
	public function crearAction(Request $request)
	{
		$validator = $this->get('validator');
		$user = $this->get("security.context")->getToken()->getUser();
		if($request->request->get("oper") == "edit")
                {
                    $result = $this->OrdenTrabajoAutoCrearSvc->editar($request,$validator,$user);
                }
                else
                {
                    $result = $this->OrdenTrabajoAutoCrearSvc->crear($request,$validator,$user);
                }
		$serializer = SerializerBuilder::create()->build();
		$result = $serializer->serialize($result, 'json');
		$httpError = 200;
		$response = new Response($result, $httpError);
		$response->headers->set('Content-Type', 'application/json');
		return $response;
	}
	
	
	/**
	 * @Route("/editar/", name="ordentrabajoeditar")
	 * @Method({"GET","POST"})
	 * @ApiDoc(
	 * 		description = "Edita una orden de trabajo automática",
	 * 		requirements = {
	 *			{
	 *         		"name"="id",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="id del adicional"
	 *			},{
	 *         		"name"="descripcion",
	 *         		"dataType"="string",
	 *         		"requirement"="string",
	 *         		"description"="descripción del tipo de adicional"
	 *			},
	 *			{
	 *         		"name"="fechaCarga",
	 *         		"dataType"="datetime",
	 *         		"requirement"="dd/mm/yyyy",
	 *         		"description"="fecha de carga del adicional"
	 *			},
	 *			{
	 *         		"name"="vehiculo",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="id del vehículo"
	 *			},
	 *			{
	 *         		"name"="kilometros",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="cada cuántos kilometros se disparará la orden de trabajo automática"
	 *			},
	 *			{
	 *         		"name"="ultimaActualizacion",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="cantidad de kilómetros de la última creación de una orden automática"
	 *			},
	 *			{
	 *         		"name"="debaja",
	 *         		"dataType"="boolean",
	 *         		"requirement"="string S o N",
	 *         		"description"="S está dado de baja"
	 *			},
	 *			{
	 *         		"name"="usuario",
	 *         		"dataType"="string",
	 *         		"requirement"="string",
	 *         		"description"="usuario logueado"
	 *			}
	 *
	 *		},
	 *      parameters={}
	 * )
	 * @Template()
	 */
	public function editarAction(Request $request)
	{
		$validator = $this->get('validator');
		$user = $this->get("security.context")->getToken()->getUser();

		
		
		
		$result = $this->OrdenTrabajoAutoCrearSvc->editar($request,$validator,$user);
		$serializer = SerializerBuilder::create()->build();
		if($result == null)
		{
			$result = "Orden de trabajo no encontrada";
			$httpError = 404;
		}
		else
		{
			$httpError = 200;
		}
		$result = $serializer->serialize($result, 'json');
		$response = new Response($result, $httpError);
		$response->headers->set('Content-Type', 'application/json');
		return $response;
	}
	
}