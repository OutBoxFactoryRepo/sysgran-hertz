<?php
namespace Hertz\ReservaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
//use Acme\DemoBundle\Form\ContactType;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use JMS\SecurityExtraBundle\Annotation as Security;

// these import the "@Route" and "@Template" annotations
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\Serializer\SerializerBuilder;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use JMS\DiExtraBundle\Annotation as DI;
use Doctrine\Common\Collections\ArrayCollection;

use Hertz\UserBundle\Entity\User;
use Hertz\UserBundle\Form\Type\UserType;
use Hertz\UserBundle\Service\UserService;

class UsuariosController extends Controller
{
	
	/**
	 *
	 * @var UserService
	 * @DI\Inject("user.manager")
	 */
	private $userSvc;
		
    /**
     *
     * @Route("/", name="usuario_index")
     * @Method({"GET","POST"})
     * @ApiDoc(
     * 		description = "Este es un servicio injectado y lista todos los usuario si se tiene permiso",
     * 		requirements = {},
     *      parameters={}
     * )
     * @Template()
     */
    public function indexAction(Request $request)
    {
            $serializer = SerializerBuilder::create()->build();

            $userx = $this->get("security.context")->getToken()->getUser();
            $userx = $userx->getUsername();

            $sucursal = $this->userSvc->getByUserUser($userx);
            if(!$sucursal)
            {
                    echo "no existe el usuario";die();
            }
            $sucursalx = $sucursal->getSucursal();

            //parametros de la grilla
            
            $orden = strtoupper($request->request->get("sord"));
            $campo = $request->request->get("sidx");
            
            $pagina = $request->request->get("page");
            $registros = (int)$request->request->get("rows");
            
            $current = $request->request->get("current");
            
            $fin = $registros;
            $inicio = ($registros * $pagina) - $registros;


            if($campo == "last_name")
            {
                $campo = "lastName";
            }
            elseif($campo == "is_super_admin")
            {
                $campo = "isSuperAdmin";
            }           
            
            
            //parámetros de búsqueda
            $campoBusqueda = $request->request->get("searchField");
            $valorCampoBusqueda = strtolower($request->request->get("searchString"));
            $searchOper = $request->request->get("searchOper");


            if($campoBusqueda == "last_name")
            {
                $campoBusqueda = "lastName";
            }
            elseif($campoBusqueda == "is_super_admin")
            {
                $campoBusqueda = "isSuperAdmin";
            }
            
            
            if($searchOper == "eq")
            {
                $searchOper = "=";
            }
            elseif($searchOper == "ne") 
            {
                $searchOper = "!=";
            }
            elseif($searchOper == "cn") 
            {
                $searchOper = "like";
            }
            elseif($searchOper == "lt") 
            {
                $searchOper = "<";
            }
            elseif($searchOper == "gt") 
            {
                $searchOper = ">";
            }
                        
            //fin parametros de la grilla
            
            //si la sucursal es "1" puede ver todos los usuarios, por más que sea superuser. Sino filtra por sucursal
            $sucursalx = $sucursalx->getId();
            if($sucursalx == 1)
            {
                    $params = $this->userSvc->getGrilla($orden,$campo,$inicio,$fin,$pagina,$registros,$current,$searchOper,$campoBusqueda,$valorCampoBusqueda);
            }
            else
            {
                    $params = $this->userSvc->getUserSucursal($sucursalx);
            }	
            
            $auxRows = array();
            foreach($params["rows"] as $row)
            {
                $auxRows[] = array(
                        "id"=>$row->getId()
                        ,"user"=>$row->getUser()
                        ,"password"=>$row->getPassword()
                        ,"created"=>$row->getCreated()
                        ,"modified"=>$row->getModified()
                        ,"status"=>$row->getStatus()
                        ,"sucursal"=>$row->getSucursal()
                        ,"franquiciado"=>$row->getFranquiciado()
                        ,"is_super_admin"=>$row->getIsSuperAdmin()
                        ,"name"=>$row->getName()
                        ,"last_name"=>$row->getLastName()
                        ,"description"=>$row->getDescription()
                        ,"email"=>$row->getEmail()
                        );
                
                //{"id":1,"user":"admin","password":"$2y$12$AhZ2DIUFuBV0JGZWhf01HukJMPBe5Gc32jB3IfJzd8oYqUHKd4hOu","created":"2015-10-29T14:49:39-0300","modified":"2015-10-29T14:49:39-0300","status":true,"sucursal":{"id":1,"descripcion":"Todas","debaja":false},"is_super_admin":true
            }
            
            $params["rows"] = $auxRows;
            
            $result = $serializer->serialize($params, 'json');
            
            $httpError = 200;
            $response = new Response($result, $httpError);
            $response->headers->set('Content-Type', 'application/json');
            return $response;
    }
    
    
    
    /**
    *
    * @Route("/role", name="usuario_role_index")
    * @Method({"GET","POST"})
    * @ApiDoc(
    * 		description = "Este es un servicio injectado y lista todos los roles",
    * 		requirements = {},
    *      parameters={}
    * )
    * @Template()
    */
    public function reoleAction()
    {
        $auxRoles = array();
        foreach($this->userSvc->getRole() as $rol)
        {
            $auxRoles[] = array(
                "id"=>$rol->getId()
               ,"description"=>$rol->getDescription()
               ,"created"=>$rol->getCreated()
               ,"name"=>$rol->getName()
               ,"modified"=>$rol->getModified()
            );
        }
        $params['roles'] = $auxRoles;
        $serializer = SerializerBuilder::create()->build();
        $result = $serializer->serialize($params, 'json');
    	$httpError = 200;
    	$response = new Response($result, $httpError);
    	$response->headers->set('Content-Type', 'application/json');
    	return $response;
    }
    
    
        /**
    *
    * @Route("/roles/", name="roles_listados")
    * @Method({"GET","POST"})
    * @ApiDoc(
    * 		description = "Este es un servicio injectado y lista todos los roles",
    * 		requirements = {},
    *      parameters={}
    * )
    * @Template()
    */
    public function rolesListadoAction()
    {
        $roles = $this->userSvc->getRole();
        
        $auxRoles = array();
        foreach($roles as $rol)
        {
            $auxRoles[] = array(
                "id"=>$rol->getId()
               ,"description"=>$rol->getDescription()
               ,"created"=>$rol->getCreated()
               ,"name"=>$rol->getName()
               ,"modified"=>$rol->getModified()
            );
        }
        
        $serializer = SerializerBuilder::create()->build();
        $result = $serializer->serialize($auxRoles, 'json');
    	$httpError = 200;
    	$response = new Response($result, $httpError);
    	$response->headers->set('Content-Type', 'application/json');
    	return $response;
    }
    
    
    /**
    *
    * @Route("/crear/", name="usuario_edit")
    * @Method({"GET","POST"})
    * @ApiDoc(
    * 		description = "Este es un servicio injectado y lista todos los roles",
    * 		requirements = {},
    *      parameters={}
    * )
    */
    public function crearAction(Request $request)
    {
                //print_r($request->request);die();
        
                 
                    
                //echo "passwordx:".$request->request->get("passwordx");die();
                if(trim($request->request->get("passwordx"))!= "")
                {
                    $valPassword = $this->valPass($request->request->get("passwordx"));
                    if($valPassword!= "")
                    {
                        $result = array("error"=>$valPassword);
                        $serializer = SerializerBuilder::create()->build();
                        $result = $serializer->serialize($result, 'json');
                        $httpError = 200;
                        $response = new Response($result, $httpError);
                        $response->headers->set('Content-Type', 'application/json');
                        return $response;                        
                    }
                }

                  
        
                    
                    
        
                $content = $request->getContent();
                $jsonContent = json_decode($content, true);
                //print_r($content);die();
                $id = null;
                //$objRequest = new ArrayCollection($request->request);
                //$objRequest = new ArrayCollection($objRequest->get("users"));		
                $objRequest = $request->request;

                if($objRequest->get("id") != null)
                {
                        $id = $objRequest->get("id");
                }

                $pos = strrpos($objRequest->get("user"), " ");
                if ($pos !== false) 
                {
                    $user = array("error"=>"No se permiten espacios en el nombre de usuario");
                }
                else
                {
                    $user = Array(
                        "user" => $objRequest->get("user"),
                        "password" => Array
                            (
                                "first" => $objRequest->get("clave"),
                                "second" => $objRequest->get("clave")
                            ),

                        "name" => $objRequest->get("name"),
                        "lastName" => $objRequest->get("last_name"),
                        "email" => $objRequest->get("email"),
                        "description" => $objRequest->get("description"),
                        "status" => $objRequest->get("status"),
                        "isSuperAdmin" => $objRequest->get("is_super_admin"),
                        "assignedRoles" => Array
                            (
                                0 => $objRequest->get("assigned_roles[0].id")
                            ),
                        "sucursal" => $objRequest->get("sucursal_descripcion"),
                        "franquiciado" => $objRequest->get("franquiciado_nombre")
                    );

                    //echo "franquiciado ".$objRequest->get("franquiciado_nombre");die();

                    $data = $user;
                    //$data['password'] = isset($data['password']['first']) ? trim($data['password']['first']) : null;
                    $data['password'] = trim($objRequest->get("passwordx"));
                    $data['email'] = isset($data['email']) ? $objRequest->get("email") : "";
                    $data['status'] = isset($data['status']) ? $data['status'] : false;
                    $data['isSuperAdmin'] = isset($data['isSuperAdmin']) ? $data['isSuperAdmin'] : false;
                    $user = $this->userSvc->createOrUpdate($data,$id);
                }
                $serializer = SerializerBuilder::create()->build();
                $result = $serializer->serialize($user, 'json');
	    	$httpError = 200;
	    	$response = new Response($result, $httpError);
	    	$response->headers->set('Content-Type', 'application/json');
	    	return $response;
	}
    
        /**
        *
        * @Route("/asignados/", name="usuario_roles_asignados")
        * @Method({"GET","POST"})
        * @ApiDoc(
        * 		description = "Este es un servicio injectado y lista todos los roles del usuario",
        * 		requirements = {},
        *      parameters={}
        * )
        * @Template()
        */
        public function getAssignedRolAction(Request $request)
        {
            
           $objRequest = $request->query;

            $idUser = $objRequest->get("idUser");

            $usuario= $this->userSvc->getByIdUser($idUser);
            
            //$result = $usuario;
            
            $auxRoles = array();
            foreach($usuario->getAssignedRoles() as $rol)
            {
                $auxRoles[] = array(
                    "id"=>$rol->getId()
                   ,"description"=>$rol->getDescription()
                   ,"created"=>$rol->getCreated()
                   ,"name"=>$rol->getName()
                   ,"modified"=>$rol->getModified()
                );
            }
            
            $serializer = SerializerBuilder::create()->build();
            $result = $serializer->serialize($auxRoles, 'json');
            $httpError = 200;
            $response = new Response($result, $httpError);
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }
        
        
        /**
        *
        * @Route("/eliminarrol/", name="usuario_roles_eliminar")
        * @Method({"GET","POST"})
        * @ApiDoc(
        * 		description = "Este es un servicio injectado y lista todos los roles del usuario",
        * 		requirements = {},
        *      parameters={}
        * )
        * @Template()
        */
        public function deleteAssignedRolAction(Request $request)
        {
            
           $objRequest = $request->query;

            $idUser = $objRequest->get("idUser");
            $idRol = $request->request->get("id");
            $user = $this->userSvc->getByIdUser($idUser);
            //$user = $this->em->getRepository(User::ORM_ENTITY)->findOneById($idUser);

            $resultado = $this->userSvc->removeRolAssigned($idUser,$idRol);
            if(!$resultado)
            {
                $result = array("error"=>"No se puede eliminar este perfil. Dado que es un perfil crítico");
            }
            else
            {
                $result = $user->getAssignedRoles();
            }
            
            $serializer = SerializerBuilder::create()->build();
            $result = $serializer->serialize($result, 'json');
            $httpError = 200;
            $response = new Response($result, $httpError);
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }
        
        
        /**
        *
        * @Route("/addrol/", name="usuario_roles_add")
        * @Method({"GET","POST"})
        * @ApiDoc(
        * 		description = "Este es un servicio injectado y lista todos los roles del usuario",
        * 		requirements = {},
        *      parameters={}
        * )
        * @Template()
        */
        public function setRolAction(Request $request)
        {
            
           $objRequest = $request->query;

            $idUser = $objRequest->get("idUser");
            $idRol = $request->request->get("id");
            
            $rol = $this->userSvc->getByIdRole($idRol);            
            
            $user = $this->userSvc->getByIdUser($idUser);
            
            
            if($user->hasRoleName($rol->getName()))
            {
                $result = array("error"=>"Ya existe ese rol");
            }
            else
            {
               $result = $this->userSvc->addRolAssigned($idUser,$idRol);
               $result = $user->getAssignedRoles();
            }
            //$result = array("reultado"=>$user->hasRoleName($rol->getName()));
            $serializer = SerializerBuilder::create()->build();
            $result = $serializer->serialize($result, 'json');
            $httpError = 200;
            $response = new Response($result, $httpError);
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }
        
        /**
        *
        * @Route("/esvendedor/", name="usuario_es_vendedor")
        * @Method({"GET","POST"})
        * @ApiDoc(
        * 		description = "Este es un servicio injectado y me dice si el usuario es un vendedor",
        * 		requirements = {},
        *      parameters={}
        * )
        * @Template()
        */
        public function getEsVendedorAction(Request $request)
        {
           
           	$userx = $this->get("security.context")->getToken()->getUser();
		$esAdmin = $userx->getEsAdmin();
                $userx = $userx->getUsername();
		
		$sucursal = $this->userSvc->getByUserUser($userx);
		
                $esVendedor = false;
                
                if($sucursal->hasRoleName("Vendedor"))
                {
                    $esVendedor = true;
                }
            
           $result = array("esVendedor"=>$esVendedor);
           $serializer = SerializerBuilder::create()->build();
            $result = $serializer->serialize($result, 'json');
            $httpError = 200;
            $response = new Response($result, $httpError);
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }
        
        
        
        
        /**
        *
        * @Route("/roles/permisos/", name="roles_permisos_asignados")
        * @Method({"GET","POST"})
        * @ApiDoc(
        * 		description = "Este es un servicio injectado y lista todos los permisos de un rol",
        * 		requirements = {{"name"="idRol"}},
        *      parameters={}
        * )
        * @Template()
        */
        public function getRoleAssignedPermissionsAction(Request $request)
        {
            
           $objRequest = $request->query;

            $idRol = $objRequest->get("idRol");

            
            $result = $this->userSvc->getAssignedRolPermissions($idRol);
            
            $serializer = SerializerBuilder::create()->build();
            $result = $serializer->serialize($result, 'json');
            $httpError = 200;
            $response = new Response($result, $httpError);
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }
        
        
        
        /**
        *
        * @Route("/roles/delete/", name="roles_borrar")
        * @Method({"GET","POST"})
        * @ApiDoc(
        * 		description = "Este es un servicio injectado y lista todos los roles",
        * 		requirements = {},
        *      parameters={}
        * )
        * @Template()
        */
        public function deleteRoleAction(Request $request)
        {

                    $content = $request->getContent();
                    $jsonContent = json_decode($content, true);

                    $id = null;
                    $objRequest = $request->request;
                    
                    //print_r($objRequest);die();
                    
                    $id = $objRequest->get("id");

                    


                    $user = $this->userSvc->deleteRole($id);
                    

                    $serializer = SerializerBuilder::create()->build();
                    $result = $serializer->serialize($user, 'json');
                    $httpError = 200;
                    $response = new Response($result, $httpError);
                    $response->headers->set('Content-Type', 'application/json');
                    return $response;
        }
        
        /**
        *
        * @Route("/roles/crear/", name="roles_crear")
        * @Method({"GET","POST"})
        * @ApiDoc(
        * 		description = "Este es un servicio injectado y lista todos los roles",
        * 		requirements = {},
        *      parameters={}
        * )
        * @Template()
        */
        public function crearOEditarRoleAction(Request $request)
        {

                    $content = $request->getContent();
                    $jsonContent = json_decode($content, true);

           
                    
                    $id = null;
                    $objRequest = $request->request;

                    if($objRequest->get("id") != null)
                    {
                            $id = $objRequest->get("id");
                    }
                    $oper = $objRequest->get("oper");


                    if($oper != "edit")
                    {
                        $data['name'] = $objRequest->get("name");
                        $data['description'] = $objRequest->get("description");
                        $user = $this->userSvc->createOrUpdateRole($data,null);
                    }
                    else
                    {
                        $data['name'] = $objRequest->get("name");
                        $data['description'] = $objRequest->get("description");
                        $user = $this->userSvc->createOrUpdateRole($data,$id);
                    }

                    $serializer = SerializerBuilder::create()->build();
                    $result = $serializer->serialize($user, 'json');
                    $httpError = 200;
                    $response = new Response($result, $httpError);
                    $response->headers->set('Content-Type', 'application/json');
                    return $response;
            }

            /**
            *
            * @Route("/permisos/", name="listar_permisos")
            * @Method({"GET","POST"})
            * @ApiDoc(
            * 		description = "Este es un servicio injectado y lista todos los permisos",
            * 		requirements = {},
            *      parameters={}
            * )
            * @Template()
            */
            public function listarPermisosAction(Request $request)
            {

                        $content = $request->getContent();
                        $jsonContent = json_decode($content, true);

                        $result = $this->userSvc->getPermissionList();
                        $serializer = SerializerBuilder::create()->build();
                        $result = $serializer->serialize($result, 'json');
                        $httpError = 200;
                        $response = new Response($result, $httpError);
                        $response->headers->set('Content-Type', 'application/json');
                        return $response;
            }
            
            /**
            *
            * @Route("/roles/permisos/add/", name="add_rol_permisos")
            * @Method({"GET","POST"})
            * @ApiDoc(
            * 		description = "Este es un servicio injectado y agrega un permiso al rol",
            * 		requirements = {},
            *      parameters={}
            * )
            */
            public function addPermiRolAction(Request $request)
            {


                    $content = $request->getContent();
                    $jsonContent = json_decode($content, true);

                    $id = null;
                    $objRequest = $request->request;

                    $id = $request->query->get("idRol");
                    
                    
                    $idPermiso = $objRequest->get("name");
                    
                    $oper = $objRequest->get("oper");
                    $result = null;
                    if($oper == "add")
                    {
                        if ($this->userSvc->hasRolPermissionId($id,$idPermiso))
                        {
                            $result = array("error"=>"Este Rol ya tiene asignado este permiso");
                        }
                        else
                        {
                            $result = $this->userSvc->addAssignedRolPermissions($id,$idPermiso);
                        }
                    }
                    
                    $serializer = SerializerBuilder::create()->build();
                    $result = $serializer->serialize($result, 'json');
                    $httpError = 200;
                    $response = new Response($result, $httpError);
                    $response->headers->set('Content-Type', 'application/json');
                    return $response;
            }
            
            /**
            *
            * @Route("/roles/permisos/del/", name="remove_rol_permisos")
            * @Method({"GET","POST"})
            * @ApiDoc(
            * 		description = "Este es un servicio injectado y elimina un permiso de un rol",
            * 		requirements = {},
            *      parameters={}
            * )
            */
            public function removePermiRolAction(Request $request)
            {


                    $content = $request->getContent();
                    $jsonContent = json_decode($content, true);

                    $id = null;
                    $objRequest = $request->request;

                    //print_r($objRequest);die();
                    
                    $id = $request->query->get("idRol");
                    
                    
                    $idPermiso = $objRequest->get("id");
                    
                    $oper = $objRequest->get("oper");
                    
                    $result = $this->userSvc->removeAssignedRolPermissions($id,$idPermiso);
                    
                    $serializer = SerializerBuilder::create()->build();
                    $result = $serializer->serialize($result, 'json');
                    $httpError = 200;
                    $response = new Response($result, $httpError);
                    $response->headers->set('Content-Type', 'application/json');
                    return $response;
            }
            
        public function valPass($password)
        {
                $error = "";
                if( strlen($password) < 8 ) 
                {
                        $error .= "Debe ser mayor a 8 Caracteres. ";
                }


                if( !preg_match("#[0-9]+#", $password) ) {
                        $error .= "Debe contener al menos un número. ";
                }


                if( !preg_match("#[a-z]+#", $password) ) {
                        $error .= "Debe contener al menos una letra minúscula. ";
                }


                if( !preg_match("#[A-Z]+#", $password) ) {
                        $error .= "Debe contener al menos una letra mayúscula. ";
                }

                /*

                if( !preg_match("#\W+#", $password) ) {
                        $error .= "Password must include at least one symbol! 
                ";
                }

                */

                if($error != "")
                {
                        return "Su contraseña es debil: ".$error;
                } else 
                {
                        return $error;
                }

        }
            
}


