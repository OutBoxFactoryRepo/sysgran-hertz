<?php
namespace Hertz\ReservaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
//use Acme\DemoBundle\Form\ContactType;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use JMS\SecurityExtraBundle\Annotation as Security;

// these import the "@Route" and "@Template" annotations
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\Serializer\SerializerBuilder;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use JMS\DiExtraBundle\Annotation as DI;
use Hertz\ReservaBundle\Service\TallerService;
use Symfony\Component\Validator;

class TallerController extends Controller
{
	
	/**
	 *
	 * @var UserService
	 * @DI\Inject("user.manager")
	 */
	private $userSvc;
	
	/**
	 *
	 * @var TallerSearchService
	 *
	 * @DI\Inject("TallerSearchService.Search")
	 */
	private $searchTallerSvc;
		
	/**
	 *
	 * @var TallerGetOneService
	 *
	 * @DI\Inject("TallerGetOneService.GetOne")
	 */
	private $TallerGetOneSvc;
	
	
	/**
	 *
	 * @var TallerCrearService
	 *
	 * @DI\Inject("TallerCrearService.Crear")
	 */
	private $TallerCrearSvc;
		
	
	public function indexAction()
	{
		return array();
	}

	/**
	 * @Route("/search/")
     * @Method({"GET","POST"})
     * 
     * @ApiDoc(
     * 		description = "La lista de todos los tipos de Talleres",
     * 		requirements = {},
	 *      parameters={}
     * )
	 */
	public function getAllAction(Request $request)
	{
		
		$user = $this->get("security.context")->getToken()->getUser();
		$userx = $user->getUsername();
		$sucursal = $this->userSvc->getByUserUser($userx);
		$sucursal = $sucursal->getSucursal();
		
		$result = $this->searchTallerSvc->getAll($request,$sucursal,$user);
		$serializer = SerializerBuilder::create()->build();
    	

    	$result = array("rows"=>$result);
    	
		$result = $serializer->serialize($result, 'json');
    	$httpError = 200;
    	$response = new Response($result, $httpError);
    	$response->headers->set('Content-Type', 'application/json');
    	return $response;
	}	
	
	/**
	 * @Route("/search/{id}/")
	 * @Method({"GET"})
	 *
	 * @ApiDoc(
	 * 		description = "obtiene un Taller en particular",
	 * 		requirements = {},
	 *      parameters={}
	 * )
	 */
	public function getOneAction($id)
	{
		$result = $this->TallerGetOneSvc->getOne($id);
		$serializer = SerializerBuilder::create()->build();
		
		if($result == null)
		{
			$result = "Taller no encontrado";
			$httpError = 404;
		}
		else
		{
			$httpError = 200;
		}
		$result = $serializer->serialize($result, 'json');
		$response = new Response($result, $httpError);
		$response->headers->set('Content-Type', 'application/json');
		return $response;
	}
	
	
	/**
	 * @Route("/crear/", name="taller_crear")
	 * @Method({"GET","POST"})
	 * @ApiDoc(
	 * 		description = "crea un taller",
	 * 		
	 * 		requirements = {
	 * 			{
	 *         		"name"="descripcion",
	 *         		"dataType"="string",
	 *         		"requirement"="string",
	 *         		"description"="descripción del tipo de adicional"
	 *			},
	 *			{
	 *         		"name"="agencia",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="id de la agencia"
	 *			},
	 *			{
	 *         		"name"="debaja",
	 *         		"dataType"="boolean",
	 *         		"requirement"="string S o N",
	 *         		"description"="S está dado de baja"
	 *			}
	 *		},
	 *      parameters={}
	 * )
	 * @Template()
	 */
	public function crearAction(Request $request)
	{
		$validator = $this->get('validator');
		$user = $this->get("security.context")->getToken()->getUser();
		$result = $this->TallerCrearSvc->crear($request,$validator,$user);
		$serializer = SerializerBuilder::create()->build();
		$result = $serializer->serialize($result, 'json');
		$httpError = 200;
		$response = new Response($result, $httpError);
		$response->headers->set('Content-Type', 'application/json');
		return $response;
	}
}