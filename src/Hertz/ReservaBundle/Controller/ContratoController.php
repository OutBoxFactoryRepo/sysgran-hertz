<?php
namespace Hertz\ReservaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
//use Acme\DemoBundle\Form\ContactType;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use JMS\SecurityExtraBundle\Annotation as Security;

// these import the "@Route" and "@Template" annotations
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\Serializer\SerializerBuilder;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use JMS\DiExtraBundle\Annotation as DI;
use Hertz\ReservaBundle\Service\ContratoService;
use Symfony\Component\Validator;
use WhiteOctober\TCPDFBundle\WhiteOctoberTCPDFBundle;

class ContratoController extends Controller
{
	/**
	 *
	 * @var UserService
	 * @DI\Inject("user.manager")
	 */
	private $userSvc;
	
	/**
	 *
	 * @var ContratoSearchService
	 *
	 * @DI\Inject("ContratoSearchService.Search")
	 */
	private $searchContratoSvc;
		
	/**
	 *
	 * @var ContratoGetOneService
	 *
	 * @DI\Inject("ContratoGetOneService.GetOne")
	 */
	private $ContratoGetOneSvc;
	
	/**
	 *
	 * @var ContratoCrearService
	 *
	 * @DI\Inject("ContratoCrearService.Crear")
	 */
	private $ContratoCrearSvc;
	
	/**
	 *
	 * @var ContratoGetByService
	 *
	 * @DI\Inject("ContratoGetByService.GetBy")
	 */
	private $ContratoGetBySvc;
        
        
	
	public function indexAction()
	{
		return array();
	}

	/**
	 * @Route("/search/")
         * @Method({"GET","POST"})
         * 
         * @ApiDoc(
         * 		description = "La lista de todos los Contratos",
         * 		requirements = {},
	 *      parameters={}
         * )
	 */
	public function getAllAction(Request $request)
	{
		
		$userx = $this->get("security.context")->getToken()->getUser();
		$userx = $userx->getUsername();
		
		$sucursal = $this->userSvc->getByUserUser($userx);
		
		if(!$sucursal)
		{
                        return array("error"=>"no existe el usuario");
		}
		$sucursalx = $sucursal->getSucursal();
                		
		$result = $this->searchContratoSvc->getAll($request,$sucursalx);
		$serializer = SerializerBuilder::create()->build();
		
		$totalRows = $result["total"];
                $totalPaginas = $result["totalPaginas"];
                $totalPaginasEntero = (int) $totalPaginas;
                
                if($totalPaginas > $totalPaginasEntero)
                {
                    $totalPaginas = (int)($totalPaginas + 1);
                }
                
		$arrayResultado = array("current"=>(int)$request->request->get("current"), "rowCount"=>$totalRows,"rows"=>$result["objeto"],"total"=>$totalPaginas);

		$result = $serializer->serialize($arrayResultado, 'json');
                $httpError = 200;
                $response = new Response($result, $httpError);
                //print_r($result);die();
                $response->headers->set('Content-Type', 'application/json');
                return $response;
	}
	
        /**
	 * @Route("/searchadicionales/")
         * @Method({"GET","POST"})
         * 
         * @ApiDoc(
         * 		description = "Lista los adicionales de un contrato por tipo",
         * 		requirements = {},
	 *      parameters={}
         * )
	 */
	public function getAdicionales(Request $request)
	{
		
		$userx = $this->get("security.context")->getToken()->getUser();
		$userx = $userx->getUsername();
		
		$sucursal = $this->userSvc->getByUserUser($userx);
		
		if(!$sucursal)
		{
                        return array("error"=>"no existe el usuario");
		}
		$sucursalx = $sucursal->getSucursal();
                		
		$result = $this->searchContratoSvc->getAll($request,$sucursalx);
		$serializer = SerializerBuilder::create()->build();
		
		$totalRows = $result["total"];
                $totalPaginas = $result["totalPaginas"];
                $totalPaginasEntero = (int) $totalPaginas;
                
                if($totalPaginas > $totalPaginasEntero)
                {
                    $totalPaginas = (int)($totalPaginas + 1);
                }
                
		$arrayResultado = array("current"=>(int)$request->request->get("current"), "rowCount"=>$totalRows,"rows"=>$result["objeto"],"total"=>$totalPaginas);

		$result = $serializer->serialize($arrayResultado, 'json');
                $httpError = 200;
                $response = new Response($result, $httpError);
                //print_r($result);die();
                $response->headers->set('Content-Type', 'application/json');
                return $response;
	}
        
	/**
	 * @Route("/{id}/search/")
	 * @Method({"GET"})
	 *
	 *
	 * @ApiDoc(
	 * 		description = "obtiene un Contrato en particular",
	 * 		requirements = {},
	 *      parameters={}
	 * )
	 */
	public function getOneAction($id)
	{
		$result = $this->ContratoGetOneSvc->getOne($id);
		$serializer = SerializerBuilder::create()->build();
		
		if($result == null)
		{
			$result = "Contrato no encontrado";
			$httpError = 404;
		}
		else
		{
			$httpError = 200;
		}
		$result = $serializer->serialize($result, 'json');
		$response = new Response($result, $httpError);
		$response->headers->set('Content-Type', 'application/json');
		return $response;
	}
        
        /**
	 * @Route("/{idReserva}/searchlock/")
	 * @Method({"POST"})
	 *
	 *
	 * @ApiDoc(
	 * 		description = "obtiene si el contrato está lockeado o no",
	 * 		requirements = {},
	 *      parameters={}
	 * )
	 */
	public function getLockAction($idReserva)
	{
                $userx = $this->get("security.context")->getToken()->getUser();
		$userx = $userx->getId();
		$result = $this->ContratoGetOneSvc->getLock($idReserva,$userx);
		$serializer = SerializerBuilder::create()->build();
		
		
			$httpError = 200;
		
		$result = $serializer->serialize($result, 'json');
		$response = new Response($result, $httpError);
		$response->headers->set('Content-Type', 'application/json');
		return $response;
	}
        
        /**
	 * @Route("/{idReserva}/setlock/")
	 * @Method({"POST"})
	 *
	 *
	 * @ApiDoc(
	 * 		description = "obtiene si el contrato está lockeado o no",
	 * 		requirements = {},
	 *      parameters={}
	 * )
	 */
	public function setLockAction($idReserva)
	{
                $userx = $this->get("security.context")->getToken()->getUser();
		//$userx = $userx->getId();
                
                
		$result = $this->ContratoGetOneSvc->setLock($idReserva,$userx);
		$serializer = SerializerBuilder::create()->build();
		
		
			$httpError = 200;
		
		$result = $serializer->serialize($result, 'json');
		$response = new Response($result, $httpError);
		$response->headers->set('Content-Type', 'application/json');
		return $response;
	}
        
        /**
	 * @Route("/esadmin/")
	 * @Method({"POST","GET"})
	 *
	 *
	 * @ApiDoc(
	 * 		description = "obtiene un Contrato en particular",
	 * 		requirements = {},
	 *      parameters={}
	 * )
	 */
	public function getEsAdminAction()
	{
                $userx = $this->get("security.context")->getToken()->getUser();
		$esAdmin = $userx->getEsAdmin();
                $sucursal = $this->userSvc->getByUserUserId($userx->getId());
                $sucursal = $sucursal->getSucursal()->getId();
		$serializer = SerializerBuilder::create()->build();
                
                $esAdmin = array("admin"=>$esAdmin,"sucursal"=>$sucursal);
                $result = $serializer->serialize($esAdmin, 'json');

                $httpError = 200;
                $response = new Response($result, $httpError);
                //print_r($result);die();
                $response->headers->set('Content-Type', 'application/json');
                return $response;
	}
        
	/**
	 * @Route("/crear/", name="contrato_crear")
	 * 
	 * @ApiDoc(
	 * 		description = "crea un Contrato nuevo",
	 * 		
	 * 		requirements = {
	 *			{
	 *         		"name"="descripcion",
	 *         		"dataType"="string",
	 *         		"requirement"="string",
	 *         		"description"="descripción del contrato"
	 *			},
	 *			{
	 *         		"name"="modelo",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="modelo del contrato"
	 *			},
	 *			{
	 *         		"name"="kilometros",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="kilometros recorridos por el contrato"
	 *			},
	 *			{
	 *         		"name"="agencia",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="id de la agencia de origen del contrato"
	 *			},
	 *			{
	 *         		"name"="modeloCal",
	 *         		"dataType"="string",
	 *         		"requirement"="string(4)",
	 *         		"description"="*********Desconocido hasta el momento, aguardando info funcional"
	 *			},
	 *			{
	 *         		"name"="fechaHoraDevolucion",
	 *         		"dataType"="datetime",
	 *         		"requirement"="dd/mm/yy hh:mm:ss",
	 *         		"description"="Fecha de devolución efectiva del contrato"
	 *			},
	 *			{
	 *         		"name"="fechaHoraSalida",
	 *         		"dataType"="datetime",
	 *         		"requirement"="dd/mm/yy hh:mm:ss",
	 *         		"description"="Fecha y hora de salida programada"
	 *			}
	 *			,
	 *			{
	 *         		"name"="fechaHoraRetorno",
	 *         		"dataType"="datetime",
	 *         		"requirement"="dd/mm/yy hh:mm:ss",
	 *         		"description"="Fecha y hora de retorno programado"
	 *			},
	 *			{
	 *				"name"="dominio",
	 *				"dataType"="string",
	 *				"requirement"="string(10)",
	 *				"description"="Dominio/Patente del contrato"
	 *			},
	 *			{
	 *				"name"="estado",
	 *				"dataType"="integer",
	 *				"requirement"="\d+",
	 *				"description"="Estado del contrato"
	 *			}
	 *		},
	 *      parameters={}
	 * )
	 * @Template()
	 */
	public function crearAction(Request $request)
	{
		$user = $this->get("security.context")->getToken()->getUser();
        $validator = $this->get('validator');
		$result = $this->ContratoCrearSvc->crear($request,$validator, $user);
                //$result = array("mensaje"=>"hola");
		$serializer = SerializerBuilder::create()->build();
		$result = $serializer->serialize($result, 'json');
		$httpError = 200;
		$response = new Response($result, $httpError);
		$response->headers->set('Content-Type', 'application/json');
		return $response;
	}
        
       
  public function returnPDFResponseFromHTML($html,$idContrato,$idReserva){
        //set_time_limit(30); uncomment this line according to your needs
        // If you are not in a controller, retrieve of some way the service container and then retrieve it
        //$pdf = $this->container->get("white_october.tcpdf")->create('vertical', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        //if you are in a controlller use :
        $pdf = $this->get("white_october.tcpdf")->create('vertical', PDF_UNIT, "A3", true, '', false);
        $pdf->SetAuthor('OutBoxFactory');
        $pdf->SetTitle(('Contrato de Reserva'));
        $pdf->SetSubject('Contrato de Reserva');
        $pdf->setFontSubsetting(true);
        $pdf->SetFont('helvetica', '', 10, '', true);
        //$pdf->SetMargins(20,20,40, true);
        $pdf->AddPage();
        
        $filename = 'cont00'.$idContrato."R000".$idReserva;
        
        $pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $html, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);
        $pdf->Output($filename.".pdf",'I'); // This will output the PDF as a response directly
}
    
    /**
	 * @Route("/pdf/", name="contrato_pdf")
	 * @Method({"POST","GET"})
         *
         * @ApiDoc(
	 * 		description = "genera un pdf pasandole una estructura html correcta",
	 * 		requirements = {},
	 *      parameters={}
	 * )
         *   
         * @param type $html
         */ 
    public function pdfAction(Request $request)
     {
    // You can send the html as you want
   //$html = '<h1>Plain HTML</h1>';
    $html = $request->request->get("html");
    
    $idContrato = $request->request->get("idContratoPdf");
    $idReserva = $request->request->get("idReserva");
    // but in this case we will render a symfony view !
    // We are in a controller and we can use renderView function which retrieves the html from a view
    // then we send that html to the user.
        if($html == "")
        {
            $html = 'texto incompleto';
        }
        
        
        
        //return $html;die();
        $this->returnPDFResponseFromHTML($html,$idContrato,$idReserva);
        //return $request->request->get("html");
    }
}