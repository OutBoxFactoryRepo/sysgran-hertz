<?php
namespace Hertz\ReservaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
//use Acme\DemoBundle\Form\ContactType;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use JMS\SecurityExtraBundle\Annotation as Security;

// these import the "@Route" and "@Template" annotations
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\Serializer\SerializerBuilder;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use JMS\DiExtraBundle\Annotation as DI;
use Hertz\ReservaBundle\Service\SearchService;
use Hertz\ReservaBundle\Service\InsertarService;
use Hertz\ReservaBundle\Service\GetOneService;
use Symfony\Component\Validator;



class ReservaController extends Controller
{
	
	/**
	 *
	 * @var UserService
	 * @DI\Inject("user.manager")
	 */
	private $userSvc;
	
        
        /**
	 *
	 * @var InsertarService
	 *
	 * @DI\Inject("InsertarService.Insertar")
	 */
	private $insertarSvc;

	
	/**
	 *
	 * @var SearchService
	 *
	 * @DI\Inject("SearchService.Search")
	 */
	private $searchSvc;
	
	/**
	 *
	 * @var GetOneService
	 *
	 * @DI\Inject("GetOneService.GetOne")
	 */
	private $getoneSvc;
	
        
        /**
	 *
	 * @var GetOneArrayService
	 *
	 * @DI\Inject("GetOneArrayService.GetOneArray")
	 */
	private $getoneSvcArray;
        
        
        /**
	 *
	 * @var AddPasAdicionalService
	 *
	 * @DI\Inject("AddPasAdicionalService.AddPasAdicional")
	 */
	private $addPasAdicionalSvc;
        

	public function indexAction()
	{
		 /*
         * The action's view can be rendered using render() method
         * or @Template annotation as demonstrated in DemoController.
         *
         */
        return $this->render('HertzReservaBundle:Reserva:index.html.twig');
	}

	/**
	 * @Route("/search/")
     * @Method({"GET","POST"})
     * 
     * @ApiDoc(
     * 		description = "La lista de todas las reservas",
     * 		requirements = {},
	 *      parameters={}
     * )
	 */
	public function getAllAction(Request $request)
	{
		$user = $this->get("security.context")->getToken()->getUser();
                $userx = $user->getUsername();
                $sucursal = $this->userSvc->getByUserUser($userx);
                
		$result = $this->searchSvc->getAll($request, $user,$sucursal);
		$serializer = SerializerBuilder::create()->build();
                $result = $serializer->serialize($result, 'json');
                $httpError = 200;
                $response = new Response($result, $httpError);
                $response->headers->set('Content-Type', 'application/json');
                return $response;
	}
        
        
        /**
	 * @Route("/searchresxveh/")
        * @Method({"GET","POST"})
        * 
        * @ApiDoc(
        * 		description = "La lista de todas las reservas por id de vehículo",
        * 		requirements = {},
            *      parameters={}
        * )
	 */
	public function getResXVehiculoAction(Request $request)
	{
		$user = $this->get("security.context")->getToken()->getUser();
		$result = $this->searchSvc->getResXVehiculo($request, $user);
		$serializer = SerializerBuilder::create()->build();
                $result = $serializer->serialize($result, 'json');
                $httpError = 200;
                $response = new Response($result, $httpError);
                $response->headers->set('Content-Type', 'application/json');
                return $response;
	}
	
	/**
	 * @Route("/insertar/")
	 * @Method({"POST"})
	 *
	 * @ApiDoc(
	 *          description = "Inserta un registro",
	 * 	   requirements = {{
	 *         "name"="descripcion",
	 *         "dataType"="string",
	 *         "requirement"="\d+",
	 *         "description"="Descripción de la reserva"
	 *		},
         *           {
	 *         		"name"="idcliente",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="id del cliente"
	 *			},
         *           {
	 *         		"name"="idmetodopago",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="id del método de pago de la reserva"
	 *			},
         *           {
	 *         		"name"="idvehiculo",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="id del vehículo"
	 *			}
         * }
	 * )
	 */
	public function insertarAction(Request $request)
	{
		
                $validator = $this->get('validator');
		$user = $this->get("security.context")->getToken()->getUser();
                //print_r($request);die();
                
                $userx = $user->getUsername();
                $sucursal = $this->userSvc->getByUserUser($userx);
                $result = $this->insertarSvc->insertar($request,$validator,$user,$sucursal);
                
		//$serializer = SerializerBuilder::create()->build();
		//$result = $serializer->serialize($result, 'json');
		$httpError = 200;
		$serializer = SerializerBuilder::create()->build();
		$result2 = $this->searchSvc->getAll($request,$user,$sucursal);
		if($result == "")
                {
                    $result2 = $serializer->serialize($result2, 'json');
                }
                else
                {
                    $result2 = $serializer->serialize($result, 'json');
                }

		$response = new Response($result2, $httpError);
		$response->headers->set('Content-Type', 'application/json');
		return $response;
	}
	
	/**
	 * @Route("/getone/")
	 * @Method({"GET","POST"})
	 *
	 * @ApiDoc(
	 * 		description = "Obtiene un objeto",
	 * 		requirements = {{
	 *		"name"="id",
	 *		"dataType"="integer",
	 *		"requirement"="\d+",
	 *		"description"="Id del objeto"
	 *		}},
	 *      parameters={ {"name"="id", "dataType"="integer", "required"=true, "description"="Id de la reserva"}}
	 * )
	 */
	public function getOneAction(Request $request)
	{
		$id = $request->query->get("id");
                //echo "id: ".$id;die();
                $result = $this->getoneSvc->getOne($id);
		$serializer = SerializerBuilder::create()->build();
		$result = $serializer->serialize($result, 'json');
		$httpError = 200;
		
		$response = new Response($result, $httpError);
		$response->headers->set('Content-Type', 'application/json');
		return $response;
	}
	
        
	/**
	 * @Route("/getreserva/")
	 * @Method({"GET","POST"})
	 *
	 * @ApiDoc(
	 * 		description = "Obtiene un objeto",
	 * 		requirements = {{
	 *		"name"="id",
	 *		"dataType"="integer",
	 *		"requirement"="\d+",
	 *		"description"="Id del objeto"
	 *		}},
	 *      parameters={ {"name"="id", "dataType"="integer", "required"=true, "description"="Id de la reserva"}}
	 * )
	 */
	public function getReservaAction(Request $request)
	{
		$id = $request->query->get("id");
                //echo "id: ".$id;die();
                $result = $this->getoneSvc->getReserva($id);
		$serializer = SerializerBuilder::create()->build();
		$result = $serializer->serialize($result, 'json');
		$httpError = 200;
		
		$response = new Response($result, $httpError);
		$response->headers->set('Content-Type', 'application/json');
		return $response;
	}
        
        
         /**
	 * @Route("/getoneadicionales/")
	 * @Method({"GET","POST"})
	 *
	 * @ApiDoc(
	 * 		description = "Obtiene un objeto",
	 * 		requirements = {{
	 *		"name"="id",
	 *		"dataType"="integer",
	 *		"requirement"="\d+",
	 *		"description"="Id del objeto"
	 *		}},
	 *      parameters={ {"name"="id", "dataType"="integer", "required"=true, "description"="Id de la reserva"}}
	 * )
	 */
	public function getOneArrayAction(Request $request)
	{
		$id = $request->query->get("id");
                //echo "id: ".$id;die();
                $result = $this->getoneSvcArray->getOneArray($id);
                
                
                //print_r($result);die();
                
		$serializer = SerializerBuilder::create()->build();
		$result = $serializer->serialize($result, 'json');
                
		$httpError = 200;
		
		$response = new Response($result, $httpError);
		$response->headers->set('Content-Type', 'application/json');
		return $response;
	}
        
        
          /**
	 * @Route("/addpasadicional/")
	 * @Method({"GET","POST"})
	 *
	 * @ApiDoc(
	 * 		description = "Obtiene un objeto",
	 * 		requirements = {{
	 *		"name"="id",
	 *		"dataType"="integer",
	 *		"requirement"="\d+",
	 *		"description"="Id del objeto"
	 *		}},
	 *      parameters={ {"name"="id", "dataType"="integer", "required"=true, "description"="Id de la reserva"}}
	 * )
	 */
	public function addPasAdicionalAction(Request $request)
	{
		//$id = $request->query->get("id");
                //echo "id: ".$id;die();
                $result = $this->addPasAdicionalSvc->addPasAdicional($request);
                
                
                //print_r($result);die();
                
		$serializer = SerializerBuilder::create()->build();
		$result = $serializer->serialize($result, 'json');
                
		$httpError = 200;
		
		$response = new Response($result, $httpError);
		$response->headers->set('Content-Type', 'application/json');
		return $response;
	}
        
        
	/**
	 * @Route("/hola/{name}", name="_demo_hello")
	 * @Template()
	 */
	public function helloAction($name)
	{
		return array('name' => $name);
	}
             	
}