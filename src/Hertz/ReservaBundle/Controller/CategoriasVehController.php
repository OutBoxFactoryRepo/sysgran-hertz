<?php
namespace Hertz\ReservaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
//use Acme\DemoBundle\Form\ContactType;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use JMS\SecurityExtraBundle\Annotation as Security;

// these import the "@Route" and "@Template" annotations
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\Serializer\SerializerBuilder;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use JMS\DiExtraBundle\Annotation as DI;
use Hertz\ReservaBundle\Service\CategoriasVehService;
use Symfony\Component\Validator;

class CategoriasVehController extends Controller
{
	
	/**
	 *
	 * @var UserService
	 * @DI\Inject("user.manager")
	 */
	private $userSvc;
	
	/**
	 *
	 * @var CategoriasVehSearchService
	 *
	 * @DI\Inject("CategoriasVehSearchService.Search")
	 */
	private $searchCategoriasVehSvc;
		
	/**
	 *
	 * @var CategoriasVehGetOneService
	 *
	 * @DI\Inject("CategoriasVehGetOneService.GetOne")
	 */
	private $CategoriasVehGetOneSvc;
	
	
	/**
	 *
	 * @var CategoriasVehCrearService
	 *
	 * @DI\Inject("CategoriasVehCrearService.Crear")
	 */
	private $CategoriasVehCrearSvc;
		
	
	public function indexAction()
	{
		return array();
	}

	/**
	 * @Route("/search/")
     * @Method({"GET","POST"})
     * 
     * @ApiDoc(
     * 		description = "La lista de todos los tipos de CategoriasVehes",
     * 		requirements = {},
	 *      parameters={}
     * )
	 */
	public function getAllAction(Request $request)
	{
		
		$userx = $this->get("security.context")->getToken()->getUser();
		$userx = $userx->getUsername();
		$sucursal = $this->userSvc->getByUserUser($userx);
		$sucursal = $sucursal->getSucursal();
		
		$result = $this->searchCategoriasVehSvc->getAll($request,$sucursal);
		$serializer = SerializerBuilder::create()->build();
		
		$totalRows = $result["total"];
                $totalPaginas = $result["totalPaginas"];
                $totalPaginasEntero = (int) $totalPaginas;
                
                if($totalPaginas > $totalPaginasEntero)
                {
                    $totalPaginas = (int)($totalPaginas + 1);
                }
                
		$arrayResultado = array("current"=>(int)$request->request->get("current"), "rowCount"=>$totalRows,"rows"=>$result["objeto"],"total"=>$totalPaginas);

		$result = $serializer->serialize($arrayResultado, 'json');
                $httpError = 200;
                $response = new Response($result, $httpError);
                
                $response->headers->set('Content-Type', 'application/json');
                return $response;
	}	
	
	/**
	 * @Route("/search/{id}/")
	 * @Method({"GET"})
	 *
	 * @ApiDoc(
	 * 		description = "obtiene un CategoriasVeh en particular",
	 * 		requirements = {},
	 *      parameters={}
	 * )
	 */
	public function getOneAction($id)
	{
		$result = $this->CategoriasVehGetOneSvc->getOne($id);
		$serializer = SerializerBuilder::create()->build();
		
		if($result == null)
		{
			$result = "CategoriasVeh no encontrado";
			$httpError = 404;
		}
		else
		{
			$httpError = 200;
		}
		$result = $serializer->serialize($result, 'json');
		$response = new Response($result, $httpError);
		$response->headers->set('Content-Type', 'application/json');
		return $response;
	}
	
	
	/**
	 * @Route("/crear/", name="categoriasVeh_crear")
	 * @Method({"GET","POST"})
	 * @ApiDoc(
	 * 		description = "crea un categoriasVeh",
	 * 		
	 * 		requirements = {
	 * 			{
	 *         		"name"="descripcion",
	 *         		"dataType"="string",
	 *         		"requirement"="string",
	 *         		"description"="descripción del tipo de adicional"
	 *			},
	 *			{
	 *         		"name"="valor",
	 *         		"dataType"="float",
	 *         		"requirement"="\d+",
	 *         		"description"="id de la agencia"
	 *			},
	 *			{
	 *         		"name"="debaja",
	 *         		"dataType"="boolean",
	 *         		"requirement"="string S o N",
	 *         		"description"="S está dado de baja"
	 *			}
	 *		},
	 *      parameters={}
	 * )
	 * @Template()
	 */
	public function crearAction(Request $request)
	{
		$validator = $this->get('validator');
		$user = $this->get("security.context")->getToken()->getUser();
		$result = $this->CategoriasVehCrearSvc->crear($request,$validator,$user);
		$serializer = SerializerBuilder::create()->build();
		$result = $serializer->serialize($result, 'json');
		$httpError = 200;
		$response = new Response($result, $httpError);
		$response->headers->set('Content-Type', 'application/json');
		return $response;
	}
}