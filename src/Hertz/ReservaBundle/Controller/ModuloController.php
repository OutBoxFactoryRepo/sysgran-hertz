<?php
namespace Hertz\ReservaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
//use Acme\DemoBundle\Form\ContactType;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use JMS\SecurityExtraBundle\Annotation as Security;

// these import the "@Route" and "@Template" annotations
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\Serializer\SerializerBuilder;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use JMS\DiExtraBundle\Annotation as DI;
use Hertz\ReservaBundle\Service\ModuloService;
use Symfony\Component\Validator;

use Hertz\UserBundle\Entity\User;
use Hertz\UserBundle\Form\Type\UserType;
use Hertz\UserBundle\Service\UserService;


class ModuloController extends Controller
{
	

	
	/**
	 *
	 * @var ModuloService
	 *
	 * @DI\Inject("ModuloSearchService.search")
	 */
	private $moduloSvc;
	
        
        /**
	 *
	 * @var ModuloCrearService
	 *
	 * @DI\Inject("ModuloCrearService.crear")
	 */
	private $moduloCrearSvc;
        
        
        /**
	 *
	 * @var ModuloRoles
	 *
	 * @DI\Inject("ModuloSearchService.roles")
	 */
	private $moduloSvcRoles;


        /**
	 *
	 * @var ModuloAddRoles
	 *
	 * @DI\Inject("ModuloEditarService.addrol")
	 */
	private $moduloAddRoles;
        
        
        /**
	 *
	 * @var ModuloDelRoles
	 *
	 * @DI\Inject("ModuloEditarService.delrol")
	 */
	private $moduloDelRoles;
        

        
     /**
     * @Route("/")
     * @Method({"GET","POST"})
     * 
     * @ApiDoc(
     * 		description = "Lista todos los módulos",
     * 		requirements = {},
	 *      parameters={}
     * )
	 */
	public function getAllAction(Request $request)
	{
		
		$result = $this->moduloSvc->getAll($request);
                
                $auxResult = array();
                foreach($result as $modulo)
                {
                    $auxResult[] = array(
                        "id"=>$modulo->getId()
                       ,"descripcion"=>$modulo->getDescripcion()
                       ,"nombre_js"=>$modulo->getNombreJs()
                       ,"debaja"=>$modulo->getDebaja()
                    );
                    
                }
                
		$serializer = SerializerBuilder::create()->build();
                $result = $serializer->serialize($auxResult, 'json');
                $httpError = 200;
                $response = new Response($result, $httpError);
                $response->headers->set('Content-Type', 'application/json');
                return $response;
	}
        
        
        /**
        * @Route("/permisos/")
        * @Method({"GET","POST"})
        * 
        * @ApiDoc(
        * 		description = "lista los módulos con permisos del usuario conectado",
        * 		requirements = {},
            *      parameters={}
        * )
	 */
	public function getAllAsginadosAction(Request $request)
	{
		
                $validator = $this->get('validator');
		$user = $this->get("security.context")->getToken();
		$result = $this->moduloSvc->getAllAsginados($request,$user);
		$serializer = SerializerBuilder::create()->build();
                $result = $serializer->serialize($result, 'json');
                $httpError = 200;
                $response = new Response($result, $httpError);
                $response->headers->set('Content-Type', 'application/json');
                return $response;
	}
	
	/**
	 * @Route("/crear/")
	 * @Method({"POST"})
	 *
	 * @ApiDoc(
	 *          description = "Inserta un registro",
	 * 	   requirements = {
         *          {
         *                      "name"="descripcion",
	 *                      "dataType"="string",
         *                     "requirement"="\d+",
	 *                      "description"="Descripción del módulo"
	 *          },
         *          {
         *                      "name"="nombreJs",
	 *                      "dataType"="string",
         *                     "requirement"="string",
	 *                      "description"="nombre del Js al que apunta el módulo"
	 *          },
         *          {
         *                      "name"="debaja",
	 *                      "dataType"="boolean",
         *                     "requirement"="boolean",
	 *                      "description"="si es true está dado de baja"
	 *          }
         * }
	 * )
	 */
	public function insertarAction(Request $request)
	{
		
                $validator = $this->get('validator');
		$user = $this->get("security.context")->getToken()->getUser();
                //print_r($request);die();
                $result = $this->moduloCrearSvc->crear($request,$validator,$user);
                

		$httpError = 200;
		$serializer = SerializerBuilder::create()->build();
		//$result2 = $this->moduloSvc->getAll($request);
		$result2 = $serializer->serialize($result, 'json');

		$response = new Response($result2, $httpError);
		$response->headers->set('Content-Type', 'application/json');
		return $response;
	}
	
	/**
	 * @Route("/getone/")
	 * @Method({"GET","POST"})
	 *
	 * @ApiDoc(
	 * 		description = "Obtiene un objeto",
	 * 		requirements = {{
	 *		"name"="id",
	 *		"dataType"="integer",
	 *		"requirement"="\d+",
	 *		"description"="Id del objeto"
	 *		}},
	 *      parameters={ {"name"="id", "dataType"="integer", "required"=true, "description"="Id de la reserva"}}
	 * )
	 */
	public function getOneAction(Request $request)
	{
		$id = $request->query->get("id");
                //echo "id: ".$id;die();
                $result = $this->getoneSvc->getOne($id);
		$serializer = SerializerBuilder::create()->build();
		$result = $serializer->serialize($result, 'json');
		$httpError = 200;
		
		$response = new Response($result, $httpError);
		$response->headers->set('Content-Type', 'application/json');
		return $response;
	}
	
        
        
        
         /**
	 * @Route("/getoneadicionales/")
	 * @Method({"GET","POST"})
	 *
	 * @ApiDoc(
	 * 		description = "Obtiene un objeto",
	 * 		requirements = {{
	 *		"name"="id",
	 *		"dataType"="integer",
	 *		"requirement"="\d+",
	 *		"description"="Id del objeto"
	 *		}},
	 *      parameters={ {"name"="id", "dataType"="integer", "required"=true, "description"="Id de la reserva"}}
	 * )
	 */
	public function getOneArrayAction(Request $request)
	{
		$id = $request->query->get("id");
                //echo "id: ".$id;die();
                $result = $this->getoneSvcArray->getOneArray($id);
                
                
                //print_r($result);die();
                
		$serializer = SerializerBuilder::create()->build();
		$result = $serializer->serialize($result, 'json');
                
		$httpError = 200;
		
		$response = new Response($result, $httpError);
		$response->headers->set('Content-Type', 'application/json');
		return $response;
	}
        
        
        
        
        

        /**
	 * @Route("/asignados/")
	 * @Method({"GET","POST"})
	 * @Method({"GET","POST"})
	 *
	 * @ApiDoc(
	 * 		description = "Obtiene un objeto",
	 * 		requirements = {{
	 *		"name"="idModulo",
	 *		"dataType"="integer",
	 *		"requirement"="\d+",
	 *		"description"="Id del objeto"
	 *		}}

	 * )
	 */
	public function getRolesAction(Request $request)
	{
 
                $validator = $this->get('validator');
		$user = $this->get("security.context")->getToken()->getUser();
                $result = $this->moduloSvcRoles->getRoles($request,$validator,$user);
                
                $auxRoles = array();
                foreach($result as $rol)
                {
                    $auxRoles[] = array(
                        "id"=>$rol->getId()
                       ,"description"=>$rol->getDescription()
                       ,"created"=>$rol->getCreated()
                       ,"name"=>$rol->getName()
                       ,"modified"=>$rol->getModified()
                    );
                }
                
                
		$serializer = SerializerBuilder::create()->build();
                $result = $serializer->serialize($auxRoles, 'json');
                $httpError = 200;
                $response = new Response($result, $httpError);
                $response->headers->set('Content-Type', 'application/json');
                return $response;
	}
        
        
        /**
	 * @Route("/addrol/")
	 * @Method({"GET","POST"})
	 * @Method({"GET","POST"})
	 *
	 * @ApiDoc(
	 * 		description = "Obtiene un objeto",
	 * 		requirements = {{
	 *		"name"="idModulo",
	 *		"dataType"="integer",
	 *		"requirement"="\d+",
	 *		"description"="Id del objeto"
	 *		}}

	 * )
	 */
	public function addRolAction(Request $request)
	{
 
                //echo "hola";die();
                $validator = $this->get('validator');
		$user = $this->get("security.context")->getToken()->getUser();
                $result = $this->moduloAddRoles->addRol($request,$validator,$user);
		$serializer = SerializerBuilder::create()->build();
                $result = $serializer->serialize($result, 'json');
                $httpError = 200;
                $response = new Response($result, $httpError);
                $response->headers->set('Content-Type', 'application/json');
                return $response;
	}
        
        
        /**
	 * @Route("/delrol/")
	 * @Method({"GET","POST"})
	 * @Method({"GET","POST"})
	 *
	 * @ApiDoc(
	 * 		description = "Obtiene un objeto",
	 * 		requirements = {{
	 *		"name"="idModulo",
	 *		"dataType"="integer",
	 *		"requirement"="\d+",
	 *		"description"="Id del objeto"
	 *		}}

	 * )
	 */
	public function delRolAction(Request $request)
	{
                $validator = $this->get('validator');
		$user = $this->get("security.context")->getToken()->getUser();
                $result = $this->moduloDelRoles->delRol($request,$validator,$user);
		$serializer = SerializerBuilder::create()->build();
                $result = $serializer->serialize($result, 'json');
                $httpError = 200;
                $response = new Response($result, $httpError);
                $response->headers->set('Content-Type', 'application/json');
                return $response;
	}            	
}