<?php
namespace Hertz\ReservaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
//use Acme\DemoBundle\Form\ContactType;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

// these import the "@Route" and "@Template" annotations
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\Serializer\SerializerBuilder;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use JMS\DiExtraBundle\Annotation as DI;
use Hertz\ReservaBundle\Service\CargoService;
use Symfony\Component\Validator;

class CargoController extends Controller
{
	
	/**
	 *
	 * @var CargoSearchService
	 *
	 * @DI\Inject("CargoSearchService.Search")
	 */
	private $searchCargoSvc;
		
	/**
	 *
	 * @var CargoGetOneService
	 *
	 * @DI\Inject("CargoGetOneService.GetOne")
	 */
	private $CargoGetOneSvc;
	
	/**
	 *
	 * @var CargoCrearService
	 *
	 * @DI\Inject("CargoCrearService.Crear")
	 */
	private $CargoCrearSvc;
	
	
	public function indexAction()
	{
		return array();
	}

	/**
	 * @Route("/search/")
     * @Method({"GET"})
     * 
     * @ApiDoc(
     * 		description = "La lista de todos los cargos",
     * 		requirements = {},
	 *      parameters={}
     * )
	 */
	public function getAllAction()
	{
		$result = $this->searchCargoSvc->getAll();
		$serializer = SerializerBuilder::create()->build();
    	$result = $serializer->serialize($result, 'json');
    	$httpError = 200;
    	$response = new Response($result, $httpError);
    	$response->headers->set('Content-Type', 'application/json');
    	return $response;
	}
	
	/**
	 * @Route("/{id}/search/")
	 * @Method({"GET"})
	 *
	 * @ApiDoc(
	 * 		description = "obtiene un cargo en particular",
	 * 		requirements = {},
	 *      parameters={}
	 * )
	 */
	public function getOneAction($id)
	{
		$result = $this->CargoGetOneSvc->getOne($id);
		$serializer = SerializerBuilder::create()->build();
		
		if($result == null)
		{
			$result = "Cargo no encontrado";
			$httpError = 404;
		}
		else
		{
			$httpError = 200;
		}
		$result = $serializer->serialize($result, 'json');
		$response = new Response($result, $httpError);
		$response->headers->set('Content-Type', 'application/json');
		return $response;
	}
	
	/**
	 * @Route("/crear/", name="cargo_crear")
	 *
	 * @ApiDoc(
	 * 		description = "crea un Cargo nuevo",
	 * 		
	 * 		requirements = {
	 *			{
	 *         		"name"="descripcion",
	 *         		"dataType"="string",
	 *         		"requirement"="string",
	 *         		"description"="descripción del cargo"
	 *			},
	 *			{
	 *         		"name"="valor",
	 *         		"dataType"="numeric",
	 *         		"requirement"="\d+",
	 *         		"description"="Valor del Cargo"
	 *			}
	 *		},
	 *      parameters={}
	 * )
	 * @Template()
	 */
	public function crearAction(Request $request)
	{
		$validator = $this->get('validator');
		$result = $this->CargoCrearSvc->crear($request,$validator);
		$serializer = SerializerBuilder::create()->build();
		$result = $serializer->serialize($result, 'json');
		$httpError = 200;
		$response = new Response($result, $httpError);
		$response->headers->set('Content-Type', 'application/json');
		return $response;
	}
	
	
	/**
	 * @Route("/{id}/editar/", name="cargo_editar")
	 *
	 * @ApiDoc(
	 * 		description = "Edita un cargo",
	 *
	 * 		requirements = {
	 *			{
	 *         		"name"="descripcion",
	 *         		"dataType"="string",
	 *         		"requirement"="string",
	 *         		"description"="descripción del cargo"
	 *			},
	 *			{
	 *         		"name"="valor",
	 *         		"dataType"="numeric",
	 *         		"requirement"="\d+",
	 *         		"description"="Valor del Cargo"
	 *			},
	 *			{
	 *         		"name"="debaja",
	 *         		"dataType"="boolean",
	 *         		"requirement"="string",
	 *         		"description"="True está dado de baja"
	 *			}
	 *		},
	 *      parameters={}
	 * )
	 * @Template()
	 */
	public function editarAction(Request $request,$id)
	{
		$validator = $this->get('validator');
		$result = $this->CargoCrearSvc->editar($request,$id,$validator);
		$serializer = SerializerBuilder::create()->build();
		if($result == null)
		{
			$result = "Cargo no encontrado";
			$httpError = 404;
		}
		else
		{
			$httpError = 200;
		}
		$result = $serializer->serialize($result, 'json');
		$response = new Response($result, $httpError);
		$response->headers->set('Content-Type', 'application/json');
		return $response;
	}
	
}