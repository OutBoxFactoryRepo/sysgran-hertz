<?php
namespace Hertz\ReservaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
//use Acme\DemoBundle\Form\ContactType;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

// these import the "@Route" and "@Template" annotations
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\Serializer\SerializerBuilder;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use JMS\DiExtraBundle\Annotation as DI;
use Hertz\ReservaBundle\Service\ClienteOtrosDatosService;

class ClienteOtrosDatosController extends Controller
{
	
	/**
	 *
	 * @var ClienteOtrosDatosSearchService
	 *
	 * @DI\Inject("ClienteOtrosDatosSearchService.Search")
	 */
	private $searchClienteOtrosDatosSvc;
        
        
        	
	/**
	 *
	 * @var ClienteOtrosDatosSearchService
	 *
	 * @DI\Inject("ClienteOtrosDatosSearchService.getByNombres")
	 */
	private $searchNombresSvc;
        

	/**
	 *
	 * @var ClienteOtrosDatosGetOneService
	 *
	 * @DI\Inject("ClienteOtrosDatosGetOneService.GetOne")
	 */
	private $ClienteOtrosDatosGetOneSvc;
	
	/**
	 *
	 * @var ClienteOtrosDatosCrearService
	 *
	 * @DI\Inject("ClienteOtrosDatosCrearService.Crear")
	 */
	private $ClienteOtrosDatosCrearSvc;
	
	
	public function indexAction()
	{
		 /*
         * The action's view can be rendered using render() method
         * or @Template annotation as demonstrated in DemoController.
         *
         */
        return array();
	}

	/**
	* @Route("/search/")
        * @Method({"GET","POST"})
        * 
        * @ApiDoc(
        * 		description = "La lista de todos los clientes",
        * 		requirements = {},
        *      parameters={}
        * )
	 */
	public function getAllAction()
	{
            $result = $this->searchClienteOtrosDatosSvc->getAll();
            $serializer = SerializerBuilder::create()->build();
            $result = $serializer->serialize($result, 'json');
            $httpError = 200;
            $response = new Response($result, $httpError);
            $response->headers->set('Content-Type', 'application/json');
            return $response;
	}
	
	/**
	 * @Route("/getone/")
	 * @Method({"GET","POST"})
	 *
	 * @ApiDoc(
	 * 		description = "obtiene un cliente en particular",
	 * 		requirements = {},
	 *      parameters={}
	 * )
	 */
	public function getOneAction(Request $request)
	{
		
                
                $id = $request->query->get("id");
		$result = $this->ClienteOtrosDatosGetOneSvc->getOne($id);
		$serializer = SerializerBuilder::create()->build();
		$result = $serializer->serialize($result, 'json');
		$httpError = 200;
		$response = new Response($result, $httpError);
		$response->headers->set('Content-Type', 'application/json');
		return $response;
		
		//return $this->render(
		//		'HertzReservaBundle:Reserva:getOne.html.twig',
		//		array("respuesta"=>$result)
		//);
	}
        
        
     
            
        /**
	 * @Route("/searchbyname/")
	 * @Method({"GET"})
	 *
	 * @ApiDoc(
	 * 		description = "obtiene una lista de clientes por nombre o apellido",
	 * 		requirements = {{
	 *         "name"="term",
	 *         "dataType"="string",
	 *         "requirement"="\d+",
	 *         "description"="o apellido del cliente"
	 *			}},
	 *      parameters={}
	 * )
	 */
	public function getByNombresAction(Request $request)
	{
		$word = $request->query->get("term");
                
		$result = $this->searchNombresSvc->getByNombres($word);
                
		$serializer = SerializerBuilder::create()->build();
                $result = $serializer->serialize($result, 'json');
                
                
                $httpError = 200;
                $response = new Response($result, $httpError);
                $response->headers->set('Content-Type', 'application/json');
                return $response;
	}
	
	/**
	 * @Route("/crear/", name="clienteotd_crear")
	 *
	 * @ApiDoc(
	 * 		description = "crea un nuevo cliente",
	 * 		
	 * 		requirements = {{
	 *         "name"="nombre",
	 *         "dataType"="string",
	 *         "requirement"="\d+",
	 *         "description"="nombre del cliente"
	 *			},
	 *			{
	 *         		"name"="apellido",
	 *         		"dataType"="string",
	 *         		"requirement"="\d+",
	 *         		"description"="apellido del cliente"
	 *			}
	 *			,
	 *			{
	 *         		"name"="fechaNac",
	 *         		"dataType"="datetime",
	 *         		"requirement"="\d+",
	 *         		"description"="fecha de nacimiento del cliente"
	 *			},
	 *			{
	 *         		"name"="email",
	 *         		"dataType"="string",
	 *         		"requirement"="\d+",
	 *         		"description"="email del cliente"
	 *			},
	 *			{
	 *         		"name"="licencia",
	 *         		"dataType"="string",
	 *         		"requirement"="\d+",
	 *         		"description"="licencia de conductor del cliente"
	 *			},
	 *			{
	 *         		"name"="licFecVto",
	 *         		"dataType"="datetime",
	 *         		"requirement"="\d+",
	 *         		"description"="Fecha de vencimiento de la licencia de conductor del cliente"
	 *			},
	 *			{
	 *         		"name"="nroTarjetaCre",
	 *         		"dataType"="string",
	 *         		"requirement"="\d+",
	 *         		"description"="Número de tarjeta de crédito del cliente"
	 *			},
	 *			{
	 *         		"name"="tarjFecVto",
	 *         		"dataType"="datetime",
	 *         		"requirement"="\d+",
	 *         		"description"="Fecha de vencimiento de la tarjeta de crédito del cliente"
	 *			},
	 *			{
	 *         		"name"="cAut",
	 *         		"dataType"="string",
	 *         		"requirement"="\d+",
	 *         		"description"="campo cAut no tengo confirmada su procedencia"
	 *			}
	 *			,
	 *			{
	 *         		"name"="cs",
	 *         		"dataType"="string",
	 *         		"requirement"="\d+",
	 *         		"description"="campo cs no tengo confirmada su procedencia"
	 *			},
	 *			{
	 *         		"name"="tipodoc",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="tipo de documento"
	 *			},
	 *			{
	 *         		"name"="numeroDoc",
	 *         		"dataType"="string",
	 *         		"requirement"="\d+",
	 *         		"description"="Número de Documento"
	 *			},
	 *			{
	 *         		"name"="cuit",
	 *         		"dataType"="string",
	 *         		"requirement"="\d+",
	 *         		"description"="Número de cuit"
	 *			}
	 *			,
	 *			{
	 *         		"name"="idPais",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="Id del país del cliente"
	 *			},
	 *			{
	 *         		"name"="debaja",
	 *         		"dataType"="boolean",
	 *         		"requirement"="\d+",
	 *         		"description"="True = cliente dado de baja"
	 *			},
	 *			{
	 *         		"name"="tipoClienteOtrosDatos",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="Tipo de cliente"
	 *			}
	 *			 ,
	 *			{
	 *         		"name"="domicilio",
	 *         		"dataType"="string",
	 *         		"requirement"="\d+",
	 *         		"description"="domicilio del cliente"
	 *			},
	 *			{
	 *         		"name"="localidad",
	 *         		"dataType"="string",
	 *         		"requirement"="\d+",
	 *         		"description"="localidad del cliente"
	 *			}
	 *			,
	 *			{
	 *         		"name"="codigoPostal",
	 *         		"dataType"="string",
	 *         		"requirement"="\d+",
	 *         		"description"="C.P. del cliente"
	 *			},
	 *			{
	 *         		"name"="provincia",
	 *         		"dataType"="string",
	 *         		"requirement"="\d+",
	 *         		"description"="provincia o estado del cliente"
	 *			},
	 *			{
	 *         		"name"="condicionIva",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="condicion de iva del cliente"
	 *			}
	 *			,
	 *			{
	 *         		"name"="idIIBB",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="Tipo de Ingresos brutos"
	 *			}
	 *			,
	 *			{
	 *         		"name"="nroIIBB",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="Número de ingresos brutos"
	 *			}
	 *		},
	 *      parameters={}
	 * )
	 * @Template()
	 */
	public function crearAction(Request $request)
	{
		
                $result = $this->ClienteOtrosDatosCrearSvc->crear($request);
                
		$serializer = SerializerBuilder::create()->build();
		$result = $serializer->serialize($result, 'json');
		$httpError = 200;
		$response = new Response($result, $httpError);
		$response->headers->set('Content-Type', 'application/json');
		return $response;
	}
	
        /**
         * @Route("/eliminar/", name="eliminar_COD")
	 *
	 * @ApiDoc(
	 * 		description = "crea un nuevo cliente",
	 * 		
	 * 		requirements = {
	 *		},
	 *      parameters={}
	 * )
	 * @Template()
	 */
	public function eliminarAction(Request $request)
	{
		
                $result = $this->ClienteOtrosDatosCrearSvc->eliminar($request);
                
		$serializer = SerializerBuilder::create()->build();
		$result = $serializer->serialize($result, 'json');
		$httpError = 200;
		$response = new Response($result, $httpError);
		$response->headers->set('Content-Type', 'application/json');
		return $response;
	}
        
	/**
	 * @Route("/{id}/editar/", name="clienteotd_editar")
	 *
	 * @ApiDoc(
	 * 		description = "edita un cliente",
	 *
	 * 		requirements = {{
	 *         "name"="nombre",
	 *         "dataType"="string",
	 *         "requirement"="\d+",
	 *         "description"="nombre del cliente"
	 *			},
	 *			{
	 *         		"name"="apellido",
	 *         		"dataType"="string",
	 *         		"requirement"="\d+",
	 *         		"description"="apellido del cliente"
	 *			}
	 *			,
	 *			{
	 *         		"name"="fechaNac",
	 *         		"dataType"="datetime",
	 *         		"requirement"="\d+",
	 *         		"description"="fecha de nacimiento del cliente"
	 *			},
	 *			{
	 *         		"name"="email",
	 *         		"dataType"="string",
	 *         		"requirement"="\d+",
	 *         		"description"="email del cliente"
	 *			},
	 *			{
	 *         		"name"="licencia",
	 *         		"dataType"="string",
	 *         		"requirement"="\d+",
	 *         		"description"="licencia de conductor del cliente"
	 *			},
	 *			{
	 *         		"name"="licFecVto",
	 *         		"dataType"="datetime",
	 *         		"requirement"="\d+",
	 *         		"description"="Fecha de vencimiento de la licencia de conductor del cliente"
	 *			},
	 *			{
	 *         		"name"="nroTarjetaCre",
	 *         		"dataType"="string",
	 *         		"requirement"="\d+",
	 *         		"description"="Número de tarjeta de crédito del cliente"
	 *			},
	 *			{
	 *         		"name"="tarjFecVto",
	 *         		"dataType"="datetime",
	 *         		"requirement"="\d+",
	 *         		"description"="Fecha de vencimiento de la tarjeta de crédito del cliente"
	 *			},
	 *			{
	 *         		"name"="cAut",
	 *         		"dataType"="string",
	 *         		"requirement"="\d+",
	 *         		"description"="campo cAut no tengo confirmada su procedencia"
	 *			}
	 *			,
	 *			{
	 *         		"name"="cs",
	 *         		"dataType"="string",
	 *         		"requirement"="\d+",
	 *         		"description"="campo cs no tengo confirmada su procedencia"
	 *			},
	 *			{
	 *         		"name"="tipodoc",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="tipo de documento"
	 *			},
	 *			{
	 *         		"name"="numeroDoc",
	 *         		"dataType"="string",
	 *         		"requirement"="\d+",
	 *         		"description"="Número de Documento"
	 *			},
	 *			{
	 *         		"name"="cuit",
	 *         		"dataType"="string",
	 *         		"requirement"="\d+",
	 *         		"description"="Número de cuit"
	 *			}
	 *			,
	 *			{
	 *         		"name"="idPais",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="Id del país del cliente"
	 *			},
	 *			{
	 *         		"name"="debaja",
	 *         		"dataType"="boolean",
	 *         		"requirement"="\d+",
	 *         		"description"="True = cliente dado de baja"
	 *			},
	 *			{
	 *         		"name"="tipoClienteOtrosDatos",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="Tipo de cliente"
	 *			}
	 *			 ,
	 *			{
	 *         		"name"="domicilio",
	 *         		"dataType"="string",
	 *         		"requirement"="\d+",
	 *         		"description"="domicilio del cliente"
	 *			},
	 *			{
	 *         		"name"="localidad",
	 *         		"dataType"="string",
	 *         		"requirement"="\d+",
	 *         		"description"="localidad del cliente"
	 *			}
	 *			,
	 *			{
	 *         		"name"="codigoPostal",
	 *         		"dataType"="string",
	 *         		"requirement"="\d+",
	 *         		"description"="C.P. del cliente"
	 *			},
	 *			{
	 *         		"name"="provincia",
	 *         		"dataType"="string",
	 *         		"requirement"="\d+",
	 *         		"description"="provincia o estado del cliente"
	 *			},
	 *			{
	 *         		"name"="condicionIva",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="condicion de iva del cliente"
	 *			} ,
	 *			{
	 *         		"name"="idIIBB",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="Tipo de Ingresos brutos"
	 *			}
	 *			,
	 *			{
	 *         		"name"="nroIIBB",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="Número de ingresos brutos"
	 *			}
	 *
	 *		},
	 *      parameters={{"name"="id", "dataType"="integer", "required"=true, "description"="Id del cliente a modificar"}}
	 * )
	 * @Template()
	 */
	public function editarAction(Request $request,$id)
	{
		$result = $this->ClienteOtrosDatosCrearSvc->editar($request,$id);
		$serializer = SerializerBuilder::create()->build();
		$result = $serializer->serialize($result, 'json');
		$httpError = 200;
		$response = new Response($result, $httpError);
		$response->headers->set('Content-Type', 'application/json');
		return $response;
	}
	
}