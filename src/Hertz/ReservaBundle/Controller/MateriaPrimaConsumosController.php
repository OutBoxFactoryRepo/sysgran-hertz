<?php
namespace Hertz\ReservaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
//use Acme\DemoBundle\Form\ContactType;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use JMS\SecurityExtraBundle\Annotation as Security;

// these import the "@Route" and "@Template" annotations
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\Serializer\SerializerBuilder;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use JMS\DiExtraBundle\Annotation as DI;
use Hertz\ReservaBundle\Service\MateriaPrimaConsumosService;
use Symfony\Component\Validator;

class MateriaPrimaConsumosController extends Controller
{
	
	/**
	 *
	 * @var UserService
	 * @DI\Inject("user.manager")
	 */
	private $userSvc;
	
	/**
	 *
	 * @var MateriaPrimaConsumosSearchService
	 *
	 * @DI\Inject("MateriaPrimaConsumosSearchService.Search")
	 */
	private $searchMateriaPrimaConsumosSvc;
		
	/**
	 *
	 * @var MateriaPrimaConsumosGetOneService
	 *
	 * @DI\Inject("MateriaPrimaConsumosGetOneService.GetOne")
	 */
	private $MateriaPrimaConsumosGetOneSvc;
	
	/**
	 *
	 * @var MateriaPrimaConsumosCrearService
	 *
	 * @DI\Inject("MateriaPrimaConsumosCrearService.Crear")
	 */
	private $MateriaPrimaConsumosCrearSvc;
	
	
	public function indexAction()
	{
		return array();
	}

	/**
	 * @Route("/search/")
     * @Method({"GET","POST"})
     * 
     * @ApiDoc(
     * 		description = "La lista de todos los tipos de MateriaPrimaConsumoses",
     * 		requirements = {},
	 *      parameters={}
     * )
	 */
	public function getAllAction(Request $request)
	{

		$result = $this->searchMateriaPrimaConsumosSvc->getAll($request);
		$serializer = SerializerBuilder::create()->build();
    	$totalRows = $result["total"];

    	$arrayResultado = array("success"=>true,"results"=>$totalRows,"rows"=>$result["objeto"]);
    	
		$result = $serializer->serialize($arrayResultado, 'json');
    	$httpError = 200;
    	$response = new Response($result, $httpError);
    	$response->headers->set('Content-Type', 'application/json');
    	return $response;
	}
	
	
	/**
	 * @Route("/searchcombo/")
     * @Method({"GET","POST"})
     * 
     * @ApiDoc(
     * 		description = "La lista de todos los tipos de MateriaPrimaConsumoses por usuario",
     * 		requirements = {},
	 *      parameters={}
     * )
	 */
	public function getAllComboAction(Request $request)
	{
		$userx = $this->get("security.context")->getToken()->getUser();
		$userx = $userx->getUsername();
		
		$sucursal = $this->userSvc->getByUserUser($userx);
		
		if(!$sucursal)
		{
			echo "no existe el usuario";die();
		}
		$sucursalx = $sucursal->getSucursal();
		
		$result = $this->searchMateriaPrimaConsumosSvc->getAllCombo($request,$sucursalx);
		$serializer = SerializerBuilder::create()->build();
    	$totalRows = $result["total"];

    	$arrayResultado = array("success"=>true,"results"=>$totalRows,"rows"=>$result["objeto"]);
    	
		$result = $serializer->serialize($arrayResultado, 'json');
    	$httpError = 200;
    	$response = new Response($result, $httpError);
    	$response->headers->set('Content-Type', 'application/json');
    	return $response;
	}
	
	
	/**
	 * @Route("/{id}/search/")
	 * @Method({"GET"})
	 *
	 * @ApiDoc(
	 * 		description = "obtiene un MateriaPrimaConsumos en particular",
	 * 		requirements = {},
	 *      parameters={}
	 * )
	 */
	public function getOneAction($id)
	{
		$result = $this->MateriaPrimaConsumosGetOneSvc->getOne($id);
		$serializer = SerializerBuilder::create()->build();
		
		if($result == null)
		{
			$result = "MateriaPrimaConsumos no encontrado";
			$httpError = 404;
		}
		else
		{
			$httpError = 200;
		}
		$result = $serializer->serialize($result, 'json');
		$response = new Response($result, $httpError);
		$response->headers->set('Content-Type', 'application/json');
		return $response;
	}
	
	/**
	 * @Route("/crear/", name="MateriaPrimaConsumos_crear")
	 * @Method({"GET","POST"})
	 * @ApiDoc(
	 * 		description = "crea un MateriaPrimaConsumos",
	 * 		
	 * 		requirements = {
	 * 			{
	 *         		"name"="descripcion",
	 *         		"dataType"="string",
	 *         		"requirement"="string",
	 *         		"description"="descripción del tipo de MateriaPrimaConsumos"
	 *			},
	 *			{
	 *         		"name"="fechaCarga",
	 *         		"dataType"="datetime",
	 *         		"requirement"="dd/mm/yyyy",
	 *         		"description"="fecha de carga del MateriaPrimaConsumos"
	 *			},
	 *			{
	 *         		"name"="cantidad",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="cantidad de MateriaPrimaConsumoses"
	 *			},
	 *			{
	 *         		"name"="valor",
	 *         		"dataType"="float",
	 *         		"requirement"="\d+",
	 *         		"description"="valor, si el tipo de MateriaPrimaConsumos es gravado se le suma el % de la tasa de aeropuerto"
	 *			},
	 *			{
	 *         		"name"="tipoMateriaPrimaConsumos",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="fecha de carga del MateriaPrimaConsumos"
	 *			},
	 *			{
	 *         		"name"="usuario",
	 *         		"dataType"="string",
	 *         		"requirement"="string",
	 *         		"description"="usuario logueado"
	 *			}
	 *		},
	 *      parameters={}
	 * )
	 * @Template()
	 */
	public function crearAction(Request $request)
	{

		$validator = $this->get('validator');
		$user = $this->get("security.context")->getToken()->getUser();
		$serializer = SerializerBuilder::create()->build();
                
                $oper = $request->request->get("oper");
                if($oper=="edit")
                {
                    
                    $result = $this->MateriaPrimaConsumosCrearSvc->editar($request,$validator,$user);
                }
                elseif($oper=="del")
                {
                    $result = $this->MateriaPrimaConsumosCrearSvc->delete($request,$validator,$user);
                }
                else
                {
                    $result = $this->MateriaPrimaConsumosCrearSvc->crear($request,$validator,$user);
                }

		$result = $serializer->serialize($result, 'json');
                
                /*          
                $result = '{
	"error": "Integer should be a positive"
                }';
                
                */
                $httpError = 200;
                $response = new Response($result, $httpError);
                $response->headers->set('Content-Type', 'application/json');
                return $response;		
	}
	
	
	/**
	 * @Route("/editar/", name="MateriaPrimaConsumos_editar")
	 * @Method({"GET","POST"})
	 * @ApiDoc(
	 * 		description = "Edita un MateriaPrimaConsumos",
	 * 		requirements = {
	 *			{
	 *         		"name"="id",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="id del MateriaPrimaConsumos"
	 *			},{
	 *         		"name"="descripcion",
	 *         		"dataType"="string",
	 *         		"requirement"="string",
	 *         		"description"="descripción del tipo de MateriaPrimaConsumos"
	 *			},
	 *			{
	 *         		"name"="fechaCarga",
	 *         		"dataType"="datetime",
	 *         		"requirement"="dd/mm/yyyy",
	 *         		"description"="fecha de carga del MateriaPrimaConsumos"
	 *			},
	 *			{
	 *         		"name"="cantidad",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="cantidad de MateriaPrimaConsumoses"
	 *			},
	 *			{
	 *         		"name"="valor",
	 *         		"dataType"="float",
	 *         		"requirement"="\d+",
	 *         		"description"="valor, si el tipo de MateriaPrimaConsumos es gravado se le suma el % de la tasa de aeropuerto"
	 *			},
	 *			{
	 *         		"name"="tipoMateriaPrimaConsumos",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="Tipo de MateriaPrimaConsumos"
	 *			},
	 *			{
	 *         		"name"="debaja",
	 *         		"dataType"="boolean",
	 *         		"requirement"="string S o N",
	 *         		"description"="S está dado de baja"
	 *			},
	 *			{
	 *         		"name"="usuario",
	 *         		"dataType"="string",
	 *         		"requirement"="string",
	 *         		"description"="usuario logueado"
	 *			}
	 *
	 *		},
	 *      parameters={}
	 * )
	 * @Template()
	 */
	public function editarAction(Request $request)
	{
		$validator = $this->get('validator');
		$user = $this->get("security.context")->getToken()->getUser();

		
		$result = $this->MateriaPrimaConsumosCrearSvc->editar($request,$validator,$user);
		$serializer = SerializerBuilder::create()->build();
		if($result == null)
		{
			$result = "MateriaPrimaConsumos no encontrado";
			$httpError = 404;
		}
		else
		{
			$httpError = 200;
		}
		$result = $serializer->serialize($result, 'json');
                
		$response = new Response($result, $httpError);
		$response->headers->set('Content-Type', 'application/json');
		return $response;
	}
	
}