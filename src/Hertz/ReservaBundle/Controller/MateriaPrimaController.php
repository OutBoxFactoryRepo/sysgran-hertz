<?php
namespace Hertz\ReservaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
//use Acme\DemoBundle\Form\ContactType;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use JMS\SecurityExtraBundle\Annotation as Security;

// these import the "@Route" and "@Template" annotations
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\Serializer\SerializerBuilder;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use JMS\DiExtraBundle\Annotation as DI;
use Hertz\ReservaBundle\Service\MateriaPrimaService;
use Symfony\Component\Validator;

class MateriaPrimaController extends Controller
{
	
	/**
	 *
	 * @var UserService
	 * @DI\Inject("user.manager")
	 */
	private $userSvc;
	
	/**
	 *
	 * @var MateriaPrimaSearchService
	 *
	 * @DI\Inject("MateriaPrimaSearchService.Search")
	 */
	private $searchMateriaPrimaSvc;
		
	/**
	 *
	 * @var MateriaPrimaGetOneService
	 *
	 * @DI\Inject("MateriaPrimaGetOneService.GetOne")
	 */
	private $MateriaPrimaGetOneSvc;
	
	/**
	 *
	 * @var MateriaPrimaCrearService
	 *
	 * @DI\Inject("MateriaPrimaCrearService.Crear")
	 */
	private $MateriaPrimaCrearSvc;

    /**
     *
     * @var MateriaPrimaCrearService
     *
     * @DI\Inject("MateriaPrimaGetAllByCategoryService.GetAllByCategory")
     */
    private $MateriaPrimaGetAllByCategorySvc;
	
	
	public function indexAction()
	{
		return array();
	}

	/**
	 * @Route("/search/")
     * @Method({"GET","POST"})
     * 
     * @ApiDoc(
     * 		description = "La lista de todos los tipos de MateriaPrimaes",
     * 		requirements = {},
	 *      parameters={}
     * )
	 */
	public function getAllAction(Request $request)
	{

		
		$user = $this->get("security.context")->getToken()->getUser();
		$userx = $user->getUsername();
		$sucursal = $this->userSvc->getByUserUser($userx);
		
		$sucursal = $sucursal->getSucursal();
		
		$result = $this->searchMateriaPrimaSvc->getAll($request,$sucursal,$user);
		
		$serializer = SerializerBuilder::create()->build();
                

                
                
                
                $totalRows = $result["total"];
                $totalPaginas = $result["totalPaginas"];
                $totalPaginasEntero = (int) $totalPaginas;
                
                if($totalPaginas > $totalPaginasEntero)
                {
                    $totalPaginas = (int)($totalPaginas + 1);
                }
                
		$arrayResultado = array("current"=>(int)$request->request->get("current"), "rowCount"=>$totalRows,"rows"=>$result["objeto"],"total"=>$totalPaginas);
                
    	
		$result = $serializer->serialize($arrayResultado, 'json');
                $httpError = 200;
                $response = new Response($result, $httpError);
                $response->headers->set('Content-Type', 'application/json');
                return $response;
	}
	
	
	/**
	 * @Route("/searchcombo/")
     * @Method({"GET","POST"})
     * 
     * @ApiDoc(
     * 		description = "La lista de todos los tipos de MateriaPrimaes por usuario",
     * 		requirements = {},
	 *      parameters={}
     * )
	 */
	public function getAllComboAction(Request $request)
	{
		$userx = $this->get("security.context")->getToken()->getUser();
		$userx = $userx->getUsername();
		
		$sucursal = $this->userSvc->getByUserUser($userx);
		
		if(!$sucursal)
		{
			echo "no existe el usuario";die();
		}
		$sucursalx = $sucursal->getSucursal();
		
		$result = $this->searchMateriaPrimaSvc->getAllCombo($request,$sucursalx);
		$serializer = SerializerBuilder::create()->build();
                $totalRows = $result["total"];

                $arrayResultado = array("success"=>true,"results"=>$totalRows,"rows"=>$result["objeto"]);

                        $result = $serializer->serialize($arrayResultado, 'json');
                $httpError = 200;
                $response = new Response($result, $httpError);
                $response->headers->set('Content-Type', 'application/json');
                return $response;
	}
	
	
	/**
	 * @Route("/{id}/search/")
	 * @Method({"GET"})
	 *
	 * @ApiDoc(
	 * 		description = "obtiene un MateriaPrima en particular",
	 * 		requirements = {},
	 *      parameters={}
	 * )
	 */
	public function getOneAction($id)
	{
		$result = $this->MateriaPrimaGetOneSvc->getOne($id);
		$serializer = SerializerBuilder::create()->build();
		
		if($result == null)
		{
			$result = "MateriaPrima no encontrado";
			$httpError = 404;
		}
		else
		{
			$httpError = 200;
		}
		$result = $serializer->serialize($result, 'json');
		$response = new Response($result, $httpError);
		$response->headers->set('Content-Type', 'application/json');
		return $response;
	}

	/**
	 * @Route("/searchbycategory/")
	 * @Method({"GET"})
	 *
	 * @ApiDoc(
	 * 		description = "obtiene un todas las materias primas de la categoria especificada.",
	 * 		requirements = {},
	 *      parameters={}
	 * )
	 */
	public function getAllByCategoryAction(Request $request)
	{
	    $category_id = $request->query->get('categoria_id');
	    $term = $request->query->get('term');
            
            $userx = $this->get("security.context")->getToken()->getUser();
            $userx2 = $this->get("security.context")->getToken()->getUser();
            $userx = $userx->getUsername();
            
            $sucursal = $this->userSvc->getByUserUser($userx);
	    //echo '<pre>';print_r($category_id);exit;
		$result = $this->MateriaPrimaGetAllByCategorySvc->getAllByCategory($category_id, $term,$sucursal,$userx2);
		$serializer = SerializerBuilder::create()->build();

		if($result == null)
		{
//			$result = "Categoría no encontrada";
			$result = array();
			$httpError = 200;
		}
		else
		{
			$httpError = 200;
		}
		$result = $serializer->serialize($result, 'json');
		$response = new Response($result, $httpError);
		$response->headers->set('Content-Type', 'application/json');
		return $response;
	}
	
	/**
	 * @Route("/crear/", name="MateriaPrima_crear")
	 * @Method({"GET","POST"})
	 * @ApiDoc(
	 * 		description = "crea un MateriaPrima",
	 * 		
	 * 		requirements = {
	 * 			{
	 *         		"name"="descripcion",
	 *         		"dataType"="string",
	 *         		"requirement"="string",
	 *         		"description"="descripción del tipo de MateriaPrima"
	 *			},
	 *			{
	 *         		"name"="fechaCarga",
	 *         		"dataType"="datetime",
	 *         		"requirement"="dd/mm/yyyy",
	 *         		"description"="fecha de carga del MateriaPrima"
	 *			},
	 *			{
	 *         		"name"="cantidad",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="cantidad de MateriaPrimaes"
	 *			},
	 *			{
	 *         		"name"="valor",
	 *         		"dataType"="float",
	 *         		"requirement"="\d+",
	 *         		"description"="valor, si el tipo de MateriaPrima es gravado se le suma el % de la tasa de aeropuerto"
	 *			},
	 *			{
	 *         		"name"="tipoMateriaPrima",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="fecha de carga del MateriaPrima"
	 *			},
	 *			{
	 *         		"name"="usuario",
	 *         		"dataType"="string",
	 *         		"requirement"="string",
	 *         		"description"="usuario logueado"
	 *			}
	 *		},
	 *      parameters={}
	 * )
	 * @Template()
	 */
	public function crearAction(Request $request)
	{
                $serializer = SerializerBuilder::create()->build();
		$validator = $this->get('validator');
		$user = $this->get("security.context")->getToken()->getUser();
			
		$result = $this->MateriaPrimaCrearSvc->crear($request,$validator,$user);
		

    	
    	
                $result = $serializer->serialize($result, 'json');
                $httpError = 200;
                $response = new Response($result, $httpError);
                $response->headers->set('Content-Type', 'application/json');
                return $response;
		
	}
	
	
	/**
	 * @Route("/editar/", name="MateriaPrima_editar")
	 * @Method({"GET","POST"})
	 * @ApiDoc(
	 * 		description = "Edita un MateriaPrima",
	 * 		requirements = {
	 *			{
	 *         		"name"="id",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="id del MateriaPrima"
	 *			},{
	 *         		"name"="descripcion",
	 *         		"dataType"="string",
	 *         		"requirement"="string",
	 *         		"description"="descripción del tipo de MateriaPrima"
	 *			},
	 *			{
	 *         		"name"="fechaCarga",
	 *         		"dataType"="datetime",
	 *         		"requirement"="dd/mm/yyyy",
	 *         		"description"="fecha de carga del MateriaPrima"
	 *			},
	 *			{
	 *         		"name"="cantidad",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="cantidad de MateriaPrimaes"
	 *			},
	 *			{
	 *         		"name"="valor",
	 *         		"dataType"="float",
	 *         		"requirement"="\d+",
	 *         		"description"="valor, si el tipo de MateriaPrima es gravado se le suma el % de la tasa de aeropuerto"
	 *			},
	 *			{
	 *         		"name"="tipoMateriaPrima",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="Tipo de MateriaPrima"
	 *			},
	 *			{
	 *         		"name"="debaja",
	 *         		"dataType"="boolean",
	 *         		"requirement"="string S o N",
	 *         		"description"="S está dado de baja"
	 *			},
	 *			{
	 *         		"name"="usuario",
	 *         		"dataType"="string",
	 *         		"requirement"="string",
	 *         		"description"="usuario logueado"
	 *			}
	 *
	 *		},
	 *      parameters={}
	 * )
	 * @Template()
	 */
	public function editarAction(Request $request)
	{
		$validator = $this->get('validator');
		$user = $this->get("security.context")->getToken()->getUser();
		if($request->request->get('oper') == "add")
                {
                    $result = $this->MateriaPrimaCrearSvc->crear($request,$validator,$user);
                }
                else
                {
                    $result = $this->MateriaPrimaCrearSvc->editar($request,$validator,$user);
                }
                
                
		$serializer = SerializerBuilder::create()->build();
		if($result == null)
		{
			$result = "MateriaPrima no encontrado";
			$httpError = 404;
		}
		else
		{
			$httpError = 200;
		}
		$result = $serializer->serialize($result, 'json');
		$response = new Response($result, $httpError);
		$response->headers->set('Content-Type', 'application/json');
		return $response;
	}
	
}