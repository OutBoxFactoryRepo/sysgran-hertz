<?php

namespace Hertz\ReservaBundle\Controller;

use Hertz\ReservaBundle\Entity\ModuloLecturaPerfil;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\Session;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use JMS\SecurityExtraBundle\Annotation as Security;

// these import the "@Route" and "@Template" annotations
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\Serializer\SerializerBuilder;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use JMS\DiExtraBundle\Annotation as DI;
use Hertz\ReservaBundle\Service\ModuloLecturaPerfilService;
use Symfony\Component\Validator;
use Hertz\UserBundle\Entity\User;
use Hertz\UserBundle\Form\Type\UserType;
use Hertz\UserBundle\Service\UserService;


class ModuloLecturaPerfilController extends Controller
{
    /**
     *
     * @var ModuloLecturaPerfilService
     *
     * @DI\Inject("ModuloLecturaPerfilSearchService.search")
     */
    private $modulolecturaperfilSvc;


    /**
     *
     * @var ModuloLecturaPerfilCrearService
     *
     * @DI\Inject("ModuloLecturaPerfilCrearService.crear")
     */
    private $modulolecturaperfilCrearSvc;

    /**
     *
     * @var ModuloLecturaPerfilIsReadOnlyService
     *
     * @DI\Inject("ModuloLecturaPerfilIsReadOnlyService.isreadonly")
     */
    private $modulolecturaperfilIsReadOnlySvc;


    /**
     * @var Session
     * @DI\Inject("session")
     */
    private $session;

    /**
     * @Route("/", name="modulolecturaperfil_index")
     * @ApiDoc(
     *        description = "Listado de los accesos por modulo"
     * )
     * @Template()
     */
    public function getAllAction(Request $request)
    {

        $result = $this->modulolecturaperfilSvc->getAll($request);

        $auxResult = array();
        foreach ($result as $modulo) {
            $auxResult[] = array(
                "id" => $modulo->getId()
            , "modulo" => $modulo->getModulo()->getDescripcion()
            , "perfil" => $modulo->getPerfil()->getName()
            );

        }

        $serializer = SerializerBuilder::create()->build();
        $result = $serializer->serialize($auxResult, 'json');
        $httpError = 200;
        $response = new Response($result, $httpError);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @Route("/crear", name="modulolecturaperfil_crear")
     * @Method({"POST"})
     * @ApiDoc(
     *        description = "crea un acceso para cada modulo y rol espesifico",
     *
     *        requirements = {
     *            {
     *                "name"="modulo",
     *                "dataType"="integer",
     *                "requirement"="\d+",
     *                "description"="id del modulo"
     *            },
     *     {
     *                "name"="perfil",
     *                "dataType"="integer",
     *                "requirement"="\d+",
     *                "description"="id del role"
     *            }
     *        },
     *      parameters={}
     * )
     * @Template()
     */
    public function crearAction(Request $request)
    {

        $validator = $this->get('validator');
        $user = $this->get("security.context")->getToken()->getUser();

        $result = $this->modulolecturaperfilCrearSvc->crear($request, $validator, $user);

        $httpError = 200;
        $serializer = SerializerBuilder::create()->build();
        $result2 = $serializer->serialize($result, 'json');

        $response = new Response($result2, $httpError);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     *
     * @Route("/delete/", name="modulolecturaperfil_delete")
     * @Method({"GET","POST"})
     * @ApiDoc(
     *        description = "Eliminar un registro del modulo Acceso Modulo",
     *        requirements = {},
     *      parameters={}
     * )
     * @Template()
     */
    public function deleteAction(Request $request)
    {

        $content = $request->getContent();
        $jsonContent = json_decode($content, true);

        $id = null;
        $objRequest = $request->request;

        $id = $objRequest->get("id");

        $em = $this->getDoctrine()->getManager();
        $moduloPerfil = $em->getRepository(ModuloLecturaPerfil::ORM_ENTITY)->findOneById($id);
        $em->remove($moduloPerfil);
        $em->flush();

        $result = $this->modulolecturaperfilSvc->getAll($request);

        $auxResult = array();
        foreach ($result as $modulo) {
            $auxResult[] = array(
                "id" => $modulo->getId()
            , "modulo" => $modulo->getModulo()->getDescripcion()
            , "perfil" => $modulo->getPerfil()->getName()
            );

        }

        $serializer = SerializerBuilder::create()->build();
        $result = $serializer->serialize($result, 'json');
        $httpError = 200;
        $response = new Response($result, $httpError);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }


    /**
     * @Route("/isreadonly/", name="modulolecturaperfil_isreadonly")
     * @Method({"GET"})
     * @ApiDoc(
     *        description = "Determina si un usuario tiene acceso de solo lectura sobre un modulo",
     *
     *        requirements = {
     *            {
     *                "name"="user",
     *                "dataType"="integer",
     *                "requirement"="\d+",
     *                "description"="id del usuario"
     *            },
     *            {
     *                "name"="modulo",
     *                "dataType"="integer",
     *                "requirement"="\d+",
     *                "description"="id del modulo"
     *            },
     *        },
     *      parameters={}
     * )
     * @Template()
     */
    public function isReadOnlyAction(Request $request)
    {
        $validator = $this->get('validator');
        $user = $this->get("security.context")->getToken()->getUser();
        $module = $request->get('modulo');

        $result = $this->modulolecturaperfilIsReadOnlySvc->isReadOnly($request, $validator, $user, $module);

        $httpError = 200;
        $serializer = SerializerBuilder::create()->build();
        $result2 = $serializer->serialize($result, 'json');

        $response = new Response($result2, $httpError);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

}