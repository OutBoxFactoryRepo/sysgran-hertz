<?php
namespace Hertz\ReservaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
//use Acme\DemoBundle\Form\ContactType;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use JMS\SecurityExtraBundle\Annotation as Security;

// these import the "@Route" and "@Template" annotations
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\Serializer\SerializerBuilder;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use JMS\DiExtraBundle\Annotation as DI;
use Hertz\ReservaBundle\Service\OrdenCompraService;
use Symfony\Component\Validator;
use WhiteOctober\TCPDFBundle\WhiteOctoberTCPDFBundle;

class OrdenCompraController extends Controller
{
	
	
	/**
	 *
	 * @var UserService
	 * @DI\Inject("user.manager")
	 */
	private $userSvc;
	
	/**
	 *
	 * @var OrdenCompraSearchService
	 *
	 * @DI\Inject("OrdenCompraSearchService.Search")
	 */
	private $searchOrdenCompraSvc;
		
	/**
	 *
	 * @var OrdenCompraGetOneService
	 *
	 * @DI\Inject("OrdenCompraGetOneService.GetOne")
	 */
	private $OrdenCompraGetOneSvc;
	
	/**
	 *
	 * @var OrdenCompraCrearService
	 *
	 * @DI\Inject("OrdenCompraCrearService.Crear")
	 */
	private $OrdenCompraCrearSvc;
	
	
	public function indexAction()
	{
		return array();
	}

	/**
	 * @Route("/search/")
     * @Method({"GET","POST"})
     * 
     * @ApiDoc(
     * 		description = "La lista de ordenes de compra",
     * 		requirements = {},
	 *      parameters={}
     * )
	 */
	public function getAllAction(Request $request)
	{
		$user = $this->get("security.context")->getToken()->getUser();
		$userx = $user->getUsername();
		$sucursal = $this->userSvc->getByUserUser($userx);
		
		$sucursal = $sucursal->getSucursal();
		
		$result = $this->searchOrdenCompraSvc->getAll($request,$sucursal,$user);
		$serializer = SerializerBuilder::create()->build();
		
                
                $totalRows = $result["total"];
                $totalPaginas = $result["totalPaginas"];
                $totalPaginasEntero = (int) $totalPaginas;
                
                if($totalPaginas > $totalPaginasEntero)
                {
                    $totalPaginas = (int)($totalPaginas + 1);
                }
                
		$arrayResultado = array("current"=>(int)$request->request->get("current"), "rowCount"=>$totalRows,"rows"=>$result["objeto"],"total"=>$totalPaginas,"inicio"=>$result["inicio"],"fin"=>$result["fin"]);

                
                
                $result = $serializer->serialize($arrayResultado, 'json');
                $httpError = 200;
                $response = new Response($result, $httpError);
                $response->headers->set('Content-Type', 'application/json');
                return $response;
	}
	
        /**
	 * @Route("/searchadmin/")
     * @Method({"GET","POST"})
     * 
     * @ApiDoc(
     * 		description = "La lista de ordenes de compra",
     * 		requirements = {},
	 *      parameters={}
     * )
	 */
	public function getAllAdminAction(Request $request)
	{
		$user = $this->get("security.context")->getToken()->getUser();
		$userx = $user->getUsername();
		$sucursal = $this->userSvc->getByUserUser($userx);
		
		$sucursal = $sucursal->getSucursal();
		$fromOcAdmin = true;
		$result = $this->searchOrdenCompraSvc->getAll($request,$sucursal,$user,$fromOcAdmin);
		$serializer = SerializerBuilder::create()->build();
		
                
                $totalRows = $result["total"];
                $totalPaginas = $result["totalPaginas"];
                $totalPaginasEntero = (int) $totalPaginas;
                
                if($totalPaginas > $totalPaginasEntero)
                {
                    $totalPaginas = (int)($totalPaginas + 1);
                }
                
		$arrayResultado = array("current"=>(int)$request->request->get("current"), "rowCount"=>$totalRows,"rows"=>$result["objeto"],"total"=>$totalPaginas,"inicio"=>$result["inicio"],"fin"=>$result["fin"]);

                
                
                $result = $serializer->serialize($arrayResultado, 'json');
                $httpError = 200;
                $response = new Response($result, $httpError);
                $response->headers->set('Content-Type', 'application/json');
                return $response;
	}
        
	/**
	 * @Route("/{id}/search/")
	 * @Method({"GET"})
	 *
	 * @ApiDoc(
	 * 		description = "obtiene una orden de compra en particular",
	 * 		requirements = {},
	 *      parameters={}
	 * )
	 */
	public function getOneAction($id)
	{
		$result = $this->OrdenCompraGetOneSvc->getOne($id);
		$serializer = SerializerBuilder::create()->build();
		
		if($result == null)
		{
			$result = "Orden de compra no encontrada";
			$httpError = 404;
		}
		else
		{
			$httpError = 200;
		}
		$result = $serializer->serialize($result, 'json');
		$response = new Response($result, $httpError);
		$response->headers->set('Content-Type', 'application/json');
		return $response;
	}
	
	/**
	 * @Route("/crear/", name="orden_compra_crear")
	 * @Method({"GET","POST"})
	 * @ApiDoc(
	 * 		description = "crea una orden de compra",
	 * 		
	 * 		requirements = {
	 * 			{
	 *         		"name"="descripcion",
	 *         		"dataType"="string",
	 *         		"requirement"="string",
	 *         		"description"="descripción del tipo de orden de compra"
	 *			},
	 *			{
	 *         		"name"="debaja",
	 *         		"dataType"="boolean",
	 *         		"requirement"="string S o N",
	 *         		"description"="S está dado de baja"
	 *			},
	 *			{
	 *         		"name"="fechaCarga",
	 *         		"dataType"="datetime",
	 *         		"requirement"="dd/mm/yyyy",
	 *         		"description"="fecha de carga de la orden de compra"
	 *			}
	 *		},
	 *      parameters={}
	 * )
	 * @Template()
	 */
	public function crearAction(Request $request)
	{
		$validator = $this->get('validator');
		$user = $this->get("security.context")->getToken()->getUser();
		
                //echo $request->request->get("oper");die();
                
                $oper = $request->request->get("oper");

                
		$userx = $user->getUsername();
		$sucursal = $this->userSvc->getByUserUser($userx);
		
		$sucursal = $sucursal->getSucursal();
		
                if($oper == "edit")
                {
                    $result = $this->OrdenCompraCrearSvc->editar($request,$validator,$user);
                }
                else
                {
                    $result = $this->OrdenCompraCrearSvc->crear($request,$validator,$user,$sucursal);
                }
                
		$serializer = SerializerBuilder::create()->build();
		$result = $serializer->serialize($result, 'json');
                
      
		$httpError = 200;
		$response = new Response($result, $httpError);
		$response->headers->set('Content-Type', 'application/json');
		return $response;
	}
	
	
	/**
	 * @Route("/editar/", name="orden_compra_editar")
	 * @Method({"GET","POST"})
	 * @ApiDoc(
	 * 		description = "Edita un adicional",
	 * 		requirements = {
	 *			{
	 *         		"name"="id",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="id del adicional"
	 *			},{
	 *         		"name"="descripcion",
	 *         		"dataType"="string",
	 *         		"requirement"="string",
	 *         		"description"="descripción del tipo de adicional"
	 *			},
	 *			{
	 *         		"name"="fechaCarga",
	 *         		"dataType"="datetime",
	 *         		"requirement"="dd/mm/yyyy",
	 *         		"description"="fecha de carga del adicional"
	 *			},
	 *			{
	 *         		"name"="cantidad",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="cantidad de adicionales"
	 *			},
	 *			{
	 *         		"name"="valor",
	 *         		"dataType"="float",
	 *         		"requirement"="\d+",
	 *         		"description"="valor, si el tipo de adicional es gravado se le suma el % de la tasa de aeropuerto"
	 *			},
	 *			{
	 *         		"name"="tipoAdicional",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="Tipo de adicional"
	 *			},
	 *			{
	 *         		"name"="debaja",
	 *         		"dataType"="boolean",
	 *         		"requirement"="string S o N",
	 *         		"description"="S está dado de baja"
	 *			},
	 *			{
	 *         		"name"="usuario",
	 *         		"dataType"="string",
	 *         		"requirement"="string",
	 *         		"description"="usuario logueado"
	 *			}
	 *
	 *		},
	 *      parameters={}
	 * )
	 * @Template()
	 */
	public function editarAction(Request $request)
	{
		$validator = $this->get('validator');
		$user = $this->get("security.context")->getToken()->getUser();

		
		
		
		$result = $this->OrdenCompraCrearSvc->editar($request,$validator,$user);
		$serializer = SerializerBuilder::create()->build();
		if($result == null)
		{
			$result = "Orden de compra no encontrada";
			$httpError = 404;
		}
		else
		{
			$httpError = 200;
		}
		$result = $serializer->serialize($result, 'json');
		$response = new Response($result, $httpError);
		$response->headers->set('Content-Type', 'application/json');
		return $response;
	}
        
        
        /**
	 * @Route("/pdf/", name="oc_pdf")
	 * @Method({"POST","GET"})
         *
         * @ApiDoc(
	 * 		description = "genera un pdf pasandole una estructura html correcta",
	 * 		requirements = {},
	 *      parameters={}
	 * )
         *   
         * @param type $html
         */ 
        public function pdfAction(Request $request)
         {
        // You can send the html as you want
       //$html = '<h1>Plain HTML</h1>';
        $html = $request->request->get("html");
        $idOt = $request->request->get("idOt");

        //$idContrato = $request->request->get("idContratoPdf");
        //$idReserva = $request->request->get("idReserva");
        // but in this case we will render a symfony view !
        // We are in a controller and we can use renderView function which retrieves the html from a view
        // then we send that html to the user.
            if($html == "")
            {
                $html = 'texto incompleto';
            }
            else
            {
                $result = $this->OrdenCompraGetOneSvc->actualizarOt($idOt);
                
            }
            

            //return $html;die();
            $this->returnPDFResponseFromHTML($html);
            //return $request->request->get("html");
        }
        
        public function returnPDFResponseFromHTML($html)
        {
        //set_time_limit(30); uncomment this line according to your needs
        // If you are not in a controller, retrieve of some way the service container and then retrieve it
        //$pdf = $this->container->get("white_october.tcpdf")->create('vertical', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        //if you are in a controlller use :
        $pdf = $this->get("white_october.tcpdf")->create('vertical', PDF_UNIT, "A3", true, '', false);
        $pdf->SetAuthor('OutBoxFactory');
        $pdf->SetTitle(('Contrato de Reserva'));
        $pdf->SetSubject('Contrato de Reserva');
        $pdf->setFontSubsetting(true);
        $pdf->SetFont('helvetica', '', 10, '', true);
        //$pdf->SetMargins(20,20,40, true);
        $pdf->AddPage();
        
        $filename = 'OT';
        
        $pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $html, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);
        $pdf->Output($filename.".pdf",'I'); // This will output the PDF as a response directly
}
	
}