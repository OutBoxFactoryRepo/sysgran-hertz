<?php
namespace Hertz\ReservaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
//use Acme\DemoBundle\Form\ContactType;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use JMS\SecurityExtraBundle\Annotation as Security;

// these import the "@Route" and "@Template" annotations
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\Serializer\SerializerBuilder;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use JMS\DiExtraBundle\Annotation as DI;
use Hertz\ReservaBundle\Service\MateriaPrimaComprasService;
use Symfony\Component\Validator;

class MateriaPrimaComprasController extends Controller
{
	
	/**
	 *
	 * @var UserService
	 * @DI\Inject("user.manager")
	 */
	private $userSvc;
	
	/**
	 *
	 * @var MateriaPrimaComprasSearchService
	 *
	 * @DI\Inject("MateriaPrimaComprasSearchService.Search")
	 */
	private $searchMateriaPrimaComprasSvc;
		
	/**
	 *
	 * @var MateriaPrimaComprasGetOneService
	 *
	 * @DI\Inject("MateriaPrimaComprasGetOneService.GetOne")
	 */
	private $MateriaPrimaComprasGetOneSvc;
	
	/**
	 *
	 * @var MateriaPrimaComprasCrearService
	 *
	 * @DI\Inject("MateriaPrimaComprasCrearService.Crear")
	 */
	private $MateriaPrimaComprasCrearSvc;
	
	
	public function indexAction()
	{
		return array();
	}

	/**
	 * @Route("/search/")
     * @Method({"GET","POST"})
     * 
     * @ApiDoc(
     * 		description = "La lista de todos los tipos de MateriaPrimaComprases",
     * 		requirements = {},
	 *      parameters={}
     * )
	 */
	public function getAllAction(Request $request)
	{

		$result = $this->searchMateriaPrimaComprasSvc->getAll($request);
		$serializer = SerializerBuilder::create()->build();
    	$totalRows = $result["total"];

    	$arrayResultado = array("success"=>true,"results"=>$totalRows,"rows"=>$result["objeto"]);
    	
		$result = $serializer->serialize($arrayResultado, 'json');
    	$httpError = 200;
    	$response = new Response($result, $httpError);
    	$response->headers->set('Content-Type', 'application/json');
    	return $response;
	}
	
	
	/**
	 * @Route("/searchcombo/")
     * @Method({"GET","POST"})
     * 
     * @ApiDoc(
     * 		description = "La lista de todos los tipos de MateriaPrimaComprases por usuario",
     * 		requirements = {},
	 *      parameters={}
     * )
	 */
	public function getAllComboAction(Request $request)
	{
		$userx = $this->get("security.context")->getToken()->getUser();
		$userx = $userx->getUsername();
		
		$sucursal = $this->userSvc->getByUserUser($userx);
		
		if(!$sucursal)
		{
			echo "no existe el usuario";die();
		}
		$sucursalx = $sucursal->getSucursal();
		
		$result = $this->searchMateriaPrimaComprasSvc->getAllCombo($request,$sucursalx);
		$serializer = SerializerBuilder::create()->build();
    	$totalRows = $result["total"];

    	$arrayResultado = array("success"=>true,"results"=>$totalRows,"rows"=>$result["objeto"]);
    	
		$result = $serializer->serialize($arrayResultado, 'json');
    	$httpError = 200;
    	$response = new Response($result, $httpError);
    	$response->headers->set('Content-Type', 'application/json');
    	return $response;
	}
	
	
	/**
	 * @Route("/{id}/search/")
	 * @Method({"GET"})
	 *
	 * @ApiDoc(
	 * 		description = "obtiene un MateriaPrimaCompras en particular",
	 * 		requirements = {},
	 *      parameters={}
	 * )
	 */
	public function getOneAction($id)
	{
		$result = $this->MateriaPrimaComprasGetOneSvc->getOne($id);
		$serializer = SerializerBuilder::create()->build();
		
		if($result == null)
		{
			$result = "MateriaPrimaCompras no encontrado";
			$httpError = 404;
		}
		else
		{
			$httpError = 200;
		}
		$result = $serializer->serialize($result, 'json');
		$response = new Response($result, $httpError);
		$response->headers->set('Content-Type', 'application/json');
		return $response;
	}
	
	/**
	 * @Route("/crear/")
	 * @Method({"GET","POST"})
	 * @ApiDoc(
	 * 		description = "crea un MateriaPrimaCompras",
	 * 		
	 * 		requirements = {
	 * 			{
	 *         		"name"="descripcion",
	 *         		"dataType"="string",
	 *         		"requirement"="string",
	 *         		"description"="descripción del tipo de MateriaPrimaCompras"
	 *			},
	 *			{
	 *         		"name"="fechaCarga",
	 *         		"dataType"="datetime",
	 *         		"requirement"="dd/mm/yyyy",
	 *         		"description"="fecha de carga del MateriaPrimaCompras"
	 *			},
	 *			{
	 *         		"name"="cantidad",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="cantidad de MateriaPrimaComprases"
	 *			},
	 *			{
	 *         		"name"="valor",
	 *         		"dataType"="float",
	 *         		"requirement"="\d+",
	 *         		"description"="valor, si el tipo de MateriaPrimaCompras es gravado se le suma el % de la tasa de aeropuerto"
	 *			},
	 *			{
	 *         		"name"="tipoMateriaPrimaCompras",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="fecha de carga del MateriaPrimaCompras"
	 *			},
	 *			{
	 *         		"name"="usuario",
	 *         		"dataType"="string",
	 *         		"requirement"="string",
	 *         		"description"="usuario logueado"
	 *			}
	 *		},
	 *      parameters={}
	 * )
	 * @Template()
	 */
	public function crearAction(Request $request)
	{

		$validator = $this->get('validator');
		$user = $this->get("security.context")->getToken()->getUser();
		$serializer = SerializerBuilder::create()->build();
                
                $oper = $request->request->get("oper");
                if($oper=="edit")
                {
                    
                    $result = $this->MateriaPrimaComprasCrearSvc->editar($request,$validator,$user);
                }
                elseif($oper=="del")
                {
                    $result = $this->MateriaPrimaComprasCrearSvc->delete($request,$validator,$user);
                }
                else
                {
                    $result = $this->MateriaPrimaComprasCrearSvc->crear($request,$validator,$user);
                }

		$result = $serializer->serialize($result, 'json');
                
                /*          
                $result = '{
	"error": "Integer should be a positive"
                }';
                
                */
                $httpError = 200;
                $response = new Response($result, $httpError);
                $response->headers->set('Content-Type', 'application/json');
                return $response;		
	}
	
	
	/**
	 * @Route("/editar/")
	 * @Method({"GET","POST"})
	 * @ApiDoc(
	 * 		description = "Edita un MateriaPrimaCompras",
	 * 		requirements = {
	 *			{
	 *         		"name"="id",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="id del MateriaPrimaCompras"
	 *			},{
	 *         		"name"="descripcion",
	 *         		"dataType"="string",
	 *         		"requirement"="string",
	 *         		"description"="descripción del tipo de MateriaPrimaCompras"
	 *			},
	 *			{
	 *         		"name"="fechaCarga",
	 *         		"dataType"="datetime",
	 *         		"requirement"="dd/mm/yyyy",
	 *         		"description"="fecha de carga del MateriaPrimaCompras"
	 *			},
	 *			{
	 *         		"name"="cantidad",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="cantidad de MateriaPrimaComprases"
	 *			},
	 *			{
	 *         		"name"="valor",
	 *         		"dataType"="float",
	 *         		"requirement"="\d+",
	 *         		"description"="valor, si el tipo de MateriaPrimaCompras es gravado se le suma el % de la tasa de aeropuerto"
	 *			},
	 *			{
	 *         		"name"="tipoMateriaPrimaCompras",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="Tipo de MateriaPrimaCompras"
	 *			},
	 *			{
	 *         		"name"="debaja",
	 *         		"dataType"="boolean",
	 *         		"requirement"="string S o N",
	 *         		"description"="S está dado de baja"
	 *			},
	 *			{
	 *         		"name"="usuario",
	 *         		"dataType"="string",
	 *         		"requirement"="string",
	 *         		"description"="usuario logueado"
	 *			}
	 *
	 *		},
	 *      parameters={}
	 * )
	 * @Template()
	 */
	public function editarAction(Request $request)
	{
		$validator = $this->get('validator');
		$user = $this->get("security.context")->getToken()->getUser();

		
		$result = $this->MateriaPrimaComprasCrearSvc->editar($request,$validator,$user);
		$serializer = SerializerBuilder::create()->build();
		if($result == null)
		{
			$result = "MateriaPrimaCompras no encontrado";
			$httpError = 404;
		}
		else
		{
			$httpError = 200;
		}
		$result = $serializer->serialize($result, 'json');
                
		$response = new Response($result, $httpError);
		$response->headers->set('Content-Type', 'application/json');
		return $response;
	}
	
}