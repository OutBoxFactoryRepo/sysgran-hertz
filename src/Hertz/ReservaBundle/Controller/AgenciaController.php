<?php
namespace Hertz\ReservaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
//use Acme\DemoBundle\Form\ContactType;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use JMS\SecurityExtraBundle\Annotation as Security;

// these import the "@Route" and "@Template" annotations
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\Serializer\SerializerBuilder;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use JMS\DiExtraBundle\Annotation as DI;
use Hertz\ReservaBundle\Service\AgenciaService;
use Symfony\Component\Validator;

use Hertz\UserBundle\Entity\User;
use Hertz\UserBundle\Form\Type\UserType;
use Hertz\UserBundle\Service\UserService;

class AgenciaController extends Controller
{
	
	/**
	 *
	 * @var UserService
	 * @DI\Inject("user.manager")
	 */
	private $userSvc;
	
	/**
	 *
	 * @var AgenciaSearchService
	 *
	 * @DI\Inject("AgenciaSearchService.Search")
	 */
	private $searchAgenciaSvc;
		
	/**
	 *
	 * @var AgenciaGetOneService
	 *
	 * @DI\Inject("AgenciaGetOneService.GetOne")
	 */
	private $AgenciaGetOneSvc;
	
	/**
	 *
	 * @var AgenciaCrearService
	 *
	 * @DI\Inject("AgenciaCrearService.Crear")
	 */
	private $AgenciaCrearSvc;

    /**
     *
     * @var AgenciaSearchByService
     *
     * @DI\Inject("AgenciaSearchByService.SearchBy")
     */
    private $searchByAgenciaSvc;


    /**
     *
     * @var AgenciaFranquiciaService
     *
     * @DI\Inject("AgenciaFranquiciaService.EditFranquicia")
     */
    private $agenciaFranquiciaSvc;
	
	
	public function indexAction()
	{
		return array();
	}

	/**
	 * @Route("/search/")
     * @Method({"GET","POST"})
     * 
     * @ApiDoc(
     * 		description = "La lista de todos los tipos de Agenciaes",
     * 		requirements = {},
	 *      parameters={}
     * )
	 */
	public function getAllAction()
	{
		$serializer = SerializerBuilder::create()->build();

		$userx = $this->get("security.context")->getToken()->getUser();
		$userx = $userx->getUsername();
		
		$sucursal = $this->userSvc->getByUserUser($userx);
		
		
		
		if(!$sucursal)
		{
			echo "no existe el usuario";die();
		}
		$sucursalx = $sucursal->getSucursal();
		
		$result = $this->searchAgenciaSvc->getAll($sucursalx);
		
    	$result = $serializer->serialize($result, 'json');
    	$httpError = 200;
    	$response = new Response($result, $httpError);
    	$response->headers->set('Content-Type', 'application/json');
    	return $response;
	}
	
	
	
	/**
	 * @Route("/searchadictodas/")
     * @Method({"GET","POST"})
     * 
     * @ApiDoc(
     * 		description = "La lista de todos los tipos de Agenciaes",
     * 		requirements = {},
	 *      parameters={}
     * )
	 */
	public function getAllAdicYTodasAction()
	{
		$serializer = SerializerBuilder::create()->build();

		$userx = $this->get("security.context")->getToken()->getUser();
		$userx = $userx->getUsername();
		
		$sucursal = $this->userSvc->getByUserUser($userx);
		
		
		
		if(!$sucursal)
		{
			echo "no existe el usuario";die();
		}
		$sucursalx = $sucursal->getSucursal();
		
		$result = $this->searchAgenciaSvc->getAllAdicYTodas($sucursalx);
		
    	$result = $serializer->serialize($result, 'json');
    	$httpError = 200;
    	$response = new Response($result, $httpError);
    	$response->headers->set('Content-Type', 'application/json');
    	return $response;
	}
	
	
	/**
	 * @Route("/searchcombo/")
     * @Method({"GET","POST"})
     * 
     * @ApiDoc(
     * 		description = "La lista de todos los tipos de Agenciaes",
     * 		requirements = {},
	 *      parameters={}
     * )
	 */
	public function getAllComboAction()
	{
		$serializer = SerializerBuilder::create()->build();

		$userx = $this->get("security.context")->getToken()->getUser();
		$userx = $userx->getUsername();
		
		$sucursal = $this->userSvc->getByUserUser($userx);
		
		
		
		if(!$sucursal)
		{
			echo "no existe el usuario";die();
		}
		$sucursalx = $sucursal->getSucursal();
		
		$result = $this->searchAgenciaSvc->getAllCombo($sucursalx);
		
    	$result = $serializer->serialize($result, 'json');
    	$httpError = 200;
    	$response = new Response($result, $httpError);
    	$response->headers->set('Content-Type', 'application/json');
    	return $response;
	}
	
	
/**
	 * @Route("/searchcomboview/")
     * @Method({"GET","POST"})
     * 
     * @ApiDoc(
     * 		description = "La lista de todos los tipos de Agencias",
     * 		requirements = {},
	 *      parameters={}
     * )
	 */
	public function getAllComboViewAction()
	{
		$serializer = SerializerBuilder::create()->build();

		
		$result = $this->searchAgenciaSvc->getAllComboView();
		
    	$result = $serializer->serialize($result, 'json');
    	$httpError = 200;
    	$response = new Response($result, $httpError);
    	$response->headers->set('Content-Type', 'application/json');
    	return $response;
	}
	
	/**
	 * @Route("/{id}/search/")
	 * @Method({"GET"})
	 *
	 * @ApiDoc(
	 * 		description = "obtiene un Agencia en particular",
	 * 		requirements = {},
	 *      parameters={}
	 * )
	 */
	public function getOneAction($id)
	{
		$result = $this->AgenciaGetOneSvc->getOne($id);
		$serializer = SerializerBuilder::create()->build();
		
		if($result == null)
		{
			$result = "Agencia no encontrado";
			$httpError = 404;
		}
		else
		{
			$httpError = 200;
		}
		$result = $serializer->serialize($result, 'json');
		$response = new Response($result, $httpError);
		$response->headers->set('Content-Type', 'application/json');
		return $response;
	}
	
	/**
	 * @Route("/crear/", name="Agencia_crear")
	 * @Method({"GET","POST"})
	 * @ApiDoc(
	 * 		description = "crea un Agencia",
	 * 		
	 * 		requirements = {
	 * 			{
	 *         		"name"="descripcion",
	 *         		"dataType"="string",
	 *         		"requirement"="string",
	 *         		"description"="descripción del tipo de Agencia"
	 *			},
	 *			{
	 *         		"name"="fechaCarga",
	 *         		"dataType"="datetime",
	 *         		"requirement"="dd/mm/yyyy",
	 *         		"description"="fecha de carga del Agencia"
	 *			},
	 *			{
	 *         		"name"="cantidad",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="cantidad de Agenciaes"
	 *			},
	 *			{
	 *         		"name"="valor",
	 *         		"dataType"="float",
	 *         		"requirement"="\d+",
	 *         		"description"="valor, si el tipo de Agencia es gravado se le suma el % de la tasa de aeropuerto"
	 *			},
	 *			{
	 *         		"name"="tipoAgencia",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="fecha de carga del Agencia"
	 *			},
	 *			{
	 *         		"name"="usuario",
	 *         		"dataType"="string",
	 *         		"requirement"="string",
	 *         		"description"="usuario logueado"
	 *			}
	 *		},
	 *      parameters={}
	 * )
	 * @Template()
	 */
	public function crearAction(Request $request)
	{
		$validator = $this->get('validator');
		$user = $this->get("security.context")->getToken()->getUser();
		$result = $this->AgenciaCrearSvc->crear($request,$validator,$user);
		$serializer = SerializerBuilder::create()->build();
		$result = $serializer->serialize($result, 'json');
		$httpError = 200;
		$response = new Response($result, $httpError);
		$response->headers->set('Content-Type', 'application/json');
		return $response;
	}
	
	
	/**
	 * @Route("/editar/", name="Agencia_editar")
	 * @Method({"GET","POST"})
	 * @ApiDoc(
	 * 		description = "Edita un Agencia",
	 * 		requirements = {
	 *			{
	 *         		"name"="id",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="id del Agencia"
	 *			},{
	 *         		"name"="descripcion",
	 *         		"dataType"="string",
	 *         		"requirement"="string",
	 *         		"description"="descripción del tipo de Agencia"
	 *			},
	 *			{
	 *         		"name"="fechaCarga",
	 *         		"dataType"="datetime",
	 *         		"requirement"="dd/mm/yyyy",
	 *         		"description"="fecha de carga del Agencia"
	 *			},
	 *			{
	 *         		"name"="cantidad",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="cantidad de Agenciaes"
	 *			},
	 *			{
	 *         		"name"="valor",
	 *         		"dataType"="float",
	 *         		"requirement"="\d+",
	 *         		"description"="valor, si el tipo de Agencia es gravado se le suma el % de la tasa de aeropuerto"
	 *			},
	 *			{
	 *         		"name"="tipoAgencia",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="Tipo de Agencia"
	 *			},
	 *			{
	 *         		"name"="debaja",
	 *         		"dataType"="boolean",
	 *         		"requirement"="string S o N",
	 *         		"description"="S está dado de baja"
	 *			},
	 *			{
	 *         		"name"="usuario",
	 *         		"dataType"="string",
	 *         		"requirement"="string",
	 *         		"description"="usuario logueado"
	 *			}
	 *
	 *		},
	 *      parameters={}
	 * )
	 * @Template()
	 */
	public function editarAction(Request $request)
	{
		$validator = $this->get('validator');
		$user = $this->get("security.context")->getToken()->getUser();

		
		
		
		$result = $this->AgenciaCrearSvc->editar($request,$validator,$user);
		$serializer = SerializerBuilder::create()->build();
		if($result == null)
		{
			$result = "Agencia no encontrada";
			$httpError = 404;
		}
		else
		{
			$httpError = 200;
		}
		$result = $serializer->serialize($result, 'json');
		$response = new Response($result, $httpError);
		$response->headers->set('Content-Type', 'application/json');
		return $response;
	}

    /**
     * @Route("/{id}/searchBy/")
     * @Method({"GET"})
     *
     * @ApiDoc(
     * 		description = "obtiene un Agencia en particular",
     * 		requirements = {
     *			{
     *         		"name"="id",
     *         		"dataType"="integer",
     *         		"requirement"="\d+",
     *         		"description"="id franquicia"
     *			}
     *     },
     *      parameters={}
     * )
     */
    public function searchByAction(Request $request, $id = null)
    {

        $serializer = SerializerBuilder::create()->build();

        $userx = $this->get("security.context")->getToken()->getUser();
        $userx = $userx->getUsername();

        $sucursal = $this->userSvc->getByUserUser($userx);

        if(!$sucursal)
        {
            echo "no existe el usuario";die();
        }
        $sucursalx = $sucursal->getSucursal();

        $result = $this->searchByAgenciaSvc->searchBy($sucursalx, $id);

        $result = $serializer->serialize($result, 'json');
        $httpError = 200;
        $response = new Response($result, $httpError);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @Route("/franquicia/", name="Agencia_editar_franquicia")
     * @Method({"GET","POST"})
     * @ApiDoc(
     * 		description = "Adiciona o Elimina un Agencia según su franquicia",
     * 		requirements = {
     *			{
     *         		"name"="id",
     *         		"dataType"="integer",
     *         		"requirement"="\d+",
     *         		"description"="id del Agencia"
     *			},{
     *         		"name"="idfranquicia",
     *         		"dataType"="integer",
     *         		"requirement"="\d+",
     *         		"description"="id de la franquicia"
     *			}
     *
     *		},
     *      parameters={}
     * )
     * @Template()
     */
    public function agenciaFranquiciaAction(Request $request)
    {

        $validator = $this->get('validator');
        $user = $this->get("security.context")->getToken()->getUser();

        $result = $this->agenciaFranquiciaSvc->editarFranquicia($request,$validator,$user);
        $serializer = SerializerBuilder::create()->build();
        if($result == null)
        {
            $result = "Agencia no encontrada";
            $httpError = 404;
        }
        else
        {
            $httpError = 200;
        }
        $result = $serializer->serialize($result, 'json');
        $response = new Response($result, $httpError);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
	
}