<?php
namespace Hertz\ReservaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
//use Acme\DemoBundle\Form\ContactType;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

// these import the "@Route" and "@Template" annotations
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\Serializer\SerializerBuilder;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use JMS\DiExtraBundle\Annotation as DI;
use Hertz\ReservaBundle\Service\ClienteService;

class ClienteController extends Controller
{
	
	/**
	 *
	 * @var ClienteSearchService
	 *
	 * @DI\Inject("ClienteSearchService.Search")
	 */
	private $searchClienteSvc;
        
        
        	
	/**
	 *
	 * @var ClienteSearchService
	 *
	 * @DI\Inject("ClienteSearchService.getByNombres")
	 */
	private $searchNombresSvc;
        
        
        /**
	 *
	 * @var ClienteSearchService
	 *
	 * @DI\Inject("ClienteSearchService.getClienteByNombre")
	 */
	private $searchClienteNombreSvc;

	/**
	 *
	 * @var ClienteGetOneService
	 *
	 * @DI\Inject("ClienteGetOneService.GetOne")
	 */
	private $ClienteGetOneSvc;
	
	/**
	 *
	 * @var ClienteCrearService
	 *
	 * @DI\Inject("ClienteCrearService.Crear")
	 */
	private $ClienteCrearSvc;
	
	
	public function indexAction()
	{
		 /*
         * The action's view can be rendered using render() method
         * or @Template annotation as demonstrated in DemoController.
         *
         */
        return array();
	}

	/**
	* @Route("/search/")
        * @Method({"GET","POST"})
        * 
        * @ApiDoc(
        * 		description = "La lista de todos los clientes",
        * 		requirements = {},
        *      parameters={}
        * )
	 */
	public function getAllAction(Request $request)
	{
			$user = $this->get("security.context")->getToken()->getUser();
            $result = $this->searchClienteSvc->getAll($request, $user);
            $serializer = SerializerBuilder::create()->build();
            
            
            $totalRows = $result["total"];
            $totalPaginas = $result["totalPaginas"];
            $totalPaginasEntero = (int) $totalPaginas;

            if($totalPaginas > $totalPaginasEntero)
            {
                $totalPaginas = (int)($totalPaginas + 1);
            }

            $arrayResultado = array("current"=>(int)$request->request->get("current"), "rowCount"=>$totalRows,"rows"=>$result["objeto"],"total"=>$totalPaginas);
            $result = $serializer->serialize($arrayResultado, 'json');
            $httpError = 200;
            $response = new Response($result, $httpError);
            $response->headers->set('Content-Type', 'application/json');
            return $response;
	}
	
	/**
	 * @Route("/{id}/search/")
	 * @Method({"GET"})
	 *
	 * @ApiDoc(
	 * 		description = "obtiene un cliente en particular",
	 * 		requirements = {},
	 *      parameters={}
	 * )
	 */
	public function getOneAction($id)
	{
		
		$result = $this->ClienteGetOneSvc->getOne($id);
		$serializer = SerializerBuilder::create()->build();
		$result = $serializer->serialize($result, 'json');
		$httpError = 200;
		$response = new Response($result, $httpError);
		$response->headers->set('Content-Type', 'application/json');
		return $response;
		
		//return $this->render(
		//		'HertzReservaBundle:Reserva:getOne.html.twig',
		//		array("respuesta"=>$result)
		//);
	}
        
        
     
            
        /**
	 * @Route("/searchbyname/")
	 * @Method({"GET"})
	 *
	 * @ApiDoc(
	 * 		description = "obtiene una lista de clientes por nombre o apellido",
	 * 		requirements = {{
	 *         "name"="term",
	 *         "dataType"="string",
	 *         "requirement"="\d+",
	 *         "description"="o apellido del cliente"
	 *			}},
	 *      parameters={}
	 * )
	 */
	public function getByNombresAction(Request $request)
	{
		$word = $request->query->get("term");
                
		$result = $this->searchNombresSvc->getByNombres($word);
                
		$serializer = SerializerBuilder::create()->build();
                $result = $serializer->serialize($result, 'json');
                
                
                $httpError = 200;
                $response = new Response($result, $httpError);
                $response->headers->set('Content-Type', 'application/json');
                return $response;
	}
	
	/**
	 * @Route("/crear/", name="cliente_crear")
	 *
	 * @ApiDoc(
	 * 		description = "crea un nuevo cliente",
	 * 		
	 * 		requirements = {{
	 *         "name"="nombre",
	 *         "dataType"="string",
	 *         "requirement"="\d+",
	 *         "description"="nombre del cliente"
	 *			},
	 *			{
	 *         		"name"="apellido",
	 *         		"dataType"="string",
	 *         		"requirement"="\d+",
	 *         		"description"="apellido del cliente"
	 *			}
	 *			,
	 *			{
	 *         		"name"="fechaNac",
	 *         		"dataType"="datetime",
	 *         		"requirement"="\d+",
	 *         		"description"="fecha de nacimiento del cliente"
	 *			},
	 *			{
	 *         		"name"="email",
	 *         		"dataType"="string",
	 *         		"requirement"="\d+",
	 *         		"description"="email del cliente"
	 *			},
	 *			{
	 *         		"name"="licencia",
	 *         		"dataType"="string",
	 *         		"requirement"="\d+",
	 *         		"description"="licencia de conductor del cliente"
	 *			},
	 *			{
	 *         		"name"="licFecVto",
	 *         		"dataType"="datetime",
	 *         		"requirement"="\d+",
	 *         		"description"="Fecha de vencimiento de la licencia de conductor del cliente"
	 *			},
	 *			{
	 *         		"name"="nroTarjetaCre",
	 *         		"dataType"="string",
	 *         		"requirement"="\d+",
	 *         		"description"="Número de tarjeta de crédito del cliente"
	 *			},
	 *			{
	 *         		"name"="tarjFecVto",
	 *         		"dataType"="datetime",
	 *         		"requirement"="\d+",
	 *         		"description"="Fecha de vencimiento de la tarjeta de crédito del cliente"
	 *			},
	 *			{
	 *         		"name"="cAut",
	 *         		"dataType"="string",
	 *         		"requirement"="\d+",
	 *         		"description"="campo cAut no tengo confirmada su procedencia"
	 *			}
	 *			,
	 *			{
	 *         		"name"="cs",
	 *         		"dataType"="string",
	 *         		"requirement"="\d+",
	 *         		"description"="campo cs no tengo confirmada su procedencia"
	 *			},
	 *			{
	 *         		"name"="tipodoc",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="tipo de documento"
	 *			},
	 *			{
	 *         		"name"="numeroDoc",
	 *         		"dataType"="string",
	 *         		"requirement"="\d+",
	 *         		"description"="Número de Documento"
	 *			},
	 *			{
	 *         		"name"="cuit",
	 *         		"dataType"="string",
	 *         		"requirement"="\d+",
	 *         		"description"="Número de cuit"
	 *			}
	 *			,
	 *			{
	 *         		"name"="idPais",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="Id del país del cliente"
	 *			},
	 *			{
	 *         		"name"="debaja",
	 *         		"dataType"="boolean",
	 *         		"requirement"="\d+",
	 *         		"description"="True = cliente dado de baja"
	 *			},
	 *			{
	 *         		"name"="tipoCliente",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="Tipo de cliente"
	 *			}
	 *			 ,
	 *			{
	 *         		"name"="domicilio",
	 *         		"dataType"="string",
	 *         		"requirement"="\d+",
	 *         		"description"="domicilio del cliente"
	 *			},
	 *			{
	 *         		"name"="localidad",
	 *         		"dataType"="string",
	 *         		"requirement"="\d+",
	 *         		"description"="localidad del cliente"
	 *			}
	 *			,
	 *			{
	 *         		"name"="codigoPostal",
	 *         		"dataType"="string",
	 *         		"requirement"="\d+",
	 *         		"description"="C.P. del cliente"
	 *			},
	 *			{
	 *         		"name"="provincia",
	 *         		"dataType"="string",
	 *         		"requirement"="\d+",
	 *         		"description"="provincia o estado del cliente"
	 *			},
	 *			{
	 *         		"name"="condicionIva",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="condicion de iva del cliente"
	 *			}
	 *			,
	 *			{
	 *         		"name"="idIIBB",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="Tipo de Ingresos brutos"
	 *			}
	 *			,
	 *			{
	 *         		"name"="nroIIBB",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="Número de ingresos brutos"
	 *			}
	 *		},
	 *      parameters={}
	 * )
	 * @Template()
	 */
	public function crearAction(Request $request)
	{
		
                $result = $this->ClienteCrearSvc->crear($request);
                
		$serializer = SerializerBuilder::create()->build();
		$result = $serializer->serialize($result, 'json');
		$httpError = 200;
		$response = new Response($result, $httpError);
		$response->headers->set('Content-Type', 'application/json');
		return $response;
	}
	
	
	/**
	 * @Route("/{id}/editar/", name="cliente_editar")
	 *
	 * @ApiDoc(
	 * 		description = "edita un cliente",
	 *
	 * 		requirements = {{
	 *         "name"="nombre",
	 *         "dataType"="string",
	 *         "requirement"="\d+",
	 *         "description"="nombre del cliente"
	 *			},
	 *			{
	 *         		"name"="apellido",
	 *         		"dataType"="string",
	 *         		"requirement"="\d+",
	 *         		"description"="apellido del cliente"
	 *			}
	 *			,
	 *			{
	 *         		"name"="fechaNac",
	 *         		"dataType"="datetime",
	 *         		"requirement"="\d+",
	 *         		"description"="fecha de nacimiento del cliente"
	 *			},
	 *			{
	 *         		"name"="email",
	 *         		"dataType"="string",
	 *         		"requirement"="\d+",
	 *         		"description"="email del cliente"
	 *			},
	 *			{
	 *         		"name"="licencia",
	 *         		"dataType"="string",
	 *         		"requirement"="\d+",
	 *         		"description"="licencia de conductor del cliente"
	 *			},
	 *			{
	 *         		"name"="licFecVto",
	 *         		"dataType"="datetime",
	 *         		"requirement"="\d+",
	 *         		"description"="Fecha de vencimiento de la licencia de conductor del cliente"
	 *			},
	 *			{
	 *         		"name"="nroTarjetaCre",
	 *         		"dataType"="string",
	 *         		"requirement"="\d+",
	 *         		"description"="Número de tarjeta de crédito del cliente"
	 *			},
	 *			{
	 *         		"name"="tarjFecVto",
	 *         		"dataType"="datetime",
	 *         		"requirement"="\d+",
	 *         		"description"="Fecha de vencimiento de la tarjeta de crédito del cliente"
	 *			},
	 *			{
	 *         		"name"="cAut",
	 *         		"dataType"="string",
	 *         		"requirement"="\d+",
	 *         		"description"="campo cAut no tengo confirmada su procedencia"
	 *			}
	 *			,
	 *			{
	 *         		"name"="cs",
	 *         		"dataType"="string",
	 *         		"requirement"="\d+",
	 *         		"description"="campo cs no tengo confirmada su procedencia"
	 *			},
	 *			{
	 *         		"name"="tipodoc",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="tipo de documento"
	 *			},
	 *			{
	 *         		"name"="numeroDoc",
	 *         		"dataType"="string",
	 *         		"requirement"="\d+",
	 *         		"description"="Número de Documento"
	 *			},
	 *			{
	 *         		"name"="cuit",
	 *         		"dataType"="string",
	 *         		"requirement"="\d+",
	 *         		"description"="Número de cuit"
	 *			}
	 *			,
	 *			{
	 *         		"name"="idPais",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="Id del país del cliente"
	 *			},
	 *			{
	 *         		"name"="debaja",
	 *         		"dataType"="boolean",
	 *         		"requirement"="\d+",
	 *         		"description"="True = cliente dado de baja"
	 *			},
	 *			{
	 *         		"name"="tipoCliente",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="Tipo de cliente"
	 *			}
	 *			 ,
	 *			{
	 *         		"name"="domicilio",
	 *         		"dataType"="string",
	 *         		"requirement"="\d+",
	 *         		"description"="domicilio del cliente"
	 *			},
	 *			{
	 *         		"name"="localidad",
	 *         		"dataType"="string",
	 *         		"requirement"="\d+",
	 *         		"description"="localidad del cliente"
	 *			}
	 *			,
	 *			{
	 *         		"name"="codigoPostal",
	 *         		"dataType"="string",
	 *         		"requirement"="\d+",
	 *         		"description"="C.P. del cliente"
	 *			},
	 *			{
	 *         		"name"="provincia",
	 *         		"dataType"="string",
	 *         		"requirement"="\d+",
	 *         		"description"="provincia o estado del cliente"
	 *			},
	 *			{
	 *         		"name"="condicionIva",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="condicion de iva del cliente"
	 *			} ,
	 *			{
	 *         		"name"="idIIBB",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="Tipo de Ingresos brutos"
	 *			}
	 *			,
	 *			{
	 *         		"name"="nroIIBB",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="Número de ingresos brutos"
	 *			}
	 *
	 *		},
	 *      parameters={{"name"="id", "dataType"="integer", "required"=true, "description"="Id del cliente a modificar"}}
	 * )
	 * @Template()
	 */
	public function editarAction(Request $request,$id)
	{
		$result = $this->ClienteCrearSvc->editar($request,$id);
		$serializer = SerializerBuilder::create()->build();
		$result = $serializer->serialize($result, 'json');
		$httpError = 200;
		$response = new Response($result, $httpError);
		$response->headers->set('Content-Type', 'application/json');
		return $response;
	}
	
        /**
	 * @Route("/searchbynombre/")
	 * @Method({"GET"})
	 *
	 * @ApiDoc(
	 * 		description = "obtiene una lista de vehículos por descripcion o patente",
	 * 		requirements = {{
	 *         "name"="term",
	 *         "dataType"="string",
	 *         "requirement"="\d+",
	 *         "description"="variable"
	 *			}},
	 *      parameters={}
	 * )
	 */
	public function getClienteByNombreAction(Request $request)
	{
		$word = $request->query->get("term");
                //echo $horaSalida;die();
		$result = $this->searchClienteNombreSvc->getClienteByNombre($word);
                
		$serializer = SerializerBuilder::create()->build();
                $result = $serializer->serialize($result, 'json');
                
                
                $httpError = 200;
                $response = new Response($result, $httpError);
                $response->headers->set('Content-Type', 'application/json');
                return $response;
	}
}