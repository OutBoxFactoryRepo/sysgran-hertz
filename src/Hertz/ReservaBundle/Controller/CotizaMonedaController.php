<?php
namespace Hertz\ReservaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
//use Acme\DemoBundle\Form\ContactType;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

// these import the "@Route" and "@Template" annotations
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\Serializer\SerializerBuilder;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use JMS\DiExtraBundle\Annotation as DI;
use Hertz\ReservaBundle\Service\CotizaMonedaService;

class CotizaMonedaController extends Controller
{
	
	/**
	 *
	 * @var CotizaMonedaCrearService
	 *
	 * @DI\Inject("CotizaMonedaCrearService.Crear")
	 */
	private $crearCotizaMonedaSvc;
		
	/**
	 *
	 * @var CotizaMonedaSearchService
	 *
	 * @DI\Inject("CotizaMonedaSearchService.Search")
	 */
	private $searchCotizaMonedaSvc;
	
	/**
	 *
	 * @var CotizaMonedaGetOneService
	 *
	 * @DI\Inject("CotizaMonedaGetOneService.GetOne")
	 */
	private $CotizaMonedaGetOneSvc;
	
	public function indexAction()
	{
		return array();
	}

	/**
	 * @Route("/crear/", name="cotiza_moneda_crear")
	 * @Method({"POST","PUT"})
	 * @ApiDoc(
	 * 		description = "crea una nueva cotización",
	 *
	 * 		requirements = {{
	 *         "name"="cotizacion",
	 *         "dataType"="float",
	 *         "requirement"="\d+",
	 *         "description"="cotizacion de moneda"
	 *			},{
	 *         "name"="cotizacionVenta",
	 *         "dataType"="float",
	 *         "requirement"="\d+",
	 *         "description"="cotizacion de Venta de la moneda"
	 *			},{
	 *         "name"="idmoneda",
	 *         "dataType"="integer",
	 *         "requirement"="\d+",
	 *         "description"="Id de la moneda a cotizar"
	 *			}
	 *		},
	 *      parameters={}
	 * )
	 * @Template()
	 */
	public function crearAction(Request $request)
	{
		$result = $this->crearCotizaMonedaSvc->crear($request);
		$serializer = SerializerBuilder::create()->build();
		$result = $serializer->serialize($result, 'json');
		$httpError = 200;
		$response = new Response($result, $httpError);
		$response->headers->set('Content-Type', 'application/json');
		return $response;
	}
	
	/**
	 * @Route("/search/")
     * @Method({"GET"})
     * 
     * @ApiDoc(
     * 		description = "Lista la útlima cotizacion disponible para todas las monedas",
     * 		requirements = {},
	 *      parameters={}
     * )
	 */
	public function getAllAction()
	{
		$result = $this->searchCotizaMonedaSvc->getAll();
		$serializer = SerializerBuilder::create()->build();
    	$result = $serializer->serialize($result, 'json');
    	$httpError = 200;
    	$response = new Response($result, $httpError);
    	$response->headers->set('Content-Type', 'application/json');
    	return $response;
	}
	
	/**
	 * @Route("/{id}/search/")
	 * @Method({"GET"})
	 *
	 * @ApiDoc(
	 * 		description = "obtiene una cotización en particular",
	 * 		requirements = {{
	 *         "name"="id",
	 *         "dataType"="integer",
	 *         "requirement"="",
	 *         "description"="Id de la moneda"
	 *			}},
	 *      parameters={}
	 * )
	 */
	public function getOneAction($id)
	{
		$result = $this->CotizaMonedaGetOneSvc->getOne($id);
		$serializer = SerializerBuilder::create()->build();
		$result = $serializer->serialize($result, 'json');
		$httpError = 200;
		$response = new Response($result, $httpError);
		$response->headers->set('Content-Type', 'application/json');
		return $response;
	}
	

}