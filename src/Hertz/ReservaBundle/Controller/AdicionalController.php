<?php
namespace Hertz\ReservaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
//use Acme\DemoBundle\Form\ContactType;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use JMS\SecurityExtraBundle\Annotation as Security;

// these import the "@Route" and "@Template" annotations
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\Serializer\SerializerBuilder;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use JMS\DiExtraBundle\Annotation as DI;
use Hertz\ReservaBundle\Service\AdicionalService;
use Symfony\Component\Validator;

class AdicionalController extends Controller
{
	
	/**
	 *
	 * @var UserService
	 * @DI\Inject("user.manager")
	 */
	private $userSvc;
	
	/**
	 *
	 * @var AdicionalSearchService
	 *
	 * @DI\Inject("AdicionalSearchService.Search")
	 */
	private $searchAdicionalSvc;
		
	/**
	 *
	 * @var AdicionalGetOneService
	 *
	 * @DI\Inject("AdicionalGetOneService.GetOne")
	 */
	private $AdicionalGetOneSvc;
	
	/**
	 *
	 * @var AdicionalCrearService
	 *
	 * @DI\Inject("AdicionalCrearService.Crear")
	 */
	private $AdicionalCrearSvc;
	
	
	public function indexAction()
	{
		return array();
	}

	/**
	 * @Route("/search/")
     * @Method({"GET","POST"})
     * 
     * @ApiDoc(
     * 		description = "La lista de todos los tipos de adicionales",
     * 		requirements = {},
	 *      parameters={}
     * )
	 */
	public function getAllAction(Request $request)
	{
		$user = $this->get("security.context")->getToken()->getUser();
		$userx = $user->getUsername();
		$sucursal = $this->userSvc->getByUserUser($userx);

		if(!$sucursal)
		{
			echo "no existe el usuario";die();
		}
		$sucursalx = $sucursal->getSucursal();

		$result = $this->searchAdicionalSvc->getAll($request,$sucursalx, $user);
		$serializer = SerializerBuilder::create()->build();

		$totalRows = $result["total"];
		$totalPaginas = $result["totalPaginas"];
		$totalPaginasEntero = (int) $totalPaginas;

		if($totalPaginas > $totalPaginasEntero)
		{
			$totalPaginas = (int)($totalPaginas + 1);
		}
		//echo "total: ".$totalPaginas;die();
		$arrayResultado = array("current"=>(int)$request->request->get("current"), "rowCount"=>$totalRows,"rows"=>$result["objeto"],"total"=>$totalPaginas);


		$result = $serializer->serialize($arrayResultado, 'json');
		$httpError = 200;
		$response = new Response($result, $httpError);
		$response->headers->set('Content-Type', 'application/json');
		return $response;
	}
	
	
	/**
	* @Route("/searchcombo/")
        * @Method({"GET","POST"})
        *   
        * @ApiDoc(
        * 		description = "La lista de todos los tipos de adicionales por usuario",
        * 		requirements = {},
	*      parameters={}
        * )
	 */
	public function getAllComboAction(Request $request)
	{
		$userx = $this->get("security.context")->getToken()->getUser();
		$userx = $userx->getUsername();
		
		$sucursal = $this->userSvc->getByUserUser($userx);
		
		if(!$sucursal)
		{
			echo "no existe el usuario";die();
		}
		$sucursalx = $sucursal->getSucursal();
		
		$result = $this->searchAdicionalSvc->getAllCombo($request,$sucursalx);
		$serializer = SerializerBuilder::create()->build();
    	$totalRows = $result["total"];

    	$arrayResultado = array("success"=>true,"results"=>$totalRows,"rows"=>$result["objeto"]);
    	
		$result = $serializer->serialize($arrayResultado, 'json');
    	$httpError = 200;
    	$response = new Response($result, $httpError);
    	$response->headers->set('Content-Type', 'application/json');
    	return $response;
	}
	
	/**
	 * @Route("/searchcontrato/")
         * @Method({"GET","POST"})
         * 
         * @ApiDoc(
         * 		description = "La lista de todos los tipos de adicionales listados por tipo",
         * 		requirements = {{
	 *         		"name"="tipo",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="tipo de adicional"
	 *			}},
	 *      parameters={}
         * )
	 */
	public function getPorTipoAdicional(Request $request)
	{
		$userx = $this->get("security.context")->getToken()->getUser();
		$userx = $userx->getUsername();
		
		$sucursal = $this->userSvc->getByUserUser($userx);
		
		if(!$sucursal)
		{
			echo "no existe el usuario";die();
		}
		$sucursalx = $sucursal->getSucursal();
		
		$result = $this->searchAdicionalSvc->getAllCombo($request,$sucursalx);
		$serializer = SerializerBuilder::create()->build();
                $totalRows = $result["total"];

                $arrayResultado = array("success"=>true,"results"=>$totalRows,"rows"=>$result["objeto"]);
    	
		$result = $serializer->serialize($arrayResultado, 'json');
                $httpError = 200;
                $response = new Response($result, $httpError);
                $response->headers->set('Content-Type', 'application/json');
    	return $response;
	}
        
	/**
	 * @Route("/{id}/search/")
	 * @Method({"GET"})
	 *
	 * @ApiDoc(
	 * 		description = "obtiene un adicional en particular",
	 * 		requirements = {},
	 *      parameters={}
	 * )
	 */
	public function getOneAction($id)
	{
		$result = $this->AdicionalGetOneSvc->getOne($id);
		$serializer = SerializerBuilder::create()->build();
		
		if($result == null)
		{
			$result = "Adicional no encontrado";
			$httpError = 404;
		}
		else
		{
			$httpError = 200;
		}
		$result = $serializer->serialize($result, 'json');
		$response = new Response($result, $httpError);
		$response->headers->set('Content-Type', 'application/json');
		return $response;
	}
	
	/**
	 * @Route("/crear/", name="adicional_crear")
	 * @Method({"GET","POST"})
	 * @ApiDoc(
	 * 		description = "crea un Adicional",
	 * 		
	 * 		requirements = {
	 * 			{
	 *         		"name"="descripcion",
	 *         		"dataType"="string",
	 *         		"requirement"="string",
	 *         		"description"="descripción del tipo de adicional"
	 *			},
	 *			{
	 *         		"name"="fechaCarga",
	 *         		"dataType"="datetime",
	 *         		"requirement"="dd/mm/yyyy",
	 *         		"description"="fecha de carga del adicional"
	 *			},
	 *			{
	 *         		"name"="cantidad",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="cantidad de adicionales"
	 *			},
	 *			{
	 *         		"name"="valor",
	 *         		"dataType"="float",
	 *         		"requirement"="\d+",
	 *         		"description"="valor, si el tipo de adicional es gravado se le suma el % de la tasa de aeropuerto"
	 *			},
	 *			{
	 *         		"name"="tipoAdicional",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="fecha de carga del adicional"
	 *			},
	 *			{
	 *         		"name"="usuario",
	 *         		"dataType"="string",
	 *         		"requirement"="string",
	 *         		"description"="usuario logueado"
	 *			}
	 *		},
	 *      parameters={}
	 * )
	 * @Template()
	 */
	public function crearAction(Request $request)
	{
		$validator = $this->get('validator');
		$user = $this->get("security.context")->getToken()->getUser();
		$result = $this->AdicionalCrearSvc->crear($request,$validator,$user);
		$serializer = SerializerBuilder::create()->build();
		$result = $serializer->serialize($result, 'json');
		$httpError = 200;
		$response = new Response($result, $httpError);
		$response->headers->set('Content-Type', 'application/json');
		return $response;
	}
	
	
	/**
	 * @Route("/editar/", name="adicional_editar")
	 * @Method({"GET","POST"})
	 * @ApiDoc(
	 * 		description = "Edita un adicional",
	 * 		requirements = {
	 *			{
	 *         		"name"="id",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="id del adicional"
	 *			},{
	 *         		"name"="descripcion",
	 *         		"dataType"="string",
	 *         		"requirement"="string",
	 *         		"description"="descripción del tipo de adicional"
	 *			},
	 *			{
	 *         		"name"="fechaCarga",
	 *         		"dataType"="datetime",
	 *         		"requirement"="dd/mm/yyyy",
	 *         		"description"="fecha de carga del adicional"
	 *			},
	 *			{
	 *         		"name"="cantidad",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="cantidad de adicionales"
	 *			},
	 *			{
	 *         		"name"="valor",
	 *         		"dataType"="float",
	 *         		"requirement"="\d+",
	 *         		"description"="valor, si el tipo de adicional es gravado se le suma el % de la tasa de aeropuerto"
	 *			},
	 *			{
	 *         		"name"="tipoAdicional",
	 *         		"dataType"="integer",
	 *         		"requirement"="\d+",
	 *         		"description"="Tipo de adicional"
	 *			},
	 *			{
	 *         		"name"="debaja",
	 *         		"dataType"="boolean",
	 *         		"requirement"="string S o N",
	 *         		"description"="S está dado de baja"
	 *			},
	 *			{
	 *         		"name"="usuario",
	 *         		"dataType"="string",
	 *         		"requirement"="string",
	 *         		"description"="usuario logueado"
	 *			}
	 *
	 *		},
	 *      parameters={}
	 * )
	 * @Template()
	 */
	public function editarAction(Request $request)
	{
		$validator = $this->get('validator');
		$user = $this->get("security.context")->getToken()->getUser();

		if($request->request->get("oper") == "edit")
                {
			
                    $result = $this->AdicionalCrearSvc->editar($request,$validator,$user);
                    $serializer = SerializerBuilder::create()->build();
                    if($result == null)
                    {
                            $result = "Adicional no encontrado";
                            $httpError = 404;
                    }
                    else
                    {
                            $httpError = 200;
                    }
                }
                else
                {
                    $result = $this->AdicionalCrearSvc->crear($request,$validator,$user);
                    $serializer = SerializerBuilder::create()->build();
                    $httpError = 200;
                }
		$result = $serializer->serialize($result, 'json');

		// var_dump($result);die;
		$response = new Response($result, $httpError);
		$response->headers->set('Content-Type', 'application/json');
		return $response;
	}
	
}