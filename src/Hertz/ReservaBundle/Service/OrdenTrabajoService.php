<?php
namespace Hertz\ReservaBundle\Service;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Response;

use Hertz\ReservaBundle\Entity\OrdenTrabajo;

class OrdenTrabajoService{
	
	/** 
	 * 
	 * @var EntityManager
	 */
	private $em;

	public function __construct(EntityManager $em){
		$this->em = $em;
		$config = $em->getConfiguration();
	}

	public function getAll($request,$sucursal,$user,$fromTaller = false, $fromOTAdmin = false)
	{
		return $this->em->getRepository(OrdenTrabajo::ORM_ENTITY)->getAll($request,$sucursal,$user,$fromTaller,$fromOTAdmin);
	}
	
	public function getOne($id)
	{
		return $this->em->getRepository(OrdenTrabajo::ORM_ENTITY)->getOne($id);
	}
	
        public function actualizarOt($id)
	{
		return $this->em->getRepository(OrdenTrabajo::ORM_ENTITY)->actualizarOt($id);
	}
        
        
	public function crear($request,$validator,$user,$sucursal)
	{
		return $this->em->getRepository(OrdenTrabajo::ORM_ENTITY)->crear($request,$validator,$user,$sucursal);
	}
	public function editar($request,$validator,$user)
	{
		return $this->em->getRepository(OrdenTrabajo::ORM_ENTITY)->editar($request,$validator,$user);
	}

    public function updatepago($id, $pagado)
    {
        return $this->em->getRepository(OrdenTrabajo::ORM_ENTITY)->updatepago($id, $pagado);
    }
}