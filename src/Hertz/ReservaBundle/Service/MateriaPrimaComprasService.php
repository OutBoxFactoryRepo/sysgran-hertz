<?php
namespace Hertz\ReservaBundle\Service;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Response;

use Hertz\ReservaBundle\Entity\MateriaPrimaCompras;

class MateriaPrimaComprasService{
	
	/** 
	 * 
	 * @var EntityManager
	 */
	private $em;

	public function __construct(EntityManager $em)
	{
		$this->em = $em;
		$config = $em->getConfiguration();
	}

	public function getAll($request)
	{
		return $this->em->getRepository(MateriaPrimaCompras::ORM_ENTITY)->getAll($request);
	}
	
	public function getAllCombo($request,$sucursal)
	{
		return $this->em->getRepository(MateriaPrimaCompras::ORM_ENTITY)->getAllCombo($request,$sucursal);
	}
	
	
	public function getOne($id)
	{
		return $this->em->getRepository(MateriaPrimaCompras::ORM_ENTITY)->getOne($id);
	}
	
	public function crear($request,$validator,$user)
	{
		return $this->em->getRepository(MateriaPrimaCompras::ORM_ENTITY)->crear($request,$validator,$user);
	}
	public function editar($request,$validator,$user)
	{
		return $this->em->getRepository(MateriaPrimaCompras::ORM_ENTITY)->editar($request,$validator,$user);
	}
        public function delete($request,$validator,$user)
	{
		return $this->em->getRepository(MateriaPrimaCompras::ORM_ENTITY)->eliminar($request,$validator,$user);
	}
}