<?php
namespace Hertz\ReservaBundle\Service;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Response;

use Hertz\ReservaBundle\Entity\MateriaPrima;

class MateriaPrimaService{
	
	/** 
	 * 
	 * @var EntityManager
	 */
	private $em;

	public function __construct(EntityManager $em)
	{
		$this->em = $em;
		$config = $em->getConfiguration();
	}

	public function getAll($request,$sucursal,$user)
	{
		return $this->em->getRepository(MateriaPrima::ORM_ENTITY)->getAll($request,$sucursal,$user);
	}
	
	public function getAllCombo($request,$sucursal)
	{
		return $this->em->getRepository(MateriaPrima::ORM_ENTITY)->getAllCombo($request,$sucursal);
	}
	
	
	public function getOne($id)
	{
		return $this->em->getRepository(MateriaPrima::ORM_ENTITY)->getOne($id);
	}
	
	public function crear($request,$validator,$user)
	{
		return $this->em->getRepository(MateriaPrima::ORM_ENTITY)->crear($request,$validator,$user);
	}
	public function editar($request,$validator,$user)
	{
		return $this->em->getRepository(MateriaPrima::ORM_ENTITY)->editar($request,$validator,$user);
	}

    public function getAllByCategory($categoria_id, $term,$sucursal,$user)
	{
		return $this->em->getRepository(MateriaPrima::ORM_ENTITY)->getAllByCategory($categoria_id, $term,$sucursal,$user);
	}
}