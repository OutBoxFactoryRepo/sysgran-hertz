<?php
namespace Hertz\ReservaBundle\Service;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Response;

use Hertz\ReservaBundle\Entity\TipoAdicional;

class TipoAdicionalService{
	
	/** 
	 * 
	 * @var EntityManager
	 */
	private $em;

	public function __construct(EntityManager $em){
		$this->em = $em;
		$config = $em->getConfiguration();
	}

	public function getAll()
	{
		return $this->em->getRepository(TipoAdicional::ORM_ENTITY)->getAll();
	}
	
	public function getOne($id)
	{
		return $this->em->getRepository(TipoAdicional::ORM_ENTITY)->getOne($id);
	}
	
	public function crear($request,$validator)
	{
		return $this->em->getRepository(TipoAdicional::ORM_ENTITY)->crear($request,$validator);
	}
	public function editar($request,$id,$validator)
	{
		return $this->em->getRepository(TipoAdicional::ORM_ENTITY)->editar($request,$id,$validator);
	}
}