<?php
namespace Hertz\ReservaBundle\Service;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Response;

use Hertz\ReservaBundle\Entity\TipoIIBB;

class TipoIIBBService{
	
	/** 
	 * 
	 * @var EntityManager
	 */
	private $em;

	public function __construct(EntityManager $em)
	{
		$this->em = $em;
		$config = $em->getConfiguration();
	}

	public function getAll($request)
	{
		return $this->em->getRepository(TipoIIBB::ORM_ENTITY)->getAll($request);
	}
		
	public function getOne($id)
	{
		return $this->em->getRepository(TipoIIBB::ORM_ENTITY)->getOne($id);
	}
	

}