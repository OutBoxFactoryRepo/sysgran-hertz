<?php
namespace Hertz\ReservaBundle\Service;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\EntityManager;


use Hertz\ReservaBundle\Entity\ModuloPerfil;

class ModuloPerfilService{
	
	/** 
	 * 
	 * @var EntityManager
	 */
	private $em;

	public function __construct(EntityManager $em)
	{
		$this->em = $em;
		$config = $em->getConfiguration();
	}

	public function getAll()
	{
		return $this->em->getRepository(ModuloPerfil::ORM_ENTITY)->getAll();
	}

	public function crear($request,$validator,$user)
	{
		return $this->em->getRepository(ModuloPerfil::ORM_ENTITY)->crear($request,$validator,$user);
	}

}