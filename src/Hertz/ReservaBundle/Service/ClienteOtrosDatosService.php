<?php
namespace Hertz\ReservaBundle\Service;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Response;

use Hertz\ReservaBundle\Entity\ClienteOtrosDatos;

class ClienteOtrosDatosService{
	
	/** 
	 * 
	 * @var EntityManager
	 */
	private $em;

	public function __construct(EntityManager $em){
		$this->em = $em;
		$config = $em->getConfiguration();
	}

	public function getAll()
	{
		return $this->em->getRepository(ClienteOtrosDatos::ORM_ENTITY)->getAll();
	}
	
        public function getByNombres($word)
	{
		return $this->em->getRepository(ClienteOtrosDatos::ORM_ENTITY)->getByNombres($word);
	}
        
        
	public function getOne($id)
	{
		return $this->em->getRepository(ClienteOtrosDatos::ORM_ENTITY)->getOne($id);
	}
	
	public function crear($request)
	{
		return $this->em->getRepository(ClienteOtrosDatos::ORM_ENTITY)->crear($request);
	}
	public function editar($request,$id)
	{
		return $this->em->getRepository(ClienteOtrosDatos::ORM_ENTITY)->editar($request,$id);
	}
        
        public function eliminar($request)
	{
		return $this->em->getRepository(ClienteOtrosDatos::ORM_ENTITY)->eliminar($request);
	}
        
}