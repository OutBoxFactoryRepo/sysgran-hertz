<?php
namespace Hertz\ReservaBundle\Service;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Response;

use Hertz\ReservaBundle\Entity\Agencia;

class AgenciaService{
	
	/** 
	 * 
	 * @var EntityManager
	 */
	private $em;

	public function __construct(EntityManager $em){
		$this->em = $em;
		$config = $em->getConfiguration();
	}

	public function getAll($sucursal)
	{
		return $this->em->getRepository(Agencia::ORM_ENTITY)->getAll($sucursal);
	}
	
	public function getAllCombo($sucursal)
	{
		return $this->em->getRepository(Agencia::ORM_ENTITY)->getAllCombo($sucursal);
	}
	
	public function getAllComboView()
	{
		return $this->em->getRepository(Agencia::ORM_ENTITY)->getAllComboView();
	}
	
	
	public function getAllAdicYTodas($sucursal)
	{
		return $this->em->getRepository(Agencia::ORM_ENTITY)->getAllAdicYTodas($sucursal);
	}
	
	public function getOne($id)
	{
		return $this->em->getRepository(Agencia::ORM_ENTITY)->getOne($id);
	}
	
	public function crear($request,$validator,$user)
	{
		return $this->em->getRepository(Agencia::ORM_ENTITY)->crear($request,$validator,$user);
	}
	public function editar($request,$validator,$user)
	{
		return $this->em->getRepository(Agencia::ORM_ENTITY)->editar($request,$validator,$user);
	}

    /**
     * Obtener las agencias de una franquicia X
     * @param $sucursal
     * @param null $franquicia
     * @return mixed
     */
    public function searchBy($sucursal, $franquicia = null)
    {
        return $this->em->getRepository(Agencia::ORM_ENTITY)->searchBy($sucursal, $franquicia);
    }


    /**
     * Adiciona o Elimina un Agencia según su franquicia
     * @param $request
     * @param $validator
     * @param $user
     * @return mixed
     */
    public function editarFranquicia($request,$validator,$user)
    {
        return $this->em->getRepository(Agencia::ORM_ENTITY)->editFranquicia($request,$validator,$user);
    }
}