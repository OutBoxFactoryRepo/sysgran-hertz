<?php
namespace Hertz\ReservaBundle\Service;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\EntityManager;


use Hertz\ReservaBundle\Entity\Reserva;

class SearchService{
	
	/** 
	 * 
	 * @var EntityManager
	 */
	private $em;

	public function __construct(EntityManager $em){
		$this->em = $em;
		$config = $em->getConfiguration();
	}

	public function getAll($request, $user,$sucursal)
	{
		return $this->em->getRepository(Reserva::ORM_ENTITY)->getAll($request, $user,$sucursal);
	}
        
        public function getResXVehiculo($request, $user)
	{
		return $this->em->getRepository(Reserva::ORM_ENTITY)->getResXVehiculo($request, $user);
	}
}