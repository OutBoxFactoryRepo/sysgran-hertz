<?php
namespace Hertz\ReservaBundle\Service;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Response;

use Hertz\ReservaBundle\Entity\Vehiculo;

class VehiculoService{
	
	/** 
	 * 
	 * @var EntityManager
	 */
	private $em;

	public function __construct(EntityManager $em){
		$this->em = $em;
		$config = $em->getConfiguration();
	}

	public function getAll($request,$sucursal,$superAdmin)
	{
		return $this->em->getRepository(Vehiculo::ORM_ENTITY)->getAll($request,$sucursal,$superAdmin);
	}
	
	public function getOne($id)
	{
		return $this->em->getRepository(Vehiculo::ORM_ENTITY)->getOne($id);
	}
	
        public function getBy($word,$fRSalida,$fRDevolucion,$horaSalida,$horaDevolucion,$categoria)
	{
                return $this->em->getRepository(Vehiculo::ORM_ENTITY)->getBy($word,$fRSalida,$fRDevolucion,$horaSalida,$horaDevolucion,$categoria);
	}
        
        public function getBySinFecha($word)
	{
		return $this->em->getRepository(Vehiculo::ORM_ENTITY)->getBySinFecha($word);
	}
        
	public function crear($request,$validator,$user)
	{
		return $this->em->getRepository(Vehiculo::ORM_ENTITY)->crear($request,$validator,$user);
	}
	public function editar($request,$validator,$user)
	{
		return $this->em->getRepository(Vehiculo::ORM_ENTITY)->editar($request,$validator,$user);
	}
        
        public function getAllExcell($request,$sucursal)
	{
		return $this->em->getRepository(Vehiculo::ORM_ENTITY)->getAllExcell($request,$sucursal);
	}
        
        
}