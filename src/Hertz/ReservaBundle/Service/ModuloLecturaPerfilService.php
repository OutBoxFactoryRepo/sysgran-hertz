<?php
namespace Hertz\ReservaBundle\Service;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\EntityManager;
use Hertz\ReservaBundle\Entity\ModuloLecturaPerfil;

class ModuloLecturaPerfilService{
	
	/** 
	 * 
	 * @var EntityManager
	 */
	private $em;

	public function __construct(EntityManager $em)
	{
		$this->em = $em;
		$config = $em->getConfiguration();
	}

	public function getAll()
	{
		return $this->em->getRepository(ModuloLecturaPerfil::ORM_ENTITY)->getAll();
	}

	public function crear($request,$validator,$user)
	{
		return $this->em->getRepository(ModuloLecturaPerfil::ORM_ENTITY)->crear($request,$validator,$user);
	}

	public function isReadOnly($request, $validator, $user, $module)
	{
		return $this->em->getRepository(ModuloLecturaPerfil::ORM_ENTITY)->isReadOnly($request, $validator, $user, $module);
	}

}