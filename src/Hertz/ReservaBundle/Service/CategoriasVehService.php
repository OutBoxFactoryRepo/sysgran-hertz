<?php
namespace Hertz\ReservaBundle\Service;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Response;

use Hertz\ReservaBundle\Entity\CategoriasVeh;

class CategoriasVehService{
	
	/** 
	 * 
	 * @var EntityManager
	 */
	private $em;

	public function __construct(EntityManager $em)
	{
		$this->em = $em;
		$config = $em->getConfiguration();
	}

	public function getAll($request,$sucursal)
	{
		return $this->em->getRepository(CategoriasVeh::ORM_ENTITY)->getAll($request,$sucursal);
	}
		
	public function getOne($id)
	{
		return $this->em->getRepository(CategoriasVeh::ORM_ENTITY)->getOne($id);
	}
	
	public function crear($request,$validator,$user)
	{
		return $this->em->getRepository(CategoriasVeh::ORM_ENTITY)->crear($request,$validator,$user);
	}
	
}