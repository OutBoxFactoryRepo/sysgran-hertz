<?php
/**
 * Created by PhpStorm.
 * User: hfuentesbx
 * Date: 28/01/2017
 * Time: 02:01 PM
 */

namespace Hertz\ReservaBundle\Service;


use Doctrine\ORM\EntityManager;
use Hertz\ReservaBundle\Entity\MateriaPrimaCategoria;

class MateriaPrimaCategoriaService
{
    /**
     *
     * @var EntityManager
     */
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $config = $em->getConfiguration();
    }

    public function getAll()
    {
        return $this->em->getRepository(MateriaPrimaCategoria::ORM_ENTITY)->getAll();
    }

    public function getAllCombo($request,$sucursal)
    {
        return $this->em->getRepository(MateriaPrimaCategoria::ORM_ENTITY)->getAllCombo($request,$sucursal);
    }


    public function getOne($id)
    {
        return $this->em->getRepository(MateriaPrimaCategoria::ORM_ENTITY)->getOne($id);
    }

    public function crear($request,$validator,$user)
    {
        return $this->em->getRepository(MateriaPrimaCategoria::ORM_ENTITY)->crear($request,$validator,$user);
    }
    public function editar($request,$validator,$user)
    {
        return $this->em->getRepository(MateriaPrimaCategoria::ORM_ENTITY)->editar($request,$validator,$user);
    }
}