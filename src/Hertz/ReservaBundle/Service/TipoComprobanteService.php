<?php
namespace Hertz\ReservaBundle\Service;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Response;

use Hertz\ReservaBundle\Entity\TipoComprobante;

class TipoComprobanteService{
	
	/** 
	 * 
	 * @var EntityManager
	 */
	private $em;

	public function __construct(EntityManager $em){
		$this->em = $em;
		$config = $em->getConfiguration();
	}

	public function getAll()
	{
		return $this->em->getRepository(TipoComprobante::ORM_ENTITY)->getAll();
	}
	
	public function getOne($id)
	{
		return $this->em->getRepository(TipoComprobante::ORM_ENTITY)->getOne($id);
	}
	
}