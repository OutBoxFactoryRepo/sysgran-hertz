<?php
namespace Hertz\ReservaBundle\Service;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Response;

use Hertz\ReservaBundle\Entity\CotizaMoneda;

class CotizaMonedaService{
	
	/** 
	 * 
	 * @var EntityManager
	 */
	private $em;

	public function __construct(EntityManager $em){
		$this->em = $em;
		$config = $em->getConfiguration();
	}

	public function getAll()
	{
		return $this->em->getRepository(CotizaMoneda::ORM_ENTITY)->getAll();
	}
	
	public function getOne($id)
	{
		return $this->em->getRepository(CotizaMoneda::ORM_ENTITY)->getOne($id);
	}
	
	public function crear($request)
	{
		return $this->em->getRepository(CotizaMoneda::ORM_ENTITY)->crear($request);
	}
	
}