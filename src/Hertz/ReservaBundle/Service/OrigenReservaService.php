<?php
namespace Hertz\ReservaBundle\Service;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Response;

use Hertz\ReservaBundle\Entity\OrigenReserva;

class OrigenReservaService{
	
	/** 
	 * 
	 * @var EntityManager
	 */
	private $em;

	public function __construct(EntityManager $em){
		$this->em = $em;
		$config = $em->getConfiguration();
	}

	public function getAll($id)
	{
		return $this->em->getRepository(OrigenReserva::ORM_ENTITY)->getAll($id);
	}
	
	public function getOne($id)
	{
		return $this->em->getRepository(OrigenReserva::ORM_ENTITY)->getOne($id);
	}
}