<?php
namespace Hertz\ReservaBundle\Service;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Response;

use Hertz\ReservaBundle\Entity\Cargo;

class CargoService{
	
	/** 
	 * 
	 * @var EntityManager
	 */
	private $em;

	public function __construct(EntityManager $em){
		$this->em = $em;
		$config = $em->getConfiguration();
	}

	public function getAll()
	{
		return $this->em->getRepository(Cargo::ORM_ENTITY)->getAll();
	}
	
	public function getOne($id)
	{
		return $this->em->getRepository(Cargo::ORM_ENTITY)->getOne($id);
	}
	
	public function crear($request,$validator)
	{
		return $this->em->getRepository(Cargo::ORM_ENTITY)->crear($request,$validator);
	}
	public function editar($request,$id,$validator)
	{
		return $this->em->getRepository(Cargo::ORM_ENTITY)->editar($request,$id,$validator);
	}
}