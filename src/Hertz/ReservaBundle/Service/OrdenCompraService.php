<?php
namespace Hertz\ReservaBundle\Service;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Response;

use Hertz\ReservaBundle\Entity\OrdenCompra;

class OrdenCompraService{
	
	/** 
	 * 
	 * @var EntityManager
	 */
	private $em;

	public function __construct(EntityManager $em){
		$this->em = $em;
		$config = $em->getConfiguration();
	}

	public function getAll($request,$sucursal,$user,$fromOcAdmin = false)
	{
		return $this->em->getRepository(OrdenCompra::ORM_ENTITY)->getAll($request,$sucursal,$user,$fromOcAdmin);
	}
	
	public function getOne($id)
	{
		return $this->em->getRepository(OrdenCompra::ORM_ENTITY)->getOne($id);
	}
	
        public function actualizarOt($id)
	{
		return $this->em->getRepository(OrdenCompra::ORM_ENTITY)->actualizarOt($id);
	}
        
        
	public function crear($request,$validator,$user,$sucursal)
	{
		return $this->em->getRepository(OrdenCompra::ORM_ENTITY)->crear($request,$validator,$user,$sucursal);
	}
	public function editar($request,$validator,$user)
	{
		return $this->em->getRepository(OrdenCompra::ORM_ENTITY)->editar($request,$validator,$user);
	}
}