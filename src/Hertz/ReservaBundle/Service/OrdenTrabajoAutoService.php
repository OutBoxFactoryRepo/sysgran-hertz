<?php
namespace Hertz\ReservaBundle\Service;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Response;

use Hertz\ReservaBundle\Entity\OrdenTrabajoAuto;

class OrdenTrabajoAutoService{
	
	/** 
	 * 
	 * @var EntityManager
	 */
	private $em;

	public function __construct(EntityManager $em){
		$this->em = $em;
		$config = $em->getConfiguration();
	}

	public function getAll($request, $user)
	{
		return $this->em->getRepository(OrdenTrabajoAuto::ORM_ENTITY)->getAll($request, $user);
	}
	
	public function getOne($id)
	{
		return $this->em->getRepository(OrdenTrabajoAuto::ORM_ENTITY)->getOne($id);
	}
	
	public function crear($request,$validator,$user)
	{
		return $this->em->getRepository(OrdenTrabajoAuto::ORM_ENTITY)->crear($request,$validator,$user);
	}
	public function editar($request,$validator,$user)
	{
		return $this->em->getRepository(OrdenTrabajoAuto::ORM_ENTITY)->editar($request,$validator,$user);
	}
}