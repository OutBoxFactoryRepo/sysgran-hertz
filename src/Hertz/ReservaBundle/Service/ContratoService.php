<?php
namespace Hertz\ReservaBundle\Service;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Response;

use Hertz\ReservaBundle\Entity\Contrato;

class ContratoService{
	
	/** 
	 * 
	 * @var EntityManager
	 */
	private $em;

	public function __construct(EntityManager $em){
		$this->em = $em;
		$config = $em->getConfiguration();
	}

	public function getAll($request,$sucursal)
	{
		return $this->em->getRepository(Contrato::ORM_ENTITY)->getAll($request,$sucursal);
	}
	
	public function getOne($id)
	{
		return $this->em->getRepository(Contrato::ORM_ENTITY)->getOne($id);
	}
	
        public function getLock($id,$user)
	{
		return $this->em->getRepository(Contrato::ORM_ENTITY)->getLock($id,$user);
	}
        
        public function setLock($id,$user)
	{
		return $this->em->getRepository(Contrato::ORM_ENTITY)->setLock($id,$user);
	}
        
        public function getBy($word)
	{
		return $this->em->getRepository(Contrato::ORM_ENTITY)->getBy($word);
	}
        
	public function crear($request,$validator,$user)
	{
		return $this->em->getRepository(Contrato::ORM_ENTITY)->crear($request,$validator,$user);
	}
	public function editar($request,$validator)
	{
		return $this->em->getRepository(Contrato::ORM_ENTITY)->editar($request,$validator);
	}
}