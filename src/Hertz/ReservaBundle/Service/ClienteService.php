<?php
namespace Hertz\ReservaBundle\Service;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Response;

use Hertz\ReservaBundle\Entity\Cliente;

class ClienteService{
	
	/** 
	 * 
	 * @var EntityManager
	 */
	private $em;

	public function __construct(EntityManager $em){
		$this->em = $em;
		$config = $em->getConfiguration();
	}

	public function getAll($request, $user)
	{
		return $this->em->getRepository(Cliente::ORM_ENTITY)->getAll($request, $user);
	}
	
        public function getByNombres($word)
	{
		return $this->em->getRepository(Cliente::ORM_ENTITY)->getByNombres($word);
	}
        
        
	public function getOne($id)
	{
		return $this->em->getRepository(Cliente::ORM_ENTITY)->getOne($id);
	}
	
	public function crear($request)
	{
		return $this->em->getRepository(Cliente::ORM_ENTITY)->crear($request);
	}
	public function editar($request,$id)
	{
		return $this->em->getRepository(Cliente::ORM_ENTITY)->editar($request,$id);
	}
        
        public function getClienteByNombre($word)
	{
		return $this->em->getRepository(Vehiculo::ORM_ENTITY)->getClienteByNombre($word);
	}
}