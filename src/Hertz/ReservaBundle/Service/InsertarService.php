<?php
namespace Hertz\ReservaBundle\Service;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use Hertz\ReservaBundle\Entity\Reserva;

class InsertarService{
	
	/** 
	 * 
	 * @var EntityManager
	 */
	private $em;

	public function __construct(EntityManager $em){
		$this->em = $em;
		$config = $em->getConfiguration();
	}

	public function insertar($request,$validator,$user,$sucursal)
	{            
            return $this->em->getRepository(Reserva::ORM_ENTITY)->insertar($request,$validator,$user,$sucursal);
	}
}