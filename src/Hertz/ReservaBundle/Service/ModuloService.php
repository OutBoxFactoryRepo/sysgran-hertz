<?php
namespace Hertz\ReservaBundle\Service;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\EntityManager;


use Hertz\ReservaBundle\Entity\Modulo;

class ModuloService{
	
	/** 
	 * 
	 * @var EntityManager
	 */
	private $em;

	public function __construct(EntityManager $em)
	{
		$this->em = $em;
		$config = $em->getConfiguration();
	}

	public function getAll($request)
	{
		return $this->em->getRepository(Modulo::ORM_ENTITY)->getAll($request);
	}
	
        public function getAllAsginados($request,$user)
	{
		return $this->em->getRepository(Modulo::ORM_ENTITY)->getAllAsignados($request,$user);
	}
        
	public function getAllCombo($request,$sucursal)
	{
		return $this->em->getRepository(Modulo::ORM_ENTITY)->getAllCombo($request,$sucursal);
	}
	
	
	public function getOne($id)
	{
		return $this->em->getRepository(Modulo::ORM_ENTITY)->getOne($id);
	}
	
	public function crear($request,$validator,$user)
	{
		return $this->em->getRepository(Modulo::ORM_ENTITY)->crear($request,$validator,$user);
	}
	
        public function editar($request,$validator,$user)
	{
		return $this->em->getRepository(Modulo::ORM_ENTITY)->editar($request,$validator,$user);
	}
        
        public function getRoles($request,$validator,$user)
	{
		return $this->em->getRepository(Modulo::ORM_ENTITY)->getRoles($request,$validator,$user);
	}
        
        public function addRol($request,$validator,$user)
	{
		return $this->em->getRepository(Modulo::ORM_ENTITY)->addRol($request,$validator,$user);
	}
        
        public function delRol($request,$validator,$user)
	{
		return $this->em->getRepository(Modulo::ORM_ENTITY)->delRol($request,$validator,$user);
	}
        
        
}