<?php
namespace Hertz\ReservaBundle\Service;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Response;


use Hertz\ReservaBundle\Entity\Reserva;

class GetOneService{
	
	/** 
	 * 
	 * @var EntityManager
	 */
	private $em;

	public function __construct(EntityManager $em){
		$this->em = $em;
		$config = $em->getConfiguration();
	}

	public function getOne($id)
	{
		return $this->em->getRepository(Reserva::ORM_ENTITY)->getOne($id);
	}
        
        public function getReserva($id)
	{
		return $this->em->getRepository(Reserva::ORM_ENTITY)->getReserva($id);
	}
        
        public function getOneArray($id)
	{
		return $this->em->getRepository(Reserva::ORM_ENTITY)->getOneArray($id);
	}
        
        
        public function addPasAdicional($request)
	{
		return $this->em->getRepository(Reserva::ORM_ENTITY)->addPasAdicional($request);
	}
}