<?php
namespace Hertz\ReservaBundle\Service;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Response;

use Hertz\ReservaBundle\Entity\Reparacion;

class ReparacionService{
	
	/** 
	 * 
	 * @var EntityManager
	 */
	private $em;

	public function __construct(EntityManager $em){
		$this->em = $em;
		$config = $em->getConfiguration();
	}

	public function getAll($sucursal)
	{
		return $this->em->getRepository(Reparacion::ORM_ENTITY)->getAll($sucursal);
	}
	
	public function getAllCombo($sucursal)
	{
		return $this->em->getRepository(Reparacion::ORM_ENTITY)->getAllCombo($sucursal);
	}
	
	public function getAllComboView()
	{
		return $this->em->getRepository(Reparacion::ORM_ENTITY)->getAllComboView();
	}
	
	
	public function getAllAdicYTodas($sucursal)
	{
		return $this->em->getRepository(Reparacion::ORM_ENTITY)->getAllAdicYTodas($sucursal);
	}
	
	public function getOne($id)
	{
		return $this->em->getRepository(Reparacion::ORM_ENTITY)->getOne($id);
	}
	
	public function crear($request,$validator,$user)
	{
		return $this->em->getRepository(Reparacion::ORM_ENTITY)->crear($request,$validator,$user);
	}
	public function editar($request,$validator,$user)
	{
		return $this->em->getRepository(Reparacion::ORM_ENTITY)->editar($request,$validator,$user);
	}
        
        public function getTareasGrilla($idReparacion,$request)
	{
		return $this->em->getRepository(Reparacion::ORM_ENTITY)->getTareasGrilla($idReparacion,$request);
	}
}