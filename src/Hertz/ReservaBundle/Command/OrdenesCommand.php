<?php
namespace Hertz\ReservaBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
//use Acme\DemoBundle\Form\ContactType;

use JMS\Serializer\SerializerBuilder;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;


use Hertz\ReservaBundle\Entity\OrdenTrabajoAuto;

class OrdenesCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this
            ->setName('ordenes')
            ->setDescription('Genera Ordenes automáticas')
            ->addArgument(
                'primerParametro',
                InputArgument::OPTIONAL,
                'algo'
            )
            ->addOption(
                'opcion',
                null,
                InputOption::VALUE_NONE,
                'If set, the task will yell in uppercase letters'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $primerParametro = $input->getArgument('primerParametro');
        if ($primerParametro) 
        {
            $text = 'parametro: '.$primerParametro;
        } 
        else 
        {
            
            $em = $this->getContainer()->get('doctrine.orm.entity_manager');
            $qb = $em->getRepository(OrdenTrabajoAuto::ORM_ENTITY)->procesarAuto();
            
            //@todo Aquí debo hacer la lógica para generar las OT automáticas
            
            
            $arrayx = $qb;
            
            if(is_array($arrayx))
            {
                for($a = 0;$a  < count($arrayx); $a++)
                {
                    $output->writeln($arrayx[$a]);
                }
            }
            else
            {
                 $output->writeln("no es un array");
            }
            //print_r($arrayx);die();
                die();
        }

        if ($input->getOption('opcion')) 
        {
            $text = strtoupper($text);
        }

        $output->writeln($text);
    }
}
