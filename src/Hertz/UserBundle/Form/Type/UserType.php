<?php
namespace Hertz\UserBundle\Form\Type;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;

use Hertz\UserBundle\Service\UserService;
use Hertz\UserBundle\Entity\User;
use Hertz\UserBundle\Entity\Role;

class UserType extends AbstractType{

	/**
	 *
	 * @var UserService
	 */
	private $userSvc;

	public function __construct(UserService $userSvc){
		$this->userSvc = $userSvc;
	}

	public function setDefaultOptions(OptionsResolverInterface $resolver){
		$resolver->setDefaults(array(
				'data_class' => 'Hertz\UserBundle\Entity\User',
		));
	}

	public function getName() {
		return 'user';
	}

	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options){
		$required = $options['data']->getId() == null;
		$builder->add('assignedRoles', 'entity', array(
				'class' => Role::ORM_ENTITY,
				'property' => 'name',
				'expanded' => true,
				'multiple' => true,
		));
		$builder->add('user', 'text', array('required' => true));
		$builder->add('description', 'textarea', array('required' => false));
		$builder->add('status', 'checkbox', array('required'=>false));
		$builder->add('isSuperAdmin', 'checkbox', array('required'=>false));		
		$builder->add('password', 'repeated', array('type'=>'password', 'invalid_message' => 'Las contraseñas deben coincidir', 'required'=>$required, 'first_options'=>array('label'=>'password'), 'second_options'=>array('label'=>'repeat password')));
		$builder->add('name', 'text', array('required' => false));
		$builder->add('lastName', 'text', array('required' => false));
		$builder->add('email', 'email', array('required' => false));
	}
}