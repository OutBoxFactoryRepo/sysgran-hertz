<?php
namespace Hertz\UserBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\AbstractType;

use Hertz\UserBundle\Service\UserService;

class PermissionType extends AbstractType{

	/**
	 *
	 * @var UserService
	 */
	private $userSvc;

	public function __construct(UserService $userSvc){
		$this->userSvc = $userSvc;
	}

	/*public function setDefaultOptions(OptionsResolverInterface $resolver){
		$resolver->setDefaults(array(
				'data_class' => 'Hertz\UserBundle\Entity\Permission',
		));
	}*/

	public function getName() {
		return 'permission';
	}

	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {
		$builder->add('name', 'text', array(
				'label' => 'Nombre',
				'max_length' => 50,
				'required' => true,
		));

		$builder->add('description', 'textarea', array(
				'label' => 'Descripcion',
				'max_length' => 255,
				'required' => false,
		));

	}

}