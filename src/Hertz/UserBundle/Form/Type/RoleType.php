<?php
namespace Hertz\UserBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\AbstractType;

use Hertz\UserBundle\Entity\Permission;

class RoleType extends AbstractType{

	public function __construct(){
	}

	public function setDefaultOptions(OptionsResolverInterface $resolver){
		$resolver->setDefaults(array(
				'data_class' => 'Hertz\UserBundle\Entity\Role',
		));
	}

	public function getName() {
		return 'role';
	}

	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {
		$builder->add('name', 'text', array(
				'label' => 'Nombre',
				'max_length' => 100,
				'required' => true,
		));

		$builder->add('description', 'textarea', array(
				'label' => 'Descripcion',
				'max_length' => 255,
				'required' => false,
		));

		$builder->add('assignedPermissions', 'entity', array(
				'class' => Permission::ORM_ENTITY,
				'property' => 'name',
				'expanded' => true,
				'multiple' => true,
		));
	}
}