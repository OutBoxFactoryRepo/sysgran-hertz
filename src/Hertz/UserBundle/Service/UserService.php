<?php
namespace Hertz\UserBundle\Service;

use Doctrine\ORM\EntityManager;

use Hertz\UserBundle\Exception\ParameterException;
use Hertz\UserBundle\Exception\NotFoundException;
use Hertz\UserBundle\Entity\User;
use Hertz\UserBundle\Entity\UserRepository;
use Hertz\UserBundle\Entity\Role;
use Hertz\UserBundle\Entity\RoleRepository;
use Hertz\UserBundle\Entity\Permission;
use Hertz\UserBundle\Entity\PermissionRepository;
use Hertz\ReservaBundle\Entity\Agencia;
use Hertz\ReservaBundle\Entity\Franquiciados;

class UserService{

	private $container;
 
	/**
	 * @var EntityManager
	 */
	private $em;

	/**
	 * @var PasswordEncoder
	 */
	private $passwordEncoder;

	/**
	 * @var Validator
	 */
	private $validator;

	public function __construct($container){
		$this->container = $container;
		$this->em = $this->container->get('doctrine.orm.entity_manager');
		$this->validator = $this->container->get('validator');

		$encoderFactory = $this->container->get('security.encoder_factory');
		$this->passwordEncoder = $encoderFactory->getEncoder(new User());
	}

	/*Usuario*/

        /**
	 *
	 * @param int $idUser
         * @param int $idRol
	 */
        public function removeRolAssigned($idUser,$idRol)
        {
            $rol = $this->getByIdRole($idRol);
            
            //$menorRol = $this->em->getRepository(Role::ORM_ENTITY)->finMinorRol();
            
            //if($menorRol === $rol)
            //{
            //    return false;
            //}
            //else
            //{
                $user = $this->getByIdUser($idUser);
                $user->removeAssignedRole($rol);
                $this->save($user);
                return true;
            //}
        }
        
        
        /**
	 *
	 * @param int $idUser
         * @param int $idRol
	 */
        public function addRolAssigned($idUser,$idRol)
        {
            $rol = $this->getByIdRole($idRol);
            $user = $this->getByIdUser($idUser);
            $user->addAssignedRole($rol);
            $this->save($user);
            return true;
        }
        
	/**
	 *
	 * @param array $data
	 * @param string $id
	 * @throws \Exception
	 * @return \Hertz\UserBundle\Entity\User
	 */
	public function createOrUpdate($data, $id=null){

		$notFoundException = new NotFoundException();
		$parameterException = new ParameterException();
                
		if(intval($id)){
			$user = $this->em->getRepository(User::ORM_ENTITY)->findOneById($id);
			$this->em->refresh($user);
		}else{
			$user = new User();
		}

		if(!$user){
			$notFoundException->setError('El usuario no existe');
			throw $notFoundException;
		}

		$defaultData = $user->toArray();
                
                

		foreach($defaultData as $k => $v){
			$data[$k] = isset($data[$k]) ? $data[$k] : $v;
		}

		$data = array_merge($defaultData, $data);

		//$password = null;//'admin';//null;
                $password = $data['password'];//'admin';//null;
                
		if(strlen(trim($password)) > 0)//si llega un password
                {
			$password = $this->passwordEncoder->encodePassword(trim($password), null); //encodeo el password
			$password = $password;
		}
                //elseif(strlen(trim($password)) == 0 && $user->getId() !== null) //si no llega un password pero el usuario existe
                //{
		//	$password = $user->getPassword();//le pongo el mismo password
		//}
                elseif(strlen(trim($password)) == 0 && $user->getId() === null) //cuando no existe un usuario, es un usuario nuevo
                {
                    	$password = $this->passwordEncoder->encodePassword(trim("inicio123"), null); //passwordpordefecto
                        //le asigno el menor rol como rol por defecto.
                        $menorRol = $this->em->getRepository(Role::ORM_ENTITY)->finMinorRol();
                        $user->addAssignedRole($menorRol);
                }

                //print_r($data['sucursal']);die();
        $agencia = $this->getSucursal($data['sucursal']);
        
            $franquiciado = "";
            if($data['franquiciado'] != "")
            {
                $franquiciado = $this->getFranquiciado($data['franquiciado']);
            }
        //echo "fran: ".$franquiciado;die();
		$user->setUser($data['user']);
		if($password != null)
                {
                    $user->setPassword($password);
                }
		$user->setStatus($data['status']);
		$user->setIsSuperAdmin($data['isSuperAdmin']);
		$user->setName($data['name']);
		$user->setSucursal($agencia);
                
                if($franquiciado != "")
                {
                    //echo $franquiciado;die();
                    $user->setFranquiciado($franquiciado);
                }
		$user->setLastName($data['lastName']);
		$user->setEmail($data['email']);
		$user->setDescription($data['description']);
		$user->setModified(new \DateTime());
                    
                    $errors = $this->validator->validate($user);

                    if(is_null($password)){
                            $parameterException->setError('No puede estar vacio', 'password');
                    }

                    if(count($errors) > 0){
                            foreach($errors as $error){
                                    $parameterException->setError($error->getMessage(), $error->getPropertyPath());
                            }
                    }

                    if($parameterException->isException()){
                            throw $parameterException;
                    }
               
                    /*
                    $rolesRemove = $user->getAssignedRoles();
                    
                    foreach($rolesRemove as $role)
                    {

                            $user->removeAssignedRole($role);
                    }
                    
                    $rolesAdd = $data['assignedRoles'];

                    if($rolesAdd)
                    {
                            foreach($rolesAdd as $roleId)
                            {
                                    echo $roleId;
                                    $role = $this->em->getRepository(Role::ORM_ENTITY)->findOneById($roleId);
                                    if($role)
                                    {
                                            $user->addAssignedRole($role);
                                    }
                            }
                    }
                    */
                
		$this->save($user);

		return $user;
	}

	/**
	 *
	 * @param mixed $id
	 * @return User deleted user
	 */
	public function deleteUser($id){
		$user = $this->em->getRepository(User::ORM_ENTITY)->findOneById($id);
	
		if(!$user){
			throw new \Exception('Usuario no registrado.');
		}
	
		$this->delete($user);
	
		return $user;
	}
	

	/**
	 * Returns the User list
	 * @return mixed empty|User
	 */
	public function getGrilla($orden,$campo,$inicio,$fin,$pagina,$registros,$current,$searchOper,$campoBusqueda,$valorCampoBusqueda){
		return $this->em->getRepository(User::ORM_ENTITY)->findGrilla($orden,$campo,$inicio,$fin,$pagina,$registros,$current,$searchOper,$campoBusqueda,$valorCampoBusqueda);
	}
        
        
        
	/**
	 * Returns the User list
	 * @return mixed empty|User
	 */
	public function getUser(){
		return $this->em->getRepository(User::ORM_ENTITY)->findByDeleted(null);
	}

	/**
	 * Returns the User list
	 * @return mixed empty|User
	 */
	public function getUserSucursal($sucursal){
		return $this->em->getRepository(User::ORM_ENTITY)->findBySucursal($sucursal);
	}
	
	
	/**
	 *@return mixed empty|User
	 */
	public function getByIdUser($id){
		return $this->em->getRepository(User::ORM_ENTITY)->findOneById($id);
	}	

	/**
	 *@return mixed empty|User
	 */
	public function getBySucursal($sucursal){
		return $this->em->getRepository(User::ORM_ENTITY)->findOneBySucursal($sucursal);
	}
	
	/**
	 *@return mixed null|array User
	 */
	public function getByIdUserArray($id){
		$qb = $this->em->createQueryBuilder();
		$qb->select('u');
		$qb->from(User::ORM_ENTITY, 'u');
		$qb->where('u.id = :id')->setParameter('id', $id);
		$user = $qb->getQuery()->getArrayResult();
		if($user) return $user[0];
		return null; 
	}

	/**
	 *@return mixed empty|User
	 */
	public function getByUserUser($user){
		return $this->em->getRepository(User::ORM_ENTITY)->findOneBy(array('user'=>$user, 'deleted'=>null));
	}

        /**
	 *@return mixed empty|User
	 */
	public function getByUserUserId($id){
		return $this->em->getRepository(User::ORM_ENTITY)->findOneById($id);
	}
        
	/*Role*/

	/**
	 * @param array $data
	 * @param string $id
	 * @throws \Exception
	 * @return \Hertz\UserBundle\Entity\Role
	 */
	public function createOrUpdateRole($data, $id=null){
		$notFoundException = new NotFoundException();
		$parameterException = new ParameterException();
                
		if(intval($id)){
			$role = $this->em->getRepository(Role::ORM_ENTITY)->findOneById($id);
		}else{
			$role = new Role();
		}

		if(!$role){
			$notFoundException->setError('El rol no existe');
			throw $notFoundException;
		}

		$defaultData = $role->toArray();

		foreach($defaultData as $k => $v){
			$data[$k] = isset($data[$k]) ? $data[$k] : null;
		}

		$data = array_merge($defaultData, $data);

		$role->setName($data['name']);
		$role->setDescription($data['description']);
		$role->setCreated(new \DateTime());

		$errors = $this->validator->validate($role);

		if(count($errors) > 0){
			foreach($errors as $error){
				$parameterException->setError($error->getMessage(), $error->getPropertyPath());
			}
			throw $parameterException;
		}
	
		$permissionsRemove = $role->getAssignedPermissions();

		foreach($permissionsRemove as $permission){
			$role->removeAssignedPermission($permission);
		}

		$permissionsAdd = $data['assignedPermissions'];

		if($permissionsAdd){
			foreach($permissionsAdd as $permisionId){
				$permission = $this->em->getRepository(Permission::ORM_ENTITY)->findOneById($permisionId);
				if($permission){
					$role->addAssignedPermission($permission);
				}
			}
		}

		$this->save($role);

		return $role;
	}

	/**
	 * @param integer $id
	 * @throws \Exception
	 * @return Role
	 */
	public function deleteRole($id){
		$role = $this->em->getRepository(Role::ORM_ENTITY)->findOneById($id);

		if(!$role){
			throw new \Exception('El rol no existe.');
		}

		$this->delete($role);

		return $role;
	}

	/**
	 * @return mixed empty|Role
	 */
	public function getRole(){
		return $this->em->getRepository(Role::ORM_ENTITY)->findAll();
	}

	/**
	 * @param integer $id
	 * @return mixed empty|Role
	 */
	public function getByIdRole($id){
		return $this->em->getRepository(Role::ORM_ENTITY)->findOneById($id);
	}

	public function getByNameRole($name){
		return $this->em->getRepository(Role::ORM_ENTITY)->findOneByName($name);
	}

	public function getByDescriptionRole($description){
		return $this->em->getRepository(Role::ORM_ENTITY)->findOneByDescription($description);
	}

	/*Permission*/

	/**
	 * @param array $data
	 * @param integer $id
	 * @throws NotFoundException ParameterException
	 * @return Permission
	 */
	public function createOrUpdatePermission($data, $id=null){
		$notFoundException = new NotFoundException();
		$parameterException = new ParameterException();

		if(intval($id)){
			$permission = $this->em->getRepository(Permission::ORM_ENTITY)->findOneById($id);
		}else{
			$permission = new Permission();
		}

		if(!$permission){
			$notFoundException->setError('El permiso no existe');
			throw $notFoundException;
		}

		$defaultData = $permission->toArray();

		foreach($defaultData as $k => $v){
			$data[$k] = isset($data[$k]) ? $data[$k] : null;
		}

		$data = array_merge($defaultData, $data);

		$permission->setName($data['name']);
		$permission->setDescription($data['description']);

		$errors = $this->validator->validate($permission);

		if(count($errors) > 0){
			foreach($errors as $error){
				$parameterException->setError($error->getMessage(), $error->getPropertyPath());
			}
			throw $parameterException;
		}

		$this->save($permission);

		return $permission;
	}

	/**
	 * @return mixed empty|Permission
	 */
	public function getPermission(){
		return $this->em->getRepository(Permission::ORM_ENTITY)->findAll();
	}

	/**
	 * @return mixed empty|Permission
	 */
	public function getPermissionList(){
		return $this->em->getRepository(Permission::ORM_ENTITY)->getList();
	}

	/** 
	 * @param integer $id
	 * @return mixed empty|Permission
	 */
	public function getByIdPermission($id){
		return $this->em->getRepository(Permission::ORM_ENTITY)->findOneById($id);
	}

	public function getByNamePermission($name){
		return $this->em->getRepository(Permission::ORM_ENTITY)->findOneByName($name);
	}

	/**
	 * @param integer $id
	 * @throws 
	 * @return Permission
	 */
	public function deletePermission($id){
		$permission = $this->em->getRepository(Permission::ORM_ENTITY)->findOneById($id);

		if(!$permission){
			throw new \Exception('El permiso no existe.');
		}

		$this->delete($permission);

		return $permission;
	}

	private function save($entity){
		$this->em->persist($entity);
		$this->em->flush();
	}

	private function delete($entity){
		$this->em->remove($entity);
		$this->em->flush();
	}
        
        
 	private function getSucursal($id)
	{
	
		$qb = $this->em->getRepository(Agencia::ORM_ENTITY)->getOne($id);
		if(count($qb)>0)
		{
			return $qb[0];
		}
		else
		{
			return null;
		}
	}
        
        
 	private function getFranquiciado($id)
	{
	
		$qb = $this->em->getRepository(Franquiciados::ORM_ENTITY)->getOne($id);
		if(count($qb)>0)
		{
			return $qb[0];
		}
		else
		{
			return null;
		}
	}
 
        
	public function getAssignedRolPermissions($idRol)
        {
                
                $role = $this->em->getRepository(Role::ORM_ENTITY)->findOneById($idRol);
                
                if(!$role)
                {
                    return array("error"=>"No existe el rol");
                }
                $rolePermission = $role->getAssignedPermissions();
                
		

		return $rolePermission;
	}       
        
        
        public function addAssignedRolPermissions($idRol,$idPermiso)
        {
                
                $role = $this->em->getRepository(Role::ORM_ENTITY)->findOneById($idRol);
                $permiso = $this->em->getRepository(Permission::ORM_ENTITY)->findOneById($idPermiso);
                
                if(!$role)
                {
                    return array("error"=>"No existe el rol");
                }
                if(!$permiso)
                {
                    return array("error"=>"No existe el permiso");
                }
                $role->addAssignedPermission($permiso);
                $this->save($role);

		return $role;
	}       
        
        public function hasRolPermissionId($idRol,$idPermiso)
        {
            $permisos = $this->getAssignedRolPermissions($idRol);
            $arrayPermisos = array();
            foreach($permisos as $permiso)
            {
                $arrayPermisos[] = $permiso->getId();
            }
            
            return in_array($idPermiso, $arrayPermisos);
        }
       
        public function removeAssignedRolPermissions($idRol,$idPermiso)
        {
                
                $role = $this->em->getRepository(Role::ORM_ENTITY)->findOneById($idRol);
                $permiso = $this->em->getRepository(Permission::ORM_ENTITY)->findOneById($idPermiso);
                
                if(!$role)
                {
                    return array("error"=>"No existe el rol");
                }
                if(!$permiso)
                {
                    return array("error"=>"No existe el permiso");
                }
                $role->removeAssignedPermission($permiso);
                $this->save($role);

		return $role;
	}      
        
}
