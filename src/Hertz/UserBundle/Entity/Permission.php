<?php
namespace Hertz\UserBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Permission
 *
 * @ORM\Table(name="permission", indexes={ @ORM\Index(name="permission_idx", columns={"name"})}) )
 * @ORM\Entity(repositoryClass="PermissionRepository")
 * @UniqueEntity(fields={"name"}, message="El Permiso existe.")
 */
class Permission{
	const ORM_ENTITY = "HertzUserBundle:Permission";

	public function __construct()
	{
		$this->roles = new ArrayCollection();
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="string", length=50, nullable=false)
	 * @Assert\Regex(pattern="/^[a-z0-9_]{1,50}$/i", message="Ingrese letras, numeros y _ entre 1 y 50 caracteres.")
	 */
	protected $name;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	protected $description;

	/**
	 * @var ArrayCollection
	 *
	 * @ORM\ManyToMany(targetEntity="Role", mappedBy="assignedPermissions")
	 */
	protected $roles;

    public function toArray(){
    	return array(
			'id' => $this->id,
			'name' => $this->name,
			'description' => $this->description
    	);
    }

    /**
     * Add roles
     *
     * @param \Hertz\UserBundle\Entity\Role $roles
     * @return Permission
     */
    public function addRole(\Hertz\UserBundle\Entity\Role $roles)
    {
        $this->roles[] = $roles;

        return $this;
    }

    /**
     * Remove roles
     *
     * @param \Hertz\UserBundle\Entity\Role $roles
     */
    public function removeRole(\Hertz\UserBundle\Entity\Role $roles)
    {
        $this->roles->removeElement($roles);
    }

    /**
     * Get roles
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Permission
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Permission
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }
}
