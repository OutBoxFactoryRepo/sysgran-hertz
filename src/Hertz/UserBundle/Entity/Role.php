<?php
namespace Hertz\UserBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Role
 *
 * @ORM\Table(name="role")
 * @ORM\Entity(repositoryClass="RoleRepository")
 */
class Role{
	const ORM_ENTITY = "HertzUserBundle:Role";

	public function __construct()
	{
		$this->users = new ArrayCollection();
		$this->assignedPermissions = new ArrayCollection();
		$this->created = new \DateTime();
		$this->modified = new \DateTime();
	}

	/**
	 *
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 *
	 * @var string
	 *
	 * @ORM\Column(type="string", length=100, nullable=false, unique=true)
	 */
	protected $name;

	/**
	 *
	 * @var string
	 *
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	protected $description;

	/**
	 *
	 * @var \DateTime
	 *
	 * @ORM\Column(name="created", type="datetime")
	 */
	private $created;

	/**
	 *
	 * @var \DateTime
	 *
	 * @ORM\Column(name="modified", type="datetime")
	 */
	private $modified;

	/**
	 *
	 * @var ArrayCollection
	 *
	 * @ORM\ManyToMany(targetEntity="User", mappedBy="assignedRoles")
	 */
	protected $users;
        
        /**
	 * @ORM\ManyToMany(targetEntity="Hertz\ReservaBundle\Entity\Modulo", mappedBy="roles", cascade={"persist"})
	 */
	private $idReserva;

	/**
	 *
	 * @var ArrayCollection
	 *
	 * @ORM\ManyToMany(targetEntity="Permission", inversedBy="roles")
	 * @ORM\JoinTable(name="role_permission")
	 */
	private $assignedPermissions;

    /**
     * Add users
     *
     * @param \Hertz\UserBundle\Entity\User $users
     * @return Role
     */
    public function addUser(\Hertz\UserBundle\Entity\User $users)
    {
        $this->users[] = $users;

        return $this;
    }

    /**
     * Remove users
     *
     * @param \Hertz\UserBundle\Entity\User $users
     */
    public function removeUser(\Hertz\UserBundle\Entity\User $users)
    {
        $this->users->removeElement($users);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Add assignedPermissions
     *
     * @param \Hertz\UserBundle\Entity\Permission $assignedPermissions
     * @return Role
     */
    public function addAssignedPermission(\Hertz\UserBundle\Entity\Permission $assignedPermissions)
    {
        $this->assignedPermissions[] = $assignedPermissions;

        return $this;
    }

    /**
     * Remove assignedPermissions
     *
     * @param \Hertz\UserBundle\Entity\Permission $assignedPermissions
     */
    public function removeAssignedPermission(\Hertz\UserBundle\Entity\Permission $assignedPermissions)
    {
        $this->assignedPermissions->removeElement($assignedPermissions);
    }

    /**
     * Get assignedPermissions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAssignedPermissions()
    {
        return $this->assignedPermissions;
    }

    public function toArray(){
		return array(
			'id' => $this->id,
			'name' => $this->name,
			'descripcion' => $this->description,
			'created' => $this->created,
			'modified' => $this->modified,
			'users' => $this->users,
			'assignedPermissions' => $this->assignedPermissions
    	);
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Role
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Role
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Role
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return Role
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Add idReserva
     *
     * @param \Hertz\ReservaBundle\Entity\Modulo $idReserva
     * @return Role
     */
    public function addIdReserva(\Hertz\ReservaBundle\Entity\Modulo $idReserva)
    {
        $this->idReserva[] = $idReserva;

        return $this;
    }

    /**
     * Remove idReserva
     *
     * @param \Hertz\ReservaBundle\Entity\Modulo $idReserva
     */
    public function removeIdReserva(\Hertz\ReservaBundle\Entity\Modulo $idReserva)
    {
        $this->idReserva->removeElement($idReserva);
    }

    /**
     * Get idReserva
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getIdReserva()
    {
        return $this->idReserva;
    }
}
