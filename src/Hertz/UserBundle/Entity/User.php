<?php
namespace Hertz\UserBundle\Entity;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * User
 *
 * @ORM\Table(name="users", indexes={ @ORM\Index(name="username_unique", columns={"username"})}) )
 * @ORM\Entity(repositoryClass="UserRepository")
 * @UniqueEntity(fields={"user"}, message="El usuario existe.")
 * @Gedmo\SoftDeleteable(fieldName="deleted")
 */

class User{
	const ORM_ENTITY = "HertzUserBundle:User";

	public function __construct() {
		$this->assignedRoles = new ArrayCollection();
		$this->created = new \DateTime();
		$this->modified = new \DateTime();
	}

	/**
	 *
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 *
	 * @var string
	 *
	 * @ORM\Column(name="username", type="string", length=150, nullable=false)
	 * @Assert\Regex(pattern="/^[a-z0-9]{1,150}$/i", message="Ingrese letras y numeros entre 1 y 150 caracteres.")
	 */
	private $user;

	/**
	 *
	 * @var string
	 *
	 * @ORM\Column(type="string", length=100, nullable=false)
	 */
	private $password;

	/**
	 *
	 * @var \DateTime
	 *
	 * @ORM\Column(name="created", type="datetime")
	 */
	private $created;

	/**
	 *
	 * @var \DateTime
	 *
	 * @ORM\Column(name="modified", type="datetime")
	 */
	private $modified;
    /**
     *
     * @var \DateTime
     *
     * @ORM\Column(name="deleted", type="datetime", nullable=true)
     */
    private $deleted;
	/**
	 *
	 * @var boolean
	 *
	 * @ORM\Column(name="status", type="boolean", nullable=false)
	 */
	private $status;

    /**
     *
     * @var string
     *
     * @ORM\Column(type="string", length=100, nullable=true)
	 * @Assert\Email(message = "'{{ value }}' no parece ser un email valido", checkMX = false)
     */
    private $email;
   
    /**
     *
     * @var string
     *
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Assert\Length(max="100", maxMessage="No puede superar los {{ limit }} caracteres")
     */
    private $name;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=100, nullable=true)
     * @Assert\Length(max="100", maxMessage="No puede superar los {{ limit }} caracteres")
     */
    private $lastName;

 
	/**
	 *
	 *
	 *@ORM\ManyToOne(targetEntity="Hertz\ReservaBundle\Entity\Agencia",  cascade={"remove"})
	 *@ORM\JoinColumn(name="sucursal", referencedColumnName="id", nullable=false )
	 *@Assert\NotBlank(message="El campo Sucursal es obligatorio.")
	 *@Assert\Type(type="object", message="El valor {{ value }} no es válido debe ser {{ type }}.")
	 *
	 */
	private $sucursal;

    
    /**
     *
     * @var boolean
     *
     * @ORM\Column(name="is_super_admin", type="boolean", nullable=true)
     */
    private $isSuperAdmin = false;

    /**
     *
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Role", inversedBy="users")
     * @ORM\JoinTable(name="user_role")
     */
    private $assignedRoles;

    private $userEntity;

    /**
     * @ORM\ManyToOne(targetEntity="Hertz\ReservaBundle\Entity\Franquiciados",  cascade={"remove"})
     * @ORM\JoinColumn(name="franquiciado", referencedColumnName="id", nullable=true )
     * @Assert\Type(type="object", message="El valor {{ value }} no es válido debe ser {{ type }}.")
     *
     */
    private $franquiciado;

    public function toArray(){
    	return array(
    			'id' => $this->id,
    			'user' => $this->user,
    			'name' => $this->name,
    			'lastName' => $this->lastName,
    			'password' => $this->password,
    			'email' => $this->email,
    			'description' => $this->description,
    			'created' => $this->created,
    			'modified' => $this->modified,
    			'deleted' => $this->deleted,
    			'status' => $this->status,
                'sucursal' => $this->sucursal,
                'franquiciado' => $this->franquiciado,
    			'isSuperAdmin' => $this->isSuperAdmin,    			
    			'assignedRoles' => $this->getRolesName()
    	);
    }
    
    /**
     *
     * @var string
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Assert\Length(max=100, maxMessage="No puede superar los {{ limit }} caracteres.")
     */
    private $description;

    /**
     * Add assignedRoles
     *
     * @param \Hertz\UserBundle\Entity\Role $assignedRoles
     * @return Role
     */
    public function addAssignedRole(\Hertz\UserBundle\Entity\Role $assignedRoles)
    {
        $this->assignedRoles[] = $assignedRoles;

        return $this;
    }

    /**
     * Remove assignedRoles
     *
     * @param \Hertz\UserBundle\Entity\Role $assignedRoles
     */
    public function removeAssignedRole(\Hertz\UserBundle\Entity\Role $assignedRoles)
    {
        $this->assignedRoles->removeElement($assignedRoles);
    }

    /**
     * Get assignedRoles
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAssignedRoles()
    {
        return $this->assignedRoles;
    }

    public function getSalt() {
        return null;
    }
    
    public function getRolesName() {
        $roles = array();

        foreach ($this->getAssignedRoles() as $role) {
            $roles[] = $role->getName();
        }

        return array_unique($roles);
    }

    public function hasRoleName($role_name){
        return in_array($role_name, $this->getRolesName());
    }

    
    public function getAssignedPermissions() {
        $allowedPermissions = array();

        foreach ($this->getAssignedRoles() as $role) {
            $permissions = array();
            foreach ($role->getAssignedPermissions() as $permission) {
                $permissions[] = $permission->getName();
            }
            $allowedPermissions = array_merge($allowedPermissions, $permissions);
        }

        return array_unique($allowedPermissions);
    }

    public function hasPermissionName($permission_name){
        return in_array($permission_name, $this->getAssignedPermissions());
    }

    /**
     * Returns the name to display on screen
     *
     * @return string
     */
    public function getDisplayName() {
        return $this->getName() . ' ' . $this->getLastName();
    }

    //EquatableInterface
    public function isEqualTo(UserInterface $user) {

        if ($user instanceof User) {

            if($user->getUsername() !== $this->getUsername()){
                return false;
            }

            return true;
        }
        return false;
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param string $user
     * @return User
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return string 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return User
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return User
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set deleted
     *
     * @param \DateTime $deleted
     * @return User
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return \DateTime 
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set status
     *
     * @param boolean $status
     * @return User
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string 
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set isSuperAdmin
     *
     * @param boolean $isSuperAdmin
     * @return User
     */
    public function setIsSuperAdmin($isSuperAdmin)
    {
        $this->isSuperAdmin = $isSuperAdmin;

        return $this;
    }

    /**
     * Get isSuperAdmin
     *
     * @return boolean 
     */
    public function getIsSuperAdmin()
    {
        return $this->isSuperAdmin;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return User
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    

    /**
     * Set sucursal
     *
     * @param \Hertz\ReservaBundle\Entity\Agencia $sucursal
     * @return User
     */
    public function setSucursal(\Hertz\ReservaBundle\Entity\Agencia $sucursal)
    {
        $this->sucursal = $sucursal;

        return $this;
    }

    /**
     * Get sucursal
     *
     * @return \Hertz\ReservaBundle\Entity\Agencia 
     */
    public function getSucursal()
    {
        return $this->sucursal;
    }
    /**
     * Set franquiciado
     *
     * @param \Hertz\ReservaBundle\Entity\Franquiciados $franquiciado
     * @return User
     */
    public function setFranquiciado(\Hertz\ReservaBundle\Entity\Franquiciados $franquiciado)
    {
        $this->franquiciado = $franquiciado;

        return $this;
    }

    /**
     * Get franquiciado
     *
     * @return \Hertz\ReservaBundle\Entity\Franquiciados 
     */
    public function getFranquiciado()
    {
        return $this->franquiciado;
    }
}
