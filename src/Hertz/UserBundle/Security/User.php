<?php
namespace Hertz\UserBundle\Security;

use Symfony\Component\Security\Core\User\UserInterface;

use Symfony\Component\Security\Core\User\EquatableInterface;

class User implements UserInterface, EquatableInterface{
	private $username;
	private $password;
	private $salt;
	private $roles;
        private $id;
        private $rolesNombres;
        private $esAdmin;

	public function __construct($username, $password, $salt, array $roles,$id,$rolesNombres,$esAdmin){
		$this->username = $username;
		$this->password = $password;
		$this->salt = $salt;
		$this->roles = $roles;
                $this->id = $id;
                $this->rolesNombres = $rolesNombres;
                $this->esAdmin = $esAdmin;
	}

	public function getRoles(){
		return $this->roles;
	}

	public function getPassword(){
		return $this->password;
	}

	public function getSalt(){
		return $this->salt;
	}

	public function getUsername(){
		return $this->username;
	}

        public function getId(){
		return $this->id;
	}
        
        
        public function getEsAdmin(){
		return $this->esAdmin;
	}
        
        public function getRolesNombres(){
		return $this->rolesNombres;
	}
	
	public function eraseCredentials(){
	}

	public function isEqualTo(UserInterface $user){
		if(!$user instanceof User){
			return false;
		}

		if ($this->password !== $user->getPassword()){
			return false;
		}

		if($this->getSalt() !== $user->getSalt()){
			return false;
		}

		if ($this->username !== $user->getUsername()){
			return false;
		}

                if ($this->id !== $user->getId()){
			return false;
		}
                
                
                if ($this->rolesNombres !== $user->getRolesNombres()){
			return false;
		}
                
                if ($this->esAdmin !== $user->getEsAdmin()){
			return false;
		}
                
		
		return true;
	}
}