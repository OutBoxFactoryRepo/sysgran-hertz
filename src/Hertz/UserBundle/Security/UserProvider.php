<?php
namespace Hertz\UserBundle\Security;

use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;

use Doctrine\ORM\EntityManager;

class UserProvider implements UserProviderInterface
{
    public $type;
     /**
     * @var EntityManager
     */
    protected $em;

    public function __construct(EntityManager $em){
        $this->em = $em;
    }

    public function loadUserByUsername($username){
        $userData = $this->getUser($username);
        
        //print_r($userData);die();
        if ($userData){
            $password = $userData['password'];
            $isSuperAdmin = $userData['is_super_admin'];
            $roles = $isSuperAdmin == 1 ? $this->getAllRole() : $this->getUserRole($username);
            $rolesNombres = $isSuperAdmin == 1 ? $this->getAllSoloRole() : $this->getUserSoloRole($userData['id']);
            $id = $userData['id'];
            
            //print_r($rolesNombres);die();
            array_merge($roles, array('ROLE_USER'));
           
            return new User($username, $password, null, $roles,$id,$rolesNombres,$isSuperAdmin);
        }

        throw new UsernameNotFoundException(sprintf('Username "%s" does not exist.', $username));
    }

    public function refreshUser(UserInterface $user){
        $class = get_class($user);
        if (!$this->supportsClass($class)){
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', $class));
        }
        return $this->loadUserByUsername($user->getUsername());
    }


    public function supportsClass($class){
        return $class === 'Hertz\UserBundle\Security\User';
    }

    protected function getUser($username){
        
        $q = $this
            ->em
            ->getConnection()
            ->prepare("select * from users where status = true and username = :username");
        $q->bindValue('username', $username);
        $q->execute();
        return $q->fetch();
    }

    protected function getAllRole(){
    	$q = $this
    	->em
    	->getConnection()
    	->prepare("select distinct(name) permission from permission");
    	$q->execute();
    	$roles = $q->fetchAll();
    	$userRole = array();
    	if($roles){
    		foreach($roles as $role){
    			$userRole[] = $role['permission'];
    		}
    	}
    	return $userRole;
    }
      
    protected function getUserRole($username){
        $q = $this
             ->em
             ->getConnection()
             ->prepare("select distinct(p.name) permission
        from users u
        inner join user_role ur on ur.user_id = u.id
        inner join role_permission rp on rp.role_id = ur.role_id
        inner join permission p on p.id = rp.permission_id
        where u.username = :user");
        $q->bindValue('user', $username);
        $q->execute();
        $roles = $q->fetchAll();
        $userRole = array();
        if($roles){
            foreach($roles as $role){
                $userRole[] = $role['permission'];
            }
        }
        return $userRole;
    }
    
    
    /*modificación de Osvaldo */
    
    protected function getAllSoloRole(){
    	$q = $this
    	->em
    	->getConnection()
    	->prepare("select distinct(id) id from role");
    	$q->execute();
    	$roles = $q->fetchAll();
    	$userRole = array();
        return $roles;
    	if($roles){
    		foreach($roles as $role){
    			$userRole[] = $role['permission'];
    		}
    	}
    	return $userRole;
    }
    
    
    
    
    
    protected function getUserSoloRole($username){
        $q = $this
             ->em
             ->getConnection()
             ->prepare("select r.id id from role r
            inner join user_role ur
            on r.id = ur.role_id
            where ur.user_id = :user");
        $q->bindValue('user', $username);
        $q->execute();
        $roles = $q->fetchAll();
        return $roles;
        $userRole = array();
        if($roles){
            foreach($roles as $role){
                $userRole[] = $role['permission'];
            }
        }
        return $userRole;
    }
}