<?php
namespace Hertz\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Form\FormError;

use JMS\DiExtraBundle\Annotation as DI;
use JMS\SecurityExtraBundle\Annotation as Security;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Hertz\UserBundle\Exception\ParameterException;
use Hertz\UserBundle\Exception\NotFoundException;
use Hertz\UserBundle\Entity\User;
use Hertz\UserBundle\Form\Type\UserType;
use Hertz\UserBundle\Service\UserService;

/**
 * @Route("/user")
 *
 * @Security\PreAuthorize("hasRole('ROLE_USER_MANAGER')");
 * 
 */
class UserController extends Controller{

	/**
	 *
	 * @var UserService
	 * @DI\Inject("user.manager")
	 */
	private $userSvc;

	/**
	 *
	 * @var Session
	 * @DI\Inject("session")
	 */
	private $session;

	/**
	 *
	 * @var SecurityContext
	 * @DI\Inject("security.context")
	 */
	private $securityContext;

	/**
	 *
	 * @Route("/", name="user_index")
	 * @Template()
	 */
	public function indexAction(){
		$params = array(
			'users' => $this->userSvc->getUser()
		);

        return $params;
    }

    /**
     * @Route("/create", name="user_create")
     *
     * @Security\Secure(roles="ROLE_USER_ADD, ROLE_USER_UPDATE")
     *
     */
    public function createAction(Request $request){
		return $this->forward('HertzUserBundle:User:edit', array('id' => 0));
    }
   
    /**
     * @Route("/{id}/edit", name="user_edit")
     *
     * @Template()
     *
     * @Security\Secure(roles="ROLE_USER_ADD, ROLE_USER_UPDATE") 
     */
    public function editAction(Request $request,  $id){

    	if(intval($id)){
			$user = $this->userSvc->getByIdUser($id);
    	}else{
    		$user = new user();
    	}

        if(!$user){
            $this->session->getFlashBag()->add('error', "El usuario con id {$id} no existe.");
            return $this->redirect($this->generateUrl('user_index'));
        }

		$form = $this->createForm(new UserType($this->userSvc), $user);
		
    	$form->handleRequest($request);

    	$errors = null;

    	try{
			if($form->isSubmitted()){
				if($form->isValid()){
					$data = $request->request->get('user');
					$data['password'] = isset($data['password']['first']) ? trim($data['password']['first']) : null;
					$data['status'] = isset($data['status']) ? true : false;
					$data['isSuperAdmin'] = isset($data['isSuperAdmin']) ? true : false;

					$user = $this->userSvc->createOrUpdate($data, $id);

					$this->session->getFlashBag()->add('success', "Los datos y roles del usuario {$user->getUser()} fueron actualizados correctamente.");

					return $this->redirect($this->generateUrl('user_index'));
				}else{
					$this->session->getFlashBag()->set('error', 'Hay errores en el formulario, verifiquelos e intente nuevamente');
				}
			}
		}catch(ParameterException $e){
			$errors = $e->getError();
		}catch(NotFoundException $e){
			$errors = $e->getError();
		}catch(\Exception $e){
			$errors = array(array($e->getMessage()));
		}

        if($errors){
			$this->session->getFlashBag()->set('error', 'Hay errores en el formulario, verifiquelos e intente nuevamente');
			foreach($errors as $key => $error){
				if($form->has($key)){
					$form->get($key)->addError(new FormError($error[0]));
				}
			}
		}

    	$params = array(
			'user' => $user,
			'form' => $form->createView(),
			'isCreate' => $user->getId() === null,
    	);

    	return $params;
    }

    /**
     * @Route("/{id}/delete", name="user_delete")
     *
     * @Template()
     *
     * @Security\Secure(roles="ROLE_USER_DELETE")
     */
    public function deleteAction($id){
    	try{
            $user = $this->userSvc->getByIdUser($id);

            $user = $this->userSvc->deleteUser($id);

    		$this->session->getFlashBag()->add('success', "El usuario {$id} fue borrado exitosamente.");
    	}
    	catch(\Exception $e){
    		$this->session->getFlashBag()->add('error', "El usuario con id {$id} no existe.");
    	}

    	return $this->redirect($this->generateUrl('user_index'));
    }

    /**
     * @Route("/{id}", name="user_show")
     *
     * @Template()
     *
     * @Security\Secure(roles="ROLE_USER_SHOW")
     */
    public function showAction($id){
    	$user = $this->userSvc->getByIdUser($id);

    	if(!$user){
    		$this->session->getFlashBag()->add('error', "El usuario con id {$id} no existe.");
    		return $this->redirect($this->generateUrl('user_index'));
    	}

    	$params = array(
			'user' => $user,
    	);

    	return $params;
    }

}