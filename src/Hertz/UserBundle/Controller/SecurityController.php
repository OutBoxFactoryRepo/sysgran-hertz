<?php
namespace Hertz\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class SecurityController extends Controller{

    /**
     * @Route("/login", name="login_route")
     */
	public function loginAction(Request $request){
	    $session = $request->getSession();

	    if($request->attributes->has(SecurityContextInterface::AUTHENTICATION_ERROR)){
	        $error = 'Credenciales Incorrectas.';
	    }elseif (null !== $session && $session->has(SecurityContextInterface::AUTHENTICATION_ERROR)){
	        $error = 'Credenciales Incorrectas.';
	        $session->remove(SecurityContextInterface::AUTHENTICATION_ERROR);
	    }else{
			$error = null;
	    }
   
	    $lastUsername = (null === $session) ? '' : $session->get(SecurityContextInterface::LAST_USERNAME);

	    return $this->render(
			'HertzUserBundle:User:login.html.twig',
			array(
				'last_username' => $lastUsername,
				'error' => $error,
	        )
	    );
	}

    /**
     * @Route("/login_check", name="login_check")
     */
    public function loginCheckAction()
    {
		throw new \RuntimeException('You must activate the logout in your security firewall configuration.');
    }

    /**
     * @Route("/logout", name="logout_route")
     */
    public function logoutAction(){
    	throw new \RuntimeException('You must activate the logout in your security firewall configuration.');
    }
}