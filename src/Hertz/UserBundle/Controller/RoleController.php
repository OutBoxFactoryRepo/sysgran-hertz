<?php
namespace Hertz\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Request;

use JMS\DiExtraBundle\Annotation as DI;
use JMS\SecurityExtraBundle\Annotation as Security;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Hertz\UserBundle\Exception\ParameterException;
use Hertz\UserBundle\Exception\NotFoundException;
use Hertz\UserBundle\Form\Type\RoleType;
use Hertz\UserBundle\Entity\Role;

/**
 * @Route("/user/role")
 *
 * @Security\PreAuthorize("hasRole('ROLE_USER_MANAGER')");
 */
class RoleController extends Controller{

	/**
	 * @var UserService
	 * @DI\Inject("user.manager")
	 */
	private $userSvc;

	/**
	 * @var Session
	 * @DI\Inject("session")
	 */
	private $session;

	/**
	 * @Route("/", name="user_role_index")
	 *
	 * @Template()
	 */
	public function indexAction(){
		$params = array(
			'roles' => $this->userSvc->getRole(),
		);
		return $params;
    }

    /**
     * @Route("/create", name="user_role_create")
     *
     * @Security\Secure(roles="ROLE_ROLE_ADD, ROLE_ROLE_UPDATE") 
     */
    public function createAction(){
    	return $this->forward('HertzUserBundle:Role:edit', array('id' => 0));
    }

    /**
     * @Route("/{id}/edit", name="user_role_edit")
     *
     * @Template()
     *
     * @Security\Secure(roles="ROLE_ROLE_ADD, ROLE_ROLE_UPDATE") 
     */
    public function editAction(Request $request,  $id){

    	 if(intval($id)){
    	 	$role = $this->userSvc->getByIdRole($id);
    	 }else{
			$role = new Role();
    	 }

    	 if(!$role){
    	 	$this->session->getFlashBag()->add('error', "El rol con id {$id} no existe.");
			return $this->redirect($this->generateUrl('user_role_index'));
    	 }

    	 $form = $this->createForm(new RoleType(), $role);

    	 $form->handleRequest($request);

    	 if($form->isSubmitted()){
   	 		if($form->isValid()){
   	 			try{
	   	 			$data = $request->request->get('role');

					$role = $this->userSvc->createOrUpdateRole($data, $id);

					if(intval($id)){
	   	 				$this->session->getFlashBag()->add('success', "El rol {$role->getName()} fue actualizado correctamente");
	   	 			}else{
	   	 				$this->session->getFlashBag()->add('success', "Se ha creado el rol {$role->getName()} correctamente");
	   	 			}

					return $this->redirect($this->generateUrl('user_role_show', array('id' => $role->getId())));
   	 			}
   	 			catch(\Exception $e){
   	 				$this->session->getFlashBag()->add('error', "Ya existe un rol con ese nombre.");
   	 			}
   	 		}
    	 }

    	 $params = array(
			'form' => $form->createView(),
			'isCreate' => $role->getId() === null,
    	 );

		return $params;
    }

    /**
     * @route("/{id}/borrar", name="user_role_delete")
     *
     * @Security\Secure(roles="ROLE_ROLE_DELETE")
     */
	public function deleteAction($id){
    	try{
    		$role = $this->userSvc->deleteRole($id);
    		$this->session->getFlashBag()->add('success', "El rol {$id} fue borrado exitosamente.");
    	}catch(\Exception $e){
    		$this->session->getFlashBag()->add('error', "El rol con id {$id} no existe.");
    	}

		return $this->redirect($this->generateUrl('user_role_index'));
    }

    /**
     * @Route("/{id}", name="user_role_show")
     *
     * @Template()
     *
     * @Security\Secure(roles="ROLE_ROLE_SHOW")
     */
    public function showAction($id){
    	$role = $this->userSvc->getByIdRole($id);

    	if(!$role){
    		$this->session->getFlashBag()->add('error', "El role con id {$id} no existe.");
			return $this->redirect($this->generateUrl('user_role_index'));
    	}

    	$params = array(
			'role' => $role,
    	);

    	return $params;
    }
}
