<?php
namespace Hertz\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormError;

use JMS\DiExtraBundle\Annotation as DI;
use JMS\SecurityExtraBundle\Annotation as Security;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Hertz\UserBundle\Exception\ParameterException;
use Hertz\UserBundle\Exception\NotFoundException;
use Hertz\UserBundle\Entity\Permission;
use Hertz\UserBundle\Form\Type\PermissionType;

/**
 * @Route("/user/permission")
 *
 * @Security\PreAuthorize("hasRole('ROLE_USER_MANAGER')");
 */
class PermissionController extends Controller{

	/**
	 *
	 * @var UserService
	 * @DI\Inject("user.manager")
	 */
	private $userSvc;

	/**
	 *
	 * @var Session
	 * @DI\Inject("session")
	 */
	private $session;


	/**
	 * @Route("/", name="user_permission_index")
	 * @Template()
	 */
	public function indexAction(){
		$params = array(
			'permissions' => $this->userSvc->getPermission()
		);
		return $params;
    }

    /**
     * @Route("/create", name="user_permission_create")
     *
     * @Security\Secure(roles="ROLE_PERMISSION_ADD, ROLE_PERMISSION_UPDATE") 
     */
    public function createAction(){
    	return $this->forward('HertzUserBundle:Permission:edit', array('id' => 0));
    }

    /**
     * @Route("/{id}/edit", name="user_permission_edit")
     *
     * @Template()
     *
     * @Security\Secure(roles="ROLE_PERMISSION_ADD, ROLE_PERMISSION_UPDATE") 
     */
    public function editAction(Request $request, $id){

    	 if($id !== 0){
    	 	$permission = $this->userSvc->getByIdPermission($id);
    	 }else{
    	 	$permission = new Permission();
    	 }

    	 if(!$permission){
    	 	$this->session->getFlashBag()->add('error', "El permiso con id {$id} no existe.");
    	 	return $this->redirect($this->generateUrl('user_permission_index'));
    	 }

    	 $form = $this->createForm(new PermissionType($this->userSvc), $permission);

    	 $form->handleRequest($request);

    	 $errors = null;

    	 if($form->isSubmitted()){
   	 		if($form->isValid()){
   	 			try{
	   	 			$data = $form->getData()->toArray();
					$permission = $this->userSvc->createOrupdatePermission($data, $id);

					if($id == 0){
						$message = "Permiso creado correctamente";
					}else{
						$message = "Permiso actualizado correctamente";
					}

					$this->session->getFlashBag()->add('success', $message);

					return $this->redirect($this->generateUrl('user_permission_show', array('id' => $permission->getId())));

				}catch(ParameterException $e){
					$errors = $e->getError();
				}catch(NotFoundException $e){
					$errors = $e->getError();
				}catch(\Exception $e){
					$errors = array(array($e->getMessage()));
				}
			}else{
				$this->session->getFlashBag()->set('error', 'Hay errores en el formulario, verifiquelos e intente nuevamente');
			}

			if($errors){
				$this->session->getFlashBag()->set('error', 'Hay errores en el formulario, verifiquelos e intente nuevamente');
				foreach($errors as $key => $error){
					if($form->has($key)){
						$form->get($key)->addError(new FormError($error[0]));
					}
				}
			}

    	 }

		$params = array(
			'form' => $form->createView(),
			'errors' => $errors,
			'isCreate' => $permission->getId() === null,
		);

    	 return $params;
    }

    /**
     * @route("/{id}/delete", name="user_permission_delete")
     *
     * @Security\Secure(roles="ROLE_PERMISSION_DELETE")
     */
	public function deleteAction($id){
    	try{
    		$permission = $this->userSvc->deletePermission($id);
    		$this->session->getFlashBag()->add('success', "Permiso {$permission->getName()} fue borrado exitosamente.");
		}catch(\Exception $e){
			$this->session->getFlashBag()->add('error', $e->getMessage());
		}

    	return $this->redirect($this->generateUrl('user_permission_index'));
    }

    /**
	 * @Route("/{id}", name="user_permission_show")
	 *
     * @Template()
     *
     * @Security\Secure(roles="ROLE_PERMISSION_SHOW")
     */
    public function showAction($id){
    	$permission = $this->userSvc->getByIdPermission($id);

    	if(!$permission){
    		$this->session->getFlashBag()->add('error', "El permiso con id {$id} no existe.");
    		return $this->redirect($this->generateUrl('user_permision_index'));
    	}

    	$params = array(
			'permission' => $permission,
    	);

		return $params;
	}
}