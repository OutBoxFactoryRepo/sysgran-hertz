<?php
namespace  Hertz\UserBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Console\Helper\TableHelper;
use Symfony\Component\Console\Output\ConsoleOutput;
use Hertz\UserBundle\Entity\User;
use Hertz\UserBundle\Entity\Role;
use Hertz\UserBundle\Entity\Permission;
use Hertz\ReservaBundle\Entity\Agencia;



class LoadRoleData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface{
    
	private $permissions = array(
		'ROLE_PERMISSION_ADD' => 'Permiso para crear permisos',
		'ROLE_PERMISSION_DELETE' => 'Permiso para eliminar permisos',
		'ROLE_PERMISSION_SHOW' => 'Permiso para ver el detalle de un permiso',
		'ROLE_PERMISSION_UPDATE' => 'Permiso para actualizar permisos',
		'ROLE_ROLE_ADD' => 'Permiso para crear roles',
		'ROLE_ROLE_DELETE' => 'Permiso para eliminar un rol',
		'ROLE_ROLE_SHOW' => 'Permiso para el detalle de un rol',
		'ROLE_ROLE_UPDATE' => 'Permiso para actualizar roles',
		'ROLE_USER' => 'Permiso básico',
		'ROLE_USER_ADD' => 'Permiso para crear usuarios',
		'ROLE_USER_DELETE' => 'Permiso para eliminar usuarios',
		'ROLE_USER_MANAGER' => 'Permiso básico para la administración de usuarios',
		'ROLE_USER_SHOW' => 'Permiso para detalle de usuario',
		'ROLE_USER_UPDATE' => 'Permiso para modificar usuarios'
	);

	private $roles = array(
		'Usuario' => array('Permiso básico.', array('ROLE_USER', 'ROLE_USER_MANAGER'))
	);

	private $usuarios = array(
		'admin' => 'Usuario',
		'user' => 'Usuario'
	);

	/**
	* @var ContainerInterface
	*/
	private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null){
		$this->container = $container;
	}

	public function load(ObjectManager $manager){
		$output= new  ConsoleOutput();
		$output->writeln("<info>Permisos...</info>");
		$table_permission = new TableHelper();
		$table_permission->setHeaders(array('Permiso', 'Description'));
		$rows_permissions = array();

		foreach($this->permissions as $name => $description){
			$permission = new Permission();
			$permission->setName($name);
			$permission->setDescription($description);
			$manager->persist($permission);

			$this->addReference($name, $permission);

			$rows_permissions[] = array($name, $description);
		}

		$table_permission->setRows($rows_permissions);
		$table_permission->render($output);

		$output->writeln("<info>Roles...</info>");

		$table_role = new TableHelper();
		$table_role->setHeaders(array('ROLE', 'Description'));
		$rows_roles = array();

		foreach($this->roles as $name => $config){
			$role = new Role();
			$role->setName($name);
			$role->setDescription($config[0]);
		
			foreach($config[1] as $k){
				$role->addAssignedPermission($this->getReference($k));
			}
		
			$manager->persist($role);

			$this->addReference($name, $role);

			$rows_roles[] = array($name, $config[0]);
		}

		$table_role->setRows($rows_roles);
		$table_role->render($output);

		$output->writeln("<info>Usuarios...</info>");

		$table_user = new TableHelper();
		$table_user->setHeaders(array('Usuario', 'Password', 'ROLE'));
		$rows_users = array();
	
                $agencia = new Agencia();//$this->getObjetoAgencia($manager,1);
                $agencia->setDebaja(false);
                $agencia->setDescripcion("Todas");
                $manager->persist($agencia);
		foreach($this->usuarios as $name => $role){
			if($name == 'admin')
			{
				$password = "admin";//$this->password();
			}
			else
			{
				$password = $this->password();
			}
			$pwd = password_hash($password, PASSWORD_BCRYPT, array('cost' => 12));
                        
			$usuario = new User();
			$usuario->setPassword($pwd);
			$usuario->setUser($name);
			$usuario->setSucursal($agencia);
			$usuario->setStatus(1);
			$usuario->setCreated(new \DateTime());
			$usuario->setModified(new \DateTime());
			if($name == 'admin'){
				$usuario->setIsSuperAdmin(true);
			}			
			$usuario->addAssignedRole($this->getReference($role));

			$manager->persist($usuario);

			$rows_users[] = array($name, $password, $role);
		}

		$table_user->setRows($rows_users);
		$table_user->render($output);

		echo "Persistiendo...\n";

		$manager->flush();

		echo "Fin.\n";
	}

	private function password(){
		$char = range('a', 'z');
		shuffle($char);
		$password = substr(implode('', $char), 0, 5);
		return $password;
	}

	/**
	* Get the order of this fixture
	*
	* @return integer
	*/
	public function getOrder()
	{
		return 0; // the order in which fixtures will be loaded
	}
        
        
        private function getObjetoAgencia($manager,$id)
	{
	
		$qb = $manager->getRepository(Agencia::ORM_ENTITY)->getOne($id);
		if(count($qb)>0)
		{
			return $qb[0];
		}
		else
		{
			return null;
		}
	}
}