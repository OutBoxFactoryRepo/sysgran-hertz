<?php
namespace Hertz\UserBundle\Exception;

class BaseException extends \Exception{

	private $error = array();

	public function setError($value, $key=null)
	{
		$key = strlen(trim($key)) > 0 ? $key : '_msg_'.(count($this->error) + 1);
		$this->error[$key][] = $value;
	}

	public function getError()
	{
		return $this->error;
	}

	public function isException()
	{
		return (count($this->error) + count($this->error) > 0 ? true : false);
	}

	public function __toString()
	{
		return json_encode($this->error);
	}
}