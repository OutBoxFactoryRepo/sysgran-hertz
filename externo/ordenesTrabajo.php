<?PHP
    include_once("config.php");
?>
<!DOCTYPE html>
<html>
    <head>
    <!-- The jQuery library is a prerequisite for all jqSuite products -->
    <script type="text/ecmascript" src="js/jquery.min.js"></script> 
    <!-- We support more than 40 localizations -->
    <script type="text/ecmascript" src="js/trirand/i18n/grid.locale-es.js"></script>
    <!-- This is the Javascript file of jqGrid -->   
    <script type="text/ecmascript" src="js/trirand/jquery.jqGrid.min.js"></script>
    <!-- This is the localization file of the grid controlling messages, labels, etc.
    <!-- A link to a jQuery UI ThemeRoller theme, more than 22 built-in and many more custom -->
    <link rel="stylesheet" type="text/css" media="screen" href="css/jquery-ui.css" />
    <!-- The link to the CSS that the grid needs -->
    <link rel="stylesheet" type="text/css" media="screen" href="css/trirand/ui.jqgrid.css" />
    

    <script type="text/ecmascript" src="js/jquery.json-2.2.min.js"></script>

    </head>
    <body>
        
    <style type="text/css">

        /* set the size of the datepicker search control for Order Date*/
        #ui-datepicker-div { font-size:11px; }
        
        /* set the size of the autocomplete search control*/
        .ui-menu-item {
            font-size: 11px;
        }

        .ui-autocomplete { font-size: 11px; position: absolute; cursor: default;z-index:5000 !important;}      
        
        
        
    </style>
        
    <table id="jqGrid" align="center"></table>
    <div id="jqGridPager"></div>

    <script type="text/javascript">
        
        
        function cargarEstadoOrdenes()
        {
            var valoresx = "";
            $.ajax({
                                data: {},
                                type: "POST",
                                dataType: "json",
                                url: "<?=$homesite;?>/web/app_dev.php/estadosot/search/",
                                async:false,
                                success: function(data)
                                {


                                                        var arrayValores = data.rows;

                                                        valoresx ="{";
                                                        $.each( arrayValores, function ( userkey, uservalue) 
                                                        {
                                                            if(valoresx == "{")
                                                            {
                                                                //console.log(userkey + ': ' + uservalue.id + ': ' + uservalue.descripcion );
                                                                valoresx += "\"" + uservalue.id + "\":\"" + uservalue.descripcion+"\"";
                                                            }
                                                            else
                                                            {
                                                                valoresx += "," + "\"" + uservalue.id + "\":\"" + uservalue.descripcion+"\"";
                                                            }
                                                        });
                                                        valoresx +="}";
                                                        //valores = data;
                                                        //$.parseJSON(valores);
                
                                                        return valoresx;
                                },
                                error: function(xhr, textStatus, errorThrown) {
                                    // Handle error
                                    alert(errorThrown);
                                    //$.unblockUI(); 
                                  }
                        });
              return valoresx;
       }
       
   
        function cargarTalleres()
        {
            var valoresx = "";
            $.ajax({
                                data: {},
                                type: "POST",
                                dataType: "json",
                                url: "<?=$homesite;?>/web/app_dev.php/taller/search/",
                                async:false,
                                success: function(data)
                                {


                                                        var arrayValores = data.rows;

                                                        valoresx ="{";
                                                        $.each( arrayValores, function ( userkey, uservalue) 
                                                        {
                                                            if(valoresx == "{")
                                                            {
                                                                //console.log(userkey + ': ' + uservalue.id + ': ' + uservalue.descripcion );
                                                                valoresx += "\"" + uservalue.id + "\":\"" + uservalue.descripcion+"\"";
                                                            }
                                                            else
                                                            {
                                                                valoresx += "," + "\"" + uservalue.id + "\":\"" + uservalue.descripcion+"\"";
                                                            }
                                                        });
                                                        valoresx +="}";
                                                        //valores = data;
                                                        //$.parseJSON(valores);
                
                                                        return valoresx;
                                },
                                error: function(xhr, textStatus, errorThrown) {
                                    // Handle error
                                    alert(errorThrown);
                                    //$.unblockUI(); 
                                  }
                        });
              return valoresx;
       }
       
       function cargarTipoComprobantes()
        {
            var valoresx = "";
            $.ajax({
                                data: {},
                                type: "POST",
                                dataType: "json",
                                url: "<?=$homesite;?>/web/app_dev.php/tipocomprobante/search/",
                                async:false,
                                success: function(data)
                                {


                                                        var arrayValores = data;

                                                        valoresx ="{\"\":\"[Seleccione]\"";
                                                        $.each( arrayValores, function ( userkey, uservalue) 
                                                        {
                                                            
                                                            
                                                                valoresx += "," + "\"" + uservalue.id + "\":\"" + uservalue.descripcion+"\"";
                                                            
                                                        });
                                                        valoresx +="}";
                                                        //valores = data;
                                                        //$.parseJSON(valores);
                                                        return valoresx;
                                },
                                error: function(xhr, textStatus, errorThrown) {
                                    // Handle error
                                    alert(errorThrown);
                                    //$.unblockUI(); 
                                  }
                        });
              return valoresx;
       }
       
        var estadosOt = JSON.parse(cargarEstadoOrdenes());
        var talleres = JSON.parse(cargarTalleres());
        var tipoComprobantes = JSON.parse(cargarTipoComprobantes());
        
        
        var debaja = {false:"No",true:"Si"};
        
    //envía a la subgrilla un valor que no es el Id por defecto, en este caso el id del taaller
    function valorTaller(id)
    {
        var rowData = jQuery('#jqGrid').jqGrid ('getRowData', id);

        //console.log(rowData["taller.id"]);
        //console.log(id);
        return rowData["taller.id"];
    }
    
    function valorTallerExterno(id)
    {
        var rowData = jQuery('#jqGrid').jqGrid ('getRowData', id);

        //console.log(rowData["taller.id"]);
        //console.log(id);
        return rowData["taller.externo"];
    }

    
    $(document).ready(function () {

              function siNoFmatter (cellvalue, options, rowObject)
              {
                   if(cellvalue)
                   {
                       return "Si";
                   }
                   else
                   {
                       return "No";
                   }
              }

              function noSiFmatter (cellvalue, options, rowObject)
              {
                   if(cellvalue)
                   {
                       return "Si";
                   }
                   else
                   {
                       return "No";
                   }
              }

      function esAdmin()
        {
          var valoresx = "";
          $.ajax({
            data: {},
            type: "POST",
            dataType: "json",
            url: "<?=$homesite;?>/web/app_dev.php/contrato/esadmin/",
            async:false,
            success: function(data)
            {
              valoresx = data.admin;
              return valoresx;
            },
            error: function(xhr, textStatus, errorThrown) 
            {
              alert(errorThrown);
            }
          });
          return valoresx;
        }

              var admin = esAdmin();
              //alert(admin);
              //admin = admin === 'true';

              //console.log(typeof admin, admin);

              $("#jqGrid").jqGrid({
              url: '<?=$homesite;?>/web/app_dev.php/ordentrabajo/searchfromtaller/',
              editurl: '<?=$homesite;?>/web/app_dev.php/ordentrabajo/crear/',
              datatype: "json",
              sortname: 'id',
              sortorder: 'asc',
              mtype: 'POST', 
              gridview:false,
              onSelectRow: function(id)
               { 
                   var ret = $("#jqGrid").jqGrid('getRowData',id);
                   //alert(ret.toSource());
                   //alert(ret["taller.externo"]);
                   if (ret["taller.externo"] == "No")
                   {
                        $("#jqGrid").setColProp('total',{editable:false});
                   }
                   else
                   {  
                       $("#jqGrid").setColProp('total',{editable:true});
                   }
             },
            //comienzo sub grilla  
               "subGridOptions": {
                "plusicon": "ui-icon-triangle-1-e",
                "minusicon": "ui-icon-triangle-1-s",
                "openicon": "ui-icon-arrowreturn-1-e"
            },
             "subGrid": true,
            "subGridRowExpanded": function(subgridid, id) {
            var data = {
                subgrid: subgridid,
                rowid: id,
                taller:valorTaller(id),
                tallerexterno:valorTallerExterno(id)
            };

            $("#" + jQuery.jgrid.jqID(subgridid)).load('subGridOt.php', data);
            }//fin de la subgrilla
        ,
            
              afterInsertRow: function(rowid, aData, rowelem) 
              {
                    
                    //condición para mostrar o no la flecha o flechita
                    /*if(rowelem.taller.externo == true){
                        $('tr#'+rowid, $(this))
                         .children("td.sgcollapsed")
                         .html("")
                         .removeClass('ui-sgcollapsed sgcollapsed');
                    }*/
                },  
              viewrecords: true,
               ondblClickRow: function(rowid) {
                   jQuery(this).jqGrid('editGridRow', rowid,
                   {
                    recreateForm:true
                    ,closeAfterEdit:true,
                    closeOnEscape:true
                    ,reloadAfterSubmit:true
                    ,"afterSubmit" : function( data, postdata, oper) 
                    {
                        var response = data.responseJSON;
                        if (response.hasOwnProperty("violations")) {
                                if(response.violations.length) 
                                {
                                        return [false,response.violations[0].message];
                                }
                        }
                        
                        if (response.hasOwnProperty("error")) {
                                if(response.error.length) 
                                {
                                        return [false,response.error];
                                }
                        }
                        return [true,"",""];
                    },
                    errorTextFormat: function (data) {
                        return 'Error: ' + data.responseText
                    }
                   });
               },
               colModel: [
                { 
                  label: 'Id'
                  , name: 'id'
                  , index: 'id'
                  , width: 75 
                  , search: false
                  , hidden: false
                  , sorttype: 'number'
                  ,key: true
                },
                { 
                  label: 'Descripción'
                  , name: 'descripcion'
                  , index: 'descripcion'
                  , width: 255 
                  ,editable: true
                  ,editrules : { required: true}
                  ,searchoptions: 
                              {
                               sopt: ['cn']
                              }
                },
                { 
                  label: 'Tareas'
                  , name: 'tareas'
                  , index: 'tareas'
                  , width: 255 
                  ,hidden:true
                  ,editable: false
                  ,edittype: 'textarea'
                  ,editoptions: { cols: 50}
                  ,editrules : { required: false}
                  ,searchoptions: 
                              {
                               sopt: ['cn']
                              }
                },
                { 
                  label: 'Estado'
                  , name: 'estado.descripcion'
                  , index: 'estado.descripcion'
                  , width: 100 
                  ,editable: admin
                  ,edittype: 'select'
                  ,stype:'select'
                  ,editoptions: 
                              {
                                 value:estadosOt
                              }
                  ,searchoptions: 
                              {
                                 value:estadosOt
                                 ,sopt: ['eq','ne']
                              }
                  ,editrules : { required: true}
                },
                { 
                  label: 'taller.id'
                  , name: 'taller.id'
                  , index: 'taller.id'
                  , width: 150 
                  ,editable: true
                  ,hidden:true
                  ,search:false
                  ,editrules : { required: false,  edithidden: false}
                }
                ,{ 
                  label: 'Taller'
                  , name: 'taller.descripcion'
                  , index: 'taller.descripcion'
                  , width: 100 
                  ,editable: true
                  ,edittype: 'select'
                  ,stype:'select'
                  ,editoptions: 
                              {
                                 value:talleres
                              }
                  ,searchoptions: 
                              {
                                 value:talleres
                                 ,sopt: ['eq','ne']
                              }
                  ,editrules : { required: true}
                },
                { 
                  label: 'Externo'
                  , name: 'taller.externo'
                  , index: 'taller.externo'
                  , width: 50 
                  ,editable: false
                  ,edittype: 'select'
                  ,hidden:false
                  ,stype: 'select'
                  ,formatter:siNoFmatter
                  ,editoptions: { value: debaja , defaultValue:"false"}
                  ,searchoptions: { value: debaja ,sopt: ['eq','ne']}
                  
                }
                ,
                { 
                  label: 'idvehiculo'
                  , name: 'vehiculo.id'
                  , index: 'vehiculo.id'
                  , width: 150 
                  ,editable: true
                  ,hidden:true
                  ,search:false
                  ,editrules : { required: true,  edithidden: false}
                }
                ,
                 { 
                  label: 'Dominio'
                  , name: 'vehiculo.dominio'
                  , index: 'dominio'
                  , width: 100 
                  ,editable: true
                  ,searchoptions: 
                              {
                                sopt: ['cn']
                              }
                  ,"editoptions": 
                    {
                        "dataInit": 

                           function(el) {
                            setTimeout(function() 
                            {
                                if (jQuery.ui) 
                                {
                                    if (jQuery.ui.autocomplete) 
                                    {
                                        jQuery(el).autocomplete({
                                            "appendTo": "body",
                                            "disabled": false,
                                            "delay": 300,
                                            "minLength": 3,
                                            select: function( event , ui ) 
                                            {
                                                    document.getElementById("vehiculo.id").value = ui.item.vehiculoId;
                                            },
                                            "source": function(request, response) {

                                                $.ajax({
                                                    url: "<?=$homesite;?>/web/app_dev.php/vehiculo/searchbysinfecha/",
                                                    dataType: "json",
                                                    data: request,
                                                    type: "GET",
                                                    error: function(res, status) {
                                                        alert(res.status + " : " + res.statusText + ". Status: " + status);
                                                    },
                                                    success: function(data) 
                                                    {

                                                        //si no devuelve datos setea el campo oculto del id del vehículo en ""
                                                        if(data.length < 1)
                                                        {                                                            
                                                            document.getElementById("vehiculo.id").value = "";
                                                        }
                                                        response(data);
                                                    }
                                                });
                                            }
                                        });
                                        jQuery(el).autocomplete('widget').css('font-size', '11px');
                                        jQuery(el).autocomplete('widget').css('z-index', '1000');
                                    }
                                }
                            }, 200);
                        }
                    }
                  ,editrules : { required: true}
                }
                ,
                { 
                  label: 'Tipo CTE'
                  , name: 'tipo_comprobante.descripcion'
                  , index: 'tipo_comprobante.descripcion'
                  , width: 50 
                  ,editable: true
                  ,hidden:false
                  ,edittype: 'select'
                  ,stype: 'select'
                  ,editoptions: { value: tipoComprobantes }
                  ,searchoptions: 
                              {
                                 value: tipoComprobantes
                                 ,sopt: ['eq','ne']
                              }
                  ,editrules : { required: false,edithidden: true}
                  
                },
                
                { 
                    label: 'Total'
                    , name: 'total'
                    , index: 'total'
                    , width: 50 
                    , editable: false
                    , sortype:'int'
                    , editrules : { required: false, number:true }
                    , searchoptions: 
                    {
                     sopt : ['eq','ne', 'lt', 'gt']
                    }
                    ,searchrules : { number:true }
                },
                
                { 
                    label: 'Nro. CTE'
                    , name: 'nrofactura'
                    , index: 'nrofactura'
                    , width: 50 
                    , editable: true
                    , sortype:'int'
                    , editrules : { required: false, number:true }
                    , searchoptions: 
                    {
                     sopt : ['eq','ne', 'lt', 'gt']
                    }
                    ,searchrules : { number:true }
                },
                { 
                  label: 'De Baja'
                  , name: 'debaja'
                  , index: 'debaja'
                  , width: 100 
                  ,editable: true
                  ,hidden:false
                  ,edittype: 'select'
                  ,stype: 'select'
                  ,formatter:siNoFmatter
                  ,editoptions: { value: debaja , defaultValue:"false"}
                  ,searchoptions: { value: debaja ,sopt: ['eq','ne']}
                  ,editrules : { required: true,edithidden: true}
                  
                }
                // sorttype is used only if the data is loaded locally or loadonce is set to true
                //{ label: 'Quantity', name: 'Quantity', width: 80, sorttype: 'number' }                   
              ],
              width: 'auto',
              height:'auto',
              page:1,
              rowNum: 10,
              rowList: [10, 50, 100],
              loadonce: false, // this is just for the demo
              pager: "#jqGridPager"
            });

              $('#jqGrid').navGrid('#jqGridPager',
                // the buttons to appear on the toolbar of the grid
                //Botonera de la grilla
                { 
                  edit: true
                  , add: true //Boton de agregar
                  , del: false //Boton de eliminar
                  , search: true
                  , refresh: true
                  , view: false //Boton de ver datos
                  , position: "left"
                  , cloneToTop: false 
                },
                // options for the Edit Dialog
                {
                    editCaption: "Editar &Oacute;rden Trabajo",
                    recreateForm: true,
                    closeAfterEdit: true,
                    closeOnEscape:true
                    ,"afterSubmit" : function( data, postdata, oper) 
                    {
                        var response = data.responseJSON;
                        if (response.hasOwnProperty("violations")) {
                                if(response.violations.length) 
                                {
                                        return [false,response.violations[0].message];
                                }
                        }
                        
                        if (response.hasOwnProperty("error")) {
                                if(response.error.length) 
                                {
                                        return [false,response.error];
                                }
                        }
                        return [true,"",""];
                    },
                    errorTextFormat: function (data) {
                        return 'Error: ' + data.responseText
                    }
                },
                // options for the Add Dialog
                {
                    addCaption: "Crear &Oacute;rden Trabajo",
                    closeAfterAdd: true,
                    recreateForm: true,
                    closeOnEscape:true,
                    "afterSubmit" : function( data, postdata, oper) 
                    {
                        var response = data.responseJSON;
                        if (response.hasOwnProperty("violations")) {
                                if(response.violations.length) 
                                {
                                        return [false,response.violations[0].message];
                                }
                        }
                        
                        if (response.hasOwnProperty("error")) {
                                if(response.error.length) 
                                {
                                        return [false,response.error];
                                }
                        }
                        return [true,"",""];
                    },
                    errorTextFormat: function (data) {
                        return 'Error: ' + data.responseText
                    }
                }
                
                );

                $('#jqGrid').jqGrid('setGridHeight',($(window).innerHeight()-80));
                $('#jqGrid').jqGrid('setGridWidth',$(window).width()-20);


           

});

    /*
     var height = $(window).height();
            $('.ui-jqgrid-bdiv').height(height-100);
            
            var alto = $(window).width();
            $('.ui-jqgrid-bdiv').width(alto);
            alert(alto);
            
            $("#jqGrid").jqGrid({with:100000});
            */
           
           $(window).resize(function(){
    $('#jqGrid').jqGrid('setGridHeight',($(window).innerHeight()-80));
    $('#jqGrid').jqGrid('setGridWidth',($(window).width()-20));
    


    
});
</script>
<script type="text/ecmascript" src="js/jquery-ui.min.js"></script>
<script>
        $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '<Ant',
        nextText: 'Sig>',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
        dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
        };
        $.datepicker.setDefaults($.datepicker.regional['es']);
</script>
    </body>
</html>