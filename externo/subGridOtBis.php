<script>

       function cargarReparaciones(idOt)
       {
            var valoresx = "";
            $.ajax({
                                data: {idOt:idOt},
                                type: "POST",
                                dataType: "json",
                                url: "<?=$homesite;?>/web/app_dev.php/otreparaciones/searchcombo/",
                                async:false,
                                success: function(data)
                                {

                                                       var arrayValores = data;

                                                        valoresx ="{\"\":\"[Seleccione]\"";
                                                        $.each( arrayValores, function ( userkey, uservalue) 
                                                        {
                                                            if(valoresx == "{")
                                                            {
                                                                //console.log(userkey + ': ' + uservalue.id + ': ' + uservalue.descripcion );
                                                                valoresx += "\"" + uservalue.id + "\":\"" + uservalue.descripcion+"\"";
                                                            }
                                                            else
                                                            {
                                                                valoresx += "," + "\"" + uservalue.id + "\":\"" + uservalue.descripcion+"\"";
                                                            }
                                                        });
                                                        valoresx +="}";
                                                        //valores = data;
                                                        //$.parseJSON(valores);
                                                        //alert(valoresx);
                                                        //valoresx = "{\"\":\"[Seleccione]\",\"2\":\"Aceite Caja de cambio\",\"1\":\"Filtros nafta \"}";
                                                        return valoresx;
                                },
                                error: function(xhr, textStatus, errorThrown) {
                                    // Handle error
                                    alert(errorThrown);
                                    //$.unblockUI(); 
                                  }
                        });
              return valoresx;
       }
       

        
        
        
</script>

<h1>Reparaciones</h1>
<table id='<?=$subgrid;?>_t_3'></table>
<div id='<?=$subgrid;?>_t_p_3'></div>

<script type='text/javascript'>
 jQuery(document).ready(function($) {
     
        var reparaciones = JSON.parse(cargarReparaciones(<?=$rowId;?>));
        
        jQuery('#<?=$subgrid;?>_t_3').jqGrid({
            "hoverrows": false,
            "viewrecords": true,
            pginput: false,
            pgbuttons: false,
           
            //comienzo sub grilla  
               "subGridOptions": {
                "plusicon": "ui-icon-triangle-1-e",
                "minusicon": "ui-icon-triangle-1-s",
                "openicon": "ui-icon-arrowreturn-1-e"
            },
             "subGrid": true,
            "subGridRowExpanded": function(subgridid, id) {
                
             //alert($(this).jqGrid('getCell',id,'reparacion.id'));   
            var repId = $(this).jqGrid('getCell',id,'reparacion.id');
            var data = {
                subgrid: subgridid,
                rowid: repId,
                reparacion_id:repId,
                idOt:<?=$rowId;?>,
                idx:id
            };

            $("#" + jQuery.jgrid.jqID(subgridid)).load('subGridOtRepTareas.php', data);
            }//fin de la subgrilla
            ,
            "jsonReader": {
                "repeatitems": false,
                "subgrid": {
                    "repeatitems": false
                }
            },
            "xmlReader": {
                "repeatitems": false,
                "subgrid": {
                    "repeatitems": false
                }
            },
            
              gridview:false,
            "url": "<?=$homesite;?>/web/app_dev.php/otreparaciones/search/",
            "editurl": "<?=$homesite;?>/web/app_dev.php/otreparaciones/crear/?idOt=<?=$rowId;?>",
            "cellurl": "subReserva.php",
            "width": 540,
            "rowNum": 10,
            "sortname": "id",
            "height": 250,
            "postData": {
                "subgrid": "<?=$subgrid;?>",
                "rowid": "<?=$rowId;?>",
                "ot": "<?=$rowId;?>",
                "taller": "<?=$taller;?>",
                "oper": "grid"
            },
            "datatype": "json",
            
            ondblClickRow: function(rowIdCliOd) {
                   return false;
               },
            
            "colModel": [{
                "label": "Id",
                "name": "id",
                "index": "id",
                "sorttype": "int",
                "key": true,
                width: 75,
                hidden:false,
                "editable": false
            }, {
                "label": "Descripci&oacute;n",
                "name": "reparacion.descripcion",
                "index": "descripcion",
                "sorttype": "string",
                "editable": false
            },
               { 
                  label: 'Reparación'
                  , name: 'reparacion.id'
                  , index: 'reparacion.id'
                  , width: 100
                  , hidden:true
                  ,editable: true
                  ,edittype: 'select'
                  ,stype:'select'
                  ,editoptions: 
                              {
                                 value:reparaciones
                              }
                  ,searchoptions: 
                              {
                                 value:reparaciones
                                 ,sopt: ['eq','ne']
                              }
                  ,editrules : { required: true,edithidden: true}
                }],
            "prmNames": {
                "page": "page",
                "rows": "rows",
                "sort": "sidx",
                "order": "sord",
                "search": "_search",
                "nd": "nd",
                "id": "id",
                "filter": "filters",
                "searchField": "searchField",
                "searchOper": "searchOper",
                "searchString": "searchString",
                "oper": "oper",
                "query": "grid",
                "addoper": "add",
                "editoper": "edit",
                "deloper": "del",
                "excel": "excel",
                "subgrid": "subgrid",
                "totalrows": "totalrows",
                "autocomplete": "autocmpl"
            },
            "loadError": function(xhr, status, err) {
                try {
                    jQuery.jgrid.info_dialog(jQuery.jgrid.errors.errcap, '<div class="ui-state-error">' + xhr.responseText + '</div>', jQuery.jgrid.edit.bClose, {
                        buttonalign: 'right'
                    });
                } catch (e) {
                    alert(xhr.responseText);
                }
            },
            "pager": "#<?=$subgrid;?>_t_p_3"
        });
        jQuery('#<?=$subgrid;?>_t_3').jqGrid('navGrid', '#<?=$subgrid;?>_t_p_3', {
            "edit": false,
            "add": true,
            "del": true,
            "search": false,
            "refresh": true,
            "view": false,
            "excel": true,
            "pdf": false,
            "csv": false,
            "columns": false
        }, 
        //edit option
        {
            "drag": true,
            "resize": true,
            "closeOnEscape": true,
            "dataheight": 150,
            top:10,
            "closeAfterEdit":true,
            "afterSubmit" : function( data, postdata, oper) {
                    var response = data.responseJSON;
                        if (response.hasOwnProperty("violations")) {
                                if(response.violations.length) 
                                {
                                        return [false,response.violations[0].message];
                                }
                        }
                        
                        if (response.hasOwnProperty("error")) {
                                if(response.error.length) 
                                {
                                        return [false,response.error];
                                }
                        }
                        
                        $("#jqGrid").toggleSubGridRow(<?=$rowId;?>);

                        $("#jqGrid").expandSubGridRow(<?=$rowId;?>);
                        
                        return [true,"",""];

            },
            "errorTextFormat": function (data) {
                return 'Error: ' + data.responseText
            },
            "beforeShowForm": function(form) 
            { 
                //$('#tr_nombre', form).hide(); 
                //$('#tr_apellido', form).hide(); 
                //$('#tr_fecha_nac', form).hide(); 
                //$("#<?=$subgrid;?>_t").jqGrid('setColProp', 'ot.id', {editrules: {required: false}});
            }
            
                   
        }, 
        //create option
        {
            "drag": true,
            "resize": true,
            "closeOnEscape": true,
            "dataheight": 150,
            top:10,
            "closeAfterAdd": true,
            "afterSubmit" : function( data, postdata, oper) {
                    var response = data.responseJSON;
                    
                        
                        
                        if (response.hasOwnProperty("error")) 
                        {
                            if (response.error.hasOwnProperty("violations")) 
                            {
                                if(response.error.violations.length) 
                                {
                                        return [false,response.error.violations[0].message];
                                }
                            }
                                if(response.error.length) 
                                {
                                        return [false,response.error];
                                }
                        }
                        
                        $("#jqGrid").toggleSubGridRow(<?=$rowId;?>);

                        $("#jqGrid").expandSubGridRow(<?=$rowId;?>);
                        
                        return [true,"",""];

            },
            "errorTextFormat": function (data) {
                return 'Error: ' + data.responseText
            }
        }
         ,
        // options for the Delete Dailog
        {
            url: "<?=$homesite;?>/web/app_dev.php/otreparaciones/crear/?idOt=<?=$rowId;?>",
            datatype: "json",
            top:10,
            "afterSubmit" : function( data, postdata, oper) {
                  var response = data.responseJSON;
                        if (response.hasOwnProperty("violations")) {
                                if(response.violations.length) 
                                {
                                        return [false,response.violations[0].message];
                                }
                        }
                        
                        if (response.hasOwnProperty("error")) {
                                if(response.error.length) 
                                {
                                        return [false,response.error];
                                }
                        }
                       
                        $("#jqGrid").toggleSubGridRow(<?=$rowId;?>);

                        $("#jqGrid").expandSubGridRow(<?=$rowId;?>);
                        
                        return [true,"",""];
            },
            "errorTextFormat": function (data) {
                return 'Error: ' + data.responseText
            }
        }, {
            "drag": true,
            "closeAfterSearch": true,
            "multipleSearch": true
        }, {
            "drag": true,
            "resize": true,
            "closeOnEscape": true,
            "dataheight": 150
        });
    });
    
    
    
    $('#<?=$subgrid;?>_t_3').jqGrid('setGridWidth',$(window).width()-80);

    
    $(window).resize(function()
           {
            $('#<?=$subgrid;?>_t_3').jqGrid('setGridWidth',$(window).width()-80);
     });
</script>