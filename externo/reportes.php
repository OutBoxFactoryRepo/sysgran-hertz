<?PHP
include_once("config.php");
    
    //echo "<pre>";
    //print_r($_POST);
    //echo "</pre>";
    /*
    $objeto = curl_init("http://local.hertz/web/app_dev.php/contrato/crear/");
    
    //curl_setopt($objeto,CURLOPT_RETURNTRANSFER,1);
    
    curl_setopt($objeto, CURLOPT_HTTPHEADER, array('Content-Type: application/xml', ""));
    curl_setopt($objeto, CURLOPT_HEADER, 1);
    curl_setopt($objeto, CURLOPT_USERPWD, "admin" . ":" . "admin");
    curl_setopt($objeto, CURLOPT_TIMEOUT, 30);
    curl_setopt($objeto, CURLOPT_POST, 1);
    curl_setopt($objeto, CURLOPT_RETURNTRANSFER, TRUE);
    $respuesta = curl_exec($objeto);
    
    echo $respuesta;
    die();*/
?>
<!DOCTYPE html>
<html>
    <head>
    <!-- The jQuery library is a prerequisite for all jqSuite products -->
    <script type="text/ecmascript" src="js/jquery.min.js"></script> 
    <!-- We support more than 40 localizations -->
    <script type="text/ecmascript" src="js/trirand/i18n/grid.locale-es.js"></script>
    <!-- This is the Javascript file of jqGrid -->   
    <script type="text/ecmascript" src="js/trirand/jquery.jqGrid.min.js"></script>
    <!-- This is the localization file of the grid controlling messages, labels, etc.
    <!-- A link to a jQuery UI ThemeRoller theme, more than 22 built-in and many more custom -->
    <link rel="stylesheet" type="text/css" media="screen" href="css/jquery-ui.css" />
    <!-- The link to the CSS that the grid needs -->
    <link rel="stylesheet" type="text/css" media="screen" href="css/trirand/ui.jqgrid.css" />
    

    <script type="text/ecmascript" src="js/jquery.json-2.2.min.js"></script>
    <script type="text/ecmascript" src="js/jquery.mask.js"></script>
    <script type="text/ecmascript" src="js/jquery.blockUI.js"></script>
    </head>
    <body >
   <style type="text/css">

        /* set the size of the datepicker search control for Order Date*/
        #ui-datepicker-div { font-size:11px; }
        
        /* set the size of the autocomplete search control*/
        .ui-menu-item {
            font-size: 11px;
        }

        .ui-autocomplete { font-size: 11px; position: absolute; cursor: default;z-index:5000 !important;}      
        .my-select-loading {
            background: white url('ui-anim_basic_16x16.gif') right center no-repeat;
            height: 16px;
            width: 16px;
            margin-left: .5em;
            display: inline-block;
        }
        
        
    </style>     
    <script type="text/javascript">
        function getExcellVehiculos()
        {
            var valoresx = "";
            var anioFil = $("#anio").val();
            var mesFil = $("#mes").val();
            valoresx = $.ajax({
                                data: {anio:anioFil,mes:mesFil},
                                type: "POST",
                                dataType: "json",
                                url: "<?=$homesite;?>/web/app_dev.php/vehiculo/getexcell/",
                                async:false,
                                success: function(data)
                                {
                                        //alert(data.objeto);
                                        return data.objeto;
                                },
                                error: function(xhr, textStatus, errorThrown) {
                                    // Handle error
                                    alert(errorThrown);
                                    //$.unblockUI(); 
                                  }
                        });

              respuesta = JSON.parse(valoresx.responseText);
              
              $("#html").val(respuesta.objeto);
              $('#reporte').attr('action', 'excell.php?id=2');
              $('#reporte').submit();
              return respuesta.objeto;
              //excell.php?id=1
       }
        
    </script>

    <fieldset>
        <legend>Reporte de Vehículos:</legend>
        <table width="98%" border="0" class="ui-jqgrid table-bordered">
            
            <tr>
                    <td width>
                        <label for="mes">Mes</label>
                          <select id="mes" name="mes" width="90%">
                            <?PHP
                            for($a = 1;$a <= 12;$a++)
                            {
                            ?>
                                <option value="<?=$a;?>"><?=$a;?></option>
                            <?PHP
                            }
                            ?>
                        </select>
                    </td>
            </tr>
            
            <tr>
                    <td>
                        <label for="mes">Año</label>
                        <select id="anio" name="anio" width="90%">
                            <?PHP
                            
                            $anioActual = date("Y");
                            $anioIncial = 2016;
                            for($a = $anioIncial;$a <= $anioActual;$a++)
                            {
                            ?>
                                <option value="<?=$a;?>"><?=$a;?></option>
                            <?PHP
                            }
                            ?>
                        </select>
                    </td>
            </tr>
            <tr>
                    <td>
                        
                        <input type="button" id="vehiculos" name="vehiculos" value="Generar Reporte" onclick="javascript:getExcellVehiculos();"/>
                        
                    </td>
            </tr>
        </table>
    </fieldset>
        
        <form id="reporte" name="reporte" target="_blank" method="POST">
            <input type="hidden" id="html" name="html" value=""/>
        </form>
        </form>
    </body>
</html>
