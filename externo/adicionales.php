<?PHP
    include_once("config.php");
?>
<!DOCTYPE html>
<html>
    <head>
    <!-- The jQuery library is a prerequisite for all jqSuite products -->
    <script type="text/ecmascript" src="js/jquery.min.js"></script> 
    <!-- We support more than 40 localizations -->
    <script type="text/ecmascript" src="js/trirand/i18n/grid.locale-es.js"></script>
    <!-- This is the Javascript file of jqGrid -->   
    <script type="text/ecmascript" src="js/trirand/jquery.jqGrid.min.js"></script>
    <!-- This is the localization file of the grid controlling messages, labels, etc.
    <!-- A link to a jQuery UI ThemeRoller theme, more than 22 built-in and many more custom -->
    <link rel="stylesheet" type="text/css" media="screen" href="css/jquery-ui.css" />
    <!-- The link to the CSS that the grid needs -->
    <link rel="stylesheet" type="text/css" media="screen" href="css/trirand/ui.jqgrid.css" />
    

    <script type="text/ecmascript" src="js/jquery.json-2.2.min.js"></script>

    </head>
    <body>
        
    <style type="text/css">

        /* set the size of the datepicker search control for Order Date*/
        #ui-datepicker-div { font-size:11px; }
        
        /* set the size of the autocomplete search control*/
        .ui-menu-item {
            font-size: 11px;
        }

        .ui-autocomplete { font-size: 11px; position: absolute; cursor: default;z-index:5000 !important;}      
        
        
        
    </style>
        
        <table id="jqGrid" align="center"></table>
    <div id="jqGridPager"></div>

    <script type="text/javascript">
        
        
        function cargarAgencias()
        {
            var valoresx = "";
            $.ajax({
                                data: {},
                                type: "POST",
                                dataType: "json",
                                url: "<?=$homesite;?>/web/app_dev.php/agencia/searchcombo/",
                                async:false,
                                success: function(data)
                                {


                                                        var arrayValores = data;

                                                        valoresx ="{";
                                                        $.each( arrayValores, function ( userkey, uservalue) 
                                                        {
                                                            if(valoresx == "{")
                                                            {
                                                                //console.log(userkey + ': ' + uservalue.id + ': ' + uservalue.descripcion );
                                                                valoresx += "\"" + uservalue.id + "\":\"" + uservalue.descripcion+"\"";
                                                            }
                                                            else
                                                            {
                                                                valoresx += "," + "\"" + uservalue.id + "\":\"" + uservalue.descripcion+"\"";
                                                            }
                                                        });
                                                        valoresx +="}";
                                                        //valores = data;
                                                        //$.parseJSON(valores);
                
                                                        return valoresx;
                                },
                                error: function(xhr, textStatus, errorThrown) {
                                    // Handle error
                                    alert(errorThrown);
                                    //$.unblockUI(); 
                                  }
                        });
              return valoresx;
       }
       
   
        function cargarTipoAdicional()
        {
            var valoresx = "";
            $.ajax({
                                data: {},
                                type: "POST",
                                dataType: "json",
                                url: "<?=$homesite;?>/web/app_dev.php/tipoadicional/search/",
                                async:false,
                                success: function(data)
                                {


                                                        var arrayValores = data;

                                                        valoresx ="{";
                                                        $.each( arrayValores, function ( userkey, uservalue) 
                                                        {
                                                            if(valoresx == "{")
                                                            {
                                                                //console.log(userkey + ': ' + uservalue.id + ': ' + uservalue.descripcion );
                                                                valoresx += "\"" + uservalue.id + "\":\"" + uservalue.descripcion+"\"";
                                                            }
                                                            else
                                                            {
                                                                valoresx += "," + "\"" + uservalue.id + "\":\"" + uservalue.descripcion+"\"";
                                                            }
                                                        });
                                                        valoresx +="}";
                                                        //valores = data;
                                                        //$.parseJSON(valores);
                
                                                        return valoresx;
                                },
                                error: function(xhr, textStatus, errorThrown) {
                                    // Handle error
                                    alert(errorThrown);
                                    //$.unblockUI(); 
                                  }
                        });
              return valoresx;
       }
       
       
       
        var agencias = JSON.parse(cargarAgencias());
        var tipoAdicionales = JSON.parse(cargarTipoAdicional());
        
        
        var debaja = {"0":"No","1":"Si"};

    
    
    $(document).ready(function () {

              

              $("#jqGrid").jqGrid({
              url: '<?=$homesite;?>/web/app_dev.php/adicional/search/',
              editurl: '<?=$homesite;?>/web/app_dev.php/adicional/editar/',
              datatype: "json",
              sortname: 'id',
              sortorder: 'asc',
              datatype: "json",
              mtype: 'POST',
              viewrecords: true,
              ondblClickRow: function(rowid) {
                   jQuery(this).jqGrid('editGridRow', rowid,
                   {
                    recreateForm:true
                    ,closeAfterEdit:true,
                     closeOnEscape:true
                    ,reloadAfterSubmit:true
                   });
               },
               colModel: [
                { 
                  label: 'Id'
                  , name: 'id'
                  , index: 'id'
                  , width: 75 
                  , search: false
                  , hidden: false
                  , sorttype: 'number'
                  ,key: true
                },
                { 
                  label: 'Tipo Adicional'
                  , name: 'tipo_adicional.descripcion'
                  , index: 'tipo_adicional'
                  , width: 50
                  , sorttype: 'int'
                  ,editable: true
                  ,edittype: 'select'
                  ,editoptions: 
                              {
                                 value:tipoAdicionales
                              }
                  ,stype: 'select'
                  ,searchoptions: 
                              {
                                 value:tipoAdicionales
                              }
                  ,editrules : { required: true}
                },
                 { 
                  label: 'Agencia'
                  , name: 'agencia.descripcion'
                  , index: 'agencia'
                  , width: 100 
                  ,editable: true
                  ,edittype: 'select'
                  ,stype: 'select'
                  ,editoptions: 
                              {
                                 value:agencias
                              }
                  ,searchoptions: 
                              {
                                 value:agencias
                                ,sopt: ['eq','ne']
                              }
                  ,editrules : { required: true}
                },
                { 
                  label: 'Cantidad'
                  , name: 'cantidad'
                  , index: 'cantidad'
                  , width: 50 
                  ,editable: true
                  ,sortype:'int'
                  ,editrules : { required: false, number:true}
                  ,searchoptions: 
                            {
                             sopt : ['eq','ne', 'lt', 'gt']
                            }
                }
                ,
                { 
                  label: 'Valor'
                  , name: 'valor'
                  , index: 'valor'
                  , width: 70
                  ,editable: true
                  ,edittype: 'text'
                  ,sortype:'number'
                  ,editrules : { required: false, number:true}
                  ,searchoptions: 
                            {
                             sopt : ['eq','ne', 'lt', 'gt']
                            }
                },
                   {
                       label: 'Descripción'
                       , name: 'descripcion'
                       , index: 'descripcion'
                       , width: 255
                       ,editable: true
                       ,editrules : { required: false}
                       ,searchoptions:
                   {
                       sopt: ['cn']
                   }
                   }
                ,
                { 
                  label: 'De Baja'
                  , name: 'debaja'
                  , index: 'debaja'
                  , width: 50 
                  ,editable: true
                  ,hidden:false
                  ,edittype: 'select'
                  ,editoptions: { value: "false:No;true:Si" }
                  ,editrules : { required: true,edithidden: true}
                   ,
                  // stype defines the search type control - in this case HTML select (dropdownlist)
                        stype: "select",
                        // searchoptions value - name values pairs for the dropdown - they will appear as options
                        searchoptions: { 
                               value:{false:"NO",true:"SI"}
                          }                    
                  
                }
                // sorttype is used only if the data is loaded locally or loadonce is set to true
                //{ label: 'Quantity', name: 'Quantity', width: 80, sorttype: 'number' }                   
              ],
              width: 'auto',
              height:'auto',
              page:1,
              rowNum: 10,
              rowList: [10, 50, 100],
              loadonce: false, // this is just for the demo
              pager: "#jqGridPager"
            });

              $('#jqGrid').navGrid('#jqGridPager',
                // the buttons to appear on the toolbar of the grid
                //Botonera de la grilla
                { 
                  edit: true
                  , add: true //Boton de agregar
                  , del: false //Boton de eliminar
                  , search: true
                  , refresh: true
                  , view: true //Boton de ver datos
                  , position: "left"
                  , cloneToTop: false 
                },
                // options for the Edit Dialog
                {
                    editCaption: "Editar Curso",
                    recreateForm: true,
                    closeAfterEdit: true,
                    closeOnEscape:true,
                    afterSubmit : function( response,data, postdata, oper) 
                    {

                       var res = $.parseJSON(response.responseText);
                       return [true, ""];
                    },
                    errorTextFormat: function (data) 
                    {
                        return 'Error: ' + data.responseText
                    }
                },
                // options for the Add Dialog
                {
                    addCaption: "Crear Curso",
                    closeAfterAdd: true,
                    recreateForm: true,
                    closeOnEscape:true,
                    afterSubmit : function( response,data, postdata, oper) 
                    {
                        var res = $.parseJSON(response.responseText);
                        if(res.okregistro=="false")
                        {
                          return [false,res.error,""];
                        }
                        return [true, ""];
                    },
                    errorTextFormat: function (data) 
                    {
                        return 'Error: ' + data.responseText
                    }
                },
                // options for the Delete Dailog
                {
                    url: '0003B.php',
                    datatype: "json",
                   
                    errorTextFormat: function (data) {
                        return 'Error: ' + data.responseText
                    }
                }
                
                );

                redimensionar();
       

});



$(window).resize(function()
{
 redimensionar();
 });
 
 function redimensionar()
 {
    $('#jqGrid').jqGrid('setGridHeight',($(window).innerHeight()-80));
    $('#jqGrid').jqGrid('setGridWidth',($(window).width()-20));

 }
</script>
<script type="text/ecmascript" src="js/jquery-ui.min.js"></script>
<script>
        $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '<Ant',
        nextText: 'Sig>',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
        dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
        };
        $.datepicker.setDefaults($.datepicker.regional['es']);
        
</script>
    </body>
</html>