<?PHP
    include_once("config.php");
?>
<!DOCTYPE html>
<html>
    <head>
    <!-- The jQuery library is a prerequisite for all jqSuite products -->
    <script type="text/ecmascript" src="js/jquery.min.js"></script> 
    <!-- We support more than 40 localizations -->
    <script type="text/ecmascript" src="js/trirand/i18n/grid.locale-es.js"></script>
    <!-- This is the Javascript file of jqGrid -->   
    <script type="text/ecmascript" src="js/trirand/jquery.jqGrid.min.js"></script>
    <!-- This is the localization file of the grid controlling messages, labels, etc.
    <!-- A link to a jQuery UI ThemeRoller theme, more than 22 built-in and many more custom -->
    <link rel="stylesheet" type="text/css" media="screen" href="css/jquery-ui.css" />
    <!-- The link to the CSS that the grid needs -->
    <link rel="stylesheet" type="text/css" media="screen" href="css/trirand/ui.jqgrid.css" />
    

    <script type="text/ecmascript" src="js/jquery.json-2.2.min.js"></script>
    </head>
    <body>

                <table id="jqGrid" align="center"></table>
                <div id="jqGridPager"></div>


    <style type="text/css">

        /* set the size of the datepicker search control for Order Date*/
        #ui-datepicker-div { font-size:11px; }
        
        /* set the size of the autocomplete search control*/
        .ui-menu-item {
            font-size: 11px;
        }

        .ui-autocomplete { font-size: 11px; position: absolute; cursor: default;z-index:5000 !important;}      
        
        
       

    </style>
        


    <script type="text/javascript">
        
        var scrollPosition = 0;
        var ids = [];

        function RefreshGridData(idGrilla) 
        {
            //alert(idGrilla);
            var num;
            ids = new Array();
            $("#jqGrid"+idGrilla+" tr:has(.sgexpanded)").each(function () {
                num = $(this).attr('id');
                ids.push(num);
            });
            $("#jqGrid"+idGrilla+"").trigger("reloadGrid");
        }
        
        
        
        function cargarEstadoOrdenes()
        {
            var valoresx = "";
            $.ajax({
                                data: {},
                                type: "POST",
                                dataType: "json",
                                url: "<?=$homesite;?>/web/app_dev.php/estadosoc/search/",
                                async:false,
                                success: function(data)
                                {


                                                        var arrayValores = data.rows;

                                                        valoresx ="{";
                                                        $.each( arrayValores, function ( userkey, uservalue) 
                                                        {
                                                            if(valoresx == "{")
                                                            {
                                                                //console.log(userkey + ': ' + uservalue.id + ': ' + uservalue.descripcion );
                                                                valoresx += "\"" + uservalue.id + "\":\"" + uservalue.descripcion+"\"";
                                                            }
                                                            else
                                                            {
                                                                valoresx += "," + "\"" + uservalue.id + "\":\"" + uservalue.descripcion+"\"";
                                                            }
                                                        });
                                                        valoresx +="}";
                                                        //valores = data;
                                                        //$.parseJSON(valores);
                
                                                        return valoresx;
                                },
                                error: function(xhr, textStatus, errorThrown) {
                                    // Handle error
                                    alert(errorThrown);
                                    //$.unblockUI(); 
                                  }
                        });
              return valoresx;
       }
        
        function getProveedores()
        {
            var valoresx = "";
            $.ajax({
                                data: {},
                                type: "POST",
                                dataType: "json",
                                url: "<?=$homesite;?>/web/app_dev.php/proveedores/search/",
                                async:false,
                                success: function(data)
                                {


                                                        var arrayValores = data.rows;

                                                        valoresx ="{";
                                                        $.each( arrayValores, function ( userkey, uservalue) 
                                                        {
                                                            if(valoresx == "{")
                                                            {
                                                                //console.log(userkey + ': ' + uservalue.id + ': ' + uservalue.descripcion );
                                                                valoresx += "\"" + uservalue.id + "\":\"" + uservalue.nombre+"\"";
                                                            }
                                                            else
                                                            {
                                                                valoresx += "," + "\"" + uservalue.id + "\":\"" + uservalue.nombre+"\"";
                                                            }
                                                        });
                                                        valoresx +="}";
                                                        //valores = data;
                                                        //$.parseJSON(valores);
                
                                                        return valoresx;
                                },
                                error: function(xhr, textStatus, errorThrown) {
                                    // Handle error
                                    alert(errorThrown);
                                    //$.unblockUI(); 
                                  }
                        });
              return valoresx;
       }
       
       var proveedores = JSON.parse(getProveedores());
       var estadosOc = JSON.parse(cargarEstadoOrdenes()); 
        var debaja = {false:"No",true:"Si"};
        var estadoAdmin = {false:"Pendiente",true:"Facturada"};
        
        
        
        function cargarTipoComprobantes()
        {
            var valoresx = "";
            $.ajax({
                                data: {},
                                type: "POST",
                                dataType: "json",
                                url: "<?=$homesite;?>/web/app_dev.php/tipocomprobante/search/",
                                async:false,
                                success: function(data)
                                {


                                                        var arrayValores = data;

                                                        valoresx ="{\"\":\"[Seleccione]\"";
                                                        $.each( arrayValores, function ( userkey, uservalue) 
                                                        {
                                                            
                                                            
                                                                valoresx += "," + "\"" + uservalue.id + "\":\"" + uservalue.descripcion+"\"";
                                                            
                                                        });
                                                        valoresx +="}";
                                                        //valores = data;
                                                        //$.parseJSON(valores);
                                                        return valoresx;
                                },
                                error: function(xhr, textStatus, errorThrown) {
                                    // Handle error
                                    alert(errorThrown);
                                    //$.unblockUI(); 
                                  }
                        });
              return valoresx;
       }
       
        var tipoComprobantes = JSON.parse(cargarTipoComprobantes());
    
    
    $(document).ready(function () {

           
        
              function siNoFmatter (cellvalue, options, rowObject)
              {
                   if(cellvalue)
                   {
                       return "Si";
                   }
                   else
                   {
                       return "No";
                   }
              }

              function noSiFmatter (cellvalue, options, rowObject)
              {
                   if(cellvalue)
                   {
                       return "Si";
                   }
                   else
                   {
                       return "No";
                   }
              }
              
              function adminFormatter(cellvalue, options, rowObject)
              {
                  if(cellvalue)
                   {
                       return "Facturada";
                   }
                   else
                   {
                       return "Pendiente";
                   }
              }
              
            function esAdmin()
            {
                 var valoresx = "";
                 $.ajax({
                                     data: {},
                                     type: "POST",
                                     dataType: "json",
                                     url: "<?=$homesite;?>/web/app_dev.php/contrato/esadmin/",
                                     async:false,
                                     success: function(data)
                                     {
                                         valoresx = data.admin;


                                         return valoresx;
                                     },
                                     error: function(xhr, textStatus, errorThrown) 
                                     {
                                         // Handle error
                                         alert(errorThrown);
                                         //$.unblockUI(); 
                                     }
                             });
                   return valoresx;
            }
       
                
              var admin = esAdmin();
              var noAdmin = true;
              if(admin == "true")
              {
                admin = true;
                noAdmin = false;
              }
              else
              {
                admin = false;
              }
              
              function getAdmin()
              {
                  return admin;
              }

              $("#jqGrid").jqGrid({
              url: '<?=$homesite;?>/web/app_dev.php/ordencompra/search/',
              editurl: '<?=$homesite;?>/web/app_dev.php/ordencompra/crear/',
              datatype: "json",
              sortname: 'id',
              sortorder: 'asc',
              mtype: 'POST', 
              gridview:false,
              gridComplete: function () {
                             for (var j = 0; j < ids.length; j = j + 1) {
                                 $("#jqGrid").jqGrid('expandSubGridRow', ids[j]);
                             }
                         },

              //comienzo sub grilla  
               "subGridOptions": {
                "plusicon": "ui-icon-triangle-1-e",
                "minusicon": "ui-icon-triangle-1-s",
                "openicon": "ui-icon-arrowreturn-1-e"
                },
             "subGrid": true,
             "subGridRowExpanded": function(subgridid, id) {
                var data = {
                    subgrid: subgridid,
                    rowid: id,
                    subgrilla:"no"
                };
               $("#" + jQuery.jgrid.jqID(subgridid)).load('subGridOc.php', data);
             }//fin de la subgrilla
             ,
              afterInsertRow: function(rowid, aData, rowelem) 
              {
                    
                    //condición para mostrar o no la subgrilla
                    /* 
                    if(rowelem.taller.externo == true){
                        $('tr#'+rowid, $(this))
                         .children("td.sgcollapsed")
                         .html("")
                         .removeClass('ui-sgcollapsed sgcollapsed');
                    }*/
                },  
              viewrecords: true,
               ondblClickRow: function(rowid) {
                   jQuery(this).jqGrid('editGridRow', rowid,
                   {
                    recreateForm:true
                    ,closeAfterEdit:true,
                    closeOnEscape:true
                    ,reloadAfterSubmit:true
                    ,"afterSubmit" : function( data, postdata, oper) 
                    {
                        var response = data.responseJSON;
                        if (response.hasOwnProperty("violations")) {
                                if(response.violations.length) 
                                {
                                        return [false,response.violations[0].message];
                                }
                        }
                        
                        if (response.hasOwnProperty("error")) {
                                if(response.error.length) 
                                {
                                        return [false,response.error];
                                }
                        }
                        return [true,"",""];
                    },
                    errorTextFormat: function (data) {
                        return 'Error: ' + data.responseText
                    }
                   });
               },
               colModel: [
                { 
                  label: 'Id'
                  , name: 'id'
                  , index: 'id'
                  , width: 75 
                  , search: false
                  , hidden: false
                  , sorttype: 'number'
                  ,key: true
                },
                { 
                  label: 'Descripcion'
                  , name: 'descripcion'
                  , index: 'descripcion'
                  , width: 255 
                  ,editable: true
                  ,editrules : { required: true}
                  ,searchoptions: 
                              {
                               sopt: ['cn']
                              }
                },
                { 
                  label: 'Proveedor'
                  , name: 'proveedor.nombre'
                  , index: 'proveedor.nombre'
                  , width: 50 
                  ,editable: true
                  ,hidden:false
                  ,edittype: 'select'
                  ,stype: 'select'
                  ,editoptions: { value: proveedores }
                  ,searchoptions: 
                              {
                                 value: proveedores
                                 ,sopt: ['eq','ne']
                              }
                  ,editrules : { required: true,edithidden: true}
                  
                },
                { 
                  label: 'Estado'
                  , name: 'estado.descripcion'
                  , index: 'estado.descripcion'
                  , width: 100 
                  ,editable:getAdmin()
                  ,edittype: 'select'
                  ,stype:'select'
                  
                  ,editoptions: 
                              {
                                 value:estadosOc
                              }
                  ,searchoptions: 
                              {
                                 value:estadosOc
                                 ,sopt: ['eq','ne']
                              }
                  ,editrules : {edithidden: true , required: true}
                  
                },
                { 
                  label: 'Tipo CTE'
                  , name: 'tipo_comprobante.descripcion'
                  , index: 'tipo_comprobante.descripcion'
                  , width: 50 
                  ,editable: true
                  ,hidden:false
                  ,edittype: 'select'
                  ,stype: 'select'
                  ,editoptions: { value: tipoComprobantes }
                  ,searchoptions: 
                              {
                                 value: tipoComprobantes
                                 ,sopt: ['eq','ne']
                              }
                  ,editrules : { required: false,edithidden: true}
                  
                },
                
                { 
                    label: 'Nro. CTE'
                    , name: 'nrofactura'
                    , index: 'nrofactura'
                    , width: 50 
                    , editable: true
                    , sortype:'int'
                    , editrules : { required: false, number:true }
                    , searchoptions: 
                    {
                     sopt : ['eq','ne', 'lt', 'gt']
                    }
                    ,searchrules : { number:true }
                },
                { 
                  label: 'De Baja'
                  , name: 'debaja'
                  , index: 'debaja'
                  , width: 100 
                  ,editable: true
                  ,hidden:false
                  ,edittype: 'select'
                  ,stype: 'select'
                  ,formatter:siNoFmatter
                  ,editoptions: { value: debaja , defaultValue:"false"}
                  ,searchoptions: { value: debaja ,sopt: ['eq','ne']}
                  ,editrules : { required: true,edithidden: true}
                  
                },
                { 
                  label: 'Estado Admin'
                  , name: 'pagado'
                  , index: 'pagado'
                  , width: 100 
                  ,editable:getAdmin()
                  ,hidden:false
                  ,edittype: 'select'
                  ,stype: 'select'
                  ,formatter:adminFormatter
                  ,editoptions: { value: estadoAdmin , defaultValue:"Pendiente"}
                  ,searchoptions: { value: estadoAdmin ,sopt: ['eq','ne']}
                  ,editrules : { required: true,edithidden: true}
                },
                
                { 
                  label: 'Total'
                  , name: 'sumatoria'
                  , index: 'sumatoria'
                  , width: 50 
                  ,editable: false
                  ,hidden:false
                  
                }
                // sorttype is used only if the data is loaded locally or loadonce is set to true
                //{ label: 'Quantity', name: 'Quantity', width: 80, sorttype: 'number' }                   
              ],
              width: 'auto',
              height:'auto',
              page:1,
              rowNum: 10,
              rowList: [10, 50, 100],
              loadonce: false, // this is just for the demo
              pager: "#jqGridPager"
            });

            $('#jqGrid').navGrid('#jqGridPager',
                // the buttons to appear on the toolbar of the grid
                //Botonera de la grilla
                { 
                  edit: true
                  , add: true //Boton de agregar
                  , del: false //Boton de eliminar
                  , search: true
                  , refresh: true
                  , view: false //Boton de ver datos
                  , position: "left"
                  , cloneToTop: false 
                },
                // options for the Edit Dialog
                {
                    editCaption: "Editar &Oacute;rden Compra",
                    recreateForm: true,
                    closeAfterEdit: true,
                    closeOnEscape:true
                    ,"afterSubmit" : function( data, postdata, oper) 
                    {
                        var response = data.responseJSON;
                        if (response.hasOwnProperty("violations")) {
                                if(response.violations.length) 
                                {
                                        return [false,response.violations[0].message];
                                }
                        }
                        
                        if (response.hasOwnProperty("error")) {
                                if(response.error.length) 
                                {
                                        return [false,response.error];
                                }
                        }
                        return [true,"",""];
                    },
                    errorTextFormat: function (data) {
                        return 'Error: ' + data.responseText
                    }
                },
                // options for the Add Dialog
                {
                    addCaption: "Crear &Oacute;rden Compra",
                    closeAfterAdd: true,
                    recreateForm: true,
                    closeOnEscape:true,
                    "afterSubmit" : function( data, postdata, oper) 
                    {
                        var response = data.responseJSON;
                        if (response.hasOwnProperty("violations")) {
                                if(response.violations.length) 
                                {
                                        return [false,response.violations[0].message];
                                }
                        }
                        
                        if (response.hasOwnProperty("error")) {
                                if(response.error.length) 
                                {
                                        return [false,response.error];
                                }
                        }
                        return [true,"",""];
                    },
                    errorTextFormat: function (data) {
                        return 'Error: ' + data.responseText
                    }
                }
                
                );

                $('#jqGrid').jqGrid('setGridHeight',($(window).innerHeight()-80));
                $('#jqGrid').jqGrid('setGridWidth',$(window).width() - 20);

                //segunda grilla
                
              
           

});

    /*
     var height = $(window).height();
            $('.ui-jqgrid-bdiv').height(height-100);
            
            var alto = $(window).width();
            $('.ui-jqgrid-bdiv').width(alto);
            alert(alto);
            
            $("#jqGrid").jqGrid({with:100000});
            */
           
           $(window).resize(function(){
   $('#jqGrid').jqGrid('setGridHeight',($(window).innerHeight()-80));
                $('#jqGrid').jqGrid('setGridWidth',$(window).width() - 20);


    
});
</script>
<script type="text/ecmascript" src="js/jquery-ui.min.js"></script>
<script>
        $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '<Ant',
        nextText: 'Sig>',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
        dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
        };
        $.datepicker.setDefaults($.datepicker.regional['es']);
</script>
    </body>
</html>