<?PHP
include_once("config.php");
    
    //echo "<pre>";
    //print_r($_POST);
    //echo "</pre>";
    /*
    $objeto = curl_init("http://local.hertz/web/app_dev.php/contrato/crear/");
    
    //curl_setopt($objeto,CURLOPT_RETURNTRANSFER,1);
    
    curl_setopt($objeto, CURLOPT_HTTPHEADER, array('Content-Type: application/xml', ""));
    curl_setopt($objeto, CURLOPT_HEADER, 1);
    curl_setopt($objeto, CURLOPT_USERPWD, "admin" . ":" . "admin");
    curl_setopt($objeto, CURLOPT_TIMEOUT, 30);
    curl_setopt($objeto, CURLOPT_POST, 1);
    curl_setopt($objeto, CURLOPT_RETURNTRANSFER, TRUE);
    $respuesta = curl_exec($objeto);
    
    echo $respuesta;
    die();*/
?>
<!DOCTYPE html>
<html>
    <head>
    <!-- The jQuery library is a prerequisite for all jqSuite products -->
    <script type="text/ecmascript" src="js/jquery.min.js"></script> 
    <!-- We support more than 40 localizations -->
    <script type="text/ecmascript" src="js/trirand/i18n/grid.locale-es.js"></script>
    <!-- This is the Javascript file of jqGrid -->   
    <script type="text/ecmascript" src="js/trirand/jquery.jqGrid.min.js"></script>
    <!-- This is the localization file of the grid controlling messages, labels, etc.
    <!-- A link to a jQuery UI ThemeRoller theme, more than 22 built-in and many more custom -->
    <link rel="stylesheet" type="text/css" media="screen" href="css/jquery-ui.css" />
    <!-- The link to the CSS that the grid needs -->
    <link rel="stylesheet" type="text/css" media="screen" href="css/trirand/ui.jqgrid.css" />
    

    <script type="text/ecmascript" src="js/jquery.json-2.2.min.js"></script>
    <script type="text/ecmascript" src="js/jquery.mask.js"></script>
    <script type="text/ecmascript" src="js/jquery.blockUI.js"></script>
    <script type="text/ecmascript" src="js/jquery.maskedinput.min.js"></script>

    </head>
    <body>
        
    <style type="text/css">

        /* set the size of the datepicker search control for Order Date*/
        #ui-datepicker-div { font-size:11px; }
        
        /* set the size of the autocomplete search control*/
        .ui-menu-item {
            font-size: 11px;
        }

        .ui-autocomplete { font-size: 11px; position: absolute; cursor: default;z-index:5000 !important;}      
        .my-select-loading {
            background: white url('ui-anim_basic_16x16.gif') right center no-repeat;
            height: 16px;
            width: 16px;
            margin-left: .5em;
            display: inline-block;
        }
        
        
    </style>    
    <table id="jqGrid" align="center"></table>
    <div id="jqGridPager"></div>

    <script type="text/javascript">
        function isReadOnly()
        {
            {
                var valoresx = "";
                $.ajax({
                    data: {modulo : 'ExternaReservaTWindow'},
                    type: "GET",
                    dataType: "json",
                    url: "<?=$homesite;?>/web/app_dev.php/modulolecturaperfil/isreadonly/",
                    async:false,
                    success: function(data)
                    {
                        valoresx = data;
                        return valoresx;
                    },
                    error: function(xhr, textStatus, errorThrown)
                    {
                        // Handle error
                        console.log(xhr, textStatus, errorThrown)
                        alert(errorThrown);
                    }
                });
                return valoresx;
            }
        }


        function cargarCategoriasVeh()
        {
            var valoresx = "";
            $.ajax({
                                data: {},
                                type: "POST",
                                dataType: "json",
                                url: "<?=$homesite;?>/web/app_dev.php/categoriasveh/search/",
                                async:false,
                                success: function(data)
                                {


                                                        var arrayValores = data.rows;

                                                        valoresx ='{';
                                                        $.each( arrayValores, function ( userkey, uservalue) 
                                                        {
                                                            if(valoresx == "{")
                                                            {
                                                                //console.log(userkey + ': ' + uservalue.id + ': ' + uservalue.descripcion );
                                                                valoresx += "\"" + uservalue.id + "\":\"" + uservalue.descripcion+"\"";
                                                            }
                                                            else
                                                            {
                                                                valoresx += "," + "\"" + uservalue.id + "\":\"" + uservalue.descripcion+"\"";
                                                            }
                                                        });
                                                        
                                                        valoresx +=',"":"[Categoria]"';
                                                        valoresx +="}";
                                                        //valores = data;
                                                        //$.parseJSON(valores);
                
                                                        return valoresx;
                                },
                                error: function(xhr, textStatus, errorThrown) {
                                    // Handle error
                                    alert(errorThrown);
                                    //$.unblockUI(); 
                                  }
                        });
              return valoresx;
       }
   
   
        function cargarMetodosPago()
        {
            var valoresx = "";
            $.ajax({
                                data: {},
                                type: "POST",
                                dataType: "json",
                                url: "<?=$homesite;?>/web/app_dev.php/metodopago/search/",
                                async:false,
                                success: function(data)
                                {


                                                        var arrayValores = data;

                                                        valoresx ="{";
                                                        $.each( arrayValores, function ( userkey, uservalue) 
                                                        {
                                                            if(valoresx == "{")
                                                            {
                                                                //console.log(userkey + ': ' + uservalue.id + ': ' + uservalue.descripcion );
                                                                valoresx += "\"" + uservalue.id + "\":\"" + uservalue.descripcion+"\"";
                                                            }
                                                            else
                                                            {
                                                                valoresx += "," + "\"" + uservalue.id + "\":\"" + uservalue.descripcion+"\"";
                                                            }
                                                        });
                                                        valoresx +="}";
                                                        //valores = data;
                                                        //$.parseJSON(valores);
                
                                                        return valoresx;
                                },
                                error: function(xhr, textStatus, errorThrown) {
                                    // Handle error
                                    alert(errorThrown);
                                    //$.unblockUI(); 
                                  }
                        });
              return valoresx;
       }
       
       function cargarTipoDoc()
        {
            var valoresx = "";
            $.ajax({
                                data: {},
                                type: "POST",
                                dataType: "json",
                                url: "<?=$homesite;?>/web/app_dev.php/tipodoc/search/",
                                async:false,
                                success: function(data)
                                {


                                                        var arrayValores = data;

                                                        valoresx ="{";
                                                        $.each( arrayValores, function ( userkey, uservalue) 
                                                        {
                                                            if(valoresx == "{")
                                                            {
                                                                //console.log(userkey + ': ' + uservalue.id + ': ' + uservalue.descripcion );
                                                                valoresx += "\"" + uservalue.id + "\":\"" + uservalue.descripcion+"\"";
                                                            }
                                                            else
                                                            {
                                                                valoresx += "," + "\"" + uservalue.id + "\":\"" + uservalue.descripcion+"\"";
                                                            }
                                                        });
                                                        valoresx +="}";
                                                        //valores = data;
                                                        //$.parseJSON(valores);
                
                                                        return valoresx;
                                },
                                error: function(xhr, textStatus, errorThrown) {
                                    // Handle error
                                    alert(errorThrown);
                                    //$.unblockUI(); 
                                  }
                        });
              return valoresx;
       }

         function esAdmin()
         {
             var valoresx = "";
             $.ajax({
                 data: {},
                 type: "POST",
                 dataType: "json",
                 url: "<?=$homesite;?>/web/app_dev.php/contrato/esadmin/",
                 async:false,
                 success: function(data)
                 {
                     valoresx = data;
                     return valoresx;
                 },
                 error: function(xhr, textStatus, errorThrown)
                 {
                     // Handle error
                     alert(errorThrown);
                     //$.unblockUI();
                 }
             });
             return valoresx;
         }
       
        function cargarOrigenReserva(idOrigen)
        {
            
            if(isNaN(idOrigen))
            {
                return "[]";
            }
            
            var valoresx = "";
            $.ajax({
                                data: {},
                                type: "POST",
                                dataType: "json",
                                url: "<?=$homesite;?>/web/app_dev.php/origenreserva/search/?idOrigen="+idOrigen,
                                async:false,
                                success: function(data)
                                {
                                                        var arrayValores = data;

                                                        valoresx ='{';
                                                        $.each( arrayValores, function ( userkey, uservalue) 
                                                        {
                                                            if(valoresx == "{")
                                                            {
                                                                //console.log(userkey + ': ' + uservalue.id + ': ' + uservalue.descripcion );
                                                                valoresx += "\"" + uservalue.id + "\":\"" + uservalue.descripcion+"\"";
                                                            }
                                                            else
                                                            {
                                                                valoresx += "," + "\"" + uservalue.id + "\":\"" + uservalue.descripcion+"\"";
                                                            }
                                                        });
                                                        if(valoresx == "{")
                                                        {
                                                            valoresx +='"":"[Origen]"';
                                                        }
                                                        else
                                                        {
                                                            valoresx +=',"":"[Origen]"';
                                                        }
                                                        valoresx +="}";
                                                        //valores = data;
                                                        //$.parseJSON(valores);
                
                                                        return valoresx;
                                },
                                error: function(xhr, textStatus, errorThrown) {
                                    // Handle error
                                    alert(errorThrown);
                                    //$.unblockUI(); 
                                  }
                        });
              return valoresx;
       }
       
       
       function getLockeo(idReserva)
       {
            
            
            var valoresx = "";
            $.ajax({
                                data: {},
                                type: "POST",
                                dataType: "json",
                                url: "<?=$homesite;?>/web/app_dev.php/contrato/"+idReserva+"/searchlock/",
                                async:false,
                                success: function(data)
                                {
                                                        
                                                        valoresx = data;
                                                        return valoresx;
                                },
                                error: function(xhr, textStatus, errorThrown) {
                                    // Handle error
                                    alert(errorThrown);
                                    //$.unblockUI(); 
                                  }
                        });
              return valoresx;
       }
       
       function setLockeo(idReserva)
       {
            
            
            var valoresx = "";
            $.ajax({
                                data: {},
                                type: "POST",
                                dataType: "json",
                                url: "<?=$homesite;?>/web/app_dev.php/contrato/"+idReserva+"/setlock/",
                                async:false,
                                success: function(data)
                                {
                                                        
                                                        valoresx = data;
                                                        return valoresx;
                                },
                                error: function(xhr, textStatus, errorThrown) {
                                    // Handle error
                                    alert(errorThrown);
                                    //$.unblockUI(); 
                                  }
                        });
              return valoresx;
       }
       

        var metodosPagos = JSON.parse(cargarMetodosPago());
        var tiposDoc = JSON.parse(cargarTipoDoc());
        //var categoriasVeh = JSON.parse(cargarCategoriasVeh());
        var origenReserva = JSON.parse(cargarOrigenReserva(0)); //origen padre

        var debaja = {false:"No",true:"Si"};
        
        var origenHijo = JSON.parse("[]");

        var admin = esAdmin().admin;

        var readOnly = !admin && isReadOnly();
        
    $(document).ready(function () {

              function siNoFmatter (cellvalue, options, rowObject)
              {
                   if(cellvalue)
                   {
                       return "Si";
                   }
                   else
                   {
                       return "No";
                   }
              }

              function noSiFmatter (cellvalue, options, rowObject)
              {
                   if(cellvalue)
                   {
                       return "Si";
                   }
                   else
                   {
                       return "No";
                   }
              }

              $("#jqGrid").jqGrid({
              url: '<?=$homesite;?>/web/app_dev.php/search/',
              editurl: '<?=$homesite;?>/web/app_dev.php/insertar/',
              datatype: "json",
              sortname: 'id',
              sortorder: 'asc',
              mtype: 'POST',
              
            "subGridOptions": {
                "plusicon": "ui-icon-triangle-1-e",
                "minusicon": "ui-icon-triangle-1-s",
                "openicon": "ui-icon-arrowreturn-1-e"
            },
             "subGrid": true,
        "subGridRowExpanded": function(subgridid, id) {
            //alert("Bloqueo: "+id);
            var respuesta = getLockeo(id);
            
            if(!respuesta)
            {
               setLockeo(id);
            }
            var data = {
                subgrid: subgridid,
                rowid: id,
                lockeado:respuesta
            };
            
            $("#" + jQuery.jgrid.jqID(subgridid)).load('subGridReserva.php', data);
        },
        ondblClickRow: function(rowid) {              
            if (!readOnly) {
                var rowData = $(this).jqGrid('getRowData', rowid);                
                if(!isNaN(rowData["origen_padre.id"]))
                {                    
                    $("#via").empty();
                    origenHijo = JSON.parse(cargarOrigenReserva(rowData["origen_padre.id"]));
                    jQuery(this).setColProp('via', {editoptions:{value:origenHijo}});
                    var auxHijo  = rowData["origen_hijo.id"];
                    $("#jqGrid").jqGrid('setCell', rowid, 'via', auxHijo);
                }
              
                   jQuery(this).jqGrid('editGridRow', rowid,
                   {
                    editCaption: "Editar Reserva",
                    recreateForm: true,
                    closeAfterEdit: true,
                    closeOnEscape:true,
                       "afterShowForm":function(formid){

                        if (!admin){
                            $(formid).find("input").prop("disabled","disabled").css("background","#ccc");
                            $(formid).find("select").prop("disabled","disabled").css("background","#ccc");

                            $("#nro_vuelo, #fecha_hora_salida, #hora_salida, #fecha_hora_devolucion, #hora_devolucion").prop("disabled","").removeAttr('style');
                        }

                       },
                    "afterSubmit" : function( data, postdata, oper) 
                    {
                        var response = data.responseJSON;
                        if (response.hasOwnProperty("violations")) {
                                if(response.violations.length) 
                                {
                                        return [false,response.violations[0].message];
                                }
                        }
                        
                        if (response.hasOwnProperty("error")) {
                                if(response.error.length) 
                                {
                                        return [false,response.error];
                                }
                        }
                        return [true,"",""];
                    },
                    "errorTextFormat": function (data) {
                        return 'Error: ' + data.responseText
                    }
                   });
                
            }    
               },    
              
               colModel: [
                { 
                  label: 'Id'
                  , name: 'id'
                  , index: 'id'
                  , width: 75 
                  , search: false
                  , hidden: false
                  , sorttype: 'number'
                  ,key: true
                },
                { 
                  label: 'Origen'
                  , name: 'origen_padre.id'
                  , index: 'origen_padre'
                  , width: 150 
                  ,hidden:true
                  ,editable: !readOnly
                  ,edittype: 'select'
                  ,stype: 'select'
                  ,formatter: 'select'
                  ,editoptions: 
                              {
                                 value:origenReserva
                                 ,
                                 dataEvents: [
                                   {
                                       type: 'change',
                                       fn: function(e) {
                                           // To be able to save the results of the current selection
                                           // the value of the column property must contain at least
                                           // the current selected 'State'. So we have to reset
                                           // the column property to the following
                                           //grid.setColProp('State', { editoptions:{value: statesOfCountry[v]} });
                                           //grid.setColProp('State', { editoptions: { value: states} });
                                           //resetStatesValues();

                                           // build 'State' options based on the selected 'Country' value
                                           var v = parseInt($(e.target).val(), 10);
                                           //alert(v);
                                           //resetStatesValues(v);
                                           
                                           origenHijo = JSON.parse(cargarOrigenReserva(v));
                                           
                                           //alert(JSON.parse(cargarOrigenReserva(v)).toSource());
                                           $("#via").empty();
                                           //$(this).setColProp('via', { editoptions: { value: cargarOrigenReserva(v) } });
                                           $.each(origenHijo, function (key, data) {
                                            //console.log(data);
                                            
                                             var option = $('<option />');
                                            option.attr('value', key).text(data);

                                            $('#via').append(option);
                                            
                                            });
                                           
                                       }
                                   }
                               ]
                              }
                  ,searchoptions: 
                              {
                                 value:origenReserva
                                ,sopt: ['eq','ne']
                              }
                  ,editrules : { required: false,edithidden: true}
                },
                {
                    label: 'origen hijo'
                  , name: 'origen_hijo.id'
                  , index: 'origen_hijo'
                  , width: 150 
                  ,hidden:true
                  ,editable: false



                  ,editoptions: 
                              {
                                 value:JSON.parse(cargarOrigenReserva($('#origen_hijo.id').val()))
                              }
                  ,editrules : { required: false}

                    
                }
                ,
                {
                    label: 'Via'
                  , name: 'via'
                  , index: 'via'
                  , width: 150 
                  ,hidden:true
                  ,editable: !readOnly
                  ,edittype: 'select'
                  ,stype: 'select'
                  ,formatter: 'select'
                  ,editoptions: 
                              {
                                 value:JSON.parse(cargarOrigenReserva($('#origen_padre.id').val()))
                              }
                  ,editrules : { required: false,edithidden: true}

                    
                },  
                { 
                  label: 'Nombre Via'
                  , name: 'nombre_via'
                  , index: 'nombrevia'
                  , width: 50 
                  , hidden:true
                  //,edittype: "textarea"
                  ,editable: !readOnly
                  ,editrules : { required: true,edithidden: true}
                },
                { 
                  label: 'idcliente'
                  , name: 'cliente.id'
                  , index: 'cliente.id'
                  , width: 150 
                  ,editable: !readOnly
                  ,hidden:true
                  ,editrules : { required: false,  edithidden: false}
                },
                { 
                  label: 'idvehiculo'
                  , name: 'vehiculo.id'
                  , index: 'vehiculo.id'
                  , width: 150 
                  ,editable: !readOnly
                  ,hidden:true
                  ,editrules : { required: true,  edithidden: false}
                },
                { 
                  label: 'Descripcion'
                  , name: 'descripcion'
                  , index: 'descripcion'
                  , width: 150
                  ,hidden:true
                  ,editable: false
                  ,editrules : { required: false}
                },
                { 
                  label: 'Nro. Vuelo'
                  , name: 'nro_vuelo'
                  , index: 'nro_vuelo'
                  , width: 50 ,searchoptions: 
                              {
                                sopt: ['cn']
                              }
                  //,edittype: "textarea"
                  ,editable: !readOnly
                  ,editrules : { required: false}
                }/*,
                { 
                  label: 'Categoría Vehículo'
                  , name: 'categoria'
                  , index: 'categoria'
                  , width: 150 
                  ,hidden:true
                  ,editable: true
                  ,edittype: 'select'
                  ,stype: 'select'
                  ,editoptions: 
                              {
                                 value:categoriasVeh
                              }
                  ,searchoptions: 
                              {
                                 value:categoriasVeh
                                ,sopt: ['eq','ne']
                              }
                  ,editrules : { required: false,edithidden: true}
                }*/,
                { 
                  label: 'Nombre'
                  , name: 'cliente.nombre'
                  , index: 'nombre'
                  , width: 150
                  , sorttype: 'string'
                  ,editable: !readOnly
                  ,editrules : { required: true}
                 ,searchoptions: 
                              {
                                sopt: ['cn']
                              }
                ,"editoptions": 
                    {
                        "dataInit": 

                           function(el) {
                            setTimeout(function() 
                            {
                                if (jQuery.ui) 
                                {
                                    if (jQuery.ui.autocomplete) 
                                    {
                                        jQuery(el).autocomplete({
                                            "appendTo": "body",
                                            "disabled": false,
                                            "delay": 300,
                                            "minLength": 3,
                                            
                                            select: function( event , ui ) 
                                            {
                                                    document.getElementById("cliente.apellido").value = ui.item.value2;
                                                    document.getElementById("cliente.id").value = ui.item.clienteId;
                                                    document.getElementById("cliente.numero_doc").value = ui.item.value3;
                                                    
                                            },
                                            "source": function(request, response) {

                                                $.ajax({
                                                    url: "<?=$homesite;?>/web/app_dev.php/cliente/searchbyname/",
                                                    dataType: "json",
                                                    data: request,
                                                    type: "GET",
                                                    error: function(res, status) {
                                                        alert(res.status + " : " + res.statusText + ". Status: " + status);
                                                    },
                                                    success: function(data) 
                                                    {
                                                        //console.log(response.toSource());



                                                       if(data.length < 1)
                                                        {
                                                            document.getElementById("cliente.apellido").value = "";
                                                            document.getElementById("cliente.id").value = "";
                                                            document.getElementById("cliente.numero_doc").value = "";
                                                        }
                                                        //console.log(data.length);
                                                        response(data);
                                                    }
                                                });
                                            }
                                        });
                                        jQuery(el).autocomplete('widget').css('font-size', '11px');
                                        jQuery(el).autocomplete('widget').css('z-index', '1000');
                                    }
                                }
                            }, 200);
                        }
                    }
                },
                { 
                  label: 'Apellido'
                  , name: 'cliente.apellido'
                  , index: 'apellido'
                  , width: 50 ,searchoptions: 
                              {
                                sopt: ['cn']
                              }
                  //,edittype: "textarea"
                  ,editable: !readOnly
                  ,editrules : { required: true}
                }
              ,
                { 
                  label: 'Tipo Doc.'
                  , name: 'cliente.tipodoc.descripcion'
                  , index: 'tipodoc'
                  , width: 100 
                  ,editable: !readOnly
                  ,edittype: 'select'
                  ,stype: 'select'
                  ,search:false
                  ,editoptions: 
                              {
                                 value:tiposDoc
                              }
                  ,searchoptions: 
                              {
                                 value:tiposDoc
                                ,sopt: ['eq','ne']
                              }
                  ,editrules : { required: true}
                },
                { 
                  label: 'Nro. Doc.'
                  , name: 'cliente.numero_doc'
                  , index: 'numero_doc'
                  , width: 100 
                  ,search:false
                  ,editable: !readOnly
                  ,editrules : { required: true}
                  ,searchoptions: 
                              {
                               sopt: ['cn']
                              }
                },
                {
                       label: 'Télefono'
                       , name: 'nro_telefono'
                       , index: 'nro_telefono'
                       , width: 100 ,searchoptions:
                              {
                           sopt: ['cn']
                              }
                       //,edittype: "textarea"
                       ,editable: !readOnly
                       ,editrules : { required: false}
                }
//                {
//                  label: 'Método Pago'
//                  , name: 'metodo_pago.descripcion'
//                  , index: 'metodo_pago'
//                  , width: 100
//                  ,editable: true
//                  ,edittype: 'select'
//                  ,search:false
//                  ,editoptions:
//                              {
//                                 value:metodosPagos
//                              }
//                  ,editrules : { required: true}
//                }
                , { 
                  label: 'Fecha Salida'
                  , name: 'fecha_hora_salida'
                  , index: 'fechaHoraSalida'
                  , width: 170
                  ,editable: !readOnly
                  ,search:false
                  ,edittype: 'text'
                  ,sorttype: 'date', datefmt:'d/mm/Y'
                  ,formatter: function (cellvalue, options, rowObject) {
                    if(cellvalue)
                    {
                        var y = Number(cellvalue.substring(0,4));
                        var m = Number(cellvalue.substring(5,7));
                        var d = Number(cellvalue.substring(8,10));

                        return String(d)+'/'+String(m)+'/'+String(y);
                    }
                    else
                    {
                        return false;
                    }
                        
                  }
                  ,editoptions: {
                            // dataInit is the client-side event that fires upon initializing the toolbar search field for a column
                            // use it to place a third party control to customize the toolbar
                            dataInit: function (element) {
                                $(element).datepicker({
                                    id: 'orderDate_datePicker',
                                    dateFormat: 'd/m/yy',
                                    //minDate: new Date(2010, 0, 1),
                                    //maxDate: new Date(2020, 0, 1),
                                    showOn: 'focus'
                                });
                            }
                        }
                  ,editrules : { required: true,edithidden: true,date:true}
                }
                ,
                
                { 
                  label: 'Hora Salida'
                  ,name: 'hora_salida'
                  ,index: 'hora_salida'
                  ,width: 170
                  ,editable: !readOnly
                  ,search:false
                  ,sortype:'int'
                  ,editrules : { required: true}
                  ,editoptions:{dataInit:function(el){$(el).mask("99:99");}}
                  ,searchoptions: 
                            {
                             sopt : ['eq','ne', 'lt', 'gt']
                            }
                },
                { 
                  label: 'Fecha Devolución'
                  , name: 'fecha_hora_devolucion'
                  , index: 'fechaHoraDevolucion'
                  , width: 170
                  ,editable: !readOnly
                  ,search:false
                  ,edittype: 'text'
                  ,sorttype: 'date', datefmt:'d/mm/Y'
                  ,formatter: function (cellvalue, options, rowObject) {
                    if(cellvalue)
                    {
                        var y = Number(cellvalue.substring(0,4));
                        var m = Number(cellvalue.substring(5,7));
                        var d = Number(cellvalue.substring(8,10));

                        return String(d)+'/'+String(m)+'/'+String(y);
                        
                    }
                    else
                    {
                        return false;
                    }
                  }
                  ,editoptions: {
                            // dataInit is the client-side event that fires upon initializing the toolbar search field for a column
                            // use it to place a third party control to customize the toolbar
                            dataInit: function (element) {
                                $(element).datepicker({
                                    id: 'orderDate_datePicker',
                                    dateFormat: 'd/m/yy',
                                    //minDate: new Date(2010, 0, 1),
                                    //maxDate: new Date(2020, 0, 1),
                                    showOn: 'focus'
                                });
                            }
                        }
                  ,editrules : { required: true,edithidden: true,date:true}
                }
                ,{ 
                  label: 'Hora Devolución'
                  ,name: 'hora_devolucion'
                  ,index: 'hora_devolucion'
                  ,width: 170
                  ,editable: !readOnly
                  ,search:false
                  ,sortype:'int'
                  ,editrules : { required: true}
                  ,editoptions:{dataInit:function(el){$(el).mask("99:99");}}
                  ,searchoptions: 
                            {
                             sopt : ['eq','ne', 'lt', 'gt']
                            }
                }
               
                ,
                { 
                  label: 'Dominio'
                  , name: 'vehiculo.dominio'
                  , index: 'dominio'
                  , width: 255 
                  ,editable: !readOnly
                  ,searchoptions: 
                              {
                                sopt: ['cn']
                              }
                  ,"editoptions": 
                    {
                        "dataInit": 

                           function(el) {
                            setTimeout(function() 
                            {
                                if (jQuery.ui) 
                                {
                                    if (jQuery.ui.autocomplete) 
                                    {
                                        jQuery(el).autocomplete({
                                            "appendTo": "body",
                                            "disabled": false,
                                            "delay": 300,
                                            "minLength": 3,
                                            
                                            select: function( event , ui ) 
                                            {
                                                    document.getElementById("vehiculo.id").value = ui.item.vehiculoId;
                                            },
                                            "source": function(request, response) {
                                                
                                                

                                                $.ajax({
                                                    url: "<?=$homesite;?>/web/app_dev.php/vehiculo/searchby/",
                                                    dataType: "json",
                                                    data: {
                                                        term : request.term,
                                                        fechaHoraSalida : $("#fecha_hora_salida").val()
                                                        ,fechaHoraDevolucion : $("#fecha_hora_devolucion").val()
                                                        ,HoraSalida : $("#hora_salida").val()
                                                        ,HoraDevolucion : $("#hora_devolucion").val()
                                                        ,categoriaVeh : ""+$("#categoria").val()+""
                                                    },
                                                    type: "GET",
                                                    async:false,                                                    
                                                    error: function(res, status) {
                                                        alert(res.status + " : " + res.statusText + ". Status: " + status);
                                                    },
                                                    success: function(data) 
                                                    {

                                                        //si no devuelve datos setea el campo oculto del id del vehículo en ""
                                                        if(data.length < 1)
                                                        {                                                            
                                                            document.getElementById("vehiculo.id").value = "";
                                                        }
                                                        response(data);
                                                    }
                                                });
                                            }
                                        });
                                        jQuery(el).autocomplete('widget').css('font-size', '11px');
                                        jQuery(el).autocomplete('widget').css('z-index', '1000');
                                    }
                                }
                            }, 200);
                        }
                    }
                  ,editrules : { required: true}
                }
                ,{ 
                  label: 'De Baja'
                  , name: 'debaja'
                  , index: 'debaja'
                  , width: 100 
                  ,editable: !readOnly
                  ,hidden:false
                  ,edittype: 'select'
                  ,stype: 'select'
                  ,formatter:siNoFmatter
                  ,editoptions: { value: debaja , defaultValue:"false"}
                  ,searchoptions: { value: debaja ,sopt: ['eq','ne']}
                  ,editrules : { required: true,edithidden: true}
                  
                  
                }
                ,
                { 
                  label: 'Origen'
                  , name: 'contrato.agencia_origen.descripcion'
                  , index: 'contrato.agencia_origen.descripcion'
                  , width: 150 
                  ,hidden:false
                  ,editable: false
                  
                },
                { 
                  label: 'Destino'
                  , name: 'contrato.agencia_destino.descripcion'
                  , index: 'contrato.agencia_destino.descripcion'
                  , width: 150 
                  ,hidden:false
                  ,editable: false
                  
                }
                // sorttype is used only if the data is loaded locally or loadonce is set to true
                //{ label: 'Quantity', name: 'Quantity', width: 80, sorttype: 'number' }                   
              ],
              viewrecords: true, // show the current page, data rang and total records on the toolbar
              width: 'auto',
              height:'auto',
              page:1,
              rowNum: 10,
              rowList: [10, 50, 100],
              loadonce: false, // this is just for the demo
              pager: "#jqGridPager"
            });

              $('#jqGrid').navGrid('#jqGridPager',
                // the buttons to appear on the toolbar of the grid
                //Botonera de la grilla
                { 
                  edit: !readOnly
                  , add: !readOnly //Boton de agregar
                  , del: !readOnly //Boton de eliminar
                  , search: true
                  , refresh: true
                  , view: false //Boton de ver datos
                  , position: "left"
                  , cloneToTop: false
                },
                // options for the Edit Dialog
                {
                    editCaption: "Editar Reserva",
                    recreateForm: true,
                    closeAfterEdit: true,                    
                    width:1200,
                    "beforeShowForm": function (form) {
                    
                    
                     var rowid = jQuery('#jqGrid').jqGrid('getGridParam', 'selrow'); 
                    var rowData = jQuery('#jqGrid').jqGrid('getRowData', rowid);
                    
                    
                      
                                           //alert(v);
                                           //resetStatesValues(v);
                                           
                                           origenHijo = JSON.parse(cargarOrigenReserva(rowData["origen_padre.id"]));
                                           
                                           //alert(JSON.parse(cargarOrigenReserva(v)).toSource());
                                           $("#via").empty();
                                           //$(this).setColProp('via', { editoptions: { value: cargarOrigenReserva(v) } });
                                           $.each(origenHijo, function (key, data) 
                                           {
                                            
                                            var option = $('<option />');
                                            option.attr('value', key).text(data);
                                            if(rowData["origen_hijo.id"] == key)
                                            {
                                                option.attr('selected', "selected");
                                            }

                                            $('#via').append(option);
                                            
                                            });

                        var auxHijo  = rowData["origen_hijo.id"];
                        

                        $("#jqGrid").jqGrid('setCell', rowid, 'via', auxHijo);
                    
                        $('<a href="#">Vehículos<span class="ui-icon ui-icon-disk"></span></a>')
                            .click(function() 
                            {
                                parent.abrirVehiculos();
                            }).addClass("fm-button ui-state-default ui-corner-all fm-button-icon-left")
                              .prependTo("#Act_Buttons>td.EditButton");
                      
                       $('<tr class="FormData"><td class="CaptionTD ui-widget-content" colspan="2">' +
                       '<hr/><div style="padding:3px" class="ui-widget-header ui-corner-all">' +
                       '<b>Invice information (all about money):</b></div></td></tr>')
                       .insertBefore($('#tr_vehiculo.dominio', form));
                       //alert(form.toSource());
                       
                       jQuery('#jqGrid').toggleSubGridRow('6');
                      
                    }
                    ,"afterSubmit" : function( data, postdata, oper) 
                    {
                        var response = data.responseJSON;
                        if (response.hasOwnProperty("violations")) {
                                if(response.violations.length) 
                                {
                                        return [false,response.violations[0].message];
                                }
                        }
                        
                        if (response.hasOwnProperty("error")) {
                                if(response.error.length) 
                                {
                                        return [false,response.error];
                                }
                        }
                        
                        return [true,"",""];
                    },
                    "errorTextFormat": function (data) {
                        return 'Error: ' + data.responseText
                    }
                },
                // options for the Add Dialog
                {
                    addCaption: "Crear Reserva",
                    closeAfterAdd: true,
                    recreateForm: true,
                    beforeShowForm: function (form) {
        form = $(form);                
        $('<a href="#">Vehículos<span class="ui-icon ui-icon-disk"></span></a>')
                            .click(function() 
                            {
                                parent.abrirVehiculos();
                            }).addClass("fm-button ui-state-default ui-corner-all fm-button-icon-left")
                              .prependTo("#Act_Buttons>td.EditButton");
                      
                                     $('<tr class="FormData"><td class="CaptionTD ui-widget-content" colspan="2">' +
                       '<hr/><div style="padding:3px" class="ui-widget-header ui-corner-all">' +
                       '<b>Invice information (all about money):</b></div></td></tr>')
                       .insertBefore($('#tr_vehiculo.dominio', form));
               
                    $("tr", form).each(function() {
                        var inputs = $(">td.DataTD:has(textarea)",this);
                        if (inputs.length == 1) {
                            var tds = $(">td", this);
                            tds.eq(1).attr("colSpan", tds.length - 1);
                            tds.slice(2).hide();
                        }
                    });
                      
                    },"afterSubmit" : function( data, postdata, oper) 
                    {
                        var response = data.responseJSON;
                        if (response.hasOwnProperty("violations")) {
                                if(response.violations.length) 
                                {
                                        return [false,response.violations[0].message];
                                }
                        }
                        
                        if (response.hasOwnProperty("error")) {
                                if(response.error.length) 
                                {
                                        return [false,response.error];
                                }
                        }
                        
                        return [true,"",""];
                    },
                    "errorTextFormat": function (data) {
                        return 'Error: ' + data.responseText
                    }
                },
                // options for the Delete Dailog
                {
                    url: '0003B.php',
                    datatype: "json",
                   
                    errorTextFormat: function (data) {
                        return 'Error: ' + data.responseText
                    }
                }
                
                );
           
    
                $('#jqGrid').jqGrid('setGridHeight',($(window).innerHeight()-80));
                $('#jqGrid').jqGrid('setGridWidth',$(window).width());
       

jQuery("#jqGrid").jqGrid('saveRow', 
{ 
    successfunc: function( response ) {
        alert(response.toSource());
        return true; 
    }
});

});

    /*
     var height = $(window).height();
            $('.ui-jqgrid-bdiv').height(height-100);
            
            var alto = $(window).width();
            $('.ui-jqgrid-bdiv').width(alto);
            alert(alto);
            
            $("#jqGrid").jqGrid({with:100000});
            */
           
           $(window).resize(function(){
    $('#jqGrid').jqGrid('setGridHeight',($(window).innerHeight()-80));
    $('#jqGrid').jqGrid('setGridWidth',($(window).width()-20));
    


    
});


</script>
<script type="text/ecmascript" src="js/jquery-ui.min.js"></script>
<script>
        $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '<Ant',
        nextText: 'Sig>',
        currentText: 'Hoy',
        numberOfMonths: 3,
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
        dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
        ,onSelect: function(dateText, inst) 
        {   
               document.getElementById("vehiculo.id").value="";
               document.getElementById("vehiculo.dominio").value="";
            }
        };
        $.datepicker.setDefaults($.datepicker.regional['es']);
</script>
    </body>
</html>
