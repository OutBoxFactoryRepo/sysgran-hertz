<?PHP
    include_once("config.php");
?>
<!DOCTYPE html>
<html>
    <head>
    <!-- The jQuery library is a prerequisite for all jqSuite products -->
    <script type="text/ecmascript" src="js/jquery.min.js"></script> 
    <!-- We support more than 40 localizations -->
    <script type="text/ecmascript" src="js/trirand/i18n/grid.locale-es.js"></script>
    <!-- This is the Javascript file of jqGrid -->   
    <script type="text/ecmascript" src="js/trirand/jquery.jqGrid.min.js"></script>
    <!-- This is the localization file of the grid controlling messages, labels, etc.
    <!-- A link to a jQuery UI ThemeRoller theme, more than 22 built-in and many more custom -->
    <link rel="stylesheet" type="text/css" media="screen" href="css/jquery-ui.css" />
    <!-- The link to the CSS that the grid needs -->
    <link rel="stylesheet" type="text/css" media="screen" href="css/trirand/ui.jqgrid.css" />
    

    <script type="text/ecmascript" src="js/jquery.json-2.2.min.js"></script>
 
    </head>
    <body>
        
    <style type="text/css">

        /* set the size of the datepicker search control for Order Date*/
        #ui-datepicker-div { font-size:11px; }
        
        /* set the size of the autocomplete search control*/
        .ui-menu-item {
            font-size: 11px;
        }

        .ui-autocomplete { font-size: 11px; position: absolute; cursor: default;z-index:5000 !important;}      
        .my-select-loading {
            background: white url('ui-anim_basic_16x16.gif') right center no-repeat;
            height: 16px;
            width: 16px;
            margin-left: .5em;
            display: inline-block;
        }
        
        
    </style>    
    <table id="jqGrid" align="center"></table>
    <div id="jqGridPager"></div>

    <script type="text/javascript">
        
       
   
        
        
        var debaja = {false:"No",true:"Sí"};

    
    
    $(document).ready(function () {

           
           function siNoFormatter (cellvalue, options, rowObject)
              {
                   if(cellvalue == true)
                   {
                       return "Sí";
                   }
                   else
                   {
                       return "No";
                   }
              }
           

            function valorName(id)
            {
                var rowData = jQuery('#jqGrid').jqGrid ('getRowData', id);

                //console.log(rowData["taller.id"]);
                //console.log(id);
                return rowData["name"];
            }


              $("#jqGrid").jqGrid({
              url: '<?=$homesite;?>/web/app_dev.php/usuarios/roles/',
              editurl: '<?=$homesite;?>/web/app_dev.php/usuarios/roles/crear/',
              datatype: "json",
              sortname: 'id',
              sortorder: 'asc',
              mtype: 'POST',
              
            "subGridOptions": {
                "plusicon": "ui-icon-triangle-1-e",
                "minusicon": "ui-icon-triangle-1-s",
                "openicon": "ui-icon-arrowreturn-1-e"
            },
             "subGrid": true,
        "subGridRowExpanded": function(subgridid, id) {
            var data = {
                subgrid: subgridid,
                rowid: id,
                nombreRol:valorName(id)
            };
            
            $("#" + jQuery.jgrid.jqID(subgridid)).load('subGridRolPerm.php', data);
        },
              
              
               colModel: [
                { 
                  label: 'Id'
                  , name: 'id'
                  , index: 'id'
                  , width: 75 
                  , search: false
                  , hidden: false
                  , sorttype: 'number'
                  ,key: true
                },
                { 
                  label: 'Nombre'
                  , name: 'name'
                  , index: 'name'
                  , width: 50 
                  //,edittype: "textarea"
                  ,editable: true
                  ,editrules : { required: true}
                },
                   {
                       label: 'Descripción'
                       , name: 'description'
                       , index: 'description'
                       , width: 150
                       ,editable: true
                       ,editrules : { required: false,  edithidden: false}
                   }
                // sorttype is used only if the data is loaded locally or loadonce is set to true
                //{ label: 'Quantity', name: 'Quantity', width: 80, sorttype: 'number' }                   
              ],
              viewrecords: true, // show the current page, data rang and total records on the toolbar
              ondblClickRow: function(rowid) {
                   jQuery(this).jqGrid('editGridRow', rowid,
                   {
                    recreateForm:true
                    ,closeAfterEdit:true,
                     closeOnEscape:true
                    ,reloadAfterSubmit:true
                    ,width:1200
                    ,left:200
                    ,top:100
                    ,"beforeShowForm": function (form) {
                    
                        $('<a href="#">Vehículos<span class="ui-icon ui-icon-disk"></span></a>')
                            .click(function() 
                            {
                                parent.abrirVehiculos();
                            }).addClass("fm-button ui-state-default ui-corner-all fm-button-icon-left")
                              .prependTo("#Act_Buttons>td.EditButton");
                      
                       $('<tr class="FormData"><td class="CaptionTD ui-widget-content" colspan="2">' +
                       '<hr/><div style="padding:3px" class="ui-widget-header ui-corner-all">' +
                       '<b>Invice information (all about money):</b></div></td></tr>')
                       .insertBefore($('#tr_vehiculo.dominio', form));
                       //alert(form.toSource());
                      
                    }
                   });
               },
              width: 'auto',
              height:'auto',
              page:1,
              rowNum: 10,
              rowList: [10, 50, 100],
              loadonce: false, // this is just for the demo
              pager: "#jqGridPager"
            });

              $('#jqGrid').navGrid('#jqGridPager',
                // the buttons to appear on the toolbar of the grid
                //Botonera de la grilla
                { 
                  edit: true
                  , add: true //Boton de agregar
                  , del: true //Boton de eliminar
                  , search: true
                  , refresh: true
                  , view: false //Boton de ver datos
                  , position: "left"
                  , cloneToTop: false
                },
                // options for the Edit Dialog
                {
                    editCaption: "Editar Rol",
                    closeAfterEdit: true,
                    closeOnEscape:true,
                    recreateForm: true,                    
                    left:200,
                    top:100,
                    width:1200,
                    "beforeShowForm": function (form) {
                    
                        $('<a href="#">Vehículos<span class="ui-icon ui-icon-disk"></span></a>')
                            .click(function() 
                            {
                                parent.abrirVehiculos();
                            }).addClass("fm-button ui-state-default ui-corner-all fm-button-icon-left")
                              .prependTo("#Act_Buttons>td.EditButton");
                      
                       $('<tr class="FormData"><td class="CaptionTD ui-widget-content" colspan="2">' +
                       '<hr/><div style="padding:3px" class="ui-widget-header ui-corner-all">' +
                       '<b>Invice information (all about money):</b></div></td></tr>')
                       .insertBefore($('#tr_vehiculo.dominio', form));
                       //alert(form.toSource());
                      
                    },
                    "afterSubmit" : function( data, postdata, oper) 
                    {
                        var response = data.responseJSON;
                        if (response.hasOwnProperty("violations")) {
                                if(response.violations.length) 
                                {
                                        return [false,response.violations[0].message];
                                }
                        }
                        
                        if (response.hasOwnProperty("error")) {
                                if(response.error.length) 
                                {
                                        return [false,response.error];
                                }
                        }
                        
                        return [true,"",""];
                    },
                    "errorTextFormat": function (data) {
                        return 'Error: ' + data.responseText
                    }
                },
                // options for the Add Dialog
                {
                    addCaption: "Crear Rol",
                    closeAfterAdd: true,
                    recreateForm: true,
                    closeOnEscape:true,
                    width:1200,
                    left:200,
                    top:100,
                    beforeShowForm: function (form) {
                        form = $(form);                
                        $('<a href="#">Vehículos<span class="ui-icon ui-icon-disk"></span></a>')
                            .click(function() 
                            {
                                parent.abrirVehiculos();
                            }).addClass("fm-button ui-state-default ui-corner-all fm-button-icon-left")
                              .prependTo("#Act_Buttons>td.EditButton");
                      
                                     $('<tr class="FormData"><td class="CaptionTD ui-widget-content" colspan="2">' +
                       '<hr/><div style="padding:3px" class="ui-widget-header ui-corner-all">' +
                       '<b>Invice information (all about money):</b></div></td></tr>')
                       .insertBefore($('#tr_vehiculo.dominio', form));
               
                    $("tr", form).each(function() {
                        var inputs = $(">td.DataTD:has(textarea)",this);
                        if (inputs.length == 1) {
                            var tds = $(">td", this);
                            tds.eq(1).attr("colSpan", tds.length - 1);
                            tds.slice(2).hide();
                        }
                    });
                      
                    },
                    "afterSubmit" : function( data, postdata, oper) 
                    {
                        var response = data.responseJSON;
                        if (response.hasOwnProperty("violations")) {
                                if(response.violations.length) 
                                {
                                        return [false,response.violations[0].message];
                                }
                        }
                        
                        if (response.hasOwnProperty("error")) {
                                if(response.error.length) 
                                {
                                        return [false,response.error];
                                }
                        }
                        
                        return [true,"",""];
                    },
                    "errorTextFormat": function (data) {
                        return 'Error: ' + data.responseText
                    }
                },
                // options for the Delete Dailog
                {
                    url: '<?=$homesite;?>/web/app_dev.php/usuarios/roles/delete/',
                    datatype: "json",
                    "afterSubmit" : function( data, postdata, oper) 
                    {
                        var response = data.responseJSON;
                        if (response.hasOwnProperty("violations")) {
                                if(response.violations.length) 
                                {
                                        return [false,response.violations[0].message];
                                }
                        }
                        
                        if (response.hasOwnProperty("error")) {
                                if(response.error.length) 
                                {
                                        return [false,response.error];
                                }
                        }
                        
                        return [true,"",""];
                    },
                    "errorTextFormat": function (data) {
                        return 'Error: ' + data.responseText
                    }
                }
                
                );
           
    
                $('#jqGrid').jqGrid('setGridHeight',($(window).innerHeight()-80));
                $('#jqGrid').jqGrid('setGridWidth',$(window).width());
       

});

    /*
     var height = $(window).height();
            $('.ui-jqgrid-bdiv').height(height-100);
            
            var alto = $(window).width();
            $('.ui-jqgrid-bdiv').width(alto);
            alert(alto);
            
            $("#jqGrid").jqGrid({with:100000});
            */
           
           $(window).resize(function()
           {
            $('#jqGrid').jqGrid('setGridHeight',($(window).innerHeight()-80));
            $('#jqGrid').jqGrid('setGridWidth',($(window).width()-20));
           });


</script>
<script type="text/ecmascript" src="js/jquery-ui.min.js"></script>
<script>
        $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '<Ant',
        nextText: 'Sig>',
        currentText: 'Hoy',
        numberOfMonths: 3,
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
        dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
        };
        $.datepicker.setDefaults($.datepicker.regional['es']);
</script>
    </body>
</html>