<?PHP
    include_once("config.php");
?>
<!DOCTYPE html>
<html>
    <head>
      <!-- The jQuery library is a prerequisite for all jqSuite products -->
      <script type="text/ecmascript" src="js/jquery.min.js"></script> 
      <!-- We support more than 40 localizations -->
      <script type="text/ecmascript" src="js/trirand/i18n/grid.locale-es.js"></script>
      <!-- This is the Javascript file of jqGrid -->   
      <script type="text/ecmascript" src="js/trirand/jquery.jqGrid.min.js"></script>
      <!-- This is the localization file of the grid controlling messages, labels, etc.
      <!-- A link to a jQuery UI ThemeRoller theme, more than 22 built-in and many more custom -->
      <link rel="stylesheet" type="text/css" media="screen" href="css/jquery-ui.css" />
      <!-- The link to the CSS that the grid needs -->
      <link rel="stylesheet" type="text/css" media="screen" href="css/trirand/ui.jqgrid.css" />
      <script type="text/ecmascript" src="js/jquery.json-2.2.min.js"></script>
    </head>

    <body>
        
    <style type="text/css">

        /* set the size of the datepicker search control for Order Date*/
        #ui-datepicker-div { font-size:11px; }
        
        /* set the size of the autocomplete search control*/
        .ui-menu-item {
            font-size: 11px;
        }

        .ui-autocomplete { font-size: 11px; position: absolute; cursor: default;z-index:5000 !important;}      
    </style>
        
    <table id="jqGrid" align="center"></table>
    <div id="jqGridPager"></div>

    <script type="text/javascript">

    var debaja = {true:"Si",false:"No"};

    $(document).ready(function () {

              function siNoFmatter (cellvalue, options, rowObject)
              {
                   if(cellvalue)
                   {
                       return "No";
                   }
                   else
                   {
                       return "Si";
                   }
              }

              function noSiFmatter (cellvalue, options, rowObject)
              {
                   if(cellvalue)
                   {
                       return "Si";
                   }
                   else
                   {
                       return "No";
                   }
              }

              $("#jqGrid").jqGrid({
              url: '<?=$homesite;?>/web/app_dev.php/marca/search/',
              editurl: '<?=$homesite;?>/web/app_dev.php/marca/crear/',
              datatype: "json",
              sortname: 'id',
              sortorder: 'asc',
              datatype: "json",
              mtype: 'POST',
              viewrecords: true,
               colModel: [
                { 
                  label: 'Id'
                  ,name: 'id'
                  ,index: 'id'
                  ,width: 75 
                  ,search: false
                  ,hidden: false
                  ,sorttype: 'number'
                  ,key: true
                },
                { 
                  label: 'Descripción'
                  ,name: 'descripcion'
                  ,index: 'descripcion'
                  ,width: 255 
                  ,editable: true
                  ,editrules : { required: false}
                },
                { 
                  label: 'De Baja'
                  ,name: 'debaja'
                  ,index: 'debaja'
                  ,width: 50 
                  ,editable: true
                  ,hidden:false
                  ,edittype: 'select'
                  ,stype: 'select'
                  ,formatter:noSiFmatter
                  ,editoptions: { value: debaja , defaultValue:"No"}
                  ,searchoptions: { value: debaja ,sopt: ['eq','ne']}
                  ,editrules : { required: true,edithidden: true}
                }
                // sorttype is used only if the data is loaded locally or loadonce is set to true
              ],
              viewrecords: true, // show the current page, data rang and total records on the toolbar
              width: 'auto',
              height:'auto',
              page:1,
              rowNum: 10,
              rowList: [10, 50, 100],
              loadonce: false, // this is just for the demo
              pager: "#jqGridPager"
            });

              $('#jqGrid').navGrid('#jqGridPager',
                // the buttons to appear on the toolbar of the grid
                //Botonera de la grilla
                { 
                  edit: true
                  , add: true //Boton de agregar
                  , del: false //Boton de eliminar
                  , search: true
                  , refresh: true
                  , view: false //Boton de ver datos
                  , position: "left"
                  , cloneToTop: false 
                },
                // options for the Edit Dialog
                {
                    editCaption: "Editar Marca",
                    recreateForm: true,
                    closeAfterEdit: true,
                    closeOnEscape:true,
                    "afterSubmit" : function( data, postdata, oper) 
                    {
                        var response = data.responseJSON;
                        if (response.hasOwnProperty("violations")) {
                                if(response.violations.length) 
                                {
                                        return [false,response.violations[0].message];
                                }
                        }
                        
                        if (response.hasOwnProperty("error")) {
                                if(response.error.length) 
                                {
                                        return [false,response.error];
                                }
                        }
                        
                        return [true,"",""];
                    },
                    "errorTextFormat": function (data) {
                        return 'Error: ' + data.responseText
                    }
                },
                // options for the Add Dialog
                {
                    addCaption: "Crear Marca",
                    closeAfterAdd: true,
                    recreateForm: true,
                    closeOnEscape:true,
                    "afterSubmit" : function( data, postdata, oper) 
                    {
                        var response = data.responseJSON;
                        if (response.hasOwnProperty("violations")) {
                                if(response.violations.length) 
                                {
                                        return [false,response.violations[0].message];
                                }
                        }
                        
                        if (response.hasOwnProperty("error")) {
                                if(response.error.length) 
                                {
                                        return [false,response.error];
                                }
                        }
                        
                        return [true,"",""];
                    },
                    "errorTextFormat": function (data) {
                        return 'Error: ' + data.responseText
                    }
                },
                // options for the Delete Dailog
                {
                    url: '0003B.php',
                    datatype: "json",
                   
                    errorTextFormat: function (data) {
                        return 'Error: ' + data.responseText
                    }
                }
                
                );

                $('#jqGrid').jqGrid('setGridHeight',($(window).innerHeight()-80));
                $('#jqGrid').jqGrid('setGridWidth',$(window).width() - 20);
});

             

           $(window).resize(function()
           {
                $('#jqGrid').jqGrid('setGridHeight',($(window).innerHeight()-80));
                $('#jqGrid').jqGrid('setGridWidth',($(window).width()-20));
    
            });
</script>
<script type="text/ecmascript" src="js/jquery-ui.min.js"></script>
</body>
</html>