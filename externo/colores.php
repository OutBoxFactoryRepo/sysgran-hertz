<?PHP
    include_once("config.php");
?>
<!DOCTYPE html>
<html>
    <head>
      <!-- The jQuery library is a prerequisite for all jqSuite products -->
      <script type="text/ecmascript" src="js/jquery.min.js"></script> 
      <!-- We support more than 40 localizations -->
      <script type="text/ecmascript" src="js/trirand/i18n/grid.locale-es.js"></script>
      <!-- This is the Javascript file of jqGrid -->   
      <script type="text/ecmascript" src="js/trirand/jquery.jqGrid.min.js"></script>
      <!-- This is the localization file of the grid controlling messages, labels, etc.
      <!-- A link to a jQuery UI ThemeRoller theme, more than 22 built-in and many more custom -->
      <link rel="stylesheet" type="text/css" media="screen" href="css/jquery-ui.css" />
      <!-- The link to the CSS that the grid needs -->
      <link rel="stylesheet" type="text/css" media="screen" href="css/trirand/ui.jqgrid.css" />
      <script type="text/ecmascript" src="js/jquery.json-2.2.min.js"></script>

      <!-- Color picker -->
      <link rel="stylesheet" media="screen" type="text/css" href="js/colorpicker/css/colorpicker.css" />
      <script type="text/javascript" src="js/colorpicker/js/colorpicker.js"></script>
    </head>

    <body>
        
    <style type="text/css">

        /* set the size of the datepicker search control for Order Date*/
        #ui-datepicker-div { font-size:11px; }
        
        /* set the size of the autocomplete search control*/
        .ui-menu-item {
            font-size: 11px;
        }

        .ui-autocomplete { font-size: 11px; position: absolute; cursor: default;z-index:5000 !important;}     

        .colorpicker { z-index: 1500 !important; } 
    </style>
        
    <table id="jqGrid" align="center"></table>
    <div id="jqGridPager"></div>

    <script type="text/javascript">

    var validateColor = function(value, column)
    {
      var re = /[0-9A-Fa-f]{6}/g;
      if ( value.startsWith("#") && re.test(value.substr(1)))
      {
        return [true, ""];
      }
      else
      {
        return [false, "Color no válido"];
      }
    }

    var debaja = {true:"Si",false:"No"};

    $(document).ready(function () {

              function siNoFmatter (cellvalue, options, rowObject)
              {
                   if(cellvalue)
                   {
                       return "No";
                   }
                   else
                   {
                       return "Si";
                   }
              }

              function noSiFmatter (cellvalue, options, rowObject)
              {
                   if(cellvalue)
                   {
                       return "Si";
                   }
                   else
                   {
                       return "No";
                   }
              }

              $("#jqGrid").jqGrid({
              url: '<?=$homesite;?>/web/app_dev.php/colores/search/',
              editurl: '<?=$homesite;?>/web/app_dev.php/colores/crear/',
              datatype: "json",
              sortname: 'id',
              sortorder: 'asc',
              datatype: "json",
              mtype: 'POST',
              viewrecords: true,
              colModel: [
                { 
                  label: 'Id'
                  ,name: 'id'
                  ,index: 'id'
                  ,width: 75 
                  ,search: false
                  ,hidden: false
                  ,sorttype: 'number'
                  ,key: true
                },
                { 
                  label: 'Color'
                  ,name: 'color'
                  ,index: 'color'
                  ,width: 100 
                  ,editable: true
                  ,editrules : { 
                    required: true, 
                    custom_func: validateColor,
                    custom: true
                  },
                  editoptions: {
                    dataInit: function (element) {
                      $('<input type="text" />').attr({
                          id: 'colorDiv',
                          name: 'colorDiv',
                          enabled: 'false',
                          class: 'ui-corner-all',
                          style: 'width:15px; margin-left:2px; background-color:'+$(element).val() + ';',
                      }).appendTo($(element).parent());

                      $(element).ColorPicker({
                        color: '#000000',
                        onChange: function (hsb, hex, rgb) {
                          $("#color").val('#' + hex);
                          $("#colorDiv").css('backgroundColor', '#' + hex);
                        },
                      });
                    }
                  }
                },
                { 
                  label: 'Descripción'
                  ,name: 'descripcion'
                  ,index: 'descripcion'
                  ,width: 255 
                  ,editable: true
                  ,editrules : { required: false}
                }
                // sorttype is used only if the data is loaded locally or loadonce is set to true
              ],
              viewrecords: true, // show the current page, data rang and total records on the toolbar
              width: 'auto',
              height:'auto',
              page:1,
              rowNum: 10,
              rowList: [10, 50, 100],
              loadonce: false, // this is just for the demo
              pager: "#jqGridPager"
            });

              $('#jqGrid').navGrid('#jqGridPager',
                // the buttons to appear on the toolbar of the grid
                //Botonera de la grilla
                { 
                  edit: true
                  , add: true //Boton de agregar
                  , del: false //Boton de eliminar
                  , search: true
                  , refresh: true
                  , view: false //Boton de ver datos
                  , position: "left"
                  , cloneToTop: false 
                },
                // options for the Edit Dialog
                {
                    editCaption: "Editar Color",
                    recreateForm: true,
                    closeAfterEdit: true,
                    closeOnEscape:true,
                    "afterSubmit" : function( data, postdata, oper) 
                    {
                        var response = data.responseJSON;
                        if (response.hasOwnProperty("violations")) {
                                if(response.violations.length) 
                                {
                                        return [false,response.violations[0].message];
                                }
                        }
                        
                        if (response.hasOwnProperty("error")) {
                                if(response.error.length) 
                                {
                                        return [false,response.error];
                                }
                        }
                        
                        return [true,"",""];
                    },
                    "errorTextFormat": function (data) {
                        return 'Error: ' + data.responseText
                    }
                },
                // options for the Add Dialog
                {
                    addCaption: "Crear Color",
                    closeAfterAdd: true,
                    recreateForm: true,
                    closeOnEscape:true,
                    "afterSubmit" : function( data, postdata, oper) 
                    {
                        var response = data.responseJSON;
                        if (response.hasOwnProperty("violations")) {
                                if(response.violations.length) 
                                {
                                        return [false,response.violations[0].message];
                                }
                        }
                        
                        if (response.hasOwnProperty("error")) {
                                if(response.error.length) 
                                {
                                        return [false,response.error];
                                }
                        }
                        
                        return [true,"",""];
                    },
                    "errorTextFormat": function (data) {
                        return 'Error: ' + data.responseText
                    }
                },
                // options for the Delete Dailog
                {
                    url: '0003B.php',
                    datatype: "json",
                   
                    errorTextFormat: function (data) {
                        return 'Error: ' + data.responseText
                    }
                }
                
                );

                $('#jqGrid').jqGrid('setGridHeight',($(window).innerHeight()-80));
                $('#jqGrid').jqGrid('setGridWidth',$(window).width() - 20);



});

             

           $(window).resize(function()
           {
                $('#jqGrid').jqGrid('setGridHeight',($(window).innerHeight()-80));
                $('#jqGrid').jqGrid('setGridWidth',($(window).width()-20));
    
            });
</script>
<script type="text/ecmascript" src="js/jquery-ui.min.js"></script>
</body>
</html>