<?php
include("config.php");
$subgrid = $_POST["subgrid"];
$rowId = $_POST["rowid"];


//echo $rowId;die();
//print_r($_REQUEST);die();
//$tabla = "<table><tbody><tr><td><b>First Name</b></td><td>Janet</td><td rowspan='9' valign='top'><img src='images/3.jpg'/></td></tr><tr><td><b>Last Name</b></td><td>Leverling</td></tr><tr><td><b>Title</b></td><td>Sales Representative</td></tr><tr><td><b>Title of Courtesy</b></td><td>Ms.</td></tr><tr><td><b>Birth Date</b></td><td>1963-08-30 00:00:00</td></tr><tr><td><b>Hire Date</b></td><td>1992-04-01 00:00:00</td></tr><tr><td><b>Address</b></td><td>722 Moss Bay Blvd.</td></tr><tr><td><b>City</b></td><td>Kirkland</td></tr><tr><td><b>Postal Code</b></td><td>98033</td></tr></tbody></table>";
//echo $tabla;
?>
<style>
    .ui-jqgrid .subgrid-data .ui-th-column { background: #819FF7 }
</style>
<h1>Otros datos del Cliente</h1>
<table id='<?=$subgrid;?>_t'></table>
<div id='<?=$subgrid;?>_t_p'>
    
</div>
<script type='text/javascript'>
    
    
    
       
        function cargarCondIva()
        {
            var valoresx = "";
            $.ajax({
                                data: {},
                                type: "POST",
                                dataType: "json",
                                url: "<?=$homesite;?>/web/app_dev.php/condicioniva/search/",
                                async:false,
                                success: function(data)
                                {


                                                        var arrayValores = data.rows;

                                                        valoresx ="{";
                                                        $.each( arrayValores, function ( userkey, uservalue) 
                                                        {
                                                            if(valoresx == "{")
                                                            {
                                                                //console.log(userkey + ': ' + uservalue.id + ': ' + uservalue.descripcion );
                                                                valoresx += "\"" + uservalue.id + "\":\"" + uservalue.destalle +"\"";
                                                            }
                                                            else
                                                            {
                                                                valoresx += "," + "\"" + uservalue.id + "\":\"" + uservalue.destalle+"\"";
                                                            }
                                                        });
                                                        valoresx +="}";
                                                        //valores = data;
                                                        //$.parseJSON(valores);
                
                                                        return valoresx;
                                },
                                error: function(xhr, textStatus, errorThrown) {
                                    // Handle error
                                    alert(errorThrown);
                                    //$.unblockUI(); 
                                  }
                        });
              return valoresx;
       }
       
       
       function cargarTiposIIBB()
        {
            var valoresx = "";
            $.ajax({
                                data: {},
                                type: "POST",
                                dataType: "json",
                                url: "<?=$homesite;?>/web/app_dev.php/tiposiibb/search/",
                                async:false,
                                success: function(data)
                                {


                                                        var arrayValores = data.rows;

                                                        valoresx ="{";
                                                        $.each( arrayValores, function ( userkey, uservalue) 
                                                        {
                                                            if(valoresx == "{")
                                                            {
                                                                //console.log(userkey + ': ' + uservalue.id + ': ' + uservalue.descripcion );
                                                                valoresx += "\"" + uservalue.id + "\":\"" + uservalue.descripcion+"\"";
                                                            }
                                                            else
                                                            {
                                                                valoresx += "," + "\"" + uservalue.id + "\":\"" + uservalue.descripcion+"\"";
                                                            }
                                                        });
                                                        valoresx +="}";
                                                        //valores = data;
                                                        //$.parseJSON(valores);
                
                                                        return valoresx;
                                },
                                error: function(xhr, textStatus, errorThrown) {
                                    // Handle error
                                    alert(errorThrown);
                                    //$.unblockUI(); 
                                  }
                        });
              return valoresx;
       }
       
       

    var condIva = JSON.parse(cargarCondIva());
    var tiposIIBB = JSON.parse(cargarTiposIIBB());
       
    var onDbl = {
                  
            };
    
    //atención los parámetros de la subgrilla van por get en symfony al recibir los parámetros
    
    jQuery(document).ready(function($) 
    {
        
        
        
        jQuery('#<?=$subgrid;?>_t').jqGrid({
            "hoverrows": false,
            "viewrecords": true,
            pginput: false,
            pgbuttons: false,
            
            "jsonReader": {
                "repeatitems": false,
                "subgrid": {
                    "repeatitems": false
                }
            },
            "xmlReader": {
                "repeatitems": false,
                "subgrid": {
                    "repeatitems": false
                }
            },
            "gridview": true,
            "url": "<?=$homesite;?>/web/app_dev.php/clienteod/getone/?id=<?=$rowId;?>",
            "editurl": "<?=$homesite;?>/web/app_dev.php/clienteod/crear/?idUser=<?=$rowId;?>",
            "cellurl": "subReserva.php",
            "width": 540,
            "rowNum": 10,
            "sortname": "id",
            "height": 110,
            "postData": {
                "subgrid": "<?=$subgrid;?>",
                "rowIdCliOd": "<?=$rowId;?>",
                "ot": "<?=$rowId;?>",
                "id": "<?=$rowId;?>",
                "oper": "grid"
            },
            "datatype": "json",
            
              ondblClickRow: function(rowIdCliOd) {
                   jQuery('#<?=$subgrid;?>_t').jqGrid('editGridRow', rowIdCliOd,
                   {
                    editCaption: "Editar Dato",
                    recreateForm: true,
                    closeAfterEdit: true,
                    closeOnEscape:true,
                    "afterSubmit" : function( data, postdata, oper) 
                    {
                        var response = data.responseJSON;
                        if (response.hasOwnProperty("violations")) {
                                if(response.violations.length) 
                                {
                                        return [false,response.violations[0].message];
                                }
                        }
                        
                        if (response.hasOwnProperty("error")) {
                                if(response.error.length) 
                                {
                                        return [false,response.error];
                                }
                        }
                        return [true,"",""];
                    },
                    "errorTextFormat": function (data) {
                        return 'Error: ' + data.responseText;
                    }
                   });
                   return false;
               },
            
            "colModel": [ { 
                  label: 'Id'
                  , name: 'id'
                  , index: 'id'
                  , width: 25 
                  , search: false
                  , hidden: false
                  , sorttype: 'number'
                  ,key: true
                },{
                "label": "Licencia",
                "name": "licencia",
                "index": "licencia",
                "sorttype": "string",
                "editable": true
                }
                ,{
                "label": "Fec. Vto. Licencia",
                "name": "lic_fec_vto",
                "index": "lic_fec_vto"
                , width: 100
                ,editable: true
                ,edittype: 'text'
                ,sorttype: 'date', datefmt:'d/mm/Y'
                ,formatter: function (cellvalue, options, rowObject) {
                  var y = Number(cellvalue.substring(0,4));
                  var m = Number(cellvalue.substring(5,7));
                  var d = Number(cellvalue.substring(8,10));
                  return String(d)+'/'+String(m)+'/'+String(y);
                }
                ,editoptions: {
                          // dataInit is the client-side event that fires upon initializing the toolbar search field for a column
                          // use it to place a third party control to customize the toolbar
                          dataInit: function (element) {
                              $(element).datepicker({
                                  id: 'orderDate_datePicker',
                                  dateFormat: 'd/m/yy',
                                  //minDate: new Date(2010, 0, 1),
                                  //maxDate: new Date(2020, 0, 1),
                                  minDate: 30,
                                  showOn: 'focus'
                              });
                          }
                      }
                ,searchoptions: {
                          sopt : ['eq','ne', 'lt', 'gt'],
                          // dataInit is the client-side event that fires upon initializing the toolbar search field for a column
                          // use it to place a third party control to customize the toolbar
                          dataInit: function (element) {
                              $(element).datepicker({
                                  id: 'orderDate_datePicker',
                                  dateFormat: 'd/m/yy',
                                  //minDate: new Date(2010, 0, 1),
                                  //maxDate: new Date(2020, 0, 1),
                                  showOn: 'focus'
                              });
                          }
                      }
                 } 
                 ,{
                "label": "Nro. Tarj. Crédito",
                "name": "nro_tarjeta_cre",
                "index": "nro_tarjeta_cre",
                "sorttype": "string",
                "editable": true
                }
                ,{
                "label": "Fec. Vto. Tarjeta",
                "name": "tarj_fec_vto",
                "index": "tarj_fec_vto"
                , width: 100
                ,editable: true
                ,	editrules: {
                        custom: true,
                        custom_func: function(value) {
                           
                            mesAnioVto = value.split('/');
                            
                            if(mesAnioVto.length == 2)
                            {
                                if(isNaN(mesAnioVto[0]))
                                {
                                	return [false, "Mes no válido"];
                                }

                                if(isNaN(mesAnioVto[1]))
                                {
                                	return [false, "Año no válido"];
                                }
                                
                                if(parseInt(mesAnioVto[0]) > 12)
                                {
                                	return [false, "Mes no válido"];
                                }

                                if(parseInt(mesAnioVto[1]) < 2015)
                                {
                                	return [false, "Año no válido"];
                                }
                                return [true, ""];
                            }
                            else
                            {
                            	return [false, "Formato de fecha mm/yyyy"];
                            }
                        }
                    }
                ,edittype: 'text'
                	,formatter: function (cellvalue, options, rowObject) {
						
                        var m = Number(cellvalue.substring(0,2));
                        var y = Number(cellvalue.substring(3,7));
                        mesAnioVto = cellvalue.split('/');
                        
                        if(mesAnioVto.length == 2)
                        {

                        	m = parseInt(mesAnioVto[0]);
                            y = parseInt(mesAnioVto[1]);
                        	
                        }
                        else
                        {
                        	
                        	mesAnioVto = cellvalue.split('-');
                            m = parseInt(mesAnioVto[1]);
                            y = parseInt(mesAnioVto[0]);
                        }

                        return String(m)+'/'+String(y);
                        
                      }
                  }
                  ,{
                "label": "CUIT",
                "name": "cuit",
                "index": "cuit",
                "sorttype": "string",
                "editable": true
                },
                { 
                  label: 'Cond. IVA'
                  , name: 'condicion_iva.destalle'
                  , index: 'condicion_iva.destalle'
                  , width: 100 
                  ,editable: true
                  ,edittype: 'select'
                  ,stype: 'select'
                  ,editoptions: 
                              {
                                 value:condIva, defaultValue:9
                              }
                  ,searchoptions: 
                              {
                                 value:condIva
                                ,sopt: ['eq','ne']
                              }
                  ,editrules : { required: true}
                },
                { 
                  label: 'Tipo IIBB'
                  , name: 'id_i_i_b_b.descripcion'
                  , index: 'id_i_i_b_b.descripcion'
                  , width: 100 
                  ,editable: true
                  ,edittype: 'select'
                  ,stype: 'select'
                  ,editoptions: 
                              {
                                 value:tiposIIBB
                              }
                  ,searchoptions: 
                              {
                                 value:tiposIIBB
                                ,sopt: ['eq','ne']
                              }
                  ,editrules : { required: true}
                },{
                "label": "Nro. IIBB",
                "name": "nro_i_i_b_b",
                "index": "nro_i_i_b_b",
                "sorttype": "string",
                "editable": true
                }  
                ],
            "prmNames": {
                "page": "page",
                "rows": "rows",
                "sort": "sidx",
                "order": "sord",
                "search": "_search",
                "nd": "nd",
                "id": "id",
                "filter": "filters",
                "searchField": "searchField",
                "searchOper": "searchOper",
                "searchString": "searchString",
                "oper": "oper",
                "query": "grid",
                "addoper": "add",
                "editoper": "edit",
                "deloper": "del",
                "excel": "excel",
                "subgrid": "subgrid",
                "totalrows": "totalrows",
                "autocomplete": "autocmpl"
            },
            "loadError": function(xhr, status, err) {
                try {
                    jQuery.jgrid.info_dialog(jQuery.jgrid.errors.errcap, '<div class="ui-state-error">' + xhr.responseText + '</div>', jQuery.jgrid.edit.bClose, {
                        buttonalign: 'right'
                    });
                } catch (e) {
                    alert(xhr.responseText);
                }
            },
            "pager": "#<?=$subgrid;?>_t_p"
        });
        jQuery('#<?=$subgrid;?>_t').jqGrid('navGrid', '#<?=$subgrid;?>_t_p', {
            "edit": true,
            "add": true,
            "del": true,
            "search": false,
            "refresh": true,
            "view": true,
            "excel": true,
            "pdf": true,
            "csv": true,
            "columns": true
        }, 
        //edit option
        {
            "drag": true,
            "resize": true,
            "closeOnEscape": true,
            //"dataheight": 150,
            "closeAfterEdit":true,
            "afterSubmit" : function( data, postdata, oper) 
            {
                var response = data.responseJSON;
                if (response.hasOwnProperty("violations")) {
                        if(response.violations.length) 
                        {
                                return [false,response.violations[0].message];
                        }
                }

                if (response.hasOwnProperty("error")) {
                        if(response.error.length) 
                        {
                                return [false,response.error];
                        }
                }
                return [true,"",""];
            },
            "errorTextFormat": function (data) {
                return 'Error: ' + data.responseText
            },
            "beforeShowForm": function(form) 
            { 
                //$('#tr_nombre', form).hide(); 
                //$('#tr_apellido', form).hide(); 000
                //$('#tr_fecha_nac', form).hide(); 
                //$("#<?=$subgrid;?>_t").jqGrid('setColProp', 'ot.id', {editrules: {required: false}});
            }
            
                   
        }, 
        //create option
        {
            "drag": true,
            "resize": true,
            "closeOnEscape": true,
            //"dataheight": 150,
            "closeAfterAdd": true,
            "afterSubmit" : function( data, postdata, oper) 
            {
                var response = data.responseJSON;
                if (response.hasOwnProperty("violations")) {
                        if(response.violations.length) 
                        {
                                return [false,response.violations[0].message];
                        }
                }

                if (response.hasOwnProperty("error")) {
                        if(response.error.length) 
                        {
                                return [false,response.error];
                        }
                }
                return [true,"",""];
            },
            "errorTextFormat": function (data) {
                return 'Error: ' + data.responseText
            }
        }
        ,
        // options for the Delete Dailog
        {
            url: '<?=$homesite;?>/web/app_dev.php/clienteod/eliminar/?idUser=<?=$rowId;?>',
            datatype: "json",
            "afterSubmit" : function( data, postdata, oper) {
                    var response = data.responseJSON;
                    if (response.hasOwnProperty("error")) {
                            if(response.error.length) {
                                    return [false,response.error ];
                            }
                    }
                    return [true,"",""];
            },
            "errorTextFormat": function (data) {
                return 'Error: ' + data.responseText
            }
        }
        , {
            "drag": true,
            "closeAfterSearch": true,
            "multipleSearch": true
        }, {
            "drag": true,
            "resize": true,
            "closeOnEscape": true,
            "dataheight": 150
        });
    });
    
    
    
    $('#<?=$subgrid;?>_t').jqGrid('setGridWidth',$(window).width()-80);

    
    $(window).resize(function()
           {
            $('#<?=$subgrid;?>_t').jqGrid('setGridWidth',$(window).width()-80);
     });
</script>


<!--
<h1>Cargos Adicionales</h1>
<table id='<?=$subgrid;?>_t_2'></table>
<div id='<?=$subgrid;?>_t_p_2'>
    
</div>
<script type='text/javascript'>
    jQuery(document).ready(function($) {
        jQuery('#<?=$subgrid;?>_t_2').jqGrid({
            "hoverrows": false,
            "viewrecords": true,
            "jsonReader": {
                "repeatitems": false,
                "subgrid": {
                    "repeatitems": false
                }
            },
            "xmlReader": {
                "repeatitems": false,
                "subgrid": {
                    "repeatitems": false
                }
            },
            "gridview": true,
            "url": "subReserva.php",
            "editurl": "subReserva.php",
            "cellurl": "subReserva.php",
            "width": 540,
            "rowNum": 10,
            "sortname": "OrderID",
            "height": 110,
            "postData": {
                "subgrid": "grid_ANATR",
                "rowid": "ANATR",
                "oper": "grid"
            },
            "datatype": "json",
            "colModel": [{
                "name": "OrderID",
                "index": "OrderID",
                "sorttype": "int",
                "key": true,
                "editable": true
            }, {
                "name": "RequiredDate",
                "index": "RequiredDate",
                "sorttype": "datetime",
                "formatter": "date",
                "formatoptions": {
                    "srcformat": "Y-m-d H:i:s",
                    "newformat": "m\/d\/Y"
                },
                "search": false,
                "editable": true
            }, {
                "name": "ShipName",
                "index": "ShipName",
                "sorttype": "string",
                "editable": true
            }, {
                "name": "ShipCity",
                "index": "ShipCity",
                "sorttype": "string",
                "editable": true
            }, {
                "name": "Freight",
                "index": "Freight",
                "sorttype": "numeric",
                "editable": true
            }],
            "prmNames": {
                "page": "page",
                "rows": "rows",
                "sort": "sidx",
                "order": "sord",edit
                "search": "_search",
                "nd": "nd",
                "id": "id",
                "filter": "filters",
                "searchField": "searchField",
                "searchOper": "searchOper",
                "searchString": "searchString",
                "oper": "oper",
                "query": "grid",
                "addoper": "add",
                "editoper": "edit",
                "deloper": "del",
                "excel": "excel",
                "subgrid": "subgrid",
                "totalrows": "totalrows",
                "autocomplete": "autocmpl"
            },
            "loadError": function(xhr, status, err) {
                try {
                    jQuery.jgrid.info_dialog(jQuery.jgrid.errors.errcap, '<div class="ui-state-error">' + xhr.responseText + '</div>', jQuery.jgrid.edit.bClose, {
                        buttonalign: 'right'
                    });
                } catch (e) {
                    alert(xhr.responseText);
                }
            },
            "pager": "#<?=$subgrid;?>_t_p_2"
        });
        jQuery('#<?=$subgrid;?>_t').jqGrid('navGrid', '#<?=$subgrid;?>_t_p_2', {
            "edit": false,
            "add": false,
            "del": false,
            "search": true,
            "refresh": true,
            "view": false,
            "excel": false,
            "pdf": false,
            "csv": false,
            "columns": false
        }, {
            "drag": true,
            "resize": true,
            "closeOnEscape": true,
            "dataheight": 150,
            "errorTextFormat": function(r) {
                return r.responseText;
            }
        }, {
            "drag": true,
            "resize": true,
            "closeOnEscape": true,
            "dataheight": 150,
            "errorTextFormat": function(r) {
                return r.responseText;
            }
        }, {
            "errorTextFormat": function(r) {
                return r.responseText;
            }
        }, {
            "drag": true,
            "closeAfterSearch": true,
            "multipleSearch": true
        }, {
            "drag": true,
            "resize": true,
            "closeOnEscape": true,
            "dataheight": 150
        });
    });
    
    $('#<?=$subgrid;?>_t_2').jqGrid('setGridWidth',$(window).width()-80);
</script>

-->