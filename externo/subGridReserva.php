<?php
include("config.php");
$subgrid = $_POST["subgrid"];
$rowId = $_POST["rowid"];
$lockeado = (bool)$_POST["lockeado"];

$mesActual = date("m");
$anioActual = date("Y");


if($lockeado)
{
    $lockeado = "true";
}
else
{
    $lockeado = "false";
}
//print_r($_REQUEST);die();
//$tabla = "<table><tbody><tr><td><b>First Name</b></td><td>Janet</td><td rowspan='9' valign='top'><img src='images/3.jpg'/></td></tr><tr><td><b>Last Name</b></td><td>Leverling</td></tr><tr><td><b>Title</b></td><td>Sales Representative</td></tr><tr><td><b>Title of Courtesy</b></td><td>Ms.</td></tr><tr><td><b>Birth Date</b></td><td>1963-08-30 00:00:00</td></tr><tr><td><b>Hire Date</b></td><td>1992-04-01 00:00:00</td></tr><tr><td><b>Address</b></td><td>722 Moss Bay Blvd.</td></tr><tr><td><b>City</b></td><td>Kirkland</td></tr><tr><td><b>Postal Code</b></td><td>98033</td></tr></tbody></table>";
//echo $tabla;
?>
<style>
    .ui-jqgrid .subgrid-data .ui-th-column { background: #819FF7 }
    
    
    fieldset {
    width:95%;
    display: block;
    margin-left: 2px;
    margin-right: 2px;
    padding-top: 0.35em;
    padding-bottom: 0.625em;
    padding-left: 0.75em;
    padding-right: 0.75em;
    border: 2px groove (internal value);
} 

.bordeTablaAdic {
    border: 1px solid black;
    border-collapse: collapse;
}

.bordeTablaAdic td {
    border: 1px solid black;
    border-collapse: collapse;
}

.bordeTablaAdic th {
    border: 1px solid black;
    border-collapse: collapse;
}

</style>

<script type="text/javascript">
        /***************/
        //Variables de usuario
        /***************/
        $.blockUI({message: '<h1>Cargando datos ...</h1>', css: { 
                                                    border: 'none', 
                                                    padding: '15px', 
                                                    backgroundColor: '#000', 
                                                    '-webkit-border-radius': '10px', 
                                                    '-moz-border-radius': '10px', 
                                                    opacity: .5, 
                                                    color: '#fff' 
                                                } });  
        var idReserva = <?=$rowId;?>;

        var valorDiaria = 0;
        var cantidadDiaria = 0;
        var cantidadDiariaGuardado = 0;
        var valorDiariaGuardado = 0;
        var contrato = false;
        
        
        function cargarAgenciasTodas()
        {
            var valoresx = "";
            $.ajax({
                                data: {},
                                type: "POST",
                                dataType: "json",
                                url: "<?=$homesite;?>/web/app_dev.php/agencia/searchcomboview/",
                                async:false,
                                success: function(data)
                                {
                                    var arrayValores = data;

                                    valoresx ="{";
                                    $.each( arrayValores, function ( userkey, uservalue) 
                                    {
                                        if(valoresx == "{")
                                        {
                                            //console.log(userkey + ': ' + uservalue.id + ': ' + uservalue.descripcion );
                                            valoresx += "\"" + uservalue.id + "\":\"" + uservalue.descripcion+"\"";
                                        }
                                        else
                                        {
                                            valoresx += "," + "\"" + uservalue.id + "\":\"" + uservalue.descripcion+"\"";
                                        }
                                    });
                                    valoresx +="}";
                                    //valores = data;
                                    //$.parseJSON(valores);
                                    $.unblockUI();
                                    return valoresx;
                                },
                                error: function(xhr, textStatus, errorThrown) 
                                {
                                    // Handle error
                                    alert(errorThrown);
                                    //$.unblockUI(); 
                                }
                        });
              return valoresx;
       }
       
        function cargarAgencias()
        {
            var valoresx = "";
            $.ajax({
                                data: {},
                                type: "POST",
                                dataType: "json",
                                url: "<?=$homesite;?>/web/app_dev.php/agencia/searchcombo/",
                                async:false,
                                success: function(data)
                                {
                                    var arrayValores = data;

                                    valoresx ="{";
                                    $.each( arrayValores, function ( userkey, uservalue) 
                                    {
                                        if(valoresx == "{")
                                        {
                                            //console.log(userkey + ': ' + uservalue.id + ': ' + uservalue.descripcion );
                                            valoresx += "\"" + uservalue.id + "\":\"" + uservalue.descripcion+"\"";
                                        }
                                        else
                                        {
                                            valoresx += "," + "\"" + uservalue.id + "\":\"" + uservalue.descripcion+"\"";
                                        }
                                    });
                                    valoresx +="}";
                                    //valores = data;
                                    //$.parseJSON(valores);
                                    $.unblockUI();
                                    return valoresx;
                                },
                                error: function(xhr, textStatus, errorThrown) 
                                {
                                    // Handle error
                                    alert(errorThrown);
                                    //$.unblockUI(); 
                                }
                        });
              return valoresx;
       }
       
       
       function cargarCategoria(id)
        {
            var valoresx = 0;
            $.ajax({
                                data: {"id":id},
                                type: "GET",
                                dataType: "json",
                                async:false,
                                url: "<?=$homesite;?>/web/app_dev.php/categoriasveh/search/"+id+"/",
                                async:false,
                                success: function(data)
                                {
                                    var arrayValores = data;


                                    valoresx = data[0].valor;

                                    return valoresx;
                                },
                                error: function(xhr, textStatus, errorThrown) 
                                {
                                    // Handle error
                                    alert(errorThrown);
                                    //$.unblockUI(); 
                                }
                        });
              
              return valoresx;
       }
       function cargarAdicional(tipo)
        {
            
            
            var valoresx = "";
            $.ajax({
                                data: {"tipo":tipo},
                                type: "POST",
                                dataType: "json",
                                url: "<?=$homesite;?>/web/app_dev.php/adicional/searchcontrato/",
                                async:false,
                                success: function(data)
                                {
                                    var arrayValores = data.rows;
                                    var valorAdicional = 0;
                                    
                                    valoresx ="[";
                                    
                                    $.each( arrayValores, function ( userkey, uservalue) 
                                    {
                                        valorAdicional = 0;
                                        
                                        if(tipo == 1 && uservalue.descripcion.toUpperCase() == "DIARIA")
                                        {
                                            valorAdicional = valorDiaria;
                                        }
                                        else
                                        {
                                            valorAdicional = uservalue.valor;
                                        }
                                        if(valoresx == "[")
                                        {
                                            //console.log(userkey + ': ' + uservalue.id + ': ' + uservalue.descripcion );
                                            valoresx += '{"orden":"' + userkey+ '","id":"' + uservalue.id + '","descripcion":"' + uservalue.descripcion + '","valor":"' + valorAdicional + '","cantidad":"0"}';
                                        }
                                        else
                                        {
                                            valoresx += ',{"orden":"' + userkey + '","id":"' + uservalue.id + '","descripcion":"' + uservalue.descripcion + '","valor":"' + valorAdicional + '","cantidad":"0"}';
                                        }
                                    });
                                    
                                    valoresx +="]";
                                    
                                    //valores = data;
                                    //$.parseJSON(valores);

                                    return valoresx;
                                },
                                error: function(xhr, textStatus, errorThrown) 
                                {
                                    // Handle error
                                    alert(errorThrown);
                                    //$.unblockUI(); 
                                }
                        });
              
              return valoresx;
       }
       
       function cargarReserva()
        {
            var valoresx = "";
            valoresx = $.ajax({
                                data: {},
                                type: "POST",
                                dataType: "json",
                                url: "<?=$homesite;?>/web/app_dev.php/getone/?id="+idReserva,
                                async:false,
                                success: function(data)
                                {
                                    var arrayValores = data.responseText;

                                    //alert(arrayValores.toSource());

                                    return arrayValores;
                                },
                                error: function(xhr, textStatus, errorThrown) 
                                {
                                    // Handle error
                                    alert(errorThrown);
                                    //$.unblockUI(); 
                                }
                        });
              //alert(valoresx);
              return valoresx;
       }
       
       function calcularValor(objeto,target,valor,id = -1,tipoCalculo)
       {
           var calculoIn = 0;
           if(isNaN(objeto.value))
           {
               $("#"+target).html(0);
               $("#subtotal1").html(0);
           }
           else
           {
               
               calculoIn = objeto.value * valor;
               $("#"+target).html(calculoIn);
               if(id != -1 && tipoCalculo == "G")
               {
                   $("#idAdicGrabCantidad_"+id).val(objeto.value);
                   $("idAdicGrabValor_"+id).val(valor);
               }
               
               if(id != -1 && tipoCalculo == "NG")
               {
                   $("#idAdicNoGrabCantidad_"+id).val(objeto.value);
                   $("idAdicNoGrabValor_"+id).val(valor);
               }
               
               if(id != -1 && tipoCalculo == "0")
               {
                   $("#idAdicOtrosCantidad_"+id).val(objeto.value);
                   $("idAdicOtrosValor_"+id).val(valor);
               }
           }
       }
      
       function calcularSubtotal1(param)
       {
           var sumatoria = 0;
           var valorCalculo = "";
           
           $.each( adicionalesGravados, function ( userkey, uservalue) 
            {
                valorCalculo = $("#lblTotalAdic_"+uservalue.id).html();
                
                if(isNaN(valorCalculo))
                {
                    $("#subtotal1").html(0);
                    return false;
                }
                else
                {
                    sumatoria = sumatoria + parseFloat(valorCalculo);
                }
                
            });
            
            var sumatoriaDiaria = 0;
            
            sumatoriaDiaria = ($("#diaria").val() * $("#diariaCantidad").val());
            
            sumatoria = sumatoria + sumatoriaDiaria;
            
            $("#subtotal1").html(sumatoria.toFixed(2));
            calcularSubtotal3();
       }
       
       function calcularSubtotal2()
       {
           var sumatoria = 0;
           var valorCalculo = "";
           $.each( adicionalesNoGravados, function ( userkey, uservalue) 
            {
                valorCalculo = $("#lblTotalAdicNG_"+uservalue.id).html();
                
                if(isNaN(valorCalculo))
                {
                    $("#subtotal2").html(0);
                    return false;
                }
                else
                {
                    sumatoria = sumatoria + parseFloat(valorCalculo);
                }
                
            });
            
            
            
           var sumatoriaOtros = 0;
           var valorCalculo = 0;
           $.each( adicionalesOtros, function ( userkey, uservalue) 
            {
                valorCalculo = $("#lblTotalAdicO_"+uservalue.id).html();
                
                if(isNaN(valorCalculo))
                {
                    $("#subtotal2").html(0);
                    return false;
                }
                else
                {
                    sumatoriaOtros = sumatoriaOtros + parseFloat(valorCalculo);
                }
                
            });
            
            
            
            sumatoria = sumatoria + sumatoriaOtros;
            
            var tasa = 0;
            var tasaApt = 0;
            var montoTasa = 0;
            var montoTasaApt = 0;
            var sumaPorcentajes = 0;
            
            
            if(!isNaN($("#tasa").val()))
            {
                
                tasa = $("#tasa").val();
                montoTasa = (sumatoria*tasa)/100;
                sumaPorcentajes = montoTasa;
            }
            
            if(!isNaN($("#tasaApt").val()))
            {
                
                tasaApt = $("#tasaApt").val();
                montoTasaApt = (sumatoria*tasaApt)/100;
                sumaPorcentajes = sumaPorcentajes + montoTasaApt;
            }
            
            
            
            sumatoria = sumatoria + sumaPorcentajes;
            
            
            $("#subtotal2").html(sumatoria.toFixed(2));
            
            
            
            
            
            
            calcularSubtotal3();
       }
       
       function calcularSubtotal3()
       {
           var sumatoria = 0;
           var subtotal1 = 0;
           var subtotal2 = 0;
           var sumaDeducciones = 0;
           
           
           
           subtotal1 = $("#subtotal1").html();
           subtotal2 = $("#subtotal2").html();
           sumatoria = parseFloat(subtotal1) + parseFloat(subtotal2);
           var valorIva = 0;
           if(!isNaN($("#valorIva").val()))
           {
               valorIva = parseFloat($("#valorIva").html());
               valorIva = valorIva.toFixed(2);
           }
           
           var ivaPorcent = 0;
           var valorIva = 0;
            
        if(!isNaN($("#ivaPorcent").val()))    
        {
           ivaPorcent = $("#ivaPorcent").val();
           if(ivaPorcent == "")
           {
               ivaPorcent = 0;
           }
           else
           {
                ivaPorcent = (ivaPorcent/100) + 1;
           }
           valorIva = parseFloat(valorIva);
           //alert(ivaPorcent);
           valorIva = sumatoria-(sumatoria/ivaPorcent);
           $("#valorIva").html(valorIva.toFixed(2));
        }
           
           
        $("#subtotal3").html(sumatoria);
          
          
          
          if(!isNaN($("#anticipo1").val()))
            {
                sumaDeducciones = parseFloat($("#anticipo1").val());
            }
            
            if(!isNaN($("#anticipo2").val()))
            {
                sumaDeducciones = sumaDeducciones + parseFloat($("#anticipo2").val());
            }
            
            if(!isNaN($("#voucher").val()))
            {
                sumaDeducciones = sumaDeducciones + parseFloat($("#voucher").val());
            }
            
            if(!isNaN($("#reintegros").val()))
            {
                sumaDeducciones = sumaDeducciones + parseFloat($("#reintegros").val());
            }
            //sumatoria = sumatoria+parseFloat(valorIva);
            $("#total1").html((sumatoria).toFixed(2));
            $("#totalDeducciones").html(sumaDeducciones.toFixed(2));
            $("#saldo").html((sumatoria-sumaDeducciones).toFixed(2));
            validarSaldos();
       }
       
       var agencias = JSON.parse(cargarAgencias());
       var agenciasTodas = JSON.parse(cargarAgenciasTodas());
       //var auxAdcionalesGravados JSON.parse(cargarCategoria());
       var adicionalesGravados = "";
       var adicionalesNoGravados = JSON.parse(cargarAdicional(2));
       var adicionalesOtros = JSON.parse(cargarAdicional(3));
       
       
        
        
        $("#fechaNac,#licFecVto,#fechaHoraDevolucion,#fechaSalida,#fechaRetorno,#fechaNacConAdic,#licFecVtoConAdic" ).datepicker({
                        // Formato de la fecha
                        dateFormat: "dd/mm/yy",
                        // Primer dia de la semana El lunes
                        firstDay: 1,
                        numberOfMonths: 1,
                        // Dias Largo en castellano
                        dayNames: [ "Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado" ],
                        // Dias cortos en castellano
                        dayNamesMin: [ "Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa" ],
                        // Nombres largos de los meses en castellano
                        monthNames: [ "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" ],
                        // Nombres de los meses en formato corto
                        monthNamesShort: [ "Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic" ]
                        // Cuando seleccionamos la fecha esta se pone en el campo Input
                        
                    });
                    
            

   
                    
        function formatearFecha(fecha)
        {
           if(fecha)
           {
            var y = Number(fecha.substring(0,4));
                    var m = Number(fecha.substring(5,7));
                    var d = Number(fecha.substring(8,10));
            return String(d)+'/'+String(m)+'/'+String(y);
            }
            else
            {
                return "";
            }
        }
    var admin = false;
    var estado = 0;

    function checkEstado(estado) {
        if(contrato != false) //existe el contrato por lo tanto saco los datos de allí
        {
            if (admin == false) {
                if (estado == 1) {
                    $("#vehiculox").prop("disabled", "");
                    $("#vehiculox input").prop("disabled", "disabled");
                    $("#dmDevolucion").prop("disabled", "");
                    $("#totalKm").prop("disabled", "");
                } else {
                    $("#vehiculox").prop("disabled", "disabled");
                    $("#dmDevolucion").val($("#km").val());
                    $("#totalKm").val(0);
                }
            }
        }
    }
    
    function validarSaldos(contratoCreado)
    {
        //alert($("#saldo").html());
        var sumatoriaDesglose = 0;
        var saldo = parseFloat($("#saldo").html());
        var ok = false;
        
        if(isNaN(parseFloat($("#cheque").val())))
        {
            $("#cheque").val(0);
        }
        if(isNaN(parseFloat($("#tarjeta").val())))
        {
            $("#tarjeta").val(0);
        }
        if(isNaN(parseFloat($("#ctacte").val())))
        {
            $("#ctacte").val(0);
        }
        if(isNaN(parseFloat($("#prepagos").val())))
        {
            $("#prepagos").val(0);
        }
        if(isNaN(parseFloat($("#contado").val())))
        {
            $("#contado").val(0);
        }
        
        sumatoriaDesglose = parseFloat($("#contado").val()) + parseFloat($("#cheque").val()) + parseFloat($("#tarjeta").val()) + parseFloat($("#ctacte").val()) + parseFloat($("#prepagos").val());

        var estado =$('input[type="radio"][name="estado[]"]:checked').val();

        //if(estado != 0 || !contratoCreado){     // estado no abierto o contrato NO creado
            if(sumatoriaDesglose > saldo)
            {
                $("#msgSaldos").html("Sobran " + (sumatoriaDesglose - saldo) + " no se puede guardar");
            }

            if(sumatoriaDesglose < saldo)
            {
                var subSaldo = saldo - sumatoriaDesglose;
                $("#msgSaldos").html("Faltan " + subSaldo.toFixed(2) + " no se puede guardar");
            }

            if(sumatoriaDesglose == saldo)
            {
                $("#msgSaldos").html("Desglose OK");
                ok = true;
            }
        /*} else {    // estado abierto
            ok = true;
        }*/
        return ok;
    }
jQuery(document).ready(function($) 
{

      function esAdmin()
       {
            var valoresx = "";
            $.ajax({
                                data: {},
                                type: "POST",
                                dataType: "json",
                                url: "<?=$homesite;?>/web/app_dev.php/contrato/esadmin/",
                                async:false,
                                success: function(data)
                                {
                                    valoresx = data;
                                    
                                    
                                    return valoresx;
                                },
                                error: function(xhr, textStatus, errorThrown) 
                                {
                                    // Handle error
                                    alert(errorThrown);
                                    //$.unblockUI(); 
                                }
                        });
              return valoresx;
       }
       
       var reserva = cargarReserva();
       var resultadoUser = esAdmin();
       admin = resultadoUser.admin;

       sucursalUsuario = resultadoUser.sucursal;
       
       
       //console.log("--------------------");
       reserva = JSON.parse(reserva.responseText);
       reserva = reserva[0];
       
       var cliente = reserva.cliente;
       var clienteOtrosDatos = false;
       var vehiculo = reserva.vehiculo;

       if(reserva.contrato)
       {
           contrato = reserva.contrato;
       }

       if(cliente.otros_datos);
       {
           clienteOtrosDatos = cliente.otros_datos[0];
       }
       $("#nombre").val(cliente.nombre);
       $("#apellido").val(cliente.apellido);
       $("#fechaNac").val(formatearFecha(cliente.fecha_nac));
       $("#domicilio").val(cliente.domicilio);
       $("#telefono").val(cliente.telefono);
       //falata el teléfono
       $("#email").val(cliente.email);
       var mesAnioVto = new Array();
       if(clienteOtrosDatos)
       {
            var anioInicial = 2015;//(new Date).getFullYear();
            var anioActual = (new Date).getFullYear();
            var anioFinal = anioActual + 30;
            var auxArray;
            
            $("#licencia").val(clienteOtrosDatos.licencia);
            $("#licFecVto").val(formatearFecha(clienteOtrosDatos.lic_fec_vto));
            $("#nroTarjeta").val(clienteOtrosDatos.nro_tarjeta_cre);
            
            
            if(clienteOtrosDatos.tarj_fec_vto != "")
            {
                mesAnioVto = clienteOtrosDatos.tarj_fec_vto;
                mesAnioVto = mesAnioVto.split('/');
                //alert("entra 1");
                if(mesAnioVto.length == 1)
                {
                
                    mesAnioVto = clienteOtrosDatos.tarj_fec_vto.split('-');
                    //alert(mesAnioVto);    
                    //alert(parseInt(mesAnioVto[1]));    
                    
                    //auxArray[0] = parseInt(mesAnioVto[1]);
                    //auxArray[1] = parseInt(mesAnioVto[0]);
                    mesAnioVto[0] = parseInt(mesAnioVto[1]);
                    mesAnioVto[1] = parseInt(mesAnioVto[0]);
                }
                //alert("valor: "+parseInt(mesAnioVto[0],10));
            }
            else
            {
                mesAnioVto;
                mesAnioVto[0] = 1;
                mesAnioVto[1] = anioActual;
            }
            var indice = 0;
            
            
            if(parseInt(mesAnioVto.length) > 2)
            {
               indice = 1; 
            }
            

            
            $("#codSeg").val(clienteOtrosDatos.cs); 
            $("#tipoTarjeta").val(clienteOtrosDatos.tipo_tarjeta); 
            $("#monto").val(clienteOtrosDatos.monto); 
       }
       else
       {
       
            var anioInicial = 2015;//(new Date).getFullYear();
            var anioActual = (new Date).getFullYear();
            var anioFinal = anioActual + 30;
            var auxArray;
            
            
            
                
                mesAnioVto[0] = 1;
                mesAnioVto[1] = anioActual;
            
            var indice = 0;
       
            
            if(parseInt(mesAnioVto.length) > 2)
            {
               indice = 1; 
            }
            
            

            
       }
       
       
       
        $("#tarjFecVtoMes").append('<option selected value="">[Mes]</option>');

        for(a=1;a<13;a++)
        {

            if(a == mesAnioVto[0+indice])
            {
                $("#tarjFecVtoMes").append('<option selected value="' + a + '">' + a + '</option>');
            }
            else
            {
                $("#tarjFecVtoMes").append('<option value="' + a + '">' + a + '</option>');
            }
            //console.log(userkey + ': ' + uservalue.id + ': ' + uservalue.descripcion );
        }

        $("#tarjFecVtoAnio").append('<option selected value="">[Año]</option>');

        for(b=anioActual;b<anioFinal;b++)
        {
            if(b == mesAnioVto[1+indice])
            {
                $("#tarjFecVtoAnio").append('<option selected value="' + b + '">' + b + '</option>');
            }
            else
            {
                $("#tarjFecVtoAnio").append('<option value="' + b + '">' + b + '</option>');
            }
            //console.log(userkey + ': ' + uservalue.id + ': ' + uservalue.descripcion );
        }
       $("#dominio").val(vehiculo.dominio); 
       $("#modelo").val(vehiculo.descripcion); 
       var agenciaOrigen = vehiculo.agencia.id;
       /*
       $.each( agencias, function ( userkey, uservalue) 
        {
            if(agenciaOrigen == userkey)
            {
                $("#origen").append('<option selected value="' + userkey + '">' + uservalue + '</option>');
            }
            else
            {
                $("#origen").append('<option value="' + userkey + '">' + uservalue + '</option>');
            }
            //console.log(userkey + ': ' + uservalue.id + ': ' + uservalue.descripcion );
        });
        */
       if (contrato != ""){
           $("#km").val(contrato.kilometros);
       } else {
           $("#km").val(vehiculo.kilometros);
       }

        if(vehiculo.categoria.descripcion)
        {
            $("#categoria").val(vehiculo.categoria.descripcion);
            
            //alert(vehiculo.categoria.id);
            valorDiaria = cargarCategoria(vehiculo.categoria.id);
            //alert(auxAdicionalesGravados);
            adicionalesGravados = JSON.parse(cargarAdicional(1),valorDiaria);
            
        }
        else
        {
            $("#categoria").val("");
        }
        
        $("#fechaHoraDevolucion").val(formatearFecha(reserva.fecha_hora_retorno));
        var estadoTanque = "";
        // Si Existe el contrato
        if(contrato != "") {
            estadoTanque = contrato.estado_tanque;
            $('#'+estadoTanque).prop('checked', true);
        }else {
            estadoTanque = vehiculo.estado_tanque;
            $('#'+estadoTanque).prop('checked', true);
        }
        
        $('#horaSalida,#horaRetorno').mask('99:99');
        $('#totalHoras').mask('9999');
        $('#totalDias').mask('9999');
        
        $('#totalDias').val(contrato.total_dias);
        $('#totalHoras').val(contrato.total_horas);
        
        $("#fechaSalida").val(formatearFecha(reserva.fecha_hora_salida));
        
        
        
        $("#fechaRetorno").val(formatearFecha(reserva.fecha_hora_devolucion));
        
        $("#horaSalida").val(reserva.hora_salida);
        $("#horaRetorno").val(reserva.hora_devolucion);
        
        $("#dmDevolucion" ).val($("#km").val());

        $("#totalKm").val(0);
        if(contrato != false) //existe el contrato por lo tanto saco los datos de allí
        {
            $("#totalKm").val(contrato.total_km);
        }

        $( "#dmDevolucion" ).keyup(function( event ) 
        {
            var cadena = $(this).val();
            var calculo = 0;
            $(this).val(cadena.replace(/[^0-9\.]/g,''));
            
            calculo = $(this).val()-$("#km").val();
            $("#totalKm").val(calculo);
          
         }).keydown(function( event ) {
            if ( event.which == 13 ) 
            {
              event.preventDefault();
            }
          });
        
        
        $("#anticipo1").val(0);
        $("#anticipo2").val(0);
        $("#voucher").val(0);
        $("#reintegros").val(0);
        $("#ivaPorcent").val(<?php echo $ivaPercent ?>);

        $("#tp1").prop('checked', true);
        
        if(contrato != false) //existe el contrato por lo tanto saco los datos de allí
        {
            $("#nroContrato").val(contrato.id);
            $("#idContrato").val(contrato.id);
            //trae los adicionales del contrato no de la tabla de adicionales
            adicionalesGravados = convertAdic(contrato.adicionales,1);
            adicionalesNoGravados = convertAdic(contrato.adicionales,2);
            adicionalesOtros = convertAdic(contrato.adicionales,3);
           
            $("#tasa").val(contrato.tasa_aeropuerto);
            $("#tasaApt").val(contrato.tasa_apt);
            $("#ivaPorcent").val(contrato.iva_porcent);
            $("#anticipo1").val(contrato.anticipo1);
            $("#anticipo2").val(contrato.anticipo2);
            $("#voucher").val(contrato.voucher);
            $("#reintegros").val(contrato.reintegros);
            
            $("#contado").val(contrato.contado);
            $("#prepagos").val(contrato.prepagos);
            $("#cheque").val(contrato.cheque);
            $("#ctacte").val(contrato.ctacte);
            $("#tarjeta").val(contrato.tarjeta);
            $("#tarjetaEmisor").val(contrato.tarjeta_emisor);
            if(contrato.estado == 0)
            {
                $("#tp1").prop('checked', true);
            }
            
            if(contrato.estado == 1)
            {
                $("#tp2").prop('checked', true);
            }
            if(contrato.estado == 2)
            {
                $("#tp3").prop('checked', true);
            }
            
            estado = contrato.estado;
            
            valorDiariaGuardado = contrato.diaria;
            cantidadDiariaGuardado = contrato.diaria_cantidad;

            
            if($("#nroContrato").val() != "")
            {                

                if(admin == "false" || admin == false)
                {
                    $("#clientex").prop("disabled","disabled");
                    
                    $("#clienteAdicionalx").prop("disabled","disabled");
                    $("#vehiculox").prop("disabled","disabled");
                    //$("#observacionex").prop("disabled","disabled");
                    if(estado != 0) //abierto
                    {   
                        $("#desglosex").prop("disabled","disabled");
                        $("#adicionalesx").prop("disabled","disabled");
                    }
                }
                else
                {
                    $("#clienteEditable").val("SI");
                }


            }
            else
            {
                $("#clienteEditable").val("SI");
            }
            
        }
        


        if(valorDiariaGuardado == 0)
        {
            $("#diaria").val(valorDiaria);
        }
        else
        {
            $("#diaria").val(valorDiariaGuardado);
            $("#diaria").attr('readonly','readonly');

        }

        if(cantidadDiariaGuardado == 0)
        {
            $("#diariaCantidad").val(cantidadDiaria);
        }
        else
        {
            $("#diariaCantidad").val(cantidadDiariaGuardado);
        }


        /*carga de adicionales*/
        
        $("#adicionalesGravados").html(getAdicGravados(adicionalesGravados));
        $("#adicionalesNoGravados").html(getAdicNoGravados(adicionalesNoGravados));
        $("#adicionalesOtros").html(getAdicOtros(adicionalesOtros));
        
        $.unblockUI();

        if(contrato != false) //existe el contrato por lo tanto saco los datos de allí
        {
            $("#nroContrato").val(contrato.id);
            $("#idContrato").val(contrato.id);
            //trae los adicionales del contrato no de la tabla de adicionales
            adicionalesGravados = convertAdic(contrato.adicionales,1);
            adicionalesNoGravados = convertAdic(contrato.adicionales,2);
            adicionalesOtros = convertAdic(contrato.adicionales,3);
            var ordenx = 0;
            $.each( adicionalesGravados, function ( userkey, uservalue)
            {                
                $("#lblTotalAdic_"+uservalue.id).html(uservalue.cantidad * uservalue.valor);
                $("#idAdicGrabCantidad_"+ordenx).val(uservalue.cantidad);
                $("idAdicGrabValor_"+ordenx).val(uservalue.valor);
                ordenx++;
            });
            
            ordenx = 0;
            $.each( adicionalesNoGravados, function ( userkey, uservalue)
            {                
                $("#lblTotalAdicNG_"+uservalue.id).html(uservalue.cantidad * uservalue.valor);
                $("#idAdicNoGrabCantidad_"+ordenx).val(uservalue.cantidad);
                $("idAdicNoGrabValor_"+ordenx).val(uservalue.valor);
                ordenx++;
            });
            
            ordenx = 0;
            $.each( adicionalesOtros, function ( userkey, uservalue)
            {                
                $("#lblTotalAdicO_"+uservalue.id).html(uservalue.cantidad * uservalue.valor);
                $("#idAdicOtrosCantidad_"+ordenx).val(uservalue.cantidad);
                $("idAdicOtrosValor_"+ordenx).val(uservalue.valor);
                ordenx++;
            });
            
            $("#observaciones").val(contrato.observaciones);
        }
        
        calcularSubtotal1();
        calcularSubtotal2();
        calcularSubtotal3();
        
        //console.log(cliente);
       var idSucursalOrigen = 0;
       var idSucursalDestino = 0;
       
       if(contrato != false) //existe el contrato por lo tanto saco los datos de allí
       {
            if(contrato.agencia_origen)
            {
                idSucursalOrigen = contrato.agencia_origen.id;
            }
            if(contrato.agencia_destino)
            {
                idSucursalDestino = contrato.agencia_destino.id;
            }
       }
       
       
       $.each( agenciasTodas, function ( userkey, uservalue) 
        {
            selectedSuc = "";
            
            
            if(idSucursalOrigen == userkey)
            {
                selectedSuc = " selected ";
                
            }
            selectedSucDisabled  = "";
            //alert(userkey);
            if(admin == false && sucursalUsuario != 1)
            {
                if(userkey != sucursalUsuario)
                {
                    selectedSucDisabled  = " disabled ";
                }
            }
            $("#sucursal").append('<option '+selectedSucDisabled+' '+selectedSuc+' value="' + userkey + '">' + uservalue + '</option>');
           
            //console.log(userkey + ': ' + uservalue.id + ': ' + uservalue.descripcion );
        });
        
        $.each( agenciasTodas, function ( userkey, uservalue) 
        {
           
            selectedSucDestino = "";
            
            if(idSucursalDestino == userkey)
            {
                selectedSucDestino = " selected ";
            }
           
            $("#sucursalDestino").append('<option '+selectedSucDestino+' value="' + userkey + '">' + uservalue + '</option>');
            //console.log(userkey + ': ' + uservalue.id + ': ' + uservalue.descripcion );
        });
});       

function convertAdic(adicionalesx,tipoAdicional)
{
    //alert("entra");
        var contador = 0;
        var valoresx ="[";
        
        $.each( adicionalesx, function ( userkey, uservalue) 
        {
            //alert(uservalue.cantidad);
            if(uservalue.adicional.tipo_adicional.id == tipoAdicional)
            {
                if(valoresx == "[")
                {
                    //console.log(userkey + ': ' + uservalue.id + ': ' + uservalue.descripcion );
                    //valoresx += '{"orden":"' + userkey+ '","id":"' + uservalue.id + '","descripcion":"' + uservalue.descripcion + '","valor":"' + uservalue.valor + '","cantidad":"0"}';
                    valoresx += '{"orden":"' + contador+ '","id":"' + uservalue.adicional.id + '","descripcion":"' + uservalue.adicional.descripcion + '","valor":"' + uservalue.valor + '","cantidad":"' + uservalue.cantidad + '"}';
                }
                else
                {
                    //valoresx += ',{"orden":"' + userkey+ '","id":"' + uservalue.id + '","descripcion":"' + uservalue.descripcion + '","valor":"' + uservalue.valor + '","cantidad":"0"}';
                    valoresx += ',{"orden":"' + contador+ '","id":"' + uservalue.adicional.id + '","descripcion":"' + uservalue.adicional.descripcion + '","valor":"' + uservalue.valor + '","cantidad":"' + uservalue.cantidad + '"}';
                }   
                contador++;
            }
        });
        valoresx +="]";
        return $.parseJSON(valoresx);
}

function getAdicGravados(adicionalesGravados)
{
    var totalAdicG = 0;
        var cadenaGravados = '<table width="98%" class="bordeTablaAdic">';
        cadenaGravados += '<tr>';
        cadenaGravados += '<th bgcolor="#D0D0D0" width="80px">';
        cadenaGravados += 'Cargos ';
        cadenaGravados += '</th>';
        cadenaGravados += '<th bgcolor="#D0D0D0" >';
        cadenaGravados += 'Diario';
        cadenaGravados += '</th>';
        cadenaGravados += '   <th bgcolor="#D0D0D0" >';
        cadenaGravados += '           Total Días';
        cadenaGravados += '      </th>';
        cadenaGravados += '      <th bgcolor="#D0D0D0" >';
        cadenaGravados += '          Total';
        cadenaGravados += '      </td>';
        cadenaGravados += '   </tr>';

        
        $.each( adicionalesGravados, function ( userkey, uservalue) 
         {
             //$("#sucursal").append('<option value="' + userkey + '">' + uservalue + '</option>');
             //console.log(userkey + ': ' + uservalue.id + ': ' + uservalue.descripcion + " :" + uservalue.valor );
             cadenaGravados +='<tr><td>' + uservalue.descripcion + '</td>';
             cadenaGravados +='<td>' + uservalue.valor + '</td><td>';
             cadenaGravados +='<input onchange="javascript:calcularValor(this,\'lblTotalAdic_'+uservalue.id+'\',' + uservalue.valor + ','+ uservalue.orden +',\'G\');calcularSubtotal1();" style="width:60px" type="text"  name="totalAdic_'+uservalue.id+'" id="totalAdic_'+uservalue.id+'" placeholder="Total Días" value="'+uservalue.cantidad +'"/>';
             cadenaGravados +='<input type="hidden" name="idAdic_'+uservalue.orden+'" id="idAdic_'+uservalue.orden+'" value="'+uservalue.id+'"/>';
             cadenaGravados +='</td>';
             cadenaGravados +='<td><label id="lblTotalAdic_'+uservalue.id+'">0</label></td></tr>';
             /*para el pdf*/
             cadenaGravados +='<input type="hidden" name="idAdicGrabDescrip_'+uservalue.orden+'" id="idAdicGrabDescrip_'+uservalue.orden+'" value="'+uservalue.descripcion+'"/>';
             cadenaGravados +='<input type="hidden" name="idAdicGrabValor_'+uservalue.orden+'" id="idAdicGrabValor_'+uservalue.orden+'" value="'+uservalue.valor+'"/>';
             cadenaGravados +='<input type="hidden" name="idAdicGrabCantidad_'+uservalue.orden+'" id="idAdicGrabCantidad_'+uservalue.orden+'" value="'+uservalue.cantidad+'"/>';
             totalAdicG++;
         });
         cadenaGravados += '</table>';
         $("#totalAdicG").val(totalAdicG);
         return cadenaGravados;
}

function getAdicNoGravados(adicionalesNoGravados)
{
    var totalAdicNG = 0;
    var cadenaNoGravados = '<table width="98%" class="bordeTablaAdic">';
        cadenaNoGravados += '<tr>';
        cadenaNoGravados += '     <th bgcolor="#D0D0D0" width="80px">';
        cadenaNoGravados += '         Cargos ';
        cadenaNoGravados += '     </th>';
        cadenaNoGravados += '     <th bgcolor="#D0D0D0" >';
        cadenaNoGravados += '         Diario';
        cadenaNoGravados += '     </th>';
        cadenaNoGravados += '     <th bgcolor="#D0D0D0" >';
        cadenaNoGravados += '         Total Días';
        cadenaNoGravados += '     </th>';
        cadenaNoGravados += '     <th bgcolor="#D0D0D0" >';
        cadenaNoGravados += '         Total';
        cadenaNoGravados += '     </td>';
        cadenaNoGravados += ' </tr>';
                            
        $.each( adicionalesNoGravados, function ( userkey, uservalue) 
         {
             //$("#sucursal").append('<option value="' + userkey + '">' + uservalue + '</option>');
             //console.log(userkey + ': ' + uservalue.id + ': ' + uservalue.descripcion + " :" + uservalue.valor );
             cadenaNoGravados +='<tr><td>' + uservalue.descripcion + '</td>';
             cadenaNoGravados +='<td>' + uservalue.valor + '</td><td>';
             cadenaNoGravados +='<input onchange="javascript:calcularValor(this,\'lblTotalAdicNG_'+uservalue.id+'\',' + uservalue.valor + ','+ uservalue.orden+ ',\'NG\');calcularSubtotal2();" style="width:60px" type="text"  name="totalAdicNg_'+uservalue.id+'" id="totalAdicNg_'+uservalue.id+'" placeholder="Total Días" value="'+uservalue.cantidad +'"/>';
             cadenaNoGravados +='<input type="hidden" name="idAdicNG_'+uservalue.orden+'" id="idAdicNG_'+uservalue.orden+'" value="'+uservalue.id+'"/>';
             cadenaNoGravados +='</td>';
             cadenaNoGravados +='<td><label id="lblTotalAdicNG_'+uservalue.id+'">0</label></td></tr>';
             
             /*para el pdf*/
             cadenaNoGravados +='<input type="hidden" name="idAdicNoGrabDescrip_'+uservalue.orden+'" id="idAdicNoGrabDescrip_'+uservalue.orden+'" value="'+uservalue.descripcion+'"/>';
             cadenaNoGravados +='<input type="hidden" name="idAdicNoGrabValor_'+uservalue.orden+'" id="idAdicNoGrabValor_'+uservalue.orden+'" value="'+uservalue.valor+'"/>';
             cadenaNoGravados +='<input type="hidden" name="idAdicNoGrabCantidad_'+uservalue.orden+'" id="idAdicNoGrabCantidad_'+uservalue.orden+'" value="'+uservalue.cantidad+'"/>';
             totalAdicNG++;
         });                

        cadenaNoGravados +='</table>';
        $("#totalAdicNG").val(totalAdicNG);
         return cadenaNoGravados;
}


function getAdicOtros(adicionalesOtros)
{
    var totalAdicO = 0;
    var cadenaOtros = '<table width="98%" class="bordeTablaAdic">';
        cadenaOtros += '<tr>';
        cadenaOtros += '     <th bgcolor="#D0D0D0" width="80px">';
        cadenaOtros += '         Cargos ';
        cadenaOtros += '     </th>';
        cadenaOtros += '     <th bgcolor="#D0D0D0" >';
        cadenaOtros += '         Diario';
        cadenaOtros += '     </th>';
        cadenaOtros += '     <th bgcolor="#D0D0D0" >';
        cadenaOtros += '         Total Días';
        cadenaOtros += '     </th>';
        cadenaOtros += '     <th bgcolor="#D0D0D0" >';
        cadenaOtros += '         Total';
        cadenaOtros += '     </td>';
        cadenaOtros += ' </tr>';
                            
        $.each( adicionalesOtros, function ( userkey, uservalue) 
         {
             //$("#sucursal").append('<option value="' + userkey + '">' + uservalue + '</option>');
             //console.log(userkey + ': ' + uservalue.id + ': ' + uservalue.descripcion + " :" + uservalue.valor );
             cadenaOtros +='<tr><td>' + uservalue.descripcion + '</td>';
             cadenaOtros +='<td>' + uservalue.valor + '</td><td>';
             cadenaOtros +='<input onchange="javascript:calcularValor(this,\'lblTotalAdicO_'+uservalue.id+'\',' + uservalue.valor + ','+ uservalue.orden+ ',\'O\');calcularSubtotal2();" style="width:60px" type="text"  name="totalAdicO_'+uservalue.id+'" id="totalAdicO_'+uservalue.id+'" placeholder="Total Días" value="'+uservalue.cantidad +'"/>';
             cadenaOtros +='<input type="hidden" name="idAdicO_'+uservalue.orden+'" id="idAdicO_'+uservalue.orden+'" value="'+uservalue.id+'"/>';
             cadenaOtros +='</td>'
             cadenaOtros +='<td><label id="lblTotalAdicO_'+uservalue.id+'">0</label></td></tr>';
             
             /*para el pdf*/
             cadenaOtros +='<input type="hidden" name="idAdicOtrosDescrip_'+uservalue.orden+'" id="idAdicOtrosDescrip_'+uservalue.orden+'" value="'+uservalue.descripcion+'"/>';
             cadenaOtros +='<input type="hidden" name="idAdicOtrosValor_'+uservalue.orden+'" id="idAdicOtrosValor_'+uservalue.orden+'" value="'+uservalue.valor+'"/>';
             cadenaOtros +='<input type="hidden" name="idAdicOtrosCantidad_'+uservalue.orden+'" id="idAdicOtrosCantidad_'+uservalue.orden+'" value="'+uservalue.cantidad+'"/>';
             
             totalAdicO++;
         });                

        cadenaOtros +='</table>';
        $("#totalAdicO").val(totalAdicO);
         return cadenaOtros;
}

 function getLockeo(idReserva)
{


     var valoresx = "";
     $.ajax({
                         data: {},
                         type: "POST",
                         dataType: "json",
                         url: "<?=$homesite;?>/web/app_dev.php/contrato/"+idReserva+"/searchlock/",
                         async:false,
                         success: function(data)
                         {

                                                 valoresx = data;
                                                 return valoresx;
                         },
                         error: function(xhr, textStatus, errorThrown) {
                             // Handle error
                             alert(errorThrown);
                             //$.unblockUI(); 
                           }
                 });
       return valoresx;
}

function setLockeo(idReserva)
{


     var valoresx = "";
     $.ajax({
                         data: {},
                         type: "POST",
                         dataType: "json",
                         url: "<?=$homesite;?>/web/app_dev.php/contrato/"+idReserva+"/setlock/",
                         async:false,
                         success: function(data)
                         {

                                                 valoresx = data;
                                                 return valoresx;
                         },
                         error: function(xhr, textStatus, errorThrown) {
                             // Handle error
                             alert(errorThrown);
                             //$.unblockUI(); 
                           }
                 });
       return valoresx;
}

function enviarDatos(imprimir)
{
        var respuesta = "";
        var lock = <?=$lockeado;?>;
        var idReserva = <?=$rowId;?>;
        
        $.blockUI({message: '<h1>Validando datos...<br>Espere por favor</h1>', css: { 
                                                    border: 'none', 
                                                    padding: '15px', 
                                                    backgroundColor: '#000', 
                                                    '-webkit-border-radius': '10px', 
                                                    '-moz-border-radius': '10px', 
                                                    opacity: .5, 
                                                    color: '#fff' 
                                                } }); 
        
        var respuesta = getLockeo(idReserva);
            
        if(!respuesta)
        {
           setLockeo(idReserva);
        }
        
        
        
        
        
        if(respuesta)
        {
            $.blockUI({message: '<h1>Este contrato de reserva se encuentra tomado por otro usuario. Espere cinco minutos y vuelva a abrir el contrato</h1><br><input type="button" value="Aceptar" onclick="$.unblockUI();"/>', css: { 
                                                    top:'10px',
                                                    border: 'none', 
                                                    padding: '15px', 
                                                    backgroundColor: '#000', 
                                                    '-webkit-border-radius': '10px', 
                                                    '-moz-border-radius': '10px', 
                                                    opacity: .5, 
                                                    color: '#fff' 
                                                } }); 
           return false;
        }
        
        
        if( (parseFloat($("#reintegros").val()) > 0 || parseFloat($("#reintegros").val()) < 0 ) && $.trim($("#observaciones").val()) == "")
        {
            $.blockUI({message: '<h1>Debe ingresar una Observacion si existen reintregros</h1><br><input type="button" value="Aceptar" onclick="$.unblockUI();"/>', css: { 
                                                    border: 'none', 
                                                    padding: '15px', 
                                                    backgroundColor: '#000', 
                                                    '-webkit-border-radius': '10px', 
                                                    '-moz-border-radius': '10px', 
                                                    opacity: .5, 
                                                    color: '#fff' 
                                                } }); 
           return false;
        }
        
        if(parseFloat(valorDiaria) > parseFloat($("#diaria").val()))
        {
        
            $.blockUI({message: '<h1>El valor del campo Diaria no puede ser inferior a '+ valorDiaria +'</h1><br><input type="button" value="Aceptar" onclick="$.unblockUI();"/>', css: { 
                                                    border: 'none', 
                                                    padding: '15px', 
                                                    backgroundColor: '#000', 
                                                    '-webkit-border-radius': '10px', 
                                                    '-moz-border-radius': '10px', 
                                                    opacity: .5, 
                                                    color: '#fff' 
                                                } }); 
           return false;
        }
        
        var mesActual = <?=$mesActual;?>;
        var anioActual = <?=$anioActual;?>;
        if(parseFloat($("#tipoTarjeta").val())>0)
        {
            var start= new Date(anioActual + "-" + mesActual + "-1");
            var end= new Date($("#tarjFecVtoAnio").val() + "-" + $("#tarjFecVtoMes").val() +"-1");
            if (start >= end || ($("#tarjFecVtoAnio").val() == "" || $("#tarjFecVtoMes").val() == "")) {
                $.blockUI({message: '<h1>La fecha de vencimiento de la terjeta no es válida</h1><br><input type="button" value="Aceptar" onclick="$.unblockUI();"/>', css: { 
                                                    border: 'none', 
                                                    padding: '15px', 
                                                    backgroundColor: '#000', 
                                                    '-webkit-border-radius': '10px', 
                                                    '-moz-border-radius': '10px', 
                                                    opacity: .5, 
                                                    color: '#fff' 
                                                } }); 
                return false;
            }
            //DateTime DateToValue = anioActual + "-" + mesActual + "-1";
            /*DateTime DateFromValue = $("#tarjFecVtoAnio").val() + "-" + $("#tarjFecVtoMes").val() +"-1";

            if (Date.parse(DateToValue) >= Date.parse(DateFromValue)) 
            {
                $.blockUI({message: '<h1>La fecha de vencimiento de la terjeta no es válida</h1><br><input type="button" value="Aceptar" onclick="$.unblockUI();"/>', css: { 
                                                    border: 'none', 
                                                    padding: '15px', 
                                                    backgroundColor: '#000', 
                                                    '-webkit-border-radius': '10px', 
                                                    '-moz-border-radius': '10px', 
                                                    opacity: .5, 
                                                    color: '#fff' 
                                                } }); 
                return false;
            }*/

        }

    // Validando los KM del Vehiculo sean mayor q 0
    if(parseFloat($("#km").val())<=0 || $("#km").val() == "" )
    {
        $.blockUI({message: '<h1>El valor del campo Kilometros en Vehiculo no puede ser 0</h1><br><input type="button" value="Aceptar" onclick="$.unblockUI();"/>', css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        } });
        return false;
    }


     var contratoCreado = ($("#nroContrato").val()).length != 0;
        // Los saldos no son validos 
        if (! validarSaldos(contratoCreado)) 
        {
            if (contratoCreado) {  // ya existe contrato creado
                $.blockUI({message: '<h1>Los pagos de desglose no coinciden con el saldo</h1><br><input type="button" value="Aceptar" onclick="$.unblockUI();"/>', css: { 
                                                    border: 'none', 
                                                    padding: '15px', 
                                                    backgroundColor: '#000', 
                                                    '-webkit-border-radius': '10px', 
                                                    '-moz-border-radius': '10px', 
                                                    opacity: .5, 
                                                    color: '#fff' 
                                                } }); 
                return false;
            } else {
                if ($.trim($("#observaciones").val()) == "") 
                { // no existe contrato creado y no hay observaciones
                    $.blockUI({message: '<h1>Debe ingresar una Observacion: el saldo no es válido y el contrato no ha sido creado</h1><br><input type="button" value="Aceptar" onclick="$.unblockUI();"/>', css: { 
                                                    border: 'none', 
                                                    padding: '15px', 
                                                    backgroundColor: '#000', 
                                                    '-webkit-border-radius': '10px', 
                                                    '-moz-border-radius': '10px', 
                                                    opacity: .5, 
                                                    color: '#fff' 
                                                } }); 
                    return false;
                }
            }
        } 
          
        if((admin == "false" || admin == false) && estado != 0)
        {
            $.blockUI({message: '<h1>No puede modificar el contrato. Solo lo puede hacer un usuario Administrador</h1><br><input type="button" value="Aceptar" onclick="$.unblockUI();"/>', css: { 
                                                    border: 'none', 
                                                    padding: '15px', 
                                                    backgroundColor: '#000', 
                                                    '-webkit-border-radius': '10px', 
                                                    '-moz-border-radius': '10px', 
                                                    opacity: .5, 
                                                    color: '#fff' 
                                                } }); 
           return false;
        }
        
        $("#clientex").prop("disabled","");
        $("#clienteAdicionalx").prop("disabled","");
        $("#vehiculox").prop("disabled","");
        //$("#observacionex").prop("disabled","");
        $("#desglosex").prop("disabled","");
        $("#adicionalesx").prop("disabled","");
        $("#totalKm").prop("disabled","");
        
        var resultado = false;
        $.ajax({
                                data: $("#formContrato").serialize(),
                                type: "POST",
                                dataType: "json",
                                url: "<?=$homesite;?>/web/app_dev.php/contrato/crear/",
                                async:false,
                                beforeSend : function() {
                                                    $.blockUI({message: '<h1>Guardando Datos...<br>Espere por favor</h1>', css: { 
                                                    border: 'none', 
                                                    padding: '15px', 
                                                    backgroundColor: '#000', 
                                                    '-webkit-border-radius': '10px', 
                                                    '-moz-border-radius': '10px', 
                                                    opacity: .5, 
                                                    color: '#fff' 
                                                } }); 
                                }, 
                                complete: function () 
                                {
                                        $.unblockUI();
                                },
                                success: function(data)
                                {


                                  
                                                    respuesta = data;
                                                    
                                                    
                                                    if (!(typeof data.error === "undefined"))
                                                    {
                                                           alert(data.error);
                                                            
                                                           return false;
                                                    }
                                                    
                                                    $("#nroContrato").val(data.id);
                                                    $("#idContrato").val(data.id);
                                                    if(imprimir)
                                                    {
                                                        imprimirDatos();
                                                    }
                                                        $("#jqGrid").toggleSubGridRow(<?=$rowId;?>);

                                                        $("#jqGrid").expandSubGridRow(<?=$rowId;?>);
                                                        //$("#jqGrid").trigger('reloadGrid');
                                                    
                                                    resultado = true;
                                },
                                error: function(xhr, textStatus, errorThrown) {
                                    // Handle error
                                    $.unblockUI();
                                    alert(errorThrown);
                                    //$.unblockUI(); 
                                  }
                        });
                        return resultado;
}


function imprimirDatos()
{
        var respuesta = "";
        
        var html = "<h1>Error al imprimir</h1>";//$("#formContrato").html();
        
        
        $("#clientex").prop("disabled","");
        $("#clienteAdicionalx").prop("disabled","");
        $("#vehiculox").prop("disabled","");
        //$("#observacionex").prop("disabled","");
        $("#desglosex").prop("disabled","");        
        $("#adicionalesx").prop("disabled",""); 
        $("#totalKm").prop("disabled","");
        $.ajax({
                                data: $("#formContrato").serialize(),
                                type: "POST",
                                url: "gethtml.php",
                                async:false,
                                beforeSend : function() {


                                                    $.blockUI({message: '<h1>Imprimiendo PDF...<br>Espere por favor</h1>', css: { 
                                                    border: 'none', 
                                                    padding: '15px', 
                                                    backgroundColor: '#000', 
                                                    '-webkit-border-radius': '10px', 
                                                    '-moz-border-radius': '10px', 
                                                    opacity: .5, 
                                                    color: '#fff' 
                                                } }); 
                                }, 
                                complete: function () 
                                {
                                         $.unblockUI();
                                },
                                success: function(data)
                                {


                                  
                                                    html = data;
                                                     //alert(html);   
                                                   
                                                    return true;
                                },
                                error: function(xhr, textStatus, errorThrown) {
                                    // Handle error
                                    $.unblockUI();
                                    alert(errorThrown);
                                    //$.unblockUI(); 
                                  }
                        });
        
        
        
        //html = "<label>holaz</label>";//$("#formContrato").html();
         
        $("#html").val(html);
        $("#idReserva").val(idReserva);
        $("#idContratoPdf").val($("#idContrato").val());
        $("#formx").submit();
        

}
</script>

<form id="formx" name="formx" target="_blank" action="<?=$homesite;?>/web/app_dev.php/contrato/pdf/" method="POST">
    <input type="hidden" id="html" name="html" value=""/>
    <input type="hidden" id="idReserva" name="idReserva" value=""/>
    <input type="hidden" id="idContratoPdf" name="idContratoPdf" value=""/>
</form>
<h1>Contrato de Reserva</h1>
  <form target="_self" method="POST" id="formContrato" name="formContrato" >
    <input type="hidden" id="idReserva" name="idReserva" value="<?=$rowId;?>"/>
    <input type="hidden" id="idContrato" name="idContrato" value=""/>
    <input type="hidden" id="clienteEditable" name="clienteEditable" value="NO"/>
    <table width="100%" border="0">
        <tr>    
                <td valign="top" width="50%">
                    <!--cliente-->
                    <table width="98%" border="0">
                        <tr>
                                <td>
                                    Origen: <select  name="sucursal" id="sucursal">
                                        <option value="">[Seleccione]</option>
                                    </select> 
                                    Destino: 
                                    <select  name="sucursalDestino" id="sucursalDestino">
                                        <option value="">[Seleccione]</option>
                                    </select> 
                                    Nro. Contrato: <input  style="width:100px;" type="text" disabled="disabled"  name="nroContrato" id="nroContrato" placeholder="Nro. Contrato"/>
                                </td>
                        </tr>
                    </table>
                    <fieldset id="clientex" name="clientex">
                        <legend><strong>Cliente:</strong></legend>
                        <table width="98%" border="0">
                          <tr>
                              <td with="80">
                                  Nombre: 
                              </td>
                              <td>
                                  <input  type="text" style="width:90%" name="nombre" id="nombre"/>
                              </td>
                              <td align="right">
                                  Apellido: 
                              </td>
                              <td>
                                  <input type="text"  style="width:90%"  name="apellido" id="apellido"/>
                              </td>

                          </tr>
                          <tr>
                              <td>
                                  Fecha Nac:
                              </td>
                              <td>
                                  <input  style="width:90%"  id="fechaNac" name="fechaNac"><br>
                              </td>
                              <td align="right">

                              </td>                          
                              <td>

                              </td>
                          </tr>

                          <tr> 
                              <td>
                                  Domicilio:
                              </td>
                              <td>    
                                  <input  style="width:90%"  type="text"  name="domicilio" id="domicilio"/>
                              </td>
                              <td align="right">

                              </td>
                              <td>

                              </td>
                          </tr>
                          <tr>
                              <td>
                                  Teléfono: 
                              </td>
                              <td>
                                  <input  style="width:90%"  type="text"  name="telefono" id="telefono"/> 
                              </td>
                              <td align="right">
                                  email:
                              </td>
                              <td>
                                  <input  style="width:90%"  type="text"  name="email" id="email"/><br>
                              </td>
                          </tr>
                          <tr><td colspan="4">&nbsp;</td></tr>
                          <tr>
                              <td >
                                  Licencia de Conducir Nro: 
                              </td>
                              <td>
                                  <input  style="width:90%"  type="text"  name="licencia" id="licencia"/> 
                              </td>
                              <td align="right">
                                  Vto:
                              </td>
                              <td>
                                  <input  style="width:90%" type="text"  name="licFecVto" id="licFecVto"/>
                              </td>
                          </tr>
                          <tr><td colspan="4">&nbsp;</td></tr>
                          <tr>
                              <td>
                                  Tarjeta 
                              </td>
                              <td>
                                  <select  style="width:90%"  name="tipoTarjeta" id="tipoTarjeta">
                                      <option value="">Tipo Tarjeta</option>
                                      <option value="1">Visa</option>
                                      <option value="2">Mastercard</option>
                                      <option value="3">American Express</option>
                                      <option value="4">Otras</option>
                                  </select>    

                              </td>
                              <td align="right">
                                  Nro:
                              </td>
                              <td>
                                  <input style="width:90%"  type="text"  name="nroTarjeta" id="nroTarjeta"/>
                              </td>
                          </tr>
                          <tr>
                              <td>
                                  Fec. Vto:
                              </td>
                              <td>
                                  <select id="tarjFecVtoMes" name="tarjFecVtoMes">
                                      
                                  </select>
                                  <select id="tarjFecVtoAnio" name="tarjFecVtoAnio">
                                      
                                  </select>
                                  <!--<input style="width:90%"  type="text"  name="tarjFecVto" id="tarjFecVto" class="tarjFecVto"/>-->
                              </td>
                              <td align="right">

                              </td>
                              <td>

                              </td>
                          </tr>


                          <tr>
                              <td>
                                  Monto
                              </td>
                              <td>
                                  <input  style="width:90%"  type="text"  name="monto" id="monto"/>
                              </td>
                              <td align="right">
                                  Cod. Seg:
                              </td>
                              <td>
                                  <input  style="width:90%" type="text"  name="codSeg" id="codSeg"/>
                              </td>
                          </tr>
                      </table>
                    </fieldset> 

                    <!-- Fin cliente -->
                    
                    <!-- conductor adicional -->
                    <br>
                    <fieldset id="clienteAdicionalx">
                        <legend><strong>Conductor Adicional:</strong></legend>
                        <table width="98%" border="0">
                          <tr>
                              <td with="80">
                                  Nombre: 
                              </td>
                              <td>
                                  <input  type="text" style="width:90%" name="nombreConAdic" id="nombreConAdic"/>
                              </td>
                              <td align="right">
                                  Apellido: 
                              </td>
                              <td>
                                  <input type="text"  style="width:90%"  name="apellidoConAdic" id="apellidoConAdic"/>
                              </td>

                          </tr>
                          <tr>
                              <td>
                                  Fecha Nac:
                              </td>
                              <td>
                                  <input  style="width:90%"  id="fechaNacConAdic" name="fechaNacConAdic"><br>
                              </td>
                              <td align="right">

                              </td>                          
                              <td>

                              </td>
                          </tr>

                          <tr> 
                              <td>
                                  Domicilio:
                              </td>
                              <td>    
                                  <input  style="width:90%"  type="text"  name="domicilioConAdic" id="domicilioConAdic"/>
                              </td>
                              <td align="right">

                              </td>
                              <td>

                              </td>
                          </tr>
                          <tr>
                              <td>
                                  Teléfono: 
                              </td>
                              <td>
                                  <input  style="width:90%"  type="text"  name="telefonoConAdic" id="telefonoConAdic"/> 
                              </td>
                              <td align="right">
                                  email:
                              </td>
                              <td>
                                  <input  style="width:90%"  type="text"  name="emailConAdic" id="emailConAdic"/><br>
                              </td>
                          </tr>
                          <tr><td colspan="4">&nbsp;</td></tr>
                          <tr>
                              <td >
                                  Licencia de Conducir Nro: 
                              </td>
                              <td>
                                  <input  style="width:90%"  type="text"  name="licenciaConAdic" id="licenciaConAdic"/> 
                              </td>
                              <td align="right">
                                  Vto:
                              </td>
                              <td>
                                  <input  style="width:90%" type="text"  name="licFecVtoConAdic" id="licFecVtoConAdic"/>
                              </td>
                          </tr>
                          

                      </table>
                    </fieldset> 
                    
                    <!-- fin conductor adicional -->
                    
                    <br>
                    <!-- inicio vehiculo -->
                        <fieldset id="vehiculox">
                            <legend><strong>Vehículo:</strong></legend>
                        <table width="98%" border="0">
                            <tr>
                                <td width="80">
                                    Patente: 
                                </td>
                                <td>
                                    <input  style="width:90%"  type="text"  name="dominio" id="dominio"/> 

                                </td>
                                <td align="right">
                                  Nombre: 
                                </td>
                                <td>
                                   <input  style="width:90%" type="text"  name="modelo" id="modelo"/> 
                                </td>
                            </tr>
                            <!--
                            <tr>
                                <td>
                                    Suc. Origen:
                                </td>
                                <td>
                                    <select style="width:90%"  name="origen" id="origen">
                                      <option value="">[Seleccione]</option>
                                    </select>    
                                </td>
                                <td align="right">

                                </td>
                                <td>

                                </td>
                            </tr>
                            -->
                            <tr>
                                <td>
                                    Kilómetros: 
                                </td>
                                <td>
                                    <input  style="width:90%"  type="text"  name="km" id="km"/> 

                                </td>
                                <td align="right">
                                  Categoría:
                                </td>
                                <td>
                                   <input style="width:90%"  type="text"  name="categoria" id="categoria"/> 
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    V: 
                                </td>
                                <td colspan="3">
                                    <label><input type="radio" checked name="v" id="v1" value="v1"/>1/8</label>
                                    <label><input type="radio" name="v" id="v2" value="v2"/>1/4</label>
                                    <label><input type="radio" name="v" id="v3" value="v3"/>3/4</label>
                                    <label><input type="radio" name="v" id="v4" value="v4"/>1/4</label>
                                    <label><input type="radio" name="v" id="v5" value="v5"/>1/2</label>
                                    <label><input type="radio" name="v" id="v6" value="v6"/>5/8</label>
                                    <label><input type="radio" name="v" id="v7" value="v7"/>3/4</label>
                                    <label><input type="radio" name="v" id="v8" value="v8"/>7/8</label>
                                    <label><input type="radio" name="v" id="v9" value="v9"/>C</label>
                                </td>

                            </tr>
                            <tr><td colspan="4">&nbsp;</td></tr>
                            <tr><td colspan="4">&nbsp;</td></tr>
                            <tr>
                                
                                <td align="right">
                                    KM Devolución
                                </td>
                                <td>
                                    <input  style="width:90%" type="text"  name="dmDevolucion"  id="dmDevolucion"/> 
                                </td>
                                <td>
                                    
                                </td>
                                <td>
                                    
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    V Devolución: 
                                </td>
                                <td colspan="3">
                                    <label><input type="radio" name="vd" id="vd1" value="v1"/>1/8</label>
                                    <label><input type="radio" name="vd" id="vd2" value="v2"/>1/4</label>
                                    <label><input type="radio" name="vd" id="vd3" value="v3"/>3/4</label>
                                    <label><input type="radio" name="vd" id="vd4" value="v4"/>1/4</label>
                                    <label><input type="radio" name="vd" id="vd5" value="v5"/>1/2</label>
                                    <label><input type="radio" name="vd" id="vd6" value="v6"/>5/8</label>
                                    <label><input type="radio" name="vd" id="vd7" value="v7"/>3/4</label>
                                    <label><input type="radio" name="vd" id="vd8" value="v8"/>7/8</label>
                                    <label><input type="radio" name="vd" id="vd9" value="v9"/>C</label>
                                </td>

                            </tr>

                            <tr>
                                <td colspan="4">

                                    <table with="100%" border="0">
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>Fecha</td>
                                            <td>Hora</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>Salida</td>
                                            <td><input  style="width:90%" type="text"  name="fechaSalida" id="fechaSalida" placeholder="Fecha Salida"/> </td>
                                            <td><input  style="width:90%" type="text"  name="horaSalida" id="horaSalida" placeholder="Hora Salida"/> </td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>Retorno</td>
                                            <td> <input  style="width:90%" type="text"  name="fechaRetorno" id="fechaRetorno" placeholder="Fecha Retorno"/> </td>
                                            <td><input  style="width:90%" type="text"  name="horaRetorno" id="horaRetorno" placeholder="Hora Retorno"/> </td>
                                            <td>K.M.</td>
                                        </tr>
                                        <tr>
                                            <td>Total</td>
                                            <td><input  style="width:90%" type="text"  name="totalDias" id="totalDias" placeholder="Total de días"/> </td>
                                            <td><input  style="width:90%" type="text"  name="totalHoras" id="totalHoras" placeholder="Total de Horas"/> </td>
                                            <td><input  style="width:90%" type="text"  name="totalKm" id="totalKm" placeholder="Total KM" disabled="true" /></td>
                                        </tr>

                                    </table>



                                </td>


                            </tr>

                        </table>
                      </fieldset>
                    <!-- Fin vehiculo -->
                </td>
                <td valign="top">
                        <table width="98%" border="0">
                            <tr>
                                <td rowspan="2">
                                    
                                </td>
                                <td width="80px" align="right">
                                    Facturas: 
                                </td>
                                <td>
                                    <input  style="width:30px" type="text"  name="letra" id="letra" placeholder="Letra"/>
                                    <input  style="width:60px" type="text"  name="suc" id="suc" placeholder="sucursal"/>
                                    <input  style="width:60px" type="text"  name="numerofac" id="numerofac" placeholder="Nro. Fac"/>
                                </td>
                            </tr>
                            <tr> 
                                <td>
                                    
                                </td>
                                <td>    
                                    <input  style="width:60px;margin-left:41px" type="text"  name="suc2" id="suc2" placeholder="sucursal2"/>
                                    <input  style="width:60px" type="text"  name="numerofac2" id="numerofac2" placeholder="Nro. Fac 2"/>    
                                </td>
                            </tr>

                        </table>
                    
                    <fieldset id="adicionalesx" name="adicionalesx">
                        <legend></legend>
                        
                            
                            <!-- contenido dinámico -->
                            <!-- adicionales gravados -->
                            <div id="adicionalesGravados"></div>
                            <input type="hidden" id="totalAdicG" name="totalAdicG" value="0"/>    
                            
                            
                            
                        <table width="98%" border="0">
                            <tr>
                                <td width="100px">
                                    Diaria
                                </td>
                                <td align="left">
                                    <input  style="width:60px" onchange="javascript:calcularSubtotal1();" type="text"  name="diaria"  id="diaria" placeholder="Diaria"/>      
                                </td>
                                <td width="60px" align="right">
                                    
                                </td>
                            </tr>
                            <tr> 

                                <td>
                                    Cantidad 
                                </td>
                                <td align="left">    
                                    <input  style="width:60px" type="text"  onchange="javascript:calcularSubtotal1();" name="diariaCantidad" id="diariaCantidad" placeholder="Diaria Cantidad"/>      
                                </td>
                                <td align="right">
                                    
                                </td>
                            </tr>

                        </table>    
                            
                            
                        
                        <table width="98%" border="0">
                            <tr>
                                <td>
                                    
                                </td>
                                <td align="right">
                                    <strong>Subtotal 1</strong>
                                </td>
                                <td width="60px" align="right">
                                    <label id="subtotal1" name="subtotal1">0</label>
                                </td>
                            </tr>
                            <tr> 

                                <td>
                                    <!--Tasa Aeropuerto(%)-->  <input  style="width:60px" onchange="javascript:calcularSubtotal2();" type="hidden"  name="tasa"  id="tasa" placeholder="Tasa %" disabled="disabled"/>      
                                </td>
                                <td align="right">    
                                    <!--Tasa APT-->
                                </td>
                                <td align="right">
                                    <input  style="width:60px" type="hidden"  onchange="javascript:calcularSubtotal2();" name="tasaApt" id="tasaApt" placeholder="Tasa APT" disabled="disabled"/>      
                                </td>
                            </tr>

                        </table>
                          <br>
                        <br>
                        <!-- adicionales no gravados -->
                        
                        <div id="adicionalesNoGravados"></div>
                        <input type="hidden" id="totalAdicNG" name="totalAdicNG" value="0"/>    
                        
                       
                         <br>
                        <br>
                        
                        <div id="adicionalesOtros"></div>
                        <input type="hidden" id="totalAdicO" name="totalAdicO" value="0"/>    
                          <br>
                        <br>
                        <!-- fin contenido dinámico -->
                        <table width="98%" border="0">
                            <tr> 

                                <td>
                                  
                                </td>
                                <td align="right">    
                                    <strong>Subtotal 2</strong>
                                </td>
                                <td width="60px" align="right">
                                    <label id="subtotal2" name="subtotal2">0</label>
                                </td>
                            </tr>

                        </table>
                        
                        
                        
                        <!-- fin adicionales no gravados -->
                        
                        
                        <table width="98%" border="0">
                            <tr>
                                <td width="100px">
                                    Anticipo 1     
                                </td>
                                <td>
                                    <input  style="width:100px" type="text"  name="anticipo1" id="anticipo1"  onchange="javascript:calcularSubtotal2();" placeholder="anticipo 1"/>  
                                </td>
                                <td>
                                    
                                </td>
                                <td align="right">    
                                    <strong>Subtotal 3</strong>
                                </td>
                                <td width="60px" align="right">
                                   <label id="subtotal3" name="subtotal3">0</label>
                                </td>
                            </tr>
                            <tr> 

                                <td>
                                  Anticipo 2      
                                </td>
                                <td>    
                                    <input  style="width:100px" type="text"  name="anticipo2" id="anticipo2"  onchange="javascript:calcularSubtotal2();" placeholder="anticipo 2"/> 
                                </td>
                                <td align="right">
                                    
                                </td>
                                <td align="right">
                                    IVA%<input  style="width:50px" type="text"  name="ivaPorcent" id="ivaPorcent" placeholder="iva %" onchange="javascript:calcularSubtotal2();" />
                                </td>
                                <td align="right">
                                    <label id="valorIva" name="valorIva">0</label>
                                </td>
                            </tr>
                            <tr> 

                                <td>
                                  Voucher       
                                </td>
                                <td>    
                                    <input  style="width:100px" type="text"  name="voucher" id="voucher"  onchange="javascript:calcularSubtotal2();" placeholder="Voucher"/>
                                </td>
                                <td>
                                    
                                </td>
                                <td align="right">
                                    Total
                                </td>
                                <td align="right">
                                    <label id="total1" name="total1">0</label>
                                </td>
                            </tr>
                            <tr> 

                                <td>
                                  Reintegros
                                </td>
                                <td>    
                                    <input  style="width:100px" type="text"  name="reintegros" id="reintegros"  onchange="javascript:calcularSubtotal2();" placeholder="Reintegros"/>
                                </td>
                                <td>
                                    
                                </td>
                                <td align="right">
                                    Total Deducciones
                                </td>
                                <td align="right">
                                    <label id="totalDeducciones" name="totalDeducciones">0</label>
                                </td>
                            </tr></tr>
                            <tr> 

                                <td>
                                  
                                </td>
                                <td>    
                                    
                                </td>
                                <td>
                                    
                                </td>
                                <td align="right">
                                    Saldo
                                </td>
                                <td align="right">
                                    <label id="saldo" name="saldo">0</label>
                                </td>
                            </tr>
                        </table>
                        
                     </fieldset>     

                </td>
                
                
            </tr>
            <tr>
                <td colspan="2">
                    
                    <br>
                    <!--Pagos-->
                    
                    <fieldset id="desglosex" name="desglosex">
                    <legend><strong>DESGLOSE DE PAGOS:</strong></legend>
                    
                    <table width="98%" border="0">
                        <tr>
                            <td>
                                Contado
                            </td>
                            <td>
                                Cheque
                            </td>
                            <td>
                                Tarjeta
                            </td>
                            <td>
                                
                            </td>
                            <td>
                                Cta.Cte.
                            </td>
                            <td>
                                Prepagos
                            </td>
                            <td rowspan="6">
                                <label style="margin-left:10px;"><input type="radio" name="estado[]" id="tp1" value="0" onchange="javascript:checkEstado(0);" />Abierto</label><br>
                                <label style="margin-left:10px;"><input type="radio" name="estado[]" id="tp2" value="1" onchange="javascript:checkEstado(1);"/>Cerrado</label><br>
                                <label style="margin-left:10px;"><input type="radio" name="estado[]" id="tp3º" value="2" onchange="javascript:checkEstado(2);"/>Cancelada</label><br>

                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input  style="width:90%" type="text"  name="contado" id="contado" placeholder="" onblur="javascript:validarSaldos();"/>
                            </td>
                            <td>
                                <input  style="width:90%" type="text"  name="cheque" id="cheque" placeholder="" onblur="javascript:validarSaldos();"/>
                            </td>
                            <td>
                                <input  style="width:90%" type="text"  name="tarjeta" id="tarjeta" placeholder="" onblur="javascript:validarSaldos();"/>
                            </td>
                            <td>
                                <select style="width:90%"  name="tarjetaEmisor" id="tarjetaEmisor">
                                  <option value="">Emisor</option>
                                  <option value="1">Visa Débito</option>
                                  <option value="2">Maestro (Mastercad)</option>
                                  <option value="3">American Express Débito</option>
                                  <option value="4">Otras</option>
                                </select>
                            </td>
                            <td>
                                <input  style="width:90%" type="text"  name="ctacte" id="ctacte" placeholder="" onblur="javascript:validarSaldos();"/>
                            </td>
                            <td>
                                <input  style="width:90%" type="text"  name="prepagos" id="prepagos" placeholder="" onblur="javascript:validarSaldos();"/>
                            </td>
                            
                        </tr>
                        <tr>
                            <td colspan="6">
                                <div id="msgSaldos" name="msgSaldos"></div>
                            </td>
                            
                        </tr>
                    </table>
                    
                    </fieldset>
                    <!--Fin pagos-->
                
                </td>
                
                
                <tr>
                <td colspan="2">
                    
                    <br>
                    <!--Observaciones-->
                    
                    <fieldset id="observacionex" name="observacionex">
                    <table width="98%" border="0">
                        <tr>
                            <td width="100px">
                                Observaciones
                            </td>
                            <td>
                                <textarea  style="width:90%" name="observaciones" id="observaciones" placeholder="Observaciones"></textarea>
                            </td>
                            
                        </tr>

                    </table>
                    </fieldset>
                    
                    
                    <!--Fin observaciones-->
                
                    <table width="98%" border="0">
                        <tr>
                            
                            <td>
                                <input type="button" onclick="javascript:enviarDatos();" value="Guardar Datos"/>
                            </td>
                            
                        </tr>
                        <tr>
                            
                            <td>
                                <input type="button" onclick="javascript:enviarDatos(true);" value="Imprimir Datos"/>
                            </td>
                            
                        </tr>
                    
                    
                </td>
                
            </tr>
        </table>
      </fieldset>  
    </form>
