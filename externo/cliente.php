<?PHP
    include_once("config.php");
?>
<!DOCTYPE html>
<html>
    <head>
    <!-- The jQuery library is a prerequisite for all jqSuite products -->
    <script type="text/ecmascript" src="js/jquery.min.js"></script> 
    <!-- We support more than 40 localizations -->
    <script type="text/ecmascript" src="js/trirand/i18n/grid.locale-es.js"></script>
    <!-- This is the Javascript file of jqGrid -->   
    <script type="text/ecmascript" src="js/trirand/jquery.jqGrid.min.js"></script>
    <!-- This is the localization file of the grid controlling messages, labels, etc.
    <!-- A link to a jQuery UI ThemeRoller theme, more than 22 built-in and many more custom -->
    <link rel="stylesheet" type="text/css" media="screen" href="css/jquery-ui.css" />
    <!-- The link to the CSS that the grid needs -->
    <link rel="stylesheet" type="text/css" media="screen" href="css/trirand/ui.jqgrid.css" />
    

    <script type="text/ecmascript" src="js/jquery.json-2.2.min.js"></script>
	<script type="text/ecmascript" src="js/jquery.mask.js"></script>
    <script type="text/ecmascript" src="js/jquery.blockUI.js"></script>
    <script type="text/ecmascript" src="js/jquery.maskedinput.min.js"></script>
    </head>
    <body>
        
    <style type="text/css">

        /* set the size of the datepicker search control for Order Date*/
        #ui-datepicker-div { font-size:11px; }
        
        /* set the size of the autocomplete search control*/
        .ui-menu-item {
            font-size: 11px;
        }

        .ui-autocomplete { font-size: 11px; position: absolute; cursor: default;z-index:5000 !important;}      
        
        
        
    </style>
        
        <table id="jqGrid" align="center"></table>
    <div id="jqGridPager"></div>

    <script type="text/javascript">
        
        
        function cargarAgencias()
        {
            var valoresx = "";
            $.ajax({
                                data: {},
                                type: "POST",
                                dataType: "json",
                                url: "<?=$homesite;?>/web/app_dev.php/agencia/search/",
                                async:false,
                                success: function(data)
                                {


                                                        var arrayValores = data;

                                                        valoresx ="{";
                                                        $.each( arrayValores, function ( userkey, uservalue) 
                                                        {
                                                            if(valoresx == "{")
                                                            {
                                                                //console.log(userkey + ': ' + uservalue.id + ': ' + uservalue.descripcion );
                                                                valoresx += "\"" + uservalue.id + "\":\"" + uservalue.descripcion+"\"";
                                                            }
                                                            else
                                                            {
                                                                valoresx += "," + "\"" + uservalue.id + "\":\"" + uservalue.descripcion+"\"";
                                                            }
                                                        });
                                                        valoresx +="}";
                                                        //valores = data;
                                                        //$.parseJSON(valores);
                
                                                        return valoresx;
                                },
                                error: function(xhr, textStatus, errorThrown) {
                                    // Handle error
                                    alert(errorThrown);
                                    //$.unblockUI(); 
                                  }
                        });
              return valoresx;
       }
       
   
        function cargarTipoDoc()
        {
            var valoresx = "";
            $.ajax({
                                data: {},
                                type: "POST",
                                dataType: "json",
                                url: "<?=$homesite;?>/web/app_dev.php/tipodoc/search/",
                                async:false,
                                success: function(data)
                                {


                                                        var arrayValores = data;

                                                        valoresx ="{";
                                                        $.each( arrayValores, function ( userkey, uservalue) 
                                                        {
                                                            if(valoresx == "{")
                                                            {
                                                                //console.log(userkey + ': ' + uservalue.id + ': ' + uservalue.descripcion );
                                                                valoresx += "\"" + uservalue.id + "\":\"" + uservalue.descripcion+"\"";
                                                            }
                                                            else
                                                            {
                                                                valoresx += "," + "\"" + uservalue.id + "\":\"" + uservalue.descripcion+"\"";
                                                            }
                                                        });
                                                        valoresx +="}";
                                                        //valores = data;
                                                        //$.parseJSON(valores);
                
                                                        return valoresx;
                                },
                                error: function(xhr, textStatus, errorThrown) {
                                    // Handle error
                                    alert(errorThrown);
                                    //$.unblockUI(); 
                                  }
                        });
              return valoresx;
       }
       /*
       function cargarTipoCli()
        {
            var valoresx = "";
            $.ajax({
                                data: {},
                                type: "POST",
                                dataType: "json",
                                url: "<?=$homesite;?>/web/app_dev.php/tipocliente/search/",
                                async:false,
                                success: function(data)
                                {


                                                        var arrayValores = data;

                                                        valoresx ="{";
                                                        $.each( arrayValores, function ( userkey, uservalue) 
                                                        {
                                                            if(valoresx == "{")
                                                            {
                                                                //console.log(userkey + ': ' + uservalue.id + ': ' + uservalue.descripcion );
                                                                valoresx += "\"" + uservalue.id + "\":\"" + uservalue.descripcion+"\"";
                                                            }
                                                            else
                                                            {
                                                                valoresx += "," + "\"" + uservalue.id + "\":\"" + uservalue.descripcion+"\"";
                                                            }
                                                        });
                                                        valoresx +="}";
                                                        //valores = data;
                                                        //$.parseJSON(valores);
                
                                                        return valoresx;
                                },
                                error: function(xhr, textStatus, errorThrown) {
                                    // Handle error
                                    alert(errorThrown);
                                    //$.unblockUI(); 
                                  }
                        });
              return valoresx;
       }
       */

        function cargarProvincias()
        {
            var valoresx = "";
            $.ajax({
                                data: {},
                                type: "POST",
                                dataType: "json",
                                url: "<?=$homesite;?>/web/app_dev.php/provincia/search/",
                                async:false,
                                success: function(data)
                                {


                                                        var arrayValores = data;

                                                        valoresx ="{";
                                                        $.each( arrayValores, function ( userkey, uservalue) 
                                                        {
                                                            if(valoresx == "{")
                                                            {
                                                                //console.log(userkey + ': ' + uservalue.id + ': ' + uservalue.descripcion );
                                                                valoresx += "\"" + uservalue.id + "\":\"" + uservalue.descripcion+"\"";
                                                            }
                                                            else
                                                            {
                                                                valoresx += "," + "\"" + uservalue.id + "\":\"" + uservalue.descripcion+"\"";
                                                            }
                                                        });
                                                        valoresx +="}";
                                                        //valores = data;
                                                        //$.parseJSON(valores);
                
                                                        return valoresx;
                                },
                                error: function(xhr, textStatus, errorThrown) {
                                    // Handle error
                                    alert(errorThrown);
                                    //$.unblockUI(); 
                                  }
                        });
              return valoresx;
       }
        function cargarPaises()
        {
            var valoresx = "";
            $.ajax({
                                data: {},
                                type: "POST",
                                dataType: "json",
                                url: "<?=$homesite;?>/web/app_dev.php/pais/search/",
                                async:false,
                                success: function(data)
                                {


                                                        var arrayValores = data;

                                                        valoresx ="{";
                                                        $.each( arrayValores, function ( userkey, uservalue) 
                                                        {
                                                            if(valoresx == "{")
                                                            {
                                                                //console.log(userkey + ': ' + uservalue.id + ': ' + uservalue.descripcion );
                                                                valoresx += "\"" + uservalue.id + "\":\"" + uservalue.descripcion+"\"";
                                                            }
                                                            else
                                                            {
                                                                valoresx += "," + "\"" + uservalue.id + "\":\"" + uservalue.descripcion+"\"";
                                                            }
                                                        });
                                                        valoresx +="}";
                                                        //valores = data;
                                                        //$.parseJSON(valores);
                
                                                        return valoresx;
                                },
                                error: function(xhr, textStatus, errorThrown) {
                                    // Handle error
                                    alert(errorThrown);
                                    //$.unblockUI(); 
                                  }
                        });
              return valoresx;
       }
       
        var agencias = JSON.parse(cargarAgencias());
        var tiposDoc = JSON.parse(cargarTipoDoc());
        //var tiposCli = JSON.parse(cargarTipoCli());
        
        var provincias = JSON.parse(cargarProvincias());
        var paises = JSON.parse(cargarPaises());
        
        
        //var debaja = {"0":"No","1":"Si"};
        var debaja = {true:"Sí",false:"No"};
    
    
    
    
    $(document).ready(function () {

        jQuery('.ui-icon-refresh').click(function()
        {
            agencias = JSON.parse(cargarAgencias());
            tiposDoc = JSON.parse(cargarTipoDoc());
            //tiposCli = JSON.parse(cargarTipoCli());
            provincias = JSON.parse(cargarProvincias());
            paises = JSON.parse(cargarPaises());
            
        });

              function siNoFmatter (cellvalue, options, rowObject)
              {
                   if(cellvalue == true)
                   {
                       return "Sí";
                   }
                   else
                   {
                       return "No";
                   }
              }


              $("#jqGrid").jqGrid({
              url: '<?=$homesite;?>/web/app_dev.php/cliente/search/',
              editurl: '<?=$homesite;?>/web/app_dev.php/cliente/crear/',
              datatype: "json",
              sortname: 'id',
              sortorder: 'asc',
              mtype: 'POST',
              viewrecords: true,
              
            //comienzo sub grilla  
              "subGridOptions": 
               {
                "plusicon": "ui-icon-triangle-1-e",
                "minusicon": "ui-icon-triangle-1-s",
                "openicon": "ui-icon-arrowreturn-1-e"
               },
               "subGrid": true,
               "subGridRowExpanded": function(subgridid, id) 
                {
                       var data = 
                       {
                           subgrid: subgridid,
                           rowid: id
                       };

                   $("#" + jQuery.jgrid.jqID(subgridid)).load('subGridCliOD.php', data);
                 }
              ,
              //fin de la subgrilla
              gridview:false,
              ondblClickRow: function(rowid) {
                   jQuery(this).jqGrid('editGridRow', rowid,
                   {
                    editCaption: "Editar Usuario",
                    recreateForm: true,
                    closeAfterEdit: true,
                    closeOnEscape:true,
                    "afterSubmit" : function( data, postdata, oper) 
                    {
                        var response = data.responseJSON;
                        if (response.hasOwnProperty("violations")) {
                                if(response.violations.length) 
                                {
                                        return [false,response.violations[0].message];
                                }
                        }
                        
                        if (response.hasOwnProperty("error")) {
                                if(response.error.length) 
                                {
                                        return [false,response.error];
                                }
                        }
                        return [true,"",""];
                    },
                    "errorTextFormat": function (data) {
                        return 'Error: ' + data.responseText
                    }
                   });
                   return false;
               },
               colModel: [
                { 
                  label: 'Id'
                  , name: 'id'
                  , index: 'id'
                  , width: 75 
                  , search: false
                  , hidden: false
                  , sorttype: 'number'
                  ,key: true
                },
                { 
                  label: 'Nombre'
                  , name: 'nombre'
                  , index: 'nombre'
                  , width: 100 
                  ,editable: true
                  ,editrules : { required: true}
                  ,searchoptions: 
                              {
                               sopt: ['cn']
                              }
                }
                ,
                { 
                  label: 'Apellido'
                  , name: 'apellido'
                  , index: 'apellido'
                  , width: 100
                  ,editable: true
                  ,editrules : { required: true}
                  ,searchoptions: 
                              {
                               sopt: ['cn']
                              }
                }
                ,
                { 
                  label: 'Fec. Nacimiento'
                  , name: 'fecha_nac'
                  , index: 'fecha_nac'
                  , width: 100
                  ,editable: true
                  ,edittype: 'text'
                  ,sorttype: 'date', datefmt:'d/mm/Y'
                  ,formatter: function (cellvalue, options, rowObject) {
                    if(cellvalue)
                    {
                        var y = Number(cellvalue.substring(0,4));
                        var m = Number(cellvalue.substring(5,7));
                        var d = Number(cellvalue.substring(8,10));
                        return String(d)+'/'+String(m)+'/'+String(y);
                    }
                    else
                    {
                        return false;
                    }
                  }
                  ,editoptions: {
                            // dataInit is the client-side event that fires upon initializing the toolbar search field for a column
                            // use it to place a third party control to customize the toolbar
                            dataInit: function (element) {
                                $(element).datepicker({
                                    id: 'orderDate_datePicker',
                                    dateFormat: 'd/m/yy',
                                    //minDate: new Date(2010, 0, 1),
                                    //maxDate: new Date(2020, 0, 1),
                                    showOn: 'focus'
                                });
                            }
                        }
                  ,searchoptions: {
                            sopt : ['eq','ne', 'lt', 'gt'],
                            // dataInit is the client-side event that fires upon initializing the toolbar search field for a column
                            // use it to place a third party control to customize the toolbar
                            dataInit: function (element) {
                                $(element).datepicker({
                                    id: 'orderDate_datePicker',
                                    dateFormat: 'd/m/yy',
                                    //minDate: new Date(2010, 0, 1),
                                    //maxDate: new Date(2020, 0, 1),
                                    showOn: 'focus'
                                });
                            }
                        }
                } 
                ,
                { 
                  label: 'Tipo Doc.'
                  , name: 'tipodoc.descripcion'
                  , index: 'tipodoc'
                  , width: 100 
                  ,editable: true
                  ,edittype: 'select'
                  ,stype: 'select'
                  ,editoptions: 
                              {
                                 value:tiposDoc
                              }
                  ,searchoptions: 
                              {
                                 value:tiposDoc
                                ,sopt: ['eq','ne']
                              }
                  ,editrules : { required: true}
                },
                { 
                  label: 'Nro. Doc.'
                  , name: 'numero_doc'
                  , index: 'numero_doc'
                  , width: 100 
                  ,editable: true
                  ,editrules : { required: true}
                  ,searchoptions: 
                              {
                               sopt: ['cn']
                              }
                }
                ,
                { 
                  label: 'Email'
                  ,name: 'email'
                  ,index: 'email'
                  ,width: 255 
                  ,editable: true
                  ,editrules : { required: true, email:true}
                  ,searchoptions: 
                              {
                               sopt: ['cn']
                              }
                }
                ,
                { 
                  label: 'Domicilio'
                  , name: 'domicilio'
                  , index: 'domicilio'
                  , width: 100 
                  ,editable: true
                  ,editrules : { required: true}
                  ,searchoptions: 
                              {
                               sopt: ['cn']
                              }
                },
                { 
                  label: 'Localidad'
                  , name: 'localidad'
                  , index: 'localidad'
                  , width: 100 
                  ,editable: true
                  ,editrules : { required: true}
                  ,searchoptions: 
                              {
                               sopt: ['cn']
                              }
                },
                { 
                  label: 'C.P.'
                  , name: 'codigo_postal'
                  , index: 'codigo_postal'
                  , width: 50
                  ,editable: true
                  ,editrules : { required: true}
                  ,searchoptions: 
                              {
                               sopt: ['cn']
                              }
                }
                ,
                { 
                  label: 'País'
                  , name: 'idpais.descripcion'
                  , index: 'idpais'
                  , width: 100 
                  ,editable: true
                  ,edittype: 'select'
                  ,stype: 'select'
                  ,editoptions: 
                              {
                                 value:paises
                              }
                  ,searchoptions: 
                              {
                                 value:paises
                                ,sopt: ['eq','ne']
                              }
                  ,editrules : { required: true}
                },
                { 
                  label: 'Provincia/Estado'
                  , name: 'provincia.descripcion'
                  , index: 'provincia'
                  , width: 100 
                  ,editable: true
                  ,edittype: 'select'
                  ,stype: 'select'
                  ,editoptions: 
                              {
                                 value:provincias
                              }
                  ,searchoptions: 
                              {
                                 value:provincias
                                ,sopt: ['eq','ne']
                              }
                  ,editrules : { required: true}
                },{ 
                  label: 'De Baja'
                  , name: 'debaja'
                  , index: 'debaja'
                  , width: 100 
                  ,editable: true
                  ,hidden:false
                  ,edittype: 'select'
                  ,stype: 'select'
                  ,formatter:siNoFmatter
                  ,editoptions: { value: debaja , defaultValue:"false"}
                  ,searchoptions: { value: debaja ,sopt: ['eq','ne']}
                  ,editrules : { required: true,edithidden: true}
                  
                  
                }
                // sorttype is used only if the data is loaded locally or loadonce is set to true
                //{ label: 'Quantity', name: 'Quantity', width: 80, sorttype: 'number' }                   
              ],
              width: 'auto',
              height:'auto',
              page:1,
              rowNum: 10,
              rowList: [2,10, 50, 100],
              loadonce: false, // this is just for the demo
              pager: "#jqGridPager"
            });

              $('#jqGrid').navGrid('#jqGridPager',
                // the buttons to appear on the toolbar of the grid
                //Botonera de la grilla
                { 
                  edit: true
                  , add: true //Boton de agregar
                  , del: false //Boton de eliminar
                  , search: true
                  , refresh: true
                  , view: true //Boton de ver datos
                  , position: "left"
                  , cloneToTop: false 
                },
                // options for the Edit Dialog
                {
                    editCaption: "Editar Cliente",
                    recreateForm: true,
                    closeAfterEdit: true,
                    closeOnEscape:true,
                    "afterSubmit" : function( data, postdata, oper) 
                    {
                        var response = data.responseJSON;
                        if (response.hasOwnProperty("violations")) {
                                if(response.violations.length) 
                                {
                                        return [false,response.violations[0].message];
                                }
                        }
                        
                        if (response.hasOwnProperty("error")) {
                                if(response.error.length) 
                                {
                                        return [false,response.error];
                                }
                        }
                        return [true,"",""];
                    },
                    "errorTextFormat": function (data) {
                        return 'Error: ' + data.responseText
                    }
                },
                // options for the Add Dialog
                {
                    addCaption: "Crear Cliente",
                    closeAfterAdd: true,
                    recreateForm: true,
                    closeOnEscape:true,
                    "afterSubmit" : function( data, postdata, oper) 
                    {
                        var response = data.responseJSON;
                        if (response.hasOwnProperty("violations")) {
                                if(response.violations.length) 
                                {
                                        return [false,response.violations[0].message];
                                }
                        }
                        
                        if (response.hasOwnProperty("error")) {
                                if(response.error.length) 
                                {
                                        return [false,response.error];
                                }
                        }
                        
                        return [true,"",""];
                    },
                    "errorTextFormat": function (data) {
                        return 'Error: ' + data.responseText
                    }
                },
                // options for the Delete Dailog
                {
                    url: '0003B.php',
                    datatype: "json",
                   
                    errorTextFormat: function (data) {
                        return 'Error: ' + data.responseText
                    }
                }
                
                );

                $('#jqGrid').jqGrid('setGridHeight',($(window).innerHeight()-80));
                $('#jqGrid').jqGrid('setGridWidth',$(window).width() - 20);
       

});

    /*
     var height = $(window).height();
            $('.ui-jqgrid-bdiv').height(height-100);
            
            var alto = $(window).width();
            $('.ui-jqgrid-bdiv').width(alto);
            alert(alto);
            
            $("#jqGrid").jqGrid({with:100000});
            */
           
           $(window).resize(function(){
    $('#jqGrid').jqGrid('setGridHeight',($(window).innerHeight()-80));
    $('#jqGrid').jqGrid('setGridWidth',($(window).width()-20));
    


    
});
</script>
<script type="text/ecmascript" src="js/jquery-ui.min.js"></script>
<script>
        $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '<Ant',
        nextText: 'Sig>',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
        dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
        };
        $.datepicker.setDefaults($.datepicker.regional['es']);
</script>
    </body>
</html>