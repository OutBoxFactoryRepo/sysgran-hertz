<?PHP
//print_r($_POST);
//die();

/*Array
(
    [idReserva] => 23
    [idContrato] => 
    [sucursal] => 2
    [nombre] => prueba
    [apellido] => prueba
    [fechaNac] => 
    [domicilio] => 
    [telefono] => 
    [email] => 
    [licencia] => 
    [licFecVto] => 
    [tipoTarjeta] => 
    [nroTarjeta] => 
    [tarjFecVto] => 
    [monto] => 
    [codSeg] => 
    [dominio] => 123456
    [modelo] => 1955
    [origen] => 2
    [km] => 125745
    [categoria] => abcd
    [v] => v7
    [fechaHoraDevolucion] => 17/5/2016
    [dmDevolucion] => 125745
    [fechaSalida] => 14/5/2016
    [horaSalida] => 
    [fechaRetorno] => 17/5/2016
    [horaRetorno] => 
    [totalDias] => 
    [totalHoras] => 
    [totalKm] => 0
    [letra] => 
    [suc] => 
    [numerofac] => 
    [suc2] => 
    [numerofac2] => 
    [totalAdic_1] => 0
    [idAdic_0] => 1
    [totalAdic_2] => 0
    [idAdic_1] => 2
    [totalAdic_3] => 0
    [idAdic_2] => 3
    [totalAdic_18] => 0
    [idAdic_3] => 18
    [totalAdicG] => 4
    [tasa] => 
    [tasaApt] => 
    [totalAdicNg_4] => 0
    [idAdicNG_0] => 4
    [totalAdicNg_5] => 0
    [idAdicNG_1] => 5
    [totalAdicNg_6] => 0
    [idAdicNG_2] => 6
    [totalAdicNg_17] => 0
    [idAdicNG_3] => 17
    [totalAdicNG] => 4
    [totalAdicO_7] => 0
    [idAdicO_0] => 7
    [totalAdicO] => 1
    [anticipo1] => 0
    [anticipo2] => 0
    [ivaPorcent] => 
    [voucher] => 0
    [reintegros] => 0
    [estado] => Array
        (
            [0] => 0
        )

    [contado] => 
    [cheque] => 
    [tarjeta] => 
    [tarjetaEmisor] => 
    [ctacte] => 
    [prepagos] => 
    [observaciones] => 
)
*/
/*  [idAdicGrabDescrip_0] => Diaria
    [idAdicGrabValor_0] => 14.37
    [idAdicGrabCantidad_0] => 1
    [totalAdic_2] => 2
    [idAdic_1] => 2
    [idAdicGrabDescrip_1] => Horas Extras
    [idAdicGrabValor_1] => 14.37
    [idAdicGrabCantidad_1] => 2
    [totalAdic_3] => 3
    [idAdic_2] => 3
    [idAdicGrabDescrip_2] => Km Rodado
    [idAdicGrabValor_2] => 17.68
    [idAdicGrabCantidad_2] => 3
    [totalAdic_18] => 4
    [idAdic_3] => 18
    [idAdicGrabDescrip_3] => Prueba gravado
    [idAdicGrabValor_3] => 755
    [idAdicGrabCantidad_3] => 4
    [totalAdicG] => 4*/
/*
[idAdicNoGrabDescrip_0] => Seguro Modificado
    [idAdicNoGrabValor_0] => 200.5
    [idAdicNoGrabCantidad_0] => 7
    [totalAdicNg_5] => 8
    [idAdicNG_1] => 5
    [idAdicNoGrabDescrip_1] => Patente
    [idAdicNoGrabValor_1] => 58.3
    [idAdicNoGrabCantidad_1] => 8
    [totalAdicNg_6] => 9
    [idAdicNG_2] => 6
    [idAdicNoGrabDescrip_2] => Poderes
    [idAdicNoGrabValor_2] => 115.3
    [idAdicNoGrabCantidad_2] => 9
    [totalAdicNg_17] => 10
    [idAdicNG_3] => 17
    [idAdicNoGrabDescrip_3] => Prueba
    [idAdicNoGrabValor_3] => 100
    [idAdicNoGrabCantidad_3] => 10
    [totalAdicNG] => 4
*/
function getDescripSucursal($id)
{
    $respuesta = "Desconocida";
    
    if($id == 2)
    {
        $respuesta = "Iguazú";
    }
    if($id == 3)
    {
        $respuesta = "Posadas";
    }
    if($id == 4)
    {
        $respuesta = "Resistencia";
    }
    return $respuesta;
}
function getTanqueV($valor)
{
    $auxArray = array();
    //<label><input id="v1" name="v" type="radio" value="v1">1/8</label> <label><input id="v2" name="v" type="radio" value="v2">1/4</label> <label>
    //<input id="v3" name="v" type="radio" value="v3">3/4</label> <label>
    //<input id="v4" name="v" type="radio" value="v4">1/4</label> <label>
    //<input id="v5" name="v" type="radio" value="v5">1/2</label> <label>
    //<input id="v6" name="v" type="radio" value="v6">5/8</label> <label>
    //<input id="v7" name="v" type="radio" value="v7">3/4</label> <label>
    //<input id="v8" name="v" type="radio" value="v8">7/8</label> <label><input id="v9" name="v" type="radio" value="v9">C</label>
    $auxArray[1] = ($valor=="v1")? "(X)":"( )";
    $auxArray[2] = ($valor=="v2")? "(X)":"( )";
    $auxArray[3] = ($valor=="v3")? "(X)":"( )";
    $auxArray[4] = ($valor=="v4")? "(X)":"( )";
    $auxArray[5] = ($valor=="v5")? "(X)":"( )";
    $auxArray[6] = ($valor=="v6")? "(X)":"( )";
    $auxArray[7] = ($valor=="v7")? "(X)":"( )";
    $auxArray[8] = ($valor=="v8")? "(X)":"( )";
    $auxArray[9] = ($valor=="v9")? "(X)":"( )";
    return $auxArray;
}

$tanqueV = getTanqueV($_POST["v"]);
$tanqueVD = getTanqueV($_POST["vd"]);
//echo $_POST["v"];
//print_r($tanqueV);die();
$sucursal = getDescripSucursal($_POST["sucursal"]);
$sucursalDestino = getDescripSucursal($_POST["sucursalDestino"]);

$sucursalOrigen = getDescripSucursal($_POST["origen"]);
$nombre = $_POST["nombre"];
$apellido = $_POST["apellido"];

$tipoTarjeta = "";

if($_POST["tipoTarjeta"] == 1)
{
    $tipoTarjeta = "Visa";
}
elseif($_POST["tipoTarjeta"] == 2)
{
    $tipoTarjeta = "Mastercard";
}
elseif($_POST["tipoTarjeta"] == 3)
{
    $tipoTarjeta = "American Express";
}
elseif($_POST["tipoTarjeta"] == 4)
{
    $tipoTarjeta = "Otra";
}

if($_POST["tarjetaEmisor"] == 1)
{
    $tarjetaEmisor = "Visa Débito";
}
elseif($_POST["tarjetaEmisor"] == 2)
{
    $tarjetaEmisor = "Maestro (Mastercard)";
}
elseif($_POST["tarjetaEmisor"] == 3)
{
    $tarjetaEmisor = "American Express (Débito)";
}
elseif($_POST["tarjetaEmisor"] == 4)
{
    $tarjetaEmisor = "Otras";
}

$nroTarjeta = $_POST["nroTarjeta"];

$tarjFecVto = $_POST["tarjFecVtoMes"]."/".$_POST["tarjFecVtoAnio"];//$_POST["tarjFecVto"];


$idContrato = "00000".$_POST["idContrato"];

$subtotal1 = 0;

$valorDiaria = ($_POST["diaria"] * $_POST["diariaCantidad"]);
//adicionales grabados
function getTotalAdicG()
{
    //totalAdicG
    /* cadenaGravados +='<input type="hidden" name="idAdicGrabDescrip_'+uservalue.orden+'" id="idAdicGrabDescrip_'+uservalue.orden+'" value="'+uservalue.descipcion+'"/>';
             cadenaGravados +='<input type="hidden" name="idAdicGrabValor_'+uservalue.orden+'" id="idAdicGrabValor_'+uservalue.orden+'" value="'+uservalue.valor+'"/>';
             cadenaGravados +='<input type="hidden" name="idAdicGrabCantidad_'+uservalue.orden+'" id="idAdicGrabCantidad_'+uservalue.orden+'" value="0"/>';*/
    $cadena = '';
    $cantidadAdicG = $_POST["totalAdicG"];
    for($a = 0;$a<$cantidadAdicG;$a++)
    {
        $totalx = ($_POST["idAdicGrabCantidad_".$a] * $_POST["idAdicGrabValor_".$a]);
        $cadena .= '<tr>
            <td>'.$_POST["idAdicGrabDescrip_".$a].'</td>
            <td>'.$_POST["idAdicGrabValor_".$a].'</td>
            <td>'.$_POST["idAdicGrabCantidad_".$a].'</td>
            <td>'.$totalx.'</td>
       </tr>';
        
       global $subtotal1;
       $subtotal1 = $subtotal1 + $totalx;//;
       //echo $subtotal1;
    }

    $subtotal1 = $subtotal1 + ($_POST["diaria"] * $_POST["diariaCantidad"]);//;
    
    return $cadena;
}
$adicionalesGrabados = getTotalAdicG();


$subtotal2 = 0;
//adicionales no grabados
function getTotalAdicNoG()
{
    $cadena = '';
    global $subtotal2;
    $cantidadAdicNG = $_POST["totalAdicNG"];
    for($a = 0;$a<$cantidadAdicNG;$a++)
    {
        $totalx = ((int)$_POST["idAdicNoGrabCantidad_".$a] * (float)$_POST["idAdicNoGrabValor_".$a]);
        $cadena .='<tr>
                <td>'.$_POST["idAdicNoGrabDescrip_".$a].'</td>

                <td>'.$_POST["idAdicNoGrabValor_".$a].'</td>

                <td>'.$_POST["idAdicNoGrabCantidad_".$a].'</td>

                <td>'.$totalx.'</td>
        </tr>';
        $subtotal2 = $subtotal2 + $totalx;//;
    }
    
    
    return $cadena;
}

$adicionalnesNoGrabados = getTotalAdicNoG();



function getTotalAdicOtros()
{
    $cadena = '';
    global $subtotal2;
    $cantidadAdicOtros = $_POST["totalAdicO"];
    for($a = 0;$a<$cantidadAdicOtros;$a++)
    {
        $totalx = ($_POST["idAdicOtrosCantidad_".$a] * $_POST["idAdicOtrosValor_".$a]);
        $cadena .='<tr>
                <td>'.$_POST["idAdicOtrosDescrip_".$a].'</td>

                <td>'.$_POST["idAdicOtrosValor_".$a].'</td>

                <td>'.$_POST["idAdicOtrosCantidad_".$a].'</td>

                <td>'.$totalx.'</td>
        </tr>';
        $subtotal2 = $subtotal2 + $totalx;//;
    }
    
    //$subtotal2 = $subtotal2 + $totalx;//;
    return $cadena;
}

$adicionalnesOtros = getTotalAdicOtros();
$sumaPorcentajes = 0;
$porcentTasa = (float)$_POST["tasa"];
$porcentTasaApt = (float)$_POST["tasaApt"];
$sumaPorcentajes = ($subtotal2 * $porcentTasa)/100;

$sumaPorcentajes = $sumaPorcentajes + ($subtotal2 * $porcentTasaApt)/100;

$sumaPorcentajes = $sumaPorcentajes;

//echo $subtotal2. " | " .($subtotal2 * $porcentTasa)/100 . " | " .($subtotal2 * $porcentTasaApt)/100;
//die();

$subtotal2 = $subtotal2 + $sumaPorcentajes;
$subtotal2 = round($subtotal2,2);


$subtotal3 = $subtotal1 + $subtotal2;
$porcentIva = $_POST["ivaPorcent"];
$totalIva = round((($subtotal2 + $valorDiaria) * $porcentIva) / 100,2);
$totalSaldos = $subtotal3;// + $totalIva;
//die();
//echo $subtotal1;die();
$reintegros = $_POST["reintegros"];

$anticipo1 = $_POST["anticipo1"];
$anticipo2 = $_POST["anticipo2"];
$voucher = $_POST["voucher"];
$totalDeducciones = $reintegros + $anticipo1 + $anticipo2 + $voucher;
$saldoFinal = $totalSaldos - $totalDeducciones;
$idReserva = "00000".$_POST["idReserva"];
?>
<!DOCTYPE html>
<html>

    <body>
    <table border="0" width="100%">
        <tr><td colspan="3"><br><br><br><br><br><br></td></tr>
        <tr>
            <td align="left">
                <label>NRO RESERVA: <?PHP echo $idReserva;?><br>NRO CONTRATO: <?PHP echo $idContrato;?></label>
            </td>
            <td align="center">
                <img src="/sistema/externo/logowonder.jpg" height="150px" width="400px"/>
            </td>
            <td align="center">
                <br>
                <label>ROJO S.R.L.</label>
                <br>
                <label>CUIT 30-71471050-4</label>
            </td>
        </tr>
    </table>

            <table border="1" width="100%">
                            <tr><td>Sucursal Origen: <?PHP echo $sucursal;?> - Sucursal Destino: <?PHP echo $sucursalDestino;?></td></tr>
            </table>
        
	<table border="1" id="tablaGeneral" width="100%">
		<tbody>
			<tr>
				<td valign="top" width="50%">
						<table border="0" width="100%">
							<tbody>
                                                               <tr>
                                                                   <td colspan="4" align="center"><strong>DATOS DEL CLIENTE</strong></td>

								</tr>
								<tr>
                                                                    <td>Nombre y apellido:</td>
                                                                    <td colspan="3"><?PHP echo $nombre . " " . $apellido;?></td>
									
								</tr>


								<tr>
									<td>Fecha Nac:</td>
                                                                        <td colspan="3"><?PHP echo $_POST["fechaNac"];?></td>
								</tr>

								<tr>
									<td>Domicilio:</td>
                                                                        <td colspan="3"><?PHP echo $_POST["domicilio"];?></td>
									
								</tr>

                                                                
                                                                <tr>
									<td>Teléfono:</td>
                                                                        <td colspan="3"><?PHP echo $_POST["telefono"];?></td>
									
								</tr>

                                                                <tr>
									<td>Email:</td>
                                                                        <td colspan="3"><?PHP echo $_POST["email"];?></td>
									
								</tr>
                                                                
								
								<tr>
									<td>Lic. Nro:</td>
									<td><?PHP echo $_POST["licencia"];?></td>
									<td>Vto:</td>
									<td><?PHP echo $_POST["licFecVto"];?></td>
								</tr>



								<tr>
									<td>Tarjeta</td>
									<td><?PHP echo $tipoTarjeta;?></td>
									<td>Nro:</td>
									<td><?PHP echo $nroTarjeta;?></td>
								</tr>

								<tr>
									<td>Fec. Vto:</td>
									<td><?PHP echo $tarjFecVto;?></td>

									<td></td>

									<td></td>
								</tr>


								<tr>
									<td>Monto</td>

									<td><?PHP echo $_POST["monto"];?></td>

									<td>Cod. Seg:</td>

									<td><?PHP echo $_POST["codSeg"];?></td>
								</tr>
							</tbody>
						</table>
                                    
                                    
                                                <table border="0" width="100%">
							<tbody>
                                                               <tr>
                                                                   <td colspan="4" align="center"><strong>CONDUCTOR ADICIONAL</strong></td>

								</tr>
								<tr>
                                                                    <td>Nombre y apellido:</td>
                                                                    <td colspan="3"><?PHP echo $_POST["nombreConAdic"]. " " .$_POST["apellidoConAdic"];?></td>
									
								</tr>


								<tr>
									<td>Fecha Nac:</td>
                                                                        <td colspan="3"><?PHP echo $_POST["fechaNacConAdic"];?></td>
								</tr>

								<tr>
									<td>Domicilio:</td>
                                                                        <td colspan="3"><?PHP echo $_POST["domicilioConAdic"];?></td>
									
								</tr>

                                                                
                                                                <tr>
									<td>Teléfono:</td>
                                                                        <td colspan="3"><?PHP echo $_POST["telefonoConAdic"];?></td>
									
								</tr>

                                                                <tr>
									<td>Email:</td>
                                                                        <td colspan="3"><?PHP echo $_POST["emailConAdic"];?></td>
									
								</tr>
                                                                
								
								<tr>
									<td>Lic. Nro:</td>
									<td><?PHP echo $_POST["licenciaConAdic"];?></td>
									<td>Vto:</td>
									<td><?PHP echo $_POST["licFecVtoConAdic"];?></td>
								</tr>



							</tbody>
						</table>

						<table border="0" width="100%">
							<tbody>
                                                                <tr>
                                                                    <td colspan="3" align="center"><strong>DATOS DEL VEHICULO</strong></td>
									
								</tr>
								<tr>
                                                                    <td colspan="2">Patente: <?PHP echo $_POST["dominio"];?></td>

									

                                                                    <td colspan="2">Nombre: <?PHP echo $_POST["modelo"];?></td>

									
								</tr>


								<tr>
                                                                    <td colspan="4"><!--Suc. Origen: <?PHP echo $sucursalOrigen;?>--></td>

									

								</tr>


								<tr>
                                                                    <td colspan="2">Kilometros: <?PHP echo $_POST["km"];?></td>

									

                                                                    <td colspan="2">Categoría: <?PHP echo $_POST["categoria"];?></td>

									
								</tr>


								<tr>

                                                                    <td colspan="4">KM Devolución: <?PHP echo $_POST["dmDevolucion"];?></td>

                                                                        
								</tr>
							</tbody>
						</table>
					

                                    <table border="1" width="100%">
                                            <tbody>
                                                    <tr>
                                                            <td>&nbsp;</td>

                                                            <td><strong>Fecha</strong></td>

                                                            <td><strong>Hora</strong></td>

                                                            <td>&nbsp;</td>
                                                    </tr>


                                                    <tr>
                                                        <td><strong>Salida</strong></td>

                                                            <td><?PHP echo $_POST["fechaSalida"];?></td>

                                                            <td><?PHP echo $_POST["horaSalida"];?></td>

                                                            <td>&nbsp;</td>
                                                    </tr>


                                                    <tr>
                                                        <td><strong>Retorno</strong></td>

                                                            <td><?PHP echo $_POST["fechaRetorno"];?></td>

                                                            <td><?PHP echo $_POST["horaRetorno"];?></td>

                                                            <td><strong>K.M.</strong></td>
                                                    </tr>


                                                    <tr>
                                                        <td><strong>Total</strong></td>

                                                            <td><?PHP echo $_POST["totalDias"];?></td>

                                                            <td><?PHP echo $_POST["totalHoras"];?></td>

                                                            <td><?PHP echo $_POST["totalKm"];?></td>
                                                    </tr>
                                            </tbody>
                                    </table>
                                    <table border="1" width="100%">
                                        <tr><td align="center" colspan="10"><strong>COMBUSTIBLE</strong></td></tr>
                                        <tr><td><font size="7">Entrada</font></td><td align="center"><font size="7">1/8<?PHP echo $tanqueV[1];?></font></td><td align="center"><font size="7">1/4<?PHP echo $tanqueV[2];?></font></td><td align="center"><font size="7">3/4<?PHP echo $tanqueV[3];?></font></td><td align="center"><font size="7">1/4<?PHP echo $tanqueV[4];?></font></td><td align="center"><font size="7">1/2<?PHP echo $tanqueV[5];?></font></td><td align="center"><font size="7">5/8<?PHP echo $tanqueV[6];?></font></td><td align="center"><font size="7">3/4<?PHP echo $tanqueV[7];?></font></td><td align="center"><font size="7">7/8<?PHP echo $tanqueV[8];?></font></td><td align="center"><font size="7">C<?PHP echo $tanqueV[9];?></font></td></tr>
                                        <tr><td><font size="7">Salida</font></td><td align="center"><font size="7">1/8<?PHP echo $tanqueVD[1];?></font></td><td align="center"><font size="7">1/4<?PHP echo $tanqueVD[2];?></font></td><td align="center"><font size="7">3/4<?PHP echo $tanqueVD[3];?></font></td><td align="center"><font size="7">1/4<?PHP echo $tanqueVD[4];?></font></td><td align="center"><font size="7">1/2<?PHP echo $tanqueVD[5];?></font></td><td align="center"><font size="7">5/8<?PHP echo $tanqueVD[6];?></font></td><td align="center"><font size="7">3/4<?PHP echo $tanqueVD[7];?></font></td><td align="center"><font size="7">7/8<?PHP echo $tanqueVD[8];?></font></td><td align="center"><font size="7">C<?PHP echo $tanqueVD[9];?></font></td></tr>
                                    </table>
                                        
				</td>

				<td valign="top">
					<table border="0" width="98%">
						<tbody>
							<tr>
								<td rowspan="2"></td>

								<td align="right" width="80px">Facturas:</td>

								<td>Letra: <?PHP echo $_POST["letra"];?> Sucursal: <?PHP echo $_POST["sucursal"];?> Nº Fac: <?PHP echo $_POST["numerofac"];?></td>
							</tr>

							<tr>
								<td></td>

								<td>Suc. 2: <?PHP echo $_POST["suc2"];?> Nro. Fac2: <?PHP echo $_POST["numerofac2"];?></td>
							</tr>
						</tbody>
					</table>


					
						
							<table class="bordeTablaAdic" width="100%" border="1">
								<tbody>
									<tr>
										<th bgcolor="#D0D0D0">Cargos</th>

										<th bgcolor="#D0D0D0">Diario</th>

										<th bgcolor="#D0D0D0">Total Días</th>

										<th bgcolor="#D0D0D0">Total</th>
									</tr>
									<?php echo $adicionalesGrabados;?>
								</tbody>
							</table>
						
						<br>
						<br>

                                                <table border="0" width="100%">
							<tbody>
                                                            
								<tr>
									<td>Diaria: <?PHP echo $_POST["diaria"];?> </td>
                                                                        <td align="right" colspan="2"><strong></strong></td>
									
								</tr>
								<tr>
									<td>Cantidad Diarias <?PHP echo $_POST["diariaCantidad"];?></td>
                                                                        <td colspan="2"></td>
									
								</tr>
							</tbody>
						</table>
                                    
                                                <br>
						<br>
                                    
						<table border="0" width="100%">
							<tbody>
								<tr>
									<td></td>
                                                                        <td align="right" colspan="2"><strong>Subtotal 1: <?PHP echo $subtotal1;?></strong></td>
									
								</tr>
								
							</tbody>
						</table>
						<br>
						<br>
						<!-- adicionales no gravados -->


						
							<table width="100%" border="1">
								<tbody>
									<tr>
										<th bgcolor="#D0D0D0">Cargos</th>

										<th bgcolor="#D0D0D0">Diario</th>

										<th bgcolor="#D0D0D0">Total Días</th>

										<th bgcolor="#D0D0D0">Total</th>
									</tr>


									<?PHP echo $adicionalnesNoGrabados;?>
								</tbody>
							</table>
						
                                                
                                                
                                                
                                                <table border="0" width="98%">
							<tbody>
								<tr>
									<td></td>
									<td align="right"></td>

									<td align="right" width="60px"></td>
								</tr>


								<tr>
									<td></td>

									<td align="right"></td>

									<td align="right"></td>
								</tr>
							</tbody>
						</table>
                                                
                                                
						
							<table width="100%" border="1">
								<tbody>
									<tr>
										<th bgcolor="#D0D0D0">Cargos</th>

										<th bgcolor="#D0D0D0">Diario</th>

										<th bgcolor="#D0D0D0">Total Días</th>

										<th bgcolor="#D0D0D0">Total</th>
									</tr>

                                                                        <?PHP echo $adicionalnesOtros;?>
									
								</tbody>
							</table>
						
                                                

						
						
                                                <table border="0" width="100%">
							<tbody>
								<tr>
									<td></td>
                                                                        <td align="right" colspan="2"><strong>Subtotal 2: <?PHP echo $subtotal2;?></strong></td>
									
								</tr>
								<tr>
									<td></td>
                                                                        <td></td>
									
								</tr>
							</tbody>
						</table>

                                                
                                                
						<table border="1" width="98%">
							<tbody>
								<tr>
									<td width="100px">Anticipo 1</td>

									<td><?PHP echo $_POST["anticipo1"];?></td>

									<td></td>

									<td align="right"><strong>Subtotal 3</strong></td>

									<td align="right" width="60px"><label id="subtotal3"><?PHP echo $subtotal3;?></label></td>
								</tr>


								<tr>
									<td>Anticipo 2</td>

									<td><?PHP echo $_POST["anticipo2"];?></td>

									<td align="right">
									</td>

									<td align="right">IVA% <?PHP echo $porcentIva;?></td>

									<td align="right"><label><?PHP echo $totalIva;?></label>
									</td>
								</tr>


								<tr>
									<td>Voucher</td>

									<td><?PHP echo $_POST["voucher"];?></td>

									<td>
									</td>

									<td align="right">Total</td>

									<td align="right"><label id="total1"><?PHP echo $totalSaldos;?></label>
									</td>
								</tr>


								<tr>
									<td>Reintegros</td>

									<td><?PHP echo $reintegros;?></td>

									<td>
									</td>

									<td align="right">Deducciones</td>

									<td align="right"><label id="totalDeducciones"><?PHP echo $totalDeducciones;?></label>
									</td>
								</tr>


								<tr>
									<td>
									</td>

									<td>
									</td>

									<td>
									</td>

									<td align="right">Saldo</td>

									<td align="right"><label id="saldo"><?PHP echo $saldoFinal;?></label>
									</td>
								</tr>
							</tbody>
						</table>
					
				</td>
			</tr>


			<tr>
				<td colspan="2">
					<br>
					<!--Pagos-->


					<fieldset>
						<legend><strong>DESGLOSE DE PAGOS:</strong></legend>

						<table border="0" width="98%">
							<tbody>
								<tr>
									<td>Contado</td>

									<td>Cheque</td>

									<td>Tarjeta</td>

									<td>Emisor</td>

									<td>Cta.Cte.</td>

									<td>Prepagos</td>

									<td rowspan="6"></td>
								</tr>


								<tr>
									<td><?PHP echo $_POST["contado"];?></td>

									<td><?PHP echo $_POST["cheque"];?></td>

									<td><?PHP echo $_POST["tarjeta"];?></td>

									<td><?PHP echo $tarjetaEmisor;?></td>

									<td><?PHP echo $_POST["ctacte"];?></td>

									<td><?PHP echo $_POST["prepagos"];?></td>
								</tr>
							</tbody>
						</table>
					</fieldset>
					<!--Fin pagos-->
				</td>
			</tr>


			<tr>
				<td colspan="2">
					<br>
					<!--Observaciones-->


					<table border="0" width="98%">
						<tbody>
							<tr>
								<td width="100px">Observaciones</td>

								<td>
								<?PHP echo $_POST["observaciones"];?>
								</td>
							</tr>
						</tbody>
					</table>
					

					
				</td>
			</tr>
		</tbody>
	</table>
        <table border="0" width="100%">
        
        <tr>
            <td align="center">
        <img src="/sistema/externo/pie.jpg"/>        
            </td>
        
        </tr>
    </table>
</body>
</html>
