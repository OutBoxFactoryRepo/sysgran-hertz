<?php
include("config.php");
$subgrid = $_POST["subgrid"];
$rowId = $_POST["rowid"];
$taller = $_POST["taller"];
$tallerExterno = $_POST["tallerexterno"];

//echo $rowId;die();
//print_r($_REQUEST);die();
//$tabla = "<table><tbody><tr><td><b>First Name</b></td><td>Janet</td><td rowspan='9' valign='top'><img src='images/3.jpg'/></td></tr><tr><td><b>Last Name</b></td><td>Leverling</td></tr><tr><td><b>Title</b></td><td>Sales Representative</td></tr><tr><td><b>Title of Courtesy</b></td><td>Ms.</td></tr><tr><td><b>Birth Date</b></td><td>1963-08-30 00:00:00</td></tr><tr><td><b>Hire Date</b></td><td>1992-04-01 00:00:00</td></tr><tr><td><b>Address</b></td><td>722 Moss Bay Blvd.</td></tr><tr><td><b>City</b></td><td>Kirkland</td></tr><tr><td><b>Postal Code</b></td><td>98033</td></tr></tbody></table>";
//echo $tabla;
?>
    <style>
        .ui-jqgrid .subgrid-data .ui-th-column {
            background: #819FF7
        }
    </style>

    <h1>Voucher</h1>
    <div id="grillaOT" style="width:100%;">
        <table width="98%" border="0">


            <tr>
                <td align="center"><img src="<?= $homesite; ?>/externo/voucher.jpg"/></td>
            </tr>
            <tr>
                <td align="center"><h1>ORDEN DE TRABAJO: <label id="nroOt"></label></h1></td>
            </tr>
        </table>
        </br>
        <table width="98%" border="1">

            <tr>
                <td width="20%">FECHA</td>
                <td width="80%"><label id="fecha"></label></td>
            </tr>
            <tr>
                <td>TITULO</td>

                <td><label id="titulo"></label></td>
            </tr>
            <tr>
                <td>TALLER</td>

                <td><label id="taller"></label></td>
            </tr>
            <tr>
                <td>VEHÍCULO - DOMINIO</td>

                <td><label id="vehiculo"></label></td>
            </tr>
            <tr>
                <td>TAREAS:</td>
                <td><label id="tareas"></label></td>
            </tr>
            <tr>
                <td height="100">FIRMA:</td>
                <td></td>
            </tr>

        </table>

    </div>


    <table width="98%" border="1">
        <tr align="right">
            <td colspan="3"><input type="button" onclick="javascript:imprimir();" value="Imprimir Voucher"/></td>

        </tr>
    </table>
    <form id="pdf" name="pdf" action="../web/app_dev.php/ordentrabajo/pdf/" method="POST" target="_prueba">
        <input type="hidden" id="html" name="html" value=""/>
        <input type="hidden" id="idOt" name="idOt" value="<?= $rowId; ?>"/>
    </form>

<?PHP
if ($tallerExterno == "No") {
    ?>

    <h1>Materiales Cosumidos</h1>
    <table id='<?= $subgrid; ?>_t'></table>
    <div id='<?= $subgrid; ?>_t_p'></div>
    <?PHP
}
?>
    <script type='text/javascript'>

    function sleep(delay) {
        var start = new Date().getTime();
        while (new Date().getTime() < start + delay);
      }

        function imprimir() {
            
            var tareasError = verificarTareas(<?=$rowId;?>);
            if(tareasError == "")
            {
                
                ordenTrabajo = cargarOT(<?=$rowId;?>);
                tareas = ordenTrabajo.ot_reparaciones_tareas;
                result = '';
                for (var i = 0; i < tareas.length; i++) {
                    result += tareas[i].id_tarea.descripcion + '<br>';
                }
                $("#tareas").html(result);
                $("#html").val($("#grillaOT").html());
                //$("#pdf").submit();
                var formx = document.getElementById("pdf");
                
                //alert(ordenTrabajo.taller.externo);
                if(ordenTrabajo.taller.externo)
                {
                    formx.submit();
                    sleep(5000);
                    $('#jqGrid').jqGrid('clearGridData');
                    $('#jqGrid').jqGrid('clearGridData');
                    $('#jqGrid').jqGrid('clearGridData');
                    $('#jqGrid').jqGrid('clearGridData');
                    $('#jqGrid').jqGrid('clearGridData');
                    $("#jqGrid").trigger('reloadGrid');
                                    }
                else
                {
                   //sleep(3000);
                   formx.submit();
                   sleep(5000);
                 $('#jqGrid').jqGrid('clearGridData');
                 $('#jqGrid').jqGrid('clearGridData');
                 $('#jqGrid').jqGrid('clearGridData');
                 $('#jqGrid').jqGrid('clearGridData');
                 $("#jqGrid").trigger('reloadGrid');
                    //}
                    //$("#jqGrid").toggleSubGridRow(<?=$rowId;?>);
                    //$("#jqGrid").expandSubGridRow(<?=$rowId;?>);
                }
            }
            else
            {
                alert(tareasError);
            }
            
        }

        function verificarTareas(idOt) {
            var valoresx = "";
            $.ajax({
                data: {idOt:idOt},
                type: "POST",
                dataType: "json",
                url: "<?=$homesite;?>/web/app_dev.php/ordentrabajo/verificartareas/",
                async: false,
                success: function (data) {

                    valoresx = data.error;

                    return valoresx;
                },
                error: function (xhr, textStatus, errorThrown) {
                    // Handle error
                    alert(errorThrown);
                    //$.unblockUI();
                }
            });
            return valoresx;
        }


        function cargarMateriaPrima() {
            var valoresx = "";
            $.ajax({
                data: {taller:<?=$taller;?>},
                type: "POST",
                dataType: "json",
                url: "<?=$homesite;?>/web/app_dev.php/materiaprima/search/",
                async: false,
                success: function (data) {


                    var arrayValores = data.rows;

                    valoresx = "{";
                    $.each(arrayValores, function (userkey, uservalue) {
                        if (valoresx == "{") {
                            //console.log(userkey + ': ' + uservalue.id + ': ' + uservalue.descripcion );
                            valoresx += "\"" + uservalue.id + "\":\"" + uservalue.descripcion + "\"";
                        }
                        else {
                            valoresx += "," + "\"" + uservalue.id + "\":\"" + uservalue.descripcion + "\"";
                        }
                    });
                    valoresx += "}";
                    //valores = data;
                    //$.parseJSON(valores);

                    return valoresx;
                },
                error: function (xhr, textStatus, errorThrown) {
                    // Handle error
                    alert(errorThrown);
                    //$.unblockUI();
                }
            });
            return valoresx;
        }


        function cargarOT(idOt) {
            var valoresx = "";
            $.ajax({
                data: {taller:<?=$taller;?>},
                type: "GET",
                dataType: "json",
                url: "<?=$homesite;?>/web/app_dev.php/ordentrabajo/" + idOt + "/search/",
                async: false,
                success: function (data) {

                    valoresx = data[0];
                    return valoresx;
                },
                error: function (xhr, textStatus, errorThrown) {
                    // Handle error
                    alert(errorThrown);
                    //$.unblockUI();
                }
            });
            return valoresx;
        }


        var materiasPrimas = JSON.parse(cargarMateriaPrima());
        var ordenTrabajo = cargarOT(<?=$rowId;?>);

        function getCategorias() {
            var valoresx = "";
            $.ajax({
                data: {},
                type: "POST",
                dataType: "json",
                url: "<?=$homesite;?>/web/app_dev.php/materiaprimacategoria/search/",
                async: false,
                success: function (data) {
                    var arrayValores = data;

                    valoresx = "{";
                    $.each(arrayValores, function (userkey, uservalue) {
                        if (valoresx == "{") {
                            //console.log(userkey + ': ' + uservalue.id + ': ' + uservalue.descripcion );
                            valoresx += "\"" + uservalue.id + "\":\"" + uservalue.nombre + "\"";
                        }
                        else {
                            valoresx += "," + "\"" + uservalue.id + "\":\"" + uservalue.nombre + "\"";
                        }
                    });
                    valoresx += "}";
                    //valores = data;
                    //$.parseJSON(valores);

                    return valoresx;
                },
                error: function (xhr, textStatus, errorThrown) {
                    // Handle error
                    alert(errorThrown);
                    //$.unblockUI();
                }
            });
            return valoresx;
        }
        var categorias = JSON.parse(this.getCategorias());

        $("#nroOt").html(ordenTrabajo.id);
        $("#titulo").html(ordenTrabajo.descripcion);
        $("#taller").html(ordenTrabajo.taller.descripcion);
        $("#vehiculo").html(ordenTrabajo.vehiculo.descripcion + " - " + ordenTrabajo.vehiculo.dominio);

        var date = new Date(ordenTrabajo.fecha_carga),
            yr = date.getFullYear(),
            month = date.getMonth() < 10 ? '0' + date.getMonth() : date.getMonth(),
            day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate(),
            newDate = day + '-' + month + '-' + yr;
        $("#fecha").html(newDate);
        var tareas = ordenTrabajo.ot_reparaciones_tareas;
        var result = '';
        for (var i = 0; i < tareas.length; i++) {
            result += tareas[i].id_tarea.descripcion + '<br>'
        }
        $("#tareas").html(result);
        //atención los parámetros de la subgrilla van por get en symfony al recibir los parámetros

        <?PHP
        if($tallerExterno == "No")
        {
        ?>
        jQuery(document).ready(function ($) {
            jQuery('#<?=$subgrid;?>_t').jqGrid({
                "hoverrows": false,
                "viewrecords": true,
                pginput: false,
                pgbuttons: false,


                "jsonReader": {
                    "repeatitems": false,
                    "subgrid": {
                        "repeatitems": false
                    }
                },
                "xmlReader": {
                    "repeatitems": false,
                    "subgrid": {
                        "repeatitems": false
                    }
                },

                gridview: false,
                "url": "<?=$homesite;?>/web/app_dev.php/materiaprimacons/search/",
                "editurl": "<?=$homesite;?>/web/app_dev.php/materiaprimacons/crear/?idOt=<?=$rowId;?>",
                "cellurl": "subReserva.php",
                "width": 540,
                "rowNum": 10,
                "sortname": "id",
                "height": 110,
                "postData": {
                    "subgrid": "<?=$subgrid;?>",
                    "rowid": "<?=$rowId;?>",
                    "ot": "<?=$rowId;?>",
                    "id": "<?=$rowId;?>",
                    "taller": "<?=$taller;?>",
                    "oper": "grid"
                },
                "datatype": "json",

                ondblClickRow: function (rowIdCliOd) {
                    jQuery('#<?=$subgrid;?>_t').jqGrid('editGridRow', rowIdCliOd,
                        {
                            editCaption: "Editar Dato",
                            recreateForm: true,
                            closeAfterEdit: true,
                            closeOnEscape: true,
                            top: 10,
                            "afterSubmit": function (data, postdata, oper) {
                                var response = data.responseJSON;
                                if (response.hasOwnProperty("violations")) {
                                    if (response.violations.length) {
                                        return [false, response.violations[0].message];
                                    }
                                }

                                if (response.hasOwnProperty("error")) {
                                    if (response.error.length) {
                                        return [false, response.error];
                                    }
                                }

                                $("#jqGrid").toggleSubGridRow(<?=$rowId;?>);

                                $("#jqGrid").expandSubGridRow(<?=$rowId;?>);

                                return [true, "", ""];
                            },
                            "errorTextFormat": function (data) {
                                return 'Error: ' + data.responseText;
                            }
                        });
                    return false;
                },

                "colModel": [{
                    "label": "Id",
                    "name": "id",
                    "index": "id",
                    "sorttype": "int",
                    "key": true,
                    "editable": false
                }, {
                    "label": "Descripci&oacute;n",
                    "name": "descripcion",
                    "index": "descripcion",
                    "sorttype": "string",
                    "editable": true
                }, {
                label: 'Categoría'
                , name: 'materiaprima.categoria.id'
                , index: 'materiaprima.categoria.id'
                , width: 255
                , hidden: true
                , editable: true
                , edittype: 'select'
                , stype: 'select'
                , editoptions: {value: categorias}
                , searchoptions: {
                    value: categorias
                    , sopt: ['eq', 'ne']
                }
                ,editrules: {required: false, edithidden: true}
            }, {
                label: 'materiaprima.id'
                , name: 'materiaprima.id'
                , index: 'materiaprima.id'
                , width: 255
                , hidden: true
                , editable: true
                ,editrules: {required: false, edithidden: false}
            },
                    {
                        "label": "Materia Prima",
                        "name": "materiaprima.descripcion",
                        "index": "materiaprima.descripcion",
                        //jsonmap:"tipo_cliente.0.descripcion",
                        editable: true,
                        editrules : {required: true},
                        /*dfdsf*/
                        "editoptions" :
                            {
                                "dataInit":
                                    function (el)
                                    {
                                        setTimeout(function() {
                                            if (jQuery.ui) {
                                                if (jQuery.ui.autocomplete) {
                                                    jQuery(el).autocomplete({
                                                        "appendTo": "body",
                                                        "disabled": false,
                                                        "delay": 300,
                                                        "minLength": 3,
                                                        select: function (event, ui) {
                                                            //document.getElementById("vehiculo.id").value = ui.item.vehiculoId;
                                                            document.getElementById("materiaprima.id").value = ui.item.materiaPrimaId;
                                                        },
                                                        "source": function (request, response) {
                                                            var e = document.getElementById("materiaprima.categoria.id");
                                                            cat_id = e.options[e.selectedIndex].value;
                                                            $.ajax({
                                                                url: "<?=$homesite;?>/web/app_dev.php/materiaprima/searchbycategory/",
                                                                dataType: "json",
                                                                data: {
                                                                    term : request.term,
                                                                    categoria_id : cat_id,
                                                                },
                                                                type: "GET",
                                                                async:false,
                                                                error:
                                                                function (res, status) {
                                                                    alert(res.status + " : " + res.statusText + ". Status: " + status);
                                                                },
                                                                success: function (data) {
                                                                    //si no devuelve datos setea el campo oculto del id de la categoria en ""
                                                                    if (data.length < 1) {
                                                                        document.getElementById("materiaprima.id").value = "";
                                                                    } else {

                                                                    }
                                                                    response(data);
                                                                }
                                                            });
                                                        }
                                                    });
                                                    jQuery(el).autocomplete('widget').css('font-size', '11px');
                                                    jQuery(el).autocomplete('widget').css('z-index', '1000');
                                                }
                                            }
                                        }, 200);
                                    }
                            }
                        /*dfdsf*/

                },
            {
                "label"
            :
                "Cantidad",
                    "name"
            :
                "cantidad",
                    "index"
            :
                "cantidad",
                    "sorttype"
            :
                "number",
                    "editable"
            :
                true
            }
            ],
            "prmNames"
            :
            {
                "page"
            :
                "page",
                    "rows"
            :
                "rows",
                    "sort"
            :
                "sidx",
                    "order"
            :
                "sord",
                    "search"
            :
                "_search",
                    "nd"
            :
                "nd",
                    "id"
            :
                "id",
                    "filter"
            :
                "filters",
                    "searchField"
            :
                "searchField",
                    "searchOper"
            :
                "searchOper",
                    "searchString"
            :
                "searchString",
                    "oper"
            :
                "oper",
                    "query"
            :
                "grid",
                    "addoper"
            :
                "add",
                    "editoper"
            :
                "edit",
                    "deloper"
            :
                "del",
                    "excel"
            :
                "excel",
                    "subgrid"
            :
                "subgrid",
                    "totalrows"
            :
                "totalrows",
                    "autocomplete"
            :
                "autocmpl"
            }
            ,
            "loadError"
            :
            function (xhr, status, err) {
                try {
                    jQuery.jgrid.info_dialog(jQuery.jgrid.errors.errcap, '<div class="ui-state-error">' + xhr.responseText + '</div>', jQuery.jgrid.edit.bClose, {
                        buttonalign: 'right'
                    });
                } catch (e) {
                    alert(xhr.responseText);
                }
            }

            ,
            "pager"
            :
            "#<?=$subgrid;?>_t_p"
        })
            ;
            jQuery('#<?=$subgrid;?>_t').jqGrid('navGrid', '#<?=$subgrid;?>_t_p', {
                    "edit": true,
                    "add": true,
                    "del": true,
                    "search": false,
                    "refresh": true,
                    "view": false,
                    "excel": true,
                    "pdf": false,
                    "csv": false,
                    "columns": false
                },
                //edit option
                {
                    "drag": true,
                    "resize": true,
                    "closeOnEscape": true,
                    "dataheight": 150,
                    "closeAfterEdit": true,
                    top: 10,
                    "afterSubmit": function (data, postdata, oper) {
                        var response = data.responseJSON;
                        if (response.hasOwnProperty("violations")) {
                            if (response.violations.length) {
                                return [false, response.violations[0].message];
                            }
                        }

                        if (response.hasOwnProperty("error")) {
                            if (response.error.length) {
                                return [false, response.error];
                            }
                        }

                        $("#jqGrid").toggleSubGridRow(<?=$rowId;?>);

                        $("#jqGrid").expandSubGridRow(<?=$rowId;?>);

                        return [true, "", ""];

                    },
                    "errorTextFormat": function (data) {
                        return 'Error: ' + data.responseText
                    },
                    "beforeShowForm": function (form) {
                        //$('#tr_nombre', form).hide();
                        //$('#tr_apellido', form).hide();
                        //$('#tr_fecha_nac', form).hide();
                        //$("#<?=$subgrid;?>_t").jqGrid('setColProp', 'ot.id', {editrules: {required: false}});
                    }


                },
                //create option
                {
                    "drag": true,
                    "resize": true,
                    "closeOnEscape": true,
                    "dataheight": 150,
                    top: 10,
                    "closeAfterAdd": true,
                    "afterSubmit": function (data, postdata, oper) {
                        var response = data.responseJSON;
                        if (response.hasOwnProperty("violations")) {
                            if (response.violations.length) {
                                return [false, response.violations[0].message];
                            }
                        }

                        if (response.hasOwnProperty("error")) {
                            if (response.error.length) {
                                return [false, response.error];
                            }
                        }
                        $("#jqGrid").toggleSubGridRow(<?=$rowId;?>);

                        $("#jqGrid").expandSubGridRow(<?=$rowId;?>);
                        return [true, "", ""];

                    },
                    "errorTextFormat": function (data) {
                        return 'Error: ' + data.responseText
                    }
                }
                ,
                // options for the Delete Dailog
                {
                    url: "<?=$homesite;?>/web/app_dev.php/materiaprimacons/crear/?idOt=<?=$rowId;?>",
                    datatype: "json",
                    top: 10,
                    "afterSubmit": function (data, postdata, oper) {
                        var response = data.responseJSON;
                        if (response.hasOwnProperty("violations")) {
                            if (response.violations.length) {
                                return [false, response.violations[0].message];
                            }
                        }

                        if (response.hasOwnProperty("error")) {
                            if (response.error.length) {
                                return [false, response.error];
                            }
                        }

                        $("#jqGrid").toggleSubGridRow(<?=$rowId;?>);

                        $("#jqGrid").expandSubGridRow(<?=$rowId;?>);

                        return [true, "", ""];
                    },
                    "errorTextFormat": function (data) {
                        return 'Error: ' + data.responseText
                    }
                }, {
                    "drag": true,
                    "closeAfterSearch": true,
                    "multipleSearch": true
                }, {
                    "drag": true,
                    "resize": true,
                    "closeOnEscape": true,
                    "dataheight": 150
                });
        });


        $('#<?=$subgrid;?>_t').jqGrid('setGridWidth', $(window).width() - 80);


        $(window).resize(function () {
            $('#<?=$subgrid;?>_t').jqGrid('setGridWidth', $(window).width() - 80);
        });

        <?PHP
        }
        ?>
    </script>
<?PHP
include_once("subGridOtBis.php");
?>