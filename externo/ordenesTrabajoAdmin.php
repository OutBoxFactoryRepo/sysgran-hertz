<?PHP
    include_once("config.php");
?>
<!DOCTYPE html>
<html>
    <head>
    <!-- The jQuery library is a prerequisite for all jqSuite products -->
    <script type="text/ecmascript" src="js/jquery.min.js"></script> 
    <!-- We support more than 40 localizations -->
    <script type="text/ecmascript" src="js/trirand/i18n/grid.locale-es.js"></script>
    <!-- This is the Javascript file of jqGrid -->   
    <script type="text/ecmascript" src="js/trirand/jquery.jqGrid.min.js"></script>
    <!-- This is the localization file of the grid controlling messages, labels, etc.
    <!-- A link to a jQuery UI ThemeRoller theme, more than 22 built-in and many more custom -->
    <link rel="stylesheet" type="text/css" media="screen" href="css/jquery-ui.css" />
    <!-- The link to the CSS that the grid needs -->
    <link rel="stylesheet" type="text/css" media="screen" href="css/trirand/ui.jqgrid.css" />
    

    <script type="text/ecmascript" src="js/jquery.json-2.2.min.js"></script>
  <script>
  $( function() {
    $( "#tabs" ).tabs();
  } );
  </script>
    </head>
    <body>

        
        
    <style type="text/css">

        /* set the size of the datepicker search control for Order Date*/
        #ui-datepicker-div { font-size:11px; }
        
        /* set the size of the autocomplete search control*/
        .ui-menu-item {
            font-size: 11px;
        }

        .ui-autocomplete { font-size: 11px; position: absolute; cursor: default;z-index:5000 !important;}      
        
         .ui-tabs .ui-tabs-panel {
	display: block;
	border-width: 0;
	padding-left: 5px;
	padding-top: 5px;
	background: none;
        }
        
    </style>
        
    
    <div id="tabs" width="100%">
        <ul>
          <li><a href="#tabs-1">Órdenes de Trabajo</a></li>
          <li><a href="#tabs-2">Pagos X Proveedor</a></li>
        </ul>
        <div id="tabs-1">
            <table id="fechasOT"  class="ui-jqgrid"  align="center">
                <tr>
                    <td>Desde</td>
                    <td>
                        <input type="text" id="fDesdeOT">
                    </td>
                    <td>Hasta</td>
                    <td>
                        <input type="text" id="fHastaOT">
                    </td>
                </tr>
            </table>
                <table id="jqGrid" align="center"></table>
                <div id="jqGridPager"></div>
        </div>
        <div id="tabs-2">
            <table id="fechasPP"  class="ui-jqgrid"  align="center">
                <tr>
                    <td>Desde</td>
                    <td>
                        <input type="text" id="fDesdePP">
                    </td>
                    <td>Hasta</td>
                    <td>
                        <input type="text" id="fHastaPP">
                    </td>
                </tr>
            </table>
          <table id="jqGrid2" align="center"></table>
          <div id="jqGrid2Pager"></div>
        </div>

    </div>
    
      

    <script type="text/javascript">
        
        var scrollPosition = 0;
        var ids = [];
        
        function RefreshGridData(idGrilla) 
        {
            //alert(idGrilla);
            var num;
            ids = new Array();
            $("#jqGrid"+idGrilla+" tr:has(.sgexpanded)").each(function () {
                num = $(this).attr('id');
                ids.push(num);
            });
            $("#jqGrid"+idGrilla+"").trigger("reloadGrid");
        }
        
        function RefreshGridDataGroup() 
        {
             //alert(idGrilla);
            var num;
            ids = new Array();
            $("#jqGrid tr:has(.sgexpanded)").each(function () {
                num = $(this).attr('id');
                ids.push(num);
            });
            $("#jqGrid").trigger("reloadGrid");
        }
        
        function cargarEstadoOrdenes()
        {
            var valoresx = "";
            $.ajax({
                    data: {},
                    type: "POST",
                    dataType: "json",
                    url: "<?=$homesite;?>/web/app_dev.php/estadosot/search/",
                    async:false,
                    success: function(data)
                    {
                        var arrayValores = data.rows;

                        valoresx ="{";
                        $.each( arrayValores, function ( userkey, uservalue) 
                        {
                            if(valoresx == "{")
                            {
                                valoresx += "\"" + uservalue.id + "\":\"" + uservalue.descripcion+"\"";
                            }
                            else
                            {
                                valoresx += "," + "\"" + uservalue.id + "\":\"" + uservalue.descripcion+"\"";
                            }
                        });
                        valoresx +="}";

                        return valoresx;
                    },
                    error: function(xhr, textStatus, errorThrown) 
                    {
                      // Handle error
                      alert(errorThrown);
                    }
            });
            return valoresx;
       }       

       function cargarEstadoOTAdmin()
        {
            var valoresx = "";
            $.ajax({
                    data: {},
                    type: "POST",
                    dataType: "json",
                    url: "<?=$homesite;?>/web/app_dev.php/estadosotadmin/search/",
                    async:false,
                    success: function(data)
                    {
                        var arrayValores = data.rows;

                        valoresx ="{";
                        $.each( arrayValores, function ( userkey, uservalue) 
                        {
                            if(valoresx == "{")
                            {
                                valoresx += "\"" + uservalue.id + "\":\"" + uservalue.descripcion+"\"";
                            }
                            else
                            {
                                valoresx += "," + "\"" + uservalue.id + "\":\"" + uservalue.descripcion+"\"";
                            }
                        });
                        valoresx +="}";
                        return valoresx;
                    },
                    error: function(xhr, textStatus, errorThrown) 
                    {
                      // Handle error
                      alert(errorThrown);
                    }
            });
            return valoresx;
       }       
   
        function cargarTalleres()
        {
            var valoresx = "";
            $.ajax({
                    data: {},
                    type: "POST",
                    dataType: "json",
                    url: "<?=$homesite;?>/web/app_dev.php/taller/search/",
                    async:false,
                    success: function(data)
                    {
                        var arrayValores = data.rows;

                        valoresx ="{";
                        $.each( arrayValores, function ( userkey, uservalue) 
                        {
                            if(valoresx == "{")
                            {
                                valoresx += "\"" + uservalue.id + "\":\"" + uservalue.descripcion+"\"";
                            }
                            else
                            {
                                valoresx += "," + "\"" + uservalue.id + "\":\"" + uservalue.descripcion+"\"";
                            }
                        });
                        valoresx +="}";

                        return valoresx;
                    },
                    error: function(xhr, textStatus, errorThrown) 
                    {
                      // Handle error
                      alert(errorThrown);
                    }
            });
            return valoresx;
       }
       
       function cargarTipoComprobantes()
        {
            var valoresx = "";
            $.ajax({
                                data: {},
                                type: "POST",
                                dataType: "json",
                                url: "<?=$homesite;?>/web/app_dev.php/tipocomprobante/search/",
                                async:false,
                                success: function(data)
                                {


                                                        var arrayValores = data;

                                                        valoresx ="{\"\":\"[Seleccione]\"";
                                                        $.each( arrayValores, function ( userkey, uservalue) 
                                                        {
                                                            
                                                            
                                                                valoresx += "," + "\"" + uservalue.id + "\":\"" + uservalue.descripcion+"\"";
                                                            
                                                        });
                                                        valoresx +="}";
                                                        //valores = data;
                                                        //$.parseJSON(valores);
                                                        return valoresx;
                                },
                                error: function(xhr, textStatus, errorThrown) {
                                    // Handle error
                                    alert(errorThrown);
                                    //$.unblockUI(); 
                                  }
                        });
              return valoresx;
       }
       
       
        function getProveedores()
        {
            var valoresx = "";
            $.ajax({
                                data: {},
                                type: "POST",
                                dataType: "json",
                                url: "<?=$homesite;?>/web/app_dev.php/proveedores/search/",
                                async:false,
                                success: function(data)
                                {


                                                        var arrayValores = data.rows;

                                                        valoresx ="{";
                                                        $.each( arrayValores, function ( userkey, uservalue) 
                                                        {
                                                            if(valoresx == "{")
                                                            {
                                                                //console.log(userkey + ': ' + uservalue.id + ': ' + uservalue.descripcion );
                                                                valoresx += "\"" + uservalue.id + "\":\"" + uservalue.nombre+"\"";
                                                            }
                                                            else
                                                            {
                                                                valoresx += "," + "\"" + uservalue.id + "\":\"" + uservalue.nombre+"\"";
                                                            }
                                                        });
                                                        valoresx +="}";
                                                        //valores = data;
                                                        //$.parseJSON(valores);
                
                                                        return valoresx;
                                },
                                error: function(xhr, textStatus, errorThrown) {
                                    // Handle error
                                    alert(errorThrown);
                                    //$.unblockUI(); 
                                  }
                        });
              return valoresx;
       }
       
       function cargarEstadoOrdenesC()
        {
            var valoresx = "";
            $.ajax({
                                data: {},
                                type: "POST",
                                dataType: "json",
                                url: "<?=$homesite;?>/web/app_dev.php/estadosoc/search/",
                                async:false,
                                success: function(data)
                                {


                                                        var arrayValores = data.rows;

                                                        valoresx ="{";
                                                        $.each( arrayValores, function ( userkey, uservalue) 
                                                        {
                                                            if(valoresx == "{")
                                                            {
                                                                //console.log(userkey + ': ' + uservalue.id + ': ' + uservalue.descripcion );
                                                                valoresx += "\"" + uservalue.id + "\":\"" + uservalue.descripcion+"\"";
                                                            }
                                                            else
                                                            {
                                                                valoresx += "," + "\"" + uservalue.id + "\":\"" + uservalue.descripcion+"\"";
                                                            }
                                                        });
                                                        valoresx +="}";
                                                        //valores = data;
                                                        //$.parseJSON(valores);
                
                                                        return valoresx;
                                },
                                error: function(xhr, textStatus, errorThrown) {
                                    // Handle error
                                    alert(errorThrown);
                                    //$.unblockUI(); 
                                  }
                        });
              return valoresx;
       }
       
       var proveedores = JSON.parse(getProveedores());
       var estadosOc = JSON.parse(cargarEstadoOrdenesC()); 
       
        var estadosOt = JSON.parse(cargarEstadoOrdenes());
        var estadosOtAdmin = JSON.parse(cargarEstadoOTAdmin()); 
        var talleres = JSON.parse(cargarTalleres());
        var tipoComprobantes = JSON.parse(cargarTipoComprobantes());
                
        var debaja = {false:"No",true:"Si"};
        var estadoAdmin = {false:"Pendiente",true:"Facturada"};
        
    //envía a la subgrilla un valor que no es el Id por defecto, en este caso el id del taaller
    function valorTaller(id)
    {
        var rowData = jQuery('#jqGrid').jqGrid ('getRowData', id);
        return rowData["taller.id"];
    }
    
    $(document).ready(function () {
        
        function validarFechas(idDesde, idHasta, jqGrid)
        {
            var fDesde = $(idDesde).datepicker("getDate");
            var fHasta = $(idHasta).datepicker("getDate");

            if(fDesde != "" && fHasta != "")
            {
                if(fDesde > fHasta)
                {
                    alert("La fecha Desde debe ser menor a la Fecha Hasta");
                    jQuery(jqGrid).setGridParam({ postData: {} });
                    return false;
                }
                else
                {
                    jQuery(jqGrid).setGridParam({ postData: {
                    "fDesde": $(idDesde).val()
                    ,"fHasta": $(idHasta).val()
                    } });
                    jQuery(jqGrid).trigger('reloadGrid');
                    return true;
                }
            }
            else
            {
                jQuery(jqGrid).setGridParam({ postData: {} });
                return false;
            }
        }

        var initDatePicker = function(idDesde, idHasta, jqGrid)
        {
            $(idDesde).datepicker({
                //comment the beforeShow handler if you want to see the ugly overlay
                beforeShow: function() 
                {
                    setTimeout(function(){
                        $('.ui-datepicker').css('z-index', 99999999999998);
                    }, 0);
                }
                ,onSelect: function(dateText) { 
        
                    var selectedDate = $(this).datepicker("getDate");
                    var msecsInADay = 86400000;
                    var endDate = new Date(selectedDate.getTime());

                    $(idHasta).datepicker( "option", "minDate", endDate );
                    $(idHasta).datepicker( "option", "maxDate", '+2y' );
                    jQuery(jqGrid).setGridParam({ postData: {
                        "fDesde": $(idDesde).val()
                        ,"fHasta": $(idHasta).val()
                    } });
                    jQuery(jqGrid).trigger('reloadGrid');
                }
            });
                
            
            $(idHasta).datepicker({
                //comment the beforeShow handler if you want to see the ugly overlay
                beforeShow: function() 
                {
                    setTimeout(function(){
                        $('.ui-datepicker').css('z-index', 99999999999999);
                    }, 0);
                }
                ,onSelect: function(dateText) 
                {                 
                    if(!validarFechas(idDesde, idHasta, jqGrid))
                    {
                        $(this).datepicker().datepicker("setDate", "");
                    }                
                }
            });
        }

        initDatePicker('#fDesdeOT', '#fHastaOT', '#jqGrid');
        initDatePicker('#fDesdePP', '#fHastaPP', '#jqGrid2');
        
        
        
           var filters = '{"groupOp":"AND","rules":[{"field":"fecha_carga","op":"gt","data":"01/01/2010"},{"field":"fecha_carga","op":"lt","data":"01/01/2017"}]}';
           //filters = JSON.parse(filters);
        
        
            function siNoFmatter (cellvalue, options, rowObject)
              {
                   if(cellvalue)
                   {
                       return "Si";
                   }
                   else
                   {
                       return "No";
                   }
              }

              function noSiFmatter (cellvalue, options, rowObject)
              {
                   if(cellvalue)
                   {
                       return "Si";
                   }
                   else
                   {
                       return "No";
                   }
              }
              
              function adminFormatter(cellvalue, options, rowObject)
              {
                  if(cellvalue)
                   {
                       return "Facturada";
                   }
                   else
                   {
                       return "Pendiente";
                   }
              }

            $("#jqGrid").jqGrid({
                url: '<?=$homesite;?>/web/app_dev.php/ordentrabajo/searchfromotadmin/',
                editurl: '<?=$homesite;?>/web/app_dev.php/ordentrabajo/crear/',
                cellEdit: true,
                cellsubmit : 'remote',
                cellurl : '<?=$homesite;?>/web/app_dev.php/ordentrabajo/updatepago/',
                datatype: "json",
                sortname: 'id',
                sortorder: 'asc',
                mtype: 'POST',
                gridview:false,
                grouping:true,
                gridComplete: function () {
                                var $grid = $("#jqGrid");
                                    var idPrefix =$grid[0].id + "ghead_0_", trspans;
                                    var groups =$grid[0].p.groupingView.groups;
                                    if ($grid[0].p.grouping) {
                                        for (var index = 0; index < groups.length; index++) 
                                        {
                                            
                                                trspans = $("#" + idPrefix + index + " span.tree-wrap-" +$grid[0].p.direction + "." +$grid[0].p.groupingView.plusicon);
                                            
                                                if (trspans.length > 0) {
                                                    $grid.jqGrid('groupingToggle', idPrefix + index);
                                                }   
                                        }
                                    } 
                         },
                afterSaveCell: function(rowid, celname, value, iRow, iCol) {
                    //$("#jqGrid").trigger('reloadGrid');
                    
                    RefreshGridDataGroup(0);
                    
                    //RefreshGridData(1);
//                    console.log(rowid, celname, value, iRow, iCol);
//                    console.log($("#jqGrid").jqGrid('getRowData', rowid));
//                    $("#jqGrid").jqGrid('setCell', rowid, 'Currency', '111111');
                },
                groupingView : 
                    { groupField : ['taller.descripcion']
                    , groupColumnShow : [true]
                    , groupText : ['<b>{0} - {1} Item(s)</b>']
                    , groupCollapse : true
                    , groupSummary: [true] // will use the "summaryTpl" property of the respective column
                    //, groupOrder: ['desc'] 
                   },
                "postData":{
                    "fDesde": $("#fDesdeOT").val()
                    ,"fHasta": $("#fHastaOT").val()
                },
                loadComplete: function () {
                    
                    $(this).jqGrid("getGridParam", "postData").filters = filters;
                    
                },
                viewrecords: true,
                
                //comienzo sub grilla  
               "subGridOptions": {
                "plusicon": "ui-icon-triangle-1-e",
                "minusicon": "ui-icon-triangle-1-s",
                "openicon": "ui-icon-arrowreturn-1-e"
            },
             "subGrid": true,
            "subGridRowExpanded": function(subgridid, id) {
            var data = {
                subgrid: subgridid,
                rowid: id,
                taller:valorTaller(id)
            };

            $("#" + jQuery.jgrid.jqID(subgridid)).load('subGridOt.php', data);
            }//fin de la subgrilla
        ,
                
                ondblClickRow: function(rowid) {
                    jQuery(this).jqGrid('editGridRow', rowid,
                    {
                        recreateForm:true,
                        closeAfterEdit:true,
                        closeOnEscape:true,
                        reloadAfterSubmit:true,
                         beforeSubmit : function(postdata, formid) 
                        {
                            postdata["otadmin"] = true;
                            return [true, ""];
                        },
                        "afterSubmit" : function( data, postdata, oper) 
                        {
                            var response = data.responseJSON;
                            if (response.hasOwnProperty("violations")) {
                                    if(response.violations.length) 
                                    {
                                            return [false,response.violations[0].message];
                                    }
                            }

                            if (response.hasOwnProperty("error")) {
                                    if(response.error.length) 
                                    {
                                            return [false,response.error];
                                    }
                            }
                            return [true,"",""];
                        }
                        
                        
                    });
                },
                colModel: [
                { 
                    label: 'Id'
                    , name: 'id'
                    , index: 'id'
                    , width: 75 
                    , search: false
                    , hidden: false
                    , sorttype: 'number'
                    , key: true
                },
                { 
                    label: 'Estado'
                    , name: 'estado.descripcion'
                    , index: 'estado.descripcion'
                    , width: 100 
                    , searchoptions: 
                    {
                        value:estadosOt
                        ,sopt: ['eq','ne']
                    }
                    ,editable: false
                    ,editrules : {  edithidden: false}
                },
                { 
                    label: 'Estado Admin'
                    , name: 'estadoadmin.descripcion'
                    , index: 'estadoadmin.descripcion'
                    , width: 100 
                    , editable: true
                    , edittype: 'select'
                    , stype:'select'
                    , editoptions: 
                    {
                        value:estadosOtAdmin
                    }
                    , searchoptions: 
                    {
                        value:estadosOtAdmin
                        ,sopt: ['eq','ne']
                    }
                    , editrules : { required: true }
                },
                { 
                        label: 'taller.id'
                        , name: 'taller.id'
                        , index: 'taller.id'
                        , width: 150 
                        ,editable: false
                        ,hidden:true
                        ,search:false
                        ,editrules : { required: false,  edithidden: false}
                }
                ,{ 
                    label: 'Taller'
                    , name: 'taller.descripcion'
                    , index: 'taller.descripcion'
                    , width: 100 
                    ,editable: false
                    ,edittype: 'select'
                    ,stype:'select'
                    ,editoptions: 
                    {
                    value:talleres
                    }
                    ,searchoptions: 
                    {
                    value:talleres
                    ,sopt: ['eq','ne']
                    }
                    ,editrules : { required: true}
                }
                ,{ 
                    label: 'Taller Externo'
                    , name: 'taller.externo'
                    , index: 'taller.externo'
                    , width: 100 
                    ,editable: false
                    ,search:false
                    ,edittype: 'select'
                    ,stype:'select'
                    ,sort:false
                    ,editoptions: 
                    {
                        value:debaja
                    }
                    
                    ,formatter:siNoFmatter
                    ,editrules : { required: true}
                }
                ,
                { 
                    label: 'idvehiculo'
                    , name: 'vehiculo.id'
                    , index: 'vehiculo.id'
                    , width: 150 
                    ,editable: false
                    ,hidden:true
                    ,search:false
                    ,editrules : { required: true,  edithidden: false}
                }
                
                , { 
                  label: 'Fecha'
                  , name: 'fecha_carga'
                  , index: 'fecha_carga'
                  , width: 50
                  ,editable: true
                  ,edittype: 'text'
                  ,sorttype: 'date', datefmt:'d/mm/Y'
                  ,formatter: function (cellvalue, options, rowObject) {
                    if(cellvalue)
                    {
                        var y = Number(cellvalue.substring(0,4));
                        var m = Number(cellvalue.substring(5,7));
                        var d = Number(cellvalue.substring(8,10));

                        return String(d)+'/'+String(m)+'/'+String(y);
                    }
                    else
                    {
                        return false;
                    }
                        
                  }
                            
              }
                ,
                { 
                    label: 'Dominio'
                    , name: 'vehiculo.dominio'
                    , index: 'dominio'
                    , width: 100 
                    ,editable: false
                    ,searchoptions: 
                    {
                        sopt: ['cn']
                    }
                    ,"editoptions": 
                    {
                        "dataInit": 

                        function(el) {
                        setTimeout(function() 
                        {
                            if (jQuery.ui) 
                            {
                                if (jQuery.ui.autocomplete) 
                                {
                                    jQuery(el).autocomplete({
                                        "appendTo": "body",
                                        "disabled": false,
                                        "delay": 300,
                                        "minLength": 3,
                                        select: function( event , ui ) 
                                        {
                                            document.getElementById("vehiculo.id").value = ui.item.vehiculoId;
                                        },
                                        "source": function(request, response) {

                                            $.ajax({
                                                url: "<?=$homesite;?>/web/app_dev.php/vehiculo/searchbysinfecha/",
                                                dataType: "json",
                                                data: request,
                                                type: "GET",
                                                error: function(res, status) {
                                                    alert(res.status + " : " + res.statusText + ". Status: " + status);
                                                },
                                                success: function(data) 
                                                {
                                                    //si no devuelve datos setea el campo oculto del id del vehículo en ""
                                                    if(data.length < 1)
                                                    {                                                            
                                                        document.getElementById("vehiculo.id").value = "";
                                                    }
                                                    response(data);
                                                }
                                            });
                                        }
                                    });
                                    jQuery(el).autocomplete('widget').css('font-size', '11px');
                                    jQuery(el).autocomplete('widget').css('z-index', '1000');
                                }
                            }
                        }, 200);
                        }
                    }
                    ,editrules : { required: true}
                }
                ,
                { 
                  label: 'Tipo CTE'
                  , name: 'tipo_comprobante.descripcion'
                  , index: 'tipo_comprobante.descripcion'
                  , width: 50 
                  ,editable: true
                  ,hidden:false
                  ,edittype: 'select'
                  ,stype: 'select'
                  ,editoptions: { value: tipoComprobantes }
                  ,searchoptions: 
                              {
                                 value: tipoComprobantes
                                 ,sopt: ['eq','ne']
                              }
                  ,editrules : { required: false,edithidden: true}
                  
                },
                
                { 
                    label: 'Nro. CTE'
                    , name: 'nrofactura'
                    , index: 'nrofactura'
                    , width: 50 
                    , editable: true
                    , sortype:'int'
                    , editrules : { required: false, number:true }
                    , searchoptions: 
                    {
                     sopt : ['eq','ne', 'lt', 'gt']
                    }
                    ,searchrules : { number:true }
                },

                    {
                        label: 'Total'
                        , name: 'total'
                        , index: 'total'
                        , width: 50
                        ,editable: false
                        ,hidden:false
                        ,summaryTpl: "{0}" // set the summary template to show the group summary
                        ,summaryType:"sum"
                        // set the formula to calculate the summary type
                        ,formatter: function (cellval, opts, rwdat, act) {

                        if (opts.rowId === "")
                        {
                            if (cellval > 1000) {
                                return '<span style="color:green">' +
                                    $.fn.fmatter('number', cellval, opts, rwdat, act) +
                                    '</span>';
                            } else {
                                return '<span style="color:red">' +
                                    $.fn.fmatter('number', cellval, opts, rwdat, act) +
                                    '</span>';
                            }
                        } else
                        {
                            return $.fn.fmatter('number', cellval, opts, rwdat, act);
                        }


                    }
                    },

                    {
                        label: 'Total Adeudado'
                        , name: 'totaladeudado'
                        , index: 'totaladeudado'
                        , width: 50
                        ,editable: false
                        ,hidden:false
                        ,summaryTpl: "{0}" // set the summary template to show the group summary
                        ,summaryType:"sum"
//                        ,summaryType: function (val, name, record)
//                         {
//                             console.log(val, name, record);
//                         if(record.pagado == false)
//                         {
//                             val += 0;
//                         }
//                         else
//                         {
//                             val += record.total;
//                         }
//                         return val;
//                         }
                        // set the formula to calculate the summary type
                        ,formatter: function (cellval, opts, rwdat, act) {

                        if (opts.rowId === "")
                        {
                            if (cellval > 0) {
                                return '<span style="color:red">' +
                                    $.fn.fmatter('number', cellval, opts, rwdat, act) +
                                    '</span>';
                            } else {
                                return '<span style="color:green">' +
                                    $.fn.fmatter('number', cellval, opts, rwdat, act) +
                                    '</span>';
                            }
                        } else
                        {
                            return $.fn.fmatter('number', cellval, opts, rwdat, act);
                        }


                    }
                    }
                    ,
                    {
                        label: 'Descripción'
                        , name: 'descripcion'
                        , index: 'descripcion'
                        , width: 255
                        , editable: false
                        , editrules : { required: false}
                        , searchoptions:
                        {
                            sopt: ['cn']
                        }
                    },{
                        label: 'Pagado'
                        , name: 'pagado'
                        , index: 'pagado'
                        , width: 100
                        ,editable: true
                        ,hidden:false
                        ,edittype: 'checkbox'
                        ,formatter:"checkbox"
                    }
                ,
                {
                  label: 'De Baja'
                  , name: 'debaja'
                  , index: 'debaja'
                  , width: 100 
                  ,editable: true
                  ,hidden:false
                  ,edittype: 'select'
                  ,stype: 'select'
                  ,formatter:siNoFmatter
                  ,editoptions: { value: debaja , defaultValue:"false"}
                  ,searchoptions: { value: debaja ,sopt: ['eq','ne']}
                  ,editrules : { required: true,edithidden: true}
                  
                }
                    // sorttype is used only if the data is loaded locally or loadonce is set to true
                    //{ label: 'Quantity', name: 'Quantity', width: 80, sorttype: 'number' }                   
              ],
              width: 'auto',
              height:'auto',
              page:1,
              rowNum: 10,
              rowList: [10, 50, 100],
              loadonce: false, // this is just for the demo
              pager: "#jqGridPager"
            });
            
            

              $('#jqGrid').navGrid('#jqGridPager',
                // the buttons to appear on the toolbar of the grid
                //Botonera de la grilla
                { 
                  edit: true
                  , add: false //Boton de agregar
                  , del: false //Boton de eliminar
                  , search: true
                  , refresh: true
                  , view: false //Boton de ver datos
                  , position: "left"
                  , cloneToTop: false 
                },
                // options for the Edit Dialog
                {
                    editCaption: "Editar &Oacute;rden Trabajo",
                    recreateForm: true,
                    closeAfterEdit: true,
                    closeOnEscape:true,
                    beforeSubmit : function(postdata, formid) 
                    {
                        postdata["otadmin"] = true;
                        return [true, ""];
                    },
                  "afterSubmit" : function( data, postdata, oper) 
                    {
                        var response = data.responseJSON;
                        if (response.hasOwnProperty("violations")) {
                                if(response.violations.length) 
                                {
                                        return [false,response.violations[0].message];
                                }
                        }
                        
                        if (response.hasOwnProperty("error")) {
                                if(response.error.length) 
                                {
                                        return [false,response.error];
                                }
                        }
                        return [true,"",""];
                    },
                    errorTextFormat: function (data) {
                        return 'Error: ' + data.responseText
                    }
                },
                // options for the Add Dialog
                {
                    addCaption: "Crear &Oacute;rden Trabajo",
                    closeAfterAdd: true,
                    recreateForm: true,
                    closeOnEscape:true,
                    afterSubmit : function( response,data, postdata, oper) {

                    var res = $.parseJSON(response.responseText);

                    if(res.okregistro=="false")
                    {
                      return [false,res.error,""];
                    }
                    return [true, ""];
                  },
                    errorTextFormat: function (data) {
                        return 'Error: ' + data.responseText
                  }
                }
                
                );

            
           
           //segunda grilla
           
           
            $("#jqGrid2").jqGrid({
              url: '<?=$homesite;?>/web/app_dev.php/ordencompra/searchadmin/',
              editurl: '<?=$homesite;?>/web/app_dev.php/ordencompra/crear/',
              datatype: "json",
              sortname: 'id',
              sortorder: 'asc',
              mtype: 'POST', 
              gridview:false,
              grouping:true, groupingView : 
              { groupField : ['proveedor.nombre']
                , groupColumnShow : [true]
                , groupText : ['<b>{0} - {1} Item(s)</b>']
                , groupCollapse : true
                , groupSummary: [true] // will use the "summaryTpl" property of the respective column
                //, groupOrder: ['desc'] 
               },
               "postData":{
                    "fDesde": $("#fDesdePP").val()
                    ,"fHasta": $("#fHastaPP").val()
                },
                loadComplete: function () {
                    
                    $(this).jqGrid("getGridParam", "postData").filters = filters;
                    
                },
              gridComplete: function () {
                             
                         },

              //comienzo sub grilla  
               "subGridOptions": {
                "plusicon": "ui-icon-triangle-1-e",
                "minusicon": "ui-icon-triangle-1-s",
                "openicon": "ui-icon-arrowreturn-1-e"
                },
             "subGrid": true,
             "subGridRowExpanded": function(subgridid, id) {
                var data = {
                    subgrid: subgridid,
                    rowid: id,
                    subgrilla:"si"
                };
               $("#" + jQuery.jgrid.jqID(subgridid)).load('subGridOc.php', data);
             }//fin de la subgrilla
             ,
              afterInsertRow: function(rowid, aData, rowelem) 
              {
                    
                    //condición para mostrar o no la subgrilla
                    /* 
                    if(rowelem.taller.externo == true){
                        $('tr#'+rowid, $(this))
                         .children("td.sgcollapsed")
                         .html("")
                         .removeClass('ui-sgcollapsed sgcollapsed');
                    }*/
                },  
              viewrecords: true,
              ondblClickRow: function(rowid) {
                    jQuery(this).jqGrid('editGridRow', rowid,
                    {
                        recreateForm:true,
                        closeAfterEdit:true,
                        closeOnEscape:true,
                        reloadAfterSubmit:true,
                         beforeSubmit : function(postdata, formid) 
                        {
                            postdata["otadmin"] = true;
                            return [true, ""];
                        },
                        "afterSubmit" : function( data, postdata, oper) 
                        {
                            var response = data.responseJSON;
                            if (response.hasOwnProperty("violations")) {
                                    if(response.violations.length) 
                                    {
                                            return [false,response.violations[0].message];
                                    }
                            }

                            if (response.hasOwnProperty("error")) {
                                    if(response.error.length) 
                                    {
                                            return [false,response.error];
                                    }
                            }
                            return [true,"",""];
                        }
                        
                        
                    });
                },
               colModel: [
                { 
                  label: 'Id'
                  , name: 'id'
                  , index: 'id'
                  , width: 75 
                  , search: false
                  , hidden: false
                  , sorttype: 'number'
                  ,key: true
                },
                { 
                  label: 'Descripcion'
                  , name: 'descripcion'
                  , index: 'descripcion'
                  , width: 255 
                  ,editable: true
                  ,editrules : { required: true}
                  ,searchoptions: 
                              {
                               sopt: ['cn']
                              }
                },
                { 
                  label: 'Proveedor'
                  , name: 'proveedor.nombre'
                  , index: 'proveedor.nombre'
                  , width: 50 
                  ,editable: true
                  ,hidden:false
                  ,edittype: 'select'
                  ,stype: 'select'
                  ,editoptions: { value: proveedores }
                  ,searchoptions: 
                              {
                                 value: proveedores
                                 ,sopt: ['eq','ne']
                              }
                  ,editrules : { required: true,edithidden: true}
                  
                },
                { 
                  label: 'Estado'
                  , name: 'estado.descripcion'
                  , index: 'estado.descripcion'
                  , width: 100 
                  ,editable: true
                  ,edittype: 'select'
                  ,stype:'select'
                  ,editoptions: 
                              {
                                 value:estadosOc
                              }
                  ,searchoptions: 
                              {
                                 value:estadosOc
                                 ,sopt: ['eq','ne']
                              }
                  ,editrules : { required: true}
                },
                   {
                  label: 'De Baja'
                  , name: 'debaja'
                  , index: 'debaja'
                  , width: 100 
                  ,editable: true
                  ,hidden:false
                  ,edittype: 'select'
                  ,stype: 'select'
                  ,formatter:siNoFmatter
                  ,editoptions: { value: debaja , defaultValue:"false"}
                  ,searchoptions: { value: debaja ,sopt: ['eq','ne']}
                  ,editrules : { required: true,edithidden: true}
                  
                },
                { 
                  label: 'Estado Admin'
                  , name: 'estadoadmin.id'
                  , index: 'estadoadmin.id'
                  , width: 50 
                  ,editable: true
                  ,hidden:false
                  ,edittype: 'select'
                  ,stype: 'select'
                  ,formatter:adminFormatter
                  ,editoptions: { value: estadoAdmin , defaultValue:"No"}
                  ,searchoptions: { value: estadoAdmin ,sopt: ['eq','ne']}
                  ,editrules : { required: true,edithidden: true}
                },
                
                { 
                  label: 'Total'
                  , name: 'sumatoria'
                  , index: 'sumatoria'
                  , width: 50 
                  ,editable: false
                  ,hidden:false
                  ,summaryTpl: "{0}" // set the summary template to show the group summary
                  ,summaryType:"sum"
                  /*,summaryType: function (val, name, record) 
                  {
                    if(record.pagado == false)
                    {
                        //alert("no pagado");
                        alert(record.sumatoria);
                        record.sumatoria = "0";
                    }
                    else
                    {
                        //alert(record.sumatoria);
                    }
                    return record.sumatoria;
                  } */
                    // set the formula to calculate the summary type
                  ,formatter: function (cellval, opts, rwdat, act) {
                        
                        if (opts.rowId === "") 
                        {
                            if (cellval > 1000) {
                                return '<span style="color:green">' +
                                    $.fn.fmatter('number', cellval, opts, rwdat, act) +
                                    '</span>';
                            } else {
                                return '<span style="color:red">' +
                                    $.fn.fmatter('number', cellval, opts, rwdat, act) +
                                    '</span>';
                            }
                        } else 
                        {
                            return $.fn.fmatter('number', cellval, opts, rwdat, act);
                        }
                        
                        
                    }
                }
                
                ,
                
                { 
                  label: 'Total Adeudado'
                  , name: 'sumatoriaDeuda'
                  , index: 'sumatoriaDeuda'
                  , width: 50 
                  ,editable: false
                  ,hidden:false
                  ,summaryTpl: "{0}" // set the summary template to show the group summary
                  ,summaryType:"sum"
                  /*,summaryType: function (val, name, record) 
                  {
                    if(record.pagado == false)
                    {
                        //alert("no pagado");
                        alert(record.sumatoria);
                        record.sumatoria = "0";
                    }
                    else
                    {
                        //alert(record.sumatoria);
                    }
                    return record.sumatoria;
                  } */
                    // set the formula to calculate the summary type
                  ,formatter: function (cellval, opts, rwdat, act) {
                        
                        if (opts.rowId === "") 
                        {
                            if (cellval > 1000) {
                                return '<span style="color:green">' +
                                    $.fn.fmatter('number', cellval, opts, rwdat, act) +
                                    '</span>';
                            } else {
                                return '<span style="color:red">' +
                                    $.fn.fmatter('number', cellval, opts, rwdat, act) +
                                    '</span>';
                            }
                        } else 
                        {
                            return $.fn.fmatter('number', cellval, opts, rwdat, act);
                        }
                        
                        
                    }
                }
                // sorttype is used only if the data is loaded locally or loadonce is set to true
                //{ label: 'Quantity', name: 'Quantity', width: 80, sorttype: 'number' }                   
              ],
              width: 'auto',
              height:'auto',
              page:1,
              rowNum: 10,
              rowList: [10, 50, 100],
              loadonce: false, // this is just for the demo
              pager: "#jqGrid2Pager"
            });

            $('#jqGrid2').navGrid('#jqGrid2Pager',
                // the buttons to appear on the toolbar of the grid
                //Botonera de la grilla
                { 
                  edit: true
                  , add: true //Boton de agregar
                  , del: false //Boton de eliminar
                  , search: true
                  , refresh: true
                  , view: false //Boton de ver datos
                  , position: "left"
                  , cloneToTop: false 
                },
                // options for the Edit Dialog
                {
                    editCaption: "Editar &Oacute;rden Trabajo",
                    recreateForm: true,
                    closeAfterEdit: true,
                    closeOnEscape:true
                    ,"afterSubmit" : function( data, postdata, oper) 
                    {
                        var response = data.responseJSON;
                        if (response.hasOwnProperty("violations")) {
                                if(response.violations.length) 
                                {
                                        return [false,response.violations[0].message];
                                }
                        }
                        
                        if (response.hasOwnProperty("error")) {
                                if(response.error.length) 
                                {
                                        return [false,response.error];
                                }
                        }
                        
                        RefreshGridData(2);
                        return [true,"",""];
                    },
                    errorTextFormat: function (data) {
                        return 'Error: ' + data.responseText
                    }
                },
                // options for the Add Dialog
                {
                    addCaption: "Crear &Oacute;rden Trabajo",
                    closeAfterAdd: true,
                    recreateForm: true,
                    closeOnEscape:true,
                    "afterSubmit" : function( data, postdata, oper) 
                    {
                        var response = data.responseJSON;
                        if (response.hasOwnProperty("violations")) {
                                if(response.violations.length) 
                                {
                                        return [false,response.violations[0].message];
                                }
                        }
                        
                        if (response.hasOwnProperty("error")) {
                                if(response.error.length) 
                                {
                                        return [false,response.error];
                                }
                        }
                        
                        RefreshGridData(2);
                        return [true,"",""];
                    },
                    errorTextFormat: function (data) {
                        return 'Error: ' + data.responseText
                    }
                }
                
                );

               $('#jqGrid').jqGrid('setGridHeight',($(window).innerHeight()-190));
    $('#jqGrid').jqGrid('setGridWidth',($(window).width()-40));
    $('#jqGrid2').jqGrid('setGridHeight',($(window).innerHeight()-155));
    $('#jqGrid2').jqGrid('setGridWidth',$(window).width()-40);

           
           //fin segunda grilla
       
           $("#jqGrid").jqGrid("getGridParam", "postData").filters = filters;

});

    /*
     var height = $(window).height();
            $('.ui-jqgrid-bdiv').height(height-100);
            
            var alto = $(window).width();
            $('.ui-jqgrid-bdiv').width(alto);
            alert(alto);
            
            $("#jqGrid").jqGrid({with:100000});
            */
           
            
           $(window).resize(function(){
    $('#jqGrid').jqGrid('setGridHeight',($(window).innerHeight()-190));
    $('#jqGrid').jqGrid('setGridWidth',($(window).width()-40));
    $('#jqGrid2').jqGrid('setGridHeight',($(window).innerHeight()-155));
    $('#jqGrid2').jqGrid('setGridWidth',$(window).width()-40);


    
});
</script>
<script type="text/ecmascript" src="js/jquery-ui.min.js"></script>
<script>
        $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '<Ant',
        nextText: 'Sig>',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
        dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
        };
        $.datepicker.setDefaults($.datepicker.regional['es']);
        
</script>
    </body>
</html>