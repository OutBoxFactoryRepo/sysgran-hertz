<?PHP
    include_once("config.php");
?>
<!DOCTYPE html>
<html>
    <head>
    <!-- The jQuery library is a prerequisite for all jqSuite products -->
    <script type="text/ecmascript" src="js/jquery.min.js"></script> 
    <!-- We support more than 40 localizations -->
    <script type="text/ecmascript" src="js/trirand/i18n/grid.locale-es.js"></script>
    <!-- This is the Javascript file of jqGrid -->   
    <script type="text/ecmascript" src="js/trirand/jquery.jqGrid.min.js"></script>
    <!-- This is the localization file of the grid controlling messages, labels, etc.
    <!-- A link to a jQuery UI ThemeRoller theme, more than 22 built-in and many more custom -->
    <link rel="stylesheet" type="text/css" media="screen" href="css/jquery-ui.css" />
    <!-- The link to the CSS that the grid needs -->
    <link rel="stylesheet" type="text/css" media="screen" href="css/trirand/ui.jqgrid.css" />
    
    <script type="text/ecmascript" src="js/jquery.json-2.2.min.js"></script>

    <!-- https://github.com/carloscabo/jquery-palette-color-picker -->
    <script src="js/jquery-palette-color-picker-master/src/palette-color-picker.min.js"></script>
    <link rel="stylesheet" type="text/css" media="screen" href="js/jquery-palette-color-picker-master/src/palette-color-picker.css">

    </head>
    <body>
        
    <style type="text/css">

        /* set the size of the datepicker search control for Order Date*/
        #ui-datepicker-div { font-size:11px; }
        
        /* set the size of the autocomplete search control*/
        .ui-menu-item {
            font-size: 11px;
        }

        .ui-autocomplete { font-size: 11px; position: absolute; cursor: default;z-index:5000 !important;}      
        
        .palette-color-picker-button
        {
          border: 1px solid #a6c9e2;
          border-radius: 5px;
          height: 20px;
          vertical-align: middle;
          width: 20px;
          box-shadow: none;
        }
        
    </style>
        
        <table id="jqGrid" align="center"></table>
    <div id="jqGridPager"></div>

    <script type="text/javascript">
        
        
        function cargarAgencias()
        {
            var valoresx = "";
            $.ajax({
                                data: {},
                                type: "POST",
                                dataType: "json",
                                url: "<?=$homesite;?>/web/app_dev.php/agencia/search/",
                                async:false,
                                success: function(data)
                                {


                                                        var arrayValores = data;

                                                        valoresx ="{";
                                                        $.each( arrayValores, function ( userkey, uservalue) 
                                                        {
                                                            if(valoresx == "{")
                                                            {
                                                                //console.log(userkey + ': ' + uservalue.id + ': ' + uservalue.descripcion );
                                                                valoresx += "\"" + uservalue.id + "\":\"" + uservalue.descripcion+"\"";
                                                            }
                                                            else
                                                            {
                                                                valoresx += "," + "\"" + uservalue.id + "\":\"" + uservalue.descripcion+"\"";
                                                            }
                                                        });
                                                        valoresx +="}";
                                                        //valores = data;
                                                        //$.parseJSON(valores);
                
                                                        return valoresx;
                                },
                                error: function(xhr, textStatus, errorThrown) {
                                    // Handle error
                                    alert(errorThrown);
                                    //$.unblockUI(); 
                                  }
                        });
              return valoresx;
       }
       
        function cargarCategoriasVeh()
        {
            var valoresx = "";
            $.ajax({
                                data: {},
                                type: "POST",
                                dataType: "json",
                                url: "<?=$homesite;?>/web/app_dev.php/categoriasveh/search/",
                                async:false,
                                success: function(data)
                                {


                                                        var arrayValores = data.rows;

                                                        valoresx ="{";
                                                        $.each( arrayValores, function ( userkey, uservalue) 
                                                        {
                                                            if(valoresx == "{")
                                                            {
                                                                //console.log(userkey + ': ' + uservalue.id + ': ' + uservalue.descripcion );
                                                                valoresx += "\"" + uservalue.id + "\":\"" + uservalue.descripcion+"\"";
                                                            }
                                                            else
                                                            {
                                                                valoresx += "," + "\"" + uservalue.id + "\":\"" + uservalue.descripcion+"\"";
                                                            }
                                                        });
                                                        valoresx +="}";
                                                        //valores = data;
                                                        //$.parseJSON(valores);
                
                                                        return valoresx;
                                },
                                error: function(xhr, textStatus, errorThrown) {
                                    // Handle error
                                    alert(errorThrown);
                                    //$.unblockUI(); 
                                  }
                        });
              return valoresx;
       }
   
   
        function cargarEstadosVehiculo()
        {
            var valoresx = "";
            $.ajax({
                                data: {},
                                type: "POST",
                                dataType: "json",
                                url: "<?=$homesite;?>/web/app_dev.php/estadovehiculo/search/",
                                async:false,
                                success: function(data)
                                {


                                                        var arrayValores = data;

                                                        valoresx ="{";
                                                        $.each( arrayValores, function ( userkey, uservalue) 
                                                        {
                                                            if(valoresx == "{")
                                                            {
                                                                //console.log(userkey + ': ' + uservalue.id + ': ' + uservalue.descripcion );
                                                                valoresx += "\"" + uservalue.id + "\":\"" + uservalue.descripcion+"\"";
                                                            }
                                                            else
                                                            {
                                                                valoresx += "," + "\"" + uservalue.id + "\":\"" + uservalue.descripcion+"\"";
                                                            }
                                                        });
                                                        valoresx +="}";
                                                        //valores = data;
                                                        //$.parseJSON(valores);
                
                                                        return valoresx;
                                },
                                error: function(xhr, textStatus, errorThrown) {
                                    // Handle error
                                    alert(errorThrown);
                                    //$.unblockUI(); 
                                  }
                        });
              return valoresx;
       }
       

      function cargarMarcas()
      {
        var valoresx = "";
        $.ajax({
          data: {},
          type: "POST",
          dataType: "json",
          url: "<?=$homesite;?>/web/app_dev.php/marca/search/",
          async:false,
          success: function(data)
          {
            var arrayValores = data;

            valoresx ="{";
            $.each( arrayValores, function ( userkey, uservalue) 
            {
              if(valoresx == "{")
              {
                valoresx += "\"" + uservalue.id + "\":\"" + uservalue.descripcion+"\"";
              }
              else
              {
                valoresx += "," + "\"" + uservalue.id + "\":\"" + uservalue.descripcion+"\"";
              }
            });
            valoresx +="}";

            return valoresx;
          },
          error: function(xhr, textStatus, errorThrown) 
          {
            // Handle error
            alert(errorThrown);
          }
        });
        return valoresx;
      }

      var validateColor = function(value, column)
      {
        var idsColores = colors.ids;
        if (idsColores[value] && idsColores[value] !== undefined)
        // var re = /[0-9A-Fa-f]{6}/g;
        // if ( value.startsWith("#") && re.test(value.substr(1)))
        {
          return [true, ""];
        }
        else
        {
          return [false, "Color no válido"];
        }
      }

      var getColors = function()
      {
        var arrayColores = [];
        var arrayIds = [];
        var object = {}
        $.ajax({
          data: {},
          type: "POST",
          dataType: "json",
          url: "<?=$homesite;?>/web/app_dev.php/colores/search/",
          async:false,
          success: function(data)
          {
            var arrayValores = data;
            $.each( arrayValores, function ( key, value) 
            { 
              var obj = {};
              obj[value.descripcion] = value.color;
              arrayColores.push(obj);
              arrayIds[value.descripcion] = value.id;
            });
            object = { "colores": arrayColores, "ids": arrayIds };
          },
          error: function(xhr, textStatus, errorThrown) 
          {
            alert(errorThrown);
          }
        });
        return object;
      }
       
       
        var agencias = JSON.parse(cargarAgencias());
        var estadosVehiculos = JSON.parse(cargarEstadosVehiculo());
        var marcas = JSON.parse(cargarMarcas());
        var categoriasVeh = JSON.parse(cargarCategoriasVeh());
        var colors = getColors();  
        var debaja = {false:"No",true:"Si"};

    
    
    $(document).ready(function () {

            function esVendedor()
            {
                 var valoresx = "";
                 $.ajax({
                                     data: {},
                                     type: "POST",
                                     dataType: "json",
                                     url: "<?=$homesite;?>/web/app_dev.php/usuarios/esvendedor/",
                                     async:false,
                                     success: function(data)
                                     {
                                         valoresx = data.esVendedor;


                                         return valoresx;
                                     },
                                     error: function(xhr, textStatus, errorThrown) 
                                     {
                                         // Handle error
                                         alert(errorThrown);
                                         //$.unblockUI(); 
                                     }
                             });
                   return valoresx;
            }
       
       
       var esVendedor = esVendedor();
      
            function siNoFmatter (cellvalue, options, rowObject)
              {
                   if(cellvalue)
                   {
                       return "Si";
                   }
                   else
                   {
                       return "No";
                   }
              }

              function noSiFmatter (cellvalue, options, rowObject)
              {
                   if(cellvalue)
                   {
                       return "Si";
                   }
                   else
                   {
                       return "No";
                   }
              }
              $("#jqGrid").jqGrid({
              url: '<?=$homesite;?>/web/app_dev.php/vehiculo/search/?debaja=true',
              editurl: '<?=$homesite;?>/web/app_dev.php/vehiculo/editar/',
              datatype: "json",
              sortname: 'id',
              sortorder: 'asc',
              mtype: 'POST',
              //comienzo sub grilla  
               "subGridOptions": 
                   {
                    "plusicon": "ui-icon-triangle-1-e",
                    "minusicon": "ui-icon-triangle-1-s",
                    "openicon": "ui-icon-arrowreturn-1-e"
                    },
                "subGrid": true,
                "subGridRowExpanded": function(subgridid, id) 
                {
                var data = {
                    subgrid: subgridid,
                    rowid: id
                    };
                $("#" + jQuery.jgrid.jqID(subgridid)).load('subGridVehiculoRes.php', data);
                },//fin de la subgrilla
              viewrecords: true,
              ondblClickRow: function(rowid) {
                  //los vendedores no pueden modificar los vehículos
                  if(!esVendedor)
                  {
                   jQuery(this).jqGrid('editGridRow', rowid,
                   {
                    recreateForm:true
                    ,closeAfterEdit:true,
                    closeOnEscape:true
                    ,reloadAfterSubmit:true,
                    onclickSubmit : function(params, posdata) {
                      var idsColores = colors.ids;
                      var descripcion = posdata["color.descripcion"];
                      posdata["color.descripcion"] = idsColores[descripcion];
                      return posdata;
                    },
                    "afterSubmit" : function( data, postdata, oper) 
                    {
                        var response = data.responseJSON;
                        if (response.hasOwnProperty("violations")) {
                                if(response.violations.length) 
                                {
                                        return [false,response.violations[0].message];
                                }
                        }
                        
                        if (response.hasOwnProperty("error")) {
                                if(response.error.length) 
                                {
                                        return [false,response.error];
                                }
                        }
                        
                        return [true,"",""];
                    },
                    "errorTextFormat": function (data) {
                        return 'Error: ' + data.responseText
                    }
                    
                   });
                }
               },
               colModel: [
                { 
                  label: 'Id'
                  , name: 'id'
                  , index: 'id'
                  , width: 75 
                  , search: false
                  , hidden: false
                  , sorttype: 'number'
                  ,key: true
                },
                { 
                  label: 'Dominio'
                  , name: 'dominio'
                  , index: 'dominio'
                  , width: 220 
                  ,editable: true
                  ,editrules : { required: true}
                  ,searchoptions:{sopt : ['cn']}
                },
                { 
                  label: 'Modelo'
                  , name: 'modelo'
                  , index: 'modelo'
                  , width: 100
                  , sorttype: 'int'
                  ,editable: true
                  ,editrules : { required: false, number:true}
                  ,searchoptions: 
                              {
                               sopt : ['eq','ne', 'lt', 'gt']
                              }
                },
                { 
                  label: 'Categoría'
                  , name: 'categoria.descripcion'
                  , index: 'categoria'
                  , width: 150 
                  ,editable: true
                  ,edittype: 'select'
                  ,stype: 'select'
                  ,editoptions: 
                              {
                                 value:categoriasVeh
                              }
                  ,searchoptions: 
                              {
                                 value:categoriasVeh
                                ,sopt: ['eq','ne']
                              }
                  ,editrules : { required: true}
                },
                { 
                  label: 'KM'
                  , name: 'kilometros'
                  , index: 'kilometros'
                  , width: 100 
                  ,editable: true
                  ,sortype:'int'
                  ,editrules : { required: false, number:true}
                  ,searchoptions: 
                            {
                             sopt : ['eq','ne', 'lt', 'gt']
                            }
                }
                ,
                { 
                  label: 'Agencia Origen'
                  , name: 'agencia.descripcion'
                  , index: 'agencia'
                  , width: 150 
                  ,editable: true
                  ,edittype: 'select'
                  ,stype: 'select'
                  ,editoptions: 
                              {
                                 value:agencias
                              }
                  ,searchoptions: 
                              {
                                 value:agencias
                                ,sopt: ['eq','ne']
                              }
                  ,editrules : { required: true}
                }
                ,
                { 
                  label: 'Agencia Destino'
                  , name: 'sucursal.descripcion'
                  , index: 'agenciadestino'
                  , width: 150
                  ,editable: false
                  ,edittype: 'select'
                  ,hidden:true
                  ,stype: 'select'
                  ,editoptions: 
                              {
                                 value:agencias
                              }
                 ,searchoptions: 
                              {
                                 value:agencias
                                ,sopt: ['eq','ne']
                              }
                  ,editrules : { required: false}
                }
                ,
                { 
                  label: 'Estado'
                  , name: 'estado.descripcion'
                  , index: 'estado'
                  , width: 150
                  ,editable: true
                  ,edittype: 'select'
                  ,stype: 'select'
                  ,editoptions: 
                              {
                                 value:estadosVehiculos
                              }
                  ,searchoptions: 
                              {
                                 value:estadosVehiculos
                                ,sopt: ['eq','ne']
                              }
                  ,editrules : { required: true}
                }
                ,
                { 
                  label: 'Marca'
                  ,name: 'marca.descripcion'
                  ,index: 'marca'
                  ,width: 220
                  ,editable: true
                  ,edittype: 'select'
                  ,stype: 'select'
                  ,editoptions: 
                              {
                                 value:marcas
                              }
                  ,searchoptions: 
                              {
                                 value:marcas
                                ,sopt: ['eq','ne']
                              }
                  ,editrules : { required: true}
                }
                ,
                { 
                  label: 'Fecha Retorno'
                  , name: 'fecha_hora_retorno'
                  , index: 'fechaHoraRetorno'
                  , width: 220
                  ,editable: false
                  ,hidden:true
                  ,edittype: 'text'
                  ,sorttype: 'date', datefmt:'d/mm/Y'
                  ,formatter: function (cellvalue, options, rowObject) {
                    if(cellvalue)
                    {
                        var y = Number(cellvalue.substring(0,4));
                        var m = Number(cellvalue.substring(5,7));
                        var d = Number(cellvalue.substring(8,10));
                        return String(d)+'/'+String(m)+'/'+String(y);
                    }
                    else
                    {
                        return false;
                    }
                  }
                  ,editoptions: {
                            // dataInit is the client-side event that fires upon initializing the toolbar search field for a column
                            // use it to place a third party control to customize the toolbar
                            dataInit: function (element) {
                                $(element).datepicker({
                                    id: 'orderDate_datePicker',
                                    dateFormat: 'd/m/yy',
                                    //minDate: new Date(2010, 0, 1),
                                    //maxDate: new Date(2020, 0, 1),
                                    showOn: 'focus'
                                });
                            }
                        }
                  ,searchoptions: {
                      
                            sopt : ['eq','ne', 'lt', 'gt'],
                            // dataInit is the client-side event that fires upon initializing the toolbar search field for a column
                            // use it to place a third party control to customize the toolbar
                            dataInit: function (element) {
                                $(element).datepicker({
                                    id: 'orderDate_datePicker',
                                    dateFormat: 'd/m/yy',
                                    //minDate: new Date(2010, 0, 1),
                                    //maxDate: new Date(2020, 0, 1),
                                    showOn: 'focus'
                                });
                            }
                        }
                  ,editrules : { required: false,edithidden: false}
                },
                { 
                  label: 'De Baja'
                  , name: 'debaja'
                  , index: 'debaja'
                  , width: 100 
                  ,editable: true
                  ,hidden:false
                  ,edittype: 'select'
                  ,stype: 'select'
                  ,formatter:siNoFmatter
                  ,editoptions: { value: debaja , defaultValue:"false"}
                  ,searchoptions: { value: debaja ,sopt: ['eq','ne']}
                  ,editrules : { required: true,edithidden: true}
                  
                  
                },
                { 
                  label: 'Número motor'
                  ,name: 'nromotor'
                  ,index: 'nromotor'
                  ,width: 255 
                  ,editable: true
                  ,editrules : { required: true }
                  ,searchoptions:{ sopt: ['cn'] }
                },
                { 
                  label: 'Número chasis'
                  ,name: 'nrochasis'
                  ,index: 'nrochasis'
                  ,width: 255 
                  ,editable: true
                  ,editrules : { required: true }
                  ,searchoptions:{ sopt: ['cn'] }
                },
                { 
                  label: 'Color'
                  ,name: 'color.descripcion'
                  ,index: 'color'
                  ,width: 150
                  ,editable: true
                  ,editrules : { 
                    custom_func: validateColor,
                    custom: true
                  }
                  ,editoptions: {
                      dataInit: function (element) {
                          $(element).paletteColorPicker({
                            colors: colors.colores,
                          });
                      }
                  }
                  ,searchoptions:{ sopt: ['cn'] }
                },
                   {
                       label: 'Descripción'
                       , name: 'descripcion'
                       , index: 'descripcion'
                       , width: 255
                       ,editable: true
                       ,editrules : { required: false}
                       ,searchoptions:
                   {
                       sopt: ['cn']
                   }
                   }
                // sorttype is used only if the data is loaded locally or loadonce is set to true
                //{ label: 'Quantity', name: 'Quantity', width: 80, sorttype: 'number' }                   
              ],
              width: 'auto',
              height:'auto',
              page:1,
              rowNum: 10,
              rowList: [10, 50, 100],
              loadonce: false, // this is just for the demo
              pager: "#jqGridPager"
            });

              $('#jqGrid').navGrid('#jqGridPager',
                // the buttons to appear on the toolbar of the grid
                //Botonera de la grilla
                { 
                  edit: !esVendedor
                  , add: !esVendedor //Boton de agregar
                  , del: false //Boton de eliminar
                  , search: true
                  , refresh: true
                  , view: true //Boton de ver datos
                  , position: "left"
                  , cloneToTop: false 
                },
                // options for the Edit Dialog
                {
                    editCaption: "Editar Vehículo",
                    recreateForm: true,
                    closeAfterEdit: true,
                    closeOnEscape:true,
                    onclickSubmit : function(params, posdata) {
                      var idsColores = colors.ids;
                      var descripcion = posdata["color.descripcion"];
                      posdata["color.descripcion"] = idsColores[descripcion];
                      return posdata;
                    }
                    ,"afterSubmit" : function( data, postdata, oper) 
                    {
                        var response = data.responseJSON;
                        
                        if (response.hasOwnProperty("violations")) {
                                if(response.violations.length) 
                                {
                                        return [false,response.violations[0].message];
                                }
                        }
                        
                        if (response.hasOwnProperty("error")) {
                                if(response.error.length) 
                                {
                                        return [false,response.error];
                                }
                        }
                        
                        return [true,"",""];
                    },
                    "errorTextFormat": function (data) {
                      console.log("error",data)
                        return 'Error: ' + data.responseText
                    }
                },
                // options for the Add Dialog
                {
                    addCaption: "Crear Vehículo",
                    closeAfterAdd: true,
                    recreateForm: true,
                    closeOnEscape:true,
                    onclickSubmit : function(params, posdata) {
                      var idsColores = colors.ids;
                      var descripcion = posdata["color.descripcion"];
                      posdata["color.descripcion"] = idsColores[descripcion];
                      return posdata;
                    },
                    "afterSubmit" : function( data, postdata, oper) 
                    {
                        var response = data.responseJSON;
                        if (response.hasOwnProperty("violations")) {
                                if(response.violations.length) 
                                {
                                        return [false,response.violations[0].message];
                                }
                        }
                        
                        if (response.hasOwnProperty("error")) {
                                if(response.error.length) 
                                {
                                        return [false,response.error];
                                }
                        }
                        
                        return [true,"",""];
                    },
                    "errorTextFormat": function (data) {
                        return 'Error: ' + data.responseText
                    }
                },
                // options for the Delete Dailog
                {
                    url: '0003B.php',
                    datatype: "json",
                   
                    errorTextFormat: function (data) {
                        return 'Error: ' + data.responseText
                    }
                }
                
                );

                $('#jqGrid').jqGrid('setGridHeight',($(window).innerHeight()-80));
                $('#jqGrid').jqGrid('setGridWidth',$(window).width() - 20);
       

});

    /*
     var height = $(window).height();
            $('.ui-jqgrid-bdiv').height(height-100);
            
            var alto = $(window).width();
            $('.ui-jqgrid-bdiv').width(alto);
            alert(alto);
            
            $("#jqGrid").jqGrid({with:100000});
            */
           
           $(window).resize(function(){
    $('#jqGrid').jqGrid('setGridHeight',($(window).innerHeight()-80));
    $('#jqGrid').jqGrid('setGridWidth',($(window).width()-20));
    


    
});
</script>
<script type="text/ecmascript" src="js/jquery-ui.min.js"></script>
<script>
        $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '<Ant',
        nextText: 'Sig>',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
        dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
        };
        $.datepicker.setDefaults($.datepicker.regional['es']);
</script>
    </body>
</html>